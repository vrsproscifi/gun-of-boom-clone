// Crashlytics Plugin
// Created by Patryk Stepniewski
// Copyright (c) 2014-2018 gameDNA Ltd. All Rights Reserved.

#pragma once

#include "Kismet/BlueprintFunctionLibrary.h"
#include "CrashlyticsBlueprintLibrary.generated.h"


UCLASS()
class CRASHLYTICS_API //UCrashlyticsBlueprintLibrary : public UBlueprintFunctionLibrary
{
	GENERATED_UCLASS_BODY()

	/**
	* Crashlytics initialization
	*/
	static void InitCrashlytics();

	UFUNCTION(BlueprintCallable)
	static void PreloadRewardedVideoAd();

	UFUNCTION(BlueprintCallable)
	static void ShowRewardedVideoAd();

	UFUNCTION(BlueprintCallable, Category = Crashlytics, DisplayName = "Crashlytics Set User Identifier")
	static FString GetUserCountry();

	/**
	* Personalize by user identifier
	*/
	UFUNCTION(BlueprintCallable, Category = Crashlytics, DisplayName = "Crashlytics Set User Identifier")
	static void SetUserIdentifier(const FString& Identifier);

	/**
	* Personalize by user email
	*/
	UFUNCTION(BlueprintCallable, Category = Crashlytics, DisplayName = "Crashlytics Set User Email")
	static void SetUserEmail(const FString& Email);

	/**
	* Personalize by user name
	*/
	UFUNCTION(BlueprintCallable, Category = Crashlytics, DisplayName = "Crashlytics Set User Name")
	static void SetUserName(const FString& Username);

	UFUNCTION(BlueprintCallable, Category = Answers)
	static void ContinuationConsumePurchase();

	UFUNCTION(BlueprintCallable, Category = Answers)
	static void LogLogin(const FString& Method);

	UFUNCTION(BlueprintCallable, Category = Answers)
	static void LogSignUp(const FString& Method);

	UFUNCTION(BlueprintCallable, Category = Answers)
	static void LogCustomEvent(const FString& InEvent);

	UFUNCTION(BlueprintCallable, Category = Answers)
	static void LogCustomEventEx(const FString& InEvent, const float& InAmount);

	

	UFUNCTION(BlueprintCallable, Category = Facebook)
	static void LogFbCustomEvent(const FString& InEvent);

	UFUNCTION(BlueprintCallable, Category = Facebook)
	static void LogFbCustomEventKeyValue(const FString& InEvent, const FString& InKey, const FString& InValue);

	UFUNCTION(BlueprintCallable, Category = Facebook)
	static void LogFbCustomEventEx(const FString& InEvent, const float& InAmount);

	UFUNCTION(BlueprintCallable, Category = Facebook)
	static void LogFbLevelUp(const int32& InNewLevel);

	UFUNCTION(BlueprintCallable, Category = Facebook)
	static void LogFbSpentCredits(const FString& InContent, const FString& InContentId, const FString& InContentType, const float& InTotalValue);

	UFUNCTION(BlueprintCallable, Category = Facebook)
	static void LogFbUpgradeItem(const FString& InItemId, const FString& InItemType, const FString& InItemName, const FString& InCurrency, const int32& InNewLevel, const float& InItemPrice);


	
	UFUNCTION(BlueprintCallable, Category = Answers)
	static void LogPurchase(const FString& InItemId, const FString& InItemType, const FString& InItemName, const FString& InReason, const FString& InCurrency, const float& InItemPrice, const bool& InSuccess);

	UFUNCTION(BlueprintCallable, Category = Answers)
	static void LogAddToCart(const FString& InItemId, const FString& InItemType, const FString& InItemName, const FString& InCurrency, const float& InItemPrice);

	UFUNCTION(BlueprintCallable, Category = Answers)
	static void LogStartCheckout(const FString& InCurrency, const float& InItemPrice);

	UFUNCTION(BlueprintCallable, Category = Answers)
	static void LogContentView(const FString& InContentId, const FString& InContentName, const FString& InContentType);

	UFUNCTION(BlueprintCallable, Category = Answers)
	static void LogContentSearch(const FString& InQuery);


	UFUNCTION(BlueprintCallable, Category = Answers)
	static void LogLevelStart(const FString& InEvent);

	UFUNCTION(BlueprintCallable, Category = Answers)
	static void LogLevelEnd(const FString& InEvent, const int32& InAmount, const bool& InSuccess);


	/**
	* Write custom log (dev and shipping build)
	*/
	UFUNCTION(BlueprintCallable, Category = Crashlytics, DisplayName = "Crashlytics Write Log")
	static void WriteLog(const FString& Log);

	static void SetField(const FString& InFieldName, const FName& InValue);
	static void SetField(const FString& InFieldName, const FText& InValue);
	static void SetField(const FString& InFieldName, const FTimespan& InValue);
	static void SetField(const FString& InFieldName, const FDateTime& InValue);
	static void SetField(const FString& InFieldName, const FGuid& InValue);
	static void SetField(const FString& InFieldName, const FString& InValue);
	static void SetField(const FString& InFieldName, const int32& InValue);
	static void SetField(const FString& InFieldName, const float& InValue);
	static void SetField(const FString& InFieldName, const bool& InValue);
	static void SetField(const FString& InFieldName, const double& InValue);
};
