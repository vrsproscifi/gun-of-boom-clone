#ifndef JAVAMEMBER_H
#define JAVAMEMBER_H

#if PLATFORM_ANDROID 

#include "jni.h"
#include "Android/AndroidJNI.h"
#include "Android/AndroidApplication.h"
#include "Android/AndroidJavaEnv.h"

 
template <typename... Args>
inline void CJavaObject::constructObject(JNIEnv * env, const FString& typeName, jobject iClassLoader , const FString& constructorSignature, Args... args)
{
	clearClass = true;
	if (iClassLoader == NULL)
	{
		mClassTypeReflector = env->FindClass(TCHAR_TO_UTF8(*typeName));
	}
	else
	{
	    jstring javaString = mEnvironment->NewStringUTF(TCHAR_TO_UTF8(*typeName));
		CJavaObject reflector(env, iClassLoader);
		jobject reflectorClassObject;

		reflector.Get("findClass","(Ljava/lang/String;)Ljava/lang/Class;")(&reflectorClassObject, javaString);
		mClassTypeReflector = (jclass)reflectorClassObject;
 		mEnvironment->DeleteLocalRef(javaString);
	}
	jmethodID classConstructor = env->GetMethodID(mClassTypeReflector, "<init>", TCHAR_TO_UTF8(*constructorSignature));
	mActualObject = env->NewObject(mClassTypeReflector, classConstructor, args...);
}

//////////////////////////////////////////////////////////////////////////
//Set fields

template <>
inline CJavaMember& CJavaMember::operator = <int>(int value) { 
    jfieldID fid = mEnvironment->GetFieldID(mClassTypeReflector, TCHAR_TO_UTF8(*mName), "I");
    mEnvironment->SetIntField(mObject,fid,value);
    return *this;
}

template <>
inline CJavaMember& CJavaMember::operator = <long long>(long long value) { 
    jfieldID fid = mEnvironment->GetFieldID(mClassTypeReflector, TCHAR_TO_UTF8(*mName), "J");
    mEnvironment->SetLongField(mObject,fid,value);
    return *this;
}

template <>
inline CJavaMember& CJavaMember::operator = <float>(float value) { 
    jfieldID fid = mEnvironment->GetFieldID(mClassTypeReflector, TCHAR_TO_UTF8(*mName), "F");
    mEnvironment->SetFloatField(mObject,fid,value);
    return *this;
}

template <>
inline CJavaMember& CJavaMember::operator = <double>(double value) { 
    jfieldID fid = mEnvironment->GetFieldID(mClassTypeReflector, TCHAR_TO_UTF8(*mName), "D");
    mEnvironment->SetDoubleField(mObject,fid,value);
    return *this;
}

template <>
inline CJavaMember& CJavaMember::operator = <unsigned short>(unsigned short value) { 
    jfieldID fid = mEnvironment->GetFieldID(mClassTypeReflector, TCHAR_TO_UTF8(*mName), "C");
    mEnvironment->SetCharField(mObject,fid,value);
    return *this;
}

template <>
inline CJavaMember& CJavaMember::operator = <short>(short value) { 
    jfieldID fid = mEnvironment->GetFieldID(mClassTypeReflector, TCHAR_TO_UTF8(*mName), "S");
    mEnvironment->SetShortField(mObject,fid,value);
    return *this;
}

template <>
inline CJavaMember& CJavaMember::operator = <bool>(bool value) { 
    jfieldID fid = mEnvironment->GetFieldID(mClassTypeReflector, TCHAR_TO_UTF8(*mName), "Z");
    mEnvironment->SetBooleanField(mObject,fid,value);
    return *this;
}

template <>
inline CJavaMember& CJavaMember::operator = <void*>(void* value) { 
    jfieldID fid = mEnvironment->GetFieldID(mClassTypeReflector, TCHAR_TO_UTF8(*mName), "J");
    mEnvironment->SetLongField(mObject,fid, (jlong)value); //cast pointer to jlong
    return *this;
}


template <>
inline CJavaMember& CJavaMember::operator = <jobject>(jobject value) { 
    jfieldID fid = mEnvironment->GetFieldID(mClassTypeReflector, TCHAR_TO_UTF8(*mName),TCHAR_TO_UTF8(*mType));
    mEnvironment->SetObjectField(mObject,fid, value); //cast pointer to jlong
    return *this;
}

template <>
inline CJavaMember& CJavaMember::operator = <CJavaObject>(CJavaObject value) {
    jfieldID fid = mEnvironment->GetFieldID(mClassTypeReflector, TCHAR_TO_UTF8(*mName),TCHAR_TO_UTF8(*mType));
    mEnvironment->SetObjectField(mObject,fid, (jobject)value); //cast pointer to jlong
    return *this;
}

template <>
inline CJavaMember& CJavaMember::operator = <FString>(FString value) { 
    jfieldID fid = mEnvironment->GetFieldID(mClassTypeReflector, TCHAR_TO_UTF8(*mName),TCHAR_TO_UTF8(*mType));
    jstring javaString = mEnvironment->NewStringUTF(TCHAR_TO_UTF8(*value));
    mEnvironment->SetObjectField(mObject,fid, javaString);
    //mEnvironment->DeleteLocalRef(javaString);
    return *this;
}

//////////////////////////////////////////////////////////////////////////
//Get fields (cast)

template <>
inline void* CJavaMember::cast<void*>() const  { 
    jfieldID fid = mEnvironment->GetFieldID(mClassTypeReflector, TCHAR_TO_UTF8(*mName), "J");
    jlong nativePointer = mEnvironment->GetLongField(mObject,fid);
    return (void*) nativePointer;
}


template <>
inline int CJavaMember::cast<int>() const  { 
    jfieldID fid = mEnvironment->GetFieldID(mClassTypeReflector, TCHAR_TO_UTF8(*mName), "I");
    return mEnvironment->GetIntField(mObject,fid);
}

template <>
inline long long CJavaMember::cast<long long>() const  { 
    jfieldID fid = mEnvironment->GetFieldID(mClassTypeReflector, TCHAR_TO_UTF8(*mName), "J");
    return mEnvironment->GetLongField(mObject,fid);
}

template <>
inline float CJavaMember::cast<float>() const  { 
    jfieldID fid = mEnvironment->GetFieldID(mClassTypeReflector, TCHAR_TO_UTF8(*mName), "F");
    return mEnvironment->GetFloatField(mObject,fid);
}

template <>
inline double CJavaMember::cast<double>() const  { 
    jfieldID fid = mEnvironment->GetFieldID(mClassTypeReflector, TCHAR_TO_UTF8(*mName), "D");
    return mEnvironment->GetDoubleField(mObject,fid);
}

template <>
inline unsigned short CJavaMember::cast<unsigned short>() const { 
    jfieldID fid = mEnvironment->GetFieldID(mClassTypeReflector, TCHAR_TO_UTF8(*mName), "C");
    return mEnvironment->GetCharField(mObject,fid);
}

template <>
inline short CJavaMember::cast<short>() const { 
    jfieldID fid = mEnvironment->GetFieldID(mClassTypeReflector, TCHAR_TO_UTF8(*mName), "S");
    return mEnvironment->GetShortField(mObject,fid);
}

template <>
inline bool CJavaMember::cast<bool>() const { 
    jfieldID fid = mEnvironment->GetFieldID(mClassTypeReflector, TCHAR_TO_UTF8(*mName), "Z");
    return mEnvironment->GetBooleanField(mObject,fid) == JNI_TRUE;
}

template <>
inline jobject CJavaMember::cast<jobject>() const { 
    jfieldID fid = mEnvironment->GetFieldID(mClassTypeReflector, TCHAR_TO_UTF8(*mName),TCHAR_TO_UTF8(*mType));
    return mEnvironment->GetObjectField(mObject,fid);
}

//////////////////////////////////////////////////////////////////////////
// Functions

template <>
inline void CJavaMember::JavaCallFunction<void> (jmethodID  mid, va_list iParams) //void no return value
{
    mEnvironment->CallVoidMethodV(mObject,mid, iParams);
}

inline void CJavaMember::JavaCallFunctionToList (jmethodID  mid, ...)
{
    va_list vaList;
    va_start (vaList, mid);
    JavaCallFunction<void>(mid, vaList);
    va_end(vaList);
}

template <>
inline jint CJavaMember::JavaCallFunction<jint> (jmethodID  mid, va_list iParams) 
{  
    return mEnvironment->CallIntMethodV(mObject,mid, iParams);
}

template <>
inline jlong CJavaMember::JavaCallFunction<jlong>(jmethodID  mid, va_list iParams)  { 
    return mEnvironment->CallLongMethodV(mObject,mid, iParams);
}

template <>
inline jfloat CJavaMember::JavaCallFunction<jfloat>(jmethodID  mid, va_list iParams)  { 
    return mEnvironment->CallFloatMethodV(mObject,mid, iParams);
}

template <>
inline jdouble CJavaMember::JavaCallFunction<jdouble>(jmethodID  mid, va_list iParams)  { 
    return mEnvironment->CallDoubleMethodV(mObject,mid, iParams);
}

template <>
inline jchar CJavaMember::JavaCallFunction<jchar>(jmethodID  mid, va_list iParams)  { 
    return mEnvironment->CallCharMethodV(mObject,mid, iParams);
}

template <>
inline jshort CJavaMember::JavaCallFunction<jshort>(jmethodID  mid, va_list iParams)  { 
    return mEnvironment->CallShortMethodV(mObject,mid, iParams);
}

template <>
inline jboolean CJavaMember::JavaCallFunction<jboolean>(jmethodID  mid, va_list iParams)  { 
    return mEnvironment->CallBooleanMethodV(mObject,mid, iParams);
}

template <>
inline jobject CJavaMember::JavaCallFunction<jobject>(jmethodID  mid, va_list iParams)  { 
    return mEnvironment->CallObjectMethodV(mObject,mid, iParams);
}


//inline constructor implementation

inline CJavaMember::CJavaMember (CJavaObject* javaobj, const FString& name, const FString& javaObjectType) :
mEnvironment(javaobj->mEnvironment) , mClassTypeReflector(javaobj->mClassTypeReflector), mObject(javaobj->mActualObject), mName(name), mType(javaObjectType)
{

}
inline CJavaMember::CJavaMember(CJavaObject* javaobj, const FString& name):
mEnvironment(javaobj->mEnvironment) , mClassTypeReflector(javaobj->mClassTypeReflector), mObject(javaobj->mActualObject), mName(name)
{

}

template<typename TObjectValueType>
inline CJavaMember::CJavaMember(CJavaObject * javaobj, const FString & name, const FString& javaObjectType, const TObjectValueType & Value)
	: CJavaMember(javaobj, name, javaObjectType)
{
	CJavaMember::operator=<TObjectValueType>(Value);
}

#endif
#endif