// Crashlytics Plugin
// Created by Patryk Stepniewski
// Copyright (c) 2014-2018 gameDNA Ltd. All Rights Reserved.

//#include "CrashlyticsBlueprintLibrary.h"
#include "Runtime/Engine/Classes/Kismet/KismetSystemLibrary.h"

#if PLATFORM_ANDROID && UE_BUILD_SHIPPING
#include "Android/AndroidJNI.h"
#include "Android/AndroidApplication.h"
#include <android_native_app_glue.h>
#elif PLATFORM_IOS
#import <Fabric/Fabric.h>
#import <Crashlytics/Crashlytics.h>
#endif

//UCrashlyticsBlueprintLibrary:://UCrashlyticsBlueprintLibrary(const FObjectInitializer& ObjectInitializer)
	: Super(ObjectInitializer)
{
}

void //UCrashlyticsBlueprintLibrary::InitCrashlytics()
{
#if PLATFORM_ANDROID && UE_BUILD_SHIPPING

#elif PLATFORM_IOS
	dispatch_async(dispatch_get_main_queue(), ^{
		[Fabric with : @[[Crashlytics class]]];
	});
#endif
}


void //UCrashlyticsBlueprintLibrary::PreloadRewardedVideoAd()
{
//#if PLATFORM_ANDROID
//	if (JNIEnv* Env = FAndroidApplication::GetJavaEnv())
//	{
//		static jmethodID Method = FJavaWrapper::FindMethod(Env, FJavaWrapper::GameActivityClassID, "AndroidThunkJava_PreloadRewardedVideoAd", "()V", false);
//		FJavaWrapper::CallVoidMethod(Env, FJavaWrapper::GameActivityThis, Method);
//	}
//#elif PLATFORM_IOS
//
//#endif
}

void //UCrashlyticsBlueprintLibrary::ShowRewardedVideoAd()
{
//#if PLATFORM_ANDROID
//	if (JNIEnv* Env = FAndroidApplication::GetJavaEnv())
//	{
//		static jmethodID Method = FJavaWrapper::FindMethod(Env, FJavaWrapper::GameActivityClassID, "AndroidThunkJava_ShowRewardedVideoAd", "()V", false);
//		FJavaWrapper::CallVoidMethod(Env, FJavaWrapper::GameActivityThis, Method);
//	}
//#elif PLATFORM_IOS
//
//#endif
}

void //UCrashlyticsBlueprintLibrary::SetField(const FString& InFieldName, const FName& InValue)
{
	SetField(InFieldName, InValue.ToString());
}

void //UCrashlyticsBlueprintLibrary::SetField(const FString& InFieldName, const FText& InValue)
{
	SetField(InFieldName, InValue.ToString());
}

void //UCrashlyticsBlueprintLibrary::SetField(const FString& InFieldName, const FTimespan& InValue)
{
	SetField(InFieldName, InValue.ToString());
}

void //UCrashlyticsBlueprintLibrary::SetField(const FString& InFieldName, const FDateTime& InValue)
{
	SetField(InFieldName, InValue.ToString());
}

void //UCrashlyticsBlueprintLibrary::SetField(const FString& InFieldName, const FGuid& InValue)
{
	SetField(InFieldName, InValue.ToString());
}

void //UCrashlyticsBlueprintLibrary::SetField(const FString& InFieldName, const FString& InValue)
{
#if PLATFORM_ANDROID && UE_BUILD_SHIPPING
	if (JNIEnv* Env = FAndroidApplication::GetJavaEnv())
	{
		jstring FieldName = Env->NewStringUTF(TCHAR_TO_UTF8(*InFieldName));
		jstring Value = Env->NewStringUTF(TCHAR_TO_UTF8(*InValue));
		static jmethodID Method = FJavaWrapper::FindMethod(Env, FJavaWrapper::GameActivityClassID, "AndroidThunkJava_SetString", "(Ljava/lang/String;Ljava/lang/String;)V", false);
		FJavaWrapper::CallVoidMethod(Env, FJavaWrapper::GameActivityThis, Method, FieldName, Value);
		Env->DeleteLocalRef(FieldName);
		Env->DeleteLocalRef(Value);
	}
#elif PLATFORM_IOS

#endif
}

void //UCrashlyticsBlueprintLibrary::ContinuationConsumePurchase()
{
#if PLATFORM_ANDROID && UE_BUILD_SHIPPING
	if (JNIEnv* Env = FAndroidApplication::GetJavaEnv())
	{
		static jmethodID Method = FJavaWrapper::FindMethod(Env, FJavaWrapper::GameActivityClassID, "ContinuationConsumePurchase", "()V", false);
		FJavaWrapper::CallVoidMethod(Env, FJavaWrapper::GameActivityThis, Method);
	}
#elif PLATFORM_IOS

#endif
}


void //UCrashlyticsBlueprintLibrary::SetField(const FString& InFieldName, const int32& InValue)
{
#if PLATFORM_ANDROID && UE_BUILD_SHIPPING
	if (JNIEnv* Env = FAndroidApplication::GetJavaEnv())
	{
		jstring FieldName = Env->NewStringUTF(TCHAR_TO_UTF8(*InFieldName));
		static jmethodID Method = FJavaWrapper::FindMethod(Env, FJavaWrapper::GameActivityClassID, "AndroidThunkJava_SetInt", "(Ljava/lang/String;I)V", false);
		FJavaWrapper::CallVoidMethod(Env, FJavaWrapper::GameActivityThis, Method, FieldName, InValue);
		Env->DeleteLocalRef(FieldName);
	}
#elif PLATFORM_IOS

#endif
}

void //UCrashlyticsBlueprintLibrary::SetField(const FString& InFieldName, const float& InValue)
{
#if PLATFORM_ANDROID && UE_BUILD_SHIPPING
	if (JNIEnv* Env = FAndroidApplication::GetJavaEnv())
	{
		jstring FieldName = Env->NewStringUTF(TCHAR_TO_UTF8(*InFieldName));
		static jmethodID Method = FJavaWrapper::FindMethod(Env, FJavaWrapper::GameActivityClassID, "AndroidThunkJava_SetFloat", "(Ljava/lang/String;F)V", false);
		FJavaWrapper::CallVoidMethod(Env, FJavaWrapper::GameActivityThis, Method, FieldName, InValue);
		Env->DeleteLocalRef(FieldName);
	}
#elif PLATFORM_IOS

#endif
}

void //UCrashlyticsBlueprintLibrary::SetField(const FString& InFieldName, const bool& InValue)
{
#if PLATFORM_ANDROID && UE_BUILD_SHIPPING
	if (JNIEnv* Env = FAndroidApplication::GetJavaEnv())
	{
		jstring FieldName = Env->NewStringUTF(TCHAR_TO_UTF8(*InFieldName));
		static jmethodID Method = FJavaWrapper::FindMethod(Env, FJavaWrapper::GameActivityClassID, "AndroidThunkJava_SetBool", "(Ljava/lang/String;Z)V", false);
		FJavaWrapper::CallVoidMethod(Env, FJavaWrapper::GameActivityThis, Method, FieldName, InValue);
		Env->DeleteLocalRef(FieldName);
	}
#elif PLATFORM_IOS

#endif
}

void //UCrashlyticsBlueprintLibrary::SetField(const FString& InFieldName, const double& InValue)
{
#if PLATFORM_ANDROID && UE_BUILD_SHIPPING
	if (JNIEnv* Env = FAndroidApplication::GetJavaEnv())
	{
		jstring FieldName = Env->NewStringUTF(TCHAR_TO_UTF8(*InFieldName));
		static jmethodID Method = FJavaWrapper::FindMethod(Env, FJavaWrapper::GameActivityClassID, "AndroidThunkJava_SetDouble", "(Ljava/lang/String;D)V", false);
		FJavaWrapper::CallVoidMethod(Env, FJavaWrapper::GameActivityThis, Method, FieldName, InValue);
		Env->DeleteLocalRef(FieldName);
	}
#elif PLATFORM_IOS

#endif
}

void //UCrashlyticsBlueprintLibrary::LogLogin(const FString& Method)
{
#if PLATFORM_ANDROID && UE_BUILD_SHIPPING
	if (JNIEnv* Env = FAndroidApplication::GetJavaEnv())
	{
		jstring IdentifierFinal = Env->NewStringUTF(TCHAR_TO_UTF8(*Method));
		static jmethodID Method = FJavaWrapper::FindMethod(Env, FJavaWrapper::GameActivityClassID, "AndroidThunkJava_LogLogin", "(Ljava/lang/String;)V", false);
		FJavaWrapper::CallVoidMethod(Env, FJavaWrapper::GameActivityThis, Method, IdentifierFinal);
		Env->DeleteLocalRef(IdentifierFinal);
	}
#elif PLATFORM_IOS

#endif
}

void //UCrashlyticsBlueprintLibrary::LogSignUp(const FString& Method)
{
#if PLATFORM_ANDROID && UE_BUILD_SHIPPING
	if (JNIEnv* Env = FAndroidApplication::GetJavaEnv())
	{
		jstring IdentifierFinal = Env->NewStringUTF(TCHAR_TO_UTF8(*Method));
		static jmethodID Method = FJavaWrapper::FindMethod(Env, FJavaWrapper::GameActivityClassID, "AndroidThunkJava_LogSignUp", "(Ljava/lang/String;)V", false);
		FJavaWrapper::CallVoidMethod(Env, FJavaWrapper::GameActivityThis, Method, IdentifierFinal);
		Env->DeleteLocalRef(IdentifierFinal);
	}
#elif PLATFORM_IOS

#endif
}

void //UCrashlyticsBlueprintLibrary::LogFbCustomEventKeyValue(const FString& InEvent, const FString& InKey, const FString& InValue)
{
#if PLATFORM_ANDROID && UE_BUILD_SHIPPING
	if (JNIEnv* Env = FAndroidApplication::GetJavaEnv())
	{
		jstring IdentifierFinal = Env->NewStringUTF(TCHAR_TO_UTF8(*InEvent));
		jstring KeyFinal = Env->NewStringUTF(TCHAR_TO_UTF8(*InKey));
		jstring ValueFinal = Env->NewStringUTF(TCHAR_TO_UTF8(*InValue));
		
		static jmethodID Method = FJavaWrapper::FindMethod(Env, FJavaWrapper::GameActivityClassID, "AndroidThunkJava_LogFbCustomEventKeyValue", "(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V", false);
		FJavaWrapper::CallVoidMethod(Env, FJavaWrapper::GameActivityThis, Method, IdentifierFinal, KeyFinal, ValueFinal);
		
		Env->DeleteLocalRef(IdentifierFinal);
		Env->DeleteLocalRef(KeyFinal);
		Env->DeleteLocalRef(ValueFinal);
	}
#elif PLATFORM_IOS

#endif
}


void //UCrashlyticsBlueprintLibrary::LogFbCustomEvent(const FString& InEvent)
{
#if PLATFORM_ANDROID && UE_BUILD_SHIPPING
	if (JNIEnv* Env = FAndroidApplication::GetJavaEnv())
	{
		jstring IdentifierFinal = Env->NewStringUTF(TCHAR_TO_UTF8(*InEvent));
		static jmethodID Method = FJavaWrapper::FindMethod(Env, FJavaWrapper::GameActivityClassID, "AndroidThunkJava_LogFbCustomEvent", "(Ljava/lang/String;)V", false);
		FJavaWrapper::CallVoidMethod(Env, FJavaWrapper::GameActivityThis, Method, IdentifierFinal);
		Env->DeleteLocalRef(IdentifierFinal);
	}
#elif PLATFORM_IOS

#endif
}

void //UCrashlyticsBlueprintLibrary::LogFbCustomEventEx(const FString& InEvent, const float& InAmount)
{
#if PLATFORM_ANDROID && UE_BUILD_SHIPPING 
	if (JNIEnv* Env = FAndroidApplication::GetJavaEnv())
	{
		jstring IdentifierFinal = Env->NewStringUTF(TCHAR_TO_UTF8(*InEvent));
		static jmethodID Method = FJavaWrapper::FindMethod(Env, FJavaWrapper::GameActivityClassID, "AndroidThunkJava_LogFbCustomEvent", "(Ljava/lang/String;F)V", false);
		FJavaWrapper::CallVoidMethod(Env, FJavaWrapper::GameActivityThis, Method, IdentifierFinal, InAmount);
		Env->DeleteLocalRef(IdentifierFinal);
	}
#elif PLATFORM_IOS

#endif
}

void //UCrashlyticsBlueprintLibrary::LogFbLevelUp(const int32& InNewLevel)
{
#if PLATFORM_ANDROID && UE_BUILD_SHIPPING 
	if (JNIEnv* Env = FAndroidApplication::GetJavaEnv())
	{
		static jmethodID Method = FJavaWrapper::FindMethod(Env, FJavaWrapper::GameActivityClassID, "AndroidThunkJava_LogFbLevelUp", "(I)V", false);
		FJavaWrapper::CallVoidMethod(Env, FJavaWrapper::GameActivityThis, Method, InNewLevel);
	}
#elif PLATFORM_IOS

#endif
}

void //UCrashlyticsBlueprintLibrary::LogFbSpentCredits(const FString& InContent, const FString& InContentId, const FString& InContentType, const float& InTotalValue)
{
#if PLATFORM_ANDROID && UE_BUILD_SHIPPING 
	if (JNIEnv* Env = FAndroidApplication::GetJavaEnv())
	{
		jstring ContentFinal = Env->NewStringUTF(TCHAR_TO_UTF8(*InContent));
		jstring ContentIdFinal = Env->NewStringUTF(TCHAR_TO_UTF8(*InContentId));
		jstring ContentTypeFinal = Env->NewStringUTF(TCHAR_TO_UTF8(*InContentType));
		
		static jmethodID Method = FJavaWrapper::FindMethod(Env, FJavaWrapper::GameActivityClassID, "AndroidThunkJava_LogFbSpentCredits", "(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;D)V", false);
		FJavaWrapper::CallVoidMethod(Env, FJavaWrapper::GameActivityThis, Method, ContentFinal, ContentIdFinal, ContentTypeFinal, static_cast<double>(InTotalValue));

		Env->DeleteLocalRef(ContentFinal);
		Env->DeleteLocalRef(ContentIdFinal);
		Env->DeleteLocalRef(ContentTypeFinal);
	}
#elif PLATFORM_IOS

#endif
}

void //UCrashlyticsBlueprintLibrary::LogFbUpgradeItem(const FString& InItemId, const FString& InItemType,	const FString& InItemName, const FString& InCurrency, const int32& InNewLevel, const float& InItemPrice)
{
#if PLATFORM_ANDROID && UE_BUILD_SHIPPING 
	if (JNIEnv* Env = FAndroidApplication::GetJavaEnv())
	{
		jstring ItemIdFinal = Env->NewStringUTF(TCHAR_TO_UTF8(*InItemId));
		jstring ItemTypeFinal = Env->NewStringUTF(TCHAR_TO_UTF8(*InItemType));
		jstring ItemNameFinal = Env->NewStringUTF(TCHAR_TO_UTF8(*InItemName));
		jstring CurrencyFinal = Env->NewStringUTF(TCHAR_TO_UTF8(*InCurrency));

		static jmethodID Method = FJavaWrapper::FindMethod(Env, FJavaWrapper::GameActivityClassID, "AndroidThunkJava_LogFbUpgradeItem", "(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;ID)V", false);
		FJavaWrapper::CallVoidMethod(Env, FJavaWrapper::GameActivityThis, Method, ItemIdFinal, ItemTypeFinal, ItemNameFinal, CurrencyFinal, InNewLevel, static_cast<double>(InItemPrice));

		Env->DeleteLocalRef(ItemIdFinal);
		Env->DeleteLocalRef(ItemTypeFinal);
		Env->DeleteLocalRef(ItemNameFinal);
		Env->DeleteLocalRef(CurrencyFinal);
	}
#elif PLATFORM_IOS

#endif
}

void //UCrashlyticsBlueprintLibrary::LogCustomEvent(const FString& InEvent)
{
	return;
#if PLATFORM_ANDROID && UE_BUILD_SHIPPING
	if (JNIEnv* Env = FAndroidApplication::GetJavaEnv())
	{
		jstring IdentifierFinal = Env->NewStringUTF(TCHAR_TO_UTF8(*InEvent));
		static jmethodID Method = FJavaWrapper::FindMethod(Env, FJavaWrapper::GameActivityClassID, "AndroidThunkJava_LogCustomEvent", "(Ljava/lang/String;)V", false);
		FJavaWrapper::CallVoidMethod(Env, FJavaWrapper::GameActivityThis, Method, IdentifierFinal);
		Env->DeleteLocalRef(IdentifierFinal);
	}
#elif PLATFORM_IOS

#endif
}

void //UCrashlyticsBlueprintLibrary::LogCustomEventEx(const FString& InEvent, const float& InAmount)
{
		return;

#if PLATFORM_ANDROID && UE_BUILD_SHIPPING 
	if (JNIEnv* Env = FAndroidApplication::GetJavaEnv())
	{
		jstring IdentifierFinal = Env->NewStringUTF(TCHAR_TO_UTF8(*InEvent));
		static jmethodID Method = FJavaWrapper::FindMethod(Env, FJavaWrapper::GameActivityClassID, "AndroidThunkJava_LogCustomEvent", "(Ljava/lang/String;D)V", false);
		FJavaWrapper::CallVoidMethod(Env, FJavaWrapper::GameActivityThis, Method, IdentifierFinal, InAmount);
		Env->DeleteLocalRef(IdentifierFinal);
	}
#elif PLATFORM_IOS

#endif
}

void //UCrashlyticsBlueprintLibrary::LogPurchase(const FString& InItemId, const FString& InItemType, const FString& InItemName, const FString& InReason, const FString& InCurrency, const float& InItemPrice, const bool& InSuccess)
{
#if PLATFORM_ANDROID && UE_BUILD_SHIPPING 
	if (JNIEnv* Env = FAndroidApplication::GetJavaEnv())
	{
		static jmethodID Method = FJavaWrapper::FindMethod(Env, FJavaWrapper::GameActivityClassID, "AndroidThunkJava_LogPurchase", "(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;DZ)V", false);

		jstring ItemId = Env->NewStringUTF(TCHAR_TO_UTF8(*InItemId));
		jstring ItemType = Env->NewStringUTF(TCHAR_TO_UTF8(*InItemType));
		jstring ItemName = Env->NewStringUTF(TCHAR_TO_UTF8(*InItemName));
		jstring Currency = Env->NewStringUTF(TCHAR_TO_UTF8(*InCurrency));
		jstring Reason = Env->NewStringUTF(TCHAR_TO_UTF8(*InReason));

		FJavaWrapper::CallVoidMethod(Env, FJavaWrapper::GameActivityThis, Method, ItemId, ItemType, ItemName, Reason, Currency, static_cast<double>(InItemPrice), InSuccess);
		
		Env->DeleteLocalRef(ItemId);
		Env->DeleteLocalRef(ItemType);
		Env->DeleteLocalRef(ItemName);
		Env->DeleteLocalRef(Currency);
		Env->DeleteLocalRef(Reason);

	}
#elif PLATFORM_IOS

#endif
}

void //UCrashlyticsBlueprintLibrary::LogStartCheckout(const FString& InCurrency, const float& InItemPrice)
{
#if PLATFORM_ANDROID && UE_BUILD_SHIPPING 
	if (JNIEnv* Env = FAndroidApplication::GetJavaEnv())
	{
		static jmethodID Method = FJavaWrapper::FindMethod(Env, FJavaWrapper::GameActivityClassID, "AndroidThunkJava_LogStartCheckout", "(Ljava/lang/String;D)V", false);

		jstring Currency = Env->NewStringUTF(TCHAR_TO_UTF8(*InCurrency));

		FJavaWrapper::CallVoidMethod(Env, FJavaWrapper::GameActivityThis, Method, Currency, static_cast<double>(InItemPrice));


		Env->DeleteLocalRef(Currency);
	}
#elif PLATFORM_IOS

#endif
}


void //UCrashlyticsBlueprintLibrary::LogAddToCart(const FString& InItemId, const FString& InItemType, const FString& InItemName, const FString& InCurrency, const float& InItemPrice)
{
#if PLATFORM_ANDROID && UE_BUILD_SHIPPING 
	if (JNIEnv* Env = FAndroidApplication::GetJavaEnv())
	{
		static jmethodID Method = FJavaWrapper::FindMethod(Env, FJavaWrapper::GameActivityClassID, "AndroidThunkJava_LogAddToCart", "(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;D)V", false);

		jstring ItemId = Env->NewStringUTF(TCHAR_TO_UTF8(*InItemId));
		jstring ItemType = Env->NewStringUTF(TCHAR_TO_UTF8(*InItemType));
		jstring ItemName = Env->NewStringUTF(TCHAR_TO_UTF8(*InItemName));
		jstring Currency = Env->NewStringUTF(TCHAR_TO_UTF8(*InCurrency));

		FJavaWrapper::CallVoidMethod(Env, FJavaWrapper::GameActivityThis, Method, ItemId, ItemType, ItemName, Currency, static_cast<double>(InItemPrice));

		Env->DeleteLocalRef(ItemId);
		Env->DeleteLocalRef(ItemType);
		Env->DeleteLocalRef(ItemName);
		Env->DeleteLocalRef(Currency);
	}
#elif PLATFORM_IOS

#endif
}

void //UCrashlyticsBlueprintLibrary::LogContentSearch(const FString& InQuery)
{
#if PLATFORM_ANDROID && UE_BUILD_SHIPPING 
	if (JNIEnv* Env = FAndroidApplication::GetJavaEnv())
	{
		static jmethodID Method = FJavaWrapper::FindMethod(Env, FJavaWrapper::GameActivityClassID, "AndroidThunkJava_LogContentSearch", "(Ljava/lang/String;)V", false);

		jstring Query = Env->NewStringUTF(TCHAR_TO_UTF8(*InQuery));

		FJavaWrapper::CallVoidMethod(Env, FJavaWrapper::GameActivityThis, Method, Query);

		Env->DeleteLocalRef(Query);
	}
#elif PLATFORM_IOS

#endif
}

void //UCrashlyticsBlueprintLibrary::LogContentView(const FString& InContentId, const FString& InContentName, const FString& InContentType)
{
#if PLATFORM_ANDROID && UE_BUILD_SHIPPING 
	if (JNIEnv* Env = FAndroidApplication::GetJavaEnv())
	{
		static jmethodID Method = FJavaWrapper::FindMethod(Env, FJavaWrapper::GameActivityClassID, "AndroidThunkJava_LogContentView", "(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V", false);

		jstring ContentId = Env->NewStringUTF(TCHAR_TO_UTF8(*InContentId));
		jstring ContentName = Env->NewStringUTF(TCHAR_TO_UTF8(*InContentName));
		jstring ContentType = Env->NewStringUTF(TCHAR_TO_UTF8(*InContentType));

		FJavaWrapper::CallVoidMethod(Env, FJavaWrapper::GameActivityThis, Method, ContentId, ContentName, ContentType);

		Env->DeleteLocalRef(ContentId);
		Env->DeleteLocalRef(ContentName);
		Env->DeleteLocalRef(ContentType);
	}
#elif PLATFORM_IOS

#endif
}

void //UCrashlyticsBlueprintLibrary::LogLevelStart(const FString& InEvent)
{
#if PLATFORM_ANDROID && UE_BUILD_SHIPPING 
	if (JNIEnv* Env = FAndroidApplication::GetJavaEnv())
	{
		static jmethodID Method = FJavaWrapper::FindMethod(Env, FJavaWrapper::GameActivityClassID, "AndroidThunkJava_LogLevelStart", "(Ljava/lang/String;)V", false);

		jstring Event = Env->NewStringUTF(TCHAR_TO_UTF8(*InEvent));

		FJavaWrapper::CallVoidMethod(Env, FJavaWrapper::GameActivityThis, Method, Event);

		Env->DeleteLocalRef(Event);
	}
#elif PLATFORM_IOS

#endif
}

void //UCrashlyticsBlueprintLibrary::LogLevelEnd(const FString& InEvent, const int32& InAmount, const bool& InSuccess)
{
#if PLATFORM_ANDROID && UE_BUILD_SHIPPING 
	if (JNIEnv* Env = FAndroidApplication::GetJavaEnv())
	{
		static jmethodID Method = FJavaWrapper::FindMethod(Env, FJavaWrapper::GameActivityClassID, "AndroidThunkJava_LogLevelEnd", "(Ljava/lang/String;IZ)V", false);

		jstring Event = Env->NewStringUTF(TCHAR_TO_UTF8(*InEvent));

		FJavaWrapper::CallVoidMethod(Env, FJavaWrapper::GameActivityThis, Method, Event, InAmount, InSuccess);

		Env->DeleteLocalRef(Event);
	}
#elif PLATFORM_IOS

#endif
}



FString //UCrashlyticsBlueprintLibrary::GetUserCountry()
{
	FString Result = FString("us");

#if PLATFORM_ANDROID && UE_BUILD_SHIPPING
	if (JNIEnv* Env = FAndroidApplication::GetJavaEnv())
	{
		static jmethodID Method = FJavaWrapper::FindMethod(Env, FJavaWrapper::GameActivityClassID, "AndroidThunkJava_GetUserCountry", "()Ljava/lang/String;", false);
		jstring JavaString = (jstring)FJavaWrapper::CallObjectMethod(Env, FJavaWrapper::GameActivityThis, Method);
		if (JavaString != NULL)
		{
			const char* JavaChars = Env->GetStringUTFChars(JavaString, 0);
			Result = FString(UTF8_TO_TCHAR(JavaChars));
			Env->ReleaseStringUTFChars(JavaString, JavaChars);
			Env->DeleteLocalRef(JavaString);
		}
		else
		{
			Result = "StrNull";
		}
	}
	else
	{
		Result = "EnvNull";
	}
#endif
	return Result;
}

void //UCrashlyticsBlueprintLibrary::SetUserIdentifier(const FString& Identifier)
{
#if PLATFORM_ANDROID && UE_BUILD_SHIPPING
	if (JNIEnv* Env = FAndroidApplication::GetJavaEnv())
	{
		jstring IdentifierFinal = Env->NewStringUTF(TCHAR_TO_UTF8(*Identifier));
		static jmethodID Method = FJavaWrapper::FindMethod(Env, FJavaWrapper::GameActivityClassID, "AndroidThunkJava_SetUserIdentifier", "(Ljava/lang/String;)V", false);
		FJavaWrapper::CallVoidMethod(Env, FJavaWrapper::GameActivityThis, Method, IdentifierFinal);
		Env->DeleteLocalRef(IdentifierFinal);
	}
#elif PLATFORM_IOS
	dispatch_async(dispatch_get_main_queue(), ^{	
		[CrashlyticsKit setUserIdentifier : Identifier.GetNSString()];
	});
#endif
}

void //UCrashlyticsBlueprintLibrary::SetUserEmail(const FString& Email)
{
#if PLATFORM_ANDROID && UE_BUILD_SHIPPING
	if (JNIEnv* Env = FAndroidApplication::GetJavaEnv())
	{
		jstring EmailFinal = Env->NewStringUTF(TCHAR_TO_UTF8(*Email));
		static jmethodID Method = FJavaWrapper::FindMethod(Env, FJavaWrapper::GameActivityClassID, "AndroidThunkJava_SetUserEmail", "(Ljava/lang/String;)V", false);
		FJavaWrapper::CallVoidMethod(Env, FJavaWrapper::GameActivityThis, Method, EmailFinal);
		Env->DeleteLocalRef(EmailFinal);
	}
#elif PLATFORM_IOS
	dispatch_async(dispatch_get_main_queue(), ^{
		[CrashlyticsKit setUserEmail : Email.GetNSString()];
	});
#endif
}

void //UCrashlyticsBlueprintLibrary::SetUserName(const FString& Username)
{
#if PLATFORM_ANDROID && UE_BUILD_SHIPPING
	if (JNIEnv* Env = FAndroidApplication::GetJavaEnv())
	{
		jstring UsernameFinal = Env->NewStringUTF(TCHAR_TO_UTF8(*Username));
		static jmethodID Method = FJavaWrapper::FindMethod(Env, FJavaWrapper::GameActivityClassID, "AndroidThunkJava_SetUserName", "(Ljava/lang/String;)V", false);
		FJavaWrapper::CallVoidMethod(Env, FJavaWrapper::GameActivityThis, Method, UsernameFinal);
		Env->DeleteLocalRef(UsernameFinal);
	}
#elif PLATFORM_IOS
	dispatch_async(dispatch_get_main_queue(), ^{
		[CrashlyticsKit setUserName : Username.GetNSString()];
	});
#endif
}

void //UCrashlyticsBlueprintLibrary::WriteLog(const FString& Log)
{
#if PLATFORM_ANDROID && UE_BUILD_SHIPPING
	if (JNIEnv* Env = FAndroidApplication::GetJavaEnv())
	{
		jstring LogFinal = Env->NewStringUTF(TCHAR_TO_UTF8(*Log));
		static jmethodID Method = FJavaWrapper::FindMethod(Env, FJavaWrapper::GameActivityClassID, "AndroidThunkJava_WriteLog", "(Ljava/lang/String;)V", false);
		FJavaWrapper::CallVoidMethod(Env, FJavaWrapper::GameActivityThis, Method, LogFinal);
		Env->DeleteLocalRef(LogFinal);
	}
#elif PLATFORM_IOS
	dispatch_async(dispatch_get_main_queue(), ^{
		CLS_LOG(@"%@", Log.GetNSString());
	});
#endif
}
