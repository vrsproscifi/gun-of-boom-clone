package com.epicgames.ue4;

abstract interface IAdmobCallback
{
  public abstract void onInterstitialClick();
  
  public abstract void onInterstitialClose();
  
  public abstract void onInterstitialShow();
  
  public abstract void onRewardedVideoComplete(String paramString, int paramInt);
  
  public abstract void onRewardedViceoClose();
}
