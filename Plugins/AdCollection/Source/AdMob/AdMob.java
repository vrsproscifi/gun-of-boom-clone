package com.epicgames.ue4;

import android.app.Activity;
import android.content.res.Resources;
import android.os.Build;
import android.os.Build.VERSION;
import android.util.DisplayMetrics;
import android.util.Log;
import android.view.ViewGroup;
import android.view.ViewGroup.MarginLayoutParams;
import android.widget.LinearLayout;
import android.widget.PopupWindow;
import com.google.android.gms.ads.AdListener;
import com.google.android.gms.ads.AdRequest;
import com.google.android.gms.ads.AdRequest.Builder;
import com.google.android.gms.ads.AdSize;
import com.google.android.gms.ads.AdView;
import com.google.android.gms.ads.InterstitialAd;
import com.google.android.gms.ads.MobileAds;
import com.google.android.gms.ads.reward.RewardItem;
import com.google.android.gms.ads.reward.RewardedVideoAd;
import com.google.android.gms.ads.reward.RewardedVideoAdListener;
import java.util.HashMap;
import org.json.JSONException;
import org.json.JSONObject;

	
public class AdMob
{
	public static final String TAG = "AdCollection:AdMob";

	private Activity _activity;

	private String mAppId;
	private String mRewardedUnit;
	private RewardedVideoAdListener mRewardedVideoAdListener;
	private RewardedVideoAd mRewardAd;

	public enum EPlayRewardedVideoStatus
    {
		None,
		NotImpl,
		PlatformNotImpl,

		EnvironmentNotFound ,
		EnvironmentMethodNotFound ,
		AdServciceNotFound ,


		RewardedVideoAdNullPtr,
		RewardedVideoAdSuccessfully 
    }
	
	public void ReloadAds()
	{
		try
		{
			Log.d(TAG, String.format("AdMob:PreloadRewardedVideo | isLoaded: %b", mRewardAd.isLoaded()));

			if (mRewardAd.isLoaded() == false)
			{
				//InitRewardAds();
				mRewardAd.loadAd(mRewardedUnit, new AdRequest.Builder().build());
			}	
		}
		catch(Exception e)
		{
			Log.d(TAG, String.format("AdMob:PreloadRewardedVideo | mRewardAd.loadAd: %s", e.toString()));

		}
	}
	
	public AdMob(Activity activity)
	{
		_activity = activity;
		Log.d(TAG, "Admob:Init");
		mRewardedVideoAdListener = new RewardedVideoAdListener()
		{
			public void onRewardedVideoAdLoaded()
			{
				Log.d(TAG, "AdMob:ReeardedVideo Loaded Success");
			}

			public void onRewardedVideoAdOpened() 
			{
				//ReloadAds();
			}

			public void onRewardedVideoStarted() {}

			public void onRewardedVideoAdClosed()
			{
				Log.d(TAG, "AdMob:Request Load when closed");
				ReloadAds();
				nativePlayRewardedClose();
			}

			public void onRewarded(RewardItem rewardItem)
			{
				Log.d(TAG, "AdMob:ReeardedVideo Rewarded");
				nativePlayRewardedComplete(rewardItem.getType(), rewardItem.getAmount());
			}

			public void onRewardedVideoAdLeftApplication() {}

			public void onRewardedVideoAdFailedToLoad(int i)
			{
				Log.d(TAG, String.format("AdMob:ReeardedVideo Fail Loads | Error: %d", i));
				ReloadAds();
			}

			//@Override
			//public void onRewardedVideoCompleted() 
			//{
			//	//ReloadAds();
			//}
		};
	}
  
	public EPlayRewardedVideoStatus PreloadRewardedVideo()
	{
		Log.d(TAG, "AdMob:PreloadRewardedVideo");

		if (mRewardAd == null) 
		{
			Log.d(TAG, "AdMob:PreloadRewardedVideo | mRewardAd was nullptr !!!");
			return EPlayRewardedVideoStatus.RewardedVideoAdNullPtr;
		}

		//_activity.runOnUiThread(new Runnable()
		//{
		//	public void run()
		//	{		
				ReloadAds();
		//	}
		//});
		return EPlayRewardedVideoStatus.RewardedVideoAdSuccessfully;
	}
	
	public boolean isLoaded()
	{
		return mRewardAd.isLoaded();
	}
	
  
	public void InitAdMob(String AppId, String rewardUnit)
	{
		mAppId = AppId;
		mRewardedUnit = rewardUnit;
		
		//mAppId = "ca-app-pub-3940256099942544~3347511713";
		//mRewardedUnit = "ca-app-pub-3940256099942544/5224354917";

		//Log.d(TAG, "redy init AdMob appid:" + mAppId + " reward:" + mRewardedUnit);
		//MobileAds.initialize(_activity, mAppId);
		
		InitRewardAds();
	}
  
	public void InitRewardAds()
	{
		Log.d(TAG, "AdMob:play rewardedvideo In UI:" + mRewardedUnit);
		mRewardAd = MobileAds.getRewardedVideoAdInstance(_activity);
		mRewardAd.setRewardedVideoAdListener(mRewardedVideoAdListener);
		mRewardAd.loadAd(mRewardedUnit, new AdRequest.Builder().build());
	}
  
	public EPlayRewardedVideoStatus playRewardAds()
	{		

		Log.d(TAG, String.format("AdMob:playRewardAds | mRewardAd was nullptr: %b | isLoaded: ", mRewardAd == null/*, mRewardAd.isLoaded()*/));

		if (mRewardAd == null) 
		{
			Log.d(TAG, "AdMob:playRewardAds | mRewardAd was nullptr !!!");
			return EPlayRewardedVideoStatus.RewardedVideoAdNullPtr;
		}

		//_activity.runOnUiThread(new Runnable()
		//{
		//	public void run()
			{		
				try
				{
					Log.d(TAG, String.format("AdMob:playRewardAds | isLoaded: %b", mRewardAd.isLoaded()));

					//if (mRewardAd.isLoaded())
					{
						mRewardAd.show();	
					}	
					//else
					//{
					//	mRewardAd.loadAd(mRewardedUnit, new AdRequest.Builder().build());
					//}
				}
				catch(Exception e)
				{
					Log.d(TAG, String.format("AdMob:playRewardAds | mRewardAd.show: %s", e.toString()));

				}
			}
		//});
		return EPlayRewardedVideoStatus.RewardedVideoAdSuccessfully;
	}
  
	public void OnPause()
	{
		if (mRewardAd != null) 
		{
			mRewardAd.pause(_activity);
		}
	}

	public void OnResume()
	{
		if (mRewardAd != null) 
		{
			mRewardAd.resume(_activity);
		}
	}

	public void OnDestroy()
	{
		if (mRewardAd != null) 
		{
			mRewardAd.destroy(_activity);
		}
	}

	public native void nativeInterstitialClick();

	public native void nativeInterstitialShow();

	public native void nativeInterstitialClose();

	public native void nativePlayRewardedComplete(String paramString, int paramInt);

	public native void nativePlayRewardedClose();
}
