// Copyright 1998-2018 Epic Games, Inc. All Rights Reserved.

using UnrealBuildTool;
using System.Collections.Generic;

public class ShooterGameTarget : TargetRules
{
    public ShooterGameTarget(TargetInfo Target) : base(Target)
    {
        Type = TargetType.Game;
        bUsesSteam = false;
        bUsesCEF3 = false;

        //=======================================
        //bCompileRecast = false;
        //bCompileSpeedTree = false;
        //bCompileAPEX = false;
        //bIncludeADO = false;
        //
        //
        ////=======================================
        //bCompileSimplygon = false;
        //bCompileSimplygonSSF = false;
        bCompileWithPluginSupport = false;
        bCompileCEF3 = false;

        //bCompileNvCloth = false;
        //bCompilePhysX = false;
        //
        ////=======================================

        //bCompileFreeType = true;
        //
        if (Platform == UnrealTargetPlatform.Android)
        {
            //bWithServerCode = false;
            //bUseXGEController = false;
            //bCompileLeanAndMeanUE = true;
            //bCompileForSize = true;
            bDisableDebugInfo = true;
            //bStripSymbolsOnIOS = true;
            //bHideSymbolsByDefault = true;
        }

        ExtraModuleNames.Add("ShooterGame");
		ExtraModuleNames.Add("ShooterGameFrameWork");
        LinkType = TargetLinkType.Monolithic;
    }
}
