﻿// Copyright 1998-2018 Epic Games, Inc. All Rights Reserved.

#include "ShooterGame.h"
#include "ShooterGameMode.h"
#include "Player/ShooterSpectatorPawn.h"
#include "Player/ShooterDemoSpectator.h"
#include "Bots/ShooterAIController.h"
#include "ShooterHUD.h"
#include "Player/ShooterPlayerController.h"
#include "GameState/ShooterGameState.h"

#include "GameState/ShooterPlayerState.h"
#include "ShooterGameSession.h"
#include "Player/ShooterCharacter.h"
#include "GamePlay/ShooterTeamStart.h"
#include "ShooterGameSingleton.h"
#include "FightTestData.h"
#include "Game/Components/NodeComponent.h"
#include "Bots/BotSettingsData.h"
#include "ShooterGameAchievements.h"
#include "Extensions/GameSingletonExtensions.h"
#include "Item/Weapon/WeaponItemEntity.h"
#include "Item/Grenade/GrenadeItemEntity.h"

#include "GameState/TeamInfo.h"
#include "Inventory/ShooterArmour.h"
#include "ArsenalComponent.h"
#include "UObjectExtensions.h"

AShooterGameMode::AShooterGameMode(const FObjectInitializer& ObjectInitializer)
	: Super(ObjectInitializer)
	, bFirstBloodOccurred(false)
	, CheckBotAmmunitionInterval(10)
	, MatchTargetLevel(ENodeSessionMatchTargetLevel::HighLevel)
#if WITH_EDITOR
	, DeveloperTeamNum(0)
#endif
{
	bIsMultiPlayerGameMode = true;
	static ConstructorHelpers::FClassFinder<APawn> PlayerPawnOb(TEXT("/Game/Blueprints/Pawns/PlayerPawn"));
	DefaultPawnClass = PlayerPawnOb.Class;
	
	static ConstructorHelpers::FClassFinder<APawn> BotPawnOb(TEXT("/Game/Blueprints/Pawns/BotPawn"));
	BotPawnClass = BotPawnOb.Class;

	HUDClass = AShooterHUD::StaticClass();
	PlayerControllerClass = AShooterPlayerController::StaticClass();
	PlayerStateClass = AShooterPlayerState::StaticClass();
	SpectatorClass = AShooterSpectatorPawn::StaticClass();
	GameStateClass = AShooterGameState::StaticClass();
	ReplaySpectatorPlayerControllerClass = AShooterDemoSpectator::StaticClass();

	MinRespawnDelay = 5.0f;

	bUseSeamlessTravel = true;	
	bStartPlayersAsSpectators = false;
}


FMatchEndResult AShooterGameMode::GetMatchEndResult() const
{
	//=============================================
	auto result = Super::GetMatchEndResult();

	//=============================================
	const auto PlayerStates = GetPlayerStates<AShooterPlayerState>(true);

	//=============================================
	for (const auto& It : PlayerStates)
	{
		//---------------------------------------
		if (It->bIsABot)
		{
			FMatchMemberBotResultModel b;

			b.TeamId = It->GetTeamId();

			if (const auto team = It->GetTeam())
			{
				UE_LOG(LogGameMode, Error, TEXT("AShooterGameMode::GetMatchEndResult >> [Bot : %s][Team was %s / %s]"), *It->GetPlayerName(), *b.TeamId.ToString(), *team->GetTeamId().ToString());
			}
			else
			{
				UE_LOG(LogGameMode, Error, TEXT("AShooterGameMode::GetMatchEndResult >> [Bot : %s][Team was nullptr]"), *It->GetPlayerName());
			}


			b.PlayerName = It->GetPlayerName();

			b.Kills = It->GetAchievementValueById(EShooterGameAchievements::Kills, 10);
			b.Deads = It->GetAchievementValueById(EShooterGameAchievements::Deads, 5);
			b.Score = It->GetAchievementValueById(EShooterGameAchievements::Score, 200);

			result.Bots.Add(b);
		}
	}

	//=============================================
	return result;
}

void AShooterGameMode::OnMatchEnded()
{
#if !WITH_EDITOR
	for (FConstControllerIterator It = GetWorld()->GetControllerIterator(); It; ++It)
	{
		AShooterPlayerController* PlayerController = GetValidObjectAs<AShooterPlayerController>(*It);
		if (PlayerController != nullptr)
		{
			PlayerController->PlayerTravelToLobby();
		}
	}
#endif
	GEngine->Exec(GetWorld(), TEXT("EXIT"));
}

FString AShooterGameMode::GetBotsCountOptionName()
{
	return FString(TEXT("Bots"));
}

void AShooterGameMode::InitGame(const FString& MapName, const FString& Options, FString& ErrorMessage)
{
	//const int32 BotsCountOptionValue = UGameplayStatics::GetIntOption(Options, GetBotsCountOptionName(), 0);
	//SetAllowBots(BotsCountOptionValue > 0 ? true : false, BotsCountOptionValue);	
	Super::InitGame(MapName, Options, ErrorMessage);

	bPauseable = false;
}

void AShooterGameMode::InitGameState()
{
	Super::InitGameState();

	auto MySingleton = UShooterGameSingleton::Get();
	ShooterGameState = GetValidObjectAs<AShooterGameState>(GameState);
	if (WITH_EDITOR && ShooterGameState && MySingleton)
	{
		auto MyNode = ShooterGameState->GetNodeComponent();
		auto TestDataArray = MySingleton->GetDataAssets<UFightTestData>();

		if (MyNode && TestDataArray.Num())
		{
			for (auto TestData : TestDataArray)
			{
				if (TestData && this->IsA(TestData->TargetGameMode))
				{
					FNodeSessionMatch FakeOptions;

					FakeOptions.MatchId = FGuid::NewGuid();
					FakeOptions.NodeId = FGuid::NewGuid();
					
					for (SIZE_T i = 0; i < TestData->NumTeams; ++i)
					{
						FakeOptions.TeamList.Add(FGuid::NewGuid());
					}

					FakeOptions.Options.NumberOfBots = TestData->NumBots;
					FakeOptions.Options.RoundTimeInSeconds = TestData->MatchTime;
					FakeOptions.Options.AdditionalFlag = TestData->GoalScore;

					MyNode->OnSimulatedInformation(FakeOptions);
					break;
				}
			}
		}
	}
}

void AShooterGameMode::OnInitializeMatchInformation()
{
	if (ShooterGameState)
	{
		const auto MatchInformation = ShooterGameState->GetMatchInformation();
		if(MatchInformation)
		{
			//========================================================
			MatchTargetLevel = MatchInformation->TargetLevel;

			//========================================================
			const auto Options = MatchInformation->Options;
			RoundTime = Options.RoundTimeInSeconds;

			//========================================================
			UE_LOG(LogGameMode, Display, TEXT("OnInitializeMatchInformation >> Round Time: %s, Goal Score: %.1f, Num Bots: %d, MatchTargetLevel: %d[%s]")
				, *FTimespan::FromSeconds(RoundTime).ToString(TEXT("%m:%s"))
				, ShooterGameState->GetGoalScore()
				, Options.NumberOfBots
				, static_cast<int32>(MatchTargetLevel), *GetEnumValueAsString("ENodeSessionMatchTargetLevel", MatchTargetLevel));
		}
	}
}

/** Returns game session class to use */
TSubclassOf<AGameSession> AShooterGameMode::GetGameSessionClass() const
{
	return AShooterGameSession::StaticClass();
}

void AShooterGameMode::PreInitializeComponents()
{
	Super::PreInitializeComponents();
}

void AShooterGameMode::DefaultTimer()
{
	// don't update timers for Play In Editor mode, it's not real match
	//if (GetWorld()->IsPlayInEditor())
	//{
	//	// start match if necessary.
	//	if (GetMatchState() == MatchState::WaitingToStart)
	//	{
	//		StartMatch();
	//	}
	//	return;
	//}

	AShooterGameState* const MyGameState = GetValidObjectAs<AShooterGameState>(GameState);
	if (MyGameState && MyGameState->RemainingTime > 0)
	{
		UE_LOG(LogGameMode, Log, TEXT("AShooterGameMode::DefaultTimer | RemainingTime: %d | MatchState: %s"), MyGameState->RemainingTime, *GetMatchState().ToString());

		MyGameState->RemainingTime--;
		
		if (MyGameState->RemainingTime <= 0)
		{
			if (GetMatchState() == MatchState::InProgress)
			{
				EndMatch();
			}
			else if (GetMatchState() == MatchState::WaitingToStart)
			{
				StartMatch();
			}
		}

		if (GetMatchState() == MatchState::InProgress)
		{
			CheckBotCount();

			//	Каждые 15 секунд
			if (GetRemainingTime() % CheckBotAmmunitionInterval == 0 /*&& GetTimeLimit() - GetRemainingTime() > 60*/)
			{
				CheckBotAmmunition();
			}
		}
	}
	else
	{
		UE_LOG(LogGameMode, Log, TEXT("AShooterGameMode::DefaultTimer | MyGameState was nullptr"));
	}
}

bool AShooterGameMode::IsAllowJoinInProgress() const
{
	return GetRemainingTime() >= GetHalfTimeLimit();
}

int32 AShooterGameMode::GetRemainingTime() const
{
	AShooterGameState* const MyGameState = GetValidObjectAs<AShooterGameState>(GameState);
	if (MyGameState)
	{
		return MyGameState->GetRemainingTime();
	}

	return Super::GetRemainingTime();
}

int32 AShooterGameMode::GetTimeLimit() const
{
	AShooterGameState* const MyGameState = GetValidObjectAs<AShooterGameState>(GameState);
	if (MyGameState)
	{
		return MyGameState->GetTimeLimit();
	}

	return Super::GetTimeLimit();
}

void AShooterGameMode::HandleMatchIsWaitingToStart()
{
	Super::HandleMatchIsWaitingToStart();

	UE_LOG(LogGameMode, Log, TEXT("AShooterGameMode::HandleMatchIsWaitingToStart | bDelayedStart: %d"), bDelayedStart);

	if (bDelayedStart)
	{
		// start warmup if needed
		AShooterGameState* const MyGameState = GetValidObjectAs<AShooterGameState>(GameState);
		if (MyGameState && MyGameState->GetRemainingTime() == 0)
		{
			MyGameState->SetTimeLimit(WarmupTime);
		}
	}
}

void AShooterGameMode::HandleMatchHasStarted()
{
	Super::HandleMatchHasStarted();

	UE_LOG(LogGameMode, Log, TEXT("AShooterGameMode::HandleMatchHasStarted | RoundTime: %d"), RoundTime);

	AShooterGameState* const MyGameState = GetValidObjectAs<AShooterGameState>(GameState);
	MyGameState->SetTimeLimit(RoundTime);
	//StartBots();	

	// notify players
	for (FConstControllerIterator It = GetWorld()->GetControllerIterator(); It; ++It)
	{
		AShooterPlayerController* PC = GetValidObjectAs<AShooterPlayerController>(*It);
		if (PC)
		{
			PC->ClientGameStarted();
		}
	}
}

void AShooterGameMode::HandleMatchHasEnded()
{
	Super::HandleMatchHasEnded();

	UE_LOG(LogGameMode, Log, TEXT("AShooterGameMode::HandleMatchHasEnded"));


	DetermineMatchWinner();

	for (FConstControllerIterator It = GetWorld()->GetControllerIterator(); It; ++It)
	{
		//===========================================================
		if (It->IsValid() && It->Get())
		{
			//------------------------------------------------
			AShooterPlayerState* PlayerState = GetValidObjectAs<AShooterPlayerState>((*It)->PlayerState);

			//------------------------------------------------
			const bool bIsWinner = IsWinner(PlayerState);

			//------------------------------------------------
			(*It)->GameHasEnded(nullptr, bIsWinner);
		}
	}

	// lock all pawns
	// pawns are not marked as keep for seamless travel, so we will create new pawns on the next match rather than
	// turning these back on.
	for (FConstPawnIterator It = GetWorld()->GetPawnIterator(); It; ++It)
	{
		(*It)->TurnOff();
	}

	ShooterGameState->SetTimeLimit(TimeBetweenMatches);
}

void AShooterGameMode::DetermineMatchWinner()
{
	// nothing to do here
}

void AShooterGameMode::PreLogin(const FString& Options, const FString& Address, const FUniqueNetIdRepl& UniqueId, FString& ErrorMessage)
{
	AShooterGameState* const MyGameState = GetValidObjectAs<AShooterGameState>(GameState);
	const bool bMatchIsOver = MyGameState && MyGameState->HasMatchEnded();
	if( bMatchIsOver )
	{
		ErrorMessage = TEXT("Match is over!");
	}
	else
	{
		// GameSession can be NULL if the match is over
		Super::PreLogin(Options, Address, UniqueId, ErrorMessage);
	}
}


bool AShooterGameMode::ChangeTeam(AController* Player, uint8 NewTeam, bool bBroadcast)
{
	// By default, we don't do anything.
	return true;
}

void AShooterGameMode::PostLogin(APlayerController* NewPlayer)
{
	static const uint8 DEFAULT_TEAMID_ID_EDITOR = 0;
	auto TargetPC = GetValidObjectAs<AShooterPlayerController>(NewPlayer);

#if 1 //UE_SERVER && !WITH_EDITOR
	Super::PostLogin(NewPlayer);

	if (auto LocalPlayer = GetValidObjectAs<UNetConnection>(NewPlayer->Player))
	{
		const auto TargetMember = FindMatchMember(LocalPlayer->PlayerId->ToString());

		if (TargetMember && TargetPC)
		{
			if (auto TargetState = GetValidObjectAs<AShooterPlayerState>(TargetPC->PlayerState))
			{
				TargetState->PlayerWinRate = TargetMember->WinRate;
				TargetState->PlayerTargetWeaponLevel = TargetMember->TargetWeaponLevel;
				TargetState->PlayerTargetArmourLevel = TargetMember->TargetArmourLevel;
				TargetState->PlayerTargetWeaponUpgrade = TargetMember->TargetWeaponUpgrade;

				//=====================================================================
				if (GetNetMode() == NM_Standalone || GEngine->IsEditor())
				{
					ChangeTeam(TargetPC, DEFAULT_TEAMID_ID_EDITOR, false);
				}
				else
				{
					MovePlayerToTeam(TargetState, GetTeamIndexById(TargetMember->TeamId));
					//TargetState->HandleTeamChanged(TargetPC);
				}

				//=====================================================================
				UE_LOG(LogShooter, Log, TEXT("Member: %s connected with team: %s[%d] | WinRate: %d | WeaponLevel: %d | ArmourLevel: %d | WeaponUpgrade: %d")
					, *TargetState->GetPlayerName()
					, *TargetMember->TeamId.ToString()
					, TargetState->GetTeamNum()
					, TargetState->PlayerWinRate
					, TargetState->PlayerTargetWeaponLevel
					, TargetState->PlayerTargetArmourLevel
					, TargetState->PlayerTargetWeaponUpgrade
				);
			}
		}
	}
#else		
	Super::PostLogin(NewPlayer);
#endif

#if WITH_EDITOR
		ChangeTeam(NewPlayer, DeveloperTeamNum, false);
#endif

	// update spectator location for client
	AShooterPlayerController* NewPC = GetValidObjectAs<AShooterPlayerController>(NewPlayer);
	if (NewPC && NewPC->GetPawn() == nullptr)
	{
		NewPC->ClientSetSpectatorCamera(NewPC->GetSpawnLocation(), NewPC->GetControlRotation());
	}

	// notify new player if match is already in progress
	if (NewPC && IsMatchInProgress())
	{
		NewPC->ClientGameStarted();
		NewPC->ClientStartOnlineGame();
	}


	if (IsMatchInProgress())
	{
		CheckBotCount();
	}
}

void AShooterGameMode::CheckBotAmmunition()
{

}



void AShooterGameMode::Killed(AController* Killer, AController* KilledPlayer, APawn* KilledPawn, const int32& InWeaponId)
{
	//=================================================
	if (GetMatchState() == MatchState::WaitingToStart)
	{
		return;
	}

	//=================================================
	AShooterPlayerState* KillerPlayerState = GetPlayerState<AShooterPlayerState>(Killer);
	AShooterPlayerState* VictimPlayerState = GetPlayerState<AShooterPlayerState>(KilledPlayer);

	//=================================================
	const auto PlayerArray = GetPlayerStates<AShooterPlayerState>(true, true);


	//=================================================
	//	Статистика убийств по оружию
	if(KillerPlayerState)
	{
		const auto weapon = FGameSingletonExtensions::FindItemById <UWeaponItemEntity>(InWeaponId);
		if (weapon)
		{
			const auto KillAchievementId = weapon->GetWeaponProperty().KillAchievementId;
			if (KillAchievementId > 0)
			{
				KillerPlayerState->GivePlayerAchievement(KillAchievementId);
			}
		}
		else if(const auto grenade = FGameSingletonExtensions::FindItemById <UGrenadeItemEntity>(InWeaponId))
		{
			KillerPlayerState->GivePlayerAchievement(EShooterGameAchievements::KillsByGrenade);
		}
	}

	//=================================================
	if (KillerPlayerState && VictimPlayerState && KillerPlayerState != VictimPlayerState)
	{
		//----------------------------------
		const auto gs = GetValidObject(GetGameState<AShooterGameState>());
		
		//----------------------------------
		int32 GiveScore = KillScore;

		//----------------------------------
		if (VictimPlayerState)
		{
			GiveScore = FMath::CeilToInt(KillScore * VictimPlayerState->GetCoefficient());
		}

		//----------------------------------
		const auto MyPawn = GetValidObjectAs<AShooterCharacter>(KilledPawn);
		const auto AssistScore = FMath::CeilToInt(float(GiveScore) / 3.0f);
		const auto MyKillers = VictimPlayerState->GetDamageData();

		//----------------------------------
		FPlayerDamageData MaxDamagedData = FPlayerDamageData::GetMaxDamagedData(MyKillers);

		//----------------------------------
		for (const auto& AssistState : PlayerArray)
		{
			//=================================================
			if (MyKillers.Contains(AssistState->GetUniqueID()))
			{
				if (MaxDamagedData.Owner == AssistState && AssistState != KillerPlayerState)
				{
					KillerPlayerState->GivePlayerAchievement(EShooterGameAchievements::CriticalAssist);
				}

				if (MyPawn)
				{
					//-----------------------------------------
					const auto DamageData = MyKillers.FindChecked(AssistState->GetUniqueID());
					const auto maxHealth = /*MyPawn->Armour.MaxAmount +*/ MyPawn->Health.Y;
					const auto percent = DamageData.Damage / maxHealth;
					const auto ScaledScoreSource = float(GiveScore) * percent;
						
					//-----------------------------------------
					int32 ScaledScore = FMath::CeilToInt(ScaledScoreSource);
					if(gs->HasSameTeam(VictimPlayerState, AssistState))
					{
						ScaledScore *= -1;
					}

					//-----------------------------------------
					if (AssistState->GetUniqueID() == KillerPlayerState->GetUniqueID())
					{
						AssistState->ScoreKill(VictimPlayerState, ScaledScore);
					}
					else
					{
						AssistState->ScoreAssist(ScaledScore);
					}
				}
				else
				{
					if (AssistState->GetUniqueID() == KillerPlayerState->GetUniqueID())
					{
						AssistState->ScoreKill(VictimPlayerState, GiveScore);
					}
					else
					{
						AssistState->ScoreAssist(AssistScore);
					}
				}
			}
		}

		//----------------------------------
		//	Добавляем информаю об убийцах в жертву
		VictimPlayerState->AddRevengeKills(KillerPlayerState->GetUniqueID());

		//----------------------------------
		//	Полачаем информацию о количествах убиств данным игроком
		const auto RevengeKills = KillerPlayerState->GetRevengeKills(VictimPlayerState->GetUniqueID());
		if(RevengeKills >= 2)
		{
			KillerPlayerState->GivePlayerAchievement(EShooterGameAchievements::Revenge);
			KillerPlayerState->ResetRevengeKills(VictimPlayerState->GetUniqueID());
		}

		//----------------------------------
		//	Полачаем информацию о количествах убиств данного игрока
		const auto DominationKills = KillerPlayerState->AddDominationKills(VictimPlayerState->GetUniqueID());
		if(DominationKills >= 3 && DominationKills % 3 == 0)
		{
			KillerPlayerState->GivePlayerAchievement(EShooterGameAchievements::Domination);
		}

		//----------------------------------
		const auto FiveSecondsKills = KillerPlayerState->AddFiveSecondsKills();
		if(FiveSecondsKills >= 2)
		{
			if (FiveSecondsKills == 2) KillerPlayerState->GivePlayerAchievement(EShooterGameAchievements::DoubleKill);
			else if (FiveSecondsKills == 3) KillerPlayerState->GivePlayerAchievement(EShooterGameAchievements::TripleKill);
			else if (FiveSecondsKills == 4) KillerPlayerState->GivePlayerAchievement(EShooterGameAchievements::Quadrakill);
			else if (FiveSecondsKills == 5) KillerPlayerState->GivePlayerAchievement(EShooterGameAchievements::Pentakill);
		}

		//----------------------------------
		const auto CombobreakerKills = VictimPlayerState->GetKillsOneLife();
		if (CombobreakerKills >= 3)
		{
			//=====================================
			//	Если сломали серию убийств
			if (CombobreakerKills == 3 || CombobreakerKills == 7 || CombobreakerKills == 11 || CombobreakerKills == 15)
			{
				KillerPlayerState->GivePlayerAchievement(EShooterGameAchievements::Combobreaker);
			}
		}
		
		//KillerPlayerState->GetPlayerId()

		//----------------------------------
		const auto series = KillerPlayerState->GetKillsOneLife();
		if (series >= 4)
		{
			//=====================================
			if (series == 4) KillerPlayerState->GivePlayerAchievement(EShooterGameAchievements::KillingSpree);
			else if (series == 8) KillerPlayerState->GivePlayerAchievement(EShooterGameAchievements::Unstoppable);
			else if (series == 12) KillerPlayerState->GivePlayerAchievement(EShooterGameAchievements::Massacre);
			else if (series >= 16) KillerPlayerState->GivePlayerAchievement(EShooterGameAchievements::Legendary);
		}
	}

	//=================================================
	if (VictimPlayerState)
	{
		VictimPlayerState->ScoreDeath(KillerPlayerState, DeathScore);

		FKillerInfo KillerInfo;

		if (KillerPlayerState && KillerPlayerState->IsValidLowLevel())
		{
			//--------------------------------------------------
			auto KillerPlayerInventoryComponent = KillerPlayerState->GetInventoryComponent();

			//--------------------------------------------------
			if (auto KillerChar = KillerPlayerState->GetBaseCharacter<AShooterCharacter>())
			{
				KillerInfo.Health = KillerChar->Health;				
				KillerInfo.Armour = FVector2D(KillerChar->GetCurrentArmour(), KillerChar->GetTotalArmour());
			}

			if (KillerPlayerInventoryComponent)
			{
				auto FindModelIdLambda = [&, KillerPlayerInventoryComponent = KillerPlayerInventoryComponent](const EGameItemType& InFindType) -> FKillerItemInfo
				{
					if (KillerPlayerInventoryComponent)
					{
						if (auto FoundItem = KillerPlayerInventoryComponent->GetPlayerItemInProfileBySlot(0, InFindType))
						{
							return FKillerItemInfo(FoundItem->GetModelId(), FoundItem->GetLevel());
						}
					}
					return FKillerItemInfo();
				};

				if (auto KilledByWeapon = KillerPlayerInventoryComponent->FindItem(InWeaponId))
				{
					KillerInfo.Weapon = FKillerItemInfo(KilledByWeapon->GetModelId(), KilledByWeapon->GetLevel());
				}

				KillerInfo.Head = FindModelIdLambda(EGameItemType::Head);
				KillerInfo.Torso = FindModelIdLambda(EGameItemType::Torso);
				KillerInfo.Legs = FindModelIdLambda(EGameItemType::Legs);
			}
		}

		for (const auto& PS : PlayerArray)
		{
			PS->OnDeathMessage(VictimPlayerState, KillerPlayerState, KillerInfo);
		}
	}

	//=================================================
	//	Первая кровь
	if (VictimPlayerState && KillerPlayerState && !bFirstBloodOccurred)
	{
		KillerPlayerState->GivePlayerAchievement(EShooterGameAchievements::FirstBlood);
		bFirstBloodOccurred = true;
	}
}

bool AShooterGameMode::AllowRemovingBot(AShooterAIController* B)
{
	//=================================================
	auto Bot = GetValidObject(B);

	//=================================================
	if(Bot == nullptr)
	{
		return false;
	}

	//=================================================
	auto BotPlayerState = GetValidObjectAs<AShooterPlayerState>(Bot->PlayerState);

	//=================================================
	if (BotPlayerState == nullptr)
	{
		return false;
	}

	//=================================================
	if (NumBots > 1)
	{
		bool bHighScore = true;
		for (APlayerState* OtherPS : GameState->PlayerArray)
		{
			if (OtherPS != BotPlayerState && OtherPS->Score >= BotPlayerState->Score)
			{
				bHighScore = false;
				break;
			}
		}

		if (bHighScore)
		{
			return false;
		}
	}

	//=================================================
	return B->GetPawn() == nullptr || B->GetEnemy() == nullptr/* || B->LostContact(5.0f)*/;
}

void AShooterGameMode::CheckBotCount()
{
	if (NumPlayers + NumBots > GetMatchOptions().GetNumberOfBots())
	{
		for (FConstControllerIterator It = GetWorld()->GetControllerIterator(); It; ++It)
		{
			if (It && It->IsValid())
			{
				if(auto Bot = GetValidObjectAs<AShooterAIController>(It->Get()))
				{
					if(AllowRemovingBot(Bot))
					{
						Bot->Destroy();
						break;
					}
				}
			}
		}
	}
	else while (NumPlayers + NumBots < GetMatchOptions().GetNumberOfBots())
	{
		auto AIC = AddBot();
		if (AIC)
		{
			RestartPlayer(AIC);
		}
	}
}

bool AShooterGameMode::IsBot(AShooterPlayerState* PS)
{
	return PS && PS->bIsABot;
}

bool AShooterGameMode::IsPlayer(AShooterPlayerState* PS)
{
	return PS && PS->bIsABot == false;
}

TArray<FMatchLevelDamageScale> AShooterGameMode::MatchLevelDamageScales =
{
	FMatchLevelDamageScale(1.00f, 1.00f),	//	По умолчанию
	FMatchLevelDamageScale(0.25f, 1.25f),	//	Нулевой уровень
	FMatchLevelDamageScale(0.50f, 1.25f),	//	Первый уровень
	FMatchLevelDamageScale(1.00f, 1.00f),	//	Остальные игроки
};


float AShooterGameMode::ModifyDamage(float Damage, AActor* DamagedActor, struct FDamageEvent const& DamageEvent, AController* EventInstigator, AActor* DamageCauser) const
{
	float ActualDamage = Damage;

	if (HasMatchStarted() && (!IsMatchInProgress()))
	{
		ActualDamage = 0.0f;
	}
	else if (AShooterCharacter* DamagedPawn = GetValidObjectAs<AShooterCharacter>(DamagedActor))
	{
		if (DamagedPawn->IsSpawnProtected())
		{
			ActualDamage = 0.0f;
		}
		else if (EventInstigator)
		{
			//==============================================================
			AShooterPlayerState* DamagedPlayerState = GetPlayerState<AShooterPlayerState>(DamagedPawn);
			AShooterPlayerState* InstigatorPlayerState = GetPlayerState<AShooterPlayerState>(EventInstigator);

			//==============================================================
			// disable friendly fire
			if (!CanDealDamage(InstigatorPlayerState, DamagedPlayerState))
			{
				ActualDamage = 0.0f;
			}

			//==============================================================
			// scale self instigated damage
			if (InstigatorPlayerState == DamagedPlayerState)
			{
				ActualDamage *= DamageSelfScale;
			}

			//==============================================================
			if (MatchTargetLevel > ENodeSessionMatchTargetLevel::None && MatchTargetLevel < ENodeSessionMatchTargetLevel::HighLevel)
			{		
				//==============================================================
				//	Если урон получил бот и урон нанес игрок
				if (IsBot(DamagedPlayerState) && IsPlayer(InstigatorPlayerState))
				{
					ActualDamage *= MatchLevelDamageScales[MatchTargetLevel].Outgoing;
				}

				//==============================================================
				//	Если урон получил игрок и урон нанес бот
				if (IsPlayer(DamagedPlayerState) && IsBot(InstigatorPlayerState))
				{
					ActualDamage *= MatchLevelDamageScales[MatchTargetLevel].Incoming;
				}
			}
		}
	}
	
	return ActualDamage;
}

bool AShooterGameMode::CanDealDamage(class AShooterPlayerState* DamageInstigator, class AShooterPlayerState* DamagedPlayer) const
{
	return true;
}

bool AShooterGameMode::AllowCheats(APlayerController* P)
{
	return true;
}

bool AShooterGameMode::ShouldSpawnAtStartSpot(AController* Player)
{
	return false;
}

UClass* AShooterGameMode::GetDefaultPawnClassForController_Implementation(AController* InController)
{
	if (InController->IsA<AShooterAIController>())
	{
		return BotPawnClass;
	}

	auto PawnClass = Super::GetDefaultPawnClassForController_Implementation(InController);
	if (PawnClass == nullptr)
	{
		UE_LOG(LogGameMode, Error, TEXT("AShooterGameMode::GetDefaultPawnClassForController >> PawnClass was nullptr"));
	}

	return PawnClass;
}

AActor* AShooterGameMode::ChoosePlayerStart_Implementation(AController* Player)
{
	//==========================================
	TArray<APlayerStart*> PreferredSpawns;
	TArray<APlayerStart*> FallbackSpawns;

	//==========================================
	APlayerStart* BestStart = NULL;

	//==========================================
	for (TActorIterator<APlayerStart> It(GetWorld()); It; ++It)
	{
		APlayerStart* TestSpawn = *It;
		if (TestSpawn->IsA<APlayerStartPIE>())
		{
			// Always prefer the first "Play from Here" PlayerStart, if we find one while in PIE mode
			BestStart = TestSpawn;
			break;
		}
		else
		{
			if (IsSpawnpointAllowed(TestSpawn, Player))
			{
				if (IsSpawnpointPreferred(TestSpawn, Player))
				{
					PreferredSpawns.Add(TestSpawn);
				}
				else
				{
					FallbackSpawns.Add(TestSpawn);
				}
			}
		}
	}

	//==========================================
	if (BestStart == nullptr)
	{
		if (PreferredSpawns.Num() > 0)
		{
			BestStart = PreferredSpawns[FMath::RandHelper(PreferredSpawns.Num())];
		}
		else if (FallbackSpawns.Num() > 0)
		{
			BestStart = FallbackSpawns[FMath::RandHelper(FallbackSpawns.Num())];
		}
	}

	//==========================================
	if (BestStart == nullptr)
	{
		auto result = Super::ChoosePlayerStart_Implementation(Player);
		UE_LOG(LogGameMode, Error, TEXT("AShooterGameMode::ChoosePlayerStart >> BestStart was nullptr, try use super::ChoosePlayerStart: %d "), result != nullptr);
		return result;
	}
	return BestStart;
}

bool AShooterGameMode::IsSpawnpointAllowed(APlayerStart* SpawnPoint, AController* Player) const
{
	AShooterTeamStart* ShooterSpawnPoint = GetValidObjectAs<AShooterTeamStart>(SpawnPoint);
	if (ShooterSpawnPoint)
	{
		AShooterAIController* AIController = GetValidObjectAs<AShooterAIController>(Player);
		if (ShooterSpawnPoint->bNotForBots && AIController)
		{
			return false;
		}

		if (ShooterSpawnPoint->bNotForPlayers && AIController == NULL)
		{
			return false;
		}
		return true;
	}

	return false;
}

bool AShooterGameMode::IsSpawnpointPreferred(APlayerStart* SpawnPoint, AController* Player) const
{
	ACharacter* MyPawn = GetValidObjectAs<ACharacter>((*DefaultPawnClass)->GetDefaultObject<ACharacter>());	
	AShooterAIController* AIController = GetValidObjectAs<AShooterAIController>(Player);
	if( AIController != nullptr )
	{
		MyPawn = GetValidObjectAs<ACharacter>(BotPawnClass->GetDefaultObject<ACharacter>());
	}
	
	if (MyPawn)
	{
		const FVector SpawnLocation = SpawnPoint->GetActorLocation();
		for (FConstPawnIterator It = GetWorld()->GetPawnIterator(); It; ++It)
		{
			ACharacter* OtherPawn = GetValidObjectAs<ACharacter>(It->Get());
			if (OtherPawn && OtherPawn != MyPawn)
			{
				const float CombinedHeight = (MyPawn->GetCapsuleComponent()->GetScaledCapsuleHalfHeight() + OtherPawn->GetCapsuleComponent()->GetScaledCapsuleHalfHeight()) * 2.0f;
				const float CombinedRadius = MyPawn->GetCapsuleComponent()->GetScaledCapsuleRadius() + OtherPawn->GetCapsuleComponent()->GetScaledCapsuleRadius();
				const FVector OtherLocation = OtherPawn->GetActorLocation();

				// check if player start overlaps this pawn
				if (FMath::Abs(SpawnLocation.Z - OtherLocation.Z) < CombinedHeight && (SpawnLocation - OtherLocation).Size2D() < CombinedRadius)
				{
					return false;
				}
			}
		}
	}
	else
	{
		return false;
	}
	
	return true;
}

AShooterAIController* AShooterGameMode::AddBot(const uint8& TeamNum)
{
	FActorSpawnParameters SpawnInfo;
	SpawnInfo.Instigator = nullptr;
	SpawnInfo.OverrideLevel = nullptr;
	SpawnInfo.SpawnCollisionHandlingOverride = ESpawnActorCollisionHandlingMethod::AlwaysSpawn;

	AShooterAIController* AIC = GetWorld()->SpawnActor<AShooterAIController>(SpawnInfo);
	if (AIC)
	{
		AIC->PlayerState->bIsABot = true;
		InitBot(AIC, TeamNum);
		NumBots++;
	}
	return AIC;
}


void AShooterGameMode::InitBot(AShooterAIController* AIController, const uint8& InTeamNum)
{	
	if (AIController)
	{
		if (auto ShooterPS = GetValidObjectAs<AShooterPlayerState>(AIController->PlayerState))
		{
			TArray<FString> UsableNames;			
			for (FConstControllerIterator It = GetWorld()->GetControllerIterator(); It; ++It)
			{
				auto ShooterItPS = GetValidObjectAs<AShooterPlayerState>(It->Get()->PlayerState);
				if (ShooterItPS && ShooterItPS->bIsABot)
				{
					UsableNames.Add(ShooterItPS->GetPlayerName());
				}
			}

			bool bIsFoundFreeName = false;
			auto BotsArray = UShooterGameSingleton::Get()->GetDataAssets<UBotSettingsData>();
			if (auto TargetBotData = BotsArray[BotsArray.Num() > 1 ? FMath::RandRange(0, BotsArray.Num() - 1) : 0])
			{
_rndName:
				SIZE_T _count = 0, _rnd = FMath::RandRange(0, TargetBotData->BotNames.Num() - 1);
				for (auto TargetName : TargetBotData->BotNames)
				{
					if (UsableNames.Contains(TargetName) == false && _count == _rnd)
					{
						bIsFoundFreeName = true;

						ShooterPS->SetPlayerName(TargetName);
						ShooterPS->InitTestData(TargetBotData->BotData);

						break;
					}

					++_count;
				}

				if (!bIsFoundFreeName)
				{
					goto _rndName;
				}
			}			
		}		
	}
}


void AShooterGameMode::StartBots()
{
	// checking number of existing human player.
	UWorld* World = GetWorld();
	for (FConstControllerIterator It = World->GetControllerIterator(); It; ++It)
	{		
		AShooterAIController* AIC = GetValidObjectAs<AShooterAIController>(*It);
		if (AIC)
		{
			RestartPlayer(AIC);
		}
	}	
}

void AShooterGameMode::Logout(AController* Exiting)
{
	//=========================================
	if (GetValidObjectAs<AShooterAIController>(Exiting))
	{
		NumBots--;
	}

	//=========================================
	Super::Logout(Exiting);

	//=========================================
	//if (NumPlayers == 0)
	//{
	//	if (GetMatchState() == MatchState::InProgress)
	//	{
	//		EndMatch();
	//	}
	//	else if (GetMatchState() == MatchState::WaitingPostMatch)
	//	{
	//		//EndGame();
	//	}
	//}
}

