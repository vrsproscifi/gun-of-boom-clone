// Copyright 1998-2018 Epic Games, Inc. All Rights Reserved.

#pragma once

//=========================================
#include "Game/OnlineGameMode.h"

//=========================================
#include "OnlineIdentityInterface.h"

//=========================================
#include "Game/Components/NodeComponent/NodeSessionMatchTargetLevel.h"

//=========================================
#include "ShooterGameMode.generated.h"

//=========================================
class AShooterGameState;

//=========================================
class AShooterAIController;
class AShooterPlayerState;
class AShooterPickup;
class FUniqueNetId;

//=========================================
USTRUCT(BlueprintType)
struct FMatchLevelDamageScale
{
	GENERATED_USTRUCT_BODY()

	UPROPERTY(VisibleInstanceOnly)
	float Incoming;

	UPROPERTY(VisibleInstanceOnly)
	float Outgoing;

	FMatchLevelDamageScale()
		: Incoming(0), Outgoing(0)
	{

	}

	FMatchLevelDamageScale(const float& InIncoming, const float& InOutgoing)
		: Incoming(InIncoming), Outgoing(InOutgoing)
	{

	}
};

//=========================================
UCLASS(config=Game)
class AShooterGameMode : public AOnlineGameMode
{
	GENERATED_UCLASS_BODY()

	/** set or change a player's team
	* NewTeam is a request, not a guarantee (game mode may force balanced teams, for example)
	*/
	UFUNCTION(BlueprintCallable, Category = TeamGame)
	virtual bool ChangeTeam(AController* Player, uint8 NewTeam = 255, bool bBroadcast = true);

	virtual uint8 GetTeamIndexById(const FGuid& teamId)
	{
		return 255;
	}

	virtual FMatchEndResult GetMatchEndResult() const override;

	virtual bool AllowRemovingBot(AShooterAIController* B);
	virtual void CheckBotAmmunition();
	virtual void CheckBotCount();


	virtual bool IsAllowJoinInProgress() const override;
	virtual int32 GetRemainingTime() const override;
	virtual int32 GetTimeLimit() const override;

	UPROPERTY(VisibleInstanceOnly, BlueprintReadOnly, Category = GameMode)
	bool bFirstBloodOccurred;

	UPROPERTY(VisibleInstanceOnly, BlueprintReadOnly, Category = GameMode)
		int32 CheckBotAmmunitionInterval;

	UPROPERTY(EditInstanceOnly)
		TEnumAsByte<ENodeSessionMatchTargetLevel::Type> MatchTargetLevel;

	static TArray<FMatchLevelDamageScale> MatchLevelDamageScales;

	/** The bot pawn class */
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category=GameMode)
	TSubclassOf<APawn> BotPawnClass;

	virtual void PreInitializeComponents() override;

	/** Initialize the game. This is called before actors' PreInitializeComponents. */
	virtual void InitGame(const FString& MapName, const FString& Options, FString& ErrorMessage) override;

	virtual void InitGameState() override;
	virtual void OnInitializeMatchInformation();

	/** Accept or reject a player attempting to join the server.  Fails login if you set the ErrorMessage to a non-empty string. */
	virtual void PreLogin(const FString& Options, const FString& Address, const FUniqueNetIdRepl& UniqueId, FString& ErrorMessage) override;

	/** starts match warmup */
	virtual void PostLogin(APlayerController* NewPlayer) override;

	/** select best spawn point for player */
	virtual AActor* ChoosePlayerStart_Implementation(AController* Player) override;

	/** always pick new random spawn */
	virtual bool ShouldSpawnAtStartSpot(AController* Player) override;

	/** returns default pawn class for given controller */
	virtual UClass* GetDefaultPawnClassForController_Implementation(AController* InController) override;

	/** prevents friendly fire */
	virtual float ModifyDamage(float Damage, AActor* DamagedActor, struct FDamageEvent const& DamageEvent, AController* EventInstigator, AActor* DamageCauser) const;

	/** notify about kills */
	virtual void Killed(AController* Killer, AController* KilledPlayer, APawn* KilledPawn, const int32& InWeaponId);

	/** can players damage each other? */
	virtual bool CanDealDamage(AShooterPlayerState* DamageInstigator, AShooterPlayerState* DamagedPlayer) const;

	/** always create cheat manager */
	virtual bool AllowCheats(APlayerController* P) override;

	/** update remaining time */
	virtual void DefaultTimer() override;

	/** called before startmatch */
	virtual void HandleMatchIsWaitingToStart() override;

	/** starts new match */
	virtual void HandleMatchHasStarted() override;

	virtual void HandleMatchHasEnded() override;

	virtual bool MovePlayerToTeam(AShooterPlayerState* PS, uint8 NewTeam) { return false; }

	/** Create a bot */
	virtual AShooterAIController* AddBot(const uint8& TeamNum = 255);

#if WITH_EDITORONLY_DATA
protected:
	UPROPERTY(config)	int32 DeveloperTeamNum;
#endif

protected:

	static bool IsBot(AShooterPlayerState* PS);
	static bool IsPlayer(AShooterPlayerState* PS);

	UPROPERTY(BlueprintReadOnly, Category = GameMode)
	AShooterGameState* ShooterGameState;

	/** delay between first player login and starting match */
	UPROPERTY(config)
	int32 WarmupTime;

	/** match duration */
	UPROPERTY(config)
	int32 RoundTime;

	UPROPERTY(config)
	int32 TimeBetweenMatches;

	/** score for kill */
	UPROPERTY(config, VisibleInstanceOnly, Category = GameMode)
	int32 KillScore;

	/** score for death */
	UPROPERTY(config, VisibleInstanceOnly, Category = GameMode)
	int32 DeathScore;

	/** scale for self instigated damage */
	UPROPERTY(config)
	float DamageSelfScale;

	UPROPERTY()
	TArray<AShooterAIController*> BotControllers;
	
	/** Handle for efficient management of DefaultTimer timer */
	FTimerHandle TimerHandle_DefaultTimer;

	/** spawning all bots for this game */
	void StartBots();

	virtual void Logout(AController* Exiting) override;

	/** initialization for bot after creation */
	virtual void InitBot(AShooterAIController* AIC, const uint8& InTeamNum = 255);

	/** check who won */
	virtual void DetermineMatchWinner();

	/** check if player can use spawnpoint */
	virtual bool IsSpawnpointAllowed(APlayerStart* SpawnPoint, AController* Player) const;

	/** check if player should use spawnpoint */
	virtual bool IsSpawnpointPreferred(APlayerStart* SpawnPoint, AController* Player) const;

	/** Returns game session class to use */
	virtual TSubclassOf<AGameSession> GetGameSessionClass() const override;	

	virtual void OnMatchEnded() override;

public:	

	/** get the name of the bots count option used in server travel URL */
	static FString GetBotsCountOptionName();

	UPROPERTY()
	TArray<AShooterPickup*> LevelPickups;

};
