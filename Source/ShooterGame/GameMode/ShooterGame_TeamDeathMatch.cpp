﻿// Copyright 1998-2018 Epic Games, Inc. All Rights Reserved.

#include "ShooterGame.h"
#include "ShooterGame_TeamDeathMatch.h"
#include "Bots/ShooterAIController.h"
#include "GameState/ShooterGameState.h"
#include "GameState/ShooterPlayerState.h"
#include "GamePlay/ShooterTeamStart.h"
#include "GameState/TeamInfo.h"
#include "Game/Components/NodeComponent.h"
#include "ShooterCharacter.h"
#include "ArsenalComponent.h"
#include "TArrayExtensions.h"

AShooterGame_TeamDeathMatch::AShooterGame_TeamDeathMatch(const FObjectInitializer& ObjectInitializer) : Super(ObjectInitializer)
{
	bDelayedStart = true;
}


uint8 AShooterGame_TeamDeathMatch::GetTeamIndexById(const FGuid& teamId)
{
	uint8 index = 0;
	TArray<ATeamInfo*> Teams = GetTeams();
	for (const auto team : Teams)
	{
		if (team->IsEqualId(teamId))
		{
			return index;
		}
		index++;
	}
	return 255;
}

void AShooterGame_TeamDeathMatch::PostLogin(APlayerController* NewPlayer)
{
	Super::PostLogin(NewPlayer);
}

void AShooterGame_TeamDeathMatch::InitGameState()
{
	Super::InitGameState();
}

void AShooterGame_TeamDeathMatch::OnInitializeMatchInformation()
{
	Super::OnInitializeMatchInformation();

	if (ShooterGameState)
	{
		//ShooterGameState->GiveTeamScore(0, ShooterGameState->GetGoalScore());
		//ShooterGameState->GiveTeamScore(1, ShooterGameState->GetGoalScore());
	}
}

int32 AShooterGame_TeamDeathMatch::GetTeamWinRate(const uint8& InTeamNum) const
{
	const auto gs = GetGameState<AShooterGameState>();
	if (gs)
	{
		return gs->GetTeamScore(InTeamNum);
	}

	return 0;
}

float AShooterGame_TeamDeathMatch::GetTeamScore(const uint8& InTeamNum) const
{
	const auto gs = GetGameState<AShooterGameState>();
	if (gs)
	{
		return gs->GetTeamScore(InTeamNum);
	}

	return 0.0f;
}

ATeamInfo* AShooterGame_TeamDeathMatch::GetTeam(const uint8& InTeamNum) const
{
	const auto gs = GetGameState<AShooterGameState>();
	if (gs)
	{
		return gs->GetTeam(InTeamNum);
	}

	return nullptr;
}

TArray<ATeamInfo*> AShooterGame_TeamDeathMatch::GetTeams() const
{
	const auto gs = GetGameState<AShooterGameState>();
	if (gs)
	{
		return gs->GetTeams();
	}

	return TArray<ATeamInfo*>();
}


FGuid AShooterGame_TeamDeathMatch::GetMatchWinnerTeamId() const
{
	//=================================================
	if(WinnerTeamInfo.Num >= 0)
	{
		return WinnerTeamInfo.Id;
	}

	//=================================================
	ATeamInfo* WinnerTeam = nullptr;

	//=================================================
	const auto gs = GetGameState<AShooterGameState>();

	//=================================================
	if (gs == nullptr)
	{
		return FGuid();
	}

	//=================================================
	const auto BlueTeam = gs->GetTeam(0);
	const auto OrangeTeam = gs->GetTeam(1);

	//=================================================
	if (BlueTeam == nullptr)
	{
		WinnerTeam = OrangeTeam;
	}
	else if (OrangeTeam == nullptr)
	{
		WinnerTeam = BlueTeam;
	}
	else
	{
		//--------------------------------------------
		auto BlueTeamScore = BlueTeam->GetScore();
		auto OrangeTeamScore = OrangeTeam->GetScore();

		//--------------------------------------------
		if (BlueTeamScore > OrangeTeamScore)
		{
			WinnerTeam = BlueTeam;
		}
		else if (BlueTeamScore < OrangeTeamScore)
		{
			WinnerTeam = OrangeTeam;
		}
	}

	//=================================================
	if (WinnerTeam)
	{
		WinnerTeamInfo.Id = WinnerTeam->GetTeamId();
		WinnerTeamInfo.Num = WinnerTeam->GetTeamNum();
		return WinnerTeam->GetTeamId();
	}

	//=================================================
	return Super::GetMatchWinnerTeamId();
}


FMatchEndResult AShooterGame_TeamDeathMatch::GetMatchEndResult() const
{
	//============================================
	const TArray<ATeamInfo*> Teams = GetTeams();

	//============================================
	FMatchEndResult MatchResult = Super::GetMatchEndResult();

	//============================================
	UE_LOG(LogInit, Warning, TEXT("[AShooterGame_TeamDeathMatch][Teams: %d][WinnerTeamId: %s]"), static_cast<int32>(Teams.Num()), *MatchResult.WinnerTeamId.ToString());

	//============================================
	for(const auto team : Teams)
	{
		if (team)
		{
			MatchResult.Scores.Add(team->GetTeamId(), FMath::FloorToInt(team->GetScore()));
			UE_LOG(LogInit, Warning, TEXT("[AShooterGame_TeamDeathMatch][Team: %d | %s][Score: %d]"), static_cast<int32>(team->GetTeamNum()), *team->GetTeamStrId(), team->GetIntScore());
		}
	}

	//============================================
	return MatchResult;
}


void AShooterGame_TeamDeathMatch::Killed(AController* Killer, AController* KilledPlayer, APawn* KilledPawn, const int32& InWeaponId)
{
	Super::Killed(Killer, KilledPlayer, KilledPawn, InWeaponId);

	AShooterPlayerState* KillerPlayerState = GetPlayerState<AShooterPlayerState>(Killer);
	AShooterPlayerState* VictimPlayerState = GetPlayerState<AShooterPlayerState>(KilledPlayer);
	if (VictimPlayerState && KillerPlayerState && VictimPlayerState != KillerPlayerState && KillerPlayerState->GetTeam())
	{
		KillerPlayerState->GetTeam()->GiveScore(1);
	}
}

bool AShooterGame_TeamDeathMatch::CanDealDamage(AShooterPlayerState* DamageInstigator, class AShooterPlayerState* DamagedPlayer) const
{
	return true;// DamageInstigator && DamagedPlayer && (DamagedPlayer == DamageInstigator || DamagedPlayer->GetTeamNum() != DamageInstigator->GetTeamNum());
}

bool AShooterGame_TeamDeathMatch::IsWinner(AOnlinePlayerState* InPlayerState) const
{
	if (const auto PlayerState = GetValidObjectAs<AShooterPlayerState>(InPlayerState))
	{
		return PlayerState && !PlayerState->IsQuitter() && PlayerState->GetTeamId() == GetMatchWinnerTeamId();
	}
	return false;
}

bool AShooterGame_TeamDeathMatch::IsSpawnpointAllowed(APlayerStart* SpawnPoint, AController* Player) const
{
	if (Player)
	{
		AShooterTeamStart* TeamStart = Cast<AShooterTeamStart>(SpawnPoint);
		AShooterPlayerState* PlayerState = GetValidObjectAs<AShooterPlayerState>(Player->PlayerState);

		if (PlayerState && TeamStart && TeamStart->SpawnTeam != PlayerState->GetTeamNum())
		{
			return false;
		}
	}

	return Super::IsSpawnpointAllowed(SpawnPoint, Player);
}


bool AShooterGame_TeamDeathMatch::ShouldBalanceTeams(const bool bInitialTeam) const
{
	return bInitialTeam == false || HasMatchStarted();
}

bool AShooterGame_TeamDeathMatch::ChangeTeam(AController* Player, uint8 NewTeam, bool bBroadcast)
{
	bool bForceTeam = false;

	//=====================================
	auto Teams = GetTeams();

	//=====================================
	if (Player == nullptr)
	{
		return false;
	}

	//=====================================
	AShooterPlayerState* PS = GetValidObjectAs<AShooterPlayerState>(Player->PlayerState);
	if (PS == nullptr || PS->bOnlySpectator)
	{
		return false;
	}

	//=====================================
	if (PS->bIsABot == false && PS->GetTeam())
	{
		return false;
	}

	//=====================================
	if (Teams.IsValidIndex(NewTeam))
	{
		if (ShouldBalanceTeams(PS->GetTeam() == nullptr))
		{
			UE_LOG(LogInit, Warning, TEXT("ChangeTeam #3 %s ShouldBalanceTeams %d was true"), *Player->PlayerState->GetPlayerName(), static_cast<int32>(NewTeam));

			for (int32 i = 0; i < Teams.Num(); i++)
			{
				const auto iHumans = Teams[i]->GetNumHumans();
				const auto nHumans = Teams[NewTeam]->GetNumHumans();

				// don't allow switching to a team with more players, or equal players if the player is on a team now
				if (i != NewTeam)
				{
					const auto v = PS->GetTeamNum() == i ? 1 : 0;
					if (iHumans - v < nHumans)
					{
						UE_LOG(LogInit, Warning, TEXT("ChangeTeam #3.1 %s ShouldBalanceTeams %d was true"), *Player->PlayerState->GetPlayerName(), static_cast<int32>(NewTeam));

						bForceTeam = true;
						break;
					}
				}
			}
		}
		else
		{
			UE_LOG(LogInit, Warning, TEXT("ChangeTeam #3 %s ShouldBalanceTeams %d was false"), *Player->PlayerState->GetPlayerName(), static_cast<int32>(NewTeam));
		}
	}
	else
	{
		bForceTeam = true;
		UE_LOG(LogInit, Warning, TEXT("ChangeTeam #1 %s invalid NewTeam %d"), *Player->PlayerState->GetPlayerName(), static_cast<int32>(NewTeam));
	}

	if (bForceTeam)
	{
		const auto oldNewTeam = static_cast<int32>(NewTeam);

		const auto realTeam = PS->GetTeamNum();
		NewTeam = PickBalancedTeam(PS, NewTeam);
		UE_LOG(LogInit, Warning, TEXT("ChangeTeam %s #4 from %d to %d [real new team %d]"), *Player->PlayerState->GetPlayerName(), realTeam, oldNewTeam, static_cast<int32>(NewTeam));
	}

	if (MovePlayerToTeam(PS, NewTeam))
	{
		UE_LOG(LogInit, Warning, TEXT("ChangeTeam #5 %s MovePlayerToTeam %d was true"), *Player->PlayerState->GetPlayerName(), static_cast<int32>(NewTeam));
		return true;
	}
	else
	{
		UE_LOG(LogInit, Warning, TEXT("ChangeTeam #5 %s MovePlayerToTeam %d was false"), *Player->PlayerState->GetPlayerName(), static_cast<int32>(NewTeam));
	}

	PS->ForceNetUpdate();
	return false;
}



bool AShooterGame_TeamDeathMatch::MovePlayerToTeam(AShooterPlayerState* PS, uint8 NewTeam)
{	
	//=====================================
	auto Teams = GetTeams();

	//=====================================
	if (Teams.IsValidIndex(NewTeam) && PS->GetTeamNum() != NewTeam)
	{
		//Make sure we kill the player before they switch sides so the correct team loses the point
		auto UTC = PS->GetBaseCharacter<AShooterCharacter>();
		if (UTC)
		{
			UTC->Die(0.0, FDamageEvent(), nullptr, nullptr);
		}

		if (PS->GetTeam())
		{
			PS->GetTeam()->RemovePlayer(PS);
		}

		Teams[NewTeam]->AddPlayer(PS);
		PS->ForceNetUpdate();
		return true;
	}

	//=====================================
	return false;
}

uint8 AShooterGame_TeamDeathMatch::PickBalancedTeam(AShooterPlayerState* PS, uint8 RequestedTeam)
{	
	//=====================================
	auto Teams = GetTeams();

	//=====================================
	TArray<ATeamInfo*, TInlineAllocator<4> > BestTeams;
	int32 BestSize = -1;

	//=====================================
	for (int32 i = 0; i < Teams.Num(); i++)
	{
		int32 TestSize = Teams[i]->GetSize();

		if (Teams[i] == PS->GetTeam())
		{
			// player will be leaving this team so count its size as post-departure
			TestSize--;
		}

		if (BestTeams.Num() == 0 || TestSize < BestSize)
		{
			BestTeams.Empty();
			BestTeams.Add(Teams[i]);
			BestSize = TestSize;
		}
		else if (TestSize == BestSize)
		{
			BestTeams.Add(Teams[i]);
		}
	}


	// if in doubt choose team with bots on it as the bots will leave if necessary to balance
	{
		auto TeamsWithBots = BestTeams.FilterByPredicate([](const ATeamInfo* InTeam)
		{
			if(InTeam)
			{
				return !!InTeam->GetTeamMembers().FilterByPredicate([](const AShooterPlayerState* InPs)
				{
					return InPs && InPs->bIsABot;
				}).Num();
			}
			return false;
		});

		if (TeamsWithBots.Num() > 0)
		{
			for (int32 i = 0; i < TeamsWithBots.Num(); i++)
			{
				if (TeamsWithBots[i]->GetTeamNum() == RequestedTeam)
				{
					return RequestedTeam;
				}
			}

			return TeamsWithBots[FMath::RandHelper(TeamsWithBots.Num())]->GetTeamNum();
		}
	}


	for (int32 i = 0; i < BestTeams.Num(); i++)
	{
		if (BestTeams[i]->GetTeamNum() == RequestedTeam)
		{
			return RequestedTeam;
		}
	}

	if (BestTeams.Num())
	{
		return BestTeams[FMath::RandHelper(BestTeams.Num())]->GetTeamNum();
	}
	return 0;
}

ATeamInfo* AShooterGame_TeamDeathMatch::GetTeamWithWorstScore() const
{
	if (GetTeamScore(0) > GetTeamScore(1))
	{
		return GetTeam(1);
	}
	return GetTeam(0);
}

ATeamInfo* AShooterGame_TeamDeathMatch::GetTeamWithBestScore() const
{
	if(GetTeamScore(0) > GetTeamScore(1))
	{
		return GetTeam(0);
	}
	return GetTeam(1);
}

ATeamInfo* AShooterGame_TeamDeathMatch::GetTargetTeamToWin() const
{
	if (GetTeamWinRate(0) > GetTeamWinRate(1))
	{
		return GetTeam(1);
	}
	return GetTeam(0);
}

//===================================================
static TMap<EGameItemType, FInventoryModelScorePair> AdditionalInventoryMaps =
{
	{EGameItemType::Head,	FInventoryModelScorePair(52, 1)},
	{EGameItemType::Torso,	FInventoryModelScorePair(50, 2)},
	{EGameItemType::Legs,	FInventoryModelScorePair(51, 3)},
};

static EGameItemType GetNextArmourSlot(const EGameItemType& InArmourSlto)
{
	if (InArmourSlto == EGameItemType::Head) return EGameItemType::Torso;
	if (InArmourSlto == EGameItemType::Torso) return EGameItemType::Legs;
	return EGameItemType::Head;
}

static int32 GetArmourScoreForTeam(ATeamInfo* InTeam, EGameItemType& OutMaximumArmourSlotEquiped)
{
	//===================================================
	OutMaximumArmourSlotEquiped = EGameItemType::None;

	//===================================================
	int32 ArmourScore = 0;

	//===================================================
	auto TeamBots = InTeam->GetTeamBots();

	//===================================================
	for (auto bot : TeamBots)
	{
		for (auto slot : AdditionalInventoryMaps)
		{
			if (IsValidObject(bot) && bot->IsInventorySlotEquiped(slot.Key))
			{
				//-------------------------------
				auto CurrentArrayIndex = static_cast<int32>(slot.Key);
				if (CurrentArrayIndex > static_cast<int32>(OutMaximumArmourSlotEquiped))
				{
					OutMaximumArmourSlotEquiped = slot.Key;
				}

				ArmourScore += slot.Value.Score;
			}
		}
	}

	//===================================================
	return ArmourScore;
}

bool AShooterGame_TeamDeathMatch::UpgradeBotAmmunition(ATeamInfo* InBestTeam, TArray<AShooterPlayerState*>& InTargetTeamBotsToWin)
{
	//===================================================
	AShooterPlayerState* Bot = nullptr;

	//===================================================
	int32 TargetArmourScoreForUpgrade = 1;

	//===================================================
	EGameItemType TargetArmourSlotEquiped = EGameItemType::None;
	
	//===================================================
	const	int32 TargetWeaponLevel = InBestTeam->GetTargetWeaponLevel();
	const	int32 TargetArmourLevel = InBestTeam->GetTargetArmourLevel();
	const	int32 CurrentArmourLevel = GetArmourScoreForTeam(InBestTeam, TargetArmourSlotEquiped);

	//===================================================
	const	int32 AvalibleArmourScore = TargetArmourLevel - CurrentArmourLevel;

	//===================================================
	UE_LOG(LogInit, Warning, TEXT("[UpgradeBotAmmunition][TargetWeaponLevel: %d][TargetArmourLevel: %d | CurrentArmourLevel: %d][AvalibleArmourScore: %d][TargetArmourSlotEquiped: %d]"), TargetWeaponLevel, TargetArmourLevel, CurrentArmourLevel, AvalibleArmourScore, static_cast<int32>(TargetArmourSlotEquiped));


	//~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
	if (AvalibleArmourScore > 0)
	{
		for (int i = 0; i < 3; i++)
		{
			//===============================================
			if (AdditionalInventoryMaps.Contains(TargetArmourSlotEquiped))
			{
				auto TargetInventoryItem = AdditionalInventoryMaps.Find(TargetArmourSlotEquiped);
				if (TargetInventoryItem)
				{
					TargetArmourScoreForUpgrade = TargetInventoryItem->Score;
				}
				else
				{
					TargetArmourScoreForUpgrade = 1;
				}
			}
			else
			{
				TargetArmourScoreForUpgrade = 1;
			}

			//===============================================
			//	Проверяем, достаточно ли у нас очков брони для улучшения бота
			if (AvalibleArmourScore >= TargetArmourScoreForUpgrade)
			{
				//	Получаем бота, который еще не одел броню для текущего слота
				Bot = FArrayExtensions::TakeRandomByPredicate<AShooterPlayerState>(InTargetTeamBotsToWin, [TargetArmourSlotEquiped](const AShooterPlayerState* InPlayerState)
				{
					return InPlayerState && InPlayerState->IsValidLowLevel() && InPlayerState->IsInventorySlotEquiped(TargetArmourSlotEquiped) == false;
				}, true);
			}

			//===============================================
			//	Если есть такой бот, то прекращаем цикл и делаем улучшение брони
			if (Bot)
			{
				break;
			}
			else
			{
				//	Если нет ботов которые еще не оделись, то ищем следующий слот для брони
				TargetArmourSlotEquiped = GetNextArmourSlot(TargetArmourSlotEquiped);
			}
		}
	}

	//~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
	//	Если есть бот, который еще не оделся, то одеваем его в TargetArmourSlotEquiped
	if (Bot && AdditionalInventoryMaps.Contains(TargetArmourSlotEquiped))
	{
		//--------------------------------------------------
		auto InventoryComponent = Bot->GetInventoryComponent();
		if (InventoryComponent)
		{
			auto TargetInventoryItem = AdditionalInventoryMaps.Find(TargetArmourSlotEquiped);
			if (TargetInventoryItem)
			{
				InventoryComponent->AddIfNotExistItemInSlot(0, TargetArmourSlotEquiped, TargetInventoryItem->ModelId);
				return true;
			}
		}
	}
	return false;
}

/** Улучшаем оружие ботов - делаем улучшение оружия */
bool AShooterGame_TeamDeathMatch::UpgradeBotWeaponModifications(ATeamInfo* InBestTeam, TArray<AShooterPlayerState*>& InTargetTeamBotsToWin)
{
	//=================================================================
	const int32 TargetWeaponUpgrade = InBestTeam->GetTargetWeaponUpgrade();

	//=================================================================
	UE_LOG(LogInit, Warning, TEXT("[UpgradeBotWeaponModifications][TargetWeaponUpgrade: %d]"), TargetWeaponUpgrade);

	//=================================================================
	//	Нужно доработать улучшение оружия
	if (TargetWeaponUpgrade == 0)
	{
		return false;
	}

	//=================================================================
	AShooterPlayerState* Bot = FArrayExtensions::TakeRandomByPredicate<AShooterPlayerState>(InTargetTeamBotsToWin, [](const AShooterPlayerState* InPlayerState) 
	{
		return InPlayerState && InPlayerState->IsValidLowLevel() && InPlayerState->BotMaximumLevelReached() == false; 
	}, true);

	//=================================================================
	if (Bot)
	{
		//--------------------------------------------------
		if (CheckBotAmmunitionInterval <= 5)
		{
			CheckBotAmmunitionInterval = 10;
		}

		//--------------------------------------------------
		auto InventoryComponent = Bot->GetInventoryComponent();
		if (InventoryComponent)
		{
			InventoryComponent->UpgradeDefaultItemInProfile(0);
			return true;
		}
	}

	//=================================================================
	return false;
}

/** Улучшаем оружие ботов - заменяем оружие на более мощное */
bool AShooterGame_TeamDeathMatch::UpgradeBotWeaponEquipment(ATeamInfo* InBestTeam, TArray<AShooterPlayerState*>& InTargetTeamBotsToWin)
{
	//=================================================================
	const int32 TargetWeaponLevel = InBestTeam->GetTargetWeaponLevel();

	//=================================================================
	UE_LOG(LogInit, Warning, TEXT("[UpgradeBotWeaponEquipment][TargetWeaponLevel: %d]"), TargetWeaponLevel);

	//=================================================================
	AShooterPlayerState* Bot = FArrayExtensions::TakeRandomByPredicate<AShooterPlayerState>(InTargetTeamBotsToWin, [TargetWeaponLevel](const AShooterPlayerState* InPlayerState) 
	{
		return InPlayerState && InPlayerState->IsValidLowLevel() && InPlayerState->BotIsTargetWeaponLevel(TargetWeaponLevel) == false; 
	}, false);

	if (Bot)
	{
		//--------------------------------------------------
		if (CheckBotAmmunitionInterval <= 10)
		{
			CheckBotAmmunitionInterval = 15;
		}

		//--------------------------------------------------
		auto InventoryComponent = Bot->GetInventoryComponent();
		if (InventoryComponent)
		{
			InventoryComponent->ReplaceWeaponInProfile(0, TargetWeaponLevel);
			return true;
		}
	}

	return false;
}

void AShooterGame_TeamDeathMatch::CheckBotAmmunition()
{
	//===================================================
	ATeamInfo* BestTeam = GetTeamWithBestScore();
	ATeamInfo* WorstTeam = GetTeamWithWorstScore();
	ATeamInfo* TargetTeamToWin = GetTargetTeamToWin();

	//===================================================
	if(BestTeam == nullptr || WorstTeam == nullptr || TargetTeamToWin == nullptr)
	{
		return;
	}

	//===================================================
	auto DeltaTeamScores = BestTeam->GetScore() - WorstTeam->GetScore();

	//===================================================
	//	Если команда выигрывает больше чем на 5 очков, то ничего не делаем
	if(BestTeam == TargetTeamToWin && DeltaTeamScores >= 5 )
	{
		return;
	}

	//===================================================
	auto TeamBots = TargetTeamToWin->GetTeamBots();

	//===================================================
	if (UpgradeBotAmmunition(BestTeam, TeamBots))
	{
		return;
	}

	//===================================================
	if (UpgradeBotWeaponModifications(BestTeam, TeamBots))
	{
		return;
	}

	//===================================================
	if (UpgradeBotWeaponEquipment(BestTeam, TeamBots))
	{
		return;
	}
}


void AShooterGame_TeamDeathMatch::CheckBotCount()
{
	//===================================================
	auto CurrentGameState = GetValidObjectAs<AShooterGameState>(GameState);

	//===================================================
	if (NumPlayers + NumBots > GetMatchOptions().GetNumberOfBots())
	{
		//--------------------------------------------------
		TArray<ATeamInfo*> SortedTeams = CurrentGameState->GetTeams();

		//--------------------------------------------------
		SortedTeams.Sort([](ATeamInfo& A, ATeamInfo& B)
		{
			return A.GetSize() > B.GetSize();
		});

		//--------------------------------------------------
		// try to remove bots from team with the most players
		for (ATeamInfo* Team : SortedTeams)
		{
			//--------------------------------------------------
			TArray<AShooterPlayerState*> Members = Team->GetTeamMembers();

			//--------------------------------------------------
			for (auto C : Members)
			{
				auto MemberPlayerState = GetValidObject<AShooterPlayerState>(C);
				if (MemberPlayerState && MemberPlayerState->bIsABot)
				{
					if (auto MemberPlayerController = MemberPlayerState->GetBaseController<AShooterAIController>())
					{
						if (AllowRemovingBot(MemberPlayerController))
						{
							MemberPlayerController->Destroy();
						}
					}
					return;
				}
			}
		}
	}
	else while (NumPlayers + NumBots < GetMatchOptions().GetNumberOfBots())
	{
		auto AIC = AddBot();
		if (AIC)
		{
			RestartPlayer(AIC);
		}
	}
}

void AShooterGame_TeamDeathMatch::InitBot(AShooterAIController* AIC, const uint8& InTeamNum)
{	
	Super::InitBot(AIC, InTeamNum);
	ChangeTeam(AIC, InTeamNum);
}