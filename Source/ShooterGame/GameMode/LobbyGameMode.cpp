// Fill out your copyright notice in the Description page of Project Settings.

#include "ShooterGame.h"
#include "LobbyGameMode.h"
#include "GameState/LobbyPlayerState.h"
#include "LobbyCharacter.h"
#include "GameAnalytics.h"
#include "ShooterPlayerController_Menu.h"
//#include "CrashlyticsBlueprintLibrary.h"
#include "Notify/SNotifyContainer.h"
#include "ShooterGameAnalytics.h"
#include "UEnumExtensions.h"

#include <OnlineSubsystem.h>

#if 0//PLATFORM_ANDROID
#include "JavaObject.h"
#endif 

ALobbyGameMode::ALobbyGameMode()
	: Super()
{
	bIsMultiPlayerGameMode = false;
	PlayerStateClass = ALobbyPlayerState::StaticClass();
	DefaultPawnClass = ALobbyCharacter::StaticClass();
	PlayerControllerClass = AShooterPlayerController_Menu::StaticClass();
}

void ALobbyGameMode::StartPlay()
{

	//=======================================================
	Super::StartPlay();

	//=======================================================
	NextAllowPurchaseDate = FDateTime::Now();


	//=======================================================
	//UCrashlyticsBlueprintLibrary::ContinuationConsumePurchase();
}

void ALobbyGameMode::EndPlay(const EEndPlayReason::Type EndPlayReason)
{

	//=======================================================
	//UCrashlyticsBlueprintLibrary::SetField("TravelSuccessfully", false);

	if(EndPlayReason == EEndPlayReason::Quit)
	{
		UGameAnalytics::endSession();
	}
	
	Super::EndPlay(EndPlayReason);
}

EInAppPurchaseStatus ALobbyGameMode::GetInAppPurchaseServiceStatus() const
{
	//=================================================
	auto system = IOnlineSubsystem::Get(GOOGLEPLAY_SUBSYSTEM);
	if (system == nullptr)
	{
		return EInAppPurchaseStatus::Invalid;
	}

	//=================================================
	auto StoreInterface = system->GetStoreInterface().Get();
	if (StoreInterface == nullptr)
	{
		return EInAppPurchaseStatus::Failed;
	}

	//=================================================

#if 0//PLATFORM_ANDROID
	if (FAndroidApplication::GetJavaEnv(true) == nullptr)
	{
		return EInAppPurchaseStatus::JavaEnvWasNullptr;
	}
#endif

	//=================================================
	bool IsAllowedToMakePurchases = StoreInterface->IsAllowedToMakePurchases();
	if (IsAllowedToMakePurchases == false)
	{
		return EInAppPurchaseStatus::Failed;
	}

	return EInAppPurchaseStatus::Deferred;
}

bool ALobbyGameMode::OnInAppPurchase(const FString& InProductName, const EInAppPurchaseMethodHandler& InMethodHandler)
{
	//UCrashlyticsBlueprintLibrary::ContinuationConsumePurchase();

	//=======================================================================
	if(FDateTime::Now() < NextAllowPurchaseDate)
	{
		SNotifyContainer::ShowNotify("Notify_MakeInAppPurchase_PaymentWIP", NSLOCTEXT("Notify", "Notify.MakeInAppPurchase.PaymentWIP", "Payment already in progress"));
		return false;
	}

	//=======================================================================
	NextAllowPurchaseDate = FDateTime::Now() + FTimespan::FromSeconds(2);
	
	//=======================================================================
	const auto PurchaseStatus = GetInAppPurchaseServiceStatus();

	//=======================================================================
	switch (PurchaseStatus)
	{
	
		case EInAppPurchaseStatus::Invalid:
		//UCrashlyticsBlueprintLibrary::SetField("MakeInAppPurchase.Purchase", "SubsytemNotAvalible");
		FShooterGameAnalytics::RecordDesignEvent("UI:Lobby:Market:Purchase:IAP:SubsytemNotAvalible");
		//UCrashlyticsBlueprintLibrary::WriteLog("UArsenalComponent:OnMakeInAppPurchase:SubsytemNotAvalible");
		FShooterGameAnalytics::RecordErrorEvent(EErrorSeverity::error, "OnMakeInAppPurchase::Purchase::SubsytemNotAvalible");
		SNotifyContainer::ShowNotify("Notify_MakeInAppPurchase_SubsytemNotAvalible", NSLOCTEXT("Notify", "Notify.MakeInAppPurchase.SubsytemNotAvalible", "Could not connect to Google Play services to make payment #1"));
		break;
	
		case EInAppPurchaseStatus::Failed:
		//UCrashlyticsBlueprintLibrary::SetField("MakeInAppPurchase.Purchase", "SubsytemStoreNotAvalible");
		FShooterGameAnalytics::RecordDesignEvent("UI:Lobby:Market:Purchase:IAP:SubsytemStoreNotAvalible");
		//UCrashlyticsBlueprintLibrary::WriteLog("UArsenalComponent:OnMakeInAppPurchase:SubsytemStoreNotAvalible");
		FShooterGameAnalytics::RecordErrorEvent(EErrorSeverity::error, "OnMakeInAppPurchase::Purchase::SubsytemStoreNotAvalible");
		SNotifyContainer::ShowNotify("Notify_MakeInAppPurchase_SubsytemStoreNotAvalible", NSLOCTEXT("Notify", "Notify.MakeInAppPurchase.SubsytemStoreNotAvalible", "Could not connect to Google Play services to make payment #2"));
		break;
	
		case EInAppPurchaseStatus::Canceled:
		//UCrashlyticsBlueprintLibrary::SetField("MakeInAppPurchase.Purchase", "MakePurchasesNotAllowed");
		FShooterGameAnalytics::RecordDesignEvent("UI:Lobby:Market:Purchase:IAP:MakePurchasesNotAllowed");
		//UCrashlyticsBlueprintLibrary::WriteLog("UArsenalComponent:OnMakeInAppPurchase:MakePurchasesNotAllowed");
		FShooterGameAnalytics::RecordErrorEvent(EErrorSeverity::error, "OnMakeInAppPurchase::Purchase::MakePurchasesNotAllowed");
		SNotifyContainer::ShowNotify("Notify_MakeInAppPurchase_MakePurchasesNotAllowed", NSLOCTEXT("Notify", "Notify.MakeInAppPurchase.MakePurchasesNotAllowed", "Could not connect to Google Play services to make payment #3"));
		break;
	
		case EInAppPurchaseStatus::Deferred:
		//UCrashlyticsBlueprintLibrary::SetField("MakeInAppPurchase.Purchase", "Successfully");
		//UCrashlyticsBlueprintLibrary::WriteLog("UArsenalComponent:OnMakeInAppPurchase:Successfully");
		FShooterGameAnalytics::RecordDesignEvent("UI:Lobby:Market:Purchase:IAP:Successfully");
		break;

/*		case EInAppPurchaseStatus::JavaEnvWasNullptr:
		//UCrashlyticsBlueprintLibrary::SetField("MakeInAppPurchase.Purchase", "JavaEnvWasNullptr");
		FShooterGameAnalytics::RecordDesignEvent("UI:Lobby:Market:Purchase:IAP:JavaEnvWasNullptr");
		//UCrashlyticsBlueprintLibrary::WriteLog("UArsenalComponent:OnMakeInAppPurchase:JavaEnvWasNullptr");
		FShooterGameAnalytics::RecordErrorEvent(EErrorSeverity::error, "OnMakeInAppPurchase::Purchase::JavaEnvWasNullptr");
		SNotifyContainer::ShowNotify("Notify_MakeInAppPurchase_JavaEnvWasNullptr", NSLOCTEXT("Notify", "Notify.MakeInAppPurchase.JavaEnvWasNullptr", "Could not start payment process, JavaEnvWasNullptr"));
		break;*/
	}

	if( PurchaseStatus == EInAppPurchaseStatus::Deferred)
	{
		OnInAppPurchaseNative(InProductName, InMethodHandler);
	}
	
	UE_LOG(LogGameMode, Error, TEXT("[ALobbyGameMode::OnInAppPurchase][Item: %s][PurchaseStatus: %d / %s]"), *InProductName, static_cast<int32>(PurchaseStatus), *GetEnumValueAsString("EInAppPurchaseStatus", PurchaseStatus));
	return PurchaseStatus == EInAppPurchaseStatus::Deferred;
}

void ALobbyGameMode::OnInAppPurchaseNative_Implementation(const FString& InProductName, const EInAppPurchaseMethodHandler& InMethodHandler)
{
	SNotifyContainer::ShowNotify("Notify_MakeInAppPurchase_ServiceNotAvailable", NSLOCTEXT("Notify", "Notify.MakeInAppPurchase.ServiceNotAvailable", "Payment service not available"));
}

void ALobbyGameMode::OnInAppPurchaseFailed(const EInAppPurchaseMethodHandler& InMethodHandler, EInAppPurchaseState::Type CompletionStatus, const FInAppPurchaseProductInfo& InAppPurchaseInformation)
{
	//=======================================================
	UE_LOG(LogGameMode, Display, TEXT("[OnMakeInAppPurchaseFailed][%s / %d][Identifier: %s][TransactionIdentifier: %s][ReceiptData: %s][DisplayName: %s]"), *GetEnumValueAsString("EInAppPurchaseState", CompletionStatus), static_cast<int32>(CompletionStatus), *InAppPurchaseInformation.Identifier, *InAppPurchaseInformation.TransactionIdentifier, *InAppPurchaseInformation.ReceiptData, *InAppPurchaseInformation.DisplayName);

	//FShooterGameAnalytics::AddBusinessEvent(InAppPurchaseInformation.CurrencyCode, InAppPurchaseInformation.RawPrice * 100.0f, "IAP", InAppPurchaseInformation.Identifier, "IAP", InAppPurchaseInformation.ReceiptData, InAppPurchaseInformation.TransactionIdentifier, static_cast<EBusinessPurchaseState>(CompletionStatus));

	//=======================================================
	if (CompletionStatus == EInAppPurchaseState::Failed)
	{
		SNotifyContainer::ShowNotify("OnMakeInAppPurchaseFailed.Failed", NSLOCTEXT("Notify", "Notify.OnMakeInAppPurchase.Failed", "A payment error occured on Google Play"));
	}
	else if (CompletionStatus == EInAppPurchaseState::Invalid)
	{
		SNotifyContainer::ShowNotify("OnMakeInAppPurchaseFailed.FailedCreateTransaction", NSLOCTEXT("Notify", "Notify.OnMakeInAppPurchase.FailedCreateTransaction", "A payment error occured on Google Play. Failed to create a transaction"));
	}
	else if (CompletionStatus == EInAppPurchaseState::Cancelled)
	{
		SNotifyContainer::ShowNotify("OnMakeInAppPurchaseFailed.Cancelled", NSLOCTEXT("Notify", "Notify.OnMakeInAppPurchase.Cancelled", "Payment on Google Play has been canceled"));
	}
	else if (CompletionStatus == EInAppPurchaseState::NotAllowed)
	{
		SNotifyContainer::ShowNotify("OnMakeInAppPurchaseFailed.NotAllowed", NSLOCTEXT("Notify", "Notify.OnMakeInAppPurchase.NotAllowed", "Payment on Google Play has been Not Allowed"));
	}
	else if (CompletionStatus == EInAppPurchaseState::Restored)
	{
		SNotifyContainer::ShowNotify("OnMakeInAppPurchaseFailed.Restored", NSLOCTEXT("Notify", "Notify.OnMakeInAppPurchase.Restored", "Payment on Google Play has been Restored"));
	}
	else if (CompletionStatus == EInAppPurchaseState::AlreadyOwned)
	{
		SNotifyContainer::ShowNotify("OnMakeInAppPurchaseFailed.AlreadyOwned", NSLOCTEXT("Notify", "Notify.OnMakeInAppPurchase.AlreadyOwned", "Payment on Google Play, product has been Already Owned"));
	}
	else
	{
		SNotifyContainer::ShowNotify("OnMakeInAppPurchaseFailed.Unknown", FText::Format(NSLOCTEXT("Notify", "Notify.OnMakeInAppPurchase.Unknown", "A payment Unknown [%s / %d] error occured on Google Play | Identifier: {0} | DisplayName: {1}"),
			FText::FromString(InAppPurchaseInformation.Identifier), 
			FText::FromString(InAppPurchaseInformation.DisplayName),
			FText::FromString(GetEnumValueAsString("EInAppPurchaseState", CompletionStatus)),
			FText::AsNumber(static_cast<int32>(CompletionStatus), &FNumberFormattingOptions::DefaultNoGrouping())
		));
	}
}

void ALobbyGameMode::OnInAppPurchaseSuccessfully(const EInAppPurchaseMethodHandler& InMethodHandler, EInAppPurchaseState::Type CompletionStatus, const FInAppPurchaseProductInfo& InAppPurchaseInformation)
{
	//==================================================
	UE_LOG(LogGameMode, Display, TEXT("[OnMakeInAppPurchaseSuccessfully][Identifier: %s][TransactionIdentifier: %s][ReceiptData: %s][DisplayName: %s]"), *InAppPurchaseInformation.Identifier, *InAppPurchaseInformation.TransactionIdentifier, *InAppPurchaseInformation.ReceiptData, *InAppPurchaseInformation.DisplayName);
	//FShooterGameAnalytics::AddBusinessEvent(InAppPurchaseInformation.CurrencyCode, InAppPurchaseInformation.RawPrice * 100.0f, "IAP", InAppPurchaseInformation.Identifier, "IAP", InAppPurchaseInformation.ReceiptData, InAppPurchaseInformation.TransactionIdentifier, EBusinessPurchaseState::Success);

	if (CompletionStatus == EInAppPurchaseState::Success)
	{
		//=======================================================
		FMarketDeliveryRequest DeliveryRequest;
		DeliveryRequest.ProductId = InAppPurchaseInformation.Identifier;
		DeliveryRequest.PurchaseToken = InAppPurchaseInformation.TransactionIdentifier;
		DeliveryRequest.Reciept = InAppPurchaseInformation.ReceiptData;
		DeliveryRequest.DataSignature = InAppPurchaseInformation.GroupingSeparator;

		//=======================================================
		if (InMethodHandler == EInAppPurchaseMethodHandler::Arsernal)
		{
			OnMarketPurchaseValidate.ExecuteIfBound(DeliveryRequest);
		}
		else if (InMethodHandler == EInAppPurchaseMethodHandler::Progress)
		{
			OnProgressPurchaseValidate.ExecuteIfBound(DeliveryRequest);
		}
	}
	else
	{
		OnInAppPurchaseFailed(InMethodHandler, CompletionStatus, InAppPurchaseInformation);
	}
}