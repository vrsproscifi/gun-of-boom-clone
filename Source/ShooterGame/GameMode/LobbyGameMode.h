// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

//==========================================
#include "CoreMinimal.h"

//==========================================
#include "Game/OnlineGameMode.h"
#include "Components/ArsenalComponent/InAppPurchaseWrapper.h"

//==========================================
#include "InAppPurchaseCallbackProxy2.h"
#include "Public/Interfaces/OnlineStoreInterface.h"

//==========================================
#include "LobbyGameMode.generated.h"



UCLASS()
class SHOOTERGAME_API ALobbyGameMode : public AOnlineGameMode
{
	GENERATED_BODY()
	
public:

	ALobbyGameMode();
	
	virtual void StartPlay() override;
	virtual void EndPlay(const EEndPlayReason::Type EndPlayReason) override;

	//========================================================================================

	UFUNCTION(BlueprintCallable, Category = "GameMode | InAppPurchase")
	bool OnInAppPurchase(const FString& InProductName, const EInAppPurchaseMethodHandler& InMethodHandler);

	UFUNCTION(BlueprintCallable, Category = "GameMode | InAppPurchase")
	EInAppPurchaseStatus GetInAppPurchaseServiceStatus() const;

	//========================================================================================
protected:
	UFUNCTION(BlueprintNativeEvent, BlueprintCallable, Category = "GameMode | InAppPurchase")
	void OnInAppPurchaseNative(const FString& InProductName, const EInAppPurchaseMethodHandler& InMethodHandler);

	UFUNCTION(BlueprintCallable, Category = "GameMode | InAppPurchase")
	void OnInAppPurchaseFailed(const EInAppPurchaseMethodHandler& InMethodHandler, EInAppPurchaseState::Type CompletionStatus, const FInAppPurchaseProductInfo& InAppPurchaseInformation);
	
	UFUNCTION(BlueprintCallable, Category = "GameMode | InAppPurchase")
	void OnInAppPurchaseSuccessfully(const EInAppPurchaseMethodHandler& InMethodHandler, EInAppPurchaseState::Type CompletionStatus, const FInAppPurchaseProductInfo& InAppPurchaseInformation);

	//========================================================================================
public:
	UPROPERTY(VisibleInstanceOnly, BlueprintReadOnly, Transient)	FInAppPurchaseSuccessful OnMarketPurchaseValidate;
	UPROPERTY(VisibleInstanceOnly, BlueprintReadOnly, Transient)	FInAppPurchaseSuccessful OnProgressPurchaseValidate;
	UPROPERTY(VisibleInstanceOnly, BlueprintReadOnly, Transient)	FDateTime NextAllowPurchaseDate;

};
