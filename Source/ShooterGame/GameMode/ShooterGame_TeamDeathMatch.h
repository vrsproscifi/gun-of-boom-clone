// Copyright 1998-2018 Epic Games, Inc. All Rights Reserved.

#pragma once

#include "ShooterGameMode.h"
#include "ShooterGame_TeamDeathMatch.generated.h"

//============================
class AShooterPlayerState;
class AShooterAIController;

//============================
struct FInventoryModelScorePair
{
	FInventoryModelScorePair(const int32& InModelId, const int32& InScore)
		: ModelId(InModelId)
		, Score(InScore)
	{

	}

	FInventoryModelScorePair()
		: ModelId(0)
		, Score(0)
	{

	}

	int32 ModelId;
	int32 Score;
};

UCLASS()
class AShooterGame_TeamDeathMatch : public AShooterGameMode
{
	GENERATED_UCLASS_BODY()

	/** setup team changes at player login */
	void PostLogin(APlayerController* NewPlayer) override;

	/** initialize replicated game data */
	virtual void InitGameState() override;
	virtual void OnInitializeMatchInformation() override;
	virtual void Killed(AController* Killer, AController* KilledPlayer, APawn* KilledPawn, const int32& InWeaponId) override;

	/** can players damage each other? */
	virtual bool CanDealDamage(AShooterPlayerState* DamageInstigator, AShooterPlayerState* DamagedPlayer) const override;
	virtual FMatchEndResult GetMatchEndResult() const override;
	virtual FGuid GetMatchWinnerTeamId() const override;
	virtual void CheckBotAmmunition() override;
	virtual void CheckBotCount() override;
	
	/** Улучшаем броню ботов по системе очков брони */
	UFUNCTION()	bool UpgradeBotAmmunition(ATeamInfo* InBestTeam, TArray<AShooterPlayerState*>& InTargetTeamBotsToWin);

	/** Улучшаем оружие ботов - делаем улучшение оружия */
	UFUNCTION() bool UpgradeBotWeaponModifications(ATeamInfo* InBestTeam, TArray<AShooterPlayerState*>& InTargetTeamBotsToWin);

	/** Улучшаем оружие ботов - заменяем оружие на более мощное */
	UFUNCTION() bool UpgradeBotWeaponEquipment(ATeamInfo* InBestTeam, TArray<AShooterPlayerState*>& InTargetTeamBotsToWin);

	/** whether we should force teams to be balanced right now
	* @param bInitialTeam - if true, request comes from a player requesting its initial team (not a team switch)
	*/
	virtual bool ShouldBalanceTeams(bool bInitialTeam) const;
	/** Process team change request.  May fail based on team sizes and balancing rules. */
	virtual bool ChangeTeam(AController* Player, uint8 NewTeam = 255, bool bBroadcast = true);

	/** Put player on new team if it is valid, return true if successful. */
	virtual bool MovePlayerToTeam(AShooterPlayerState* PS, uint8 NewTeam) override;

	virtual uint8 GetTeamIndexById(const FGuid& teamId) override;

	UFUNCTION(BlueprintCallable, BlueprintPure, Category = "Gameplay|Team")
	virtual ATeamInfo* GetTeamWithBestScore() const;

	UFUNCTION(BlueprintCallable, BlueprintPure, Category = "Gameplay|Team")
		virtual ATeamInfo* GetTeamWithWorstScore() const;

	UFUNCTION(BlueprintCallable, BlueprintPure, Category = "Gameplay|Team")
	virtual ATeamInfo* GetTargetTeamToWin() const;

	UFUNCTION(BlueprintCallable, BlueprintPure, Category = "Gameplay|Team")
		int32 GetTeamWinRate(const uint8& InTeamNum) const;


	UFUNCTION(BlueprintCallable, BlueprintPure, Category = "Gameplay|Team")
		float GetTeamScore(const uint8& InTeamNum) const;

	UFUNCTION(BlueprintCallable, BlueprintPure, Category = "Gameplay|Team")
		ATeamInfo* GetTeam(const uint8& InTeamNum) const;

protected:
	
	UFUNCTION(BlueprintCallable, BlueprintPure, Category = "Gameplay|State")
	TArray<ATeamInfo*> GetTeams() const;

	/** check if PlayerState is a winner */
	virtual bool IsWinner(AOnlinePlayerState* PlayerState) const override;

	/** check team constraints */
	virtual bool IsSpawnpointAllowed(APlayerStart* SpawnPoint, AController* Player) const;

	/** initialization for bot after spawning */
	virtual void InitBot(AShooterAIController* AIC, const uint8& InTeamNum) override;

	virtual uint8 PickBalancedTeam(AShooterPlayerState* PS, uint8 RequestedTeam);
};
