#include "ShooterGame.h"
#include "AdsComponent.h"
#include "AdCollection.h"
#include "Notify/SNotifyContainer.h"
#include "Notify/SMessageBox.h"


UAdsComponent::UAdsComponent()
{
	OnPlayRewardVideoSuccessDelegate.AddDynamic(this, &UAdsComponent::OnPlayRewardVideoSuccess);
	OnPlayRewardVideoClosedDelegate.AddDynamic(this, &UAdsComponent::OnPlayRewardVideoCanceled);
}


void UAdsComponent::EndPlay(const EEndPlayReason::Type EndPlayReason)
{
	//=======================================================
	auto AdModule = FModuleManager::Get().LoadModulePtr<IAdModuleInterface>("AdMob");

	//=======================================================
	UE_LOG(LogInit, Display, TEXT("[InitGameAds][AdModule: %d]"), AdModule != nullptr);

	//=======================================================
	OnPlayRewardVideoSuccessDelegate.Clear();
	OnPlayRewardVideoClosedDelegate.Clear();

	//=======================================================
	if (AdModule)
	{
		AdModule->ClearAllPlayRewardClosedDelegate_Handle();
		AdModule->ClearAllPlayRewardCompleteDelegate_Handle();
	}

	//=======================================================
	Super::EndPlay(EndPlayReason);
}

void UAdsComponent::CreateGameWidgets()
{
	//=======================================================
	auto AdModule = FModuleManager::Get().LoadModulePtr<IAdModuleInterface>("AdMob");

	//=======================================================
	UE_LOG(LogInit, Display, TEXT("[InitGameAds][AdModule: %d]"), AdModule != nullptr);

	//=======================================================
	if (AdModule)
	{
		//------------------------------------------------------
		AdModule->ClearAllPlayRewardClosedDelegate_Handle();
		AdModule->ClearAllPlayRewardCompleteDelegate_Handle();

		//------------------------------------------------------
		AdModule->AddPlayRewardCompleteDelegate_Handle(FPlayRewardCompleteDelegate::CreateLambda([&](const FRewardedStatus& InRewardedStatus)
		{
			if(IsValidObject(this))
			{
				OnPlayRewardVideoSuccessDelegate.Broadcast(FPlayRewardedVideoResponse(RewardedVideoRequest));
			}
		}));

		AdModule->AddPlayRewardClosedDelegate_Handle(FPlayRewardClosedDelegate::CreateLambda([&]()
		{
			if (IsValidObject(this))
			{
				OnPlayRewardVideoClosedDelegate.Broadcast(FPlayRewardedVideoResponse(RewardedVideoRequest));
			}
		}));

		//------------------------------------------------------
		OnPreloadRewardedVideo();

		//------------------------------------------------------
	}
}

void UAdsComponent::OnPlayRewardVideoSuccess(const FPlayRewardedVideoResponse& InResponse)
{
	//SNotifyContainer::ShowNotify("PlayRewardComplete", "PlayRewardComplete");
//ShowErrorMessage("PlayRewardCompleteM", FString::Printf(TEXT("PlayRewardComplete | IsValidObject: %d | IsBound: %d"), IsValidObject(this), OnPlayRewardVideoSuccess.IsBound()));
}

void UAdsComponent::OnPlayRewardVideoCanceled(const FPlayRewardedVideoResponse& InResponse)
{
	//SNotifyContainer::ShowNotify("PlayRewardClosed", "PlayRewardClosed");
}

void UAdsComponent::OnPreloadRewardedVideo()
{
	//=======================================================
	auto AdModule = FModuleManager::Get().LoadModulePtr<IAdModuleInterface>("AdMob");
	UE_LOG(LogInit, Display, TEXT("[OnPreloadRewardedVideo][AdModule: %d]"), AdModule != nullptr);

	//=======================================================
	if (AdModule == nullptr)
	{
		SNotifyContainer::ShowNotify("OnPlayerEntryReward", NSLOCTEXT("DailyReward", "UArsenalComponent.OnPlayerEntryReward.AdModule.nullptr", "Unfortunately, the advertising service is not available. Try again latter. #1"));
		return;
	}
	//=======================================================
	const auto bPlayRewardedVideo = AdModule->PreloadRewardedVideo();
	if (bPlayRewardedVideo != EPlayRewardedVideoStatus::RewardedVideoAdSuccessfully)
	{
		SNotifyContainer::ShowNotify("OnPlayerEntryReward", FText::Format(NSLOCTEXT("DailyReward", "UArsenalComponent.OnPlayerEntryReward.AdModule.VideoNotReady", "Unfortunately, the advertising service is not available. Try again latter. {0}"), bPlayRewardedVideo));
	}

}


bool UAdsComponent::PlayRewardVideo(const FPlayRewardedVideoRequest& InVideoRequest)const
{
	//=======================================================
	PreviewRewardedVideoRequest = RewardedVideoRequest;
	RewardedVideoRequest = InVideoRequest;

	//=======================================================
#if UE_EDITOR

	UE_LOG(LogInit, Display, TEXT("[PlayRewardVideo][AdModule: EDITOR][InVideoRequest: %s / %s]"), *InVideoRequest.Category, *InVideoRequest.Entity);
	OnPlayRewardVideoSuccessDelegate.Broadcast(FPlayRewardedVideoResponse(RewardedVideoRequest));
	return true;

#else

	//=======================================================
	auto AdModule = FModuleManager::Get().LoadModulePtr<IAdModuleInterface>("AdMob");
	UE_LOG(LogInit, Display, TEXT("[PlayRewardVideo][AdModule: %d][InVideoRequest: %s / %s]"), AdModule != nullptr, *InVideoRequest.Category, *InVideoRequest.Entity);

	//=======================================================
	if(AdModule == nullptr)
	{
		SNotifyContainer::ShowNotify("OnPlayerEntryReward", NSLOCTEXT("DailyReward", "UArsenalComponent.OnPlayerEntryReward.AdModule.nullptr", "Unfortunately, the advertising service is not available. Try again latter. #1"));
		return false;
	}
	//=======================================================	 
	const auto bPlayRewardedVideo = AdModule->PlayRewardedVideo();
	
	if (bPlayRewardedVideo != EPlayRewardedVideoStatus::RewardedVideoAdSuccessfully)
	{
		SNotifyContainer::ShowNotify("OnPlayerEntryReward", FText::Format(NSLOCTEXT("DailyReward", "UArsenalComponent.OnPlayerEntryReward.AdModule.VideoNotReady", "Unfortunately, the advertising service is not available. Try again latter. {0}"), bPlayRewardedVideo));
	}

	//=======================================================
	return bPlayRewardedVideo == EPlayRewardedVideoStatus::RewardedVideoAdSuccessfully;
#endif
}
