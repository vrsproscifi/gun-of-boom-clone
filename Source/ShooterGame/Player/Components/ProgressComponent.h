﻿#pragma once

//======================================
#include "BasicPlayerComponent.h"

//======================================
#include "ProgressComponent/GameMatchInformation.h"
#include "ProgressComponent/MatchUpgradeResponse.h"

//======================================
#include "ArsenalComponent/MarketDeliveryRequest.h"
#include "ProgressComponent/RatingTopContainer.h"
//======================================
#include "ProgressComponent.generated.h"


//======================================
class UOperationRequest;
struct FGameMatchInformation;

//======================================
DECLARE_LOG_CATEGORY_EXTERN(LogProgressComponent, Log, All);

DECLARE_DYNAMIC_MULTICAST_DELEGATE_OneParam(FOnMatchResultsUpdate, const TArray<FGameMatchInformation>&, OutResults);
DECLARE_DYNAMIC_MULTICAST_DELEGATE_OneParam(FOnTopPlayers, const FRatingTopContainer&, OutResults);


//======================================
class UInAppPurchaseWrapper;

//======================================
UCLASS(Transient)
class UProgressComponent : public UBasicPlayerComponent
{
	GENERATED_BODY()
protected:
	UPROPERTY()
	int32 OnRequestMatchResultCalled;

	UPROPERTY(VisibleInstanceOnly)
		FGuid TargetMatchMemberId;

public:
	UProgressComponent();

	virtual bool IsAllowServerQueries() const override;

	UPROPERTY(BlueprintAssignable, Category = Progress)
	FOnMatchResultsUpdate OnMatchResultsUpdate;

	UPROPERTY(BlueprintAssignable, Category = Progress)
	FOnTopPlayers OnTopPlayers;

	FOnMatchUsedPremiumAccount OnApplyMatchAdsBonus;
	FOnMatchUsedPremiumAccount OnApplyMatchPremiumAccountBonus;

	UFUNCTION() void SendRequestAdditionalBonus(const FGuid& InMatchMemberId, const bool& InHasPremiumAccount, const int32& InTargetPremiumAccount);
	UFUNCTION() void SendRequestTopPlayers() const;
	UFUNCTION() void StartRequestTopPlayers() const;

	virtual void OnPlayRewardVideoSuccess(const FPlayRewardedVideoResponse& InResponse) override;


protected:
	/*! Событие вызываемое при успешной авторизации*/
	virtual void OnAuthorizationComplete() override;

	
	UPROPERTY(Transient, VisibleInstanceOnly) UOperationRequest* RequestTopPlayers;
	UPROPERTY(Transient, VisibleInstanceOnly) UOperationRequest* RequestMatchResult;
	UPROPERTY(Transient, VisibleInstanceOnly) UOperationRequest* RequestMatchResults;
	UPROPERTY(Transient, VisibleInstanceOnly) UOperationRequest* RequestApplyMatchPremiumAccountBonus;
	UPROPERTY(Transient, VisibleInstanceOnly) UOperationRequest* RequestApplyMatchAdsBonus;

	UPROPERTY(Transient, VisibleInstanceOnly) UOperationRequest* MarketDelivery;

	void OnRequestTopPlayers(const FRatingTopContainer& InRatingContainer);
	void OnRequestTopPlayers_Failed(const FRequestExecuteError& InError);

	UFUNCTION() void OnApplyMatchAdsBonusSuccessfully(const FMatchUpgradeResponse& InResponse);


	UFUNCTION() void OnRequestMatchUpgrade(const FMatchUpgradeResponse& InResponse);
	UFUNCTION() void OnRequestMatchUpgrade_PremiumAlwaysUsed();
	UFUNCTION() void OnRequestMatchUpgrade_PremiumNotFound();
	UFUNCTION() void OnRequestMatchUpgrade_MatchNotFound();
	UFUNCTION() void OnRequestMatchUpgrade_Failed(const FRequestExecuteError& InError);

	UFUNCTION() void SendRequestOrderValidation(const FMarketDeliveryRequest& InOrderRequest);
	UFUNCTION() void OnRequestOrderValidation(const FMarketDeliveryResponse& InOrderResponse);
	UFUNCTION() void OnRequestOrderValidation_Failed(const FRequestExecuteError& InError);


	UFUNCTION()	void OnRequestMatchResults(const TArray<FGameMatchInformation>& results);
	UFUNCTION() void OnRequestMatchResultsFailed(const FRequestExecuteError& InErrorData);

	UFUNCTION() void SendRequestMatchResults();
};