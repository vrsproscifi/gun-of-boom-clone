﻿//============================================
#include "ShooterGame.h"

//============================================
#include "ArsenalComponent.h"
#include "ArsenalComponent/PlayerInventoryItemUnlock.h"

//============================================
#include "Extensions/GameSingletonExtensions.h"

//============================================
#include "Engine/Network/LokaNetUserInterface.h"
#include "Engine/Http/RequestManager.h"

//============================================
#include "Player/BasicPlayerItem.h"
#include "Item/Product/ProductItemEntity.h"

//============================================
#include "ArsenalComponent/InAppPurchaseWrapper.h"

//======================================
#include "Notify/SMessageBox.h"
#include "Localization/TableBaseStrings.h"

//======================================
#include "Player/BasicPlayerController.h"
#include "GameMode/LobbyGameMode.h"

//=====================================
#include "ArsenalComponent/UsePlayerItemRequest.h"
#include "Entity/PlayerCaseEntity.h"
#include "Components/SDropedShopItem.h"
#include "ShooterGameAnalytics.h"

//=====================================
#include "Engine/Classes/Kismet/BlueprintPlatformLibrary.h"
#include "CaseProperty.h"
#include "WeaponPlayerItem.h"

//=====================================
//#include "CrashlyticsBlueprintLibrary.h"
#include "GooglePlayHelper/AndroidNotify.h"
#include "NotifySoundsData.h"
#include "Notify/SNotifyContainer.h"
#include "WeaponItemEntity.h"
#include "TArrayExtensions.h"
#include "Entity/PlayerDiscountEntity.h"
#include "DateTimeExtensions.h"
#include "CharacterItemEntity.h"
#include "ArmourItemEntity.h"
#include "EverydayAwardEntity.h"

//=====================================
#include "Lobby/Cases/SAvalibleCasesContainer.h"
#include "ArsenalComponent/InAppPurchaseWrapper.h"
#include "Input/SVideoRewardButton.h"

#define LOCTEXT_NAMESPACE "Arsenal"

DEFINE_LOG_CATEGORY(LogArsenalComponent);

UArsenalComponent::UArsenalComponent()
	: CachedHealth(0)
	, CachedArmour(0)
	, CachedAids(0)
	, CashedGrenades(0)
{
	//==========================================
	RequestLootData = CreateRequest(FOperationRequestServerHost::MasterClient, "Identity/RequestLootData", 0.250f, EOperationRequestTimerMethod::CallViaResponse, FTimerDelegate::CreateUObject(this, &UArsenalComponent::SendRequestLootData), 10, true);
	RequestLootData->BindArray<TArray<FPlayerInventoryItemModel>>(200).AddUObject(this, &UArsenalComponent::OnLootData);
	RequestLootData->OnLoseAttemps.BindUObject(this, &UArsenalComponent::OnBadNetworkConnection);


	//==========================================
	RequestProfilesData = CreateRequest(FOperationRequestServerHost::MasterClient, "Arsenal/GetPlayerProfile", 0.250f, EOperationRequestTimerMethod::CallViaResponse, FTimerDelegate::CreateUObject(this, &UArsenalComponent::SendRequestProfilesData), 10, true);
	RequestProfilesData->BindArray<TArray<FPlayerProfileModel>>(200).AddUObject(this, &UArsenalComponent::OnProfiles);
	RequestProfilesData->OnLoseAttemps.BindUObject(this, &UArsenalComponent::OnBadNetworkConnection);

	//==========================================
	RequestItemEquip = CreateRequest(FOperationRequestServerHost::MasterClient, "Arsenal/EquipItem");
	RequestItemEquip->BindObject<FPlayerProfileItemEquip>(200).AddUObject(this, &UArsenalComponent::OnItemEquip);
	RequestItemEquip->Bind(400).AddUObject(this, &UArsenalComponent::OnItemEquip_ProfileNotFound);
	RequestItemEquip->OnFailedResponse.BindUObject(this, &UArsenalComponent::OnItemEquip_Failed);

	//==========================================
	RequestItemUpgrade = CreateRequest(FOperationRequestServerHost::MasterClient, "Arsenal/UpgradeItem");
	RequestItemUpgrade->BindObject<FUpgradeItemResponse>(200).AddUObject(this, &UArsenalComponent::OnItemUpgrade);
	RequestItemUpgrade->OnFailedResponse.BindUObject(this, &UArsenalComponent::OnItemUpgrade_Failed);
	RequestItemUpgrade->BindObject<FGuid>(402).AddUObject(this, &UArsenalComponent::OnItemUpgrade_NotEnouthMoney);

	RequestItemUpgrade->Bind(404).AddUObject(this, &UArsenalComponent::OnItemUpgrade_NotFound);
	RequestItemUpgrade->Bind(409).AddUObject(this, &UArsenalComponent::OnItemUpgrade_RequestedInvalidLevel);
	RequestItemUpgrade->Bind(400).AddUObject(this, &UArsenalComponent::OnItemUpgrade_MaximumLevelReached);


	//==========================================
	RequestItemUnlock = CreateRequest(FOperationRequestServerHost::MasterClient, "Arsenal/UnlockItem");
	RequestItemUnlock->BindObject<FPlayerInventoryItemUnlocked>(200).AddUObject(this, &UArsenalComponent::OnUnlockItemSuccessfully);
	RequestItemUnlock->BindObject<int32>(402).AddUObject(this, &UArsenalComponent::OnUnlockItem_NotEnouthMoney);

	
	//==========================================
	RequestUsePlayerItem = CreateRequest(FOperationRequestServerHost::MasterClient, "Arsenal/UsePlayerItem");
	RequestUsePlayerItem->BindObject<FPlayerInventoryItemModel>(200).AddUObject(this, &UArsenalComponent::OnSendRequestUsePlayerItem);

	//==========================================
	MarketDelivery = CreateRequest(FOperationRequestServerHost::MasterClient, "Arsenal/OnMarketDelivery");
	MarketDelivery->BindObject<FMarketDeliveryResponse>(200).AddUObject(this, &UArsenalComponent::OnMarketDelivery);
	MarketDelivery->OnFailedResponse.BindUObject(this, &UArsenalComponent::OnMarketDelivery_Failed);

	//==========================================
	MarketExchange = CreateRequest(FOperationRequestServerHost::MasterClient, "Arsenal/OnMarketExchange");
	MarketExchange->BindObject<FMarketDeliveryResponse>(200).AddUObject(this, &UArsenalComponent::OnMarketExchange);
	MarketExchange->BindObject<FGameItemCost>(402).AddUObject(this, &UArsenalComponent::OnMarketExchange_NotEnouthMoney);
	MarketExchange->OnFailedResponse.BindUObject(this, &UArsenalComponent::OnMarketExchange_Failed);

	//==========================================
	RequestBonusCase = CreateRequest(FOperationRequestServerHost::MasterClient, "Arsenal/RequestBonusCase");
	RequestBonusCase->BindObject<FDropedCaseModel>(200).AddUObject(this, &UArsenalComponent::OnRequestOpenPlayerCase);
	RequestBonusCase->Bind(404).AddUObject(this, &UArsenalComponent::OnRequestOpenPlayerCase_NotFoundCase);
	RequestBonusCase->Bind(409).AddUObject(this, &UArsenalComponent::OnRequestOpenPlayerCase_NotFoundEntity);
	RequestBonusCase->Bind(400).AddUObject(this, &UArsenalComponent::OnRequestOpenPlayerCase_NotAllowUse);

	//==========================================
	RequestPlayerCases = CreateRequest(FOperationRequestServerHost::MasterClient, "Arsenal/GetPlayerCases", 5, EOperationRequestTimerMethod::CallViaInterval, FTimerDelegate::CreateUObject(this, &UArsenalComponent::SendRequestPlayerCases));
	RequestPlayerCases->BindArray<TArray<FPlayerCaseModel>>(200).AddUObject(this, &UArsenalComponent::OnRequestPlayerCases);
	
	//==========================================
	RequestPlayerCaseOpen = CreateRequest(FOperationRequestServerHost::MasterClient, "Arsenal/TakeCase");
	RequestPlayerCaseOpen->BindObject<FDropedCaseModel>(200).AddUObject(this, &UArsenalComponent::OnRequestOpenPlayerCase);
	RequestPlayerCaseOpen->Bind(404).AddUObject(this, &UArsenalComponent::OnRequestOpenPlayerCase_NotFoundCase);
	RequestPlayerCaseOpen->Bind(409).AddUObject(this, &UArsenalComponent::OnRequestOpenPlayerCase_NotFoundEntity);
	RequestPlayerCaseOpen->Bind(400).AddUObject(this, &UArsenalComponent::OnRequestOpenPlayerCase_NotAllowUse);

	RequestPlayerCaseOpen->OnFailedResponse.BindUObject(this, &UArsenalComponent::OnRequestOpenPlayerCase_Failed);

	//==========================================
	RequestPlayerDiscounts = CreateRequest(FOperationRequestServerHost::MasterClient, "Arsenal/GetPlayerDiscounts", 30, EOperationRequestTimerMethod::CallViaIntervalOrResponse, FTimerDelegate::CreateUObject(this, &UArsenalComponent::SendRequestPlayerDiscounts), 10, false);
	RequestPlayerDiscounts->BindArray<TArray<FPlayerDiscountModel>>(200).AddUObject(this, &UArsenalComponent::OnRequestPlayerDiscounts);
	RequestPlayerDiscounts->OnFailedResponse.BindUObject(this, &UArsenalComponent::OnRequestPlayerDiscounts_Failed);
}

void UArsenalComponent::OnPlayRewardVideoSuccess(const FPlayRewardedVideoResponse& InResponse)
{
	//ShowErrorMessage("UArsenalComponent::OnPlayRewardVideoSuccess", FString::Printf(TEXT("UArsenalComponent::OnPlayRewardVideoSuccess | InEntityCategory: %s / %d | InEntityId: %d"), *InEntityCategory, InEntityCategory.Equals("Cases", ESearchCase::IgnoreCase), *InEntityId));

	if(InResponse.Category.Equals("GetFreeDayliCase", ESearchCase::IgnoreCase))
	{
		FGuid EntityId;
		if(FGuid::Parse(InResponse.Entity, EntityId))
		{
			SendRequestOpenPlayerCase(EntityId);
		}
	}
	else if (InResponse.Category.Equals("GetAdditionalBonusCase", ESearchCase::IgnoreCase))
	{
		//======================================
		FGuid EntityId;

		//======================================
		if (FGuid::Parse(InResponse.Entity, EntityId))
		{
			if (auto CaseEntity = GetCaseById(EntityId))
			{
				CaseEntity->UseBonusCase();
				SendRequestBonusCase(EntityId);
			}
		}
	}
}

#pragma region PlayerDiscountEntity


bool UArsenalComponent::RemoveDiscountCoupone(const FPlayerDiscountModel& InDiscountCoupone)
{
	//==================================
	auto entity = GetDiscountCouponeById(InDiscountCoupone.EntityId);

	//==================================
	if (entity == nullptr)
	{
		return false;
	}

	//==================================
	auto RemovedIndex = DiscountCoupones.Remove(entity);

	//==================================
	return !!RemovedIndex;
}


UPlayerDiscountEntity* UArsenalComponent::AddOrUpdateDiscountCoupone(const FPlayerDiscountModel& InDiscountCoupone)
{
	//==================================
	auto entity = GetDiscountCouponeById(InDiscountCoupone.EntityId);

	//==================================
	if (entity == nullptr)
	{
		DiscountCoupones.Add(entity = NewObject<UPlayerDiscountEntity>(this));
	}

	//==================================
	checkf(entity, TEXT("NewObject<UPlayerDiscountEntity> was nullptr"));

	//==================================
	entity->Initialize(InDiscountCoupone);

	//==================================
	return entity;
}

const TArray<UPlayerDiscountEntity*>& UArsenalComponent::GetDiscountCoupones() const
{
	return DiscountCoupones;
}

const UPlayerDiscountEntity* UArsenalComponent::GetDiscountCouponeById(const FGuid& InDiscountCouponeId) const
{
	const auto r = DiscountCoupones.FindByPredicate([&](const UPlayerDiscountEntity* InEntity)
	{
		return UBasicPlayerEntity::IsEqual(InEntity, InDiscountCouponeId);
	});

	if (r)
	{
		return *r;
	}

	return nullptr;
}

UPlayerDiscountEntity* UArsenalComponent::GetDiscountCouponeById(const FGuid& InDiscountCouponeId)
{
	const auto r = DiscountCoupones.FindByPredicate([&](const UPlayerDiscountEntity* InEntity)
	{
		return UBasicPlayerEntity::IsEqual(InEntity, InDiscountCouponeId);
	});

	if (r)
	{
		return *r;
	}

	return nullptr;
}

void UArsenalComponent::SendRequestPlayerDiscounts() 
{
	RequestPlayerDiscounts->GetRequest();
}

void UArsenalComponent::StartRequestPlayerDiscounts() 
{
	StartTimer(RequestPlayerDiscounts);
}

void UArsenalComponent::UpdateCurrentDiscountCoupone(const TArray<FPlayerDiscountModel>& InDiscountCoupones)
{	
	//====================================
	const auto PlayerLevel = GetPlayerLevel();

	//---------------------------------------------------------
	auto RandomDiscountCoupone = FArrayExtensions::TakeRandomByPredicate(InDiscountCoupones, [&, PlayerLevel](const FPlayerDiscountModel& InPlayerDiscount)
	{
		if (const auto instance = FGameSingletonExtensions::FindItemById(InPlayerDiscount.ModelId))
		{
			return PlayerLevel >= instance->GetItemRequiredLevel();
		}
		return false;
	});

	//---------------------------------------------------------
	if (RandomDiscountCoupone)
	{
		CurrentDiscountCoupone = *RandomDiscountCoupone;
		AddOrUpdateDiscountCoupone(CurrentDiscountCoupone);
	}
}

void UArsenalComponent::UpdateCurrentDiscountCouponeIfExist(const TArray<FPlayerDiscountModel>& InDiscountCoupones)
{	
	//====================================
	const auto PlayerLevel = GetPlayerLevel();

	//-------------------------------------------
	auto bExistCurrentDiscountCoupone = InDiscountCoupones.FindByPredicate([&](const FPlayerDiscountModel& InCoupone)
	{
		return InCoupone.EntityId == CurrentDiscountCoupone.EntityId || InCoupone.ModelId == CurrentDiscountCoupone.ModelId;
	});

	//-------------------------------------------
	if (bExistCurrentDiscountCoupone == nullptr)
	{
		//-------------------------------------------
		RemoveDiscountCoupone(CurrentDiscountCoupone);

		//-------------------------------------------
		auto RandomDiscountCoupone = FArrayExtensions::TakeRandomByPredicate(InDiscountCoupones, [&, PlayerLevel](const FPlayerDiscountModel& InPlayerDiscount)
		{
			if (const auto instance = FGameSingletonExtensions::FindItemById(InPlayerDiscount.ModelId))
			{
				return PlayerLevel >= instance->GetItemRequiredLevel();
			}
			return false;
		});

		if (RandomDiscountCoupone)
		{
			CurrentDiscountCoupone = *RandomDiscountCoupone;
			AddOrUpdateDiscountCoupone(CurrentDiscountCoupone);
		}
	}
}


void UArsenalComponent::OnRequestPlayerDiscounts(const TArray<FPlayerDiscountModel>& InDiscountCoupones) 
{
	//====================================
	if (CurrentDiscountCoupone.EntityId.IsValid() == false || CurrentDiscountCoupone.ModelId == 0)
	{
		UpdateCurrentDiscountCoupone(InDiscountCoupones);
	}
	else
	{
		UpdateCurrentDiscountCouponeIfExist(InDiscountCoupones);
	}

	//====================================
	ResetPreviewActiveDiscountCoupones();

	//====================================
	UpdateActiveDiscountCoupones(InDiscountCoupones);

	//====================================
	PreviewDiscountCoupones = InDiscountCoupones;
}

void UArsenalComponent::UpdateActiveDiscountCoupones(const TArray<FPlayerDiscountModel>& InDiscountCoupones)
{
	for (const auto &c : InDiscountCoupones)
	{
		if (const auto instance = FGameSingletonExtensions::FindItemById(c.ModelId))
		{
			////====================================
			const auto EndDiscountDate = FDateTime::FromUnixTimestamp(c.EndDiscountDate);

			//====================================
			instance->SetActiveDiscount(c.Coupone, EndDiscountDate);
		}
	}
}

void UArsenalComponent::ResetPreviewActiveDiscountCoupones()
{
	for (const auto &c : PreviewDiscountCoupones)
	{
		if (const auto instance = FGameSingletonExtensions::FindItemById(c.ModelId))
		{
			instance->SetActiveDiscount(EProductDiscountCoupone::None, FDateTime::UtcNow());
		}
	}
}


void UArsenalComponent::OnRequestPlayerDiscounts_Failed(const FRequestExecuteError& InError)
{
	//====================================
	FShooterGameAnalytics::RecordDesignEvent("UI:Lobby:PlayerDiscounts:Fail:Code" + InError.GetStatusCode());

	//====================================
	UE_LOG(LogArsenalComponent, Display, TEXT("> [UArsenalComponent::OnRequestPlayerDiscounts_Failed][InError: %s]"), *InError.ToString());

	//====================================
	FShooterGameAnalytics::RecordErrorEvent(EErrorSeverity::warning, "OnRequestPlayerDiscounts::Failed");
}


#pragma endregion


#pragma region PlayerCaseEntity

void UArsenalComponent::SendRequestPlayerCases() const
{
	if (FPlatformProperties::IsServerOnly() == false)
	{
		RequestPlayerCases->GetRequest();
	}
}

void UArsenalComponent::StartRequestPlayerCases()
{
	UE_LOG(LogArsenalComponent, Display, TEXT("> [UArsenalComponent::StartRequestPlayerCases][IsServerOnly: %d]"), FPlatformProperties::IsServerOnly());

	if (FPlatformProperties::IsServerOnly() == false)
	{
		StartTimer(RequestPlayerCases);
	}
}

void UArsenalComponent::OnOpenDayliRewardClicked(const int32& InRewardDay) const
{
	//====================================
	const auto DayliRewardInstance = FGameSingletonExtensions::FindEverydayAwardById(InRewardDay);

	//====================================
	if (DayliRewardInstance == nullptr)
	{
		SNotifyContainer::ShowNotify("OnPlayerEntryReward", NSLOCTEXT("DailyReward", "UArsenalComponent.OnPlayerEntryReward.DayliRewardInstance.WasNullptr", "DayliRewardInstance was nullptr"));
		return;
	}

	//====================================
	const auto& DayliRewardCaseId = DayliRewardInstance->GetRewardCaseId();

	//====================================
	const auto PlayerDayliRewardCaseEntity = GetCaseByModelId(DayliRewardCaseId);
	if (PlayerDayliRewardCaseEntity == nullptr)
	{
		SNotifyContainer::ShowNotify("OnPlayerEntryReward", NSLOCTEXT("DailyReward", "UArsenalComponent.OnPlayerEntryReward.PlayerDayliRewardCaseEntity.WasNullptr", "PlayerDayliRewardCaseEntity was nullptr"));
		return;
	}

	//====================================
	PlayRewardVideo(FPlayRewardedVideoRequest("GetAdditionalBonusCase", PlayerDayliRewardCaseEntity->GetEntityId().ToString()));
}

void UArsenalComponent::SendRequestOpenPlayerCase(const FGuid& InCaseId) const
{
	//UCrashlyticsBlueprintLibrary::WriteLog("UArsenalComponent:SendRequestOpenPlayerCase");
	RequestPlayerCaseOpen->SendRequestObject<FGuid>(InCaseId);
}

void UArsenalComponent::OnRequestPlayerCases(const TArray<FPlayerCaseModel>& InCases)
{
	//====================================
	UE_LOG(LogArsenalComponent, Display, TEXT("> [UArsenalComponent::OnRequestPlayerCases][InCases: %d / Cases: %d]"), InCases.Num(), Cases.Num());
	
	//====================================
	for (const auto &c : InCases)
	{
		AddOrUpdateCase(c);
	}

	OnChestsUpdate.Broadcast(Cases);
}

UPlayerCaseEntity* UArsenalComponent::AddOrUpdateCase(const FPlayerCaseModel& InCase)
{
	//==================================
	auto entity = GetCaseById(InCase.EntityId);

	//==================================
	if(entity == nullptr)
	{
		Cases.Add(entity = NewObject<UPlayerCaseEntity>(this));
	}

	//==================================
	entity->Initialize(InCase);

	//==================================
	return entity;
}

const UPlayerCaseEntity* UArsenalComponent::GetCaseByModelId(const int32& InCaseModelId) const
{
	const auto r = Cases.FindByPredicate([&](const UPlayerCaseEntity* InEntity)
	{
		return UBasicPlayerEntity::IsEqualModelId(InEntity, InCaseModelId);
	});

	if (r)
	{
		return *r;
	}

	return nullptr;
}

UPlayerCaseEntity* UArsenalComponent::GetCaseByModelId(const int32& InCaseModelId)
{
	const auto r = Cases.FindByPredicate([&](const UPlayerCaseEntity* InEntity)
	{
		return UBasicPlayerEntity::IsEqualModelId(InEntity, InCaseModelId);
	});

	if (r)
	{
		return *r;
	}

	return nullptr;
}

const UPlayerCaseEntity* UArsenalComponent::GetCaseById(const FGuid& InCaseId) const
{
	const auto r = Cases.FindByPredicate([&](const UPlayerCaseEntity* InEntity)
	{
		return UBasicPlayerEntity::IsEqual(InEntity, InCaseId);
	});

	if(r)
	{
		return *r;
	}

	return nullptr;
}

const UPlayerCaseEntity* UArsenalComponent::GetCaseByModelOrId(const FGuid& InCaseId, const int32& InCaseModelId) const
{
	const auto r = Cases.FindByPredicate([&](const UPlayerCaseEntity* InEntity)
	{
		return UBasicPlayerEntity::IsEqual(InEntity, InCaseId) || UBasicPlayerEntity::IsEqualModelId(InEntity, InCaseModelId);
	});

	if (r)
	{
		return *r;
	}

	return nullptr;
}

UPlayerCaseEntity* UArsenalComponent::GetCaseByModelOrId(const FGuid& InCaseId, const int32& InCaseModelId)
{
	const auto r = Cases.FindByPredicate([&](const UPlayerCaseEntity* InEntity)
	{
		return UBasicPlayerEntity::IsEqual(InEntity, InCaseId) || UBasicPlayerEntity::IsEqualModelId(InEntity, InCaseModelId);
	});

	if (r)
	{
		return *r;
	}

	return nullptr;
}

UPlayerCaseEntity* UArsenalComponent::GetCaseById(const FGuid& InCaseId)
{
	const auto r = Cases.FindByPredicate([&](const UPlayerCaseEntity* InEntity)
	{
		return UBasicPlayerEntity::IsEqual(InEntity, InCaseId);
	});

	if (r)
	{
		return *r;
	}

	return nullptr;
}

void UArsenalComponent::OnRequestOpenPlayerCase_Failed(const FRequestExecuteError& InError)
{
	//====================================
	FShooterGameAnalytics::RecordDesignEvent("UI:Lobby:OpenCase:Fail:Code" + InError.GetStatusCode());

	//====================================
	UE_LOG(LogArsenalComponent, Display, TEXT("> [UArsenalComponent::OnRequestOpenPlayerCase_Failed][InError: %s]"), *InError.ToString());

	//====================================
	FShooterGameAnalytics::RecordErrorEvent(EErrorSeverity::warning, "OnRequestOpenPlayerCase::Failed");

	//===============================================
	//QueueBegin(SNotifyContainer, "OnRequestOpenPlayerCase_Failed")
	//	SNotifyContainer::Get()->SetContent(FText::Format(NSLOCTEXT("Notify", "Notify.OnRequestOpenPlayerCase_Failed", "Could not open case. Error code: {0}"), FText::AsNumber(InError.Status)));
	//	SNotifyContainer::Get()->ToggleWidget(true);
	//QueueEnd
}

void UArsenalComponent::OnRequestOpenPlayerCase_NotFoundEntity()
{
	FShooterGameAnalytics::RecordErrorEvent(EErrorSeverity::warning, "OnRequestOpenPlayerCase::NotFoundEntity");

	//QueueBegin(SNotifyContainer, "OnRequestOpenPlayerCase_Failed")
	//	SNotifyContainer::Get()->SetContent(NSLOCTEXT("Notify", "Notify.OnRequestOpenPlayerCase_NotFoundEntity", "Could not open case. Error code: #1"));
	//	SNotifyContainer::Get()->ToggleWidget(true);
	//QueueEnd
}

void UArsenalComponent::OnRequestOpenPlayerCase_NotFoundCase()
{
	FShooterGameAnalytics::RecordErrorEvent(EErrorSeverity::warning, "OnRequestOpenPlayerCase::NotFoundCase");

	//QueueBegin(SNotifyContainer, "OnRequestOpenPlayerCase_Failed")
	//	SNotifyContainer::Get()->SetContent(NSLOCTEXT("Notify", "Notify.OnRequestOpenPlayerCase_NotFoundCase", "Could not open case. Error code: #2"));
	//	SNotifyContainer::Get()->ToggleWidget(true);
	//QueueEnd
}

void UArsenalComponent::OnRequestOpenPlayerCase_NotAllowUse()
{
	FShooterGameAnalytics::RecordErrorEvent(EErrorSeverity::warning, "OnRequestOpenPlayerCase::NotAllowUse");

	QueueBegin(SNotifyContainer, "OnRequestOpenPlayerCase_Failed")
		SNotifyContainer::Get()->SetContent(NSLOCTEXT("Notify", "Notify.OnRequestOpenPlayerCase_NotAllowUse", "Could not open case. The reward is not yet available, try again later."));
		SNotifyContainer::Get()->ToggleWidget(true);
	QueueEnd

}

void UArsenalComponent::OnRequestOpenPlayerCase(const FDropedCaseModel& InDropedCase)
{
	//UCrashlyticsBlueprintLibrary::WriteLog("UArsenalComponent:OnRequestOpenPlayerCase");

	//=======================================
	OnRequestPlayerDataUpdate.Broadcast();


	//=======================================
	const auto PlayerCaseEntity = GetCaseByModelOrId(InDropedCase.EntityId, InDropedCase.CaseId);
	if (IsValidObject(PlayerCaseEntity))
	{
		const auto CaseEntityProperty = PlayerCaseEntity->GetCaseEntityProperty();

		//UCrashlyticsBlueprintLibrary::LogFbCustomEventKeyValue(TEXT("Open_Chest"), TEXT("Chest"), PlayerCaseEntity->GetName());
		
		//	Не показывать список выпавших предметов, если это ежедневный кейс
		if(CaseEntityProperty && CaseEntityProperty->CaseType == EGameCaseType::EveryDayAward)
		{
			return;
		}
	}

	QueueBegin(SMessageBox, *FString::Printf(TEXT("OpenPlayerCase_%d"), InDropedCase.CaseId), DCI = InDropedCase)

	auto ItemsContainer = SNew(SScrollBox).Orientation(Orient_Horizontal).ScrollBarVisibility(EVisibility::Collapsed);

	//====================================
	const auto CaseEntity = AddOrUpdateCase(DCI);

	//====================================
	UE_LOG(LogArsenalComponent, Display, TEXT("> [UArsenalComponent::OnRequestOpenPlayerCase][InDropedCase: %s / %d][Items: %d][CaseEntity: %d]"), *DCI.EntityId.ToString(), DCI.CaseId, DCI.Items.Num(), CaseEntity != nullptr);

	TMap<UBasicItemEntity*, int32> ItemsMap;

	for (auto di : DCI.Items)
	{
		if (const auto item = FGameSingletonExtensions::FindItemById(di.ItemModelId))
		{
			ItemsMap.Add(item, di.Amount);
		}
	}

	ItemsMap.KeySort([](const UBasicItemEntity& InA, const UBasicItemEntity& InB)
	{
		return InA.GetItemProperty().DisplayPosition < InB.GetItemProperty().DisplayPosition;
	});

	for (auto ItemPair : ItemsMap)
	{
		ItemsContainer->AddSlot()
		[
			SNew(SBox).WidthOverride(400).HeightOverride(400)
			[
				SNew(SDropedShopItem, ItemPair.Key).Num(ItemPair.Value)
			]
		];
	}

	if(!!DCI.Items.Num())
	{
		StartTimer(RequestLootData);
	}

	TSharedRef<SVideoRewardButton> VideoRewardButton = SNew(SVideoRewardButton)
		.Visibility(CaseEntity->AllowGetBonusCase() ? EVisibility::Visible : EVisibility::Collapsed)
		.OnClicked_Lambda([&, ce = CaseEntity]()
	{
		if (IsValidObject(ce))
		{
			if (ce->AllowGetBonusCase())
			{
				PlayRewardVideo(FPlayRewardedVideoRequest("GetAdditionalBonusCase", ce->GetEntityId().ToString()));
			}
		}

		SMessageBox::Get()->ToggleWidget(false);
		return FReply::Handled();
	});	

	//====================================
	SMessageBox::Get()->SetHeaderText(NSLOCTEXT("Cases", "UArsenalComponent.OnRequestOpenPlayerCase.Title", "Droped items"));
	SMessageBox::Get()->SetContent
	(
		SNew(SVerticalBox)
		+ SVerticalBox::Slot()
		[
			ItemsContainer
		]
		+ SVerticalBox::Slot().AutoHeight().HAlign(HAlign_Center)
		[
			VideoRewardButton
		]
	);

	SMessageBox::Get()->SetButtonsText();
	SMessageBox::Get()->SetEnableClose(true);
	SMessageBox::Get()->ToggleWidget(true);

	QueueEnd
}

#pragma endregion

void UArsenalComponent::SendRequestBonusCase(const FGuid& InPlayerCaseId)
{	
	//====================================
	UE_LOG(LogArsenalComponent, Display, TEXT("> [UArsenalComponent::SendRequestBonusCase][%s]"), *InPlayerCaseId.ToString());

	//====================================
	if (RequestBonusCase != nullptr)
	{
		RequestBonusCase->SendRequestObject(InPlayerCaseId);
	}
}

void UArsenalComponent::SendRequestUsePlayerItem(const UBasicPlayerItem* inItem, const int32& InAmount) const
{
	//====================================
	UE_LOG(LogArsenalComponent, Display, TEXT("> [UArsenalComponent::SendRequestUsePlayerItem]%s[InAmount: %d]"), *GetEntityName(inItem), InAmount);
	
	//====================================
	if(inItem)
	{
		FUsePlayerItemRequest request;
		request.EntityId = inItem->GetEntityId();
		request.Amount = InAmount;
		RequestUsePlayerItem->SendRequestObject(request);
	}
}


void UArsenalComponent::OnSendRequestUsePlayerItem(const FPlayerInventoryItemModel& InResponse) const
{
	//====================================
	auto item = FindItem(InResponse.EntityId, InResponse.ModelId);

	//====================================
	UE_LOG(LogArsenalComponent, Display, TEXT("> [UArsenalComponent::OnSendRequestUsePlayerItem][%s / %d]%s"), *InResponse.EntityId.ToString(), InResponse.ModelId, *GetEntityName(item));

	//====================================
	if(item)
	{
		item->SetAmount(InResponse.Amount);
	}

}

void UArsenalComponent::OnSendRequestUsePlayerItem_Failed(const FRequestExecuteError& InError) const
{	
	//====================================
	FShooterGameAnalytics::RecordDesignEvent("UI:Lobby:UsePlayerItem:Fail:Code" + InError.GetStatusCode());

	FShooterGameAnalytics::RecordErrorEvent(EErrorSeverity::warning, "OnSendRequestUsePlayerItem::Failed");

	UE_LOG(LogArsenalComponent, Error, TEXT("> [UArsenalComponent::OnSendRequestUsePlayerItem_Failed] %s"), *InError.ToString());


}


void UArsenalComponent::OnBadNetworkConnection(const FRequestExecuteError& InErrorData)
{	
#if !UE_SERVER
	//======================================================
	//UCrashlyticsBlueprintLibrary::SetField("BadNetworkConnections", ++FShooterGameAnalytics::BadNetworkConnections);

	//UCrashlyticsBlueprintLibrary::WriteLog("UArsenalComponent:OnBadNetworkConnection");
	FShooterGameAnalytics::RecordDesignEvent(FString::Printf(TEXT("UI:Lobby:%s:Fail:Code%d"), *InErrorData.Operation, InErrorData.Status));

	if ((InErrorData.IsTimeout() || InErrorData.IsBadGateway()) && WITH_EDITOR == 0)
	{
		FShooterGameAnalytics::RecordErrorEvent(EErrorSeverity::critical, "UArsenalComponent::OnBadNetworkConnection");
		UE_LOG(LogArsenalComponent, Error, TEXT("> [UArsenalComponent::OnBadNetworkConnection] \nError: %s"), *InErrorData.ToString());

		ShowRestarGameErrorMessage
		(
			"UArsenalComponent::OnBadNetworkConnection",
			NSLOCTEXT("Network", "Network.OnBadNetworkConnection.Title", "Problem with internet connection"),
			FText::Format(NSLOCTEXT("Network", "Network.OnBadNetworkConnection.Message", "The request to the server could not be completed.\n The problem with the Internet connection.\n Do you want return to the lobby?"), FText::FromString(InErrorData.ToString()))
		);
	}
#endif
}

void UArsenalComponent::OnMarketRequestBuy(const UBaseProductItemEntity* inEntity)
{
#if UE_BUILD_DEVELOPMENT
	GEngine->AddOnScreenDebugMessage(-1, 5.f, FColor::Red, TEXT("[OnMarketRequestBuy]"));

#endif


	//UCrashlyticsBlueprintLibrary::WriteLog("UArsenalComponent:OnMarketRequestBuy");

	if (const auto entity = GetValidObject(inEntity))
	{
		FShooterGameAnalytics::RecordDesignEvent("Purchase:Market");

		const auto category = entity->GetProductCategory();
		if(category == EProductItemCategory::Cases)
		{
			FShooterGameAnalytics::RecordDesignEvent("Purchase:Cases");
			//if(entity->IsCase() == false)
			//{
			//	ShowErrorMessage("OnArsenalRequestBuy_Case", TEXT("[UArsenalComponent::OnMarketRequestBuy][Case is not case]"));
			//}
			OnArsenalRequestBuyItem(entity);
		}
		else if (category == EProductItemCategory::Money)
		{	
			FShooterGameAnalytics::RecordDesignEvent("Purchase:Money");
			SendMarketExchangeRequest(entity->GetModelName());
		}
		else if (const auto product = GetValidObjectAs<UProductItemEntity>(entity))
		{
			FShooterGameAnalytics::RecordDesignEvent("Purchase:Donate");

			const auto cost = product->GetItemCost();

			if (product->IsInAppPurchase())
			{
				auto TargetProductId = product->GetTargetProductId();
#if UE_BUILD_DEVELOPMENT
				GEngine->AddOnScreenDebugMessage(-1, 5.f, FColor::Red, FString::Printf(TEXT("[OnMarketRequestBuy][%s]"), *TargetProductId));

#endif

				OnMakeInAppPurchase(TargetProductId);
			}
			else
			{
				ShowErrorMessage("OnArsenalRequestBuy_ProductCategory", FString::Printf(TEXT("Payment failed. Failed to get product information #3.1\n More information about the error:\n [Item: %s / %d][Invlid ProductCategory: %d]"), *entity->GetName(), entity->GetModelId(), static_cast<int32>(category)));
				FShooterGameAnalytics::RecordErrorEvent(EErrorSeverity::warning, "Payment failed. Failed to get product information #3.1");
			}

			FShooterGameAnalytics::AddBusinessEvent(product->GetProductProperty().ProductId, product->GetName(), product->GetProductCategoryName(), cost.CurrencyAsISO(), cost.AmountAsCents());
		}
		else
		{
			ShowErrorMessage("OnArsenalRequestBuy_Propduct", TEXT("Payment failed. Failed to get product information #2.1"));
			FShooterGameAnalytics::RecordErrorEvent(EErrorSeverity::warning, "Payment failed. Failed to get product information #2.1");
		}
	}
	else
	{
		ShowErrorMessage("OnArsenalRequestBuy_Enity", TEXT("Payment failed. Failed to get product information #1.1"));
		FShooterGameAnalytics::RecordErrorEvent(EErrorSeverity::warning, "Payment failed. Failed to get product information #1.1");
	}
}

void UArsenalComponent::OnArsenalRequestBuyItem(const UBasicItemEntity* InEntity) const
{
	//===============================
	FShooterGameAnalytics::RecordDesignEvent("Purchase:Arsenal");

	//===============================
	const auto ItemTypeName = InEntity->GetItemTypeName();
	FShooterGameAnalytics::RecordDesignEvent(FString::Printf(TEXT("Purchase:%s"), *ItemTypeName));

	//===============================
	const auto ItemTypeGroupName = InEntity->GetItemTypeGroupName();
	FShooterGameAnalytics::RecordDesignEvent(FString::Printf(TEXT("Purchase:%s"), *ItemTypeGroupName));

	//===============================
	FPlayerInventoryItemUnlock request;
	request.ModelId = InEntity->GetModelId();
	request.SetSlotId = InEntity->GetItemType();
	request.Amount = 1;

	//===============================
	FShooterGameAnalytics::AddResourceEvent(EResourceFlowType::Sink, InEntity->GetItemCost().CurrencyAsISO(), InEntity->GetItemCost().Amount, "IAP", FString::FromInt(InEntity->GetItemModelId()));
	
	//===============================
	RequestItemUnlock->SendRequestObject(request);
}

void UArsenalComponent::OnArsenalRequestBuy(const UBasicPlayerItem* inItem)
{
	if (auto item = GetValidObject(inItem))
	{
		if (const auto entity = item->GetEntityBase())
		{
			OnArsenalRequestBuyItem(entity);
		}
		else
		{
			ShowErrorMessage("OnArsenalRequestBuy_Enity", FString::Printf(TEXT("Payment failed. Failed to get product information #2.2\n More information about the error: \n[Item: %s / model: %d]"), *item->GetEntityId().ToString(), item->GetModelId()));
			FShooterGameAnalytics::RecordErrorEvent(EErrorSeverity::warning, "Payment failed. Failed to get product information #2.2");
		}
	}
	else
	{
		ShowErrorMessage("OnArsenalRequestBuy_Item", TEXT("Payment failed. Failed to get product information #1.2"));
		FShooterGameAnalytics::RecordErrorEvent(EErrorSeverity::warning, "Payment failed. Failed to get product information #1.2");
	}
}

void UArsenalComponent::OnArsenalRequestEquip(const UBasicPlayerItem* inItem)
{
	if (auto item = GetValidObject(inItem))
	{
		if (const auto entity = item->GetEntityBase())
		{
			//===============================
			FPlayerProfileItemEquip request;
			request.ItemId = item->GetEntityId();
			request.SetSlotId = entity->GetItemType();

			//===============================
			FShooterGameAnalytics::RecordDesignEvent("UI:Lobby:Arsenal:Equip");

			//===============================
			SendRequestItemEquip(request);
		}
		else
		{
			SNotifyContainer::ShowNotify("Notify.OnArsenalRequestEquip", NSLOCTEXT("Notify", "Notify.OnArsenalRequestEquip.NullEntity", "Could not equip item. Error retrieving item information #2"));
			FShooterGameAnalytics::RecordErrorEvent(EErrorSeverity::warning, "Could not equip item. Error retrieving item information #2");
		}
	}
	else
	{
		SNotifyContainer::ShowNotify("Notify.OnArsenalRequestEquip", NSLOCTEXT("Notify", "Notify.OnArsenalRequestEquip.NullItem", "Could not equip item. Error retrieving item information #1"));
		FShooterGameAnalytics::RecordErrorEvent(EErrorSeverity::warning, "Could not equip item. Error retrieving item information #1");
	}
}

void UArsenalComponent::OnMarketRequestUpgrade(const UBasicPlayerItem* inItem, const int32& InLevel)
{
	if (const auto item = GetValidObject(inItem))
	{
		if (item->IsValidEntityId())
		{

			FUpgradeItemResponse r;
			r.EntityId = item->GetEntityId();
			r.Level = InLevel;

			RequestItemUpgrade->SendRequestObject(r);

			//===============================
			const auto entity = item->GetEntityBase();
			if (entity)
			{
				const auto ItemTypeName = entity->GetItemTypeName();
				FShooterGameAnalytics::RecordDesignEvent(FString::Printf(TEXT("Purchase:Up_%s"), *ItemTypeName));

				//===============================
				const auto ItemTypeGroupName = entity->GetItemTypeGroupName();
				FShooterGameAnalytics::RecordDesignEvent(FString::Printf(TEXT("Purchase:Up_%s"), *ItemTypeGroupName));
			}

			//===============================
			FShooterGameAnalytics::RecordDesignEvent("Purchase:Upgrade");

			//==========================================================
			if (auto prop = item->GetItemProperty())
			{
				auto modifications = prop->Modifications;
				auto modification = FBasicItemProperty::GetSafeModification(modifications, item->GetLevel());
				if (modification)
				{
					FShooterGameAnalytics::AddResourceEvent(EResourceFlowType::Sink, modification->Cost.CurrencyAsISO(), modification->Cost.Amount, "Upgrade", FString::FromInt(item->GetModelId()));
				}
			}
		}
		else
		{
			OnArsenalRequestBuy(inItem);
		}
	}
	else
	{
		ShowErrorMessage("OnMarketRequestUpgrade_Item", TEXT("Could not upgrade item. Error retrieving item information #1."));
		FShooterGameAnalytics::RecordErrorEvent(EErrorSeverity::warning, "Could not upgrade item. Error retrieving item information #1.");
	}
}

void UArsenalComponent::OnItemUpgrade(const FUpgradeItemResponse& InResponse)
{	
	//=======================================
	OnRequestPlayerDataUpdate.Broadcast();

	//=======================================
	//ShowErrorMessage("OnItemUpgrade", TEXT("OnItemUpgrade"));
	if (auto item = FindItem(InResponse.EntityId))
	{
		const auto Event = FString::Printf(TEXT("Arsenal:Up:Item_%d:From_%d"), item->GetModelId(), item->GetLevel());
		FShooterGameAnalytics::RecordDesignEvent(Event);
		item->SetLevel(InResponse.Level);

		if (auto itemEntity = item->GetEntityBase())
		{
			const auto& ModsArray = itemEntity->GetItemProperty().Modifications;
			if (ModsArray.Num() && ModsArray.IsValidIndex(InResponse.Level))
			{
				const auto& LevelMod = ModsArray[InResponse.Level];
				//UCrashlyticsBlueprintLibrary::LogFbUpgradeItem(item->GetEntityId().ToString(), itemEntity->GetItemTypeName(), itemEntity->GetName(), LevelMod.Cost.CurrencyAsISO(), InResponse.Level, LevelMod.Cost.AmountAsCurrency());
			}
		}
		
		OnImproveItem.Broadcast();

		auto NotifySounds = FGameSingletonExtension::GetDataAssets<UNotifySoundsData>();
		if (NotifySounds.IsValidIndex(0) && NotifySounds[0] && NotifySounds[0]->IsValidLowLevel())
		{
			FSlateApplication::Get().PlaySound(NotifySounds[0]->Notify_BuyImprovement);

			SNotifyContainer::ShowNotify
			(
				*FString::Printf(TEXT("Notify_BuyImprovement_%d"), InResponse.Level), 
				FText::Format(NSLOCTEXT("Notify", "Notify.BuyImprovement", "You successfully upgraded \"{0}\""), item->GetItemProperty()->Title)
			);
		}
	}
	else
	{
		FShooterGameAnalytics::RecordDesignEvent("UI:Lobby:Arsenal:Upgrade:Successfuly");
		FShooterGameAnalytics::RecordErrorEvent(EErrorSeverity::warning, "[OnItemUpgrade][Not found item]: " + InResponse.EntityId.ToString());
	}
}

void UArsenalComponent::OnItemUpgrade_Failed(const FRequestExecuteError& error)
{
	FShooterGameAnalytics::RecordDesignEvent("UI:Lobby:Arsenal:Upgrade:Fail:Code" + FString::FormatAsNumber(error.Status));
	FShooterGameAnalytics::RecordErrorEvent(EErrorSeverity::warning, "UI:Lobby:Arsenal:Upgrade:Fail:Code" + FString::FormatAsNumber(error.Status));

	SNotifyContainer::ShowNotify("Notify_BuyImprovement_Failed", FText::Format(NSLOCTEXT("Notify", "Notify.BuyImprovement_Failed", "Failed to upgrade item. Error code : {0}"), FText::AsNumber(error.Status, &FNumberFormattingOptions::DefaultNoGrouping())));
}

void UArsenalComponent::OnItemUpgrade_NotFound()
{
	FShooterGameAnalytics::RecordErrorEvent(EErrorSeverity::warning, "OnItemUpgrade_NotFound");
	SNotifyContainer::ShowNotify("Notify_BuyImprovement_NotFound", NSLOCTEXT("Notify", "Notify.BuyImprovement_NotFound", "Failed to upgrade item. Error code: #1"));
}

void UArsenalComponent::OnItemUpgrade_NotEnouthMoney(const FGuid& InPlayerItemId) const
{
	SNotifyContainer::ShowNotify("Notify_BuyImprovement_NotEnouthMoney", NSLOCTEXT("Notify", "Notify.BuyImprovement_NotEnouthMoney", "Not enough money to upgrade"));

	if (const auto item = FindItem(InPlayerItemId))
	{
		OnPlayerNotEnoughMoney.Broadcast(item->GetItemCost());
	}
	else
	{
		OnPlayerNotEnoughMoney.Broadcast(FGameItemCost());
	}

}

void UArsenalComponent::OnItemUpgrade_MaximumLevelReached()
{
	QueueBegin(SNotifyContainer, "Notify_BuyImprovement_MaximumLevelReached")
		SNotifyContainer::Get()->SetContent(NSLOCTEXT("Notify", "Notify.BuyImprovement_MaximumLevelReached", "The item has already been improved to the maximum."));
		SNotifyContainer::Get()->ToggleWidget(true);
	QueueEnd

	FShooterGameAnalytics::RecordErrorEvent(EErrorSeverity::warning, "OnItemUpgrade_MaximumLevelReached");
}

void UArsenalComponent::OnItemUpgrade_RequestedInvalidLevel()
{
	QueueBegin(SNotifyContainer, "Notify_BuyImprovement__RequestedInvalidLevel")
		SNotifyContainer::Get()->SetContent(NSLOCTEXT("Notify", "Notify.BuyImprovement__RequestedInvalidLevel", "Failed to upgrade item. Error code: #2"));
		SNotifyContainer::Get()->ToggleWidget(true);
	QueueEnd

	FShooterGameAnalytics::RecordErrorEvent(EErrorSeverity::warning, "OnItemUpgrade_RequestedInvalidLevel");
}

void UArsenalComponent::OnMakeInAppPurchase(const FString& inProductId)
{
	//=======================================================================
	//UCrashlyticsBlueprintLibrary::SetField("MakeInAppPurchase.ProductId", inProductId);
	//UCrashlyticsBlueprintLibrary::SetField("MakeInAppPurchase.Date", FDateTime::Now());
	//UCrashlyticsBlueprintLibrary::SetField("MakeInAppPurchase.Status", "SendRequest");

	//=======================================================================
	//UCrashlyticsBlueprintLibrary::WriteLog("UArsenalComponent:OnMakeInAppPurchase");

	//=======================================================================
#if UE_BUILD_DEVELOPMENT
	GEngine->AddOnScreenDebugMessage(-1, 5.f, FColor::Red, FString::Printf(TEXT("[OnMakeInAppPurchase][%s]"), *inProductId));
#endif

	//=======================================================================
	auto gm = GetGameMode<ALobbyGameMode>();

	//=======================================================================
	if (IsValidObject(gm))
	{
		gm->OnMarketPurchaseValidate.BindDynamic(this, &UArsenalComponent::SendMarketDeliveryRequest);
		gm->OnInAppPurchase(inProductId, EInAppPurchaseMethodHandler::Arsernal);
	}
	else
	{
		SNotifyContainer::ShowNotify("Notify_MakeInAppPurchase_ServiceNotAvailable", NSLOCTEXT("Notify", "Notify.MakeInAppPurchase.ServiceNotAvailable", "Payment service not available"));
		FShooterGameAnalytics::RecordErrorEvent(EErrorSeverity::error, "OnMakeInAppPurchase::Purchase::ServiceNotAvailable");
	}

}

void UArsenalComponent::SendMarketDeliveryRequest(const FMarketDeliveryRequest& request)
{
	//UCrashlyticsBlueprintLibrary::SetField("MarketDelivery.ProductId", request.ProductId);
	//UCrashlyticsBlueprintLibrary::SetField("MarketDelivery.Date", FDateTime::Now());
	//UCrashlyticsBlueprintLibrary::SetField("MarketDelivery.Status", "SendRequest");

	FShooterGameAnalytics::RecordDesignEvent("UI:Lobby:Market:Purchase:Delivery:Request");

	checkf(IsValidObject(MarketDelivery), TEXT("MarketDeliveryRequest was nullptr"));
	MarketDelivery->SendRequestObject(request);
}

void UArsenalComponent::SendMarketExchangeRequest(const FString& inProductId) const
{
	//UCrashlyticsBlueprintLibrary::SetField("MarketExchange.ProductId", inProductId);
	//UCrashlyticsBlueprintLibrary::SetField("MarketExchange.Status", "SendRequest");
	//UCrashlyticsBlueprintLibrary::SetField("MarketExchange.Date", FDateTime::Now());

	checkf(IsValidObject(MarketExchange), TEXT("MarketExchange was nullptr"));
	MarketExchange->SendRequestObject(inProductId);
}


//=================================================================================================================

void UArsenalComponent::OnMarketExchange_NotEnouthMoney(const FGameItemCost& InEnouthMoney) const
{
	SNotifyContainer::ShowNotify("Notify.OnMarketExchange_NotEnouthMoney", NSLOCTEXT("Notify", "Notify.OnMarketExchange_NotEnouthMoney", "Insufficient funds to purchase item."));
	//UCrashlyticsBlueprintLibrary::SetField("MarketExchange.Status", "NotEnouthMoney");
	OnPlayerNotEnoughMoney.Broadcast(InEnouthMoney);

}

void UArsenalComponent::OnMarketDelivery(const FMarketDeliveryResponse& response)
{		
	//=======================================
	SendRequestPlayerDiscounts();

	//=======================================
	//UCrashlyticsBlueprintLibrary::SetField("MarketDelivery.Status", "Successfully");
	FShooterGameAnalytics::RecordDesignEvent("UI:Lobby:Market:Purchase:Delivery:Successfully");

	//=======================================
	OnRequestPlayerDataUpdate.Broadcast();

	//=======================================
	SNotifyContainer::ShowNotify("Notify.OnMarketDelivery", LOCTEXT("Notify.OnMarketDelivery.Content", "Your payment was successful"));
}

void UArsenalComponent::OnMarketDelivery_Failed(const FRequestExecuteError& error)
{	
	//=======================================
	//UCrashlyticsBlueprintLibrary::SetField("MarketDelivery.Status", "StatusCode" + error.GetStatusCode());
	FShooterGameAnalytics::RecordDesignEvent("UI:Lobby:Market:Purchase:Delivery:Fail:Code" + FString::FormatAsNumber(error.Status));

	FShooterGameAnalytics::RecordErrorEvent(EErrorSeverity::warning, "OnMarketDelivery_Failed:" + FString::FormatAsNumber(error.Status));

	QueueBegin(SMessageBox, "OnMarketDelivery_Failed")
		SMessageBox::Get()->SetHeaderText(LOCTEXT("Notify.InAppPurchase.Title", "InAppPurchase information"));
		SMessageBox::Get()->SetContent(FText::Format(LOCTEXT("Notify.OnMarketDelivery_Failed.Content", "Your payment was made with an error! \nDetails of the error:\n {0}"), FText::FromString(error.ToString())));
		
		SMessageBox::Get()->SetButtonsText(FTableBaseStrings::GetBaseText(EBaseStrings::Close));
		SMessageBox::Get()->OnMessageBoxButton.AddLambda([&](const EMessageBoxButton& Button)
		{
			SMessageBox::Get()->ToggleWidget(false);
		});
		SMessageBox::Get()->ToggleWidget(true);
	QueueEnd
}

//=================================================================================================================

void UArsenalComponent::OnMarketExchange(const FMarketDeliveryResponse& response)
{
	//=======================================
	SendRequestPlayerDiscounts();

	//=======================================
	//UCrashlyticsBlueprintLibrary::SetField("MarketExchange.Status", "Successfuly");

	//=======================================
	FShooterGameAnalytics::RecordDesignEvent("UI:Lobby:Market:Purchase:Exchange:Successfuly");

	//=======================================
	OnRequestPlayerDataUpdate.Broadcast();

	//=======================================
	QueueBegin(SNotifyContainer, "Notify_OnMarketExchange")
		SNotifyContainer::Get()->SetContent(LOCTEXT("Notify.OnMarketDelivery.Content", "Your payment was successful"));
		SNotifyContainer::Get()->ToggleWidget(true);
	QueueEnd
}

void UArsenalComponent::OnMarketExchange_Failed(const FRequestExecuteError& error)
{	
	//=======================================
	//UCrashlyticsBlueprintLibrary::SetField("MarketExchange.Status", "StatusCode" + error.GetStatusCode());
	FShooterGameAnalytics::RecordDesignEvent("UI:Lobby:Market:Purchase:Exchange:Fail:Code" + FString::FormatAsNumber(error.Status));

	FShooterGameAnalytics::RecordErrorEvent(EErrorSeverity::warning, "OnMarketExchange_Failed:" + FString::FormatAsNumber(error.Status));

	QueueBegin(SMessageBox, "OnMarketExchange_Failed")
		SMessageBox::Get()->SetHeaderText(LOCTEXT("Notify.InAppPurchase.Title", "InAppPurchase information"));
		SMessageBox::Get()->SetContent(FText::Format(LOCTEXT("Notify.OnMarketDelivery_Failed.Content", "Your payment was made with an error! \nDetails of the error:\n {0}"), FText::FromString(error.ToString())));

		SMessageBox::Get()->SetButtonsText(FTableBaseStrings::GetBaseText(EBaseStrings::Close));
		SMessageBox::Get()->OnMessageBoxButton.AddLambda([&](const EMessageBoxButton& Button)
		{
			SMessageBox::Get()->ToggleWidget(false);
		});
		SMessageBox::Get()->ToggleWidget(true);
	QueueEnd
}

bool UArsenalComponent::SendRequestLootData_Validate() { return true; }
void UArsenalComponent::SendRequestLootData_Implementation()
{
	checkf(IsValidObject(RequestLootData), TEXT("RequestLootData was nullptr"));
	RequestLootData->GetRequest();
}

void UArsenalComponent::OnLootData(const TArray<FPlayerInventoryItemModel>& InData)
{
	if (GetUserOwner() && GetUserOwner()->IsAllowUserInventory())
	{
		OnLoadPlayerInventorySuccessfully(InData);
	}
}




void UArsenalComponent::CreateGameWidgets()
{

}

void UArsenalComponent::OnUnlockItemSuccessfully(const FPlayerInventoryItemUnlocked& response)
{
	//=======================================
	SendRequestPlayerDiscounts();

	//=======================================
	OnRequestPlayerDataUpdate.Broadcast();

	//=======================================
	bool NeedUnlockedItemCreate = true;
	if (auto FoundItem = FGameSingletonExtensions::FindItemById(response.ModelId))
	{
		NeedUnlockedItemCreate = FoundItem->IsProduct();

		auto NotifySounds = FGameSingletonExtension::GetDataAssets<UNotifySoundsData>();
		if (NotifySounds.IsValidIndex(0) && NotifySounds[0] && NotifySounds[0]->IsValidLowLevel())
		{
			if (NeedUnlockedItemCreate)
			{
				FSlateApplication::Get().PlaySound(NotifySounds[0]->Notify_BuyShopItem);

				QueueBegin(SNotifyContainer, *FString::Printf(TEXT("Notify_BuyShopItem_%d"), response.ModelId), i = FoundItem)
					SNotifyContainer::Get()->SetContent(FText::Format(NSLOCTEXT("Notify", "Notify.BuyShopItem", "You successfully purchased \"{0}\""), i->GetItemProperty().Title));
					SNotifyContainer::Get()->ToggleWidget(true);
				QueueEnd
			}
			else
			{
				FSlateApplication::Get().PlaySound(NotifySounds[0]->Notify_BuyArsenalItem);

				QueueBegin(SNotifyContainer, *FString::Printf(TEXT("Notify_BuyArsenalItem_%d"), response.ModelId), i = FoundItem)
					SNotifyContainer::Get()->SetContent(FText::Format(NSLOCTEXT("Notify", "Notify.BuyArsenalItem", "You successfully purchased \"{0}\""), i->GetItemProperty().Title));
					SNotifyContainer::Get()->ToggleWidget(true);
				QueueEnd
			}
		}

		OnItemUnlocked.Broadcast(FoundItem);
	}

	if (NeedUnlockedItemCreate)
	{
		CreateAndInsertItem(response);
		OnRep_Items();

		StartTimer(RequestProfilesData);
	}
}

void UArsenalComponent::OnUnlockItem_NotEnouthMoney(const int32& InModelId) const
{
	if (const auto item = FGameSingletonExtensions::FindItemById(InModelId))
	{
		OnPlayerNotEnoughMoney.Broadcast(item->GetItemCost());
	}
	else
	{
		OnPlayerNotEnoughMoney.Broadcast(FGameItemCost());
	}
}

UBasicPlayerItem* UArsenalComponent::CreateAndInsertItem(const FPlayerInventoryItemModel& i)
{
	//============================================
	const auto instance = FGameSingletonExtensions::FindItemById(i.ModelId);
	if (instance == nullptr)
	{
		//============================================
		UE_LOG(LogArsenalComponent, Error, TEXT("[UArsenalComponent][CreateAndInsertItem][Instance: %d]: instance was nullptr | entity: %s"), i.ModelId, *i.EntityId.ToString());

		//============================================
		return nullptr;
	}
	else
	{
		UE_LOG(LogArsenalComponent, Error, TEXT("[UArsenalComponent][CreateAndInsertItem][%s][Model: %d / amount: %d | %s]"), *GetBasicPlayerState<APlayerState>()->GetPlayerName(), i.ModelId, i.Amount, *i.EntityId.ToString());
	}

	//============================================
	UBasicPlayerItem* Item = FindItem(i.EntityId);

	//============================================
	if (Item == nullptr)
	{
		Item = FindItem(i.ModelId);
		if (Item)
		{
			Item->SetEntityId(i.EntityId);
			Item->SetState(EPlayerItemState::Purchased);
		}
		else
		{
			Item = NewObject<UBasicPlayerItem>(this, instance->GetInventoryClass());
			if (i.EntityId.IsValid())
			{
				Item->SetEntityId(i.EntityId);
				Item->InitializeInstanceDynamic(instance);
				Item->SetState(EPlayerItemState::Purchased);
				Items.AddUnique(Item);
			}
		}
	}

	//============================================
	checkf(Item, TEXT("[UArsenalComponent][CreateAndInsertItem][Item %d / %s] : was nullptr"), i.ModelId, *i.EntityId.ToString());

	//============================================
	Item->SetAmount(i.Amount);
	Item->SetLevel(i.Level);

	//============================================
	return Item;
}

void UArsenalComponent::OnLoadPlayerInventorySuccessfully(const TArray<FPlayerInventoryItemModel>& response)
{
	//UCrashlyticsBlueprintLibrary::SetField("Identity.LoadPlayerInventory", true);

	StartTimer(RequestProfilesData);

	//UCrashlyticsBlueprintLibrary::WriteLog("UArsenalComponent:OnLoadPlayerInventorySuccessfully");
	//=============================================================================
	auto instances = FGameSingletonExtensions::GetItems();

	//=============================================================================
	if (GetGameMode<ALobbyGameMode>())
	{
		for (auto instance : instances)
		{
			if (FindItem(instance->GetModelId()) == nullptr)
			{
				auto item = NewObject<UBasicPlayerItem>(this, instance->GetInventoryClass());
				item->InitializeInstanceDynamic(instance);
				item->SetState(EPlayerItemState::None);
				Items.AddUnique(item);
			}
		}
	}

	//=============================================================================
	for (auto i : response)
	{
		CreateAndInsertItem(i);
	}

	//=============================================================================
	OnRep_Items();
}

void UArsenalComponent::OnLoadPlayerInventoryFailed(const FRequestExecuteError& InError) const
{
	//UCrashlyticsBlueprintLibrary::SetField("Identity.LoadPlayerInventory", false);

	//====================================
	FShooterGameAnalytics::RecordDesignEvent("UI:Lobby:LoadInventory:Fail:Code" + InError.GetStatusCode());

	//====================================
	FShooterGameAnalytics::RecordErrorEvent(EErrorSeverity::critical, "OnLoadPlayerInventoryFailed");

}

void UArsenalComponent::OnAuthorizationComplete()
{
	if (HasAuthority())
	{
		StartTimer(RequestLootData);
		StartRequestPlayerCases();
		StartRequestPlayerDiscounts();
	}
}

void UArsenalComponent::RequestChangeDefaultWeapon(UWeaponPlayerItem* InTarget)
{
	if (ProfilesData.IsValidIndex(0) && InTarget && InTarget->IsValidLowLevel())
	{
		const FGuid InTargetId = InTarget->GetEntityId();
		for (auto TargetItem : ProfilesData[0].Items)
		{
			if (TargetItem.ItemId == InTargetId)
			{				
				UE_LOG(LogArsenalComponent, Warning, TEXT("RequestChangeDefaultWeapon >> %s to %s"), *ProfilesData[0].DefaultItemId.ToString(), *TargetItem.EntityId.ToString());
				ProfilesData[0].DefaultItemId = TargetItem.EntityId;
				break;
			}
		}		
	}
}

#pragma region Items

#pragma region FindItem
UBasicPlayerItem* UArsenalComponent::FindItem(const int32& InModelId) const
{
	for (auto item : Items)
	{
		if (const auto i = GetValidObject(item))
		{
			if (i && i->IsEqual(InModelId))
			{
				return i;
			}
		}
	}

	return nullptr;
}

UBasicPlayerItem* UArsenalComponent::FindItem(const FString& InIndex) const
{
	FGuid id;
	if(FGuid::Parse(InIndex, id))
	{
		return FindItem(id);
	}
	return nullptr;
}

UBasicPlayerItem* UArsenalComponent::FindItem(const FLokaGuid& InIndex) const
{
	return FindItem(InIndex.Source);
}

UBasicPlayerItem* UArsenalComponent::FindItem(const FGuid& InIndex) const
{
	if (ItemMap.Contains(InIndex))
	{
		return GetValidObject(ItemMap.FindChecked(InIndex));
	}

	return nullptr;
}

UBasicPlayerItem* UArsenalComponent::FindItem(const FGuid& InIndex, const int32& InModelId) const
{
	const auto item = FindItem(InIndex);
	if(item)
	{
		return item;
	}
	return FindItem(InModelId);
}

FString UArsenalComponent::GetEntityName(const UBasicPlayerItem* InItem)
{
	return UBasicPlayerItem::GetEntityName(InItem);
}

FString UArsenalComponent::GetEntityName(const UBasicItemEntity* InItem)
{
	return UBasicItemEntity::GetEntityName(InItem);
}

#pragma endregion

void UArsenalComponent::OnRep_Items()
{
	ItemMap.Empty(Items.Num() + 2);

	CachedAids = 0;
	CashedGrenades = 0;

	for (auto item : Items)
	{
		if (item && item->IsValidLowLevel())
		{
			ItemMap.Add(item->GetEntityId(), item);

			if (item->GetItemType() == EGameItemType::Stimulant)
			{
				CachedAids += item->GetAmount();
			}
			else if (item->GetItemType() == EGameItemType::Grenade)
			{
				CashedGrenades += item->GetAmount();
			}
		}
	}

	OnInventoryUpdate.Broadcast(Items);
}

#pragma endregion


bool UArsenalComponent::SendRequestProfilesData_Validate() { return true; }
void UArsenalComponent::SendRequestProfilesData_Implementation()
{
	checkf(IsValidObject(RequestProfilesData), TEXT("RequestProfilesData was nullptr"));
	RequestProfilesData->GetRequest();
}

bool UArsenalComponent::SendRequestItemEquip_Validate(const FPlayerProfileItemEquip& InData) { return true; }
void UArsenalComponent::SendRequestItemEquip_Implementation(const FPlayerProfileItemEquip& InData)
{
	checkf(IsValidObject(RequestItemEquip), TEXT("RequestItemEquip was nullptr"));
	RequestItemEquip->SendRequestObject(InData);
}


void UArsenalComponent::OnProfiles(const TArray<FPlayerProfileModel>& InData)
{
	//UCrashlyticsBlueprintLibrary::SetField("Identity.LoadPlayerProfiles", true);

	UE_LOG(LogArsenalComponent, Display, TEXT("UArsenalComponent::OnRequestPreSetDataSuccessfully[%s][%d]"), *GetIdentityToken().Get().ToString(), static_cast<int32>(GetNetMode()));

	CachedHealth = .0f;
	CachedArmour = .0f;

	if (GetUserOwner() && GetUserOwner()->IsAllowUserInventory())
	{
		for (auto MyItem : Items)
		{
			if (MyItem && MyItem->IsValidLowLevel())
			{
				MyItem->SetState(MyItem->GetEntityId().IsValid() ? EPlayerItemState::Purchased : EPlayerItemState::None);
			}
		}
		

		//=================================
		for (auto Equipped : InData[0].Items)
		{
			if (auto TargetItem = FindItem(Equipped.ItemId))
			{
				TargetItem->SetState(EPlayerItemState::Equipped);
				if (Equipped.EntityId == InData[0].DefaultItemId)
				{
					TargetItem->SetState(EPlayerItemState::Active);
				}

				if (auto TargetAsChar = TargetItem->GetEntity<UCharacterItemEntity>())
				{
					CachedHealth += TargetAsChar->GetCharacterProperty().Health;
				}

				if (auto TargetAsArmour = TargetItem->GetEntity<UArmourItemEntity>())
				{
					CachedArmour += TargetAsArmour->GetArmourProperty().Armour;
				}
			}			
		}

		//=================================
		const auto DefaultActiveItemModel = InData[0].GetDefaultItem();
		if(DefaultActiveItemModel)
		{
			auto TargetItem = FindItem(DefaultActiveItemModel->ItemId);
			UE_LOG(LogArsenalComponent, Display, TEXT("UArsenalComponent::OnProfiles[DefaultActiveItemModel][ItemId: %s][EntityId: %s][TargetItem: %d]"), *DefaultActiveItemModel->ItemId.ToString(), *DefaultActiveItemModel->EntityId.ToString(), TargetItem != nullptr);
			
			if (TargetItem)
			{
				TargetItem->SetState(EPlayerItemState::Active);
			}
		}
		else
		{
			UE_LOG(LogArsenalComponent, Display, TEXT("UArsenalComponent::OnProfiles[DefaultActiveItemModel][was nullptr]"));
		}

		//=================================
		OnProfileDataEvent.Broadcast(InData);
		OnProfilesUpdateHandle(InData);
	}
}

//===========================================================

#pragma region OnItemEquip

void UArsenalComponent::OnItemEquip(const FPlayerProfileItemEquip& InResponse)
{
	StartTimer(RequestProfilesData);

#if !UE_SERVER
	if (auto FoundItem = FindItem(InResponse.ItemId))
	{
		QueueBegin(SNotifyContainer, "Notify_ItemEquip", i = FoundItem)
			SNotifyContainer::Get()->SetContent(FText::Format(NSLOCTEXT("Notify", "Notify.ItemEquip", "You successfully equipped item \"{0}\""), i->GetBaseItemProperty()->Title));
			SNotifyContainer::Get()->ToggleWidget(true);
		QueueEnd
	}
#endif
}

void UArsenalComponent::OnItemEquip_Failed(const FRequestExecuteError& error)
{
	StartTimer(RequestProfilesData);
}

void UArsenalComponent::OnItemEquip_ProfileNotFound()
{
	StartTimer(RequestProfilesData);
}

#pragma endregion 

void UArsenalComponent::GetLifetimeReplicatedProps(TArray< FLifetimeProperty > & OutLifetimeProps) const
{
	Super::GetLifetimeReplicatedProps(OutLifetimeProps);

	DOREPLIFETIME(UArsenalComponent, ProfilesData);
	DOREPLIFETIME(UArsenalComponent, Items);


	//DOREPLIFETIME(UArsenalComponent, ProfilesData);
	//DOREPLIFETIME(UArsenalComponent, Items);
}

void UArsenalComponent::OnProfilesUpdateHandle(const TArray<FPlayerProfileModel>& InData)
{
	ProfilesData = InData;
	for (auto &MyProfile : ProfilesData)
	{
		MyProfile.Items.Sort([](const FPlayerProfileItemModel& A, const FPlayerProfileItemModel& B) { return A.SetSlotId < B.SetSlotId; });
	}

	OnProfileDataUpdated.Broadcast();
}

FPlayerProfileModel UArsenalComponent::GetProfileData(const int32 InTargetProfile) const
{
	FPlayerProfileModel Data;
	if (ProfilesData.Num() && ProfilesData.IsValidIndex(InTargetProfile))
	{
		Data = ProfilesData[InTargetProfile];
	}

	return Data;
}

FPlayerProfileModel* UArsenalComponent::GetProfileDataPtr(const int32 InTargetProfile)
{
	if (ProfilesData.Num() && ProfilesData.IsValidIndex(InTargetProfile))
	{
		return &ProfilesData[InTargetProfile];
	}

	return nullptr;
}


const FPlayerProfileModel* UArsenalComponent::GetProfileDataPtr(const int32 InTargetProfile) const
{
	if (ProfilesData.Num() && ProfilesData.IsValidIndex(InTargetProfile))
	{
		return &ProfilesData[InTargetProfile];
	}

	return nullptr;
}

UBasicPlayerItem* UArsenalComponent::GetPlayerItemInProfileBySlot(const int32& InTargetProfile, const EGameItemType& InTargetSlot)
{
	auto DefaultProfile = GetProfileDataPtr(InTargetProfile);
	if (DefaultProfile)
	{
		for (auto i : DefaultProfile->Items)
		{
			if (i.SetSlotId == static_cast<int32>(InTargetSlot))
			{
				auto Item = FindItem(i.ItemId);
				if (Item && Item->GetBaseItemProperty())
				{
					return Item;
				}
			}
		}
	}
	return nullptr;
}

bool UArsenalComponent::GetDefaultItem(const int32& InTargetProfile, FPlayerProfileItemModel& OutDefaultProfileItem, UBasicPlayerItem*& OutDefaultPlayerItem) const
{
	auto DefaultProfile = GetProfileDataPtr(InTargetProfile);
	if (DefaultProfile)
	{
		auto DefaultItem = DefaultProfile->GetDefaultItem();
		if (DefaultItem)
		{
			OutDefaultProfileItem = *DefaultItem;
			OutDefaultPlayerItem = FindItem(DefaultItem->ItemId);
			return OutDefaultPlayerItem != nullptr;
		}
	}

	return false;
}

bool UArsenalComponent::IsTargetWeaponLevel(const int32& InTargetProfile, const int32& InTargetWeaponLevel) const
{
	//=======================================================
	int32 MaximumAmmunationLevel = 0;

	//=======================================================
	auto DefaultProfile = GetProfileDataPtr(InTargetProfile);
	if (DefaultProfile)
	{
		for(auto i : DefaultProfile->Items)
		{
			auto Item = FindItem(i.ItemId);
			if(Item)
			{
				auto ItemRequiredLevel = Item->GetRequiredLevel();
				if (ItemRequiredLevel > MaximumAmmunationLevel)
				{
					MaximumAmmunationLevel = ItemRequiredLevel;
				}
			}
		}
	}

	//=======================================================
	return MaximumAmmunationLevel >= InTargetWeaponLevel;
}


bool UArsenalComponent::IsMaximumLevelReached(const int32& InTargetProfile) const
{
	UBasicPlayerItem* DefaultPlayerItem;
	FPlayerProfileItemModel DefaultProfileItem;

	if (GetDefaultItem(InTargetProfile, DefaultProfileItem, DefaultPlayerItem))
	{
		return DefaultPlayerItem->IsMaximumLevelReached();
	}

	return true;
}

bool UArsenalComponent::ReplaceWeaponInProfile(const int32& InTargetProfile, const int32& InTargetWeaponLevel)
{
	auto DefaultProfile = GetProfileDataPtr(InTargetProfile);
	if (DefaultProfile)
	{
		for (auto i : DefaultProfile->Items)
		{
			auto Item = FindItem<UWeaponPlayerItem>(i.ItemId);
			if (Item && Item->GetWeaponEntity())
			{
				auto ItemRequiredLevel = Item->GetRequiredLevel();
				if(ItemRequiredLevel < InTargetWeaponLevel)
				{
					for(int level = ItemRequiredLevel; level < InTargetWeaponLevel; level++)
					{
						//==========================================
						auto MinTargetLevel = static_cast<uint32>(level);
						auto MaxTargetLevel = FMath::Min(static_cast<uint32>(InTargetWeaponLevel), MinTargetLevel + 1);

						//==========================================
						auto TargetItems = FGameSingletonExtension::FindItemsByType<UWeaponItemEntity>(Item->GetItemType(), [Damage = Item->GetWeaponEntity()->GetWeaponConfiguration().Damage, MinTargetLevel, MaxTargetLevel](const UWeaponItemEntity* InItemEntity)
						{
							if (InItemEntity)
							{
								auto WeaponProperty = InItemEntity->GetWeaponProperty();
								auto WeaponRequiredLevel = WeaponProperty.Level;
								return WeaponRequiredLevel >= MinTargetLevel && WeaponRequiredLevel <= MaxTargetLevel && InItemEntity->GetWeaponConfiguration().Damage > Damage;
							}
							return false;
						});

						//==========================================
						if (TargetItems.Num())
						{
							auto TargetItemEntity = FArrayExtensions::TakeRandom(TargetItems, false);
							if (TargetItemEntity)
							{
								Item->ReplaceEntity(TargetItemEntity);
								break;
							}
						}
					}
				}
			}
		}
	}

	return true;
}

bool UArsenalComponent::UpgradeDefaultItemInProfile(const int32& InTargetProfile)
{
	UBasicPlayerItem* DefaultPlayerItem;
	FPlayerProfileItemModel DefaultProfileItem;

	if(GetDefaultItem(InTargetProfile, DefaultProfileItem, DefaultPlayerItem))
	{
		if (DefaultPlayerItem->IsMaximumLevelReached() == false)
		{
			DefaultPlayerItem->IncLevel();
			return true;
		}
	}

	return false;
}

bool UArsenalComponent::IsExistAllSlotsInProfile(const int32& InTargetProfile, const TArray<EGameItemType>& InTargetSlots) const
{
	//=====================================================
	bool IsAllExist = true;

	//=====================================================
	auto TargetProfile = GetProfileDataPtr(InTargetProfile);

	//=====================================================
	if (TargetProfile)
	{
		for (auto slot : InTargetSlots)
		{
			if(TargetProfile->Items.FindByPredicate([slot = static_cast<int32>(slot)](const FPlayerProfileItemModel& InItem)
			{
				return InItem.SetSlotId == slot;
			}) == nullptr)
			{
				IsAllExist = false;
				break;
			}
		}
	}

	//=====================================================
	return IsAllExist;
}

bool UArsenalComponent::AddIfNotExistItemInSlot(const int32& InTargetProfile, const EGameItemType& InTargetSlot, const int32& InTargetModelId)
{
	//=====================================================
	auto TargetProfile = GetProfileDataPtr(InTargetProfile);
	if(TargetProfile == nullptr)
	{
		return false;
	}

	//=====================================================
	auto TargetSlot = TargetProfile->GetItemBySlot(static_cast<int32>(InTargetSlot));
	if(TargetSlot == nullptr)
	{
		auto FoundItem = FGameSingletonExtensions::FindItemById<UBasicItemEntity>(InTargetModelId);
		if (FoundItem)
		{
			//-----------------------------------------------------------
			FPlayerInventoryItemModel ItemInventory(InTargetModelId);
			OnLoadPlayerInventorySuccessfully(TArray<FPlayerInventoryItemModel> {ItemInventory});

			//-----------------------------------------------------------
			FPlayerProfileItemModel ItemProf(ItemInventory.EntityId, static_cast<int32>(FoundItem->GetItemProperty().Type));

			//-----------------------------------------------------------
			TargetProfile->Items.Add(ItemProf);

			return true;
		}
	}

	//=====================================================
	return false;
}

#undef LOCTEXT_NAMESPACE
