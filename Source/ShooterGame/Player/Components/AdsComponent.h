#pragma once

#pragma once
//==========================================
#include "BasicPlayerComponent.h"

//==========================================
#include "AdsComponent.generated.h"



DECLARE_DYNAMIC_MULTICAST_DELEGATE_OneParam(FPlayRewardVideoComplete, const FPlayRewardedVideoResponse&, InResponse);

UCLASS()
class UAdsComponent : public UBasicPlayerComponent
{
	GENERATED_BODY()

		UAdsComponent();

	UPROPERTY(VisibleInstanceOnly)
	TMap<int32, FString> RewardVideoIds;

	UPROPERTY(VisibleInstanceOnly)
	mutable FPlayRewardedVideoRequest RewardedVideoRequest;

	UPROPERTY(VisibleInstanceOnly)
	mutable FPlayRewardedVideoRequest PreviewRewardedVideoRequest;


	//========================================================================================
	virtual void EndPlay(const EEndPlayReason::Type EndPlayReason) override;
	virtual void CreateGameWidgets() override;

	//========================================================================================
public:
	virtual bool PlayRewardVideo(const FPlayRewardedVideoRequest& InVideoRequest) const override final;
	virtual void OnPlayRewardVideoSuccess(const FPlayRewardedVideoResponse& InResponse) override final;
	virtual void OnPlayRewardVideoCanceled(const FPlayRewardedVideoResponse& InResponse) override final;

	virtual void OnPreloadRewardedVideo();

	UPROPERTY(VisibleInstanceOnly, BlueprintAssignable)
	FPlayRewardVideoComplete OnPlayRewardVideoSuccessDelegate;

	UPROPERTY(VisibleInstanceOnly, BlueprintAssignable)
	FPlayRewardVideoComplete OnPlayRewardVideoClosedDelegate;

};
