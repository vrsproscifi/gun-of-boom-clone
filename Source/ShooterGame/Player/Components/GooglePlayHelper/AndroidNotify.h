#pragma once

#ifndef H_AndroidNotify
#define H_AndroidNotify

struct FAndroidNotify
{
	FString Id;
	FString Title;
	FString Message;
	FTimespan NotifyDate;

	int32 NotifyStep;
	int32 NotifyStepCount;

	FAndroidNotify()
		: NotifyStep(0)
		, NotifyStepCount(0)
	{

	}

	int64 GetNotifyDate() const
	{
		return FMath::RoundToInt(NotifyDate.GetTotalMilliseconds());
	}

};

struct FAndroidNotifyManager
{
	static bool ScheduleLocalNotification(const FAndroidNotify& InNotify, const TArray<FString>& InNotifyToCancel);
	static bool ScheduleLocalNotification(const TArray<FAndroidNotify>& InNotify, const TArray<FString>& InNotifyToCancel);
};

#endif