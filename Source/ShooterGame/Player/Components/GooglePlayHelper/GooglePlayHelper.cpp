//======================================
#include "ShooterGame.h"
#include "GooglePlayHelper.h"

//======================================
#if PLATFORM_ANDROID
#include <jni.h>
#include "Android/AndroidJNI.h"
#include "Android/AndroidApplication.h"
#include "Android/AndroidJavaEnv.h"
#endif


bool FGooglePlayHelper::TravelToGooglePlay()
{

#if PLATFORM_ANDROID
	if (JNIEnv* env = FAndroidApplication::GetJavaEnv())
	{
		//=================================
		jstring methodString = env->NewStringUTF("market://details?id=com.VRSPro.LOKA_AW");
		UE_LOG(LogInit, Log, TEXT("[OnAuthorizationComplete] methodString: %d"), methodString != nullptr);

		//=================================
		// Get defenitions of Intent and it's constructor.
		jclass IntentClass = env->FindClass("android/content/Intent");
		UE_LOG(LogInit, Log, TEXT("[OnAuthorizationComplete] IntentClass: %d"), IntentClass != nullptr);

		jmethodID IntentConstructor = env->GetMethodID(IntentClass, "<init>", "(Ljava/lang/String;Landroid/net/Uri;)V");


		//jmethodID IntentConstructor = env->GetMethodID(IntentClass, "<init>", "(Ljava/lang/String;)V");
		UE_LOG(LogInit, Log, TEXT("[OnAuthorizationComplete] IntentConstructor: %d"), IntentConstructor != nullptr);

		// Get the Activity defenition and startActivity method
		jclass ActivityClass = env->FindClass("android/app/Activity");
		UE_LOG(LogInit, Log, TEXT("[OnAuthorizationComplete] ActivityClass: %d"), ActivityClass != nullptr);

		jmethodID MethodStartActivity = env->GetMethodID(ActivityClass, "startActivity", "(Landroid/content/Intent;)V");
		UE_LOG(LogInit, Log, TEXT("[OnAuthorizationComplete] MethodStartActivity: %d"), MethodStartActivity != nullptr);

		// Get the values of the string actions we need
		jstring ACTION_VIEW = env->NewStringUTF("android.intent.action.VIEW");
		UE_LOG(LogInit, Log, TEXT("[OnAuthorizationComplete] ACTION_VIEW: %d"), ACTION_VIEW != nullptr);


		// Get the methods setType, putExtra and createChooser
		jmethodID MethodSetData = env->GetMethodID(IntentClass, "setData", "(Landroid/net/Uri;)Landroid/content/Intent;");
		UE_LOG(LogInit, Log, TEXT("[OnAuthorizationComplete] MethodSetData: %d"), MethodSetData != nullptr);

		jmethodID MethodSetAction = env->GetMethodID(IntentClass, "setAction", "(Ljava/lang/String;)Landroid/content/Intent;");
		UE_LOG(LogInit, Log, TEXT("[OnAuthorizationComplete] MethodSetAction: %d"), MethodSetAction != nullptr);

		//=================================
		jclass UriClass = env->FindClass("android/net/Uri");
		UE_LOG(LogInit, Log, TEXT("[OnAuthorizationComplete] UriClass: %d"), UriClass != nullptr);


		jmethodID UriParse = env->GetStaticMethodID(UriClass, "parse", "(Ljava/lang/String;)Landroid/net/Uri;");
		UE_LOG(LogInit, Log, TEXT("[OnAuthorizationComplete] UriParse: %d"), UriParse != nullptr);


		jobject uri = env->CallStaticObjectMethod(UriClass, UriParse, methodString);
		UE_LOG(LogInit, Log, TEXT("[OnAuthorizationComplete] uri: %d"), uri != nullptr);

		//=================================
		jobject intentObject = env->NewObject(IntentClass, IntentConstructor, ACTION_VIEW, uri);
		UE_LOG(LogInit, Log, TEXT("[OnAuthorizationComplete] intentObject: %d"), intentObject != nullptr);

		//=================================
		jmethodID startActivity = env->GetMethodID(ActivityClass, "startActivity", "(Landroid/content/Intent;)V");
		UE_LOG(LogInit, Log, TEXT("[OnAuthorizationComplete] startActivity: %d"), startActivity != nullptr);


		jobject lNativeActivity = FAndroidApplication::GetGameActivityThis();
		UE_LOG(LogInit, Log, TEXT("[OnAuthorizationComplete] lNativeActivity: %d"), lNativeActivity != nullptr);

		env->CallVoidMethod(lNativeActivity, startActivity, intentObject);
		return true;
	}
#endif
	return false;
}
