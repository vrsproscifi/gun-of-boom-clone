#pragma once

#ifndef IdentityService_H
#define IdentityService_H

//======================================
#ifdef USE_GOOGLE_SERVICES
#undef USE_GOOGLE_SERVICES
#endif

//======================================
#ifdef USE_LOKA_SERVICES
#undef USE_LOKA_SERVICES
#endif

//======================================
#if PLATFORM_ANDROID //&& UE_BUILD_DEVELOPMENT == 0 || true
#define USE_GOOGLE_SERVICES 1
#else
#define USE_GOOGLE_SERVICES 0
#endif

//======================================
#if (WITH_EDITOR || UE_BUILD_DEVELOPMENT || USE_GOOGLE_SERVICES == false) && USE_GOOGLE_SERVICES == 0
#define USE_LOKA_SERVICES 1
#else
#define USE_LOKA_SERVICES 0
#endif

#endif