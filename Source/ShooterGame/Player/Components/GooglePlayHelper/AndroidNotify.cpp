//======================================
#include "ShooterGame.h"
#include "AndroidNotify.h"
//#include "JavaObject.h"
#include "Notify/SMessageBox.h"

#if 0//PLATFORM_ANDROID && UE_BUILD_SHIPPING

struct FJNIAndroidNotify
{
	static const FString JNIMethodSignature;
	static jmethodID JNIMethodPtr;
	static jclass JNIStringClassId;
	
	static bool Init(JNIEnv* env)
	{
		//============================================
		if (JNIMethodPtr && JNIStringClassId)
		{
			return true;
		}

		//============================================
		if (env == nullptr)
		{
			ShowErrorMessage("env", "env was nullptr");
			return false;
		}

		//============================================	
		JNIStringClassId = env->FindClass("java/lang/String");
		if (JNIStringClassId == nullptr)
		{
			ShowErrorMessage("JStringClassId", "JStringClassId was nullptr");
			return false;
		}

		//============================================
		JNIMethodPtr = FJavaWrapper::FindMethod(env, FJavaWrapper::GameActivityClassID, "AndroidThunkJava_ScheduleLocalNotifications", TCHAR_TO_UTF8(*JNIMethodSignature), false);

		//============================================	
		if (JNIMethodPtr == nullptr)
		{
			ShowErrorMessage("Method", "Method was nullptr");
			return false;
		}

		//============================================
		if (JNIMethodPtr && JNIStringClassId)
		{
			return true;
		}

		return false;
	}
};

jclass FJNIAndroidNotify::JNIStringClassId = nullptr;
jmethodID FJNIAndroidNotify::JNIMethodPtr = nullptr;
const FString FJNIAndroidNotify::JNIMethodSignature = "([Ljava/lang/String;[Ljava/lang/String;[Ljava/lang/String;[J[Ljava/lang/String;)V";

#endif

bool FAndroidNotifyManager::ScheduleLocalNotification(const FAndroidNotify& InNotify, const TArray<FString>& InNotifyToCancel)
{
	return FAndroidNotifyManager::ScheduleLocalNotification(TArray<FAndroidNotify> {InNotify }, InNotifyToCancel);
}

bool FAndroidNotifyManager::ScheduleLocalNotification(const TArray<FAndroidNotify>& InNotify, const TArray<FString>& InNotifyToCancel)
{	
#if 0//PLATFORM_ANDROID && UE_BUILD_SHIPPING
	//============================================
	JNIEnv* env = FAndroidApplication::GetJavaEnv();

	//============================================
	if (FJNIAndroidNotify::Init(env) == false)
	{
		return false;
	}

	//============================================
	jobjectArray Id = env->NewObjectArray(InNotify.Num(), FJNIAndroidNotify::JNIStringClassId, nullptr);
	jobjectArray Title = env->NewObjectArray(InNotify.Num(), FJNIAndroidNotify::JNIStringClassId, nullptr);
	jobjectArray Message = env->NewObjectArray(InNotify.Num(), FJNIAndroidNotify::JNIStringClassId, nullptr);
	jobjectArray NotifyToCancel = env->NewObjectArray(InNotifyToCancel.Num(), FJNIAndroidNotify::JNIStringClassId, nullptr);
	jlongArray NotifyDate = env->NewLongArray(InNotify.Num());


	//jintArray NotifyStepCount = env->NewIntArray(InNotify.Num());
	//jintArray NotifyStep = env->NewIntArray(InNotify.Num());

	//============================================
	//{
	//	jlong* dates = new jlong[InNotify.Num()];
	//	for (int32 i = 0; i < InNotify.Num(); i++)
	//	{
	//		dates[i] = InNotify[i].NotifyStepCount;
	//	}
	//
	//	//============================================
	//	env->SetIntArrayRegion(NotifyStepCount, 0, InNotify.Num(), dates);
	//
	//	//============================================
	//	delete[] dates;
	//}

	//============================================
	//{
	//	jlong* dates = new jlong[InNotify.Num()];
	//	for (int32 i = 0; i < InNotify.Num(); i++)
	//	{
	//		dates[i] = InNotify[i].NotifyStep;
	//	}
	//
	//	//============================================
	//	env->SetLongArrayRegion(NotifyStep, 0, InNotify.Num(), dates);
	//
	//	//============================================
	//	delete[] dates;
	//}

	//============================================
	{
		jlong* dates = new jlong[InNotify.Num()];
		for (int32 i = 0; i < InNotify.Num(); i++)
		{
			dates[i] = InNotify[i].GetNotifyDate();
		}

		//============================================
		env->SetLongArrayRegion(NotifyDate, 0, InNotify.Num(), dates);

		//============================================
		delete[] dates;
	}

	//============================================
	for (int32 i = 0; i < InNotify.Num(); i++)
	{
		env->SetObjectArrayElement(Id, i, env->NewStringUTF(TCHAR_TO_UTF8(*InNotify[i].Id)));
		env->SetObjectArrayElement(Title, i, env->NewStringUTF(TCHAR_TO_UTF8(*InNotify[i].Title)));
		env->SetObjectArrayElement(Message, i, env->NewStringUTF(TCHAR_TO_UTF8(*InNotify[i].Message)));
	}

	//============================================
	for (int32 i = 0; i < InNotifyToCancel.Num(); i++)
	{
		env->SetObjectArrayElement(NotifyToCancel, i, env->NewStringUTF(TCHAR_TO_UTF8(*InNotifyToCancel[i])));
	}
		
	//============================================
	FJavaWrapper::CallObjectMethod(env, FJavaWrapper::GameActivityThis, FJNIAndroidNotify::JNIMethodPtr, Id, Title, Message, NotifyDate, NotifyToCancel);

	//============================================
	for (int32 i = 0; i < InNotify.Num(); i++)
	{
		env->DeleteLocalRef(env->GetObjectArrayElement(Id, i));
		env->DeleteLocalRef(env->GetObjectArrayElement(Title, i));
		env->DeleteLocalRef(env->GetObjectArrayElement(Message, i));
	}

	//============================================
	for (int32 i = 0; i < InNotifyToCancel.Num(); i++)
	{
		env->DeleteLocalRef(env->GetObjectArrayElement(NotifyToCancel, i));
	}

	env->DeleteLocalRef(Id);
	env->DeleteLocalRef(Title);
	env->DeleteLocalRef(Message);
	env->DeleteLocalRef(NotifyDate);
	env->DeleteLocalRef(NotifyToCancel);

	//env->DeleteLocalRef(NotifyStepCount);
	//env->DeleteLocalRef(NotifyStep);


	//============================================
	return true;
#else
	return false;
#endif
}
