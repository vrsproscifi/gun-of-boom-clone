//======================================
#include "ShooterGame.h"
#include "SquadComponent.h"

//======================================
#include "Engine/Http/RequestManager.h"

//======================================
#include "Game/Components//NodeComponent/NodeSessionMatch.h"

//======================================
#include "Notify/SMessageBox.h"
#include "Localization/TableBaseStrings.h"

//======================================
#include "Player/BasicPlayerController.h"

#include "Game/GameMapData.h"
#include "Game/GameModeData.h"

#include "GameMode/LobbyGameMode.h"
#include "ShooterGameAnalytics.h"
//#include "CrashlyticsBlueprintLibrary.h"

//======================================
#include "GooglePlayGameVersion.h"
#include "Notify/SNotifyContainer.h"


DEFINE_LOG_CATEGORY(LogSquadComponent);
#define LOCTEXT_NAMESPACE "Squad"

USquadComponent::USquadComponent()
{
	//=========================================================
	//					[ GetSqaudInformation ]
	RequestSquadInformation = CreateRequest(FOperationRequestServerHost::MasterClient, "Squad/GetSqaudInformation", 2.0f, EOperationRequestTimerMethod::CallViaIntervalOrResponse, FTimerDelegate::CreateUObject(this, &USquadComponent::SendRequestSquadInformation));
	RequestSquadInformation->BindObject<FSqaudInformation>(200).AddUObject(this, &USquadComponent::OnSquadInformation);
	RequestSquadInformation->OnFailedResponse.BindUObject(this, &USquadComponent::OnBadNetworkConnection);

	//=========================================================
	//					[ JoinToSearchQueue ]
	RequestStartSearch = CreateRequest(FOperationRequestServerHost::MasterClient, "Squad/JoinToSearchQueue");
	RequestStartSearch->Bind(200).AddUObject(this, &USquadComponent::OnStartSearch);
	RequestStartSearch->Bind(903).AddUObject(this, &USquadComponent::OnRequestStartSearch_MembersNotReady);
	RequestStartSearch->Bind(906).AddUObject(this, &USquadComponent::OnRequestStartSearch_ErrorOnDataBase);
	
	RequestStartSearch->Bind(907).AddUObject(this, &USquadComponent::OnRequestStartSearch_ServerHasInvalidVersion);
	RequestStartSearch->Bind(908).AddUObject(this, &USquadComponent::OnRequestStartSearch_ServerHasUpdating	);
	RequestStartSearch->Bind(909).AddUObject(this, &USquadComponent::OnRequestStartSearch_ServerHasDeleted);


	RequestStartSearch->OnFailedResponse.BindUObject(this, &USquadComponent::OnRequestStartSearch_Failed);

	//=========================================================
	//					[ LeaveFromSearchQueue ]
	RequestStopSearch = CreateRequest(FOperationRequestServerHost::MasterClient, "Squad/LeaveFromSearchQueue");
	RequestStopSearch->OnFailedResponse.BindUObject(this, &USquadComponent::OnBadNetworkConnection);
	RequestStopSearch->Bind(200).AddUObject(this, &USquadComponent::OnStopSearch);
}

void USquadComponent::OnBadNetworkConnection(const FRequestExecuteError& InErrorData)
{	
#if !UE_SERVER
	//======================================================
	//UCrashlyticsBlueprintLibrary::SetField("BadNetworkConnections", ++FShooterGameAnalytics::BadNetworkConnections);

	FShooterGameAnalytics::RecordDesignEvent(FString::Printf(TEXT("UI:Lobby:%s:Fail:Code%d"), *InErrorData.Operation, InErrorData.Status));

	if ((InErrorData.IsTimeout() || InErrorData.IsBadGateway()) && WITH_EDITOR == 0)
	{
		ShowRestarGameErrorMessage
		(
			"USquadComponent::OnBadNetworkConnection",
			NSLOCTEXT("Network", "Network.OnBadNetworkConnection.Title", "Problem with internet connection"),
			FText::Format(NSLOCTEXT("Network", "Network.OnBadNetworkConnection.Message", "The request to the server could not be completed.\n The problem with the Internet connection.\n Do you want return to the lobby?"), FText::FromString(InErrorData.ToString()))
		);

		UE_LOG(LogInit, Error, TEXT("> [USquadComponent::OnBadNetworkConnection] \nError: %s"), *InErrorData.ToString());
	}
#endif
}

void USquadComponent::GetLifetimeReplicatedProps(TArray<FLifetimeProperty>& OutLifetimeProps) const
{
	Super::GetLifetimeReplicatedProps(OutLifetimeProps);

	//DOREPLIFETIME(USquadComponent, SqaudInformation);
	DOREPLIFETIME_CONDITION(USquadComponent, SqaudInformation, COND_OwnerOnly);
	DOREPLIFETIME_CONDITION(USquadComponent, LastSqaudInformation, COND_OwnerOnly);
}


void USquadComponent::OnAuthorizationComplete()
{
	StartTimer(RequestSquadInformation);
}

void USquadComponent::SendRequestStartSearch(const FSessionMatchOptions& InOptions)
{
	SendRequestStartSearchExecutor(InOptions);
	//if (auto tm = GetTimerManager())
	//{
	//	FTimerDelegate dlg;
	//
	//	//=======================================
	//	dlg.BindLambda([&, InOptions]()
	//	{
	//		SendRequestStartSearchExecutor(InOptions);
	//	});
	//
	//	//=======================================
	//	tm->SetTimer(RequestStartSearchTimerHandle, dlg, 0.250f, false, 0.250f);
	//}
	//else
	//{
	//	SendRequestStartSearchExecutor(InOptions);
	//}
}

void USquadComponent::SendRequestStartSearchExecutor(FSessionMatchOptions InOptions)
{
	//=======================================
	InOptions.TargetServerVersion = GAME_TARGET_SERVER;

	//=======================================
	RequestStartSearch->SendRequestObject(InOptions);

	//=======================================
	if (auto tm = GetTimerManager())
	{
		tm->ClearTimer(RequestStartSearchTimerHandle);
	}
}

void USquadComponent::SendRequestToggleSearch(const FSessionMatchOptions& InOptions, const bool& InIsStartSearch)
{
	//=======================================================
	//UCrashlyticsBlueprintLibrary::SetField("InIsStartSearch", InIsStartSearch);

	//=======================================================
	if (InIsStartSearch)
	{
		//UCrashlyticsBlueprintLibrary::WriteLog("USquadComponent:SendRequestStartSearch");

		if ((SqaudInformation.Status == ESearchSessionStatus::None || SqaudInformation.Status == ESearchSessionStatus::SearchPrepare) && (SqaudInformation.IsReady == false || SqaudInformation.IsLeader))
		{
			FShooterGameAnalytics::RecordMatchPerSession();
			//UCrashlyticsBlueprintLibrary::SetField("MatchesPerSession", FShooterGameAnalytics::MatchesPerSession);

			SqaudInformation.Status = ESearchSessionStatus::SearchStarting;
			SendRequestStartSearch(InOptions);
		}
	}
	else if (SqaudInformation.IsAllowCancelSearch())
	{
		//UCrashlyticsBlueprintLibrary::WriteLog("USquadComponent:RequestStopSearch");

		SqaudInformation.Status = ESearchSessionStatus::SearchStopping;
		RequestStopSearch->GetRequest();
	}

	UE_LOG(LogSquadComponent, Log, TEXT("OldStatus: %d, NewStatus: %d | InIsStartSearch %d"), static_cast<int32>(LastSqaudInformation.Status), static_cast<int32>(SqaudInformation.Status), InIsStartSearch);
	OnRep_SqaudInformation();
}

void USquadComponent::SendRequestSquadInformation_Implementation()
{
	RequestSquadInformation->GetRequest();
}


void USquadComponent::OnSquadInformation(const FSqaudInformation& InInformation)
{
	if (SqaudInformation != InInformation)
	{
		LastSqaudInformation = SqaudInformation;
		SqaudInformation = InInformation;
		OnRep_SqaudInformation();

		if (SqaudInformation.IsAllowJoinMatch() && IsAllowServerQueries() && IsAllowTravel())
		{
			if (auto MyController = GetBaseController())
			{
				MyController->PlayerTravel(SqaudInformation.Host, SqaudInformation.Token);
			}
		}
	}
}

bool USquadComponent::IsAllowTravel() const
{
	UE_LOG(LogInit, Display, TEXT("USquadComponent::IsAllowTravel | ServerId[%s] | TokenId[%s] | Alllow: %d"), *SqaudInformation.ServerId.ToString(), *FNodeSessionMatch::TokenId.ToString(), SqaudInformation.ServerId.Source != FNodeSessionMatch::TokenId);
	return SqaudInformation.ServerId.Source != FNodeSessionMatch::TokenId;
}

void USquadComponent::OnStartSearch()
{
	OnClientStartSearch();
}

void USquadComponent::OnStopSearch()
{
	OnClientStopSearch();
}

void USquadComponent::OnClientStartSearch_Implementation()
{
	//SMessageBox::Get()->SetHeaderText(LOCTEXT("Notify.MatchMaking.Title", "Match information"));
	//if (SqaudInformation.IsLeader)
	//{
	//	SMessageBox::Get()->SetContent(LOCTEXT("Notify.OnClientStartSearch.Content", "The search of fight is started."));
	//}
	//else
	//{
	//	SMessageBox::Get()->SetContent(LOCTEXT("Notify.OnClientStartSearch_Member.Content", "You have activated the ready for battle mode. Expect as squad leader will start searching for a fight!"));
	//}

	//SMessageBox::Get()->SetButtonsText(FTableBaseStrings::GetBaseText(EBaseStrings::Cancel));
	//SMessageBox::Get()->SetEnableClose(false);
	//SMessageBox::Get()->OnMessageBoxButton.AddLambda([&](const EMessageBoxButton& Button) 
	//{
	//	RequestStopSearch->GetRequest();
	//	SMessageBox::Get()->ToggleWidget(false);
	//});
	//SMessageBox::Get()->ToggleWidget(true);
}

void USquadComponent::OnRequestStartSearch_ServerHasInvalidVersion_Implementation()
{
	SNotifyContainer::ShowNotify("ServerHasInvalidVersion", NSLOCTEXT("Notify", "Notify.OnRequestStartSearch.ServerHasInvalidVersion", "Could not start search for battle. Game server is temporarily unavailable #1. Check updates for the game or check back later."));
}

void USquadComponent::OnRequestStartSearch_ServerHasUpdating_Implementation()
{
	SNotifyContainer::ShowNotify("ServerHasUpdating", NSLOCTEXT("Notify", "Notify.OnRequestStartSearch.ServerHasUpdating", "Could not start search for battle. Game server is temporarily unavailable #2. Check updates for the game or check back later."));
}

void USquadComponent::OnRequestStartSearch_ServerHasDeleted_Implementation()
{
	SNotifyContainer::ShowNotify("ServerHasDeleted", NSLOCTEXT("Notify", "Notify.OnRequestStartSearch.ServerHasDeleted", "Could not start search for battle. Game server is temporarily unavailable #3. Check updates for the game or check back later."));
}

void USquadComponent::OnRequestStartSearch_Failed_Implementation(const FRequestExecuteError &InErrorData)
{
	FShooterGameAnalytics::RecordDesignEvent("UI:Lobby:IntoFight:Fail:Code" + InErrorData.GetStatusCode());

	UE_LOG(LogSquadComponent, Display, TEXT("USquadComponent::OnRequestStartSearch_Failed_Implementation: %s"), *InErrorData.ToString());
	OnBadNetworkConnection(InErrorData);
}

void USquadComponent::OnRequestStartSearch_ErrorOnDataBase_Implementation()
{
	SNotifyContainer::ShowNotify("ErrorOnDataBase", NSLOCTEXT("Notify", "Notify.OnRequestStartSearch.ErrorOnDataBase", "Could not start search for battle. Game server is temporarily unavailable #4."));
}

void USquadComponent::OnRequestStartSearch_MembersNotReady_Implementation()
{
	//SMessageBox::Get()->SetHeaderText(LOCTEXT("Notify.MatchMaking.Title", "Match information"));
	//SMessageBox::Get()->SetContent(LOCTEXT("Notify.OnRequestStartSearch.MembersNotReady.Content", "One or more members of the squad are not ready for battle"));

	//SMessageBox::Get()->OnMessageBoxButton.AddLambda([&](const EMessageBoxButton& Button)
	//{
	//	SMessageBox::Get()->ToggleWidget(false);
	//});
	//SMessageBox::Get()->ToggleWidget(true);

}


void USquadComponent::OnClientStopSearch_Implementation()
{
	//SMessageBox::Get()->SetHeaderText(LOCTEXT("Notify.MatchMaking.Title", "Match information"));
	//SMessageBox::Get()->SetContent(LOCTEXT("Notify.OnClientStopSearch.Content", "The search of fight is canceled"));
	//SMessageBox::Get()->OnMessageBoxButton.AddLambda([&](const EMessageBoxButton& Button)
	//{
	//	SMessageBox::Get()->ToggleWidget(false);
	//});
	//SMessageBox::Get()->ToggleWidget(true);
}

static TArray<FString> GetSquadMemberNames(const TArray<FQueueMemberContainer>& members)
{
	TArray<FString> names;
	//for(const auto &m : members)
	//{
	//	names.Add(FRichHelpers::TextHelper_NotifyValue.SetValue(m.Name).ToString());
	//}
	return names;
}

static FString GetSquadMemberName(const TArray<FQueueMemberContainer>& members)
{
	return FString::Join(GetSquadMemberNames(members), TEXT(","));
}

void USquadComponent::OnClientMatchMakingProgress_Implementation()
{
	//==========================================================
	//{
	//	TArray<FQueueMemberContainer> join, leave;
	//	//----------------------------------------
	//	auto& current = SqaudInformation.Members;
	//	auto& last = LastSqaudInformation.Members;
	//
	//	//----------------------------------------
	//	for (const auto &m : current)
	//	{
	//		if (last.ContainsByPredicate([&, m](const FQueueMemberContainer& mm) { return mm.PlayerId == m.PlayerId; }) == false)
	//		{
	//			join.Add(m);
	//		}
	//	}
	//
	//	//----------------------------------------
	//	for (const auto &m : last)
	//	{
	//		if (current.ContainsByPredicate([&, m](const FQueueMemberContainer& mm) { return mm.PlayerId == m.PlayerId; }) == false)
	//		{
	//			leave.Add(m);
	//		}
	//	}
	//
	//	//----------------------------------------
	//	if (!!join.Num())
	//	{
	//		FNotifyInfo NotifyInfo;
	//		const auto name = GetSquadMemberName(join);
	//		NotifyInfo.Title = LOCTEXT("Notify.Squad.Join.Title", "Squad information");
	//		NotifyInfo.Content = FText::Format(LOCTEXT("Notify.Squad.Join.Content", "\"{0}\" joined in the squad"), FText::FromString(name));
	//		AddNotify(NotifyInfo);
	//	}
	//
	//	//----------------------------------------
	//	if (!!leave.Num())
	//	{
	//		FNotifyInfo NotifyInfo;
	//		const auto name = GetSquadMemberName(leave);
	//		NotifyInfo.Title = LOCTEXT("Notify.Squad.Join.Title", "Squad information");
	//		NotifyInfo.Content = FText::Format(LOCTEXT("Notify.Squad.Leave.Content", "\"{0}\" leaves from the squad"), FText::FromString(name));
	//		AddNotify(NotifyInfo);
	//	}
	//}

	//==========================================================
	if (SqaudInformation.Status == ESearchSessionStatus::None || SqaudInformation.Status == ESearchSessionStatus::End)
	{
		return;
	}

	//==========================================================
	//const auto localGameMap = FRichHelpers::TextHelper_NotifyValue.SetValue(SqaudInformation.Options.GetGameMap().GetDisplayName()).ToText();
	//const auto localGameMode = FRichHelpers::TextHelper_NotifyValue.SetValue(SqaudInformation.Options.GetGameMode().GetDisplayName()).ToText();

	//FNotifyInfo NotifyInfo;
	//==========================================================
	//NotifyInfo.Title = LOCTEXT("Notify.MatchMaking.Title", "Match information");

	switch (SqaudInformation.Status)
	{
	case ESearchSessionStatus::SearchPrepare:
		//if (SqaudInformation.IsLeader)
		//{
		//	NotifyInfo.Content = LOCTEXT("Notify.SearchPrepare.Content.Leader", "Waiting answer from squad members, not all squad members is ready.");
		//}
		//else
		//{
		//	NotifyInfo.Content = LOCTEXT("Notify.SearchPrepare.Content.Memeber", "Squad leader requested a search of the fight.");
		//}
		break;
	case ESearchSessionStatus::FoundJoin:
		//NotifyInfo.Content = FText::Format(LOCTEXT("Notify.OnClientMatchMakingProgress.FoundJoin.Content", "Entering into the fight.\nGame mode: {0}\nGame map: {1}\n\nStart the loading should happen within few seconds."), localGameMode, localGameMap);
		break;
	case ESearchSessionStatus::FoundPrepare:
		if (auto map = UGameMapData::GetById(SqaudInformation.Options.GameMap))
		{
			//=======================================================
			//UCrashlyticsBlueprintLibrary::SetField("Match.LastMap", "Lobby");
			//UCrashlyticsBlueprintLibrary::SetField("Match.CurrentMap", map->GetName());
			//UCrashlyticsBlueprintLibrary::SetField("Match.TravelMap", map->GetName());
			//UCrashlyticsBlueprintLibrary::SetField("Match.MatchId", SqaudInformation.ServerId);
			//UCrashlyticsBlueprintLibrary::SetField("Match.Token", SqaudInformation.Token);

			//=======================================================
			LoadPackageAsync(FString::Printf(TEXT("Game/1LOKAgame/Maps/%s.umap"), *map->GetName()), FLoadPackageAsyncDelegate(), 0);
			FShooterGameAnalytics::RecordDesignEvent(FString::Printf(TEXT("Match:GameMap:%s"), *map->GetName()));
			FShooterGameAnalytics::RecordDesignEvent("Match:GameMode:TDM");
		}

		//NotifyInfo.Content = FText::Format(LOCTEXT("Notify.OnClientMatchMakingProgress.FoundPrepare.Content", "Search game ended, preparation of entering into the fight.\nGame mode: {0}\nGame map: {1}"), localGameMode, localGameMap);
		break;
	case ESearchSessionStatus::FoundError:
		FShooterGameAnalytics::RecordDesignEvent("UI:Lobby:IntoFight:FoundError");
		FShooterGameAnalytics::RecordErrorEvent(EErrorSeverity::error, "An error occurred while searching of the fight.");
		//NotifyInfo.Content = LOCTEXT("Notify.OnClientMatchMakingProgress.FoundError.Content", "An error occurred while searching of the fight.");
		break;
	case ESearchSessionStatus::NotEnouthMoney:
		FShooterGameAnalytics::RecordDesignEvent("UI:Lobby:IntoFight:NotEnouthMoney");
		//NotifyInfo.Content = LOCTEXT("Notify.OnClientMatchMakingProgress.NotEnouthMoney.Content", "Not enough money to participate in the fight.");
		break;
	default:
		return;
	}

	//AddNotify(NotifyInfo);
}

void USquadComponent::OnRep_SqaudInformation()
{
	OnSquadUpdate.Broadcast(SqaudInformation);

	switch (SqaudInformation.Status) 
	{ 
		case ESearchSessionStatus::FoundJoin:
		case ESearchSessionStatus::FoundContinue: 
		case ESearchSessionStatus::FoundPrepare:
			EventOnSearchFound.Broadcast();
		break;
		case ESearchSessionStatus::MatchSearch:
		case ESearchSessionStatus::SearchStarting:
		case ESearchSessionStatus::SearchPrepare: 
			EventOnStartSearch.Broadcast();
		break;
		case ESearchSessionStatus::SearchStopping:
		case ESearchSessionStatus::None:
			EventOnStopSearch.Broadcast();
		break;
	}

	if (HasLocalClientComponent())
	{
		OnClientMatchMakingProgress();
	}
}

bool USquadComponent::IsAllowServerQueries() const
{
	if (auto world = GetValidWorld())
	{
		return !!world->GetAuthGameMode<ALobbyGameMode>();
	}
	return false;
}



#undef LOCTEXT_NAMESPACE