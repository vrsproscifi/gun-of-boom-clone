// Fill out your copyright notice in the Description page of Project Settings.

#include "ShooterGame.h"
#include "TutorialPlayerComponent.h"
#include "Notify/SMessageBox.h"
#include "TutorialData.h"
#include "Decorators/DecoratorHelpers.h"
#include "IdentityComponent.h"
#include "Notify/SNotifyContainer.h"
#include "GameInstance/ShooterGameViewportClient.h"
#include "OnlinePlayerState.h"

UTutorialPlayerComponent::UTutorialPlayerComponent()
	: Super()
{
	LastStage = 0;
	CurrentStage = 0;
	bIsStarted = false;
}

bool UTutorialPlayerComponent::ForceStage_Implementation(const int32& InStage)
{
	if (bIsStarted && IsValidObject(CurrentData) && CurrentData->GetStages().IsValidIndex(InStage))
	{
		const FTutorialInfo& StageData = CurrentData->GetStage(InStage);
		if (StageData.ShowAs != ETutorialInfoShowAs::None)
		{
			CurrentStage = InStage;

			if (StageData.ShowAs == ETutorialInfoShowAs::Notify || StageData.ShowAs == ETutorialInfoShowAs::Message || StageData.ShowAs == ETutorialInfoShowAs::Tip)
			{
				FFormatNamedArguments FormatNamedArguments;

				if (auto MyPS = GetBasicPlayerState<AOnlinePlayerState>())
				{
					if (auto MyIdentity = MyPS->GetIdentityComponent())
					{
						FTextDecoratorHelper DecoratorHelperValue = FTextDecoratorHelper().SetColor(FColorList::OrangeRed);

						FormatNamedArguments.Add(TEXT("PlayerName"), DecoratorHelperValue.SetValue(MyIdentity->GetPlayerInfo().Login).ToText());
						FormatNamedArguments.Add(TEXT("PlayerMoney"), FResourceDecoratorHelper().SetType(EResourceTextBoxType::Money).SetAmount(MyIdentity->GetPlayerInfo().TakeCash(EGameCurrency::Money)).ToText());
						FormatNamedArguments.Add(TEXT("PlayerDonate"), FResourceDecoratorHelper().SetType(EResourceTextBoxType::Donate).SetAmount(MyIdentity->GetPlayerInfo().TakeCash(EGameCurrency::Donate)).ToText());
					}
				}

				if (StageData.ShowAs == ETutorialInfoShowAs::Notify)
				{
					FName QueueName = *FString::Printf(TEXT("Tutorial_%d"), CurrentStage);
					QueueBegin(SNotifyContainer, QueueName, stage = StageData, args = FormatNamedArguments)
						SNotifyContainer::Get()->SetContent(FText::Format(stage.Message, args));
					SNotifyContainer::Get()->SetDisplayTime(40.0f);
					SNotifyContainer::Get()->ToggleWidget(true);
					QueueEnd
				}
				else if (StageData.ShowAs == ETutorialInfoShowAs::Message)
				{
					FName QueueName = *FString::Printf(TEXT("Tutorial_%d"), CurrentStage);
					QueueBegin(SMessageBox, QueueName, stage = StageData, args = FormatNamedArguments, uName = CurrentData->GetTitle())
						SMessageBox::Get()->SetHeaderText(FText::Format(stage.Title.IsEmpty() ? uName : stage.Title, args));
						SMessageBox::Get()->SetContent(FText::Format(stage.Message, args), stage.MessageJustification);
						SMessageBox::Get()->SetButtonsText(FTableBaseStrings::GetBaseText(EBaseStrings::Continue));
						SMessageBox::Get()->OnMessageBoxButton.AddLambda([&](EMessageBoxButton) {
							SMessageBox::Get()->ToggleWidget(false);
						});
						SMessageBox::Get()->SetEnableClose(true);
						if (stage.Sounds[ETutorialInfoSound::Show].GetResourceObject())	SMessageBox::Get()->SetSound(stage.Sounds[ETutorialInfoSound::Show], true);
						if (stage.Sounds[ETutorialInfoSound::Hide].GetResourceObject())	SMessageBox::Get()->SetSound(stage.Sounds[ETutorialInfoSound::Hide], false);
						SMessageBox::Get()->ToggleWidget(true);
					QueueEnd
				}
			}

			if (StageData.Sounds[ETutorialInfoSound::Voice].GetResourceObject())
			{
				FSlateApplication::Get().PlaySound(StageData.Sounds[ETutorialInfoSound::Voice]);
			}

			auto MyViewport = UShooterGameViewportClient::Get();
			if (StageData.WidgetsHighlightList.Num() && MyViewport)
			{
				for (auto MyTarget : StageData.WidgetsHighlightList)
				{
					MyViewport->HighlightWidgetByTag(MyTarget.Key, MyTarget.Value);
				}
			}

			if (StageData.WidgetsUnhighlightList.Num() && MyViewport)
			{
				for (auto MyTarget : StageData.WidgetsUnhighlightList)
				{
					MyViewport->UnHighlightWidgetByTag(MyTarget);
				}
			}

			if ((StageData.TargetAction == ETutorialAction::Skip || StageData.TargetAction == ETutorialAction::Time) && GetWorld())
			{
				GetWorld()->GetTimerManager().SetTimer(timer_TutorialTime, this, &UTutorialPlayerComponent::EndCurrentStage_Internal, StageData.TargetAction == ETutorialAction::Time ? StageData.CompleteSpecifer : 5.0f, false);
			}

			return true;
		}		
	}
	else if (bIsStarted && IsValidObject(CurrentData) && CurrentData->GetStagesNum() <= InStage)
	{
		return EndTutorial();
	}

	return false;
}

void UTutorialPlayerComponent::EndCurrentStage_Internal()
{
	EndCurrentStage();
}

bool UTutorialPlayerComponent::StartTutorial_Implementation(UTutorialData* InData, const int32& InStage)
{
	if (bIsStarted == false)
	{
		CurrentData = InData;

		bIsStarted = true;
		CurrentStage = InStage;
		ForceStage(CurrentStage);
		return true;
	}

	return false;
}

bool UTutorialPlayerComponent::EndCurrentStage_Implementation()
{
	if (IsValidObject(CurrentData))
	{
		if (GetCurrentStage() < CurrentData->GetStagesNum() - 1)
		{
			LastStage = CurrentStage;
			return ForceStage(CurrentStage + 1);
		}

		return EndTutorial();
	}

	return false;
}

bool UTutorialPlayerComponent::EndTutorial_Implementation()
{
	if (bIsStarted == true && IsValidObject(CurrentData))
	{
		bIsStarted = false;
		if (GetCurrentStage() < CurrentData->GetStagesNum() - 1)
		{
			EndCurrentStage();
		}
		return true;
	}
	return false;
}

void UTutorialPlayerComponent::ForceHide_Implementation()
{
	FName QueueName = *FString::Printf(TEXT("Tutorial_%d"), CurrentStage);
	if (SMessageBox::Get()->GetQueueId() == QueueName) SMessageBox::Get()->ToggleWidget(false);
}
