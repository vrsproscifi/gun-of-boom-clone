// Fill out your copyright notice in the Description page of Project Settings.

#include "ShooterGame.h"
#include "LookOnComponent.h"
#include "UObjectExtensions.h"

static TAutoConsoleVariable<int32> CVarEnableDrawDebugLookOn(
	TEXT("EnableDrawDebugLookOn"),
	0,
	TEXT("(0: Disable, 1: Enable) Enable or disable debug drawing points, lines & text on ULookOnComponent"),
	ECVF_Default);

ULookOnComponent::ULookOnComponent()
{
	PrimaryComponentTick.bCanEverTick = true;

	SetFilterClass(APawn::StaticClass());
	SetTargetRange(250.0f);
	SetTargetVisionAngle(60.0f);
	SetTargetingSpeed(5.0f);
	SetTargetCollisionChannel(ECC_Pawn);
	SetTargetingPriorityDistance(100.0f);
	SetTargetingAlertDistance(20.0f);

	ReFindingCounter = .0f;

	TargetActor = nullptr;
	LastTargetActor = nullptr;
}

void ULookOnComponent::PostInitProperties()
{
	Super::PostInitProperties();

	SetTargetVisionAngle(PeripheralVisionAngle);
}

void ULookOnComponent::TickComponent(float DeltaTime, ELevelTick TickType, FActorComponentTickFunction* ThisTickFunction)
{
	Super::TickComponent(DeltaTime, TickType, ThisTickFunction);

	if (IsActive())
	{
		if (bIsAutoFinding)
		{
			ReFindingCounter += DeltaTime;

			if (ReFindingCounter >= AutoFindingInterval)
			{
				ReFindingCounter = .0f;
				RequestFindingTarget();
			}
		}

		if (TargetActor)
		{
			auto OwnerAsPawn = GetValidObjectAs<APawn>(GetOwner());
			if (OwnerAsPawn && OwnerAsPawn->Controller)
			{
				auto TargetRotation = FRotationMatrix::MakeFromX(TargetActor->GetActorLocation() - GetOwner()->GetActorLocation()).Rotator();
				auto CurrentRotation = OwnerAsPawn->GetControlRotation();
				auto Dist = FVector::Dist(TargetActor->GetActorLocation(), GetOwner()->GetActorLocation());
				auto DistFactor = 1.0f - (Dist / GetSourceRange());
				const float RotationDot = FVector::DotProduct(TargetRotation.Vector(), CurrentRotation.Vector());

				if (CVarEnableDrawDebugLookOn.GetValueOnGameThread())
				{
					GEngine->AddOnScreenDebugMessage(10, 10.0f, FColorList::LimeGreen, *FString::Printf(TEXT("FRotator >> CurrentRotation: %s, TargetRotation: %s"), *CurrentRotation.ToString(), *TargetRotation.ToString()));
					GEngine->AddOnScreenDebugMessage(12, 10.0f, FColorList::LimeGreen, *FString::Printf(TEXT("FVector  >> CurrentRotation: %s, TargetRotation: %s"), *CurrentRotation.Vector().ToString(), *TargetRotation.Vector().ToString()));

					GEngine->AddOnScreenDebugMessage(13, 10.0f, FColorList::OrangeRed, *FString::Printf(TEXT("DistFactor: %.2f"), DistFactor));
					GEngine->AddOnScreenDebugMessage(14, 10.0f, FColorList::OrangeRed, *FString::Printf(TEXT("Dist: %.2f"), Dist));
					GEngine->AddOnScreenDebugMessage(15, 10.0f, FColorList::OrangeRed, *FString::Printf(TEXT("RotationDot: %.2f"), RotationDot));
				}
							
				if (FMath::IsNearlyEqual(RotationDot, 1.0f, .001f * DistFactor) == false && FMath::IsNearlyEqual(DistFactor, 1.0f, .05f) == false)
				{
					const float TargetSpeed = RotationDot * InterpRotationSpeed * DistFactor;
					TargetRotation = FMath::RInterpTo(CurrentRotation, TargetRotation, DeltaTime, TargetSpeed);
					OwnerAsPawn->Controller->SetControlRotation(TargetRotation);
				}
				else
				{
					const float TargetSpeed = RotationDot * InterpRotationSpeed * DistFactor;
					TargetRotation = FMath::RInterpTo(CurrentRotation, TargetRotation, DeltaTime, TargetSpeed / 5.0f);
					OwnerAsPawn->Controller->SetControlRotation(TargetRotation);
				}
			}
		}
	}
}

void ULookOnComponent::SetFilterClass(TSubclassOf<AActor> InClass)
{
	FilterClass = InClass;
}

void ULookOnComponent::SetTargetRange(const float InDistance)
{
	TraceDistance = InDistance;
}

void ULookOnComponent::SetTargetVisionAngle(const float InDegress)
{
	PeripheralVisionAngle = InDegress;
	PeripheralVisionCosine = /*FMath::Cos(*/FMath::DegreesToRadians(GetSourceAngle());
}

void ULookOnComponent::SetTargetingSpeed(float InSpeed) 
{
	InterpRotationSpeed = InSpeed;
}

void ULookOnComponent::SetTargetingAutoFinding(float InInterval)
{
	bIsAutoFinding = !(FMath::IsNearlyZero(InInterval) || FMath::IsNegativeFloat(InInterval));
	AutoFindingInterval = InInterval;
}

void ULookOnComponent::SetTargetingPriorityDistance(float InDistance)
{
	PriorityDistance = InDistance;
}

void ULookOnComponent::SetTargetingAlertDistance(float InDistance)
{
	AlertDistance = InDistance;
}

void ULookOnComponent::SetTargetCollisionChannel(const ECollisionChannel InCollisionChannel)
{
	TargetCollisionChannel = InCollisionChannel;
}

void ULookOnComponent::RequestFindingTarget()
{
	TargetActor = FindTarget();
}

AActor* ULookOnComponent::FindTarget()
{
	// Sphere sweep for find targets on forward vector
	// Trace's on found actor to detect is real visibily
	// Set variable to closest actor and help rotate to target

	PeripheralVisionCosine = FMath::Cos(FMath::DegreesToRadians(GetSourceAngle()));

	AActor* Target = nullptr;

	if (CheckTarget(LastTargetActor))
	{
		return LastTargetActor;
	}

	float LastScore = 0.0f;
	LastPecentageOfCenterCone = .0f;

	if (FilterClass.Get()->IsChildOf(APawn::StaticClass()))
	{
		for (FConstPawnIterator It = GetWorld()->GetPawnIterator(); It; ++It)
		{
			if (It->Get() && It->Get()->GetClass()->IsChildOf(FilterClass) && It->Get() != GetOwner())
			{
				const float CurrentScore = GetTargetScore(It->Get());
				if (CurrentScore > LastScore)
				{
					LastScore = CurrentScore;
					Target = It->Get();
				}
			}
		}
	}
	else
	{
		TArray<FOverlapResult> OverlapResults;
		TArray<FHitResult> HitResults;
		FCollisionQueryParams localQueryParams = FCollisionQueryParams(TEXT("LookOn_Main"), false, GetOwner());
		FCollisionResponseParams localResponseParams = FCollisionResponseParams::DefaultResponseParam;
		localResponseParams.CollisionResponse.SetAllChannels(ECR_Ignore);
		localResponseParams.CollisionResponse.SetResponse(TargetCollisionChannel, ECR_Overlap);

		FRotator localOwnerRotation = GetOwner()->GetActorForwardVector().Rotation();
		FVector localOwnerLocation = GetOwner()->GetActorLocation();
		if (auto OwnerIsPawn = GetValidObjectAs<APawn>(GetOwner()))
		{
			OwnerIsPawn->GetActorEyesViewPoint(localOwnerLocation, localOwnerRotation);
		}

		FVector localStartTrace = localOwnerLocation;
		FCollisionShape localShape = FCollisionShape::MakeSphere(GetSourceRange());		

		GetWorld()->OverlapMultiByChannel(OverlapResults, localStartTrace, FQuat::Identity, TargetCollisionChannel, localShape, localQueryParams, localResponseParams);

		if (CVarEnableDrawDebugLookOn.GetValueOnGameThread())
			::DrawDebugSphere(GetWorld(), localStartTrace, localShape.GetSphereRadius(), 8, FColorList::LimeGreen, false, AutoFindingInterval, 0, 2);

		for (auto HitResult : OverlapResults)
		{
			if (HitResult.GetActor() && HitResult.GetActor()->GetClass()->IsChildOf(FilterClass))
			{
				if (CheckTarget(HitResult.GetActor()))
				{
					Target = HitResult.GetActor();
				}
			}
		}
	}

	return Target;
}

static void Visibility__GetRenderedActors(APlayerController* pc, TArray<ACharacter*>& CurrentlyRenderedActors, float MinRecentTime = 0.01f)
{
	//Empty any previous entries
	CurrentlyRenderedActors.Empty();

	//Iterate Over Actors
	for (TObjectIterator<ACharacter> Itr; Itr; ++Itr)
	{
		FVector2D tmp;
		if (pc->ProjectWorldLocationToScreen(Itr->GetActorLocation(), tmp))
		{
			CurrentlyRenderedActors.Add(*Itr);
		}
	}
}

bool ULookOnComponent::CheckTarget(AActor* InTargetActor)
{
	if (InTargetActor && InTargetActor->IsValidLowLevel())
	{
		float PercentageOfCenterCone = .0f;
		const auto TargetActorLocation = InTargetActor->GetActorLocation();		

		if (IsPointInCone(TargetActorLocation, PercentageOfCenterCone))
		{
			FRotator localOwnerRotation;
			FVector localOwnerLocation;

			GetOwnerViewPoint(localOwnerLocation, localOwnerRotation);
			
			FHitResult SubHitResult;
			FCollisionQueryParams SubQueryParams(TEXT("LookOn_Visibility"), true, GetOwner());

			FCollisionResponseParams SubResponseParams = FCollisionResponseParams::DefaultResponseParam;
			SubResponseParams.CollisionResponse.SetAllChannels(ECR_Ignore);
			SubResponseParams.CollisionResponse.SetResponse(ECC_Visibility, ECR_Block);
			SubResponseParams.CollisionResponse.SetResponse(ECC_WorldStatic, ECR_Block);
			SubResponseParams.CollisionResponse.SetResponse(ECC_WorldDynamic, ECR_Block);
			SubResponseParams.CollisionResponse.SetResponse(TargetCollisionChannel, ECR_Block);

			if (GetWorld()->LineTraceSingleByChannel(SubHitResult, localOwnerLocation, TargetActorLocation, TargetCollisionChannel, SubQueryParams, SubResponseParams))
			{
				bool bExternalCheck = false;
				if (OnExternalTargetCheck.IsBound())
				{
					bExternalCheck = OnExternalTargetCheck.Execute(SubHitResult.GetActor());
				}

				if (CVarEnableDrawDebugLookOn.GetValueOnGameThread())
					::DrawDebugPoint(GetWorld(), SubHitResult.Location, 10.0f, bExternalCheck ? FColor::Red : FColor::Green, false, AutoFindingInterval, SDPG_Foreground);
			
				if (bExternalCheck && SubHitResult.GetActor() && SubHitResult.GetActor()->GetClass()->IsChildOf(FilterClass) && SubHitResult.GetActor() != GetOwner())
				{
					if (CVarEnableDrawDebugLookOn.GetValueOnGameThread())
						GEngine->AddOnScreenDebugMessage(9, 10.0f, FColorList::LimeGreen, *FString::Printf(TEXT("PercentageOfCenterCone: %.2f, VisionAngle: %.2f[%.2f], LastPecentageOfCenterCone: %.2f"), PercentageOfCenterCone, PeripheralVisionCosine, PeripheralVisionAngle, LastPecentageOfCenterCone));
										
					if (FMath::IsNearlyEqual(PercentageOfCenterCone, LastPecentageOfCenterCone, .2f) || PercentageOfCenterCone > LastPecentageOfCenterCone)
					{
						LastTargetActor = SubHitResult.GetActor();
						LastPecentageOfCenterCone = PercentageOfCenterCone;
						return true;
					}/*
					else if (LastTargetActor == InTargetActor && FMath::IsNearlyEqual(PercentageOfCenterCone, LastPecentageOfCenterCone, .5f))
					{
						return true;
					}*/
				}
			}
		}
	}

	return false;
}

float ULookOnComponent::GetTargetScore(AActor* InTargetActor)
{
	float TargetScore = .0f;

	if (InTargetActor && InTargetActor->IsValidLowLevel())
	{
		float PercentageOfCenterCone = .0f;
		const auto TargetActorLocation = InTargetActor->GetActorLocation();

		if (IsPointInCone(TargetActorLocation, PercentageOfCenterCone))
		{
			FRotator localOwnerRotation;
			FVector localOwnerLocation;

			GetOwnerViewPoint(localOwnerLocation, localOwnerRotation);

			FHitResult SubHitResult;
			FCollisionQueryParams SubQueryParams(TEXT("LookOn_Visibility"), true, GetOwner());

			FCollisionResponseParams SubResponseParams = FCollisionResponseParams::DefaultResponseParam;
			SubResponseParams.CollisionResponse.SetAllChannels(ECR_Ignore);
			SubResponseParams.CollisionResponse.SetResponse(ECC_Visibility, ECR_Block);
			SubResponseParams.CollisionResponse.SetResponse(ECC_WorldStatic, ECR_Block);
			SubResponseParams.CollisionResponse.SetResponse(ECC_WorldDynamic, ECR_Block);
			SubResponseParams.CollisionResponse.SetResponse(TargetCollisionChannel, ECR_Block);

			if (GetWorld()->LineTraceSingleByChannel(SubHitResult, localOwnerLocation, TargetActorLocation, TargetCollisionChannel, SubQueryParams, SubResponseParams))
			{
				bool bExternalCheck = false;
				if (OnExternalTargetCheck.IsBound())
				{
					bExternalCheck = OnExternalTargetCheck.Execute(SubHitResult.GetActor());
				}

				if (CVarEnableDrawDebugLookOn.GetValueOnGameThread())
					::DrawDebugPoint(GetWorld(), SubHitResult.Location, 10.0f, bExternalCheck ? FColor::Red : FColor::Green, false, AutoFindingInterval, SDPG_Foreground);

				if (bExternalCheck && SubHitResult.GetActor() && SubHitResult.GetActor()->GetClass()->IsChildOf(FilterClass) && SubHitResult.GetActor() != GetOwner())
				{
					if (CVarEnableDrawDebugLookOn.GetValueOnGameThread())
						GEngine->AddOnScreenDebugMessage(9, 10.0f, FColorList::LimeGreen, *FString::Printf(TEXT("PercentageOfCenterCone: %.2f, VisionAngle: %.2f[%.2f], LastPecentageOfCenterCone: %.2f"), PercentageOfCenterCone, PeripheralVisionCosine, PeripheralVisionAngle, LastPecentageOfCenterCone));

					TargetScore += PercentageOfCenterCone;

					const float SourceTargetDistance = FVector::Distance(SubHitResult.GetActor()->GetActorLocation(), GetOwner()->GetActorLocation());

					const float SourcePriorityDistance = GetTargetingPriorityDistance() * 100.0f;
					const float SourceAlertDistance = GetTargetingAlertDistance() * 100.0f;

					if (SourceTargetDistance <= SourcePriorityDistance)
					{
						TargetScore += 1.0f - (SourceTargetDistance / SourcePriorityDistance);
					}

					if (SourceTargetDistance <= SourceAlertDistance)
					{
						TargetScore += 1.0f - (SourceTargetDistance / SourceAlertDistance);
					}
				}
			}
		}
	}

	return TargetScore;
}

void ULookOnComponent::GetOwnerViewPoint(FVector& OutPoint, FRotator& OutRotation)
{
	if (GetOwner())
	{		
		if (auto OwnerIsPawn = GetValidObjectAs<APawn>(GetOwner()))
		{
			OwnerIsPawn->GetActorEyesViewPoint(OutPoint, OutRotation);
		}
		else
		{
			OutRotation = GetOwner()->GetActorForwardVector().Rotation();
			OutPoint = GetOwner()->GetActorLocation();
		}
	}
}

bool ULookOnComponent::IsPointInCone(const FVector& InPoint, float& OutPercentToCenter)
{
	FRotator localOwnerRotation;
	FVector localOwnerLocation;

	GetOwnerViewPoint(localOwnerLocation, localOwnerRotation);

	const FVector LineEnd = localOwnerRotation.Vector() * GetSourceRange();
	const bool Aswer = FMath::GetDistanceWithinConeSegment(InPoint, localOwnerLocation, LineEnd, 90.0f, GetSourceRange() * PeripheralVisionCosine, OutPercentToCenter);

	if (CVarEnableDrawDebugLookOn.GetValueOnGameThread())
	{
		const FColor TargetColor = FMath::Lerp(FColorList::OrangeRed.ReinterpretAsLinear(), FColorList::ForestGreen.ReinterpretAsLinear(), OutPercentToCenter).ToFColor(false);
		::DrawDebugPoint(GetWorld(), InPoint + FVector::UpVector * 20.0f, 10.0f, TargetColor, false, AutoFindingInterval, SDPG_Foreground);

		GEngine->AddOnScreenDebugMessage(20, 10.0f, FColorList::OrangeRed, *FString::Printf(TEXT("PercentageOfCenterCone: %.2f, LastPecentageOfCenterCone: %.2f"), OutPercentToCenter, LastPecentageOfCenterCone));
	}

	return Aswer;
}
