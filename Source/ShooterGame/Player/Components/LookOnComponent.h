// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "Components/ActorComponent.h"
#include "LookOnComponent.generated.h"


DECLARE_DYNAMIC_DELEGATE_RetVal_OneParam(bool, FOnExternalTargetCheck, AActor*, InActor);

UCLASS( ClassGroup=(Custom), meta=(BlueprintSpawnableComponent) )
class SHOOTERGAME_API ULookOnComponent : public UActorComponent
{
	GENERATED_BODY()
public:
	ULookOnComponent();

	virtual void PostInitProperties() override;
	virtual void TickComponent(float DeltaTime, ELevelTick TickType, FActorComponentTickFunction* ThisTickFunction) override;

	UFUNCTION(BlueprintCallable, Category = Targeting)
		void SetFilterClass(TSubclassOf<AActor> InClass);

	UFUNCTION(BlueprintCallable, Category = Targeting)
		void SetTargetRange(const float InDistance);

	UFUNCTION(BlueprintCallable, Category = Targeting)
		void SetTargetVisionAngle(const float InDegress);

	UFUNCTION(BlueprintCallable, Category = Targeting)
		void SetTargetingSpeed(float InSpeed);

	UFUNCTION(BlueprintCallable, Category = Targeting)
		void SetTargetingAutoFinding(float InInterval);

	UFUNCTION(BlueprintCallable, Category = Targeting)
		void SetTargetingPriorityDistance(float InDistance);

	UFUNCTION(BlueprintCallable, Category = Targeting)
		void SetTargetingAlertDistance(float InDistance);

	UFUNCTION(BlueprintCallable, Category = Targeting)
		void SetTargetCollisionChannel(const ECollisionChannel InCollisionChannel);

	UFUNCTION(BlueprintPure, BlueprintCallable, Category = Targeting)
		FORCEINLINE float GetSourceRange() const { return GetHumanRange() * 100.0f; }

	UFUNCTION(BlueprintPure, BlueprintCallable, Category = Targeting)
		FORCEINLINE float GetHumanRange() const { return TraceDistance; }

	UFUNCTION(BlueprintPure, BlueprintCallable, Category = Targeting)
		FORCEINLINE float GetSourceAngle() const { return GetHumanAngle() / 2.0f; }

	UFUNCTION(BlueprintPure, BlueprintCallable, Category = Targeting)
		FORCEINLINE float GetHumanAngle() const { return PeripheralVisionAngle; }

	UFUNCTION(BlueprintPure, BlueprintCallable, Category = Targeting)
		FORCEINLINE float GetTargetingPriorityDistance() const { return PriorityDistance; }

	UFUNCTION(BlueprintPure, BlueprintCallable, Category = Targeting)
		FORCEINLINE float GetTargetingAlertDistance() const { return AlertDistance; }

	UFUNCTION(BlueprintCallable, Category = Targeting)
		void RequestFindingTarget();

	UFUNCTION(BlueprintCallable, Category = Targeting)
		AActor* FindTarget();

	UPROPERTY(BlueprintReadWrite, Category = Targeting)
		FOnExternalTargetCheck OnExternalTargetCheck;

	bool CheckTarget(AActor* TargetActor);

	float GetTargetScore(AActor* TargetActor);

protected:

	void GetOwnerViewPoint(FVector& OutPoint, FRotator& OutRotation);
	bool IsPointInCone(const FVector& InPoint, float& OutPercentToCenter);

	UPROPERTY(EditDefaultsOnly, Category = Targeting, meta = (DisplayName = "Filter By Class"))
		TSubclassOf<AActor> FilterClass;

	UPROPERTY()
		AActor* TargetActor;

	UPROPERTY()
		AActor* LastTargetActor;

	UPROPERTY(EditDefaultsOnly, Category = Targeting, meta = (DisplayName = "Finding range"))
		float TraceDistance;

	UPROPERTY(EditDefaultsOnly, Category = Targeting, meta = (DisplayName = "Finding cone"))
		float PeripheralVisionAngle;

	UPROPERTY(EditDefaultsOnly, Category = Targeting, meta = (DisplayName = "Targeting Speed"))
		float InterpRotationSpeed;

	UPROPERTY()
		float PeripheralVisionCosine;

	UPROPERTY(EditDefaultsOnly, Category = Targeting, meta = (DisplayName = "Is Auto Re-Finding"))
		bool bIsAutoFinding;

	UPROPERTY(EditDefaultsOnly, Category = Targeting, meta = (DisplayName = "Interval of Auto Re-Finding"))
		float AutoFindingInterval;

	UPROPERTY()
		float ReFindingCounter;

	UPROPERTY()
		float LastDot;

	UPROPERTY()
		float LastPecentageOfCenterCone;

	UPROPERTY(EditDefaultsOnly, Category = Targeting, meta = (DisplayName = "Finding Collision Channel"))
		TEnumAsByte<ECollisionChannel> TargetCollisionChannel;

	UPROPERTY(EditDefaultsOnly, Category = Targeting, meta = (DisplayName = "Priority Distance"))
		float PriorityDistance;

	UPROPERTY(EditDefaultsOnly, Category = Targeting, meta = (DisplayName = "Alert Distance"))
		float AlertDistance;
};
