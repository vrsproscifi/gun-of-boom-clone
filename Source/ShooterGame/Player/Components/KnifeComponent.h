// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "ExecutableGamePlayerItem.h"
#include "KnifeComponent.generated.h"

/**
 * 
 */
UCLASS()
class SHOOTERGAME_API UKnifeComponent : public UExecutableGamePlayerItem
{
	GENERATED_BODY()

public:

	UKnifeComponent();

protected:

	virtual void OnRep_Instance() override;
	virtual void TickComponent(float DeltaTime, ELevelTick TickType, FActorComponentTickFunction* ThisTickFunction) override;

	bool KnifeTrace(FHitResult& OutHitResult) const;

	virtual void OnRep_Activated() override;
	virtual void OnProcess() override;
	virtual bool IsAllowExecuteProcess() const override;
public:

	virtual bool IsAllowStartProcess() const override;
	virtual bool StartProcess() override;
	virtual bool StopProcess() override;

protected:

	UPROPERTY()
	bool bCanUse;

	UPROPERTY(VisibleInstanceOnly)
	USkeletalMeshComponent* Mesh;
};
