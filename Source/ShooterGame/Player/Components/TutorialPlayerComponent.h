// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "Player/BasicPlayerComponent.h"
#include "TutorialPlayerComponent.generated.h"

class UTutorialData;
/**
 * 
 */
UCLASS()
class SHOOTERGAME_API UTutorialPlayerComponent : public UBasicPlayerComponent
{
	GENERATED_BODY()

public:

	UTutorialPlayerComponent();

	UFUNCTION(BlueprintCallable, BlueprintNativeEvent, Category = Stage)
	bool ForceStage(const int32& InStage);

	UFUNCTION(BlueprintCallable, BlueprintPure, Category = Stage)
	FORCEINLINE int32 GetCurrentStage() const { return CurrentStage; }

	UFUNCTION(BlueprintCallable, BlueprintPure, Category = Stage)
	FORCEINLINE UTutorialData* GetCurrentTutorial() const { return CurrentData; }

	UFUNCTION(BlueprintCallable, BlueprintNativeEvent, Category = Stage)
	bool StartTutorial(UTutorialData* InData, const int32& InStage = 0);

	UFUNCTION(BlueprintCallable, BlueprintNativeEvent, Category = Stage)
	bool EndCurrentStage();

	UFUNCTION(BlueprintCallable, BlueprintNativeEvent, Category = Stage)
	bool EndTutorial();

	UFUNCTION(BlueprintCallable, BlueprintNativeEvent, Category = Visual)
	void ForceHide();

protected:

	UPROPERTY(BlueprintReadWrite, Category = Stage)			int32 LastStage;
	UPROPERTY(BlueprintReadWrite, Category = Stage)			int32 CurrentStage;
	UPROPERTY(BlueprintReadWrite, Category = Stage)			bool bIsStarted;
	UPROPERTY(BlueprintReadWrite, Category = Tutorial)		UTutorialData* CurrentData;

	UFUNCTION()
	void EndCurrentStage_Internal();

	FTimerHandle timer_TutorialTime;
};
