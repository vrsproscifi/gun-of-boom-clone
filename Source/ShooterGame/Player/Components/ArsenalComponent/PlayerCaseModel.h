#pragma once
#include "LokaGuid.h"
#include "PlayerCaseModel.generated.h"

USTRUCT()
struct SHOOTERGAME_API FPlayerCaseModel
{
	GENERATED_USTRUCT_BODY()

	FPlayerCaseModel()
		: CaseId(0)
		, LastActiveDate(0)
		, Amount(0)
	{

	}

	UPROPERTY()		FLokaGuid EntityId;

	UPROPERTY()		uint32 CaseId;

	UPROPERTY()		uint64 LastActiveDate;

	UPROPERTY()		int32 Amount;
};

USTRUCT()
struct SHOOTERGAME_API FDropedCaseItemModel
{
	GENERATED_USTRUCT_BODY()

	FDropedCaseItemModel()
		: ItemModelId(0)
		, Amount(0)
	{

	}

	UPROPERTY()		int32 ItemModelId;

	UPROPERTY()		int32 Amount;
};

USTRUCT()
struct SHOOTERGAME_API FDropedCaseModel : public FPlayerCaseModel
{
	GENERATED_USTRUCT_BODY()


	UPROPERTY()		TArray<FDropedCaseItemModel> Items;
};