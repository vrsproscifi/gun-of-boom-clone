#pragma once
//==========================================
#include "MarketDeliveryRequest.h"

//==========================================
#include "Public/Interfaces/OnlineStoreInterface.h"

//==========================================
#include "InAppPurchaseWrapper.generated.h"

//==========================================
DECLARE_DYNAMIC_DELEGATE_OneParam(FInAppPurchaseSuccessful, const FMarketDeliveryRequest&, InMarketDeliveryRequest);
DECLARE_DYNAMIC_DELEGATE_TwoParams(FOnInGamePurchaseComplete, const EInAppPurchaseState::Type&, InCompletionStatus, const FInAppPurchaseProductInfo&, InAppPurchaseInformation);


//==========================================
/*UENUM()
enum class EInAppPurchaseStatus : uint8
{
	None,
	SubsytemNotAvalible,
	SubsytemStoreNotAvalible,
	MakePurchasesNotAllowed,
	BeginPurchaseSuccessfuly,
	BeginPurchaseFailed,
	JavaEnvWasNullptr,
};*/

UENUM()
enum class EInAppPurchaseMethodHandler : uint8
{
	None,
	Arsernal,
	Progress,
};