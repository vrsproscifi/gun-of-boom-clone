#pragma once
//==========================================
#include "ArsenalComponent/PlayerProfileItemModel.h"

//==========================================
#include "PlayerProfileModel.generated.h"

USTRUCT(BlueprintType)
struct FPlayerProfileModel
{
	GENERATED_USTRUCT_BODY()

	UPROPERTY()	FLokaGuid ProfileId;
	UPROPERTY()	FLokaGuid DefaultItemId;
	UPROPERTY()	TArray<FPlayerProfileItemModel> Items;

	FPlayerProfileModel() 
		: ProfileId()
		, Items()
	{
	
	}

	const FPlayerProfileItemModel* GetDefaultItem() const
	{
		const auto SafeSlots = {1, 2, 3, 4};
		auto r = Items.FindByPredicate([&](const FPlayerProfileItemModel& item)
		{
			return item.EntityId == DefaultItemId;
		});

		if (r == nullptr)
		{
			for (const auto Slot : SafeSlots)
			{
				r = GetItemBySlot(Slot);
				if(r != nullptr)
				{
					break;
				}
			}
		}
		return r;
	}

	const FPlayerProfileItemModel* GetItemBySlot(const int32& slot) const
	{
		auto r = Items.FindByPredicate([s = slot](const FPlayerProfileItemModel& item)
		{
			return item.SetSlotId == s;
		});

		return r;
	}

	friend bool operator==(const FPlayerProfileModel& Lhs, const FPlayerProfileModel& Rhs)
	{
		return	Lhs.ProfileId == Rhs.ProfileId && Lhs.DefaultItemId == Rhs.DefaultItemId &&
			Lhs.Items == Rhs.Items;
	}
};
