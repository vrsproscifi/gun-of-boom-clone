#pragma once

//==========================================
#include "UsePlayerItemRequest.generated.h"

USTRUCT(BlueprintType)
struct FUsePlayerItemRequest
{
	GENERATED_USTRUCT_BODY()

	UPROPERTY()	FLokaGuid EntityId;
	UPROPERTY()	uint32 Amount;
};