#pragma once

//==========================================
#include "MarketDeliveryRequest.generated.h"

USTRUCT(BlueprintType)
struct FMarketDeliveryRequest
{
	GENERATED_USTRUCT_BODY()

	UPROPERTY(BlueprintReadWrite)	FString ProductId;
	UPROPERTY(BlueprintReadWrite)	FString PurchaseToken;
	UPROPERTY(BlueprintReadWrite)	FString DataSignature;
	UPROPERTY(BlueprintReadWrite)	FString Reciept;

	FString ToString() const
	{
		return FString::Printf(TEXT("[ProductId: %s] [PurchaseToken: %s] [DataSignature: %s][Reciept: %s]"), *ProductId, *PurchaseToken, *DataSignature, *Reciept);
	}
};

USTRUCT()
struct FMarketDeliveryResponse
{
	GENERATED_USTRUCT_BODY()

	UPROPERTY()	FString ProductId;
	UPROPERTY()	FString OrderId;
	UPROPERTY()	uint64 Amount;

	FString ToString() const
	{
		return FString::Printf(TEXT("[ProductId: %s] [OrderId: %s] [Amount: %s]"), *ProductId, *OrderId, Amount);
	}
};
