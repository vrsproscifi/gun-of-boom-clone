#pragma once

#include "Item/Product/ProductDiscountCoupone.h"
#include "PlayerDiscountModel.generated.h"

USTRUCT()
struct FPlayerDiscountModel
{
	GENERATED_USTRUCT_BODY()

		FPlayerDiscountModel()
		: ModelId(0)
		, Coupone(EProductDiscountCoupone::None)
		, EndDiscountDate(0)
	{

	}

	UPROPERTY()
		FLokaGuid EntityId;

	UPROPERTY()
		int32 ModelId;

	UPROPERTY()
		EProductDiscountCoupone Coupone;

	UPROPERTY()
		int64 EndDiscountDate;
};