#pragma once

//==========================================
#include "UpgradeItemResponse.generated.h"

USTRUCT(BlueprintType)
struct FUpgradeItemResponse
{
	GENERATED_USTRUCT_BODY()

	UPROPERTY()	FLokaGuid EntityId;
	UPROPERTY()	int32 Level;
};