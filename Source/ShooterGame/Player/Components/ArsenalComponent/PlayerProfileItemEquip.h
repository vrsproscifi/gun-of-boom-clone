#pragma once

//=========================================
#include "GameItemType.h"

//==========================================
#include "PlayerProfileItemEquip.generated.h"

USTRUCT()
struct FPlayerProfileItemEquip
{
	GENERATED_USTRUCT_BODY()

	FPlayerProfileItemEquip()
		: SetSlotId(EGameItemType::None)
	{

	}

	FPlayerProfileItemEquip(
		const FLokaGuid& itemId,
		const EGameItemType& setSlotId
		)
		: ItemId(itemId)
		, SetSlotId(setSlotId)
	{

	}

	UPROPERTY()		FLokaGuid ItemId;

	UPROPERTY()		EGameItemType SetSlotId;
};