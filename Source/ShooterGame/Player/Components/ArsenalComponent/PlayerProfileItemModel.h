#pragma once
//==========================================
#include "PlayerProfileItemModel.generated.h"

USTRUCT(BlueprintType)
struct FPlayerProfileItemModel
{
	GENERATED_USTRUCT_BODY()

	// Ид предмета в профиле
	UPROPERTY()	FLokaGuid EntityId;

	// Ид предмета в инвентаре / арсенале
	UPROPERTY()	FLokaGuid ItemId;

	//	Ид слота в который он был вставлен в профиле
	UPROPERTY()	int32 SetSlotId;


	FPlayerProfileItemModel(const FLokaGuid& InItemId, const int32& InSetSlotId)
		: EntityId(FGuid::NewGuid())
		, ItemId(InItemId)
		, SetSlotId(InSetSlotId)
	{

	}


	FPlayerProfileItemModel()
		: SetSlotId(0)
	{
		
	}


	friend bool operator==(const FPlayerProfileItemModel& Lhs, const FPlayerProfileItemModel& Rhs)
	{
		return Lhs.EntityId == Rhs.EntityId && Lhs.ItemId == Rhs.ItemId && Lhs.SetSlotId == Rhs.SetSlotId;
	}
};
