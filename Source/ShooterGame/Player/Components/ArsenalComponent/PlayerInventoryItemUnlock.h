#pragma once

//========================================
#include "GameItemType.h"

//========================================
#include "ArsenalComponent/PlayerInventoryItemModel.h"

//========================================
#include "PlayerInventoryItemUnlock.generated.h"


//========================================
USTRUCT()
struct FPlayerInventoryItemUnlock
{
	GENERATED_USTRUCT_BODY()

		FPlayerInventoryItemUnlock()
		: ModelId(0)
		, Amount(0)
		, SetSlotId(EGameItemType::None)
	{

	}

	FPlayerInventoryItemUnlock(const int32& itemId, const int32& amount = 1)
		: ModelId(itemId)
		, Amount(amount)
	{

	}

	//	Модель премета
	UPROPERTY()		int32 ModelId;

	//	Количество
	UPROPERTY()		int32 Amount;

	UPROPERTY()		EGameItemType SetSlotId;
};


//========================================
USTRUCT()
struct FPlayerInventoryItemUnlocked : public FPlayerInventoryItemModel
{
	GENERATED_USTRUCT_BODY()

	FPlayerInventoryItemUnlocked()
		: AmountRequest(0)
		, AmountBought(0)
	{

	}

	UPROPERTY()	FLokaGuid ItemId;

	//	Количество которе игрок хотел купить
	UPROPERTY()	int32 AmountRequest;

	//	Количество на которое хватило денег
	UPROPERTY()	int32 AmountBought;
};