#pragma once
#include "PlayerInventoryItemModel.generated.h"

//	FInvertoryItemWrapper

USTRUCT()
struct FPlayerInventoryItemModel
{
	GENERATED_USTRUCT_BODY()

	FPlayerInventoryItemModel(const int32& InModelId)
		: EntityId(FGuid::NewGuid())
		, ModelId(InModelId)
		, Level(0)
		, Amount(0)
	{
		
	}

	FPlayerInventoryItemModel()
		: ModelId(0)
		, Level(0)
		, Amount(0)
	{

	}

	UPROPERTY()		FLokaGuid EntityId;

	UPROPERTY()		int32 ModelId;

	UPROPERTY()		int32 Level;

	UPROPERTY()		int32 Amount;
};