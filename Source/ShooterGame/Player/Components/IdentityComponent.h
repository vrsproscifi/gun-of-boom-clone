﻿#pragma once
#include "Engine/EngineTypes.h"
#include "BasicPlayerComponent.h"
#include "Shared/AttachAccountService.h"
#include "IdentityComponent/PlayerEntityInfo.h"
#include "IdentityComponent/Authorization.h"
#include "IdentityComponent/GameVersionModel.h"
#include "IdentityComponent/PlayerStatisticModel.h"
#include "IdentityComponent.generated.h"


class UPlayerFractionEntity;
class UOperationRequest;

struct FOnlineError;
struct FPlayerStatisticModel;

#define STEAM_SUPPORT 0

DECLARE_DYNAMIC_MULTICAST_DELEGATE_OneParam(FOnPlayerEntryRewardSuccessfully, const int32&, InRewardDay);
DECLARE_DYNAMIC_MULTICAST_DELEGATE_OneParam(FOnRequestClientDataSuccessfully, const FPlayerEntityInfo&, PlayerInfo);
DECLARE_DYNAMIC_MULTICAST_DELEGATE_OneParam(FOnAuthorization, const FGuid&, AuthorizationResponse);
DECLARE_DYNAMIC_MULTICAST_DELEGATE(FOnPlayerLevelUp);

UCLASS(Transient)
class UIdentityComponent : public UBasicPlayerComponent
{
	GENERATED_BODY()

	UPROPERTY() bool IsAuthorized = false;

	//========================================
	UPROPERTY() int32 ToggleAuthFormAttempCount;
	UPROPERTY(Transient) FTimerHandle ToggleAuthFormHandle;
	
	UFUNCTION() void OnPrepareSendRequestAuthorization();
private:
	TSharedPtr<class SAuthForm>			Slate_AuthForm;

	UFUNCTION()
	bool InitFaceBookSystem() const;

public:


	UIdentityComponent();

	void OnAuthorizeAsSimulation();
	virtual void BeginPlay() override;
	virtual void EndPlay(const EEndPlayReason::Type EndPlayReason) override;


	bool IsMultiPlayer() const;

	// Send requests/Отправка запросов

	UFUNCTION(Reliable, Server, WithValidation) void SendRequestAuthorization(const FPlayerAuthorizationRequest& InRequest);
	UFUNCTION(Reliable, Server, WithValidation) void SendRequestChangeRegion();
	UFUNCTION(Reliable, Server, WithValidation) void SendRequestClientData();
	UFUNCTION(Reliable, Server, WithValidation) void SendRequestClientDataUpdate();
	UFUNCTION(Reliable, Server, WithValidation) void SendRequestStatus();
	UFUNCTION(Reliable, Server, WithValidation) void SendRequestExist();

	void SendRequestPlayerStatistic(const FGuid& InPlayerId = FGuid()) const;


	UFUNCTION()
	void OnBadNetworkConnection(const FRequestExecuteError& InErrorData);

	UFUNCTION() 
		void SendRequestAndValidateVersion(const FGameVersionModel& version);

	UFUNCTION()
		void ValidateGameSingleton();

	UPROPERTY(Transient)							FTimerHandle ValidateGSTimerHandle;
	UPROPERTY(Transient)							FTimerHandle ValidateAuthTimerHandle;

	UPROPERTY(Transient, VisibleInstanceOnly)		bool bPlayerHasLogIn;

	UFUNCTION()
		void OnValidateTimeAuth();


	UFUNCTION()
		void OnValidateGameSingletonSuccessfully();

	UFUNCTION()
		void OnValidateGameSingletonFailed();


	UFUNCTION() 
		void SendRequestChangeName(const FText& InName) const;

	UFUNCTION()
		void StartRequestIsoCountry();

	UFUNCTION()
		void SendRequestIsoCountry() const;

	UFUNCTION()
		void OnRequestIsoCountry(const EIsoCountry& InIsoCountry);

	UFUNCTION() 
		void OnRequestIsoCountryFailed(const FRequestExecuteError& InErrorData) const;

protected:
	FGameVersionModel CurrentGameVersion;

	// Requests/Запросы
	UPROPERTY(Transient, VisibleInstanceOnly) UOperationRequest*	RequestDataDump;
	UPROPERTY(Transient, VisibleInstanceOnly) UOperationRequest*	RequestIsoCountry;

	UPROPERTY(Transient, VisibleInstanceOnly) UOperationRequest*	RequestAttachAccountService;
	UPROPERTY(Transient, VisibleInstanceOnly) UOperationRequest*	RequestReAttachAccountService;


	UPROPERTY(Transient, VisibleInstanceOnly) UOperationRequest*	RequestLatestVersion;

	UPROPERTY(Transient, VisibleInstanceOnly) UOperationRequest*	RequestPlayerStatistic;
	UPROPERTY(Transient, VisibleInstanceOnly) UOperationRequest*	RequestChangeName;


	UPROPERTY(Transient, VisibleInstanceOnly) UOperationRequest*	RequestAuthorization;
	UPROPERTY(Transient, VisibleInstanceOnly) UOperationRequest*	RequestExist;
	UPROPERTY(Transient, VisibleInstanceOnly) UOperationRequest*	RequestClientData;
	UPROPERTY(Transient, VisibleInstanceOnly) UOperationRequest*	RequestClientDataUpdate;
	UPROPERTY(Transient, VisibleInstanceOnly) UOperationRequest*	RequestStatus;	
	UPROPERTY(Transient, VisibleInstanceOnly) int32					RequestLatestVersionAttemp;
	UPROPERTY(Transient, VisibleInstanceOnly) int32					RequestLatestVersionLimit;

	// Receive answers/Получение ответов
	UFUNCTION() void OnAuthorization(const FPlayerAuthorizationResponse& InData);
	UFUNCTION() void OnAuthorizationFailed(const FRequestExecuteError& InErrorData);

	UFUNCTION() void OnAttachAccountService(const EAccountService::Type& InAttachedToService);
	UFUNCTION() void OnAttachAccountServiceExist(const FAttachAccountServiceExist& InExistAccount);
	UFUNCTION() void OnAttachAccountServiceFailed(const FRequestExecuteError& InErrorData);

	UFUNCTION() void OnReAttachAccountServiceUseLast(const FAttachAccountServiceExistSuccessfully& InLastPlayerToken);
	UFUNCTION() void OnReAttachAccountServiceFailed(const FRequestExecuteError& InErrorData);
	UFUNCTION() void OnReAttachAccountServiceAuthorized();


	UFUNCTION() void OnRequestLatestVersion(const FGameVersionModel& version);
	UFUNCTION() void OnRequestLatestVersion_ServerError();
	UFUNCTION() void OnRequestLatestVersion_Failed(const FRequestExecuteError& error);
	UFUNCTION() void ShowRequestUpdateNotify(const int32& code);

	UFUNCTION() void OnRequestChangeName(const FString& InInputPlayerName);
	UFUNCTION() void OnRequestChangeName_EmptyString();
	UFUNCTION() void OnRequestChangeName_NotEnouthMoney(const FString& InInputPlayerName);
	UFUNCTION() void OnRequestChangeName_NameExist(const FString& InInputPlayerName);
	UFUNCTION() void OnRequestChangeNameFailed(const FRequestExecuteError& InErrorData);



	void OnRequestPlayerStatistic(const FPlayerStatisticModel& InPlayerStatistic);
	void OnRequestPlayerStatistic_Failed(const FRequestExecuteError& InError);


	UFUNCTION() void OnExist();
	UFUNCTION() void OnExistNotFound();
	UFUNCTION() void OnExistNotAuthorized();
	
	public:
	void OnAttachGoogleSeviceStart();

	private:
	void OnAttachGoogleSeviceComplete(int32 LocalUserNum, bool bWasSuccessful, const FUniqueNetId& UserId, const FString& Error);

	UFUNCTION() void OnCreate(const bool& InResponse);
	UFUNCTION() void OnCreateIncorrect();
	
	UFUNCTION() void OnPlayerData(const FPlayerEntityInfo& InData);
	UFUNCTION() void OnPlayerDataUpdate(const FPlayerEntityInfo& InData);

	UFUNCTION() void OnRep_PlayerInfo();
	UPROPERTY(ReplicatedUsing = OnRep_PlayerInfo) FPlayerEntityInfo PlayerInfo;

	FORCEINLINE_DEBUGGABLE void SetPlayerInfo(const FPlayerEntityInfo& info);
public:

	UPROPERTY() FPlayerStatisticModel Statistic;

	UFUNCTION(BlueprintCallable, BlueprintPure)
	bool HasPremiumAccount() const
	{
		return GetPlayerInfo().HasPremiumAccount();
	}

	FORCEINLINE_DEBUGGABLE const FPlayerEntityInfo& GetPlayerInfo()
	{
		return PlayerInfo;
	}

	UFUNCTION(BlueprintCallable, BlueprintPure)
	const FPlayerEntityInfo& GetPlayerInfo() const
	{
		return PlayerInfo;
	}

protected:
	void CreateGameWidgets() override;

public:

	UPROPERTY(BlueprintAssignable, Category = Identity)
	FOnPlayerEntryRewardSuccessfully OnPlayerEntryReward;

	UPROPERTY(BlueprintAssignable, Category = Identity)
	FOnPlayerLevelUp OnSchedulePlayerLevelUp;

	UPROPERTY(BlueprintAssignable, Category = Identity)
	FOnAuthorization OnAuthorizationEvent;

	UPROPERTY(BlueprintAssignable, Category = Identity)
	FOnPlayerLevelUp OnStatisticUpdate;

	// TODO: Неизвестные события, пояснить или удалить.

	FOnRequestClientDataSuccessfully	OnRequestClientDataDelegate;
	FOnFailedOperationResponse			OnRequestLootDataFailed;

	void OnToggleAuthFormAttemp(const bool InToggle);
	UFUNCTION(Reliable, Client) void ToggleAuthForm(const bool InToggle);
	UFUNCTION(Reliable, Client) void ShowErrorOnAuthForm(const ELoginFromRemoteStatus InCode, const FString& InAdditionalMessage = FString());
protected:

	UFUNCTION()
	void OnRep_Simulation();

	UPROPERTY(ReplicatedUsing = OnRep_Simulation)
	bool bIsSimulation;

public:

	void ChangeNameDialog(const FString& InTag, const FString& InInitialName, const FText& InErrorText = FText::GetEmpty());
	void ShowChangeNameDialog();
};