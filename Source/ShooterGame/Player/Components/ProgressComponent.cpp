//======================================
#include "ShooterGame.h"
#include "ProgressComponent.h"
//======================================
#include "Engine/Http/RequestManager.h"

//======================================
#include "ArsenalComponent/InAppPurchaseWrapper.h"

//======================================
#include "Notify/SMessageBox.h"
#include "Localization/TableBaseStrings.h"
#include "GameSingletonExtensions.h"
#include "ProductItemEntity.h"
#include "ShooterGameAchievements.h"
#include "ProgressComponent/RatingTopContainer.h"
#include "ShooterGameAnalytics.h"
//#include "CrashlyticsBlueprintLibrary.h"
#include "Notify/SNotifyContainer.h"
#include "GooglePlayHelper/AndroidNotify.h"
//#include "CrashlyticsBlueprintLibrary.h"
#include "ArsenalComponent/InAppPurchaseWrapper.h"
#include "GameMode/LobbyGameMode.h"

DEFINE_LOG_CATEGORY(LogProgressComponent);

#define LOCTEXT_NAMESPACE "Progress"

UProgressComponent::UProgressComponent()
	: OnRequestMatchResultCalled(0)
{	
	//==========================================
	//InAppPurchase = CreateDefaultSubobject<UInAppPurchaseWrapper>(TEXT("InAppPurchaseWrapper"));
	//InAppPurchase->OnRequestPurchaseValidate.BindUObject(this, &UProgressComponent::SendRequestOrderValidation);

	//==========================================
	MarketDelivery = CreateRequest(FOperationRequestServerHost::MasterClient, "Arsenal/OnMarketDelivery");
	MarketDelivery->BindObject<FMarketDeliveryResponse>(200).AddUObject(this, &UProgressComponent::OnRequestOrderValidation);
	MarketDelivery->OnFailedResponse.BindUObject(this, &UProgressComponent::OnRequestOrderValidation_Failed);

	//==========================================
	RequestApplyMatchPremiumAccountBonus = CreateRequest(FOperationRequestServerHost::MasterClient, "Session/ApplyMatchPremiumAccountBonus");
	RequestApplyMatchPremiumAccountBonus->BindObject<FMatchUpgradeResponse>(200).AddUObject(this, &UProgressComponent::OnRequestMatchUpgrade);
	RequestApplyMatchPremiumAccountBonus->Bind(404).AddUObject(this, &UProgressComponent::OnRequestMatchUpgrade_MatchNotFound);
	RequestApplyMatchPremiumAccountBonus->Bind(409).AddUObject(this, &UProgressComponent::OnRequestMatchUpgrade_PremiumNotFound);
	RequestApplyMatchPremiumAccountBonus->Bind(400).AddUObject(this, &UProgressComponent::OnRequestMatchUpgrade_PremiumAlwaysUsed);
	RequestApplyMatchPremiumAccountBonus->OnFailedResponse.BindUObject(this, &UProgressComponent::OnRequestMatchUpgrade_Failed);

	//==========================================
	RequestApplyMatchAdsBonus = CreateRequest(FOperationRequestServerHost::MasterClient, "Session/ApplyMatchAdsBonus");
	RequestApplyMatchAdsBonus->BindObject<FMatchUpgradeResponse>(200).AddUObject(this, &UProgressComponent::OnApplyMatchAdsBonusSuccessfully);
	RequestApplyMatchAdsBonus->Bind(404).AddUObject(this, &UProgressComponent::OnRequestMatchUpgrade_MatchNotFound);
	RequestApplyMatchAdsBonus->Bind(409).AddUObject(this, &UProgressComponent::OnRequestMatchUpgrade_PremiumNotFound);
	RequestApplyMatchAdsBonus->Bind(400).AddUObject(this, &UProgressComponent::OnRequestMatchUpgrade_PremiumAlwaysUsed);
	RequestApplyMatchAdsBonus->OnFailedResponse.BindUObject(this, &UProgressComponent::OnRequestMatchUpgrade_Failed);


	//==========================================
	RequestMatchResults = CreateRequest(FOperationRequestServerHost::MasterClient, "Session/GetMatchResults", 10.0f, EOperationRequestTimerMethod::CallViaInterval, FTimerDelegate::CreateUObject(this, &UProgressComponent::SendRequestMatchResults));
	RequestMatchResults->BindArray<TArray<FGameMatchInformation>>(200).AddUObject(this, &UProgressComponent::OnRequestMatchResults);
	RequestMatchResults->OnFailedResponse.BindUObject(this, &UProgressComponent::OnRequestMatchResultsFailed);

	//==========================================
	RequestTopPlayers = CreateRequest(FOperationRequestServerHost::MasterClient, "Session/GetPlayerRatingTop", 0.500f, EOperationRequestTimerMethod::CallViaIntervalOrResponse, FTimerDelegate::CreateUObject(this, &UProgressComponent::SendRequestTopPlayers), 5, true);
	RequestTopPlayers->BindObject<FRatingTopContainer>(200).AddUObject(this, &UProgressComponent::OnRequestTopPlayers);
	RequestTopPlayers->OnFailedResponse.BindUObject(this, &UProgressComponent::OnRequestTopPlayers_Failed);

}


void UProgressComponent::StartRequestTopPlayers() const
{
	StartTimer(RequestTopPlayers);
}

void UProgressComponent::OnPlayRewardVideoSuccess(const FPlayRewardedVideoResponse& InResponse)
{
	if (InResponse.Category.Equals("ApplyMatchAdsBonus", ESearchCase::IgnoreCase))
	{
		FGuid EntityId;
		if (FGuid::Parse(InResponse.Entity, EntityId))
		{
			RequestApplyMatchAdsBonus->SendRequestObject(EntityId);
		}
	}
}

void UProgressComponent::SendRequestTopPlayers() const
{
	//UCrashlyticsBlueprintLibrary::WriteLog("UProgressComponent:SendRequestTopPlayers");
	RequestTopPlayers->GetRequest();
}

void UProgressComponent::OnRequestTopPlayers(const FRatingTopContainer& InRatingContainer)
{
	StopTimer(RequestTopPlayers);
	OnTopPlayers.Broadcast(InRatingContainer);
}

void UProgressComponent::OnRequestTopPlayers_Failed(const FRequestExecuteError& InErrorData)
{
	//===============================================
	FShooterGameAnalytics::RecordDesignEvent("UI:Lobby:TopPlayers:Fail:Code" + InErrorData.GetStatusCode());
	FShooterGameAnalytics::RecordErrorEvent(EErrorSeverity::warning, "UI:Lobby:TopPlayers:Fail:Code" + InErrorData.GetStatusCode());

	//===============================================
	QueueBegin(SNotifyContainer, "Notify_OnRequestTopPlayers_Failed")
		SNotifyContainer::Get()->SetContent(FText::Format(NSLOCTEXT("Notify", "Notify.OnRequestTopPlayers_Failed", "Failed load player ratings, error code: {0}"), FText::AsNumber(InErrorData.Status, &FNumberFormattingOptions::DefaultNoGrouping())));
		SNotifyContainer::Get()->ToggleWidget(true);
	QueueEnd
}

bool UProgressComponent::IsAllowServerQueries() const
{
	return FPlatformProperties::IsServerOnly() == false;
}

void UProgressComponent::OnAuthorizationComplete()
{
	StartTimer(RequestMatchResults);
}

void UProgressComponent::SendRequestOrderValidation(const FMarketDeliveryRequest& InOrderRequest)
{
	//UCrashlyticsBlueprintLibrary::WriteLog("UProgressComponent:SendRequestOrderValidation");

	MarketDelivery->SendRequestObject(InOrderRequest);
}

void UProgressComponent::OnRequestOrderValidation(const FMarketDeliveryResponse& InOrderResponse)
{
	if (InOrderResponse.OrderId.IsEmpty())
	{
		SNotifyContainer::ShowNotify("OnRequestOrderValidation", NSLOCTEXT("Notify", "Notify.MakeInAppPurchase.OrderValidation.OrderId", "Failed validate payment order, OrderId was empty"));
	}
	else if (InOrderResponse.ProductId.IsEmpty())
	{
		SNotifyContainer::ShowNotify("OnRequestOrderValidation", NSLOCTEXT("Notify", "Notify.MakeInAppPurchase.OrderValidation.ProductId", "Failed validate payment order, ProductId was empty"));
	}
	else if (InOrderResponse.ProductId.Contains("pr", ESearchCase::IgnoreCase) == false)
	{
		SNotifyContainer::ShowNotify("OnRequestOrderValidation", FText::Format(NSLOCTEXT("Notify", "Notify.MakeInAppPurchase.OrderValidation.ProductIdInvalid", "Failed validate payment order, ProductId[{0}] was invalid"), FText::FromString(InOrderResponse.ProductId)));
	}
	else
	{
		SendRequestAdditionalBonus(TargetMatchMemberId, true, -1);
	}
}

void UProgressComponent::OnRequestOrderValidation_Failed(const FRequestExecuteError& InError)
{	
	//============================================
	FShooterGameAnalytics::RecordDesignEvent("Match:MatchUpgrade:OrderValidation:Fail:Code" + InError.GetStatusCode());
	FShooterGameAnalytics::RecordErrorEvent(EErrorSeverity::error, "Match:MatchUpgrade:OrderValidation:Fail:Code" + InError.GetStatusCode());

	//===============================================
	SNotifyContainer::ShowNotify("Notify_OnRequestOrderValidation_Failed", FText::Format(NSLOCTEXT("Notify", "Notify.OnRequestOrderValidation_Failed", "Failed validate payment order: {0}"), FText::AsNumber(InError.Status, &FNumberFormattingOptions::DefaultNoGrouping())));
}


void UProgressComponent::OnApplyMatchAdsBonusSuccessfully(const FMatchUpgradeResponse& InResponse)
{
	//============================================
	FShooterGameAnalytics::RecordDesignEvent("Match:OnApplyMatchAdsBonus:Successfuly");	
	
	//UCrashlyticsBlueprintLibrary::LogFbSpentCredits("MatchAdBonus", "", TEXT("Money"), InResponse.Money);
	//UCrashlyticsBlueprintLibrary::LogFbSpentCredits("MatchAdBonus", "", TEXT("Experience"), InResponse.Experience);
	
	//============================================
	OnApplyMatchAdsBonus.ExecuteIfBound();

	//===============================================
	SNotifyContainer::ShowNotify("OnApplyMatchAdsBonus", NSLOCTEXT("Notify", "Notify.OnRequestMatchUpgrade", "You successfuly got extra money and experience for the current battle."));
}

void UProgressComponent::OnRequestMatchUpgrade(const FMatchUpgradeResponse& InResponse)
{
	//============================================
	FShooterGameAnalytics::RecordDesignEvent("Match:MatchUpgrade:Successfuly");

	//============================================
	OnApplyMatchPremiumAccountBonus.ExecuteIfBound();

	//===============================================
	SNotifyContainer::ShowNotify("OnRequestMatchUpgrade", NSLOCTEXT("Notify", "Notify.OnRequestMatchUpgrade", "You successfuly got extra money and experience for the current battle."));
}

void UProgressComponent::OnRequestMatchUpgrade_PremiumAlwaysUsed()
{
	FShooterGameAnalytics::RecordErrorEvent(EErrorSeverity::warning, "UProgressComponent::OnRequestMatchUpgrade_PremiumAlwaysUsed");


	//===============================================
	QueueBegin(SNotifyContainer, "Notify_OnRequestMatchUpgrade_PremiumAlwaysUsed")
		SNotifyContainer::Get()->SetContent(NSLOCTEXT("Notify", "Notify.OnRequestMatchUpgrade", "You successfuly got extra money and experience for the current battle."));
		SNotifyContainer::Get()->ToggleWidget(true);
	QueueEnd

}

void UProgressComponent::OnRequestMatchUpgrade_PremiumNotFound()
{
	FShooterGameAnalytics::RecordErrorEvent(EErrorSeverity::warning, "UProgressComponent::OnRequestMatchUpgrade_PremiumNotFound");

	//===============================================
	QueueBegin(SNotifyContainer, "Notify_OnRequestMatchUpgrade_PremiumNotFound")
		SNotifyContainer::Get()->SetContent(NSLOCTEXT("Notify", "Notify.OnRequestMatchUpgrade_PremiumNotFound", "Could not give a bonus for the match. Error #1"));
		SNotifyContainer::Get()->ToggleWidget(true);
	QueueEnd

}

void UProgressComponent::OnRequestMatchUpgrade_MatchNotFound()
{
	FShooterGameAnalytics::RecordDesignEvent("Match:MatchUpgrade:Failed:MatchNotFound");

	FShooterGameAnalytics::RecordErrorEvent(EErrorSeverity::warning, "UProgressComponent::OnRequestMatchUpgrade_MatchNotFound");

	//===============================================
	QueueBegin(SNotifyContainer, "Notify_OnRequestMatchUpgrade_MatchNotFound")
		SNotifyContainer::Get()->SetContent(NSLOCTEXT("Notify", "Notify.OnRequestMatchUpgrade_MatchNotFound", "Could not give a bonus for the match. Error #2"));
		SNotifyContainer::Get()->ToggleWidget(true);
	QueueEnd
}

void UProgressComponent::OnRequestMatchUpgrade_Failed(const FRequestExecuteError& InError)
{
	FShooterGameAnalytics::RecordDesignEvent("Match:MatchUpgrade:Fail:Code" + InError.GetStatusCode());

	FShooterGameAnalytics::RecordErrorEvent(EErrorSeverity::warning, "UProgressComponent::OnRequestMatchUpgrade_Failed");

	//===============================================
	QueueBegin(SNotifyContainer, "OnRequestMatchUpgrade_Failed")
		SNotifyContainer::Get()->SetContent(FText::Format(NSLOCTEXT("Notify", "Notify.OnRequestMatchUpgrade_Failed", "Could not give a bonus for the match. Error: {0}"), FText::AsNumber(InError.Status, &FNumberFormattingOptions::DefaultNoGrouping())));
		SNotifyContainer::Get()->ToggleWidget(true);
	QueueEnd
}

void UProgressComponent::SendRequestAdditionalBonus(const FGuid& InMatchMemberId, const bool& InHasPremiumAccount, const int32& InTargetPremiumAccount)
{
	//UCrashlyticsBlueprintLibrary::WriteLog("UProgressComponent:SendRequestAdditionalBonus");

	if (InHasPremiumAccount)
	{
		RequestApplyMatchPremiumAccountBonus->SendRequestObject(InMatchMemberId);
	}
	else
	{
		TargetMatchMemberId = InMatchMemberId;

		//============================================
		auto PremiumAccountEntity = FGameSingletonExtensions::FindItemById(InTargetPremiumAccount);

		//============================================
		if (IsValidObject(PremiumAccountEntity))
		{
			const auto ProductId = PremiumAccountEntity->GetModelName();

			//UCrashlyticsBlueprintLibrary::SetField("MakeInAppPurchase.ProductId", ProductId);
			//UCrashlyticsBlueprintLibrary::SetField("MakeInAppPurchase.Date", FDateTime::Now());
			//UCrashlyticsBlueprintLibrary::SetField("MakeInAppPurchase.Status", "SendRequest");
			//UCrashlyticsBlueprintLibrary::WriteLog("UArsenalComponent:OnMakeInAppPurchase");

#if UE_BUILD_DEVELOPMENT
			GEngine->AddOnScreenDebugMessage(-1, 5.f, FColor::Red, FString::Printf(TEXT("[OnMakeInAppPurchase][%s]"), *ProductId));
#endif

			//=======================================================================
			const auto gm = GetGameMode<ALobbyGameMode>();

			//=======================================================================
			if (IsValidObject(gm))
			{
				gm->OnProgressPurchaseValidate.BindDynamic(this, &UProgressComponent::SendRequestOrderValidation);
				gm->OnInAppPurchase(ProductId, EInAppPurchaseMethodHandler::Progress);
			}
			else
			{
				SNotifyContainer::ShowNotify("Notify_MakeInAppPurchase_ServiceNotAvailable", NSLOCTEXT("Notify", "Notify.MakeInAppPurchase.ServiceNotAvailable", "Payment service not available"));
				FShooterGameAnalytics::RecordErrorEvent(EErrorSeverity::error, "OnMakeInAppPurchase::Purchase::ServiceNotAvailable");
			}
		}
		else
		{
			SNotifyContainer::ShowNotify("Notify_MakeInAppPurchase_ServiceNotAvailable", NSLOCTEXT("Notify", "Notify.MakeInAppPurchase.ServiceNotAvailable", "Payment service not available"));
			FShooterGameAnalytics::RecordErrorEvent(EErrorSeverity::error, "OnMakeInAppPurchase::Purchase::ServiceNotAvailable");
		}
	}
}



void UProgressComponent::OnRequestMatchResults(const TArray<FGameMatchInformation>& results)
{
	OnRequestMatchResultCalled++;

	if (!!results.Num())
	{
		//UCrashlyticsBlueprintLibrary::WriteLog("UProgressComponent:OnRequestMatchResults");
	}

	OnMatchResultsUpdate.Broadcast(results);


	if (OnRequestMatchResultCalled >= 5)
	{
		StopTimer(RequestMatchResults);
	}
	//else
	//{
	//	TArray<FGameMatchInformation> q;
	//	q.Add(FGameMatchInformation());
	//
	//	OnMatchResultsUpdate.Broadcast(q);
	//}

	const auto PlayerId = GetPlayerId();
	UE_LOG(LogProgressComponent, Log, TEXT("OnRequestMatchResults >> Total Results: %d | PlayerId: %s"), results.Num(), *PlayerId.ToString());

	for(const auto& result : results)
	{
		UE_LOG(LogProgressComponent, Log, TEXT("r\n%s"), *result.ToString());
	}
}

void UProgressComponent::OnRequestMatchResultsFailed(const FRequestExecuteError& InErrorData)
{
	FShooterGameAnalytics::RecordDesignEvent("UI:Lobby:MatchResults:Fail:Code" + InErrorData.GetStatusCode());

	FShooterGameAnalytics::RecordErrorEvent(EErrorSeverity::warning, "UProgressComponent::OnRequestMatchResultsFailed:" + InErrorData.GetStatusCode());
	UE_LOG(LogProgressComponent, Log, TEXT("OnRequestMatchResultsFailed >> Error: %s"), *InErrorData.ToString());
}

void UProgressComponent::SendRequestMatchResults()
{
	if (IsAllowServerQueries())
	{
		RequestMatchResults->GetRequest();
	}
}


#undef LOCTEXT_NAMESPACE