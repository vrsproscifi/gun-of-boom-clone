﻿#pragma once
#include "SearchSessionStatus.generated.h"

UENUM()
enum class ESearchSessionStatus : uint8
{
	None,

	//	Идет поиск матча
	MatchSearch,

	//	Матч найдет, вход в игру
	FoundJoin,

	//	Матч найден, продолжение матча
	FoundContinue,

	//	Матч найден, подготовка ко входу
	FoundPrepare,

	//  Матч найден, но произошла ошибка
	FoundError,

	//	Матч найден, но матч завершен
	Finished,

	//  Недостаточно денег для боя
	NotEnouthMoney,

	//===================================
	//	Только на клиенте

	//	Состояние кнопки, когда игрок начал поиск
	SearchStarting,

	//	Состояние кнопки, когда игрок отменил поиск
	SearchStopping,

	//  Состояние кнопки, когда ожидаются игроки
	SearchPrepare,

	End
};
