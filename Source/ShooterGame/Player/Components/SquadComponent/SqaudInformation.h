#pragma once


#include "SearchSessionStatus.h"

#include "SqaudMemberInformation.h"
#include "Shared/SessionMatchOptions.h"
#include "SqaudInformation.generated.h"


USTRUCT(BlueprintType)
struct FSqaudInformation
{
	GENERATED_USTRUCT_BODY()

	UPROPERTY(BlueprintReadOnly)		FLokaGuid							ServerId;
	UPROPERTY(BlueprintReadOnly)		FLokaGuid							LeaderId;
	UPROPERTY(BlueprintReadOnly)		FString								Host;
	UPROPERTY(BlueprintReadOnly)		FString								Token;
	UPROPERTY(BlueprintReadOnly)		FString								Message;
	UPROPERTY(BlueprintReadOnly)		bool								IsDefence;
	UPROPERTY(BlueprintReadOnly)		bool								IsLeader;
	UPROPERTY(BlueprintReadOnly)		bool								IsReady;
	UPROPERTY(BlueprintReadOnly)		int32								ElapsedSeconds;
	UPROPERTY(BlueprintReadOnly)		ESearchSessionStatus				Status;
	UPROPERTY(BlueprintReadOnly)		FSessionMatchOptions				Options;
	UPROPERTY(BlueprintReadOnly)		TArray<FQueueMemberContainer>		Members;

	bool IsInSquad() const
	{
		return Members.Num() > 1;
	}

	bool IsAllowJoinMatch() const
	{
		return	Host.IsEmpty() == false 
			&& Token.IsEmpty() == false 
			&& (Status == ESearchSessionStatus::FoundJoin || Status == ESearchSessionStatus::FoundContinue);
	}

	bool IsAllowCancelSearch() const
	{
		return	Status == ESearchSessionStatus::None ||
				Status == ESearchSessionStatus::MatchSearch ||
				Status == ESearchSessionStatus::SearchStarting ||
				Status == ESearchSessionStatus::SearchStopping 
			;
	}

	friend bool operator==(const FSqaudInformation& InLeftValue, const FSqaudInformation& InRightValue)
	{
		return InLeftValue.LeaderId == InRightValue.LeaderId
				&& InLeftValue.Host == InRightValue.Host
				&& InLeftValue.Token == InRightValue.Token
				&& InLeftValue.Message == InRightValue.Message
				&& InLeftValue.IsDefence == InRightValue.IsDefence
				&& InLeftValue.IsLeader == InRightValue.IsLeader
				&& InLeftValue.IsReady == InRightValue.IsReady
				&& InLeftValue.ElapsedSeconds == InRightValue.ElapsedSeconds
				&& InLeftValue.Status == InRightValue.Status
				&& InLeftValue.Members.Num() == InRightValue.Members.Num();
	}

	friend bool operator!=(const FSqaudInformation& InLeftValue, const FSqaudInformation& InRightValue)
	{
		return operator==(InLeftValue, InRightValue) == false;
	}
};