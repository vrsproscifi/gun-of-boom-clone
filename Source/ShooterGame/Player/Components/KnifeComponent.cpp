// Fill out your copyright notice in the Description page of Project Settings.

#include "ShooterGame.h"
#include "KnifeComponent.h"
#include "KnifeItemEntity.h"

UKnifeComponent::UKnifeComponent()
	: Super()
{
	Mesh = CreateDefaultSubobject<USkeletalMeshComponent>(TEXT("Mesh"));

	PrimaryComponentTick.bCanEverTick = true;
	SetComponentTickEnabled(true);
}

void UKnifeComponent::OnRep_Instance()
{
	Super::OnRep_Instance();

	if (auto MyEntity = GetEntity<UKnifeItemEntity>())
	{
		Mesh->SetSkeletalMesh(MyEntity->GetTPPMeshData().Mesh);
		Mesh->SetAnimInstanceClass(MyEntity->GetTPPMeshData().Anim);
	}
}

void UKnifeComponent::TickComponent(float DeltaTime, ELevelTick TickType, FActorComponentTickFunction* ThisTickFunction)
{
	Super::TickComponent(DeltaTime, TickType, ThisTickFunction);

	if (auto MyEntity = GetEntity<UKnifeItemEntity>())
	{
		FHitResult HitResult;
		bCanUse = KnifeTrace(HitResult);
	}
}

bool UKnifeComponent::KnifeTrace(FHitResult& OutHitResult) const
{
	static FName KnifeTraceTag = FName(TEXT("KnifeTrace"));

	if (auto MyCharacter = GetValidObjectAs<ACharacter>(GetOwner()))
	{
		FVector ViewLocation;
		FRotator ViewRotation;

		MyCharacter->GetActorEyesViewPoint(ViewLocation, ViewRotation);

		// Perform trace to retrieve hit info
		FCollisionQueryParams TraceParams(KnifeTraceTag, true, MyCharacter);
		TraceParams.bReturnPhysicalMaterial = true;
		TraceParams.MobilityType = EQueryMobilityType::Any;
		TraceParams.bFindInitialOverlaps = true;

		GetWorld()->LineTraceSingleByChannel(OutHitResult, ViewLocation, ViewLocation + ViewRotation.Vector() * 5.0f, COLLISION_WEAPON, TraceParams);

		return Cast<ACharacter>(OutHitResult.GetActor()) != nullptr;
	}
	
	return false;
}

void UKnifeComponent::OnRep_Activated()
{
	if (auto MyProperty = GetItemProperty<FKnifeItemProperty>())
	{
		if (IsActivated())
		{
			//Mesh->AnimScriptInstance->Montage_Play(MyProperty->FireAnimation.)
			UGameplayStatics::PlaySoundAtLocation(GetWorld(), MyProperty->GetFireSound(), GetComponentLocation(), GetComponentRotation());
		}
	}
}

void UKnifeComponent::OnProcess()
{
	FHitResult LocalHitResult;
	const bool LocalTraceResult = KnifeTrace(LocalHitResult);

	if (LocalTraceResult)
	{
		auto TargetCharacter = Cast<ACharacter>(LocalHitResult.GetActor());
		if (TargetCharacter && TargetCharacter->IsValidLowLevel())
		{
			if (auto MyProperty = GetItemProperty<FKnifeItemProperty>())
			{
				UseItem();

				FPointDamageEvent PointDamageEvent;
				PointDamageEvent.Damage = MyProperty->Damage;
				PointDamageEvent.HitInfo = LocalHitResult;

				TargetCharacter->TakeDamage(PointDamageEvent.Damage, PointDamageEvent, nullptr, nullptr);
			}
		}
	}

	StopProcess();
}

bool UKnifeComponent::IsAllowExecuteProcess() const
{
	return IsAllowStartProcess();
}

bool UKnifeComponent::IsAllowStartProcess() const
{
	return bCanUse;
}

bool UKnifeComponent::StartProcess()
{
	Super::StartProcess();
	OnProcess();
	return true;
}

bool UKnifeComponent::StopProcess()
{
	Super::StopProcess();
	return true;
}
