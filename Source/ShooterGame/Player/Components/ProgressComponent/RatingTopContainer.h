#pragma once
//=========================================
#include "Engine/Types/IsoCountry.h"

#include "LokaGuid.h"
#include "RatingTopContainer.generated.h"


USTRUCT(BlueprintType)
struct FPlayerRatingTopContainer
{
	GENERATED_USTRUCT_BODY()

	UPROPERTY()	FLokaGuid Id;
	UPROPERTY()	FString Name;
	UPROPERTY()	int64 Score;
	UPROPERTY()	int32 Level;
	UPROPERTY()	int32 Position;
	UPROPERTY()	int32 EloRatingScore;
	UPROPERTY()	bool IsOnline;
	UPROPERTY()	EIsoCountry					Country;

	FPlayerRatingTopContainer()
	: Score(0)
	, Level(0)
	, Position(0)
	, EloRatingScore(0)
	, IsOnline(0)
	, Country(EIsoCountry::None)

	{
		
	}
	
};

USTRUCT(BlueprintType)
struct FRatingTopContainer
{
	GENERATED_USTRUCT_BODY()

	UPROPERTY()	TArray<FPlayerRatingTopContainer> PlayerRating;
	UPROPERTY()	TArray<FPlayerRatingTopContainer> PlayerEloRating;
};