#pragma once

//=========================================
#include "Engine/Types/IsoCountry.h"

//=========================================
#include "Player/PlayerGameAchievement.h"

//=========================================
#include "GameMatchMemberInformation.generated.h"

//=========================================
DECLARE_DELEGATE(FOnMatchUsedPremiumAccount);

//=========================================
UENUM(Blueprintable)
enum class EMatchMemberHistoryState : uint8
{
	None = 0,

	//	Покинул ли игрок бой
	Deserted = 1,

	//	Есть ли у игрока премиум аккаунт
	HasUsedPremiumAccount = 2,

	//	Показано ли было уведомление. Нужно для мастер сервера
	HasNotified = 4,

	//	Есть ли у игрока ежедневный бонус в 5 боев. Уже готово, нужно проверить
	HasUsedEveryDayBooster = 8,

	//	Есть ли у игрока бонус за счастливые часы. TODO
	HasUsedPrimeTimeBooster = 16,

	HasUsedAdsBooster = 32,

};
ENUM_CLASS_FLAGS(EMatchMemberHistoryState);

//=========================================
USTRUCT(BlueprintType)
struct FGameMatchMemberInformation
{
	GENERATED_USTRUCT_BODY()
	
	UPROPERTY()		FLokaGuid								MemberId;
	UPROPERTY()		FLokaGuid								PlayerId;
	UPROPERTY()		FLokaGuid								TeamId;
	UPROPERTY()		FString									PlayerName;
	UPROPERTY()		TArray<FPlayerGameAchievement>			Achievements;
	UPROPERTY()		EMatchMemberHistoryState				State;
	UPROPERTY()		uint64									EloRatingScore;
	UPROPERTY()		uint64									EloRatingScorePreview;
	UPROPERTY()		EIsoCountry								Country;


	FGameMatchMemberInformation()
		: State(EMatchMemberHistoryState::None)
		, EloRatingScore(0)
		, EloRatingScorePreview(0)
		, Country(EIsoCountry::None)
	{

	}


	void SetUsedPremiumAccount()
	{
		State |= EMatchMemberHistoryState::HasUsedPremiumAccount;
	}

	void SetUsedAdsBonus()
	{
		State |= EMatchMemberHistoryState::HasUsedAdsBooster;
	}

	bool HasUsedPremiumAccount() const
	{
		return EnumHasAnyFlags(State, EMatchMemberHistoryState::HasUsedPremiumAccount);
	}

	bool HasUsedEveryDayBooster() const
	{
		return EnumHasAnyFlags(State, EMatchMemberHistoryState::HasUsedEveryDayBooster);
	}

	bool HasUsedPrimeTimeBooster() const
	{
		return EnumHasAnyFlags(State, EMatchMemberHistoryState::HasUsedPrimeTimeBooster);
	}

	bool HasUsedAdsBooster() const
	{
		return EnumHasAnyFlags(State, EMatchMemberHistoryState::HasUsedAdsBooster);
	}

	

	bool HasDeserted() const
	{
		return EnumHasAnyFlags(State, EMatchMemberHistoryState::Deserted);
	}

	FString ToString() const
	{
		TMap<FString, FStringFormatArg> args;

		args.Add("MemberId", FStringFormatArg(MemberId.ToString()));
		args.Add("PlayerId", FStringFormatArg(PlayerId.ToString()));
		args.Add("TeamId", FStringFormatArg(TeamId.ToString()));

		args.Add("PlayerName", FStringFormatArg(PlayerName));

		args.Add("EloScore", FStringFormatArg(EloRatingScore));
		args.Add("EloScorePreview", FStringFormatArg(EloRatingScorePreview));


		args.Add("State", FStringFormatArg(static_cast<int32>(State)));

		return FString::Format(TEXT("> {PlayerName} - {PlayerId} / MemberId: {MemberId} in team [{TeamId}] | State: {State} | EloScore: {EloScorePreview} -> {EloScore} \r\n"), args);
	}
};