#pragma once

//=========================================
#include "GameMatchMemberInformation.h"

//=========================================
#include "Shared/SessionMatchOptions.h"

//=========================================
#include "GameMatchInformation.generated.h"

//=========================================
USTRUCT(BlueprintType)
struct FGameMatchInformation
{
	GENERATED_USTRUCT_BODY()

	FGameMatchInformation()
	{
		
	}

	UPROPERTY()		FLokaGuid								OwnerTeamId;
	UPROPERTY()		FLokaGuid								MatchId;
	UPROPERTY()		FSessionMatchOptions					Options;
	UPROPERTY()		TArray<FGameMatchMemberInformation>		Members;
	UPROPERTY()		TMap<FLokaGuid, int32>					Scores;

	//FGameMatchMemberInformation* GetMemberByPlayerId(const FLokaGuid& InPlayerId) const
	//{
	//	return Members.FindByPredicate([InPlayerId](const FGameMatchMemberInformation& InMemner) {return InMemner.PlayerId == InPlayerId; });
	//}

	FString ToString() const
	{
		TMap<FString, FStringFormatArg> args;
		args.Add("Header", FString("\n ============= FGameMatchInformation ============= \r\n"));

		args.Add("MatchId", FStringFormatArg(MatchId.ToString()));

		args.Add("Options", Options.ToString());



		FString members = "\n ============= Members ===================== \r\n";
		for(const auto& member : Members)
		{
			members += member.ToString();
		}

		args.Add("Members", members);


		return FString::Format(TEXT("{Header} MatchId: {MatchId}r\n {Options}r\n {Members}r\n"), args);
	}

};