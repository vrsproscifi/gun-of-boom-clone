#pragma once

//=========================================
#include "MatchUpgradeResponse.generated.h"

//=========================================
USTRUCT(BlueprintType)
struct FMatchUpgradeResponse
{
	GENERATED_USTRUCT_BODY()
	
	UPROPERTY()		uint64	Money;
	UPROPERTY()		uint64	Experience;
};