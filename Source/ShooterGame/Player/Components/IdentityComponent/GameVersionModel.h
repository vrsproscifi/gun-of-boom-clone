#pragma once
//==========================
#include "LokaGuid.h"

//==========================
#include "GameVersionModel.generated.h"

//==========================
UENUM()
enum class EDeployTarget : uint8
{
	None,

	Internal,
	Alpha,
	Beta,
	Production,
};

//==========================
USTRUCT()
struct FGameVersionModel
{
	GENERATED_USTRUCT_BODY()

	//==========================
	//			[ Property ]
	UPROPERTY()	int64 StoreVersionId;
	UPROPERTY()	int64 ServerVersionId;
	UPROPERTY()	EDeployTarget DeployTarget;

	//==========================
	//			[ Constructor ]

	FGameVersionModel()
		: StoreVersionId(0)
		, ServerVersionId(0)
		, DeployTarget(EDeployTarget::None)
	{
	}

	FGameVersionModel(const int64& InStoreVersionId, const int64& InServerVersionId, const int64& InDeployTarget)
		: StoreVersionId(InStoreVersionId)
		, ServerVersionId(InServerVersionId)
		, DeployTarget(static_cast<EDeployTarget>(InDeployTarget))
	{

	}

};
