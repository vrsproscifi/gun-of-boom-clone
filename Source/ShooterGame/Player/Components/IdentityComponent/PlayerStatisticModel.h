#pragma once
//=========================================
#include "Engine/Types/IsoCountry.h"

#include "Shared/PlayerOnlineStatus.h"
#include "Player/PlayerGameAchievement.h"
#include "PlayerExperience.h"


#include "PlayerStatisticModel.generated.h"

USTRUCT()
struct FPlayerStatisticItemModel
{
	GENERATED_USTRUCT_BODY()

		FPlayerStatisticItemModel()
		: SlotId(-1)
		, ModelId(-1)
		, Level(-1)
	{
		
	}


	//	Ид слота в который он был вставлен в профиле
	UPROPERTY()		uint32 SlotId;

	//	Модель предмета в репозитории, -1 если ошибка при загрузке предмета из БД
	UPROPERTY()		int32 ModelId;

	//	Уровень предмета
	UPROPERTY()		int32 Level;
};

USTRUCT()
struct FPlayerStatisticModel
{
	GENERATED_USTRUCT_BODY()


	FPlayerStatisticModel()
		: DefaulItemModelId(-1)
		, RegistrationDate(0)
		, LastActivityDate(0)
		, PremiumEndDate(0)
		, EloRatingScore(0)
		, PlayerStatus(EPlayerFriendStatus::None)
		, Country(EIsoCountry::None)

	{

	}

	//	Модель предмета в репозитории, -1 если ошибка при загрузке предмета из БД
	UPROPERTY()		int32 DefaulItemModelId;

	UPROPERTY()		uint64 RegistrationDate;
	UPROPERTY()		uint64 LastActivityDate;
	UPROPERTY()		uint64 PremiumEndDate;
	UPROPERTY()		uint64 EloRatingScore;

	UPROPERTY()		FPlayerExperience Experience;

	UPROPERTY()		FLokaGuid EntityId;
	
	UPROPERTY()		FString PlayerName;


	UPROPERTY()		EPlayerFriendStatus PlayerStatus;
	UPROPERTY()		EIsoCountry			Country;

	UPROPERTY()		TArray<FPlayerGameAchievement> Achievements;
	UPROPERTY()		TArray<FPlayerStatisticItemModel> Items;
};