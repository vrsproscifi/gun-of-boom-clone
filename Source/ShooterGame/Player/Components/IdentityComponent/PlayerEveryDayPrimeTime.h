#pragma once
#include "PlayerEveryDayPrimeTime.generated.h"

USTRUCT()
struct FPlayerEveryDayPrimeTime
{
	GENERATED_USTRUCT_BODY()

	FPlayerEveryDayPrimeTime()
		: AvalibleMatches(0)
		, NextAvalibleDate(0)
	{
		
	}

	/*
		Количество доступных матчей с удвоенной наградой. 
		Если равно 0, то матчи не доступны и нужно использовать время
	*/
	UPROPERTY()		int32 AvalibleMatches;
	
	/*
		Дата в UNIX-Time, когда матчи станут доступны. 
		Если матчи есть, то значение равно 0
	*/
	UPROPERTY()		int64 NextAvalibleDate;
};