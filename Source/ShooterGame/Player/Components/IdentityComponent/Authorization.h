#pragma once

#include "UEnumExtensions.h"
#include "Authorization.generated.h"

USTRUCT()
struct FPlayerAuthorizationRequest
{
	GENERATED_USTRUCT_BODY()

	//==========================
	//			[ Property ]

	UPROPERTY()	FString Login;
	UPROPERTY()	FString Password;
	UPROPERTY()	bool IsAuthorization;

	//==========================
	//			[ Constructor ]

	FPlayerAuthorizationRequest() : IsAuthorization(false)
	{
	}

	FPlayerAuthorizationRequest(const FString& login, const FString& password, const bool isAuthorization)
		: Login(login)
		, Password(password)
		, IsAuthorization(isAuthorization)
	{

	}

};

DECLARE_DELEGATE_OneParam(FOnAuthorizationRequest, const FPlayerAuthorizationRequest&);

UENUM()
enum class ELoginFromRemoteStatus : uint8
{
	LoginNotFound,
	LoginWasExist,

	InvalidLogin,
	InvalidPassword,

	Successfully,

	//-----------------
	BadRequest,
	IsNotAllowed,
	IsLockedOut,
	RequiresTwoFactor,

	//-----------------
	SessionExist,
	//----------------- Early Acces
	GameNotPurchased,
	GameNotAvailable
};

USTRUCT()
struct FPlayerAuthorizationResponse
{
	GENERATED_USTRUCT_BODY()

	FString GetStatus() const
	{
		return GetEnumValueAsString("ELoginFromRemoteStatus", Status);
	}

	UPROPERTY()	ELoginFromRemoteStatus Status;
	UPROPERTY()	FLokaGuid AccountId;
	UPROPERTY()	FString AccountName;

};