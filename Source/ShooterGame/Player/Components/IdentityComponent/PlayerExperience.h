#pragma once
#include "PlayerExperience.generated.h"

USTRUCT()
struct FPlayerExperience
{
	GENERATED_USTRUCT_BODY()

		FPlayerExperience()
		: IsLevelUpNotified(false)
		, IsNameConfirmed(false)
		, Level(0)
		, LastLevel(0)
		, SkillPoints(0)
		, Experience(0)
		, NextExperience(0)
		, ChangeNameTickets(0)
	{
		
	}

	UPROPERTY()		bool IsLevelUpNotified;

	UPROPERTY()		bool IsNameConfirmed;

	UPROPERTY()		int32 Level;
	
	UPROPERTY()		int32 LastLevel;

	UPROPERTY()		int32 SkillPoints;

	UPROPERTY()		int32 Experience;

	UPROPERTY()		int32 NextExperience;

	UPROPERTY()		int32 ChangeNameTickets;

};