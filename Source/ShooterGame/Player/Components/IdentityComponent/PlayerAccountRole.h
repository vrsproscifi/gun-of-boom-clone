#pragma once
#include "PlayerAccountRole.generated.h"


UENUM()
namespace PlayerAccountRole
{
	enum Type
	{
        None = 0,
        Partner = 1,
        Moderator = 2,
        Administrator = 4,
        Developer = 8,
	};
}