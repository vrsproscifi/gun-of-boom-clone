#pragma once
#include "LokaGuid.h"
#include "GameItemCost.h"

//=========================================
#include "Shared/AttachAccountService.h"


//=========================================
#include "Engine/Types/IsoCountry.h"

#include "PlayerExperience.h"
#include "PlayerAccountRole.h"
#include "PlayerEveryDayPrimeTime.h"
#include "PlayerEntityInfo.generated.h"

USTRUCT(Blueprintable)
struct FPlayerEveryDayModel
{
	GENERATED_USTRUCT_BODY()

	UPROPERTY(BlueprintReadOnly)	int32		JoinCount;
	UPROPERTY(BlueprintReadOnly)	bool		IsNotified;

	FPlayerEveryDayModel()
		: JoinCount(0)
		, IsNotified(true)
	{

	}

};

USTRUCT(Blueprintable)
struct FPlayerEntityInfo
{
	GENERATED_USTRUCT_BODY()

	UPROPERTY(BlueprintReadOnly)	TEnumAsByte<PlayerAccountRole::Type>		PlayerRole;

	
	UPROPERTY(BlueprintReadOnly)	FLokaGuid									PlayerId;
	UPROPERTY(BlueprintReadOnly)	FString										Login;
	UPROPERTY(BlueprintReadOnly)	TArray<FGameItemCost>						Cash;
	UPROPERTY()						FPlayerEveryDayPrimeTime					EveryDayPrimeTime;
	UPROPERTY()						FPlayerExperience							Experience;
	UPROPERTY()						FPlayerEveryDayModel						EveryDayAward;
	UPROPERTY(BlueprintReadOnly)	bool										IsSquadLeader;
	UPROPERTY(BlueprintReadOnly)	int32										PremiumEndDate;
	UPROPERTY(BlueprintReadOnly)	int32										EloRatingScore;
	UPROPERTY(BlueprintReadOnly)	EIsoCountry									Country;

	UPROPERTY(BlueprintReadOnly)	TArray<TEnumAsByte<EAccountService::Type>>	Accounts;

	
	UPROPERTY(BlueprintReadOnly)	int32										NumberOfMatches;
	//UPROPERTY(BlueprintReadOnly)	int32										NotifiedNumberOfMatches;

	FPlayerEntityInfo()
		: PlayerRole(PlayerAccountRole::None)
		, IsSquadLeader(false)
		, PremiumEndDate(0)
		, EloRatingScore(0)
		, Country(EIsoCountry::None)
		, NumberOfMatches(0)
	{

	}

	bool HasPremiumAccount() const
	{
		return !!PremiumEndDate && FDateTime::FromUnixTimestamp(PremiumEndDate) >= FDateTime::UtcNow();
	}

	int64 TakeCash(const EGameCurrency& currency) const
	{
		auto c = Cash.FindByPredicate([cc = currency](const FGameItemCost& cash)
		{
			return cash.Currency == cc;
		});

		if (c == nullptr) return 0;
		return c->Amount;
	}
};