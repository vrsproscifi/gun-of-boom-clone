#include "ShooterGame.h"
#include "IdentityComponent.h"

//======================================
#include "Engine/Http/RequestManager.h"

//======================================
//				Models
#include "IdentityComponent/CheckExistAccount.h"
#include "IdentityComponent/PlayerEntityInfo.h"
#include "IdentityComponent/PlayerStatisticModel.h"

//======================================
#include "Auth/SAuthForm.h"

//======================================
#include "BasicPlayerState.h"
#include "Game/OnlineGameMode.h"

//======================================
//	Для авторизации
//#include "OnlineSubsystemGoogle.h"
#include <OnlineIdentityInterface.h>
#include <OnlineExternalUIInterface.h>

#include <OnlineSubsystem.h>
#include "GameInstance/ShooterGameViewportClient.h"
#include "Notify/SMessageBox.h"
#include "Localization/TableBaseStrings.h"

//	Для оплаты
//#include "OnlineSubsystemGooglePlay.h"

//======================================
#include "GooglePlayHelper/GooglePlayHelper.h"
#include "BasicPlayerController.h"
#include "ShooterGameAnalytics.h"
//#include "CrashlyticsBlueprintLibrary.h"
#include "UEnumExtensions.h"


//======================================
#include "GooglePlayHelper/IdentityService.h"

//======================================
#if USE_GOOGLE_SERVICES
#include "Android/AndroidPlatformApplicationMisc.h"
#include "GooglePlayGameVersion.h"
#endif

//======================================
#include "Input/SInputWindow.h"
#include "GameSingletonExtensions.h"

//=====================================
#include "Item/Product/ProductItemEntity.h"
//#include "CrashlyticsBlueprintLibrary.h"
#include "NotifySoundsData.h"
#include "OnlinePlayerState.h"
#include "GameInstance/ShooterGameUserSettings.h"
#include "Containers/SChangeNameContainer.h"
#include "Notify/SNotifyContainer.h"

#include "Decorators/LokaTextDecorator.h"
#include "Decorators/LokaResourceDecorator.h"
#include "Decorators/DecoratorHelpers.h"


//======================================
UIdentityComponent::UIdentityComponent()
	: ToggleAuthFormAttempCount(1)
	, RequestLatestVersionAttemp(0)
	, RequestLatestVersionLimit(15)
#if USE_GOOGLE_SERVICES
	, CurrentGameVersion(GAME_STORE_VERSION, GAME_TARGET_SERVER, GAME_TARGET_DEPLOY)
#endif
{
#if USE_GOOGLE_SERVICES
	RequestAuthorization = CreateRequest(FOperationRequestServerHost::MasterClient, "Identity/OnGooglePlayAuthenticationV3", 0.250f, EOperationRequestTimerMethod::CallViaResponse, FTimerDelegate::CreateUObject(this, &UIdentityComponent::OnPrepareSendRequestAuthorization), FGuid(), 10, true);
	RequestAuthorization->OnLoseAttemps.BindUObject(this, &UIdentityComponent::OnBadNetworkConnection);
#else
	RequestAuthorization = CreateRequest(FOperationRequestServerHost::MasterClient, "Identity/OnAuthentication", FGuid());
#endif

	RequestDataDump = CreateRequest(FOperationRequestServerHost::MasterClient, "Identity/OnDump");

	RequestAttachAccountService = CreateRequest(FOperationRequestServerHost::MasterClient, "Identity/OnAttachAccountService");
	RequestAttachAccountService->BindEnum<EAccountService::Type>(200).AddUObject(this, &UIdentityComponent::OnAttachAccountService);
	RequestAttachAccountService->BindObject<FAttachAccountServiceExist>(400).AddUObject(this, &UIdentityComponent::OnAttachAccountServiceExist);
	RequestAttachAccountService->OnFailedResponse.BindUObject(this, &UIdentityComponent::OnAttachAccountServiceFailed);

	RequestReAttachAccountService = CreateRequest(FOperationRequestServerHost::MasterClient, "Identity/OnReAttachAccountService");
	RequestReAttachAccountService->BindEnum<EAccountService::Type>(200).AddUObject(this, &UIdentityComponent::OnAttachAccountService);
	RequestReAttachAccountService->BindObject<FAttachAccountServiceExistSuccessfully>(409).AddUObject(this, &UIdentityComponent::OnReAttachAccountServiceUseLast);
	RequestReAttachAccountService->OnFailedResponse.BindUObject(this, &UIdentityComponent::OnReAttachAccountServiceFailed);
	RequestReAttachAccountService->Bind(403).AddUObject(this, &UIdentityComponent::OnReAttachAccountServiceAuthorized);

	


	RequestAuthorization->BindObject<FPlayerAuthorizationResponse>(200).AddUObject(this, &UIdentityComponent::OnAuthorization);
	RequestAuthorization->OnFailedResponse.BindUObject(this, &UIdentityComponent::OnAuthorizationFailed);

	RequestClientData = CreateRequest(FOperationRequestServerHost::MasterClient, "Identity/OnRequestClientData", 0.1f, EOperationRequestTimerMethod::CallViaResponse, FTimerDelegate::CreateUObject(this, &UIdentityComponent::SendRequestClientData), 10, true);
	RequestClientData->BindObject<FPlayerEntityInfo>(200).AddUObject(this, &UIdentityComponent::OnPlayerData);
	RequestClientData->OnLoseAttemps.BindUObject(this, &UIdentityComponent::OnBadNetworkConnection);

	RequestClientDataUpdate = CreateRequest(FOperationRequestServerHost::MasterClient, "Identity/OnRequestUpdateClientData", 10, EOperationRequestTimerMethod::CallViaInterval, FTimerDelegate::CreateUObject(this, &UIdentityComponent::SendRequestClientDataUpdate), 10, true);
	RequestClientDataUpdate->BindObject<FPlayerEntityInfo>(200).AddUObject(this, &UIdentityComponent::OnPlayerDataUpdate);
	RequestClientDataUpdate->OnLoseAttemps.BindUObject(this, &UIdentityComponent::OnBadNetworkConnection);

	RequestIsoCountry = CreateRequest(FOperationRequestServerHost::MasterClient, "Identity/OnRequestIsoCountry", 5.0f, EOperationRequestTimerMethod::CallViaIntervalOrResponse, FTimerDelegate::CreateUObject(this, &UIdentityComponent::SendRequestIsoCountry), 10, true);
	RequestIsoCountry->BindEnum<EIsoCountry>(200).AddUObject(this, &UIdentityComponent::OnRequestIsoCountry);
	RequestIsoCountry->OnFailedResponse.BindUObject(this, &UIdentityComponent::OnRequestIsoCountryFailed);

	RequestExist = CreateRequest(FOperationRequestServerHost::MasterClient, "Identity/IsExistGameAccount", 0.1f, EOperationRequestTimerMethod::CallViaResponse, FTimerDelegate::CreateUObject(this, &UIdentityComponent::SendRequestExist), 10, true);
	RequestExist->Bind(200).AddUObject(this, &UIdentityComponent::OnExist);
	RequestExist->Bind(404).AddUObject(this, &UIdentityComponent::OnExistNotFound);
	RequestExist->Bind(401).AddUObject(this, &UIdentityComponent::OnExistNotAuthorized);
	RequestExist->Bind(900).AddUObject(this, &UIdentityComponent::OnExistNotAuthorized);
	RequestExist->Bind(901).AddUObject(this, &UIdentityComponent::OnExistNotAuthorized);
	RequestExist->Bind(902).AddUObject(this, &UIdentityComponent::OnExistNotAuthorized);
	RequestExist->Bind(903).AddUObject(this, &UIdentityComponent::OnExistNotAuthorized);
	RequestExist->Bind(904).AddUObject(this, &UIdentityComponent::OnExistNotAuthorized);
	RequestExist->Bind(905).AddUObject(this, &UIdentityComponent::OnExistNotAuthorized);
	RequestExist->OnFailedResponse.BindUObject(this, &UIdentityComponent::OnBadNetworkConnection);
	RequestExist->OnLoseAttemps.BindUObject(this, &UIdentityComponent::OnBadNetworkConnection);

	RequestLatestVersion = CreateRequest(FOperationRequestServerHost::MasterClient, "Identity/IsLatestVersion");
	RequestLatestVersion->BindObject<FGameVersionModel>(200).AddUObject(this, &UIdentityComponent::OnRequestLatestVersion);
	RequestLatestVersion->Bind(0).AddUObject(this, &UIdentityComponent::OnRequestLatestVersion_ServerError);
	RequestLatestVersion->Bind(408).AddUObject(this, &UIdentityComponent::OnRequestLatestVersion_ServerError);
	RequestLatestVersion->Bind(417).AddUObject(this, &UIdentityComponent::OnRequestLatestVersion_ServerError);
	RequestLatestVersion->Bind(504).AddUObject(this, &UIdentityComponent::OnRequestLatestVersion_ServerError);
	RequestLatestVersion->Bind(522).AddUObject(this, &UIdentityComponent::OnRequestLatestVersion_ServerError);
	RequestLatestVersion->Bind(524).AddUObject(this, &UIdentityComponent::OnRequestLatestVersion_ServerError);
	RequestLatestVersion->Bind(503).AddUObject(this, &UIdentityComponent::OnRequestLatestVersion_ServerError);
	RequestLatestVersion->Bind(521).AddUObject(this, &UIdentityComponent::OnRequestLatestVersion_ServerError);
	RequestLatestVersion->Bind(523).AddUObject(this, &UIdentityComponent::OnRequestLatestVersion_ServerError);
	RequestLatestVersion->Bind(500).AddUObject(this, &UIdentityComponent::OnRequestLatestVersion_ServerError);
	RequestLatestVersion->OnFailedResponse.BindUObject(this, &UIdentityComponent::OnRequestLatestVersion_Failed);

	//
	RequestPlayerStatistic = CreateRequest(FOperationRequestServerHost::MasterClient, "Identity/GetPlayerStatistic");
	RequestPlayerStatistic->BindObject<FPlayerStatisticModel>(200).AddUObject(this, &UIdentityComponent::OnRequestPlayerStatistic);
	RequestPlayerStatistic->OnFailedResponse.BindUObject(this, &UIdentityComponent::OnRequestPlayerStatistic_Failed);

	//
	RequestChangeName = CreateRequest(FOperationRequestServerHost::MasterClient, "Identity/OnChangePlayerName");
	RequestChangeName->BindObject<FString>(200).AddUObject(this, &UIdentityComponent::OnRequestChangeName);
	RequestChangeName->BindObject<FString>(409).AddUObject(this, &UIdentityComponent::OnRequestChangeName_NameExist);
	RequestChangeName->BindObject<FString>(402).AddUObject(this, &UIdentityComponent::OnRequestChangeName_NotEnouthMoney);
	RequestChangeName->Bind(404).AddUObject(this, &UIdentityComponent::OnRequestChangeName_EmptyString);

	RequestChangeName->OnFailedResponse.BindUObject(this, &UIdentityComponent::OnRequestChangeNameFailed);


	bIsSimulation = false;
}

void UIdentityComponent::SendRequestChangeName(const FText& InName) const
{
	const auto Name = InName.ToString();
	
	//============================================
	FShooterGameAnalytics::RecordDesignEvent("UI:Lobby:ChangeName:SendRequest");

	//============================================
	//UCrashlyticsBlueprintLibrary::WriteLog(FString::Printf(TEXT("SendRequestChangeName: %s"), *Name));

	//============================================
	RequestChangeName->SendRequestObject(Name);
}

void UIdentityComponent::OnValidateTimeAuth()
{
	//============================================
	if (auto TM = GetTimerManager())
	{
		TM->ClearTimer(ValidateAuthTimerHandle);
	}

	//============================================
	//UCrashlyticsBlueprintLibrary::WriteLog(FString::Printf(TEXT("ValidateAuthTime | bPlayerHasLogIn: %d | IdentityToken: %s"), bPlayerHasLogIn, *GetIdentityToken().Get().ToString()));

	//============================================
	if(bPlayerHasLogIn || (GetIdentityToken().IsSet() && GetIdentityToken().Get().IsValid()))
	{
		//UCrashlyticsBlueprintLibrary::SetField("ValidateAuthTime", true);
		FShooterGameAnalytics::RecordDesignEvent("UI:Lobby:Authorization:TimeValidation:Successfully");
	}
	else if (WITH_EDITOR == 0)
	{
		//UCrashlyticsBlueprintLibrary::SetField("ValidateAuthTime", false);
		FShooterGameAnalytics::RecordDesignEvent("UI:Lobby:Authorization:TimeValidation:Failed");
		
		ShowRestarGameErrorMessage
		(
			FName("UIdentityComponent::OnValidateTimeAuth"),
			NSLOCTEXT("Network", "Network.OnBadNetworkConnection.Title", "Problem with internet connection"),
			NSLOCTEXT("Network", "Authentication.OnAuthorization.OnValidateTimeAuth", "We noticed that you could not log in. \nPlease try to check your internet connection, restart the game or come back later. \nIf the problem persists, please contact customer service or send us an email swagpm@gmail.com")
		);
	}
}


void UIdentityComponent::OnRequestChangeName(const FString& InInputPlayerName)
{
	SNotifyContainer::ShowNotify("OnRequestChangeName", FText::Format(NSLOCTEXT("IdentityComponent", "IdentityComponent.ChangeName.Successfully", "You have successfully changed player name to {0}"), FText::FromString(InInputPlayerName) ));
}

void UIdentityComponent::OnRequestChangeName_NotEnouthMoney(const FString& InInputPlayerName)
{
	//============================================
	//UCrashlyticsBlueprintLibrary::WriteLog("SendRequestChangeName:NotEnouthMoney");

	//============================================
	ChangeNameDialog(TEXT("OnRequestChangeName_NotEnouthMoney"), InInputPlayerName, NSLOCTEXT("IdentityComponent", "IdentityComponent.ChangeName.NotEnouthMoney", "Not enouth crystals for change name!"));
}

void UIdentityComponent::OnRequestChangeName_EmptyString()
{	
	//============================================
	//UCrashlyticsBlueprintLibrary::WriteLog("SendRequestChangeName:EmptyString");

	//============================================
	ChangeNameDialog("ChangeName_EmptyString", PlayerInfo.Login, NSLOCTEXT("IdentityComponent", "IdentityComponent.ChangeName.EmptyString", "Entered string is empty."));
}

void UIdentityComponent::OnRequestChangeName_NameExist(const FString& InInputPlayerName)
{	
	//============================================
	//UCrashlyticsBlueprintLibrary::WriteLog("SendRequestChangeName:NameExist");

	//============================================
	ChangeNameDialog("ChangeName_NameExist", InInputPlayerName, NSLOCTEXT("IdentityComponent", "IdentityComponent.ChangeName.NameExist", "The name you entered already exists, please try again."));
}

void UIdentityComponent::OnRequestChangeNameFailed(const FRequestExecuteError& InErrorData)
{	
	//============================================
	//UCrashlyticsBlueprintLibrary::WriteLog("SendRequestChangeName:Failed");

	//============================================
	FShooterGameAnalytics::RecordDesignEvent("UI:Lobby:ChangeName:Fail:Code" + InErrorData.GetStatusCode());
	ChangeNameDialog("ChangeName_Failed", PlayerInfo.Login, FText::FromString(FString::Printf(TEXT("Error: [%d]%s"), InErrorData.Status, *InErrorData.ToString())));
}


void UIdentityComponent::SendRequestPlayerStatistic(const FGuid& InPlayerId) const
{
	RequestPlayerStatistic->SendRequestObject<FGuid>(InPlayerId);
}

void UIdentityComponent::OnRequestPlayerStatistic(const FPlayerStatisticModel& InPlayerStatistic)
{
	Statistic = InPlayerStatistic;
	OnStatisticUpdate.Broadcast();
	//ShowErrorMessage("OnRequestPlayerStatistic", FString::Printf(TEXT("[OnRequestPlayerStatistic][Name: %s][Level: %d][Achievements: %d]"), *InPlayerStatistic.PlayerName, InPlayerStatistic.Level, InPlayerStatistic.Achievements.Num()));
}

void UIdentityComponent::OnRequestPlayerStatistic_Failed(const FRequestExecuteError& InErrorData)
{
	FShooterGameAnalytics::RecordDesignEvent("UI:Lobby:PlayerStatistic:Fail:Code" + InErrorData.GetStatusCode());
	FShooterGameAnalytics::RecordErrorEvent(EErrorSeverity::warning, "Failed to load player statistics.");
	ShowErrorMessage("OnRequestPlayerStatistic_Failed", FString::Printf(TEXT("Failed to load player statistics.\n Details of the error: \n %s"), *InErrorData.ToString()));
}


void UIdentityComponent::OnBadNetworkConnection(const FRequestExecuteError& InErrorData)
{	
#if !UE_SERVER
	//======================================================
	//UCrashlyticsBlueprintLibrary::SetField("BadNetworkConnections", ++FShooterGameAnalytics::BadNetworkConnections);

	//============================================
	//UCrashlyticsBlueprintLibrary::WriteLog("UIdentityComponent:OnBadNetworkConnection:" + InErrorData.GetStatusCode());

	//============================================
	FShooterGameAnalytics::RecordDesignEvent(FString::Printf(TEXT("UI:Lobby:%s:Fail:Code%d"), *InErrorData.Operation, InErrorData.Status));

	//============================================
	if ((InErrorData.IsTimeout() || InErrorData.IsBadGateway()) && WITH_EDITOR == 0)
	{
		FShooterGameAnalytics::RecordErrorEvent(EErrorSeverity::critical, "UIdentityComponent::OnBadNetworkConnection");

		ShowRestarGameErrorMessage
		(
			FName("UIdentityComponent::OnBadNetworkConnection"),
			NSLOCTEXT("Network", "Network.OnBadNetworkConnection.Title", "Problem with internet connection"), 
			FText::Format(NSLOCTEXT("Network", "Network.OnBadNetworkConnection.Message", "The request to the server could not be completed.\n The problem with the Internet connection.\n Do you want return to the lobby?"), FText::FromString(InErrorData.ToString()))
		);

		UE_LOG(LogInit, Error, TEXT("> [UIdentityComponent::OnBadNetworkConnection] \nError: %s"), *InErrorData.ToString());
	}
#endif
}

void UIdentityComponent::OnAuthorizeAsSimulation()
{
	//============================================
	//UCrashlyticsBlueprintLibrary::WriteLog("UIdentityComponent:OnAuthorizeAsSimulation");

	//============================================
	UE_LOG(LogInit, Display, TEXT("UIdentityComponent::OnAuthorizeAsSimulation[%s][%d]"), *GetIdentityToken().Get().ToString(), static_cast<int32>(GetNetMode()));

	bIsSimulation = true;
	OnRep_Simulation();
}

void UIdentityComponent::BeginPlay()
{
	//============================================
	Super::BeginPlay();
	//android.permission.READ_PHONE_STATE
	//android.permission.WRITE_EXTERNAL_STORAGE
	//============================================
	OnRep_Simulation();

	//============================================
#if !UE_SERVER
	if (!IsMultiPlayer())
	{
		//============================================
		//UCrashlyticsBlueprintLibrary::WriteLog("UIdentityComponent:BeginPlay");

		FShooterGameAnalytics::RecordDesignEvent("UI:Lobby:BeginPlay");

		//============================================
		if (auto TM = GetTimerManager())
		{
			TM->SetTimer(ValidateAuthTimerHandle, FTimerDelegate::CreateUObject(this, &UIdentityComponent::OnValidateTimeAuth), 15.0f, false, 15.0f);
		}
	}
#endif
}

void UIdentityComponent::EndPlay(const EEndPlayReason::Type EndPlayReason)
{
	//============================================
	Super::EndPlay(EndPlayReason);

	//============================================
#if !UE_SERVER
	if (!IsMultiPlayer())
	{
		//============================================
		//UCrashlyticsBlueprintLibrary::WriteLog("UIdentityComponent:EndPlay:" + FString::FormatAsNumber(EndPlayReason));
		FShooterGameAnalytics::RecordDesignEvent("UI:Lobby:EndPlay");
	}
#endif
}


// Send requests/Отправка запросов


void UIdentityComponent::SendRequestAndValidateVersion(const FGameVersionModel& version)
{
#if USE_GOOGLE_SERVICES
#if !UE_SERVER

	//============================================
	//UCrashlyticsBlueprintLibrary::SetField("RequestAndValidateVersion", "Prepare");

	//============================================
	//	Запрещаем отключать экран во время игры
	FAndroidApplicationMisc::ControlScreensaver(FAndroidApplicationMisc::EScreenSaverAction::Disable);

	//============================================
#ifdef GAME_TARGET_DEPLOY 
#if GAME_TARGET_DEPLOY == 4
	FShooterGameAnalytics::StartSession("6e249cbbbd3aa504c0abc8237448ce50", "642ae6210f1cc76af712353a526c67b83f2085cd");
#endif // GAME_TARGET_DEPLOY == 4
#endif
	FShooterGameAnalytics::RecordDesignEvent("UI:Lobby:ValidateVersion:SendRequest");

	//-----------------------------------

	//UGameAnalytics::initialize();
	//UGameAnalytics::startSession();
#endif

	if (!IsMultiPlayer())
	{
		RequestLatestVersion->SendRequestObject(version);
	}
#else
	if (!FPlatformProperties::IsServerOnly())
	{
		ToggleAuthForm(true);
	}
#endif
}

bool UIdentityComponent::IsMultiPlayer() const
{
	if (const auto gm = GetGameMode<AOnlineGameMode>())
	{
		return gm->bIsMultiPlayerGameMode;
	}
	return true;
}

void UIdentityComponent::OnRequestLatestVersion(const FGameVersionModel& version)
{
	//UCrashlyticsBlueprintLibrary::SetField("RequestAndValidateVersion", "Successfully");
	FShooterGameAnalytics::RecordDesignEvent("UI:Lobby:ValidateVersion:Successfully");

	UE_LOG(LogInit, Display, TEXT("[UIdentityComponent::OnRequestLatestVersion] [StoreVersionId][%d / %d] | [ServerVersionId][%d / %d]"),
	CurrentGameVersion.StoreVersionId, version.StoreVersionId, CurrentGameVersion.ServerVersionId, version.ServerVersionId);

	StartTimer(RequestAuthorization);

	//if(CurrentGameVersion.EntityId == version.EntityId && CurrentGameVersion.StoreVersionId == version.StoreVersionId)
	//{
	//	// Используется Request Manager для вызова OnTryGoogleAuthorize, если не удается выполнить запрос
	//	{
	//		//	OnTryGoogleAuthorize();
	//	}
	//}
	//else
	//{
	//	ShowRequestUpdateNotify(1);
	//}
}

void UIdentityComponent::OnRequestLatestVersion_ServerError()
{
	//==========================================
	FShooterGameAnalytics::RecordDesignEvent("UI:Lobby:ValidateVersion:Fail:Code500");

	//==========================================
	RequestLatestVersionAttemp++;

	//==========================================
	if (RequestLatestVersionAttemp <= RequestLatestVersionLimit)
	{
		if (!IsMultiPlayer())
		{
			RequestLatestVersion->SendRequestObject(CurrentGameVersion);
		}
	}

	//==========================================
	if (RequestLatestVersionAttemp >= RequestLatestVersionLimit)
	{
		ShowRestarGameErrorMessage
		(
			FName("OnRequestLatestVersion_ServerError"),
			NSLOCTEXT("Authentication", "Authentication.OnRequestLatestVersion.Failed.Title", "Failed validate game version"),
			NSLOCTEXT("Authentication", "Authentication.OnRequestLatestVersion.Failed.FinalMessage", "Unfortunately, we were unable to verify the version of your game. \nWe tried to do our best to resolve this misunderstanding. \n\tTry updating the app or enter the game later. \n\nIf the problem is not solved, please contact customer service or send an email to swagpm@gmail.com")
		);
	}
	else if (RequestLatestVersionAttemp > 3)
	{
		ShowRestarGameErrorMessage
		(
			FName("OnRequestLatestVersion_ServerError"),
			NSLOCTEXT("Authentication", "Authentication.OnRequestLatestVersion.Failed.Title", "Failed validate game version"),
			FText::Format
			(
				NSLOCTEXT("Authentication", "Authentication.OnRequestLatestVersion.Failed.Message", "Unfortunately, we were unable to verify the version of your game.\n We are trying to do everything in solving this misunderstanding. \n\t\tAttempt {0} of {1}."),
				RequestLatestVersionAttemp, RequestLatestVersionLimit
			)
		);
	}
}

void UIdentityComponent::OnRequestLatestVersion_Failed(const FRequestExecuteError& InErrorData)
{
	//UCrashlyticsBlueprintLibrary::SetField("RequestAndValidateVersion", "Failed");
	FShooterGameAnalytics::RecordDesignEvent("UI:Lobby:ValidateVersion:Fail:Code" + InErrorData.GetStatusCode());

	UE_LOG(LogInit, Display, TEXT("[UIdentityComponent::OnRequestLatestVersion_Failed] %s"), *InErrorData.ToString());
	ShowRequestUpdateNotify(2);
}

void UIdentityComponent::ShowRequestUpdateNotify(const int32& code)
{
	QueueBegin(SMessageBox, TEXT("RequestUpdate"))

		SMessageBox::Get()->SetHeaderText(NSLOCTEXT("Authentication", "Authentication.ShowRequestUpdateNotify.Title", "Update required"));
		SMessageBox::Get()->SetContent(
			FText::Format(
			NSLOCTEXT("Authentication", "Authentication.ShowRequestUpdateNotify.Content", "To enter the game you need to update the application. Click Next to go to Google Play and perform the upgrade. Error code: {0}"), FText::AsNumber(code, &FNumberFormattingOptions::DefaultNoGrouping())));
		
	SMessageBox::Get()->SetButtonsText(FTableBaseStrings::GetBaseText(EBaseStrings::Continue));
		SMessageBox::Get()->OnMessageBoxButton.AddLambda([&](EMessageBoxButton InButton)
		{
			FGooglePlayHelper::TravelToGooglePlay();
		});
		SMessageBox::Get()->ToggleWidget(true);
	QueueEnd
}


bool UIdentityComponent::SendRequestAuthorization_Validate(const FPlayerAuthorizationRequest& InRequest) { return true; }
void UIdentityComponent::SendRequestAuthorization_Implementation(const FPlayerAuthorizationRequest& InRequest)
{
	RequestAuthorization->SendRequestObject(InRequest);
}

bool UIdentityComponent::SendRequestChangeRegion_Validate() { return true; }
void UIdentityComponent::SendRequestChangeRegion_Implementation() {}

bool UIdentityComponent::SendRequestClientData_Validate() { return true; }
void UIdentityComponent::SendRequestClientData_Implementation()
{
	RequestClientData->GetRequest();
}

bool UIdentityComponent::SendRequestClientDataUpdate_Validate() { return true; }
void UIdentityComponent::SendRequestClientDataUpdate_Implementation()
{
	RequestClientDataUpdate->GetRequest();
}

bool UIdentityComponent::SendRequestStatus_Validate() { return true; }
void UIdentityComponent::SendRequestStatus_Implementation() {}

bool UIdentityComponent::SendRequestExist_Validate() { return true; }
void UIdentityComponent::SendRequestExist_Implementation()
{
	RequestExist->GetRequest();
}

// Receive answers/Получение ответов
void UIdentityComponent::OnAuthorization(const FPlayerAuthorizationResponse& InData)
{		
	//============================================
	//UCrashlyticsBlueprintLibrary::WriteLog("UIdentityComponent:OnAuthorization:" + InData.GetStatus());

	//============================================
	FShooterGameAnalytics::RecordDesignEvent("UI:Lobby:Authorization:Successfully:" + InData.GetStatus());

	UE_LOG(LogInit, Display, TEXT("[UIdentityComponent::OnAuthorization][AccountId: %s][HasAuthority: %d]"), *InData.AccountId.ToString(), (int)HasAuthority());

	if (HasAuthority())
	{
		IsAuthorized = false;

		//UCrashlyticsBlueprintLibrary::SetField("OnAuthorization", InData.GetStatus());
		//UCrashlyticsBlueprintLibrary::SetField("IdentityToken", InData.AccountId);

		if (InData.Status != ELoginFromRemoteStatus::Successfully)
		{
			ShowErrorOnAuthForm(InData.Status);
		}
		else
		{
			bPlayerHasLogIn = true;
			auto MyOwnerState = GetBasicPlayerState();
			if (MyOwnerState)
			{
				MyOwnerState->SetIdentityToken(InData.AccountId);
			}

#if !UE_SERVER
			InitFaceBookSystem();
#endif

			StartTimer(RequestExist);
		}
	}
}

void UIdentityComponent::OnAuthorizationFailed(const FRequestExecuteError& InErrorData)
{
	//============================================
	//UCrashlyticsBlueprintLibrary::WriteLog("UIdentityComponent:OnAuthorizationFailed:" + InErrorData.GetStatusCode());

	FShooterGameAnalytics::RecordDesignEvent("UI:Lobby:Authorization:Fail:Code" + InErrorData.GetStatusCode());

	UE_LOG(LogInit, Error, TEXT("[UIdentityComponent::OnAuthorizationFailed]"), *InErrorData.ToString());
	ShowErrorOnAuthForm(ELoginFromRemoteStatus::BadRequest, InErrorData.ToString());
	OnBadNetworkConnection(InErrorData);
}

void UIdentityComponent::OnExist()
{
	//UCrashlyticsBlueprintLibrary::WriteLog("UIdentityComponent:OnExist");

	UE_LOG(LogInit, Display, TEXT("[UIdentityComponent::OnExist][HasAuthority: %d]"), (int)HasAuthority());

	auto MyOwnerState = GetBasicPlayerState();
	if (MyOwnerState)
	{
		MyOwnerState->SetIdentityToken(GetIdentityToken().Get());
	}

	ToggleAuthForm(false);


	OnAuthorizationEvent.Broadcast(GetIdentityToken().Get());
	StartTimer(RequestClientData);
}

void UIdentityComponent::OnExistNotFound()
{
	UE_LOG(LogInit, Display, TEXT("[UIdentityComponent::OnExistNotFound][HasAuthority: %d]"), (int)HasAuthority());

	//OnClientExistNotFound();
}


void UIdentityComponent::OnPrepareSendRequestAuthorization()
{
	FPlayerAuthorizationRequest rquest;

#if USE_GUID_USER_ID
	//=====================================
	FGuid LoginUserId;

	//=====================================
	UShooterGameUserSettings* Settings = Cast<UShooterGameUserSettings>(GEngine->GameUserSettings);
	if (Settings)
	{
		if (Settings->GetLoginUserId(LoginUserId) == false)
		{
			LoginUserId = FGuid::NewGuid();
			Settings->SetLoginUserId(LoginUserId);
		}
	}

	rquest.Login = LoginUserId.ToString();
	rquest.Password = LoginUserId.ToString();
#else
	//=====================================
	FString LoginUserId;

	//=====================================
	UShooterGameUserSettings* Settings = Cast<UShooterGameUserSettings>(GEngine->GameUserSettings);
	if (Settings && Settings->GetLoginUserId(LoginUserId) == false)
	{
		LoginUserId = FPlatformMisc::GetDeviceId();
		if (LoginUserId.IsEmpty())
		{
			LoginUserId = FGuid::NewGuid().ToString();
		}

		Settings->SetLoginUserId(LoginUserId);
	}

	rquest.Login = LoginUserId;
	rquest.Password = LoginUserId;
#endif

	//=====================================
	//uint8 LoginBuffer[20];
	//
	////=====================================
	//FString LoginSalted = FString::Printf(TEXT("Loka%sGame%sAw"), LoginUserId.ToString(), LoginUserId.ToString());
	//
	////=====================================
	//FSHA1::HashBuffer(*LoginSalted, LoginSalted.GetAllocatedSize(), &LoginBuffer[0]);

	//=====================================

	//rquest.Password = FString::FromBlob(LoginBuffer, 20);

	//=====================================
	SendRequestAuthorization(rquest);
}


bool UIdentityComponent::InitFaceBookSystem() const
{
	auto FaceBookSystem = IOnlineSubsystem::Get(FACEBOOK_SUBSYSTEM);
	if (FaceBookSystem)
	{
		//UCrashlyticsBlueprintLibrary::SetField("FACEBOOK_SUBSYSTEM", true);

		auto IsInitSuccessfully = FaceBookSystem->Init();
		if (IsInitSuccessfully)
		{
			//UCrashlyticsBlueprintLibrary::WriteLog("UIdentityComponent:InitFaceBookSystem:Successfully");
		}
		else
		{
			//UCrashlyticsBlueprintLibrary::WriteLog("UIdentityComponent:InitFaceBookSystem:Failed2");
			ShowErrorMessage("InitFaceBookSystem", "Failed to start the Facebook service. #2");
		}
		return IsInitSuccessfully;
	}
	else
	{
		//UCrashlyticsBlueprintLibrary::SetField("FACEBOOK_SUBSYSTEM", false);
		//UCrashlyticsBlueprintLibrary::WriteLog("UIdentityComponent:InitFaceBookSystem:Failed");
		ShowErrorMessage("InitFaceBookSystem", "Failed to start the Facebook service. #1");
	}
	return false;
}

void UIdentityComponent::OnReAttachAccountServiceAuthorized()
{
	SNotifyContainer::ShowNotify
	(
		"OnReAttachAccountServiceUseLast",
		NSLOCTEXT("Authentication", "Authentication.:OnReAttachAccountServiceAuthorized", "Could not connect account. Old account already authorized or in game")
	);
}

void UIdentityComponent::OnReAttachAccountServiceUseLast(const FAttachAccountServiceExistSuccessfully& InLastPlayerToken)
{
	//=====================================
	UShooterGameUserSettings* Settings = Cast<UShooterGameUserSettings>(GEngine->GameUserSettings);
	if (Settings)
	{
#if USE_GUID_USER_ID

		//==============================
		FGuid Account, Token;

		//==============================
		const auto bAccount = FGuid::Parse(InLastPlayerToken.Account, Account);
		const auto bToken = FGuid::Parse(InLastPlayerToken.Token, Token);

		//==============================
		if (bAccount == false)
		{
			SNotifyContainer::ShowNotify
			(
				"OnReAttachAccountServiceUseLast",
				FText::Format(NSLOCTEXT("Authentication", "Authentication.:OnReAttachAccountServiceUseLast.Parse.Account", "Could not connect account. Failed parse user account: {0}"), FText::FromString(InLastPlayerToken.Account))
			);
		}
		//==============================
		else if (bToken == false)
		{
			SNotifyContainer::ShowNotify
			(
				"OnReAttachAccountServiceUseLast",
				FText::Format(NSLOCTEXT("Authentication", "Authentication.:OnReAttachAccountServiceUseLast.Parse.Token", "Could not connect account. Failed parse user token: {0}"), FText::FromString(InLastPlayerToken.Token))
			);
		}
		else
		{
			Settings->SetLoginUserId(Account);

			SNotifyContainer::ShowNotify
			(
				"OnAttachAccountService",
				NSLOCTEXT("Authentication", "Authentication.:OnReAttachAccountServiceUseLast.Successfully", "You have successfully restored your old account. To access your account, restart the game if it did not happen automatically.")
			);

			auto MyOwnerState = GetBasicPlayerState();
			MyOwnerState->SetIdentityToken(Token);

			auto PC = GetBaseController();
			PC->PlayerTravelToLobby();
		}
#else
	#error Not IMPL!
#endif
	}
	else
	{
		SNotifyContainer::ShowNotify
		(
			"OnReAttachAccountServiceUseLast",
			NSLOCTEXT("Authentication", "Authentication.:OnReAttachAccountServiceUseLast.Settings", "Could not connect account. Failed access to Settings Storage")
		);
	}
}

void UIdentityComponent::OnReAttachAccountServiceFailed(const FRequestExecuteError& InErrorData)
{
	SNotifyContainer::ShowNotify
	(
		"OnReAttachAccountServiceFailed", 
		FText::Format(NSLOCTEXT("Authentication", "Authentication.:OnReAttachAccountService.Failed", "Could not connect account. \nError Message: {0}"), InErrorData.ToShortText())
	);

	SMessageBox::ShowErrorMessageText
	(
		TEXT("OnAttachAccountService"),
		FText::Format(FText::FromString(FString("OnReAttachAccountService | {0}")), InErrorData.ToText())
	);
}

void UIdentityComponent::OnAttachAccountService(const EAccountService::Type& InAttachedToService)
{
	SNotifyContainer::ShowNotify
	(
		"OnAttachAccountService",
		FText::Format(NSLOCTEXT("Authentication", "Authentication.:OnAttachAccountService.Successfully", "You have successfully connected {0} account to the game profile"), FText::FromString(GetEnumValueAsString("EAccountService", InAttachedToService)))
	);
}

void UIdentityComponent::OnAttachAccountServiceExist(const FAttachAccountServiceExist& InExistAccount)
{
	FFormatNamedArguments args;
	args.Add("Name", FFormatArgumentValue(FTextDecoratorHelper(InExistAccount.Name).SetFont(FString("Oswald.Bold")).ToText()));
	args.Add("Level", FFormatArgumentValue(FResourceDecoratorHelper(InExistAccount.Level, EResourceTextBoxType::Level).ToText()));
	args.Add("Money", FFormatArgumentValue(FResourceDecoratorHelper(InExistAccount.Money, EResourceTextBoxType::Money).ToText()));
	args.Add("Donate", FFormatArgumentValue(FResourceDecoratorHelper(InExistAccount.Donate, EResourceTextBoxType::Donate).ToText()));
	args.Add("CreatedDate", FFormatArgumentValue(FText::AsDate(FDateTime::FromUnixTimestamp(InExistAccount.CreatedDate))));
	args.Add("LastActivityDate", FFormatArgumentValue(FText::AsDate(FDateTime::FromUnixTimestamp(InExistAccount.LastActivityDate))));
	args.Add("LastActivityDays", FFormatArgumentValue(FText::AsNumber((FDateTime::UtcNow() - FDateTime::FromUnixTimestamp(InExistAccount.LastActivityDate)).GetTotalDays())));

	//RequestReAttachAccountService->SendRequestObject(Request);

	const auto title = NSLOCTEXT("IdentityComponent", "IdentityComponent.OnAttachAccountServiceExist.Title", "Linking game account");
	const auto message = FText::Format(NSLOCTEXT("IdentityComponent", "IdentityComponent.OnAttachAccountServiceExist.Message", "You already once had a game account attached to this Google account. \nDo you want to restore your previous account or do you want to connect this one? \nPlayer Name: {Name}, {Level}\nCurrency: {Money} | {Donate} \nRegistered Date: {CreatedDate} \nLast activity date: {LastActivityDate} / {LastActivityDays} days ago."), args);

	QueueBegin(SMessageBox, FName("OnAttachAccountServiceExist"), message, InExistAccount)
		SMessageBox::Get()->SetHeaderText(title);
		SMessageBox::Get()->SetContent(message);
		SMessageBox::Get()->SetButtonsText
		(
			NSLOCTEXT("IdentityComponent", "IdentityComponent.OnAttachAccountServiceExist.UseLatets", "Use latest"),
			FTableBaseStrings::GetBaseText(EBaseStrings::Cancel),
			NSLOCTEXT("IdentityComponent", "IdentityComponent.OnAttachAccountServiceExist.LinkCurrent", "Link current")
		);

		SMessageBox::Get()->OnMessageBoxButton.AddLambda([&, InExistAccount](EMessageBoxButton InButton)
		{
			if (InButton != EMessageBoxButton::Middle)
			{
				FReAttachAccountService request(InExistAccount, InButton == EMessageBoxButton::Left);
				RequestReAttachAccountService->SendRequestObject(request);
			}

			SMessageBox::Get()->ToggleWidget(false);
		});
		SMessageBox::Get()->ToggleWidget(true);
	QueueEnd

}


void UIdentityComponent::OnAttachAccountServiceFailed(const FRequestExecuteError& InErrorData)
{
	SNotifyContainer::ShowNotify("OnReAttachAccountServiceFailed", FText::Format(NSLOCTEXT("Authentication", "Authentication.:OnReAttachAccountService.Failed", "Could not connect account. \nError Message: {0}"), InErrorData.ToShortText()));

	SMessageBox::ShowErrorMessageText
	(
		TEXT("OnAttachAccountService"),
		FText::Format(FText::FromString(FString("OnAttachAccountServiceFailed | {0}")), FText::FromString(InErrorData.ToString()))
	);
}

void UIdentityComponent::OnAttachGoogleSeviceComplete(int32 LocalUserNum, bool bWasSuccessful, const FUniqueNetId& UserId, const FString& Error)
{
	if (bWasSuccessful == false)
	{
		SMessageBox::ShowErrorMessageText
		(
			TEXT("OnAttachGoogleSeviceStart"), 
			FText::Format(NSLOCTEXT("Authentication", "Authentication.OnAuthorization.Service_8", "Could not log in. Could not connect to Google Play services #8. \nError Message: {0}"), FText::FromString(Error))
		);
		SNotifyContainer::ShowNotify("OnAttachGoogleSeviceStart", FText::Format(NSLOCTEXT("Authentication", "Authentication.OnAuthorization.Service_8", "Could not log in. Could not connect to Google Play services #8. \nError Message: {0}"), FText::FromString(Error)));


		RequestDataDump->SendRequestObject(Error);

		return;
	}

	auto system = IOnlineSubsystem::Get(GOOGLE_SUBSYSTEM);
	if (system)
	{
		auto Identity = system->GetIdentityInterface();
		if (Identity.IsValid())
		{
			//=====================================================================
			FAttachAccountService Request;
			Request.Service = EAccountService::Google;

			//=====================================================================
			auto acc = Identity->GetUserAccount(UserId);
			if (acc.IsValid())
			{
				Request.Login = acc->GetDisplayName("Email");
				Request.Token = acc->GetDisplayName("Sub");
				RequestAttachAccountService->SendRequestObject(Request);
			}
			else
			{
				SNotifyContainer::ShowNotify("OnAttachGoogleSeviceStart", NSLOCTEXT("Authentication", "Authentication.OnAuthorization.Service_5", "Could not log in. Could not connect to Google Play services #5"));
			}
		}
		else
		{
			SNotifyContainer::ShowNotify("OnAttachGoogleSeviceStart", NSLOCTEXT("Authentication", "Authentication.OnAuthorization.Service_2", "Could not log in. Could not connect to Google Play services #2"));
		}
	}
	else
	{
		SNotifyContainer::ShowNotify("OnAttachGoogleSeviceStart", NSLOCTEXT("Authentication", "Authentication.OnAuthorization.Service_1", "Could not log in. Could not connect to Google Play services #1"));
	}
}

void UIdentityComponent::OnAttachGoogleSeviceStart()
{
	auto system = IOnlineSubsystem::Get(GOOGLE_SUBSYSTEM);
	if (system)
	{
		auto identity = system->GetIdentityInterface();
		if (identity.IsValid())
		{
			//===================================
			identity->AddOnLoginCompleteDelegate_Handle(0, FOnLoginCompleteDelegate::CreateUObject(this, &UIdentityComponent::OnAttachGoogleSeviceComplete));

			//===================================
			const bool bLoginSuccessfully = identity->Login(0, FOnlineAccountCredentials());
		}
		else
		{
			SNotifyContainer::ShowNotify("OnAttachGoogleSeviceStart", NSLOCTEXT("Authentication", "Authentication.OnAuthorization.Service_4", "Could not log in. Could not connect to Google Play services #4"));
		}
	}
	else
	{
		SNotifyContainer::ShowNotify("OnAttachGoogleSeviceStart", NSLOCTEXT("Authentication", "Authentication.OnAuthorization.Service_3", "Could not log in. Could not connect to Google Play services #3"));
	}
}

void UIdentityComponent::ValidateGameSingleton()
{
	//UCrashlyticsBlueprintLibrary::WriteLog("UIdentityComponent:ValidateGameSingleton:Begin");

	FShooterGameAnalytics::RecordDesignEvent("Lobby:GameSingleton::Begin");

	if(FGameSingletonExtensions::IsGameSingletonLoaded())
	{
		OnValidateGameSingletonSuccessfully();
	}
	else if(FGameSingletonExtensions::IsGameSingletonAllowReload())
	{
		//UCrashlyticsBlueprintLibrary::WriteLog("UIdentityComponent:ValidateGameSingleton:Reload");
		FShooterGameAnalytics::RecordDesignEvent("Lobby:GameSingleton::Reload");
		if (auto TM = GetTimerManager())
		{
			TM->SetTimer(ValidateGSTimerHandle, FTimerDelegate::CreateUObject(this, &UIdentityComponent::ValidateGameSingleton), 2.0f, true, 0.250f);
		}
		else
		{
			FShooterGameAnalytics::RecordDesignEvent("Lobby:GameSingleton::NoTimerManager");
		}

		FGameSingletonExtensions::ReloadGameSingleton();
	}
	else
	{
		OnValidateGameSingletonFailed();
	}

	if (auto TM = GetTimerManager())
	{
		TM->ClearTimer(ValidateGSTimerHandle);
	}
	else
	{
		FShooterGameAnalytics::RecordDesignEvent("Lobby:GameSingleton::NoTimerManager");
	}

}

void UIdentityComponent::OnValidateGameSingletonSuccessfully()
{
	//UCrashlyticsBlueprintLibrary::SetField("GameSingletonLoadAttemps.Successfully", FGameSingletonExtensions::GameSingletonLoadAttemps);
	//UCrashlyticsBlueprintLibrary::SetField("GameSingleton.Successfully", true);
	//UCrashlyticsBlueprintLibrary::WriteLog("UIdentityComponent:ValidateGameSingleton:Successfully");
	FShooterGameAnalytics::RecordDesignEvent("Lobby:GameSingleton::Successfully");
	SendRequestAndValidateVersion(CurrentGameVersion);
}

void UIdentityComponent::OnValidateGameSingletonFailed()
{
#if WITH_EDITOR == 0
	//UCrashlyticsBlueprintLibrary::SetField("GameSingletonLoadAttemps.Successfully", FGameSingletonExtensions::GameSingletonLoadAttemps);
	//UCrashlyticsBlueprintLibrary::SetField("GameSingleton.Successfully", false);
	//UCrashlyticsBlueprintLibrary::WriteLog("UIdentityComponent:ValidateGameSingleton:Failed");
	FShooterGameAnalytics::RecordDesignEvent("Lobby:GameSingleton::Failed");

	ShowRestarGameErrorMessage
	(
		FName("UIdentityComponent::OnValidateGameSingletonFailed"),
		NSLOCTEXT("Network", "Network.OnBadNetworkConnection.Title", "Problem with internet connection"),
		NSLOCTEXT("Network", "Network.OnValidateGameSingletonFailed", "Error while trying to download critical data from the server. \nTry to check your internet connection and restart the game or enter the game later.")
	);
#endif
}


void UIdentityComponent::OnExistNotAuthorized()
{
	ValidateGameSingleton();
}


void UIdentityComponent::OnCreate(const bool& InResponse)
{
	if (InResponse)
	{
		StartTimer(RequestExist);
	}
}

void UIdentityComponent::OnCreateIncorrect()
{

}

void UIdentityComponent::OnPlayerData(const FPlayerEntityInfo& InData)
{
	//=====================================
#if !UE_SERVER
	UShooterGameUserSettings* Settings = Cast<UShooterGameUserSettings>(GEngine->GameUserSettings);
	if (Settings)
	{
		Settings->SetPlayerAccountUUID(InData.PlayerId);
	}
#endif

	//======================================================
	//UCrashlyticsBlueprintLibrary::SetField("Experience.Level", InData.Experience.Level);
	//UCrashlyticsBlueprintLibrary::SetField("Experience.IsNameConfirmed", InData.Experience.IsNameConfirmed);
	//UCrashlyticsBlueprintLibrary::SetField("Experience.IsLevelUpNotified", InData.Experience.IsLevelUpNotified);
	//UCrashlyticsBlueprintLibrary::SetField("Experience.NumberOfMatches", PlayerInfo.NumberOfMatches);

	//UCrashlyticsBlueprintLibrary::WriteLog("UIdentityComponent:OnPlayerData");

	UE_LOG(LogInit, Display, TEXT("UIdentityComponent::OnRequestClientDataSuccessfully[%s][%d]"), *GetIdentityToken().Get().ToString(), static_cast<int32>(GetNetMode()));

	//======================================================
	SetPlayerInfo(InData);

#if !UE_SERVER
	//======================================================
	if (PlayerInfo.NumberOfMatches)
	{
		FShooterGameAnalytics::RecordDesignEventRange("Match:MatchesPlayed:More[Key]", PlayerInfo.NumberOfMatches, { 1000, 750, 500, 400, 300, 250, 150, 100, 75, 50, 30, 25, 20, 15, 10, 7, 5, 4, 3, 2, 1, 0 });
	}

	if (InData.Experience.IsNameConfirmed == false)
	{
		//	Показать форму ввода имени. В форму подставить текущее имя
		ChangeNameDialog("ChangeName", InData.Login);
	}
#endif

	//======================================================
	if (IsAuthorized == false)
	{
		//	Устанавливаем EpicAccountId только на клиенте
		//	для возможности определить у кого произошел crash
#if !UE_SERVER
		FShooterGameAnalytics::SetUserId(InData.PlayerId);
		//UCrashlyticsBlueprintLibrary::SetUserName(InData.Login);
		FGameSingletonExtension::SetLastLocalPlayerId(InData.PlayerId);
		//UCrashlyticsBlueprintLibrary::LogLogin("GooglePlay");
#endif


		if (auto state = GetBasicPlayerState())
		{
			IsAuthorized = true;
			state->SetPlayerName(InData.Login);
			state->SetPlayerAccountId(InData.PlayerId);

			//	Вызвываем событие OnAuthorizationComplete,
			//	Что бы выполнить загрузку друзей, отряда и т.д..
			state->OnAuthorizationCompleteDelegate.Broadcast();
		}
	}

	//======================================================
	OnRequestClientDataDelegate.Broadcast(InData);
	//SendRequestPlayerStatistic();

	//======================================================
	StartTimer(RequestClientDataUpdate);
}

void UIdentityComponent::OnPlayerDataUpdate(const FPlayerEntityInfo& InData)
{
#if !UE_SERVER

	//============================================
	if (InData.Experience.IsLevelUpNotified == false && InData.Experience.Level && InData.Experience.Level > InData.Experience.LastLevel)
	{
		//=============================================
		auto LastLevel = InData.Experience.LastLevel;
		auto CurrentLevel = InData.Experience.Level;
		auto NextLevel = InData.Experience.Level + 1;

		//=============================================
		if (CurrentLevel > 1)
		{
			FShooterGameAnalytics::AddProgressionEvent(EProgressionStatus::Complete, "Experience", FString::Printf(TEXT("LevelUp%0dTo%0d"), LastLevel, CurrentLevel));
		}
		
		//=============================================
		FShooterGameAnalytics::AddProgressionEvent(EProgressionStatus::Start, "Experience", FString::Printf(TEXT("LevelUp%0dTo%0d"), CurrentLevel, NextLevel));

		//=============================================
		OnSchedulePlayerLevelUp.Broadcast();

		//UCrashlyticsBlueprintLibrary::LogFbLevelUp(CurrentLevel);
	}

	//============================================
	//	Показать уведомление о ежедневном бонусе
	if (InData.EveryDayAward.IsNotified == false && InData.EveryDayAward.JoinCount > 0)
	{
		OnPlayerEntryReward.Broadcast(InData.EveryDayAward.JoinCount);

		//UCrashlyticsBlueprintLibrary::LogFbCustomEventEx(TEXT("Everyday_Reward"), InData.EveryDayAward.JoinCount);
	}

	//============================================
	auto NotifySounds = FGameSingletonExtension::GetDataAssets<UNotifySoundsData>();
	if (NotifySounds.IsValidIndex(0) && NotifySounds[0] && NotifySounds[0]->IsValidLowLevel())
	{
		if (InData.TakeCash(EGameCurrency::Money) != PlayerInfo.TakeCash(EGameCurrency::Money))
		{
			FSlateApplication::Get().PlaySound(NotifySounds[0]->Notify_ChangeMoney);
		}

		if (InData.TakeCash(EGameCurrency::Donate) != PlayerInfo.TakeCash(EGameCurrency::Donate))
		{
			FSlateApplication::Get().PlaySound(NotifySounds[0]->Notify_ChangeDonate);
		}
	}
#endif

	SetPlayerInfo(InData);
}

void UIdentityComponent::OnRep_PlayerInfo()
{
	if (HasLocalClientComponent())
	{
		//UGameSingleton::Get()->UpdateSavedPlayerEntityInfo(this);
	}
}

void UIdentityComponent::StartRequestIsoCountry()
{
	StartTimer(RequestIsoCountry);
}

void UIdentityComponent::SendRequestIsoCountry() const
{
	RequestIsoCountry->GetRequest();
}

void UIdentityComponent::OnRequestIsoCountry(const EIsoCountry& InIsoCountry)
{	
	//=======================================================
	UProductItemEntity::CurrentPlayerCountry = InIsoCountry;

	//=======================================================
	FShooterGameAnalytics::RecordDesignEvent("UI:Lobby:RequestIsoCountry:Successfully:Count");
	FShooterGameAnalytics::RecordDesignEvent("UI:Lobby:RequestIsoCountry:Successfully:" + GetEnumValueAsString<EIsoCountry>("EIsoCountry", UProductItemEntity::CurrentPlayerCountry));

	//=======================================================
	//UCrashlyticsBlueprintLibrary::SetField("Experience.PlayerCountryRaw", GetEnumValueAsString<EIsoCountry>("EIsoCountry", UProductItemEntity::CurrentPlayerCountry));
	//UCrashlyticsBlueprintLibrary::SetField("Experience.PlayerCountryNum", static_cast<int32>(UProductItemEntity::CurrentPlayerCountry));

	//=======================================================
	StopTimer(RequestIsoCountry);
}

void UIdentityComponent::OnRequestIsoCountryFailed(const FRequestExecuteError& InErrorData) const
{
	//=======================================================
	FShooterGameAnalytics::RecordDesignEvent("UI:Lobby:RequestIsoCountry:Failed:Status" + InErrorData.GetStatusCode());
	//UCrashlyticsBlueprintLibrary::SetField("Experience.PlayerCountryRaw", "Failed");
	//UCrashlyticsBlueprintLibrary::SetField("Experience.PlayerCountryNum", -1);
}


void UIdentityComponent::SetPlayerInfo(const FPlayerEntityInfo& info)
{
	PlayerInfo = info;
	//PlayerInfo.Login += " | " + //UCrashlyticsBlueprintLibrary::GetUserCountry();
	OnRep_PlayerInfo();
}

void UIdentityComponent::CreateGameWidgets()
{
	//======================================================
	//UCrashlyticsBlueprintLibrary::WriteLog("UIdentityComponent:CreateGameWidgets");

#if USE_LOKA_SERVICES
	SAssignNew(Slate_AuthForm, SAuthForm)
	.OnAuthentication_UObject(this, &UIdentityComponent::SendRequestAuthorization);
	
	if (auto TargetViewport = UShooterGameViewportClient::Get())
	{
		TargetViewport->AddUsableViewportWidgetContent_AlwaysVisible(Slate_AuthForm.ToSharedRef(), 150);
	}	
#endif
}


void UIdentityComponent::OnToggleAuthFormAttemp(const bool InToggle)
{
#if USE_LOKA_SERVICES
	UE_LOG(LogInit, Display, TEXT("[UIdentityComponent::ToggleAuthForm_Implementation][IsValid: %d][InToggle: %d][1]"), (int)Slate_AuthForm.IsValid(), (int)InToggle);
	if (Slate_AuthForm.IsValid())
	{
		Slate_AuthForm->ToggleWidget(InToggle);
	}

	if (ToggleAuthFormAttempCount > 5 || Slate_AuthForm.IsValid())
	{
		if (auto TimerManager = GetTimerManager())
		{
			TimerManager->ClearTimer(ToggleAuthFormHandle);
		}
	}
	ToggleAuthFormAttempCount++;
#endif
}

void UIdentityComponent::ToggleAuthForm_Implementation(const bool InToggle)
{
#if USE_LOKA_SERVICES
	FTimerDelegate glg;
	glg.BindLambda([&, InToggle]()
	{
		OnToggleAuthFormAttemp(InToggle);
	});

	UE_LOG(LogInit, Display, TEXT("[UIdentityComponent::ToggleAuthForm_Implementation][IsValid: %d][InToggle: %d][0]"), (int)Slate_AuthForm.IsValid(), (int)InToggle);

	if (auto TimerManager = GetTimerManager())
	{
		TimerManager->SetTimer(ToggleAuthFormHandle, glg, 0.250f, true, 0.250f);
	}
	else
	{
		UE_LOG(LogInit, Error, TEXT("[UIdentityComponent::ToggleAuthForm_Implementation][TimerManager was nullptr]"));
	}
#endif
}

void UIdentityComponent::ShowErrorOnAuthForm_Implementation(const ELoginFromRemoteStatus InCode, const FString& InAdditionalMessage)
{
	FText ErrorMessage;

	switch (InCode)
	{
	case ELoginFromRemoteStatus::LoginNotFound:
		ErrorMessage = NSLOCTEXT("Authentication", "Authentication.OnAuthorization.LoginNotFound", "Login not found!"); break;

	case ELoginFromRemoteStatus::LoginWasExist:
		ErrorMessage = NSLOCTEXT("Authentication", "Authentication.OnAuthorization.LoginWasExist", "Login was exist!"); break;

	case ELoginFromRemoteStatus::InvalidPassword:
		ErrorMessage = NSLOCTEXT("Authentication", "Authentication.OnAuthorization.InvalidPassword", "Invalid password"); break;

	case ELoginFromRemoteStatus::BadRequest:
		ErrorMessage = NSLOCTEXT("Authentication", "Authentication.OnAuthorization.BadRequest", "Bad request"); break;

	case ELoginFromRemoteStatus::IsNotAllowed:
		ErrorMessage = NSLOCTEXT("Authentication", "Authentication.OnAuthorization.IsNotAllowed", "Authentication is not allowed"); break;

	case ELoginFromRemoteStatus::IsLockedOut:
		ErrorMessage = NSLOCTEXT("Authentication", "Authentication.OnAuthorization.IsLockedOut", "Account is locked out"); break;

	case ELoginFromRemoteStatus::RequiresTwoFactor:
		ErrorMessage = NSLOCTEXT("Authentication", "Authentication.OnAuthorization.RequiresTwoFactor", "RequiresTwoFactor, not impl"); break;

	case ELoginFromRemoteStatus::SessionExist:
		ErrorMessage = NSLOCTEXT("Authentication", "Authentication.OnAuthorization.SessionExist", "Player was logged"); break;

	default:
		ErrorMessage = FText::Format(NSLOCTEXT("Authentication", "Authentication.OnAuthorization.Unknown", "Unknown: {0}"), static_cast<int32>(InCode)); break;
}
	

#if USE_GOOGLE_SERVICES
	ShowRestarGameErrorMessage
	(
		FName("UIdentityComponent::ShowErrorOnAuthForm"),
		NSLOCTEXT("Authentication", "Authentication.OnAuthorization.Google.Title", "Failed Google Play Login"),
		FText::Format(NSLOCTEXT("Authentication", "Authentication.OnAuthorization.Google.Message", "Could not sign in with Google Play services\nAdditional Message: \n{0}\nErrorMessage: {1}\nStatus Code: {2}"), FText::FromString(InAdditionalMessage), ErrorMessage, FText::AsNumber(static_cast<int32>(InCode), &FNumberFormattingOptions::DefaultNoGrouping()))
	);
#else
	if (Slate_AuthForm.IsValid())
	{
		Slate_AuthForm->OnErrorMessage(ErrorMessage);
	}
#endif
}

void UIdentityComponent::OnRep_Simulation()
{
	UE_LOG(LogInit, Display, TEXT("[UIdentityComponent::OnRep_Simulation][bIsSimulation: %d]"), (int)bIsSimulation);

	if (bIsSimulation)
	{
		ToggleAuthForm(false);
	}
}

void UIdentityComponent::ShowChangeNameDialog()
{
	ChangeNameDialog("ChangeName", PlayerInfo.Login);
}

void UIdentityComponent::ChangeNameDialog(const FString& InTag, const FString& InInitialName, const FText& InErrorText)
{
	//==========================
	//UCrashlyticsBlueprintLibrary::LogSignUp("GooglePlay");

	//==========================
	//	Не показывает виджет с однотипной ошибкой
	const FString TagString = FString::Printf(TEXT("%s_%d"), *InTag, FMath::Rand());
	const FName TagName = FName(*TagString);

	//==========================
	QueueBegin(SMessageBox, TagName, InErrorText, IsNameConfirmed= PlayerInfo.Experience.IsNameConfirmed, InitialName = FText::FromString(InInitialName), ChangeNameTickets = PlayerInfo.Experience.ChangeNameTickets, ErrorText = InErrorText)
		SMessageBox::Get()->SetHeaderText(NSLOCTEXT("IdentityComponent", "IdentityComponent.ChangeName.Title", "Change Name"));
		SMessageBox::Get()->SetContent(SNew(SChangeNameContainer).ChangeNameError(ErrorText).PlayerName(InitialName).PlayerNameTickets(ChangeNameTickets).OnChangeName_UObject(this, &UIdentityComponent::SendRequestChangeName));
		SMessageBox::Get()->SetEnableClose(IsNameConfirmed);
		SMessageBox::Get()->SetButtonsText();
		SMessageBox::Get()->ToggleWidget(true);
	QueueEnd
}

void UIdentityComponent::GetLifetimeReplicatedProps(TArray<FLifetimeProperty>& OutLifetimeProps) const
{
	Super::GetLifetimeReplicatedProps(OutLifetimeProps);

	//DOREPLIFETIME(UIdentityComponent, PlayerInfo);
	DOREPLIFETIME_CONDITION(UIdentityComponent, PlayerInfo, COND_OwnerOnly);
	DOREPLIFETIME_CONDITION(UIdentityComponent, bIsSimulation, COND_OwnerOnly);
}
