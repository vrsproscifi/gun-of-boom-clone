﻿#pragma once

#include "BasicPlayerComponent.h"
#include "SquadComponent/SqaudInformation.h"
#include "SquadComponent/SquadInviteInformation.h"
#include "SquadComponent.generated.h"


class UOperationRequest;

DECLARE_LOG_CATEGORY_EXTERN(LogSquadComponent, Log, All);
DECLARE_MULTICAST_DELEGATE_OneParam(FOnSquadUpdate, const FSqaudInformation&);
DECLARE_DYNAMIC_MULTICAST_DELEGATE(FOnSquadEvent);

UCLASS(Transient)
class USquadComponent : public UBasicPlayerComponent
{
	GENERATED_BODY()

public:

	USquadComponent();
	FOnSquadUpdate OnSquadUpdate;

protected:
	UFUNCTION()
	void OnBadNetworkConnection(const FRequestExecuteError& InErrorData);


	/*! Событие вызываемое при успешной авторизации*/
	void OnAuthorizationComplete() override;

	FTimerHandle RequestStartSearchTimerHandle;

	UPROPERTY(Transient, VisibleInstanceOnly) UOperationRequest* RequestSquadInformation;
	UPROPERTY(Transient, VisibleInstanceOnly) UOperationRequest* RequestStartSearch;
	UPROPERTY(Transient, VisibleInstanceOnly) UOperationRequest* RequestStopSearch;

	void SendRequestStartSearch(const FSessionMatchOptions& InOptions);
	void SendRequestStartSearchExecutor(FSessionMatchOptions InOptions);

public:
	/*! Событие для смены статуса поиска боя
	* Если SqaudInformation.Status == ESearchSessionStatus::None, то отправляется запрос на поиск боя и блокируется кнопка
	* Если SqaudInformation.AllowCancelSearch(), то отправляется запрос на отмену боя
	* Иначе, метод ничего не делает
	*/
	UFUNCTION() void SendRequestToggleSearch(const FSessionMatchOptions& InOptions, const bool& InIsStartSearch);

	UFUNCTION(Reliable, Server, WithValidation, BlueprintCallable, Category = Squad)
	void SendRequestSquadInformation();
	bool SendRequestSquadInformation_Validate() { return true; }

protected:

	UFUNCTION() void OnRep_SqaudInformation();

	/*! Предыдущее состояние отряда*/
	UPROPERTY(Replicated) FSqaudInformation LastSqaudInformation;

	/*! Текущее состояние отряда*/
	UPROPERTY(ReplicatedUsing = OnRep_SqaudInformation) FSqaudInformation SqaudInformation;

	UFUNCTION() void OnSquadInformation(const FSqaudInformation& InInformation);
	UFUNCTION() void OnStartSearch();
	UFUNCTION() void OnStopSearch();

	UFUNCTION(Reliable, Client) void OnRequestStartSearch_Failed(const FRequestExecuteError&error);
	UFUNCTION(Reliable, Client) void OnRequestStartSearch_ErrorOnDataBase();
	UFUNCTION(Reliable, Client) void OnRequestStartSearch_MembersNotReady();
	UFUNCTION(Reliable, Client) void OnRequestStartSearch_ServerHasInvalidVersion();
	UFUNCTION(Reliable, Client) void OnRequestStartSearch_ServerHasUpdating();
	UFUNCTION(Reliable, Client) void OnRequestStartSearch_ServerHasDeleted();


	UFUNCTION(Reliable, Client) void OnClientMatchMakingProgress();

	UFUNCTION(Reliable, Client) void OnClientStartSearch();
	UFUNCTION(Reliable, Client) void OnClientStopSearch();

public:

	bool IsAllowTravel() const;
	virtual bool IsAllowServerQueries() const override;

	UFUNCTION(BlueprintCallable, BlueprintPure, Category = Squad)
	FSqaudInformation GetSqaudInformation() const
	{
		return SqaudInformation;
	}
		
	UPROPERTY(BlueprintAssignable, Category = MatchMaking) FOnSquadEvent EventOnStartSearch;
	UPROPERTY(BlueprintAssignable, Category = MatchMaking) FOnSquadEvent EventOnStopSearch;
	UPROPERTY(BlueprintAssignable, Category = MatchMaking) FOnSquadEvent EventOnSearchFound;
};