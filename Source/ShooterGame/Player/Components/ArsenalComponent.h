#pragma once
//==========================================
#include "BasicPlayerComponent.h"

//==========================================
#include "ArsenalComponent/PlayerInventoryItemModel.h"
#include "ArsenalComponent/PlayerInventoryItemUnlock.h"
#include "ArsenalComponent/PlayerProfileItemEquip.h"
#include "ArsenalComponent/PlayerProfileModel.h"
#include "ArsenalComponent/MarketDeliveryRequest.h"
#include "ArsenalComponent/UpgradeItemResponse.h"
#include "ArsenalComponent/PlayerCaseModel.h"
#include "ArsenalComponent/PlayerDiscountModel.h"

//=======================================
#include "Public/Interfaces/OnlineStoreInterface.h"

//==========================================
#include "ArsenalComponent.generated.h"

//==========================================
class UPlayerDiscountEntity;

class UPlayerCaseEntity;
struct FDropedCaseModel;
//==========================================
class UBasicPlayerItem;
class UBasicItemEntity;
class UBaseProductItemEntity;

//==========================================
struct FPlayerInventoryItemUnlocked;
struct FGameItemCost;


//==========================================
DECLARE_MULTICAST_DELEGATE_OneParam(FOnInventorySignature, const TArray<UBasicPlayerItem*>);
DECLARE_MULTICAST_DELEGATE_OneParam(FOnPlayerNotEnoughMoney, const FGameItemCost&);
DECLARE_DYNAMIC_MULTICAST_DELEGATE_OneParam(FOnPlayerChestsSignature, const TArray<UPlayerCaseEntity*>&, InChests);

//==========================================
DECLARE_DYNAMIC_MULTICAST_DELEGATE_OneParam(FOnProfileData, const TArray<FPlayerProfileModel>&, InResponse);
DECLARE_DYNAMIC_MULTICAST_DELEGATE(FOnProfileDataUpdated);

DECLARE_MULTICAST_DELEGATE_OneParam(FOnItemUnlocked, UBasicItemEntity*);

DECLARE_LOG_CATEGORY_EXTERN(LogArsenalComponent, All, All);

UCLASS(Transient)
class UArsenalComponent : public UBasicPlayerComponent
{
#if WITH_EDITOR
	friend class AShooterPlayerState;
#endif

	GENERATED_BODY()
	
public:
	FOnPlayerNotEnoughMoney OnPlayerNotEnoughMoney;


	//=====================================================================
	//	PlayerCaseEntity
	//=====================================================================
#pragma region PlayerCaseEntity
private:
	UPROPERTY(VisibleInstanceOnly)
	TArray<UPlayerCaseEntity*> Cases;

public:
	virtual void OnPlayRewardVideoSuccess(const FPlayRewardedVideoResponse& InResponse) override;

	UFUNCTION()
	UPlayerCaseEntity* AddOrUpdateCase(const FPlayerCaseModel& InCase);

	UFUNCTION()
	const TArray<UPlayerCaseEntity*>& GetCases() const
	{
		return Cases;
	}

	template <typename Predicate>
	TArray<UPlayerCaseEntity*> GetCasesByPredicate(Predicate Pred) const
	{
		return GetCases().FilterByPredicate(Pred);
	}

	UFUNCTION(BlueprintCallable, BlueprintPure, Category = Cases)
	const UPlayerCaseEntity* GetCaseById(const FGuid& InCaseId) const;
	UPlayerCaseEntity* GetCaseById(const FGuid& InCaseId);

	UFUNCTION(BlueprintCallable, BlueprintPure, Category = Cases)
	const UPlayerCaseEntity* GetCaseByModelId(const int32& InCaseModelId) const;
	UPlayerCaseEntity* GetCaseByModelId(const int32& InCaseModelId);

	UFUNCTION(BlueprintCallable, BlueprintPure, Category = Cases)
	const UPlayerCaseEntity* GetCaseByModelOrId(const FGuid& InCaseId, const int32& InCaseModelId) const;
	UPlayerCaseEntity* GetCaseByModelOrId(const FGuid& InCaseId, const int32& InCaseModelId);


#pragma endregion

	//=====================================================================
	//	PlayerDiscountEntity
	//=====================================================================
#pragma region PlayerDiscountEntity
private:
	UPROPERTY(VisibleInstanceOnly)
		TArray<UPlayerDiscountEntity*> DiscountCoupones;

public:
	UFUNCTION()
	UPlayerDiscountEntity* AddOrUpdateDiscountCoupone(const FPlayerDiscountModel& InDiscountCoupone);

	UFUNCTION()
	bool RemoveDiscountCoupone(const FPlayerDiscountModel& InDiscountCoupone);


	UFUNCTION()
	const TArray<UPlayerDiscountEntity*>& GetDiscountCoupones() const;

	UFUNCTION(BlueprintCallable, BlueprintPure, Category = Cases)
	const UPlayerDiscountEntity* GetDiscountCouponeById(const FGuid& InDiscountCouponeId) const;

	UPlayerDiscountEntity* GetDiscountCouponeById(const FGuid& InDiscountCouponeId);

#pragma endregion

public:
	UFUNCTION()
	void OnBadNetworkConnection(const FRequestExecuteError& InErrorData);

	UFUNCTION()
	void SendRequestUsePlayerItem(const UBasicPlayerItem* inItem, const int32& InAmount) const;


	UFUNCTION() void OnMakeInAppPurchase(const FString& inProductId);

public:
	UFUNCTION() void OnMarketRequestBuy(const UBaseProductItemEntity* inEntity);
	UFUNCTION() void OnMarketRequestUpgrade(const UBasicPlayerItem* inItem, const int32& InLevel);
	UFUNCTION() void OnArsenalRequestBuy(const UBasicPlayerItem* inItem);
	UFUNCTION() void OnArsenalRequestBuyItem(const UBasicItemEntity* InEntity) const;

	UFUNCTION() void OnArsenalRequestEquip(const UBasicPlayerItem* inItem);

public:
	UFUNCTION()	void SendRequestPlayerCases() const;
	UFUNCTION()	void StartRequestPlayerCases();
	UFUNCTION()	void SendRequestOpenPlayerCase(const FGuid& InCaseId) const;
	UFUNCTION() void OnOpenDayliRewardClicked(const int32& InRewardDay) const;


private:
	UFUNCTION() void OnRequestPlayerCases(const TArray<struct FPlayerCaseModel>& InCases);
	UFUNCTION() void OnRequestOpenPlayerCase(const FDropedCaseModel& InDropedCase);
	UFUNCTION() void OnRequestOpenPlayerCase_Failed(const FRequestExecuteError& InError);
	UFUNCTION() void OnRequestOpenPlayerCase_NotFoundEntity();
	UFUNCTION() void OnRequestOpenPlayerCase_NotFoundCase();
	UFUNCTION() void OnRequestOpenPlayerCase_NotAllowUse();


public:	
	UArsenalComponent();
	virtual void CreateGameWidgets() override;

	FOnItemUnlocked OnItemUnlocked;

	UPROPERTY(BlueprintAssignable, Category = Profiles)				FOnProfileDataUpdated		OnRequestPlayerDataUpdate;
	UPROPERTY(BlueprintAssignable, Category = Profiles)				FOnProfileData				OnProfileDataEvent;
	UPROPERTY(BlueprintAssignable, Category = Profiles)				FOnProfileDataUpdated		OnProfileDataUpdated;
	UPROPERTY(BlueprintAssignable, Category = Improve)				FOnProfileDataUpdated		OnImproveItem;
	UPROPERTY(BlueprintAssignable, Category = Chests)				FOnPlayerChestsSignature	OnChestsUpdate;

	UPROPERTY(VisibleInstanceOnly, Replicated, BlueprintReadOnly, Category = Profile)	TArray<FPlayerProfileModel> ProfilesData;
	
	//============================================
	UFUNCTION(Reliable, Server, WithValidation) void SendRequestProfilesData();
	UFUNCTION(Reliable, Server, WithValidation)	void SendRequestItemEquip(const FPlayerProfileItemEquip& InData);

	UFUNCTION(BlueprintCallable, BlueprintPure, Category = Profile)
	TArray<FPlayerProfileModel> GetProfilesData() const
	{
		return ProfilesData;
	}

	UFUNCTION(BlueprintCallable, BlueprintPure, Category = Profile)
	FPlayerProfileModel GetProfileData(const int32 InTargetProfile) const;
	const FPlayerProfileModel* GetProfileDataPtr(const int32 InTargetProfile) const;
	FPlayerProfileModel * GetProfileDataPtr(const int32 InTargetProfile);

	UFUNCTION(BlueprintCallable, BlueprintPure, Category = Profile)
	UBasicPlayerItem* GetPlayerItemInProfileBySlot(const int32& InTargetProfile, const EGameItemType& InTargetSlot);


	UFUNCTION(BlueprintCallable, BlueprintPure, Category = Profile)
	bool ReplaceWeaponInProfile(const int32& InTargetProfile, const int32& InTargetWeaponLevel);

	UFUNCTION(BlueprintCallable, BlueprintPure, Category = Profile)
	bool UpgradeDefaultItemInProfile(const int32& InTargetProfile);

	UFUNCTION(BlueprintCallable, BlueprintPure, Category = Profile)
	bool AddIfNotExistItemInSlot(const int32& InTargetProfile, const EGameItemType& InTargetSlot, const int32& InTargetModelId);

	UFUNCTION(BlueprintCallable, BlueprintPure, Category = Profile)
		bool IsTargetWeaponLevel(const int32& InTargetProfile, const int32& InTargetWeaponLevel) const;

	UFUNCTION(BlueprintCallable, BlueprintPure, Category = Profile)
	bool IsMaximumLevelReached(const int32& InTargetProfile) const;

	UFUNCTION(BlueprintCallable, BlueprintPure, Category = Profile)
	bool IsExistAllSlotsInProfile(const int32& InTargetProfile, const TArray<EGameItemType>& InTargetSlots) const;

	UFUNCTION(BlueprintCallable, BlueprintPure, Category = Profile)
	bool GetDefaultItem(const int32& InTargetProfile, FPlayerProfileItemModel& OutDefaultProfileItem, UBasicPlayerItem*& OutDefaultPlayerItem) const;

public:
	UFUNCTION() void SendMarketExchangeRequest(const FString& inProductId) const;

private:
	UFUNCTION() void ResetPreviewActiveDiscountCoupones();
	UFUNCTION() void UpdateActiveDiscountCoupones(const TArray<FPlayerDiscountModel>& InDiscountCoupones);
	UFUNCTION() void UpdateCurrentDiscountCoupone(const TArray<FPlayerDiscountModel>& InDiscountCoupones);
	UFUNCTION() void UpdateCurrentDiscountCouponeIfExist(const TArray<FPlayerDiscountModel>& InDiscountCoupones);


	public:
	UFUNCTION() void SendRequestBonusCase(const FGuid& InPlayerCaseId);
	UFUNCTION() void SendRequestPlayerDiscounts();
	UFUNCTION() void StartRequestPlayerDiscounts();
	UFUNCTION() void OnRequestPlayerDiscounts(const TArray<FPlayerDiscountModel>& InDiscountCoupones);
	UFUNCTION() void OnRequestPlayerDiscounts_Failed(const FRequestExecuteError& InError);

	UPROPERTY(Transient, VisibleInstanceOnly) FPlayerDiscountModel CurrentDiscountCoupone;
	UPROPERTY(Transient, VisibleInstanceOnly) TArray<FPlayerDiscountModel> PreviewDiscountCoupones;

protected:
	//============================================
	UPROPERTY(Transient, VisibleInstanceOnly) UOperationRequest* RequestBonusCase;


	UPROPERTY(Transient, VisibleInstanceOnly) UOperationRequest* RequestPlayerCases;
	UPROPERTY(Transient, VisibleInstanceOnly) UOperationRequest* RequestPlayerCaseOpen;
	UPROPERTY(Transient, VisibleInstanceOnly) UOperationRequest* RequestPlayerDiscounts;

	UPROPERTY(Transient, VisibleInstanceOnly) UOperationRequest* RequestUsePlayerItem;
	UPROPERTY(Transient, VisibleInstanceOnly) UOperationRequest* RequestProfilesData;
	UPROPERTY(Transient, VisibleInstanceOnly) UOperationRequest* RequestItemUpgrade;
	UPROPERTY(Transient, VisibleInstanceOnly) UOperationRequest* RequestItemEquip;
	UPROPERTY(Transient, VisibleInstanceOnly) UOperationRequest* RequestItemUnlock;
	UPROPERTY(Transient, VisibleInstanceOnly) UOperationRequest* MarketDelivery;
	UPROPERTY(Transient, VisibleInstanceOnly) UOperationRequest* MarketExchange;

	UFUNCTION() void OnSendRequestUsePlayerItem(const FPlayerInventoryItemModel& InResponse) const;
	UFUNCTION() void OnSendRequestUsePlayerItem_Failed(const FRequestExecuteError& error) const;


	UFUNCTION() void SendMarketDeliveryRequest(const FMarketDeliveryRequest& request);

	UFUNCTION() void OnMarketDelivery(const FMarketDeliveryResponse& response);
	UFUNCTION() void OnMarketDelivery_Failed(const FRequestExecuteError& error);

	UFUNCTION() void OnMarketExchange(const FMarketDeliveryResponse& response);
	UFUNCTION() void OnMarketExchange_Failed(const FRequestExecuteError& error);
	
	void OnMarketExchange_NotEnouthMoney(const FGameItemCost& InEnouthMoney) const;

	UFUNCTION()	void OnProfilesUpdateHandle(const TArray<FPlayerProfileModel>& InData);


	UFUNCTION() void OnItemEquip(const FPlayerProfileItemEquip& InResponse);
	UFUNCTION() void OnItemEquip_Failed(const FRequestExecuteError& error);
	UFUNCTION() void OnItemEquip_ProfileNotFound();

	UFUNCTION() void OnItemUpgrade(const FUpgradeItemResponse& InResponse);
	UFUNCTION() void OnItemUpgrade_NotFound();
	UFUNCTION() void OnItemUpgrade_NotEnouthMoney(const FGuid& InPlayerItemId) const;
	UFUNCTION() void OnItemUpgrade_MaximumLevelReached();
	UFUNCTION() void OnItemUpgrade_RequestedInvalidLevel();

	UFUNCTION() void OnItemUpgrade_Failed(const FRequestExecuteError& error);

public:
	UFUNCTION() void OnProfiles(const TArray<FPlayerProfileModel>& InData);



	//=============================================================
#pragma region RequestLootData
public:
	FOnInventorySignature OnInventoryUpdate;

	UFUNCTION(Reliable, Server, WithValidation)
	void SendRequestLootData();

protected:
	UPROPERTY(Transient, VisibleInstanceOnly) UOperationRequest*				RequestLootData;
	UFUNCTION()									
	void OnLootData(const TArray<FPlayerInventoryItemModel>& InData);

public:
	UFUNCTION()
	void OnLoadPlayerInventorySuccessfully(const TArray<FPlayerInventoryItemModel>& response);

	UFUNCTION()
	void OnLoadPlayerInventoryFailed(const FRequestExecuteError& error) const;
#pragma endregion


	void OnUnlockItemSuccessfully(const FPlayerInventoryItemUnlocked& response);
	void OnUnlockItem_NotEnouthMoney(const int32& InModelId) const;

	//=============================================================
#pragma region Items
private:
	UFUNCTION()									void OnRep_Items();
	UPROPERTY(VisibleInstanceOnly, ReplicatedUsing = OnRep_Items)	TArray<UBasicPlayerItem*> Items;
	UPROPERTY(VisibleInstanceOnly )									TMap<FGuid, UBasicPlayerItem*> ItemMap;

	UBasicPlayerItem* CreateAndInsertItem(const FPlayerInventoryItemModel& item);

	friend class AShooterCharacter;

	const TArray<UBasicPlayerItem*>& GetRawItems() const
	{
		return Items;
	}

	TArray<UBasicPlayerItem*>& GetRawItems()
	{
		return Items;
	}

public:
#pragma region FindItem
	UBasicPlayerItem* FindItem(const int32& InModelId) const;
	UBasicPlayerItem* FindItem(const FGuid& InIndex) const;
	UBasicPlayerItem* FindItem(const FLokaGuid& InIndex) const;

	UBasicPlayerItem* FindItem(const FGuid& InIndex, const int32& InModelId) const;
	UBasicPlayerItem* FindItem(const FString& InIndex) const;

	template<class T = UBasicPlayerItem>
	T* FindItem(const int32& InModelId) const
	{
		return Cast<T>(FindItem(InModelId));
	}

	template<class T = UBasicPlayerItem>
	T* FindItem(const FGuid& InIndex) const
	{
		return Cast<T>(FindItem(InIndex));
	}

	template<class T = UBasicPlayerItem>
	T* FindItem(const FString& InIndex) const
	{
		return Cast<T>(FindItem(InIndex));
	}

	template<typename Predicate>
	UBasicPlayerItem* FindItem(Predicate Pred) const
	{
		return GetRawItems().FindByPredicate(Pred);
	}

	template<class T = UBasicPlayerItem, typename Predicate>
	T* FindItem(Predicate Pred) const
	{
		return Cast<T>(FindItem(Pred));
	}

#pragma endregion

	static FString GetEntityName(const UBasicPlayerItem* InItem);
	static FString GetEntityName(const UBasicItemEntity* InItem);

#pragma endregion

	virtual void OnAuthorizationComplete() override;

public:

	void RequestChangeDefaultWeapon(class UWeaponPlayerItem* InTarget);

protected:

	UPROPERTY(Transient) float CachedHealth;
	UPROPERTY(Transient) float CachedArmour;
	UPROPERTY(Transient) int32 CachedAids;
	UPROPERTY(Transient) int32 CashedGrenades;

public:

	FORCEINLINE float GetCachedHealth() const { return CachedHealth; }
	FORCEINLINE float GetCachedArmour() const { return CachedArmour; }
	FORCEINLINE int32 GetCachedAids() const { return CachedAids; }
	FORCEINLINE int32 GetCachedGrenades() const { return CashedGrenades; }
};