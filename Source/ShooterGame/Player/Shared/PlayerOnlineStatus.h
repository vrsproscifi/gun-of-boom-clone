#pragma once
#include "PlayerOnlineStatus.generated.h"


UENUM(meta = (Bitflags))
enum class EPlayerFriendStatus
{
	None,

	Offline = 1,
	Online = 2,
	SearchGame = 4,
	PlayGame = 8,

	Ready = 16,
	Leader = 32,
};


//ENUM_CLASS_FLAGS(PlayerFriendStatus::Type)

UENUM()
namespace ActionPlayerRequestState
{
	enum Type
	{
		Waiting,
		Confirmed,
		ConfirmedWaiting,
		Rejected,
		Blocked,
		End
	};
}