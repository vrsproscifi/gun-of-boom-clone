﻿//=============================================
#include "ShooterGame.h"
#include "OnlinePlayerState.h"

//=============================================
#include "Components/IdentityComponent.h"
#include "Components/ArsenalComponent.h"
#include "Components/SquadComponent.h"
#include "Components/ProgressComponent.h"
#include "Components/TutorialPlayerComponent.h"
#include "GameInstance/ShooterGameViewportClient.h"
#include "BasicPlayerItem.h"
#include "CharacterItemEntity.h"
#include "BasicPlayerCharacter.h"
#include "GameInstance/ShooterGameUserSettings.h"
#include "Settings/SSettingsManager.h"
#include "Notify/SMessageBox.h"
#include "AndroidPermissionFunctionLibrary.h"
#include "AndroidPermissionCallbackProxy.h"
#include "Localization/TableBaseStrings.h"
//#include "CrashlyticsBlueprintLibrary.h"
#include "ProductItemEntity.h"
#include "GooglePlayHelper/IdentityService.h"

AOnlinePlayerState::AOnlinePlayerState(const FObjectInitializer& ObjectInitializer)
{
	IdentityComponent = CreateDefaultSubobject<UIdentityComponent>(TEXT("IdentityComponent"));
	SquadComponent = CreateDefaultSubobject<USquadComponent>(TEXT("SquadComponent"));
	InventoryComponent = CreateDefaultSubobject<UArsenalComponent>(TEXT("InventoryComponent"));
	ProgressComponent = CreateDefaultSubobject<UProgressComponent>(TEXT("ProgressComponent"));
	TutorialComponent = CreateDefaultSubobject<UTutorialPlayerComponent>(TEXT("TutorialComponent"));

	//========================
	if (FApp::IsGame())
	{
		IdentityComponent->OnAuthorizationEvent.AddDynamic(this, &AOnlinePlayerState::OnOwnerAuthorization);
		
		InventoryComponent->OnRequestPlayerDataUpdate.AddDynamic(this, &AOnlinePlayerState::SendRequestPlayerDataUpdate);
		InventoryComponent->OnInventoryUpdate.AddUObject(this, &AOnlinePlayerState::OnInventoryUpdated, true);
		InventoryComponent->OnProfileDataUpdated.AddDynamic(this, &AOnlinePlayerState::OnFullyLoadedData);
	}

	NetUpdateFrequency = 2.0f;
	SetReplicates(true);
}

void AOnlinePlayerState::SendRequestPlayerDataUpdate()
{
	//	Насильно обновляем информацию об игроке
	if (auto identity = GetIdentityComponent())
	{
		identity->SendRequestClientDataUpdate();
	}
}

void AOnlinePlayerState::OnInventoryUpdated(const TArray<UBasicPlayerItem*> InData, const bool IsInternal)
{
	if (HasAuthority() && InventoryComponent->GetProfileDataPtr(0) == nullptr && IsInternal)
	{
		InventoryComponent->SendRequestProfilesData();
	}
}

void AOnlinePlayerState::OnOwnerAuthorization(const FGuid& InOwnerToken)
{

}

void AOnlinePlayerState::OnFullyLoadedData()
{
	OnInitializeCharacter();
}

//=================================================================
#pragma region Components
UIdentityComponent* AOnlinePlayerState::GetIdentityComponent() const
{
	if (IsValidObject(this))
	{
		return GetValidObject(IdentityComponent);
	}
	return nullptr;
}

UArsenalComponent* AOnlinePlayerState::GetInventoryComponent() const
{
	if (IsValidObject(this))
	{
		return GetValidObject(InventoryComponent);
	}
	return nullptr;
}

USquadComponent* AOnlinePlayerState::GetSquadComponent() const
{
	if (IsValidObject(this))
	{
		return GetValidObject(SquadComponent);
	}
	return nullptr;
}

UProgressComponent* AOnlinePlayerState::GetProgressComponent() const
{
	if (IsValidObject(this))
	{
		return GetValidObject(ProgressComponent);
	}
	return nullptr;
}

UTutorialPlayerComponent* AOnlinePlayerState::GetTutorialComponent() const
{
	if (IsValidObject(this))
	{
		return GetValidObject(TutorialComponent);
	}
	return nullptr;
}

bool AOnlinePlayerState::HasPremiumAccount() const
{
	if (auto Identity = GetIdentityComponent())
	{
		return Identity->HasPremiumAccount();
	}
	return false;
}

#pragma endregion

void AOnlinePlayerState::EndPlay(const EEndPlayReason::Type EndPlayReason)
{
	auto TargetViewport = UShooterGameViewportClient::Get();
	if (TargetViewport && HasLocalClientState())
	{
		TargetViewport->RemoveAllViewportWidgetsAlt();
	}

	Super::EndPlay(EndPlayReason);
}


void AOnlinePlayerState::OnInitializeCharacter()
{
	if (HasAuthority())
	{
		auto TargetProfile = GetInventoryComponent()->GetProfileDataPtr(0);
		auto MyCharacter = GetBaseCharacter();

		if (MyCharacter && TargetProfile)
		{
			// Сначала нужно задать персонажа, гоняем все предметы т.к. отсутствует нужная переменная.
			for (auto Item : TargetProfile->Items)
			{
				if (auto FoundItem = GetInventoryComponent()->FindItem(Item.ItemId))
				{
					if (FoundItem->GetEntity<UCharacterItemEntity>())
					{
						MyCharacter->InitializeInstance(FoundItem);
						break;
					}
				}
			}

			//============================================
			FGuid DefaultItemId = TargetProfile->DefaultItemId;
			if(DefaultItemId.IsValid() == false)
			{
				const auto DefaultActiveItemModel = TargetProfile->GetDefaultItem();
				if (DefaultActiveItemModel)
				{
					DefaultItemId = DefaultActiveItemModel->EntityId;
				}
			}

			//============================================
			// Теперь задаём всё остальное к персонажу
			for (auto Item : TargetProfile->Items)
			{
				if (auto FoundItem = GetInventoryComponent()->FindItem(Item.ItemId))
				{
					if (!FoundItem->GetEntity<UCharacterItemEntity>())
					{
						MyCharacter->InitializeItem(FoundItem, DefaultItemId == Item.EntityId);
					}
				}
			}
		}
	}
}

void AOnlinePlayerState::UsePlayerItem(const FGuid& InPlayerItemId, const int32& InAmount)
{

}

void AOnlinePlayerState::OnCreateUserInterface()
{
	Super::OnCreateUserInterface();

	if (auto TargetViewport = UShooterGameViewportClient::Get())
	{
		SAssignNew(Slate_SettingsManager, SSettingsManager).IsCentered(false);

		TargetViewport->AddUsableViewportWidgetContent_AlwaysVisible(Slate_SettingsManager.ToSharedRef(), 10);
	}
}


bool AOnlinePlayerState::CheckAndRequestAndroidPermission(const FString& InPermission, const FText& InMessage)
{
#if USE_GOOGLE_SERVICES && PLATFORM_ANDROID
	if (UAndroidPermissionFunctionLibrary::CheckPermission(InPermission))
	{
		return true;
	}

	if (InMessage.IsEmpty() == false)
	{
		QueueBegin(SMessageBox, *FString::Printf(TEXT("Permission_%s"), *InPermission))
			SMessageBox::Get()->SetHeaderText(NSLOCTEXT("Permission", "Permission.Title", "Permission"));
			SMessageBox::Get()->SetContent(InMessage);
			SMessageBox::Get()->SetButtonsText(FTableBaseStrings::GetBaseText(EBaseStrings::Continue));
			SMessageBox::Get()->OnMessageBoxButton.AddLambda([&, p = InPermission](EMessageBoxButton)
			{
				CheckAndRequestAndroidPermission(p);
				SMessageBox::Get()->ToggleWidget(false);
			});
			SMessageBox::Get()->ToggleWidget(true);
		QueueEnd

		return false;
	}

	const auto Answer = UAndroidPermissionFunctionLibrary::AcquirePermissions(TArray<FString>({ InPermission }));
	if (Answer && Answer->IsValidLowLevel())
	{
		Answer->OnPermissionsGrantedDelegate.BindStatic(&AOnlinePlayerState::OnGrantResultsAndroidPermission);
	}

	return false;
#else
	return true;
#endif
}

void AOnlinePlayerState::OnGrantResultsAndroidPermission(const TArray<FString>& InPermissions, const TArray<bool>& InGrantResults)
{
#if USE_GOOGLE_SERVICES
	SIZE_T _count = 0;
	for (auto Perm : InPermissions)
	{
		if (InGrantResults[_count] == false)
		{
			QueueBegin(SMessageBox, *FString::Printf(TEXT("Permission_%s_%d"), *Perm, _count), p = Perm)
				SMessageBox::Get()->SetHeaderText(NSLOCTEXT("Permission", "Permission.Title", "Permission"));
				SMessageBox::Get()->SetContent(NSLOCTEXT("Permission", "Permission.Desc", "You rejected required permission, please try again, us are really need this permission for correct working the game!"));
				SMessageBox::Get()->SetButtonsText(FTableBaseStrings::GetBaseText(EBaseStrings::Continue));
				SMessageBox::Get()->OnMessageBoxButton.AddLambda([&, sp = p](EMessageBoxButton)
				{
					CheckAndRequestAndroidPermission(sp);
					SMessageBox::Get()->ToggleWidget(false);
				});
				SMessageBox::Get()->ToggleWidget(true);
			QueueEnd
		}
	}
#endif
}

void AOnlinePlayerState::RequestUpdateCurrencyRegion()
{
	if (UProductItemEntity::CurrentPlayerCountry == EIsoCountry::None)
	{
		if (auto Identity = GetIdentityComponent())
		{
			Identity->StartRequestIsoCountry();
		}
	}
}
