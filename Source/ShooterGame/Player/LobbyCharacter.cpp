// Fill out your copyright notice in the Description page of Project Settings.

#include "ShooterGame.h"
#include "LobbyCharacter.h"
#include "BasicPlayerItem.h"
#include "CharacterItemEntity.h"
#include "Inventory/ShooterWeapon.h"
#include "WeaponItemEntity.h"
#include "WeaponPlayerItem.h"
#include "Components/SkinnedMeshComponent.h"
#include "BasicPlayerState.h"

ALobbyCharacter::ALobbyCharacter(const FObjectInitializer& ObjectInitializer)
	: Super(ObjectInitializer)
{
	SpringArm = ObjectInitializer.CreateDefaultSubobject<USpringArmComponent>(this, TEXT("SpringArm"));
	SpringArm->SetupAttachment(GetMesh());

	Camera = ObjectInitializer.CreateDefaultSubobject<UCameraComponent>(this, TEXT("Camera"));
	Camera->SetupAttachment(SpringArm);

	WeaponPreview = ObjectInitializer.CreateDefaultSubobject<USkeletalMeshComponent>(this, TEXT("WeaponPreview"));
	WeaponPreview->bEnableUpdateRateOptimizations = true;
	WeaponPreview->bUseAttachParentBound = true;
	//WeaponPreview->bDeferMovementFromSceneQueries = true;
	WeaponPreview->SetupAttachment(GetMesh());

	GetMesh()->bEnableUpdateRateOptimizations = true;
	GetMesh()->bUseAttachParentBound = true;
	////GetMesh()->bDeferMovementFromSceneQueries = true;

	bIsArmAlternativeMode = false;
}

void ALobbyCharacter::PostInitializeComponents()
{
	Super::PostInitializeComponents();
	if (WeaponPreview)
	{
		WeaponPreview->SetComponentTickEnabledAsync(true);
	}
}

void ALobbyCharacter::TurnAtRate(float InValue)
{
	TurnAtValue(InValue);
}

void ALobbyCharacter::TurnAtValue(float InValue)
{
	AddControllerYawInput(InValue);
}

void ALobbyCharacter::LookUpAtRate(float InValue)
{
	LookUpAtValue(InValue);
}

void ALobbyCharacter::LookUpAtValue(float InValue)
{
	AddControllerPitchInput(InValue);
}

void ALobbyCharacter::InitializeItem(UBasicPlayerItem* InItem, bool IsDefault)
{
	Super::InitializeItem(InItem, IsDefault);

	if (IsDefault)
	{
		if (auto WeaponItem = Cast<UWeaponPlayerItem>(InItem))
		{
			auto MyWeaponEntity = WeaponItem->GetWeaponEntity();

			//WeaponPreview->SetSkeletalMesh(MyWeaponEntity->GetPreviewMeshData().Mesh);
			//WeaponPreview->SetAnimInstanceClass(MyWeaponEntity->GetPreviewMeshData().Anim);
			WeaponPreview->SetSkeletalMesh(MyWeaponEntity->GetFPPMeshData().Mesh);
			//WeaponPreview->SetAnimInstanceClass(MyWeaponEntity->GetFPPMeshData().Anim);
		}
	}
}

void ALobbyCharacter::SetArmMode(bool InIsAlternative)
{
	bIsArmAlternativeMode = InIsAlternative;	
}

void ALobbyCharacter::Tick(float DeltaSeconds)
{
	Super::Tick(DeltaSeconds);

	SpringArm->SocketOffset = FMath::VInterpTo(SpringArm->SocketOffset, bIsArmAlternativeMode ? ArmOffsetAlternative : ArmOffsetNormal, DeltaSeconds, 5);
}

void ALobbyCharacter::OnRep_Instance()
{
	Super::OnRep_Instance();

	if (Instance && Instance->IsValidLowLevel() && Instance->GetEntity<UCharacterItemEntity>())
	{
		auto MyCharacterEntity = Instance->GetEntity<UCharacterItemEntity>();

		GetMesh()->SetSkeletalMesh(MyCharacterEntity->GetLobbyMeshData().Mesh);
		GetMesh()->SetAnimInstanceClass(MyCharacterEntity->GetLobbyMeshData().Anim);
	}
}
