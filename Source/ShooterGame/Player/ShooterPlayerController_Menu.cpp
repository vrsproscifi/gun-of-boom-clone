// Copyright 1998-2018 Epic Games, Inc. All Rights Reserved.

#include "ShooterGame.h"
#include "Player/ShooterPlayerController_Menu.h"
#include "Style/ShooterStyle.h"
#include "GameInstance/ShooterGameViewportClient.h"

AShooterPlayerController_Menu::AShooterPlayerController_Menu(const FObjectInitializer& ObjectInitializer) : Super(ObjectInitializer)
{
}

void AShooterPlayerController_Menu::PostInitializeComponents() 
{
	Super::PostInitializeComponents();

	FShooterStyle::Initialize();
}

void AShooterPlayerController_Menu::OnEscape()
{
	if (auto MyViewport = UShooterGameViewportClient::Get())
	{
		MyViewport->OnBackWidget();
	}
}
