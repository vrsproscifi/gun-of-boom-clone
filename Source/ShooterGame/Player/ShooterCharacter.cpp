﻿// Copyright 1998-2018 Epic Games, Inc. All Rights Reserved.

#include "ShooterGame.h"
#include "ShooterCharacter.h"
#include "Inventory/ShooterWeapon.h"
#include "GamePlay/Pickups/ShooterPickup.h"

#include "Inventory/ShooterDamageType.h"
#include "ShooterHUD.h"
#include "GameState/ShooterPlayerState.h"
#include "Animation/AnimMontage.h"
#include "Animation/AnimInstance.h"
#include "Sound/SoundNodeLocalPlayer.h"
#include "AudioThread.h"
#include "ShooterCharacterMovement.h"
#include "GameMode/ShooterGameMode.h"
#include "ShooterPlayerController.h"

#include "Engine/Types/ShooterPhysMaterialType.h"
#include "WeaponPlayerItem.h"
#include "CharacterItemEntity.h"
#include "GameState/ShooterGameState.h"
#include "WeaponItemEntity.h"
#include "LookOnComponent.h"
#include "GameInstance/ShooterGameUserSettings.h"
#include "ArsenalComponent.h"

#include "Inventory/ShooterGrenade.h"
#include "Inventory/ShooterArmour.h"
#include "Inventory/ShooterAid.h"

#include "GrenadePlayerItem.h"
#include "ArmourPlayerItem.h"
#include "AidPlayerItem.h"

#include "ShooterGameSingleton.h"
#include "TeamColorsData.h"
#include "ShooterGameAchievements.h"

#include "Extensions/GameSingletonExtensions.h"
#include "Inventory/Projectile/GrenadeProjectile.h"
#include "GrenadeItemEntity.h"
#include "ShooterGameAnalytics.h"
#include "WidgetComponent.h"
#include "State/SNameAndHealth.h"
#include "UObjectExtensions.h"
#include "KnifeComponent.h"
#include "KnifePlayerItem.h"
#include "GamePlay/Knife.h"
#include "KnifeItemEntity.h"
#include "GamePlay/Pickups/ShooterPickup_Weapon.h"

static int32 NetVisualizeRelevancyTestPoints = 0;
FAutoConsoleVariableRef CVarNetVisualizeRelevancyTestPoints(
	TEXT("p.NetVisualizeRelevancyTestPoints"),
	NetVisualizeRelevancyTestPoints,
	TEXT("")
	TEXT("0: Disable, 1: Enable"),
	ECVF_Cheat);


static int32 NetEnablePauseRelevancy = 1;
FAutoConsoleVariableRef CVarNetEnablePauseRelevancy(
	TEXT("p.NetEnablePauseRelevancy"),
	NetEnablePauseRelevancy,
	TEXT("")
	TEXT("0: Disable, 1: Enable"),
	ECVF_Cheat);

const FName AShooterCharacter::HeadBoneName = "head";


FTakeHitInfo::FTakeHitInfo()
	: ActualDamage(0)
	, DamageTypeClass(NULL)
	, PawnInstigator(NULL)
	, DamageCauser(NULL)
	, DamageEventClassID(0)
	, bKilled(false)
	, EnsureReplicationByte(0)
{}

FDamageEvent& FTakeHitInfo::GetDamageEvent()
{
	switch (DamageEventClassID)
	{
	case FPointDamageEvent::ClassID:
		if (PointDamageEvent.DamageTypeClass == NULL)
		{
			PointDamageEvent.DamageTypeClass = DamageTypeClass ? DamageTypeClass : UDamageType::StaticClass();
		}
		return PointDamageEvent;

	case FRadialDamageEvent::ClassID:
		if (RadialDamageEvent.DamageTypeClass == NULL)
		{
			RadialDamageEvent.DamageTypeClass = DamageTypeClass ? DamageTypeClass : UDamageType::StaticClass();
		}
		return RadialDamageEvent;

	default:
		if (GeneralDamageEvent.DamageTypeClass == NULL)
		{
			GeneralDamageEvent.DamageTypeClass = DamageTypeClass ? DamageTypeClass : UDamageType::StaticClass();
		}
		return GeneralDamageEvent;
	}
}

void FTakeHitInfo::SetDamageEvent(const FDamageEvent& DamageEvent)
{
	DamageEventClassID = DamageEvent.GetTypeID();
	switch (DamageEventClassID)
	{
	case FPointDamageEvent::ClassID:
		PointDamageEvent = *((FPointDamageEvent const*)(&DamageEvent));
		break;
	case FRadialDamageEvent::ClassID:
		RadialDamageEvent = *((FRadialDamageEvent const*)(&DamageEvent));
		break;
	default:
		GeneralDamageEvent = DamageEvent;
	}

	DamageTypeClass = DamageEvent.DamageTypeClass;
}

void FTakeHitInfo::EnsureReplication()
{
	EnsureReplicationByte++;
}

AShooterCharacter::AShooterCharacter(const FObjectInitializer& ObjectInitializer)
	: Super(ObjectInitializer.SetDefaultSubobjectClass<UShooterCharacterMovement>(ACharacter::CharacterMovementComponentName))
	, bSpawnProtectionEligible(true)
	, Health(FVector2D(1000.0f, 1000.0f))
{
	HitSoundConcurrency = ObjectInitializer.CreateDefaultSubobject<USoundConcurrency>(this, TEXT("HitSoundConcurrency"));	
	HitSoundConcurrency->Concurrency.ResolutionRule = EMaxConcurrentResolutionRule::PreventNew;
	HitSoundConcurrency->Concurrency.bLimitToOwner = true;
	HitSoundConcurrency->Concurrency.MaxCount = 1;

	Armours[ECharacterArmour::Mask] = ObjectInitializer.CreateDefaultSubobject<UShooterArmour>(this, TEXT("ShooterArmourMask"));
	Armours[ECharacterArmour::Tors] = ObjectInitializer.CreateDefaultSubobject<UShooterArmour>(this, TEXT("ShooterArmourTors"));
	Armours[ECharacterArmour::Pants] = ObjectInitializer.CreateDefaultSubobject<UShooterArmour>(this, TEXT("ShooterArmourPants"));

	ShooterGrenade = ObjectInitializer.CreateDefaultSubobject<UShooterGrenade>(this, TEXT("ShooterGrenade"));
	ShooterGrenade->OnPlayerItemUsed.BindUObject(this, &AShooterCharacter::OnPlayerItemUsed);

	ShooterAid = ObjectInitializer.CreateDefaultSubobject<UShooterAid>(this, TEXT("ShooterAid"));
	ShooterAid->OnPlayerItemUsed.BindUObject(this, &AShooterCharacter::OnPlayerItemUsed);
	ShooterAid->SetupAttachment(GetCapsuleComponent());

	Shooter1PCamera = ObjectInitializer.CreateDefaultSubobject<UCameraComponent>(this, TEXT("Shooter1PCamera"));
	Shooter1PCamera->SetupAttachment(GetCapsuleComponent());
	Shooter1PCamera->SetRelativeLocation(FVector(-1000, 0, 0));
	Shooter1PCamera->SetAutoActivate(true);

	ShooterArmComponent = ObjectInitializer.CreateDefaultSubobject<USphereComponent>(this, TEXT("ShooterArmComponent"));
	ShooterArmComponent->SetupAttachment(Shooter1PCamera);

	Mesh1P = ObjectInitializer.CreateDefaultSubobject<USkeletalMeshComponent>(this, TEXT("PawnMesh1P"));
	Mesh1P->SetupAttachment(ShooterArmComponent);
	Mesh1P->bOnlyOwnerSee = true;
	Mesh1P->bOwnerNoSee = false;
	Mesh1P->bReceivesDecals = false;
	Mesh1P->bEnableUpdateRateOptimizations = true;
	Mesh1P->VisibilityBasedAnimTickOption = EVisibilityBasedAnimTickOption::OnlyTickPoseWhenRendered;
	Mesh1P->PrimaryComponentTick.TickGroup = TG_PrePhysics;
	Mesh1P->SetCollisionObjectType(ECC_Pawn);
	Mesh1P->SetCollisionEnabled(ECollisionEnabled::NoCollision);
	Mesh1P->SetCollisionResponseToAllChannels(ECR_Ignore);
	Mesh1P->bUseAttachParentBound = true;
	//Mesh1P->bDeferMovementFromSceneQueries = true;
	//Mesh1P->UseAsyncScene = EDynamicActorScene::UseAsyncScene;
	//PrimaryActorTick.bRunOnAnyThread = true;
	Mesh1P->bDisableClothSimulation = true;	
	Mesh1P->CastShadow = false;
	Mesh1P->bAffectDynamicIndirectLighting = false;
	Mesh1P->bAffectDistanceFieldLighting = false;
	Mesh1P->bCastStaticShadow = false;
	Mesh1P->bCastDynamicShadow = false;
	Mesh1P->bCastVolumetricTranslucentShadow = false;
	Mesh1P->bSelfShadowOnly = true;
	Mesh1P->bReceiveMobileCSMShadows = false;
	Mesh1P->bDisableMorphTarget = true;
	Mesh1P->bPerBoneMotionBlur = false;

	
	Mesh1P->ClothingSimulationFactory = nullptr;

	GetMesh()->bOnlyOwnerSee = false;
	GetMesh()->bOwnerNoSee = true;
	GetMesh()->bReceivesDecals = false;
	GetMesh()->bEnableUpdateRateOptimizations = true;
	GetMesh()->SetCollisionObjectType(ECC_Pawn);
	GetMesh()->VisibilityBasedAnimTickOption = EVisibilityBasedAnimTickOption::OnlyTickPoseWhenRendered;
	GetMesh()->SetCollisionEnabled(ECollisionEnabled::QueryAndPhysics);
	GetMesh()->SetCollisionResponseToChannel(COLLISION_WEAPON, ECR_Block);
	GetMesh()->SetCollisionResponseToChannel(ECC_Visibility, ECR_Block);
	GetMesh()->bUseAttachParentBound = true;
	//GetMesh()->bDeferMovementFromSceneQueries = true;
	//GetMesh()->UseAsyncScene = EDynamicActorScene::UseAsyncScene;

	GetMesh()->bDisableClothSimulation = true;
	GetMesh()->CastShadow = false;
	GetMesh()->bAffectDynamicIndirectLighting = false;
	GetMesh()->bAffectDistanceFieldLighting = false;
	GetMesh()->bCastStaticShadow = false;
	GetMesh()->bCastDynamicShadow = false;
	GetMesh()->bCastVolumetricTranslucentShadow = false;
	GetMesh()->bSelfShadowOnly = true;
	GetMesh()->bReceiveMobileCSMShadows = false;
	GetMesh()->bDisableMorphTarget = true;
	GetMesh()->bPerBoneMotionBlur = false;


	GetCapsuleComponent()->SetCollisionResponseToChannel(ECC_Camera, ECR_Ignore);
	GetCapsuleComponent()->SetCollisionResponseToChannel(COLLISION_WEAPON, ECR_Ignore);

	AutoTargeting = ObjectInitializer.CreateDefaultSubobject<ULookOnComponent>(this, TEXT("AutoTargeting"));
	AutoTargeting->SetTargetRange(50.0f);
	AutoTargeting->SetTargetVisionAngle(50.0f);
	AutoTargeting->SetTargetingAutoFinding(0.1f);
	//AutoTargeting->OnExternalTargetCheck.RemoveDynamic(this, &AShooterCharacter::OnIsEnemy);
	AutoTargeting->OnExternalTargetCheck.BindDynamic(this, &AShooterCharacter::OnAutoTargetingValidation);
	
	WidgetScreenComponent = ObjectInitializer.CreateDefaultSubobject<UWidgetComponent>(this, TEXT("WidgetScreenComponent"));
	WidgetScreenComponent->SetupAttachment(GetMesh());
	WidgetScreenComponent->SetBlendMode(EWidgetBlendMode::Transparent);
	WidgetScreenComponent->SetDrawAtDesiredSize(true);
	WidgetScreenComponent->SetWidgetSpace(EWidgetSpace::Screen);
	

	TargetingSpeedModifier = 0.5f;
	bIsTargeting = false;
	RunningSpeedModifier = 1.5f;
	bWantsToRun = false;
	bWantsToFire = false;
	LowHealthPercentage = 0.5f;
	BodyDissolveStarted = false;

	BaseTurnRate = 45.f;
	BaseLookUpRate = 45.f;

	NetUpdateFrequency = 45.0f;
	NetCullDistanceSquared = 500000000.0f;

	MeshLag_LookAmount = { 1.5f, 5.0f };
	MeshLag_SmoothAmount = { 10.0f, 8.0f };
	MeshLag_InitLoc = FVector::ZeroVector;
	MeshLag_InitRot = FRotator::ZeroRotator;
	MeshLag_TargetingMultipler = 1.0f;
	bIsInitMeshLag = false;
}

void AShooterCharacter::InitializeArmourInstance(const ECharacterArmour::Type& InArmourSlot, UBasicPlayerItem* InPlayerItemInstance)
{
	if (auto armour = GetArmour(InArmourSlot))
	{
		armour->InitializeInstance(InPlayerItemInstance);
	}
	else
	{
		UE_LOG(LogOnline, Error, TEXT("> [AShooterCharacter::InitializeArmourInstance][%s / %d][Armour %d was nullptr]"), *InPlayerItemInstance->GetName(), InPlayerItemInstance->GetModelId(), static_cast<int32>(InArmourSlot));
	}
}

void AShooterCharacter::InitializeArmourInstance(UBasicPlayerItem* InPlayerItemInstance)
{
	if (InPlayerItemInstance)
	{
		switch (InPlayerItemInstance->GetItemType())
		{
		case EGameItemType::Mask: InitializeArmourInstance(ECharacterArmour::Mask, InPlayerItemInstance); break;
		case EGameItemType::Torso: InitializeArmourInstance(ECharacterArmour::Tors, InPlayerItemInstance); break;
		case EGameItemType::Legs: InitializeArmourInstance(ECharacterArmour::Pants, InPlayerItemInstance); break;
		}
	}
}

UShooterArmour* AShooterCharacter::GetArmour(const ECharacterArmour::Type& InArmourSlot) const
{
	return Armours[InArmourSlot];
}


float AShooterCharacter::TakeArmourDamage(const float& InActualDamage, float& OutArmourDamage) const
{
	float ResultActualDamage = InActualDamage;
	for (const auto TargetArmour : Armours)
	{
		if (auto MyArmour = GetValidObject(TargetArmour))
		{
			ResultActualDamage = MyArmour->TakeDamage(ResultActualDamage, OutArmourDamage);
		}
	}

	return ResultActualDamage;
}

float AShooterCharacter::GetCurrentArmour() const
{
	float ResultActualDamage = 0;
	for (const auto Armour : Armours)
	{
		if (auto armour = GetValidObject(Armour))
		{
			ResultActualDamage += armour->GetCurrentArmour();
		}
	}
	return ResultActualDamage;
}

float AShooterCharacter::GetTotalArmour() const
{
	float ResultActualDamage = 0;
	for (const auto Armour : Armours)
	{
		if (auto armour = GetValidObject(Armour))
		{
			ResultActualDamage += armour->GetMaximumArmour();
		}
	}
	return FMath::Max(1.0f, ResultActualDamage);
}

void AShooterCharacter::AddOverlapedPickup(AShooterPickup* InPickup)
{
	OverlapedPickups.AddUnique(InPickup);
}

void AShooterCharacter::RemoveOverlapedPickup(AShooterPickup* InPickup)
{
	OverlapedPickups.Remove(InPickup);
}

TArray<AShooterPickup*> AShooterCharacter::GetOverlapedPickups() const
{
	return OverlapedPickups;
}

AShooterPickup* AShooterCharacter::GetNearlyOverlapedPickup() const
{
	AShooterPickup* Answer = nullptr;
	float CurrentDistance = MAX_FLT;
	const FVector CurrentLocation = GetActorLocation();

	for (auto Target : OverlapedPickups)
	{
		if (IsValidObject(Target))
		{
			const float TargetDistance = FVector::Distance(CurrentLocation, Target->GetActorLocation());
			if (TargetDistance < CurrentDistance)
			{
				CurrentDistance = TargetDistance;
				Answer = Target;
			}
		}
	}

	return Answer;
}

bool AShooterCharacter::OnAutoTargetingValidation(AActor* InActor)
{
	if(auto character = GetValidObjectAs<AShooterCharacter>(InActor))
	{

		//-----------------------------
		if (character->IsAlive() == false)
		{
			return false;
		}

		//-----------------------------
		//auto Dist = FVector::Dist(GetActorLocation(), InActor->GetActorLocation());

		////-----------------------------
		//if(auto weapon = GetWeapon())
		//{
		//	return weapon->IsAllowFireOnDistance(Dist) && OnIsEnemy(InActor);
		//}
		return OnIsEnemy(InActor);
	}
	return false;
}

float AShooterCharacter::GetWorldTimeSeconds() const
{
	if (auto world = GetValidObject(GetWorld()))
	{
		return world->TimeSeconds;
	}
	return 0.0f;
}

const bool& AShooterCharacter::IsSpawnProtectionEligible() const
{
	return bSpawnProtectionEligible;
}

bool AShooterCharacter::IsSpawnProtected() const
{
	return IsSpawnProtectionEligible() && GetWorldTimeSeconds() - CreationTime < GetWorldSpawnProtectionTime();
}

void AShooterCharacter::DisableSpawnProtection()
{
	bSpawnProtectionEligible = false;
}

float AShooterCharacter::GetSpawnProtectionProgress() const
{
	return (GetWorldTimeSeconds() - CreationTime) / GetWorldSpawnProtectionTime(1.0f);
}

float AShooterCharacter::GetWorldSpawnProtectionTime(const float& InDefaultResult) const
{
	if (auto gs = GetBasicGameState<AShooterGameState>())
	{
		return gs->GetSpawnProtectionTime();
	}

	return InDefaultResult;
}

void AShooterCharacter::OnPlayerItemUsed(const UBasicPlayerItem* inItem) const
{
	if(const auto inventory = GetInventoryComponent())
	{
		inventory->SendRequestUsePlayerItem(inItem, 1);
	}
	else
	{
		UE_LOG(LogInit, Error, TEXT("> [AShooterCharacter::OnPlayerItemUsed][inventory was nullptr]"));
	}
}

//=================================================================
#pragma region Components
UIdentityComponent* AShooterCharacter::GetIdentityComponent() const
{
	if (const auto ps = GetBasePlayerState<AShooterPlayerState>())
	{
		return ps->GetIdentityComponent();
	}
	return nullptr;
}

UArsenalComponent* AShooterCharacter::GetInventoryComponent() const
{
	if (const auto ps = GetBasePlayerState<AShooterPlayerState>())
	{
		return ps->GetInventoryComponent();
	}
	return nullptr;
}

USquadComponent* AShooterCharacter::GetSquadComponent() const
{
	if (const auto ps = GetBasePlayerState<AShooterPlayerState>())
	{
		return ps->GetSquadComponent();
	}
	return nullptr;
}

UProgressComponent* AShooterCharacter::GetProgressComponent() const
{
	if (const auto ps = GetBasePlayerState<AShooterPlayerState>())
	{
		return ps->GetProgressComponent();
	}
	return nullptr;
}

#pragma endregion

void AShooterCharacter::OnClientWeaponPickUp()
{
	OnWeaponPickUp();
	FShooterGameAnalytics::RecordDesignEvent("UI:Game:WeaponPickUp");
}

void AShooterCharacter::OnClientAidActivate()
{
	OnAidActivate();
	FShooterGameAnalytics::RecordDesignEvent("UI:Game:AidActivate");
}

void AShooterCharacter::OnClientGrenadeActivate()
{
	//if (auto MyGrenade = GetGrenade())
	//{
	//	if (MyGrenade->AnyAmount() && MyGrenade->IsAllowStartProcess() && MyGrenade->GetGrenadeProperty())
	//	{
	//		PlayTwoAnimMontage(MyGrenade->GetGrenadeProperty()->FireAnim);
	//		
	//	}
	//}

	OnGrenadeActivate();
	FShooterGameAnalytics::RecordDesignEvent("UI:Game:GrenadeActivate");
}

void AShooterCharacter::OnClientKnifeActivate()
{
	if (auto MyKnife = GetValidObject(ShooterKnife))
	{
		if (IsTargeting())
		{
			SetTargeting(false);
		}

		OnKnifeActivate();
		FShooterGameAnalytics::RecordDesignEvent("UI:Game:KnifeActivate");
	}
}

void AShooterCharacter::SetupPlayerInputComponent(class UInputComponent* PlayerInputComponent)
{
	Super::SetupPlayerInputComponent(PlayerInputComponent);
	PlayerInputComponent->BindAction("WeaponPickup", IE_Pressed, this, &AShooterCharacter::OnClientWeaponPickUp);
	PlayerInputComponent->BindAction("AidActivate", IE_Pressed, this, &AShooterCharacter::OnClientAidActivate);
	PlayerInputComponent->BindAction("GrenadeActivate", IE_Pressed, this, &AShooterCharacter::OnClientGrenadeActivate);
	PlayerInputComponent->BindAction("KnifeActivate", IE_Pressed, this, &AShooterCharacter::OnClientKnifeActivate);

	PlayerInputComponent->BindAction("Fire", IE_Pressed, this, &AShooterCharacter::OnStartFire);
	PlayerInputComponent->BindAction("Fire", IE_Released, this, &AShooterCharacter::OnStopFire);

	PlayerInputComponent->BindAction("Targeting", IE_Pressed, this, &AShooterCharacter::OnClientStartTargeting);
	PlayerInputComponent->BindAction("Targeting", IE_Released, this, &AShooterCharacter::OnStopTargeting);

	PlayerInputComponent->BindAction("NextWeapon", IE_Pressed, this, &AShooterCharacter::OnNextWeapon);
	PlayerInputComponent->BindAction("PrevWeapon", IE_Pressed, this, &AShooterCharacter::OnPrevWeapon);

	PlayerInputComponent->BindAction("Reload", IE_Pressed, this, &AShooterCharacter::OnClientReload);

	PlayerInputComponent->BindAction("Jump", IE_Pressed, this, &AShooterCharacter::OnStartJump);
	PlayerInputComponent->BindAction("Jump", IE_Released, this, &AShooterCharacter::OnStopJump);

	PlayerInputComponent->BindAction("Run", IE_Pressed, this, &AShooterCharacter::OnStartRunning);
	PlayerInputComponent->BindAction("RunToggle", IE_Pressed, this, &AShooterCharacter::OnStartRunningToggle);
	PlayerInputComponent->BindAction("Run", IE_Released, this, &AShooterCharacter::OnStopRunning);
}

void AShooterCharacter::OnTryDisableSpawnProtection()
{
	if (IsSpawnProtected() == false)
	{
		if (auto tm = GetTimerManager())
		{
			tm->ClearTimer(SpawnProtectionHandle);
		}
	}
}

void AShooterCharacter::PostInitializeComponents()
{
	Super::PostInitializeComponents();

	SetNetDormancy(ENetDormancy::DORM_Never);
		
	if (ShooterArmComponent)
	{
		ShooterArmComponent->SetComponentTickEnabledAsync(true);
	}

	if (Mesh1P)
	{
		Mesh1P->SetComponentTickEnabledAsync(true);
	}

	if (auto mesh = GetMesh())
	{
		mesh->SetComponentTickEnabledAsync(true);
	}

	if (GetLocalRole() == ROLE_Authority)
	{
		//Health.X = Health.Y;
		SpawnDefaultInventory();

		if (auto tm = GetTimerManager())
		{
			tm->SetTimer(SpawnProtectionHandle, this, &AShooterCharacter::OnTryDisableSpawnProtection, 10.0f, true, 10.0f);
		}
	}

	// set initial mesh visibility (3rd person view)
	UpdatePawnMeshes();

	// play respawn effects
	if (GetNetMode() != NM_DedicatedServer)
	{
		if (RespawnFX)
		{
			UGameplayStatics::SpawnEmitterAtLocation(this, RespawnFX, GetActorLocation(), GetActorRotation());
		}

		if (RespawnSound)
		{
			UGameplayStatics::PlaySoundAtLocation(this, RespawnSound, GetActorLocation());
		}
	}
}

void AShooterCharacter::Destroyed()
{
	Super::Destroyed();
	DestroyInventory();
}

void AShooterCharacter::PawnClientRestart()
{
	Super::PawnClientRestart();

	// switch mesh to 1st person view
	UpdatePawnMeshes();

	// reattach weapon if needed
	SetCurrentWeapon(CurrentWeapon);
}

void AShooterCharacter::PossessedBy(class AController* InController)
{
	Super::PossessedBy(InController);

	// [server] as soon as PlayerState is assigned, set team colors of this pawn for local player
	UpdateTeamColor();
}

void AShooterCharacter::OnRep_PlayerState()
{
	Super::OnRep_PlayerState();

	// [client] as soon as PlayerState is assigned, set team colors of this pawn for local player
	if (GetPlayerState())
	{
		UpdateTeamColor();
	}
}

FRotator AShooterCharacter::GetAimOffsets() const
{
	const FVector AimDirWS = GetBaseAimRotation().Vector();
	const FVector AimDirLS = ActorToWorld().InverseTransformVectorNoScale(AimDirWS);
	const FRotator AimRotLS = AimDirLS.Rotation();

	return AimRotLS;
}

bool AShooterCharacter::IsEnemyFor(AController* TestPC) const
{
	AShooterGameState* ShooterGameState = GetBasicGameState<AShooterGameState>();
	if (ShooterGameState && ShooterGameState->IsValidLowLevel())
	{
		return !ShooterGameState->HasSameTeam(this, TestPC);
	}

	return true;
}

//////////////////////////////////////////////////////////////////////////
// Meshes

void AShooterCharacter::UpdatePawnMeshes()
{
	bool const bFirstPerson = IsFirstPerson();

	Mesh1P->VisibilityBasedAnimTickOption = !bFirstPerson ? EVisibilityBasedAnimTickOption::OnlyTickPoseWhenRendered : EVisibilityBasedAnimTickOption::AlwaysTickPoseAndRefreshBones;
	Mesh1P->SetOwnerNoSee(!bFirstPerson);

	GetMesh()->VisibilityBasedAnimTickOption = bFirstPerson ? EVisibilityBasedAnimTickOption::OnlyTickPoseWhenRendered : EVisibilityBasedAnimTickOption::AlwaysTickPoseAndRefreshBones;
	GetMesh()->SetOwnerNoSee(bFirstPerson);
}

void AShooterCharacter::OnCameraUpdate(const FVector& CameraLocation, const FRotator& CameraRotation)
{
	//USceneComponent* DefMesh1P = Cast<USceneComponent>(GetClass()->GetDefaultSubobjectByName(TEXT("PawnMesh1P"))); // PawnMesh1P ShooterArmComponent
	//const FMatrix DefMeshLS = FRotationTranslationMatrix(DefMesh1P->RelativeRotation, DefMesh1P->RelativeLocation);
	//const FMatrix LocalToWorld = ActorToWorld().ToMatrixWithScale();

	//// Mesh rotating code expect uniform scale in LocalToWorld matrix

	//const FRotator RotCameraPitch(CameraRotation.Pitch, 0.0f, 0.0f);
	//const FRotator RotCameraYaw(0.0f, CameraRotation.Yaw, 0.0f);

	//const FMatrix LeveledCameraLS = FRotationTranslationMatrix(RotCameraYaw, CameraLocation) * LocalToWorld.Inverse();
	//const FMatrix PitchedCameraLS = FRotationMatrix(RotCameraPitch) * LeveledCameraLS;
	//const FMatrix MeshRelativeToCamera = DefMeshLS * LeveledCameraLS.Inverse();
	//const FMatrix PitchedMesh = MeshRelativeToCamera * PitchedCameraLS;

	//// Mesh lag
	//const float LookAmount = 5.0f;
	//const float SmoothAmount = 8.0f;

	//const float Turn = GetInputAxisValue(TEXT("Turn")) * LookAmount;
	//const float LookUp = GetInputAxisValue(TEXT("LookUp")) * LookAmount;

	//const FRotator FinalRot(LookUp, Turn, Turn);
	//const FRotator ZeroRot = FRotator::ZeroRotator;

	//FRotator TargetRot;

	//TargetRot.Roll = ZeroRot.Roll - FinalRot.Roll;
	//TargetRot.Pitch = ZeroRot.Pitch + FinalRot.Pitch;
	//TargetRot.Yaw = ZeroRot.Yaw - FinalRot.Yaw;

	//const FRotator TargetRotationBlend = FMath::Lerp(PitchedMesh.Rotator(), TargetRot, GetWorld()->GetDeltaSeconds() * SmoothAmount);

	////DefMesh1P->SetRelativeRotation(PitchedMesh.Rotator());
	//DefMesh1P->SetRelativeLocationAndRotation(PitchedMesh.GetOrigin(), PitchedMesh.Rotator());
}


//////////////////////////////////////////////////////////////////////////
// Damage & death


void AShooterCharacter::FellOutOfWorld(const class UDamageType& dmgType)
{
	Die(Health.X, FDamageEvent(dmgType.GetClass()), NULL, NULL);
}

void AShooterCharacter::Suicide()
{
	KilledBy(this);
}

void AShooterCharacter::KilledBy(APawn* EventInstigator)
{
	if (GetLocalRole() == ROLE_Authority && !bIsDying)
	{
		AController* Killer = NULL;
		if (EventInstigator != NULL)
		{
			Killer = EventInstigator->Controller;
			LastHitBy = NULL;
		}

		Die(Health.X, FDamageEvent(UDamageType::StaticClass()), Killer, NULL);
	}
}

FName AShooterCharacter::GetGetDamagedBoneName(const FDamageEvent& DamageEvent)
{
	if (DamageEvent.IsOfType(FPointDamageEvent::ClassID))
	{
		return static_cast<FPointDamageEvent const&>(DamageEvent).HitInfo.BoneName;
	}
	return NAME_None;
}

float AShooterCharacter::GetDamageRate(const FDamageEvent& DamageEvent) const
{
	float dmg = 1.0f;
	if (DamageEvent.IsOfType(FPointDamageEvent::ClassID))
	{
		const auto PointDamageEvent = static_cast<FPointDamageEvent const&>(DamageEvent);
		const auto BoneName = PointDamageEvent.HitInfo.BoneName;
		if (BoneName != NAME_None)
		{
			const auto rate = FGameSingletonExtensions::FindBonesDamageRate(BoneName);
			if (rate)
			{
				dmg = *rate;
			}
		}
	}
	else if (DamageEvent.IsOfType(FRadialDamageEvent::ClassID))
	{
		const auto RadialDamageEvent = static_cast<FRadialDamageEvent const&>(DamageEvent);
		for (auto HitInfo : RadialDamageEvent.ComponentHits)
		{
			if (HitInfo.BoneName != NAME_None && HitInfo.Actor == this)
			{
				const auto rate = FGameSingletonExtensions::FindBonesDamageRate(HitInfo.BoneName);
				if (rate)
				{
					dmg += *rate;
				}
			}
		}
	}
	return dmg;
}

float AShooterCharacter::TakeDamage(float Damage, struct FDamageEvent const& DamageEvent, class AController* InEventInstigator, class AActor* InDamageCauser)
{
	//==================================
	if (Damage < 0)
	{
		return 0;
	}

	//==================================
	if (Health.X <= 0.f)
	{
		return 0.f;
	}

	//==================================
	const float dmgRate = GetDamageRate(DamageEvent);

	//==================================
	const auto EventInstigator = GetValidObject(InEventInstigator);
	const auto DamageCauser = GetValidObject(InDamageCauser);

	//==================================
	if(const auto world = GetValidWorld())
	{
		if (const auto gm = GetValidObject(world->GetAuthGameMode<AShooterGameMode>()))
		{
			Damage = gm->ModifyDamage(Damage, this, DamageEvent, EventInstigator, DamageCauser);
		}
		else Damage = 0.0f;
	}
	else Damage = 0.0f;

	//==================================
	float ActualDamage = Super::TakeDamage(Damage, DamageEvent, EventInstigator, DamageCauser);
	if (ActualDamage > 0.f)
	{
		float DamageToArmor = .0f;

		//-------------------------------------------------------------
		ActualDamage = TakeArmourDamage(ActualDamage, DamageToArmor);

		//-------------------------------------------------------------
		float StoreDamage = FMath::Clamp<float>(ActualDamage * dmgRate, .0f, Health.X);

		//-------------------------------------------------------------
		const float PreviewHealthAmount = Health.X;

		//-------------------------------------------------------------
		Health.X -= StoreDamage;
		StoreDamage += DamageToArmor;


		//---------------------------------
		if (auto MyState = GetValidObjectAs<AShooterPlayerState>(GetPlayerState()))
		{
			if (EventInstigator && EventInstigator->PlayerState)
			{
				FPlayerDamageData DamageData;

				if (const auto DamageByWeapon = GetValidObjectAs<AShooterWeapon>(DamageCauser))
				{
					DamageData.Weapon = DamageByWeapon->GetModelId();
				}
				else if (auto GrenadeActor = Cast<AGrenadeProjectile>(DamageCauser))
				{
					if (GrenadeActor->GetInstanceEntity())
					{
						DamageData.Weapon = GrenadeActor->GetInstanceEntity()->GetModelId();
					}
				}
				else if (auto KnifeActor = Cast<AKnife>(DamageCauser))
				{
					if (KnifeActor->GetInstanceEntity())
					{
						DamageData.Weapon = KnifeActor->GetInstanceEntity()->GetModelId();
					}
				}

				DamageData.Damage = StoreDamage;
				DamageData.Owner = EventInstigator->PlayerState;
				MyState->AddDamageData(EventInstigator->PlayerState->GetUniqueID(), DamageData);
			}
		}

		//-------------------------------------------------------------
		if (Health.X <= 0.5f)
		{
			//==========================================================
			const auto EventInstigatorSPC = GetValidObjectAs<AShooterPlayerController>(EventInstigator);
			if (EventInstigatorSPC)
			{
				//-------------------------------------------------------------
				//	PreviewHealthAmount = 100
				//	StoreDamage = 1200
				//	BrutalKillDamage = 1200 - 100 = 100

				//-------------------------------------------------------------
				const float BrutalKillDamage = StoreDamage - PreviewHealthAmount;
				if(BrutalKillDamage >= 999.0f)
				{
					EventInstigatorSPC->GivePlayerAchievement(EShooterGameAchievements::BrutalKill);
				}

				//-------------------------------------------------------------
				if(GetGetDamagedBoneName(DamageEvent) == HeadBoneName)
				{
					EventInstigatorSPC->GivePlayerAchievement(EShooterGameAchievements::HeadShot);
				}
			}
				
			//==========================================================
			Die(StoreDamage, DamageEvent, EventInstigator, DamageCauser);
		}
		else
		{
			PlayHit(StoreDamage, DamageEvent, EventInstigator ? EventInstigator->GetPawn() : nullptr, DamageCauser);
		}
		
		MakeNoise(1.0f, EventInstigator ? EventInstigator->GetPawn() : this);
		return StoreDamage;
	}

	return ActualDamage;
}


bool AShooterCharacter::CanDie(float KillingDamage, FDamageEvent const& DamageEvent, AController* Killer, AActor* DamageCauser) const
{
	if (bIsDying										// already dying
		|| IsPendingKill()								// already destroyed
		|| GetLocalRole() != ROLE_Authority						// not authority
		|| GetWorld()->GetAuthGameMode<AShooterGameMode>() == NULL
		|| GetWorld()->GetAuthGameMode<AShooterGameMode>()->GetMatchState() == MatchState::LeavingMap)	// level transition occurring
	{
		return false;
	}

	return true;
}


bool AShooterCharacter::Die(float KillingDamage, FDamageEvent const& DamageEvent, AController* Killer, AActor* DamageCauser)
{
	if (!CanDie(KillingDamage, DamageEvent, Killer, DamageCauser))
	{
		return false;
	}

	Health.X = FMath::Max(0.0f, Health.X);

	int32 WeaponId = INDEX_NONE;
	if (auto WeaponActor = GetValidObjectAs<AShooterWeapon>(DamageCauser))
	{
		if (WeaponActor->GetInstanceEntity())
		{
			WeaponId = WeaponActor->GetInstanceEntity()->GetModelId();
		}
	}
	else if (auto GrenadeActor = Cast<AGrenadeProjectile>(DamageCauser))
	{
		if (GrenadeActor->GetInstanceEntity())
		{
			WeaponId = GrenadeActor->GetInstanceEntity()->GetModelId();
		}
	}
	else if (auto KnifeActor = Cast<AKnife>(DamageCauser))
	{
		if (KnifeActor->GetInstanceEntity())
		{
			WeaponId = KnifeActor->GetInstanceEntity()->GetModelId();
		}
	}

	// if this is an environmental death then refer to the previous killer so that they receive credit (knocked into lava pits, etc)
	UDamageType const* const DamageType = DamageEvent.DamageTypeClass ? DamageEvent.DamageTypeClass->GetDefaultObject<UDamageType>() : GetDefault<UDamageType>();
	Killer = GetDamageInstigator(Killer, *DamageType);

	AController* const KilledPlayer = (Controller != NULL) ? Controller : Cast<AController>(GetOwner());
	GetWorld()->GetAuthGameMode<AShooterGameMode>()->Killed(Killer, KilledPlayer, this, WeaponId);

	NetUpdateFrequency = GetDefault<AShooterCharacter>()->NetUpdateFrequency;
	GetCharacterMovement()->ForceReplicationUpdate();

	OnDeath(KillingDamage, DamageEvent, Killer ? Killer->GetPawn() : NULL, DamageCauser);
	return true;
}


void AShooterCharacter::OnDeath(float KillingDamage, struct FDamageEvent const& DamageEvent, class APawn* PawnInstigator, class AActor* DamageCauser)
{
	if (bIsDying)
	{
		return;
	}

	SetReplicatingMovement(false);
	
	TearOff();
	bIsDying = true;

	if (GetLocalRole() == ROLE_Authority)
	{
		if (DissolveCurve)
		{
			FVector2D TimeRange;
			DissolveCurve->GetTimeRange(TimeRange.X, TimeRange.Y);
			SetLifeSpan(TimeRange.Y + .5f);
		}

		ReplicateHit(KillingDamage, DamageEvent, PawnInstigator, DamageCauser, true);

		// play the force feedback effect on the client player controller
		AShooterPlayerController* PC = GetValidObjectAs<AShooterPlayerController>(Controller);
		if (PC && DamageEvent.DamageTypeClass)
		{
			UShooterDamageType *DamageType = Cast<UShooterDamageType>(DamageEvent.DamageTypeClass->GetDefaultObject());
			if (DamageType && DamageType->KilledForceFeedback && PC->IsVibrationEnabled())
			{
				FForceFeedbackParameters FeedbackParameters;
				FeedbackParameters.Tag = TEXT("Damage");
				FeedbackParameters.bLooping = false;
				FeedbackParameters.bIgnoreTimeDilation = false;
				FeedbackParameters.bPlayWhilePaused = false;
				
				PC->ClientPlayForceFeedback(DamageType->KilledForceFeedback, FeedbackParameters);
			}
		}
	}

	// cannot use IsLocallyControlled here, because even local client's controller may be NULL here
	if (GetNetMode() != NM_DedicatedServer && DeathSound && Mesh1P && Mesh1P->IsVisible())
	{
		UGameplayStatics::PlaySoundAtLocation(this, DeathSound, GetActorLocation());
	}

	SpawnWeaponPickup();

	// remove all weapons
	DestroyInventory();

	// switch back to 3rd person view
	UpdatePawnMeshes();

	DetachFromControllerPendingDestroy();
	StopAllAnimMontages();

	if (LowHealthWarningPlayer && LowHealthWarningPlayer->IsPlaying())
	{
		LowHealthWarningPlayer->Stop();
	}

	if (RunLoopAC)
	{
		RunLoopAC->Stop();
	}

	if (GetMesh())
	{
		static FName CollisionProfileName(TEXT("Ragdoll"));
		GetMesh()->SetCollisionProfileName(CollisionProfileName);
		GetMesh()->SetCollisionResponseToChannel(COLLISION_WEAPON, ECR_Ignore);
	}
	SetActorEnableCollision(true);

	// Death anim
	float DeathAnimDuration = PlayAnimMontage(DeathAnim);

	// Ragdoll
	if (DeathAnimDuration > 0.f)
	{
		// Trigger ragdoll a little before the animation early so the character doesn't
		// blend back to its normal position.
		const float TriggerRagdollTime = DeathAnimDuration - 0.7f;

		// Enable blend physics so the bones are properly blending against the montage.
		GetMesh()->bBlendPhysics = true;

		// Use a local timer handle as we don't need to store it for later but we don't need to look for something to clear
		FTimerHandle TimerHandle;
		GetWorldTimerManager().SetTimer(TimerHandle, this, &AShooterCharacter::SetRagdollPhysics, FMath::Max(0.1f, TriggerRagdollTime), false);
	}
	else
	{
		SetRagdollPhysics();
	}

	// disable collisions on capsule
	GetCapsuleComponent()->SetCollisionEnabled(ECollisionEnabled::NoCollision);
	GetCapsuleComponent()->SetCollisionResponseToAllChannels(ECR_Ignore);

	if (DissolveCurve)
	{
		BodyDissolveElapsedTime = .0f;
		BodyDissolveStarted = true;
	}
}

void AShooterCharacter::PlayHit(float DamageTaken, struct FDamageEvent const& DamageEvent, class APawn* PawnInstigator, class AActor* DamageCauser)
{
	if (GetLocalRole() == ROLE_Authority)
	{
		ReplicateHit(DamageTaken, DamageEvent, PawnInstigator, DamageCauser, false);

		// play the force feedback effect on the client player controller
		AShooterPlayerController* PC = GetValidObjectAs<AShooterPlayerController>(Controller);
		if (PC && DamageEvent.DamageTypeClass)
		{
			UShooterDamageType *DamageType = Cast<UShooterDamageType>(DamageEvent.DamageTypeClass->GetDefaultObject());
			if (DamageType && DamageType->HitForceFeedback && PC->IsVibrationEnabled())
			{
				FForceFeedbackParameters FeedbackParameters;
				FeedbackParameters.Tag = TEXT("Damage");
				FeedbackParameters.bLooping = false;
				FeedbackParameters.bIgnoreTimeDilation = false;
				FeedbackParameters.bPlayWhilePaused = false;
				
				PC->ClientPlayForceFeedback(DamageType->HitForceFeedback, FeedbackParameters);
			}
		}
	}

	if (DamageTaken > 0.f)
	{
		ApplyDamageMomentum(DamageTaken, DamageEvent, PawnInstigator, DamageCauser);
	}

	AShooterPlayerController* MyPC = GetValidObjectAs<AShooterPlayerController>(Controller);
	AShooterHUD* MyHUD = MyPC ? GetValidObjectAs<AShooterHUD>(MyPC->GetHUD()) : NULL;
	if (MyHUD)
	{
		MyHUD->NotifyWeaponHit(DamageTaken, DamageEvent, PawnInstigator);
	}

	//
	auto EntityInstance = Instance ? Instance->GetEntity<UCharacterItemEntity>() : nullptr;
	if (EntityInstance && EntityInstance->GetCharacterProperty().GetHitSound())
	{
		UGameplayStatics::SpawnSoundAttached(EntityInstance->GetCharacterProperty().GetHitSound(), GetMesh(), NAME_None, FVector(ForceInit), FRotator::ZeroRotator, EAttachLocation::KeepRelativeOffset, true, 1.0f, 1.0f, 0.0f, nullptr, HitSoundConcurrency);
	}
}

void AShooterCharacter::OnRep_GiveHitInfo()
{
	if (IsLocallyControlled() && GiveHitInfo.PawnDamaged.IsValid())
	{
		if (!IsRunningDedicatedServer()) LastGiveHitInfoTime = FSlateApplication::Get().GetCurrentTime();

		AShooterWeapon* DamageDealer = GetValidObjectAs<AShooterWeapon>(GiveHitInfo.DamageCauser);
		AShooterPlayerController* InstigatorPC = GetValidObjectAs<AShooterPlayerController>(Controller);
		AShooterHUD* InstigatorHUD = InstigatorPC ? GetValidObjectAs<AShooterHUD>(InstigatorPC->GetHUD()) : NULL;
		if (InstigatorHUD)
		{
			if (DamageDealer && DamageDealer->GetWeaponDamage() * 1.1f < GiveHitInfo.Damage)
			{
				InstigatorHUD->NotifyEnemyHit(GiveHitInfo.Damage, GiveHitInfo.PawnDamaged->GetActorLocation() + FVector(0, 0, GetSimpleCollisionHalfHeight() / 2), true, !GiveHitInfo.PawnDamaged->IsAlive());
			}
			else
			{
				InstigatorHUD->NotifyEnemyHit(GiveHitInfo.Damage, GiveHitInfo.PawnDamaged->GetActorLocation() + FVector(0, 0, GetSimpleCollisionHalfHeight() / 2), false, !GiveHitInfo.PawnDamaged->IsAlive());
			}
		}

		if (FMath::IsNearlyZero(GiveHitInfo.PawnDamaged->GetCurrentArmour()) == false)
		{
			GiveHitInfo.PawnDamaged->SetBodyColorFlash(ArmorFlashCurve, true);
		}
		else
		{
			GiveHitInfo.PawnDamaged->SetBodyColorFlash(HealthFlashCurve, false);
		}
	}
}

void AShooterCharacter::SetRagdollPhysics()
{
	bool bInRagdoll = false;

	if (IsPendingKill())
	{
		bInRagdoll = false;
	}
	else if (!GetMesh() || !GetMesh()->GetPhysicsAsset())
	{
		bInRagdoll = false;
	}
	else
	{
		// initialize physics/etc

		GetMesh()->SetCollisionResponseToChannel(COLLISION_FRIENDLY, ECR_Ignore);
		GetMesh()->SetCollisionResponseToChannel(COLLISION_ENEMY, ECR_Ignore);
		GetMesh()->SetCollisionResponseToChannel(ECC_Pawn, ECR_Ignore);

		GetMesh()->SetSimulatePhysics(true);
		GetMesh()->WakeAllRigidBodies();
		GetMesh()->bBlendPhysics = true;

		bInRagdoll = true;
	}

	GetCharacterMovement()->StopMovementImmediately();
	GetCharacterMovement()->DisableMovement();
	GetCharacterMovement()->SetComponentTickEnabled(false);

	if (bInRagdoll)
	{
		if (!DissolveCurve) SetLifeSpan(3.0f);
	}
	else
	{
		// hide and set short lifespan
		TurnOff();
		//SetActorHiddenInGame(true);
		if (!DissolveCurve) SetLifeSpan(3.0f);
	}
}



void AShooterCharacter::ReplicateHit(float Damage, struct FDamageEvent const& DamageEvent, class APawn* PawnInstigator, class AActor* DamageCauser, bool bKilled)
{
	const float TimeoutTime = GetWorld()->GetTimeSeconds() + 1.5f;

	FDamageEvent const& LastDamageEvent = LastTakeHitInfo.GetDamageEvent();
	
	if ((PawnInstigator == LastTakeHitInfo.PawnInstigator.Get()) && (LastDamageEvent.DamageTypeClass == LastTakeHitInfo.DamageTypeClass) && (LastTakeHitTimeTimeout == TimeoutTime))
	{
		// otherwise, accumulate damage done this frame
		Damage += LastTakeHitInfo.ActualDamage;
	}

	FTakeHitInfo TakeHitInfo;

	TakeHitInfo.ActualDamage = Damage;
	TakeHitInfo.PawnInstigator = GetValidObjectAs<AShooterCharacter>(PawnInstigator);
	TakeHitInfo.DamageCauser = DamageCauser;
	TakeHitInfo.SetDamageEvent(DamageEvent);
	TakeHitInfo.bKilled = bKilled;
	TakeHitInfo.EnsureReplication();

	LastTakeHitInfo = TakeHitInfo;
	LastTakeHitTimeTimeout = TimeoutTime;

	if (IsValidObject(TakeHitInfo.PawnInstigator))
	{
		TakeHitInfo.PawnInstigator->GiveHitInfo.Damage = Damage;
		TakeHitInfo.PawnInstigator->GiveHitInfo.DamageCauser = DamageCauser;
		TakeHitInfo.PawnInstigator->GiveHitInfo.PawnDamaged = this;
		TakeHitInfo.PawnInstigator->GiveHitInfo.EnsureReplication();
	}
}

void AShooterCharacter::OnRep_LastTakeHitInfo()
{
	if (LastTakeHitInfo.bKilled)
	{
		OnDeath(LastTakeHitInfo.ActualDamage, LastTakeHitInfo.GetDamageEvent(), LastTakeHitInfo.PawnInstigator.Get(), LastTakeHitInfo.DamageCauser.Get());
	}
	else
	{
		PlayHit(LastTakeHitInfo.ActualDamage, LastTakeHitInfo.GetDamageEvent(), LastTakeHitInfo.PawnInstigator.Get(), LastTakeHitInfo.DamageCauser.Get());
	}
}

//Pawn::PlayDying sets this lifespan, but when that function is called on client, dead pawn's role is still SimulatedProxy despite bTearOff being true. 
void AShooterCharacter::TornOff()
{
	SetLifeSpan(5.f);
}

bool AShooterCharacter::IsMoving()
{
	return FMath::Abs(GetLastMovementInputVector().Size()) > 0.f;
}

//////////////////////////////////////////////////////////////////////////
// Inventory

void AShooterCharacter::SpawnDefaultInventory()
{
	if (GetLocalRole() < ROLE_Authority)
	{
		return;
	}

	int32 NumWeaponClasses = DefaultInventoryClasses.Num();
	for (int32 i = 0; i < NumWeaponClasses; i++)
	{
		if (DefaultInventoryClasses[i])
		{
			FActorSpawnParameters SpawnInfo;
			SpawnInfo.SpawnCollisionHandlingOverride = ESpawnActorCollisionHandlingMethod::AlwaysSpawn;
			AShooterWeapon* NewWeapon = GetWorld()->SpawnActor<AShooterWeapon>(DefaultInventoryClasses[i], SpawnInfo);
			AddWeapon(NewWeapon);
		}
	}

	// equip first weapon in inventory
	if (Inventory.Num() > 0 && GetWeapon() == nullptr)
	{
		EquipWeapon(Inventory[0]);
	}
}

void AShooterCharacter::SpawnWeaponPickup()
{
	//===============================
	if (GetLocalRole() < ROLE_Authority)
	{
		return;
	}
	
	if (CurrentWeapon && CurrentWeapon->IsValidLowLevel() && CurrentWeapon->GetItemType() != EGameItemType::Pistol)
	{
		//===============================
		FActorSpawnParameters parameters;
		parameters.SpawnCollisionHandlingOverride = ESpawnActorCollisionHandlingMethod::AlwaysSpawn;

		auto subclass = FGameSingletonExtensions::FindSubClassByName(AShooterPickup_Weapon::PICKUP_OBJECT_CLASS_NAME, AShooterPickup_Weapon::StaticClass());
		auto pickup = GetWorld()->SpawnActor<AShooterPickup_Weapon>(subclass, GetTransform(), parameters);
		if (pickup)
		{
			pickup->SetModelId(CurrentWeapon->GetModelId());
			pickup->SetCountAmmo(CurrentWeapon->GetAmmo());
			pickup->SetLevel(CurrentWeapon->GetLevel());
		}
	}
}

void AShooterCharacter::DestroyInventory()
{
	if (GetLocalRole() < ROLE_Authority)
	{
		return;
	}

	OverlapedPickups.Empty();

	//===============================================
	//	Удаляем временные предметы
	if (auto arsenal = GetArsenalComponent())
	{
		auto &items = arsenal->GetRawItems();
		items.RemoveAll([&](const UBasicPlayerItem* InItem)
		{
			if (auto item = GetValidObject(InItem))
			{
				return item->GetState() == EPlayerItemState::Temporary;
			}
			return true;
		});
	}

	// remove all weapons from inventory and destroy them
	for (int32 i = Inventory.Num() - 1; i >= 0; i--)
	{
		AShooterWeapon* Weapon = Inventory[i];
		if (Weapon)
		{
			RemoveWeapon(Weapon);
			Weapon->Destroy();
		}
	}
}

void AShooterCharacter::AddWeapon(AShooterWeapon* Weapon)
{
	if (Weapon && GetLocalRole() == ROLE_Authority)
	{
		Weapon->OnEnterInventory(this);
		Inventory.AddUnique(Weapon);
	}
}

void AShooterCharacter::RemoveWeapon(AShooterWeapon* Weapon)
{
	if (Weapon && GetLocalRole() == ROLE_Authority)
	{
		Weapon->OnLeaveInventory();
		Inventory.RemoveSingle(Weapon);
	}
}

AShooterWeapon* AShooterCharacter::FindWeapon(const int32& InModelId) const
{
	for(const auto WeaponIt : Inventory)
	{
		if(const auto weapon = GetValidObject(WeaponIt))
		{
			if(weapon->GetModelId() == InModelId)
			{
				return weapon;
			}
		}
	}

	return nullptr;
}

AShooterWeapon* AShooterCharacter::FindWeapon(TSubclassOf<AShooterWeapon> WeaponClass)
{
	for (int32 i = 0; i < Inventory.Num(); i++)
	{
		if (Inventory[i] && Inventory[i]->IsA(WeaponClass))
		{
			return Inventory[i];
		}
	}

	return nullptr;
}

void AShooterCharacter::EquipWeapon(AShooterWeapon* Weapon, bool IsDefault)
{
	if (Weapon)
	{
		if (GetLocalRole() == ROLE_Authority)
		{
			SetCurrentWeapon(Weapon, CurrentWeapon);

			if (IsDefault)
			{
				if (auto MyPS = GetBasePlayerState<AShooterPlayerState>())
				{
					if (auto InventoryComp = MyPS->GetInventoryComponent())
					{
						InventoryComp->RequestChangeDefaultWeapon(Weapon->GetInstance());
					}
				}
			}
		}
		else
		{
			ServerEquipWeapon(Weapon, IsDefault);
		}
	}
}

bool AShooterCharacter::ServerEquipWeapon_Validate(AShooterWeapon* Weapon, bool IsDefault)
{
	return true;
}

void AShooterCharacter::ServerEquipWeapon_Implementation(AShooterWeapon* Weapon, bool IsDefault)
{
	EquipWeapon(Weapon, IsDefault);
}

void AShooterCharacter::OnRep_CurrentWeapon(AShooterWeapon* LastWeapon)
{
	SetCurrentWeapon(GetValidObject(CurrentWeapon), GetValidObject(LastWeapon));
}

void AShooterCharacter::SetCurrentWeapon(AShooterWeapon* NewWeapon, AShooterWeapon* LastWeapon)
{
	AShooterWeapon* LocalLastWeapon = nullptr;

	if (LastWeapon != nullptr)
	{
		LocalLastWeapon = LastWeapon;
	}
	else if (NewWeapon != CurrentWeapon)
	{
		LocalLastWeapon = CurrentWeapon;
	}

	// unequip previous
	if (LocalLastWeapon)
	{
		LocalLastWeapon->OnUnEquip();
	}

	CurrentWeapon = NewWeapon;

	// equip new one
	if (NewWeapon)
	{
		NewWeapon->SetOwningPawn(this);	// Make sure weapon's MyPawn is pointing back to us. During replication, we can't guarantee APawn::CurrentWeapon will rep after AWeapon::MyPawn!

		NewWeapon->OnEquip();

		if (AutoTargeting && AutoTargeting->IsValidLowLevel())
		{
			if (auto WeaponProperty = NewWeapon->GetWeaponConfiguration())
			{
				AutoTargeting->SetTargetingPriorityDistance(WeaponProperty->Range);
				AutoTargeting->SetTargetingAlertDistance(WeaponProperty->Range / 5);				
			}
		}
	}
}


//////////////////////////////////////////////////////////////////////////
// Weapon usage

void AShooterCharacter::SwitchWeaponToPreview()
{
	EquipWeapon(PreviewWeapon);
}


bool AShooterCharacter::SwitchWeaponToPistol()
{
	const auto pistol = FindWeaponBySlot(EGameItemType::Pistol);
	if (pistol != nullptr && pistol != CurrentWeapon)
	{
		PreviewWeapon = CurrentWeapon;
		pistol->ForceReloadWeapon();
		EquipWeapon(pistol);		
		return true;
	}
	return false;
}

AShooterWeapon* AShooterCharacter::FindWeaponBySlot(const EGameItemType& InSlotId) const
{
	const auto result = Inventory.FindByPredicate([&](const AShooterWeapon* InWeapon)
	{
		if (const auto weapon = GetValidObject(InWeapon))
		{
			return weapon->GetItemType() == InSlotId;
		}
		return false;
	});

	if (result)
	{
		return GetValidObject(*result);
	}
	return nullptr;
}

void AShooterCharacter::StartWeaponFire()
{
	if (!bWantsToFire)
	{
		bWantsToFire = true;
		if (CurrentWeapon)
		{
			if (CurrentWeapon->GetCurrentAmmo() <= 0)
			{
				if (SwitchWeaponToPistol() == false)
				{
					CurrentWeapon->StartFire();
				}
			}
			else
			{
				CurrentWeapon->StartFire();
			}
		}
	}
}

void AShooterCharacter::StopWeaponFire()
{
	if (bWantsToFire)
	{
		bWantsToFire = false;
		if (CurrentWeapon)
		{
			CurrentWeapon->StopFire();
		}
	}
}

bool AShooterCharacter::CanFire() const
{
	bool bUseKnife = false;
	if (auto MyKnife = GetValidObject(ShooterKnife))
	{
		bUseKnife = MyKnife->IsActivated();
	}

	return IsAlive() && !bUseKnife;
}

bool AShooterCharacter::CanReload() const
{
	return true;
}

void AShooterCharacter::SetTargeting(bool bNewTargeting)
{
	bIsTargeting = bNewTargeting;

	if (TargetingSound)
	{
		UGameplayStatics::SpawnSoundAttached(TargetingSound, GetRootComponent());
	}

	if (GetLocalRole() < ROLE_Authority)
	{
		ServerSetTargeting(bNewTargeting);
		if (GetWeapon() && GetWeapon()->IsValidLowLevel())
		{
			GetWeapon()->ToggleAimingParams(bNewTargeting);
		}
	}
}

bool AShooterCharacter::ServerSetTargeting_Validate(bool bNewTargeting)
{
	return true;
}

void AShooterCharacter::ServerSetTargeting_Implementation(bool bNewTargeting)
{
	SetTargeting(bNewTargeting);
}

//////////////////////////////////////////////////////////////////////////
// Movement

void AShooterCharacter::SetRunning(bool bNewRunning, bool bToggle)
{
	bWantsToRun = bNewRunning;
	bWantsToRunToggled = bNewRunning && bToggle;

	if (bWantsToRun == false)
	{
		LastRunningTime = GetWorldTimeSeconds();
	}

	if (GetLocalRole() < ROLE_Authority)
	{
		ServerSetRunning(bNewRunning, bToggle);
	}
}

bool AShooterCharacter::ServerSetRunning_Validate(bool bNewRunning, bool bToggle)
{
	return true;
}

void AShooterCharacter::ServerSetRunning_Implementation(bool bNewRunning, bool bToggle)
{
	SetRunning(bNewRunning, bToggle);
}

void AShooterCharacter::UpdateRunSounds()
{
	const bool bIsRunSoundPlaying = RunLoopAC != nullptr && RunLoopAC->IsActive();
	const bool bWantsRunSoundPlaying = IsRunning() && IsMoving();

	// Don't bother playing the sounds unless we're running and moving.
	//if (!bIsRunSoundPlaying && bWantsRunSoundPlaying)
	//{
	//	if (RunLoopAC != nullptr)
	//	{
	//		RunLoopAC->Play();
	//	}
	//	else if (RunLoopSound != nullptr)
	//	{
	//		RunLoopAC = UGameplayStatics::SpawnSoundAttached(RunLoopSound, GetRootComponent());
	//		if (RunLoopAC != nullptr)
	//		{
	//			RunLoopAC->bAutoDestroy = false;
	//		}
	//	}
	//}
	//else if (bIsRunSoundPlaying && !bWantsRunSoundPlaying)
	//{
	//	RunLoopAC->Stop();
	//	if (RunStopSound != nullptr)
	//	{
	//		UGameplayStatics::SpawnSoundAttached(RunStopSound, GetRootComponent());
	//	}
	//}
}

//////////////////////////////////////////////////////////////////////////
// Animations

float AShooterCharacter::PlayAnimMontage(class UAnimMontage* AnimMontage, float InPlayRate, FName StartSectionName)
{
	USkeletalMeshComponent* UseMesh = GetPawnMesh();
	if (AnimMontage && UseMesh && UseMesh->AnimScriptInstance)
	{
		return UseMesh->AnimScriptInstance->Montage_Play(AnimMontage, InPlayRate);
	}

	return 0.0f;
}

void AShooterCharacter::StopAnimMontage(class UAnimMontage* AnimMontage)
{
	USkeletalMeshComponent* UseMesh = GetPawnMesh();
	if (
		IsValidObject(AnimMontage) &&
		IsValidObject(UseMesh) &&
		IsValidObject(UseMesh->AnimScriptInstance) &&
		UseMesh->AnimScriptInstance->Montage_IsPlaying(AnimMontage)
		)
	{
		UseMesh->AnimScriptInstance->Montage_Stop(AnimMontage->BlendOut.GetBlendTime(), AnimMontage);
	}
}

void AShooterCharacter::StopAllAnimMontages()
{
	USkeletalMeshComponent* UseMesh = GetPawnMesh();
	if 
		(
			IsValidObject(UseMesh) && 
			IsValidObject(UseMesh->AnimScriptInstance)
		)
	{
		UseMesh->AnimScriptInstance->Montage_Stop(0.0f);
	}
}


//////////////////////////////////////////////////////////////////////////
// Input

void AShooterCharacter::MoveForward(float Val)
{
	if (Controller && Val != 0.f)
	{
		// Limit pitch when walking or falling
		const bool bLimitRotation = (GetCharacterMovement()->IsMovingOnGround() || GetCharacterMovement()->IsFalling());
		const FRotator Rotation = bLimitRotation ? GetActorRotation() : Controller->GetControlRotation();
		const FVector Direction = FRotationMatrix(Rotation).GetScaledAxis(EAxis::X);
		AddMovementInput(Direction, Val);
	}
}

void AShooterCharacter::MoveRight(float Val)
{
	if (Val != 0.f)
	{
		const FQuat Rotation = GetActorQuat();
		const FVector Direction = FQuatRotationMatrix(Rotation).GetScaledAxis(EAxis::Y);
		AddMovementInput(Direction, Val);
	}
}

void AShooterCharacter::MoveUp(float Val)
{
	if (Val != 0.f)
	{
		// Not when walking or falling.
		if (GetCharacterMovement()->IsMovingOnGround() || GetCharacterMovement()->IsFalling())
		{
			return;
		}

		AddMovementInput(FVector::UpVector, Val);
	}
}

void AShooterCharacter::TurnAtRate(float Val)
{
	// calculate delta for this frame from the rate information
	AddControllerYawInput(Val * BaseTurnRate * GetWorld()->GetDeltaSeconds());
}

void AShooterCharacter::TurnAtValue(float InValue)
{
	AddControllerYawInput(InValue);
}

void AShooterCharacter::LookUpAtRate(float Val)
{
	// calculate delta for this frame from the rate information
	AddControllerPitchInput(Val * BaseLookUpRate * GetWorld()->GetDeltaSeconds());
}

void AShooterCharacter::LookUpAtValue(float InValue)
{
	AddControllerPitchInput(InValue);
}

ATeamInfo* AShooterCharacter::GetTeam() const
{
	if (auto CastedState = GetValidObjectAs<AShooterPlayerState>(GetPlayerState()))
	{
		return CastedState->GetTeam();
	}

	return nullptr;
}

uint8 AShooterCharacter::GetTeamNum() const
{
	if (auto CastedState = GetValidObjectAs<AShooterPlayerState>(GetPlayerState()))
	{
		return CastedState->GetTeamNum();
	}

	return NullTeam;
}


bool AShooterCharacter::OnAidActivate_Validate()
{
	return true;
}

void AShooterCharacter::OnAidActivate_Implementation()
{
	if (auto aid = GetAid())
	{
		aid->StartProcess();
	}
}

bool AShooterCharacter::OnGrenadeActivate_Validate()
{
	return true;
}

void AShooterCharacter::OnGrenadeActivate_Implementation()
{
	if (auto grenade = GetGrenade())
	{
		grenade->StartProcess();
	}
}

bool AShooterCharacter::OnKnifeActivate_Validate() { return true; }
void AShooterCharacter::OnKnifeActivate_Implementation()
{
	if (auto MyKnife = GetValidObject(ShooterKnife))
	{
		MyKnife->Activate();
	}
}

bool AShooterCharacter::OnWeaponPickUp_Validate()
{
	return true;
}

void AShooterCharacter::OnWeaponPickUp_Implementation()
{
	if (auto NearlyPickup = GetNearlyOverlapedPickup())
	{
		NearlyPickup->PickupOnTouch(this);
	}
}

void AShooterCharacter::OnStartFire()
{
	AShooterPlayerController* MyPC = GetValidObjectAs<AShooterPlayerController>(Controller);
	if (MyPC && MyPC->IsGameInputAllowed())
	{
		if (IsRunning())
		{
			SetRunning(false, false);
		}
		//else if (LastRunningTime + 0.4f < GetWorldTimeSeconds())
		{
			StartWeaponFire();
		}
	}
}

void AShooterCharacter::OnStopFire()
{
	StopWeaponFire();
}

void AShooterCharacter::OnClientStartTargeting()
{
	OnStartTargeting();
	FShooterGameAnalytics::RecordDesignEvent("UI:Game:StartTargeting");
}

void AShooterCharacter::OnStartTargeting()
{
	AShooterPlayerController* MyPC = GetValidObjectAs<AShooterPlayerController>(Controller);
	if (MyPC && MyPC->IsGameInputAllowed())
	{
		if (IsRunning())
		{
			SetRunning(false, false);
		}
		SetTargeting(true);
	}
}

void AShooterCharacter::OnStopTargeting()
{
	SetTargeting(false);
}

TArray<AShooterWeapon*> AShooterCharacter::GetAvalibleWeapons() const
{
	return Inventory.FilterByPredicate([&](const AShooterWeapon* InWeapon)
	{
		return GetValidObject(InWeapon) && InWeapon->GetItemType() != EGameItemType::Pistol && InWeapon->GetItemType() != EGameItemType::Knife;
	});
}	


void AShooterCharacter::OnNextWeapon()
{
	AShooterPlayerController* MyPC = GetValidObjectAs<AShooterPlayerController>(Controller);
	if (MyPC && MyPC->IsGameInputAllowed())
	{
		const auto AvalibleWeapons = GetAvalibleWeapons();
		if (!!AvalibleWeapons.Num() && (CurrentWeapon == nullptr || CurrentWeapon->GetCurrentState() != EWeaponState::Equipping))
		{
			const int32 CurrentWeaponIdx = AvalibleWeapons.IndexOfByKey(CurrentWeapon);
			AShooterWeapon* NextWeapon = AvalibleWeapons[(CurrentWeaponIdx + 1) % AvalibleWeapons.Num()];
			EquipWeapon(NextWeapon);
		}
	}
}

void AShooterCharacter::OnPrevWeapon()
{
	AShooterPlayerController* MyPC = GetValidObjectAs<AShooterPlayerController>(Controller);
	if (MyPC && MyPC->IsGameInputAllowed())
	{
		const auto AvalibleWeapons = GetAvalibleWeapons();

		if (!!AvalibleWeapons.Num() && (CurrentWeapon == nullptr || CurrentWeapon->GetCurrentState() != EWeaponState::Equipping))
		{
			const int32 CurrentWeaponIdx = AvalibleWeapons.IndexOfByKey(CurrentWeapon);
			AShooterWeapon* PrevWeapon = AvalibleWeapons[FMath::Max(0, CurrentWeaponIdx - 1 + AvalibleWeapons.Num()) % Inventory.Num()];
			EquipWeapon(PrevWeapon);
		}
	}
}

void AShooterCharacter::OnClientReload()
{
	OnReload();
	FShooterGameAnalytics::RecordDesignEvent("UI:Game:Reload");
}


void AShooterCharacter::OnReload()
{
	AShooterPlayerController* MyPC = GetValidObjectAs<AShooterPlayerController>(Controller);
	if (MyPC && MyPC->IsGameInputAllowed())
	{
		if (CurrentWeapon)
		{
			CurrentWeapon->StartReload();
		}
	}
}

void AShooterCharacter::OnStartRunning()
{
	AShooterPlayerController* MyPC = GetValidObjectAs<AShooterPlayerController>(Controller);
	if (MyPC && MyPC->IsGameInputAllowed())
	{
		if (IsTargeting())
		{
			SetTargeting(false);
		}
		StopWeaponFire();
		SetRunning(true, false);
	}
}

void AShooterCharacter::OnStartRunningToggle()
{
	AShooterPlayerController* MyPC = GetValidObjectAs<AShooterPlayerController>(Controller);
	if (MyPC && MyPC->IsGameInputAllowed())
	{
		if (IsTargeting())
		{
			SetTargeting(false);
		}
		StopWeaponFire();
		SetRunning(true, true);
	}
}

void AShooterCharacter::OnStopRunning()
{
	SetRunning(false, false);
}

bool AShooterCharacter::IsRunning() const
{
	if (!GetCharacterMovement())
	{
		return false;
	}

	return (bWantsToRun || bWantsToRunToggled) && !GetVelocity().IsZero() && (GetVelocity().GetSafeNormal2D() | GetActorForwardVector()) > -0.1;
}

void AShooterCharacter::UpdateMeshLag(float InDeltaTime)
{
	if (bIsInitMeshLag)
	{
		const FRotator CurrentRot = GetControlRotation();
		MeshLag_DeltaRot = FRotator::ZeroRotator;

		if (MeshLag_OldRot != CurrentRot)
		{			
			MeshLag_DeltaRot = (MeshLag_OldRot - CurrentRot).GetNormalized();

			GEngine->AddOnScreenDebugMessage(55, 10.0f, FColorList::VioletRed, FString::Printf(TEXT("UpdateMeshLag >> [Source]MeshLag_DeltaRot: %s"), *MeshLag_DeltaRot.ToString()));

			MeshLag_DeltaRot.Yaw = FMath::Clamp(MeshLag_DeltaRot.Yaw, -MeshLag_DeltaLimits.Yaw, MeshLag_DeltaLimits.Yaw);
			MeshLag_DeltaRot.Pitch = FMath::Clamp(MeshLag_DeltaRot.Pitch, -MeshLag_DeltaLimits.Pitch, MeshLag_DeltaLimits.Pitch);
			MeshLag_DeltaRot.Roll = FMath::Clamp(MeshLag_DeltaRot.Roll, -MeshLag_DeltaLimits.Roll, MeshLag_DeltaLimits.Roll);

			GEngine->AddOnScreenDebugMessage(56, 10.0f, FColorList::VioletRed, FString::Printf(TEXT("UpdateMeshLag >> [Clamped]MeshLag_DeltaRot: %s"), *MeshLag_DeltaRot.ToString()));

			MeshLag_OldRot = CurrentRot;
		}

		// MeshLag_TargetingMultipler

		const float lTargetingMultipler = IsTargeting() ? MeshLag_TargetingMultipler : 1.0f;
		const FRotator FinalRot = FRotator(MeshLag_DeltaRot.Pitch * MeshLag_LookAmount.Y, MeshLag_DeltaRot.Yaw * MeshLag_LookAmount.X, MeshLag_DeltaRot.Yaw * MeshLag_LookAmount.X);

		if (auto ArmComponent = GetValidObject(ShooterArmComponent))
		{
			const FRotator CombinedRot = MeshLag_InitRot + FinalRot;//     FRotator(MeshLag_InitRot.Pitch + FinalRot.Pitch, MeshLag_InitRot.Yaw + FinalRot.Yaw, MeshLag_InitRot.Roll + FinalRot.Roll);
			const FVector2D RotAlpha = FVector2D(MeshLag_SmoothAmount.X * InDeltaTime, MeshLag_SmoothAmount.Y * InDeltaTime) * lTargetingMultipler;

			const FRotator CompRot = ArmComponent->GetRelativeRotation();

			const FRotator LerpedRot = FRotator
			(
				FMath::Lerp(CompRot.Pitch, CombinedRot.Pitch, RotAlpha.Y),
				FMath::Lerp(CompRot.Yaw, CombinedRot.Yaw, RotAlpha.X),
				FMath::Lerp(CompRot.Roll, CombinedRot.Roll, RotAlpha.X)
			);

			ArmComponent->SetRelativeLocationAndRotation(MeshLag_InitLoc, LerpedRot);			
		}
	}
}

void AShooterCharacter::Tick(float DeltaSeconds)
{
	Super::Tick(DeltaSeconds);

	if (bWantsToRunToggled && !IsRunning())
	{
		SetRunning(false, false);
	}

	if (IsSpawnProtected() && AntiSKFlashCurve)
	{
		static FName NAME_SpawnProtectionPct(TEXT("SpawnProtectionPct"));

		for (auto MI : MeshMIDs)
		{
			if (MI != nullptr)
			{
				MI->SetScalarParameterValue(NAME_SpawnProtectionPct, AntiSKFlashCurve->GetFloatValue(GetSpawnProtectionProgress()));
			}
		}
	}

	if (BodyColorFlashCurve != nullptr)
	{
		UpdateBodyColorFlash(DeltaSeconds);
	}

	if (DissolveCurve && BodyDissolveStarted)
	{
		UpdateDissolve(DeltaSeconds);
	}

	//if (GEngine->UseSound())
	//{
	//	if (LowHealthSound)
	//	{
	//		if ((Health.X > 0 && Health.X < Health.Y * LowHealthPercentage) && (!LowHealthWarningPlayer || !LowHealthWarningPlayer->IsPlaying()))
	//		{
	//			LowHealthWarningPlayer = UGameplayStatics::SpawnSoundAttached(LowHealthSound, GetRootComponent(),
	//				NAME_None, FVector(ForceInit), EAttachLocation::KeepRelativeOffset, true);
	//			LowHealthWarningPlayer->SetVolumeMultiplier(0.0f);
	//		}
	//		else if ((Health.X > Health.Y * LowHealthPercentage || Health.X < 0) && LowHealthWarningPlayer && LowHealthWarningPlayer->IsPlaying())
	//		{
	//			LowHealthWarningPlayer->Stop();
	//		}
	//		if (LowHealthWarningPlayer && LowHealthWarningPlayer->IsPlaying())
	//		{
	//			const float MinVolume = 0.3f;
	//			const float VolumeMultiplier = (1.0f - (Health.X / (Health.Y * LowHealthPercentage)));
	//			LowHealthWarningPlayer->SetVolumeMultiplier(MinVolume + (1.0f - MinVolume) * VolumeMultiplier);
	//		}
	//	}

	//	UpdateRunSounds();
	//}

	const APlayerController* PC = GetValidObjectAs<APlayerController>(GetController());
	const bool bLocallyControlled = (PC ? PC->IsLocalController() : false);
	const uint32 UniqueID = GetUniqueID();
	FAudioThread::RunCommandOnAudioThread([UniqueID, bLocallyControlled]()
	{
	    USoundNodeLocalPlayer::GetLocallyControlledActorCache().Add(UniqueID, bLocallyControlled);
	});
	
	TArray<FVector> PointsToTest;
	BuildPauseReplicationCheckPoints(PointsToTest);

	if (NetVisualizeRelevancyTestPoints == 1)
	{
		for (FVector PointToTest : PointsToTest)
		{
			DrawDebugSphere(GetWorld(), PointToTest, 10.0f, 8, FColor::Red);
		}
	}

	if (IsLocallyControlled())
	{
		if (AutoTargeting && AutoTargeting->IsValidLowLevel())
		{
			if (auto MySettings = UShooterGameUserSettings::Get())
			{
				const bool bIsEnable = MySettings->IsEnabledAutoTargeting();
				AutoTargeting->SetActive(bIsEnable);
			}
		}

		if (IsAlive())
		{
			UpdateMeshLag(DeltaSeconds);
		}
	}
}

void AShooterCharacter::BeginDestroy()
{
	Super::BeginDestroy();

	if (!GExitPurge)
	{
		const uint32 UniqueID = GetUniqueID();
		FAudioThread::RunCommandOnAudioThread([UniqueID]()
		{
			USoundNodeLocalPlayer::GetLocallyControlledActorCache().Remove(UniqueID);
		});
	}
}

void AShooterCharacter::OnStartJump()
{
	AShooterPlayerController* MyPC = GetValidObjectAs<AShooterPlayerController>(Controller);
	if (MyPC && MyPC->IsGameInputAllowed())
	{
		bPressedJump = true;
	}
}

void AShooterCharacter::OnStopJump()
{
	bPressedJump = false;
	StopJumping();
}

//////////////////////////////////////////////////////////////////////////
// Replication

void AShooterCharacter::PreReplication(IRepChangedPropertyTracker & ChangedPropertyTracker)
{
	Super::PreReplication(ChangedPropertyTracker);

	// Only replicate this property for a short duration after it changes so join in progress players don't get spammed with fx when joining late
	//DOREPLIFETIME_ACTIVE_OVERRIDE(AShooterCharacter, LastTakeHitInfo, GetWorld() && GetWorld()->GetTimeSeconds() < LastTakeHitTimeTimeout);
}

void AShooterCharacter::GetLifetimeReplicatedProps(TArray< FLifetimeProperty > & OutLifetimeProps) const
{
	Super::GetLifetimeReplicatedProps(OutLifetimeProps);

	// only to local owner: weapon change requests are locally instigated, other clients don't need it
	DOREPLIFETIME_CONDITION(AShooterCharacter, Inventory, COND_OwnerOnly);
	DOREPLIFETIME_CONDITION(AShooterCharacter, PreviewWeapon, COND_OwnerOnly);


	DOREPLIFETIME_CONDITION(AShooterCharacter, Armours, COND_OwnerOnly);
	DOREPLIFETIME_CONDITION(AShooterCharacter, OverlapedPickups, COND_OwnerOnly);
	DOREPLIFETIME_CONDITION(AShooterCharacter, bSpawnProtectionEligible, COND_OwnerOnly);
	

	// everyone except local owner: flag change is locally instigated
	DOREPLIFETIME_CONDITION(AShooterCharacter, bIsTargeting, COND_SkipOwner);
	DOREPLIFETIME_CONDITION(AShooterCharacter, bWantsToRun, COND_SkipOwner);

	DOREPLIFETIME(AShooterCharacter, LastTakeHitInfo);
	DOREPLIFETIME_CONDITION(AShooterCharacter, GiveHitInfo, COND_OwnerOnly);

	// everyone
	DOREPLIFETIME(AShooterCharacter, CurrentWeapon);
	DOREPLIFETIME(AShooterCharacter, ShooterGrenade);
	DOREPLIFETIME(AShooterCharacter, ShooterAid);
	DOREPLIFETIME(AShooterCharacter, ShooterKnife);

	DOREPLIFETIME(AShooterCharacter, Health);
}

//bool AShooterCharacter::IsReplicationPausedForConnection(const FNetViewer& ConnectionOwnerNetViewer)
//{
//	if (NetEnablePauseRelevancy == 1)
//	{
//		APlayerController* PC = GetValidObjectAs<APlayerController>(ConnectionOwnerNetViewer.InViewer);
//		check(PC);
//
//		FVector ViewLocation;
//		FRotator ViewRotation;
//		PC->GetPlayerViewPoint(ViewLocation, ViewRotation);
//
//		FCollisionQueryParams CollisionParams(SCENE_QUERY_STAT(LineOfSight), true, PC->GetPawn());
//		CollisionParams.AddIgnoredActor(this);
//
//		TArray<FVector> PointsToTest;
//		BuildPauseReplicationCheckPoints(PointsToTest);
//
//		for (FVector PointToTest : PointsToTest)
//		{
//			if (!GetWorld()->LineTraceTestByChannel(PointToTest, ViewLocation, ECC_Visibility, CollisionParams))
//			{
//				return false;
//			}
//		}
//
//		return true;
//	}
//
//	return false;
//}


UArsenalComponent* AShooterCharacter::GetArsenalComponent() const
{
	if(const auto ps = GetValidObjectAs<AShooterPlayerState>(GetPlayerState()))
	{
		return ps->GetInventoryComponent();
	}
	return nullptr;
}

AShooterWeapon* AShooterCharacter::GetWeapon() const
{
	return GetValidObject(CurrentWeapon);
}

int32 AShooterCharacter::GetInventoryCount() const
{
	return Inventory.Num();
}

USkeletalMeshComponent* AShooterCharacter::GetPawnMesh() const
{
	return IsFirstPerson() ? Mesh1P : GetMesh();
}

USkeletalMeshComponent* AShooterCharacter::GetSpecifcPawnMesh(bool WantFirstPerson) const
{
	return WantFirstPerson == true ? Mesh1P : GetMesh();
}

void AShooterCharacter::UpdateTeamColor()
{
	if (MeshMIDs.Num() <= 0 && Instance)
	{
		if (auto mesh = GetValidObject(GetMesh()))
		{
			for (SIZE_T i = 0; i < mesh->GetNumMaterials(); ++i)
			{
				MeshMIDs.Add(mesh->CreateAndSetMaterialInstanceDynamic(i));
			}
		}
	}

	auto LocalCtrl = GetWorld()->GetFirstPlayerController();
	auto MyGameState = GetWorld()->GetGameState<AShooterGameState>();
	auto Assets = UShooterGameSingleton::Get()->GetDataAssets<UTeamColorsData>();
	if (Assets.Num() && MyGameState && LocalCtrl)
	{
		const bool IsFriendly = MyGameState->HasSameTeam(this, LocalCtrl);
		auto ColorsData = Assets[0];

		for (auto MyMID : MeshMIDs)
		{
			if (auto DynMaterialInstance = GetValidObject(MyMID))
			{
				if (IsFriendly)
				{
					for (auto FriendlyParams : ColorsData->GetFriendlyColorParameters())
					{
						DynMaterialInstance->SetVectorParameterValue(FriendlyParams, ColorsData->GetFriendlyColor());
					}
				}
				else
				{
					for (auto EnemyParams : ColorsData->GetEnemyColorParameters())
					{
						DynMaterialInstance->SetVectorParameterValue(EnemyParams, ColorsData->GetEnemyColor());
					}
				}
				DynMaterialInstance->SetScalarParameterValue(TEXT("Team Color Index"), !IsFriendly);
			}
		}
	}
}

AShooterWeapon* AShooterCharacter::GetPreviewWeapon() const
{
	return GetValidObject(PreviewWeapon);
}

FName AShooterCharacter::GetWeaponAttachPoint() const
{
	return WeaponAttachPoint;
}

float AShooterCharacter::GetTargetingSpeedModifier() const
{
	return TargetingSpeedModifier;
}

bool AShooterCharacter::IsTargeting() const
{
	return bIsTargeting;
}

float AShooterCharacter::GetRunningSpeedModifier() const
{
	return RunningSpeedModifier;
}

bool AShooterCharacter::IsFiring() const
{
	return bWantsToFire;
};

void AShooterCharacter::GiveHealth(const float& InHealth)
{
	Health.X = FMath::Clamp<float>(Health.X + InHealth, 0.0f, GetMaxHealth());
}

float AShooterCharacter::GetMaxHealth() const
{
	return Health.Y;
}

bool AShooterCharacter::IsAlive() const
{
	return Health.X > 0;
}

float AShooterCharacter::GetLowHealthPercentage() const
{
	return LowHealthPercentage;
}

void AShooterCharacter::BuildPauseReplicationCheckPoints(TArray<FVector>& RelevancyCheckPoints)
{
	FBoxSphereBounds Bounds = GetCapsuleComponent()->CalcBounds(GetCapsuleComponent()->GetComponentTransform());
	FBox BoundingBox = Bounds.GetBox();
	float XDiff = Bounds.BoxExtent.X * 2;
	float YDiff = Bounds.BoxExtent.Y * 2;

	RelevancyCheckPoints.Add(BoundingBox.Min);
	RelevancyCheckPoints.Add(FVector(BoundingBox.Min.X + XDiff, BoundingBox.Min.Y, BoundingBox.Min.Z));
	RelevancyCheckPoints.Add(FVector(BoundingBox.Min.X, BoundingBox.Min.Y + YDiff, BoundingBox.Min.Z));
	RelevancyCheckPoints.Add(FVector(BoundingBox.Min.X + XDiff, BoundingBox.Min.Y + YDiff, BoundingBox.Min.Z));
	RelevancyCheckPoints.Add(FVector(BoundingBox.Max.X - XDiff, BoundingBox.Max.Y, BoundingBox.Max.Z));
	RelevancyCheckPoints.Add(FVector(BoundingBox.Max.X, BoundingBox.Max.Y - YDiff, BoundingBox.Max.Z));
	RelevancyCheckPoints.Add(FVector(BoundingBox.Max.X - XDiff, BoundingBox.Max.Y - YDiff, BoundingBox.Max.Z));
	RelevancyCheckPoints.Add(BoundingBox.Max);
}

void AShooterCharacter::SetBodyColorFlash(UCurveLinearColor* ColorCurve, bool bRimOnly)
{
	BodyColorFlashCurve = ColorCurve;
	BodyColorFlashElapsedTime = 0.0f;
	for (auto MI : MeshMIDs)
	{
		static FName NAME_FullBodyFlashPct(TEXT("FullBodyFlashPct"));
		if (MI != nullptr)
		{
			MI->SetScalarParameterValue(NAME_FullBodyFlashPct, bRimOnly ? 0.0f : 1.0f);
		}
	}
}

void AShooterCharacter::UpdateBodyColorFlash(float DeltaTime)
{
	static FName NAME_HitFlashColor(TEXT("HitFlashColor"));

	BodyColorFlashElapsedTime += DeltaTime;
	float MinTime, MaxTime;
	BodyColorFlashCurve->GetTimeRange(MinTime, MaxTime);
	for (auto MI : MeshMIDs)
	{
		if (MI != nullptr)
		{
			if (BodyColorFlashElapsedTime > MaxTime)
			{
				BodyColorFlashCurve = nullptr;
				MI->SetVectorParameterValue(NAME_HitFlashColor, FLinearColor(0.0f, 0.0f, 0.0f, 0.0f));
			}
			else
			{
				MI->SetVectorParameterValue(NAME_HitFlashColor, BodyColorFlashCurve->GetLinearColorValue(BodyColorFlashElapsedTime));
			}
		}
	}
}

void AShooterCharacter::UpdateDissolve(float DeltaTime)
{
	if (DissolveCurve)
	{
		BodyDissolveElapsedTime += DeltaTime;

		for (auto MI : MeshMIDs)
		{
			if (MI != nullptr)
			{
				MI->SetVectorParameterValue(DissolveParameterColor, DissolveCurve->GetLinearColorValue(BodyDissolveElapsedTime));
				MI->SetScalarParameterValue(DissolveParameterAlpha, DissolveCurve->GetLinearColorValue(BodyDissolveElapsedTime).A);
			}
		}
	}
}

void AShooterCharacter::OnRep_Instance()
{
	auto EntityInstance = Instance ? Instance->GetEntity<UCharacterItemEntity>() : nullptr;

	if (EntityInstance)
	{
		if (GetLocalRole() == ROLE_Authority)
		{
			Health.Y = EntityInstance->GetCharacterProperty().Health;
			Health.X = Health.Y;
		}

		GetMesh()->SetSkeletalMesh(EntityInstance->GetTPPMeshData().Mesh);		
		GetMesh()->SetAnimInstanceClass(EntityInstance->GetTPPMeshData().Anim);
		GetMesh()->SetMinLOD(EntityInstance->GetTPPMeshData().MinLOD);

		Mesh1P->SetSkeletalMesh(EntityInstance->GetFPPMeshData().Mesh);
		Mesh1P->SetAnimInstanceClass(EntityInstance->GetFPPMeshData().Anim);
		Mesh1P->SetMinLOD(EntityInstance->GetFPPMeshData().MinLOD);

		// create material instance for setting team colors (3rd person view)
		for (int32 iMat = 0; iMat < GetMesh()->GetNumMaterials(); iMat++)
		{
			MeshMIDs.Add(GetMesh()->CreateAndSetMaterialInstanceDynamic(iMat));
		}

		UpdateTeamColor();

		GetCapsuleComponent()->SetCollisionResponseToChannel(COLLISION_FRIENDLY, ECR_Ignore);
		GetCapsuleComponent()->SetCollisionResponseToChannel(COLLISION_ENEMY, ECR_Ignore);
		GetCapsuleComponent()->SetCollisionResponseToChannel(ECC_Pawn, ECR_Ignore);

		GetCapsuleComponent()->SetCollisionResponseToChannel(GetTeamNum() ? COLLISION_ENEMY : COLLISION_FRIENDLY, ECR_Block);
		GetCapsuleComponent()->SetCollisionObjectType(GetTeamNum() ? COLLISION_FRIENDLY : COLLISION_ENEMY);

		GetMesh()->SetCollisionResponseToChannel(GetTeamNum() ? COLLISION_ENEMY : COLLISION_FRIENDLY, ECR_Block);
		GetMesh()->SetCollisionObjectType(GetTeamNum() ? COLLISION_FRIENDLY : COLLISION_ENEMY);

		AutoTargeting->SetTargetCollisionChannel(GetTeamNum() ? COLLISION_ENEMY : COLLISION_FRIENDLY);

		if (!IsRunningDedicatedServer())
		{
			WidgetScreenComponent->SetSlateWidget(SNew(SNameAndHealth, this));
		}

		auto MyCtrl = GetBaseController<AShooterPlayerController>();
		if (MyCtrl && MyCtrl->IsLocalController())
		{
			MyCtrl->LastCharacter = this;
		}

		if (IsValidObject(ShooterArmComponent))
		{
			MeshLag_InitLoc = ShooterArmComponent->GetRelativeLocation();
			MeshLag_InitRot = ShooterArmComponent->GetRelativeRotation();
			bIsInitMeshLag = true;
		}

		auto ResetCameraLambda = [&]()
		{
			Shooter1PCamera->SetRelativeLocation(FVector::ZeroVector + FVector(FVector2D::ZeroVector, Shooter1PCamera->GetRelativeLocation().Z));
		};

		if (IsLocallyControlled())
		{
			FTimerHandle _tmp;
			GetWorldTimerManager().SetTimer(_tmp, FTimerDelegate::CreateLambda(ResetCameraLambda), 1.0f, false);
		}
		else
		{
			ResetCameraLambda();
		}
	}
}

void AShooterCharacter::OnRep_Knife()
{
	if (auto MyKnife = GetKnife())
	{
		MyKnife->SetTargetCollision(GetTeamNum() ? COLLISION_ENEMY : COLLISION_FRIENDLY);
	}
}

UShooterAid* AShooterCharacter::GetAid() const
{
	return GetValidObject(ShooterAid);
}

AKnife* AShooterCharacter::GetKnife() const
{
	return GetValidObject(ShooterKnife);
}

bool AShooterCharacter::IsAllowUseAid() const
{
	return GetAid() && GetAid()->IsAllowStartProcess();
}

UShooterGrenade* AShooterCharacter::GetGrenade() const
{
	return GetValidObject(ShooterGrenade);
}

bool AShooterCharacter::IsAllowUseGrenade() const
{
	return GetGrenade() && GetGrenade()->IsAllowStartProcess();
}

void AShooterCharacter::InitializeItem(UBasicPlayerItem* InItem, bool IsDefault)
{
	Super::InitializeItem(InItem, IsDefault);

	FString playername = GetName();
	if(GetPlayerState())
	{
		playername = GetPlayerState()->GetPlayerName();
	}

	if (InItem)
	{
		if (const auto ic = InItem->GetEntity<UBasicItemEntity>()->GetInventoryClass())
		{
			UE_LOG(LogInit, Error, TEXT("> [AShooterCharacter::InitializeItem][%s][%s / %d][GetItemType: %d][InventoryClass: %s]"), *playername, *InItem->GetName(), InItem->GetModelId(), static_cast<int32>(InItem->GetItemType()), *ic->GetName());
		}
		else
		{
			UE_LOG(LogInit, Error, TEXT("> [AShooterCharacter::InitializeItem][%s][%s / %d][GetItemType: %d][InventoryClass: null]"), *playername, *InItem->GetName(), InItem->GetModelId(), static_cast<int32>(InItem->GetItemType()));
		}
	}
	else
	{
		UE_LOG(LogInit, Error, TEXT("> [AShooterCharacter::InitializeItem][%s][null]"), *playername);
	}

	const auto ModelId = InItem->GetModelId();

	if (!FindWeapon(ModelId))
	{
		if (const auto WeaponItem = Cast<UWeaponPlayerItem>(InItem))
		{
			AShooterWeapon* NewWeapon = GetWorld()->SpawnActorDeferred<AShooterWeapon>(AShooterWeapon::StaticClass(), FTransform(), this, this, ESpawnActorCollisionHandlingMethod::AlwaysSpawn);
			NewWeapon->InitializeInstance(WeaponItem);
			NewWeapon->FinishSpawning(FTransform());
			AddWeapon(NewWeapon);

			if (GetWeapon() == nullptr && IsDefault)
			{
				EquipWeapon(NewWeapon);
			}
		}
	}
	
	if (const auto ArmourItem = Cast<UArmourPlayerItem>(InItem))
	{
		InitializeArmourInstance(ArmourItem);
	}

	if (const auto AidItem = Cast<UAidPlayerItem>(InItem))
	{
		if (auto aid = GetAid())
		{
			aid->InitializeInstance(AidItem);
		}
		else
		{
			UE_LOG(LogOnline, Error, TEXT("> [AShooterCharacter::InitializeItem][%s / %d][Aid was nullptr]"), *InItem->GetName(), InItem->GetModelId());
		}
	}

	if (const auto GrenadeItem = Cast<UGrenadePlayerItem>(InItem))
	{
		if (auto grenade = GetGrenade())
		{
			grenade->InitializeInstance(GrenadeItem);
		}
		else
		{
			UE_LOG(LogOnline, Error, TEXT("> [AShooterCharacter::InitializeItem][%s / %d][Grenade was nullptr]"), *InItem->GetName(), InItem->GetModelId());
		}
	}

	if (const auto KnifeItem = Cast<UKnifePlayerItem>(InItem))
	{
		const auto KnifeClass = FGameSingletonExtensions::FindSubClassByName("KnifeClass", AKnife::StaticClass());

		ShooterKnife = GetWorld()->SpawnActorDeferred<AKnife>(KnifeClass, FTransform(), this, this, ESpawnActorCollisionHandlingMethod::AlwaysSpawn);
		ShooterKnife->InitializeInstance(KnifeItem);
		ShooterKnife->FinishSpawning(FTransform());

		OnRep_Knife();
	}
}
//
bool AShooterCharacter::OnIsEnemy(AActor* InActor)
{
	AShooterGameState* MyGameState = GetWorld() ? GetWorld()->GetGameState<AShooterGameState>() : nullptr;

	if (auto character = GetValidObjectAs<AShooterCharacter>(InActor))
	{

		//-----------------------------
		if (character->IsAlive() == false)
		{
			return false;
		}

		//-----------------------------
		//auto Dist = FVector::Dist(GetActorLocation(), InActor->GetActorLocation());

		////-----------------------------
		//if (auto weapon = GetWeapon())
		//{
		//	return weapon->IsAllowFireOnDistance(Dist) && MyGameState && MyGameState->HasSameTeam(this, InActor) == false;
		//}
		return MyGameState && MyGameState->HasSameTeam(this, InActor) == false;
	}
	return false;

	
}
