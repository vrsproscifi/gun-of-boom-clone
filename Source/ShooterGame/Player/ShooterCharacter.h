// Copyright 1998-2018 Epic Games, Inc. All Rights Reserved.

#pragma once

//=============================================
#include "Item/GameItemType.h"

//=============================================
#include "BasicPlayerCharacter.h"
#include "Interfaces/TeamInterface.h"

//=============================================
#include "ShooterCharacter.generated.h"

class AShooterPickup;
class AKnife;
class UKnifeComponent;
class UWidgetComponent;
//=============================================
class UShooterGrenade;
class UShooterArmour;
class UShooterAid;
class USoundCue;

//=============================================
class UIdentityComponent;
class USquadComponent;
class UArsenalComponent;
class UProgressComponent;

//=============================================
class ULookOnComponent;

//=============================================
UENUM()
namespace ECharacterArmour
{
	enum Type
	{
		Mask,
		Tors,
		Pants,
		END
	};
}

/** replicated information on a hit we've taken */
USTRUCT()
struct FTakeHitInfo
{
	GENERATED_USTRUCT_BODY()

		/** The amount of damage actually applied */
		UPROPERTY()
		float ActualDamage;

	/** The damage type we were hit with. */
	UPROPERTY()
		UClass* DamageTypeClass;

	/** Who hit us */
	UPROPERTY()
		TWeakObjectPtr<class AShooterCharacter> PawnInstigator;

	/** Who actually caused the damage */
	UPROPERTY()
		TWeakObjectPtr<class AActor> DamageCauser;

	/** Specifies which DamageEvent below describes the damage received. */
	UPROPERTY()
		uint8 DamageEventClassID;

	/** Rather this was a kill */
	UPROPERTY()
		uint32 bKilled : 1;

private:

	/** A rolling counter used to ensure the struct is dirty and will replicate. */
	UPROPERTY()
		uint32 EnsureReplicationByte : 1;

	/** Describes general damage. */
	UPROPERTY()
		FDamageEvent GeneralDamageEvent;

	/** Describes point damage, if that is what was received. */
	UPROPERTY()
		FPointDamageEvent PointDamageEvent;

	/** Describes radial damage, if that is what was received. */
	UPROPERTY()
		FRadialDamageEvent RadialDamageEvent;

public:
	FTakeHitInfo();

	FDamageEvent& GetDamageEvent();
	void SetDamageEvent(const FDamageEvent& DamageEvent);
	void EnsureReplication();
};

USTRUCT()
struct FGiveHitInfo
{
	GENERATED_USTRUCT_BODY()

		UPROPERTY()		float										Damage;
	UPROPERTY()		TWeakObjectPtr<class AShooterCharacter>		PawnDamaged;
	UPROPERTY()		TWeakObjectPtr<class AActor>				DamageCauser;

	void EnsureReplication()
	{
		++ReplicationCounter;
	}

protected:

	UPROPERTY()		uint8										ReplicationCounter;
};

//=============================================
UCLASS(Abstract)
class AShooterCharacter 
	: public ABasicPlayerCharacter
	, public ITeamInterface
{
	GENERATED_UCLASS_BODY()

	static const FName HeadBoneName;

	UPROPERTY(VisibleInstanceOnly)
	class USoundConcurrency* HitSoundConcurrency;

	//=============================================================================================================

#pragma region SpawnProtection
protected:
	/** whether spawn protection may potentially be applied (still must meet time since spawn check in UTGameMode)
	* set to false after firing weapon or any other action that is considered offensive
	*/
	UPROPERTY(VisibleInstanceOnly, BlueprintReadOnly, Replicated, Category = Pawn)
		bool bSpawnProtectionEligible;

	UPROPERTY(Transient) 
		FTimerHandle SpawnProtectionHandle;

	UFUNCTION()
		void OnTryDisableSpawnProtection();

public:
	/** returns whether spawn protection currently applies for this character (valid on client) */
	UFUNCTION(BlueprintCallable, BlueprintPure, Category = Damage)
		const bool& IsSpawnProtectionEligible() const;

	/** returns whether spawn protection currently applies for this character (valid on client) */
	UFUNCTION(BlueprintCallable, BlueprintPure, Category = Damage)
		bool IsSpawnProtected() const;

	UFUNCTION(BlueprintCallable, Category = Damage)
		void DisableSpawnProtection();

	UFUNCTION(BlueprintCallable, BlueprintPure, Category = Damage)
		float GetSpawnProtectionProgress() const;

	UFUNCTION(BlueprintCallable, BlueprintPure, Category = Damage)
		float GetWorldSpawnProtectionTime(const float& InDefaultResult = 0.0f) const;

	UFUNCTION(BlueprintCallable, BlueprintPure, Category = Damage)
		float GetWorldTimeSeconds() const;

#pragma endregion

	//=============================================================================================================

public:
#pragma region Components

public:
	UFUNCTION(BlueprintCallable, BlueprintPure, Category = Component)
		UIdentityComponent* GetIdentityComponent() const;

	UFUNCTION(BlueprintCallable, BlueprintPure, Category = Component)
		UArsenalComponent* GetInventoryComponent() const;

	UFUNCTION(BlueprintCallable, BlueprintPure, Category = Component)
		USquadComponent* GetSquadComponent() const;

	UFUNCTION(BlueprintCallable, BlueprintPure, Category = Component)
		UProgressComponent* GetProgressComponent() const;
#pragma endregion

private:

	UPROPERTY(Replicated)
	TArray<AShooterPickup*> OverlapedPickups;

	UPROPERTY(Replicated, VisibleInstanceOnly)
	UShooterArmour* Armours[ECharacterArmour::END];

	
	void InitializeArmourInstance(const ECharacterArmour::Type& InArmourSlot, class UBasicPlayerItem* InPlayerItemInstance);

	void InitializeArmourInstance(class UBasicPlayerItem* InPlayerItemInstance);

	UPROPERTY(Replicated, VisibleInstanceOnly)
		UShooterAid* ShooterAid;
	
	UPROPERTY(Replicated, VisibleInstanceOnly)
		UShooterGrenade* ShooterGrenade;

	UFUNCTION()
		void OnRep_Knife();

	UPROPERTY(ReplicatedUsing = OnRep_Knife, VisibleInstanceOnly)
		AKnife* ShooterKnife;

	UFUNCTION(BlueprintCallable, Category = "Inventory")
	void OnPlayerItemUsed(const UBasicPlayerItem* inItem) const;

	UFUNCTION(BlueprintCallable, Category = "Game|Weapon")
	static FName GetGetDamagedBoneName(const FDamageEvent& DamageEvent);

	UFUNCTION(BlueprintCallable, Category = "Game|Weapon")
	float GetDamageRate(const FDamageEvent& DamageEvent) const;

public:

	UFUNCTION(BlueprintCallable, Category = "Game|Character")
	float TakeArmourDamage(const float& InActualDamage, float& OutArmourDamage) const;

	UFUNCTION(BlueprintCallable, Category = "Game|Character")
	float GetCurrentArmour() const;

	UFUNCTION(BlueprintCallable, Category = "Game|Character")
	float GetTotalArmour() const;

	UFUNCTION(BlueprintCallable, Category = "Game|Character")
	void AddOverlapedPickup(AShooterPickup* InPickup);

	UFUNCTION(BlueprintCallable, Category = "Game|Character")
	void RemoveOverlapedPickup(AShooterPickup* InPickup);

	UFUNCTION(BlueprintCallable, Category = "Game|Character")
	TArray<AShooterPickup*> GetOverlapedPickups() const;

	UFUNCTION(BlueprintCallable, Category = "Game|Character")
	AShooterPickup* GetNearlyOverlapedPickup() const;

	//----------------------------------------
	UFUNCTION(BlueprintCallable, Category = "Game|Character")
	UShooterArmour* GetArmour(const ECharacterArmour::Type& InArmourSlot) const;

	//----------------------------------------
	UFUNCTION(BlueprintCallable, Category = "Game|Character")
	UShooterAid* GetAid() const;

	UFUNCTION(BlueprintCallable, Category = "Game|Character")
	AKnife* GetKnife() const;

	UFUNCTION(BlueprintCallable, Category = "Game|Character")
	bool IsAllowUseAid() const;

	//----------------------------------------
	UFUNCTION(BlueprintCallable, Category = "Game|Character")
		UShooterGrenade* GetGrenade() const;

	UFUNCTION(BlueprintCallable, Category = "Game|Character")
		bool IsAllowUseGrenade() const;
	
	//----------------------------------------
	// ABasicPlayerCharacter overrides begin

	virtual void MoveForward(float InValue) override;
	virtual void MoveRight(float InValue) override;
	virtual void MoveUp(float InValue) override;

	virtual void TurnAtRate(float InValue) override;
	virtual void TurnAtValue(float InValue) override;

	virtual void LookUpAtRate(float InValue) override;
	virtual void LookUpAtValue(float InValue) override;

	// ABasicPlayerCharacter overrides end

	virtual ATeamInfo* GetTeam() const override;
	virtual uint8 GetTeamNum() const override;

	virtual void BeginDestroy() override;

	/** spawn inventory, setup initial variables */
	virtual void PostInitializeComponents() override;

	/** Update the character. (Running, health etc). */
	virtual void Tick(float DeltaSeconds) override;

	/** cleanup inventory */
	virtual void Destroyed() override;

	/** update mesh for first person view */
	virtual void PawnClientRestart() override;

	/** [server] perform PlayerState related setup */
	virtual void PossessedBy(class AController* C) override;

	/** [client] perform PlayerState related setup */
	virtual void OnRep_PlayerState() override;

	/** [server] called to determine if we should pause replication this actor to a specific player */
	//virtual bool IsReplicationPausedForConnection(const FNetViewer& ConnectionOwnerNetViewer) override;

	/**
	* Add camera pitch to first person mesh.
	*
	*	@param	CameraLocation	Location of the Camera.
	*	@param	CameraRotation	Rotation of the Camera.
	*/
	void OnCameraUpdate(const FVector& CameraLocation, const FRotator& CameraRotation);

	/** get aim offsets */
	UFUNCTION(BlueprintCallable, Category = "Game|Weapon")
	FRotator GetAimOffsets() const;

	/**
	* Check if pawn is enemy if given controller.
	*
	* @param	TestPC	Controller to check against.
	*/
	bool IsEnemyFor(AController* TestPC) const;

	//////////////////////////////////////////////////////////////////////////
	// Inventory

	/**
	* [server] add weapon to inventory
	*
	* @param Weapon	Weapon to add.
	*/
	void AddWeapon(class AShooterWeapon* Weapon);

	/**
	* [server] remove weapon from inventory
	*
	* @param Weapon	Weapon to remove.
	*/
	void RemoveWeapon(class AShooterWeapon* Weapon);

	/**
	* Find in inventory
	*
	* @param WeaponClass	Class of weapon to find.
	*/
	class AShooterWeapon* FindWeapon(TSubclassOf<class AShooterWeapon> WeaponClass);

	class AShooterWeapon* FindWeapon(const int32& InModelId) const;

	class AShooterWeapon* FindWeaponBySlot(const EGameItemType& InSlotId) const;


	/**
	* [server + local] equips weapon from inventory
	*
	* @param Weapon	Weapon to equip
	*/
	void EquipWeapon(class AShooterWeapon* Weapon, bool IsDefault = false);

	//////////////////////////////////////////////////////////////////////////
	// Weapon usage

	bool SwitchWeaponToPistol();

	void SwitchWeaponToPreview();

	/** [local] starts weapon fire */
	void StartWeaponFire();

	/** [local] stops weapon fire */
	void StopWeaponFire();

	/** check if pawn can fire weapon */
	bool CanFire() const;

	/** check if pawn can reload weapon */
	bool CanReload() const;

	/** [server + local] change targeting state */
	void SetTargeting(bool bNewTargeting);

	//////////////////////////////////////////////////////////////////////////
	// Movement

	/** [server + local] change running state */
	void SetRunning(bool bNewRunning, bool bToggle);

	//////////////////////////////////////////////////////////////////////////
	// Animations

	/** play anim montage */
	virtual float PlayAnimMontage(class UAnimMontage* AnimMontage, float InPlayRate = 1.f, FName StartSectionName = NAME_None) override;

	/** stop playing montage */
	virtual void StopAnimMontage(class UAnimMontage* AnimMontage) override;

	/** stop playing all montages */
	UFUNCTION(BlueprintCallable, Category = "Game|Animation")
	void StopAllAnimMontages();

	//////////////////////////////////////////////////////////////////////////
	// Input handlers

	/** setup pawn specific input handlers */
	virtual void SetupPlayerInputComponent(class UInputComponent* InputComponent) override;
	
	UFUNCTION(reliable, server, WithValidation)
	void OnWeaponPickUp();
	void OnClientWeaponPickUp();

	UFUNCTION(reliable, server, WithValidation)
	void OnAidActivate();
	void OnClientAidActivate();
	
	UFUNCTION(reliable, server, WithValidation)
	void OnGrenadeActivate();
	void OnClientGrenadeActivate();

	UFUNCTION(Reliable, Server, WithValidation)
	void OnKnifeActivate();
	void OnClientKnifeActivate();

	/** player pressed start fire action */
	void OnStartFire();

	/** player released start fire action */
	void OnStopFire();

	/** player pressed targeting action */
	void OnStartTargeting();

	void OnClientStartTargeting();


	/** player released targeting action */
	void OnStopTargeting();

	TArray<AShooterWeapon*> GetAvalibleWeapons() const;

	/** player pressed next weapon action */
	void OnNextWeapon();

	/** player pressed prev weapon action */
	void OnPrevWeapon();

	/** player pressed reload action */
	void OnReload();
	void OnClientReload();

	/** player pressed jump action */
	void OnStartJump();

	/** player released jump action */
	void OnStopJump();

	/** player pressed run action */
	void OnStartRunning();

	/** player pressed toggled run action */
	void OnStartRunningToggle();

	/** player released run action */
	void OnStopRunning();

	//////////////////////////////////////////////////////////////////////////
	// Reading data

	class UArsenalComponent* GetArsenalComponent() const;

	/** get mesh component */
	USkeletalMeshComponent* GetPawnMesh() const;

	/** get currently equipped weapon */
	UFUNCTION(BlueprintCallable, Category = "Game|Weapon")
	class AShooterWeapon* GetWeapon() const;

	/** get weapon attach point */
	FName GetWeaponAttachPoint() const;

	/** get total number of inventory items */
	int32 GetInventoryCount() const;

	/** get weapon taget modifier speed	*/
	UFUNCTION(BlueprintCallable, Category = "Game|Weapon")
	float GetTargetingSpeedModifier() const;

	/** get targeting state */
	UFUNCTION(BlueprintCallable, Category = "Game|Weapon")
	bool IsTargeting() const;

	/** get firing state */
	UFUNCTION(BlueprintCallable, Category = "Game|Weapon")
	bool IsFiring() const;

	/** get the modifier value for running speed */
	UFUNCTION(BlueprintCallable, Category = Pawn)
	float GetRunningSpeedModifier() const;

	/** get running state */
	UFUNCTION(BlueprintCallable, Category = Pawn)
	bool IsRunning() const;

	/** get max health */
	float GetMaxHealth() const;

	void GiveHealth(const float& InHealth);

	virtual bool IsAlive() const override;

	/** returns percentage of health when low health effects should start */
	float GetLowHealthPercentage() const;

	/*
	* Get either first or third person mesh.
	*
	* @param	WantFirstPerson		If true returns the first peron mesh, else returns the third
	*/
	USkeletalMeshComponent* GetSpecifcPawnMesh(bool WantFirstPerson) const;

	/** Update the team color of all player meshes. */
	void UpdateTeamColor();

	class AShooterWeapon* GetPreviewWeapon() const;

protected:

	/** pawn mesh: 1st person view */
	UPROPERTY(VisibleDefaultsOnly, BlueprintReadOnly, Category = Mesh)
	USkeletalMeshComponent* Mesh1P;

	UPROPERTY(VisibleDefaultsOnly, BlueprintReadOnly, Category = Mesh)
	class UCameraComponent* Shooter1PCamera;

	UPROPERTY(VisibleDefaultsOnly, BlueprintReadOnly, Category = Mesh)
	class USphereComponent* ShooterArmComponent;

	UPROPERTY(VisibleDefaultsOnly, Category = Mesh)
	ULookOnComponent* AutoTargeting;

	UPROPERTY(VisibleDefaultsOnly, Category = UI)
	UWidgetComponent* WidgetScreenComponent;

	/** socket or bone name for attaching weapon mesh */
	UPROPERTY(EditDefaultsOnly, Category = Inventory)
	FName WeaponAttachPoint;

	/** default inventory list */
	UPROPERTY(EditDefaultsOnly, Category = Inventory)
	TArray<TSubclassOf<class AShooterWeapon> > DefaultInventoryClasses;

	/** weapons in inventory */
	UPROPERTY(Transient, Replicated)
	TArray<class AShooterWeapon*> Inventory;

	/** currently equipped weapon */
	UPROPERTY(Transient, ReplicatedUsing = OnRep_CurrentWeapon)
	class AShooterWeapon* CurrentWeapon;

	UPROPERTY(Transient, Replicated)
	class AShooterWeapon* PreviewWeapon;

	/** Replicate where this pawn was last hit and damaged */
	UPROPERTY(Transient, ReplicatedUsing = OnRep_LastTakeHitInfo)
	struct FTakeHitInfo LastTakeHitInfo;

	/** Time at which point the last take hit info for the actor times out and won't be replicated; Used to stop join-in-progress effects all over the screen */
	float LastTakeHitTimeTimeout;

	/** modifier for max movement speed */
	UPROPERTY(EditDefaultsOnly, Category = Inventory)
	float TargetingSpeedModifier;

	/** current targeting state */
	UPROPERTY(Transient, Replicated)
	uint8 bIsTargeting : 1;

	/** modifier for max movement speed */
	UPROPERTY(EditDefaultsOnly, Category = Pawn)
	float RunningSpeedModifier;

	/** current running state */
	UPROPERTY(Transient, Replicated)
	uint8 bWantsToRun : 1;

	float LastRunningTime;

	/** from gamepad running is toggled */
	uint8 bWantsToRunToggled : 1;

	/** current firing state */
	uint8 bWantsToFire : 1;

	/** when low health effects should start */
	float LowHealthPercentage;

	/** Base turn rate, in deg/sec. Other scaling may affect final turn rate. */
	float BaseTurnRate;

	/** Base lookup rate, in deg/sec. Other scaling may affect final lookup rate. */
	float BaseLookUpRate;

	/** material instances for setting team color in mesh (3rd person view) */
	UPROPERTY(Transient)
	TArray<UMaterialInstanceDynamic*> MeshMIDs;

	/** animation played on death */
	UPROPERTY(EditDefaultsOnly, Category = Animation)
	UAnimMontage* DeathAnim;

	/** sound played on death, local player only */
	UPROPERTY(EditDefaultsOnly, Category = Pawn)
	USoundCue* DeathSound;

	/** effect played on respawn */
	UPROPERTY(EditDefaultsOnly, Category = Pawn)
	UParticleSystem* RespawnFX;

	/** sound played on respawn */
	UPROPERTY(EditDefaultsOnly, Category = Pawn)
	USoundCue* RespawnSound;

	/** sound played when health is low */
	UPROPERTY(EditDefaultsOnly, Category = Pawn)
	USoundCue* LowHealthSound;

	/** sound played when running */
	UPROPERTY(EditDefaultsOnly, Category = Pawn)
	USoundCue* RunLoopSound;

	/** sound played when stop running */
	UPROPERTY(EditDefaultsOnly, Category = Pawn)
	USoundCue* RunStopSound;

	/** sound played when targeting state changes */
	UPROPERTY(EditDefaultsOnly, Category = Pawn)
	USoundCue* TargetingSound;

	/** used to manipulate with run loop sound */
	UPROPERTY()
	UAudioComponent* RunLoopAC;

	/** hook to looped low health sound used to stop/adjust volume */
	UPROPERTY()
	UAudioComponent* LowHealthWarningPlayer;

	/** handles sounds for running */
	void UpdateRunSounds();

	/** handle mesh visibility and updates */
	void UpdatePawnMeshes();

	/** Responsible for cleaning up bodies on clients. */
	virtual void TornOff();

private:

	/** Whether or not the character is moving (based on movement input). */
	bool IsMoving();

	//////////////////////////////////////////////////////////////////////////
	// Damage & death

public:

	/** Identifies if pawn is in its dying state */
	UPROPERTY(VisibleAnywhere, BlueprintReadOnly, Category = Health)
	uint32 bIsDying : 1;

	// Current health of the Pawn
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Replicated, Category = Health)
	FVector2D Health;

	/** Take damage, handle death */
	virtual float TakeDamage(float Damage, struct FDamageEvent const& DamageEvent, class AController* InEventInstigator, class AActor* InDamageCauser) override;

	/** Pawn suicide */
	virtual void Suicide();

	/** Kill this pawn */
	virtual void KilledBy(class APawn* EventInstigator);

	/** Returns True if the pawn can die in the current state */
	virtual bool CanDie(float KillingDamage, FDamageEvent const& DamageEvent, AController* Killer, AActor* DamageCauser) const;

	/**
	* Kills pawn.  Server/authority only.
	* @param KillingDamage - Damage amount of the killing blow
	* @param DamageEvent - Damage event of the killing blow
	* @param Killer - Who killed this pawn
	* @param DamageCauser - the Actor that directly caused the damage (i.e. the Projectile that exploded, the Weapon that fired, etc)
	* @returns true if allowed
	*/
	virtual bool Die(float KillingDamage, struct FDamageEvent const& DamageEvent, class AController* Killer, class AActor* DamageCauser);

	// Die when we fall out of the world.
	virtual void FellOutOfWorld(const class UDamageType& dmgType) override;

	/** Called on the actor right before replication occurs */
	virtual void PreReplication(IRepChangedPropertyTracker & ChangedPropertyTracker) override;
protected:
	/** notification when killed, for both the server and client. */
	virtual void OnDeath(float KillingDamage, struct FDamageEvent const& DamageEvent, class APawn* InstigatingPawn, class AActor* DamageCauser);

	/** play effects on hit */
	virtual void PlayHit(float DamageTaken, struct FDamageEvent const& DamageEvent, class APawn* PawnInstigator, class AActor* DamageCauser);

	/** switch to ragdoll */
	void SetRagdollPhysics();

	/** sets up the replication for taking a hit */
	void ReplicateHit(float Damage, struct FDamageEvent const& DamageEvent, class APawn* InstigatingPawn, class AActor* DamageCauser, bool bKilled);

	/** play hit or death on client */
	UFUNCTION()
	void OnRep_LastTakeHitInfo();

	//////////////////////////////////////////////////////////////////////////
	// Inventory

	/** updates current weapon */
	void SetCurrentWeapon(class AShooterWeapon* NewWeapon, class AShooterWeapon* LastWeapon = NULL);

	/** current weapon rep handler */
	UFUNCTION()
	void OnRep_CurrentWeapon(class AShooterWeapon* LastWeapon);

	/** [server] spawns default inventory */
	void SpawnDefaultInventory();

	/** [server] Spawn active weapon pickup */
	void SpawnWeaponPickup();

	/** [server] remove all weapons from inventory and destroy them */
	void DestroyInventory();

	/** equip weapon */
	UFUNCTION(reliable, server, WithValidation)
	void ServerEquipWeapon(class AShooterWeapon* NewWeapon, bool IsDefault = false);

	/** update targeting state */
	UFUNCTION(reliable, server, WithValidation)
	void ServerSetTargeting(bool bNewTargeting);

	/** update targeting state */
	UFUNCTION(reliable, server, WithValidation)
	void ServerSetRunning(bool bNewRunning, bool bToggle);

	/** Builds list of points to check for pausing replication for a connection*/
	void BuildPauseReplicationCheckPoints(TArray<FVector>& RelevancyCheckPoints);
	void SetBodyColorFlash(UCurveLinearColor* ColorCurve, bool bRimOnly);
	void UpdateBodyColorFlash(float DeltaTime);
	void UpdateDissolve(float DeltaTime);

public:

	/** Returns Mesh1P subobject **/
	FORCEINLINE USkeletalMeshComponent* GetMesh1P() const { return Mesh1P; }

	FName GetFPPWeaponAttachPoint() const
	{
		return TEXT("WeaponPoint");// FPPWeaponAttachPoint;
	}

	FName GetTPPWeaponAttachPoint() const
	{
		return TEXT("RightHandSocket");// TPPWeaponAttachPoint;
	}

	virtual void OnRep_Instance() override;
	virtual void InitializeItem(UBasicPlayerItem* InInstance, bool IsDefault = false) override;

	UFUNCTION()	bool OnIsEnemy(AActor* InActor);

	UFUNCTION()
		bool OnAutoTargetingValidation(AActor* InActor);

	float BodyColorFlashElapsedTime;
	float BodyDissolveElapsedTime;
	bool BodyDissolveStarted;

	UPROPERTY(EditDefaultsOnly, Category = Effects)
	UCurveLinearColor* DissolveCurve;

	UPROPERTY(EditDefaultsOnly, Category = Effects)
	FName DissolveParameterColor;

	UPROPERTY(EditDefaultsOnly, Category = Effects)
	FName DissolveParameterAlpha;

	UPROPERTY()
	UCurveLinearColor* BodyColorFlashCurve;

	UPROPERTY(EditDefaultsOnly, Category = Effects)
	UCurveLinearColor* HealthFlashCurve;

	UPROPERTY(EditDefaultsOnly, Category = Effects)
	UCurveLinearColor* ArmorFlashCurve;

	UPROPERTY(EditDefaultsOnly, Category = Effects)
	UCurveFloat* AntiSKFlashCurve;

protected:

	UFUNCTION()
	void OnRep_GiveHitInfo();

	UPROPERTY(ReplicatedUsing=OnRep_GiveHitInfo)
	FGiveHitInfo GiveHitInfo;

	UPROPERTY()
	double LastGiveHitInfoTime;

	UFUNCTION()
	void UpdateMeshLag(float InDeltaTime);

	UPROPERTY()
	FVector MeshLag_InitLoc;

	UPROPERTY()
	FRotator MeshLag_InitRot;

	UPROPERTY()
	FRotator MeshLag_DeltaRot;

	UPROPERTY()
	FRotator MeshLag_OldRot;

	UPROPERTY()
	bool bIsInitMeshLag;

	UPROPERTY(EditDefaultsOnly, Category = MeshLag)
	FVector2D MeshLag_LookAmount;

	UPROPERTY(EditDefaultsOnly, Category = MeshLag)
	FVector2D MeshLag_SmoothAmount;

	UPROPERTY(EditDefaultsOnly, Category = MeshLag)
	FRotator MeshLag_DeltaLimits;

	UPROPERTY(EditDefaultsOnly, Category = MeshLag)
	float MeshLag_TargetingMultipler;

	UPROPERTY(EditDefaultsOnly, Category = MeshLag)
	TSubclassOf<UCameraShake> MeshLag_MovementShake;

public:
	FORCEINLINE TSubclassOf<UCameraShake> GetMovementShake() const { return MeshLag_MovementShake; }

	FORCEINLINE const FGiveHitInfo& GetGiveHitInfo() const { return GiveHitInfo; }
	FORCEINLINE	double GetLastGiveHitInfoTime() const { return LastGiveHitInfoTime; }

	FORCEINLINE const FTakeHitInfo& GetTakeHitInfo() const { return LastTakeHitInfo; }
};


