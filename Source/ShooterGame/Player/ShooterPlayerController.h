// Copyright 1998-2018 Epic Games, Inc. All Rights Reserved.

#pragma once
//=========================================
#include "Game/OnlinePlayerController.h"

//=========================================
#include "Online.h"
#include "Interfaces/TeamInterface.h"
//=========================================
#include "ShooterPlayerController.generated.h"


//=========================================
class AShooterHUD;

//=========================================
UCLASS(config=Game)
class AShooterPlayerController 
	: public AOnlinePlayerController
	, public ITeamInterface
{
	GENERATED_UCLASS_BODY()

	//virtual FString GetGameMapName() const override;


public:
	UPROPERTY(VisibleAnywhere, BlueprintReadOnly)
	int32 LocalElapsedGameTime;

	virtual void CollectPerfomanceStatistic() override;

	/** sets spectator location and rotation */
	UFUNCTION(reliable, client)
	void ClientSetSpectatorCamera(FVector CameraLocation, FRotator CameraRotation);

	/** notify player about started match */
	UFUNCTION(reliable, client)
	void ClientGameStarted();

	/** Starts the online game using the session name in the PlayerState */
	UFUNCTION(reliable, client)
	void ClientStartOnlineGame();

	/** Ends the online game using the session name in the PlayerState */
	UFUNCTION(reliable, client)
	void ClientEndOnlineGame();	

	/** notify player about finished match */
	virtual void ClientGameEnded_Implementation(class AActor* EndGameFocus, bool bIsWinner);

	/** used for input simulation from blueprint (for automatic perf tests) */
	UFUNCTION(BlueprintCallable, Category="Input")
	void SimulateInputKey(FKey Key, bool bPressed = true);

	/** notify local client about deaths */
	void OnDeathMessage(class AShooterPlayerState* KillerPlayerState, class AShooterPlayerState* KilledPlayerState, const UDamageType* KillerDamageType);

	/** sets the produce force feedback flag. */
	void SetIsVibrationEnabled(bool bEnable);

	/** should produce force feedback? */
	bool IsVibrationEnabled() const;

	/** check if gameplay related actions (movement, weapon usage, etc) are allowed right now */
	bool IsGameInputAllowed() const;

	/**
	 * Called when the read achievements request from the server is complete
	 *
	 * @param PlayerId The player id who is responsible for this delegate being fired
	 * @param bWasSuccessful true if the server responded successfully to the request
	 */
	void OnQueryAchievementsComplete(const FUniqueNetId& PlayerId, const bool bWasSuccessful );
	
	// Begin APlayerController interface

	/** handle weapon visibility */
	virtual void SetCinematicMode(bool bInCinematicMode, bool bHidePlayer, bool bAffectsHUD, bool bAffectsMovement, bool bAffectsTurning) override;

	/** Returns true if movement input is ignored. Overridden to always allow spectators to move. */
	virtual bool IsMoveInputIgnored() const override;

	/** Returns true if look input is ignored. Overridden to always allow spectators to look around. */
	virtual bool IsLookInputIgnored() const override;

	/** initialize the input system from the player settings */
	virtual void InitInputSystem() override;

	virtual bool SetPause(bool bPause, FCanUnpause CanUnpauseDelegate = FCanUnpause()) override;

	virtual FVector GetFocalLocation() const override;

	// End APlayerController interface

	// begin AShooterPlayerController-specific

	/** 
	 * Reads achievements to precache them before first use
	 */
	void QueryAchievements();

	/** 
	 * Writes a single achievement (unless another write is in progress).
	 *
	 * @param Id achievement id (string)
	 * @param Percent number 1 to 100
	 */
	void UpdateAchievementProgress( const FString& Id, float Percent );

	/** Returns a pointer to the shooter game hud. May return NULL. */
	AShooterHUD* GetShooterHUD() const;

	/** Returns the persistent user record associated with this player, or null if there is't one. */
	class UShooterPersistentUser* GetPersistentUser() const;


	/** Associate a new UPlayer with this PlayerController. */
	virtual void SetPlayer(UPlayer* Player);

	// end AShooterPlayerController-specific

	virtual void PreClientTravel(const FString& PendingURL, ETravelType TravelType, bool bIsSeamlessTravel) override;

protected:

	/** should produce force feedback? */
	uint8 bIsVibrationEnabled : 1;

	/** if set, gameplay related actions (movement, weapn usage, etc) are allowed */
	uint8 bAllowGameActions : 1;

	/** true for the first frame after the game has ended */
	uint8 bGameEndedFrame : 1;

	/** stores pawn location at last player death, used where player scores a kill after they died **/
	FVector LastDeathLocation;

	/** shooter in-game menu */
	TSharedPtr<class FShooterIngameMenu> ShooterIngameMenu;

	/** Achievements write object */
	FOnlineAchievementsWritePtr WriteObject;

	/** try to find spot for death cam */
	bool FindDeathCameraSpot(FVector& CameraLocation, FRotator& CameraRotation);

	virtual void BeginDestroy() override;

	//Begin AActor interface

	/** after all game elements are created */
	virtual void PostInitializeComponents() override;

public:
	virtual void TickActor(float DeltaTime, enum ELevelTick TickType, FActorTickFunction& ThisTickFunction) override;
	//End AActor interface

	//Begin AController interface
	
	/** transition to dead state, retries spawning later */
	virtual void FailedToSpawnPawn() override;
	
	/** update camera when pawn dies */
	virtual void PawnPendingDestroy(APawn* P) override;

	//End AController interface

	// Begin APlayerController interface

	/** respawn after dying */
	virtual void UnFreeze() override;

	/** sets up input */
	virtual void SetupInputComponent() override;

	/**
	 * Called from game info upon end of the game, used to transition to proper state. 
	 *
	 * @param EndGameFocus Actor to set as the view target on end game
	 * @param bIsWinner true if this controller is on winning team
	 */
	virtual void GameHasEnded(class AActor* EndGameFocus = NULL, bool bIsWinner = false) override;	

	/** Causes the player to commit suicide */
	UFUNCTION(exec)
	virtual void Suicide();

	/** Notifies the server that the client has suicided */
	UFUNCTION(reliable, server, WithValidation)
	void ServerSuicide();

	// End APlayerController interface

	FName	ServerSayString;

	// Timer used for updating friends in the player tick.
	float ShooterFriendUpdateTimer;

private:

	/** Handle for efficient management of ClientStartOnlineGame timer */
	FTimerHandle TimerHandle_ClientStartOnlineGame;

public:

	virtual void InitPlayerState() override;

	virtual ATeamInfo* GetTeam() const override;
	virtual uint8 GetTeamNum() const override;

	//====================================[ Kill-Cam ]

	UFUNCTION()
	void OnRep_MyLastKiller();

	UPROPERTY(ReplicatedUsing = OnRep_MyLastKiller)
	const AActor* MyLastKiller;

	UPROPERTY()
	const APawn* LastCharacter;

protected:

	UFUNCTION()
	void ProccesAutofire(float InDeltaTime);
};

