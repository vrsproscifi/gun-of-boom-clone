// Copyright 1998-2018 Epic Games, Inc. All Rights Reserved.

#include "ShooterGame.h"
#include "Player/ShooterPlayerController.h"
#include "Player/ShooterPlayerCameraManager.h"
#include "Player/ShooterLocalPlayer.h"
#include "Inventory/ShooterWeapon.h"
#include "Online.h"
#include "OnlineAchievementsInterface.h"
#include "OnlineEventsInterface.h"
#include "OnlineIdentityInterface.h"
#include "OnlineSessionInterface.h"
#include "Sound/SoundNodeLocalPlayer.h"
#include "AudioThread.h"
#include "Style/ShooterStyle.h"
#include "ShooterHUD.h"
#include "ShooterCharacter.h"
#include "GameState/ShooterPlayerState.h"

#include "GameState/ShooterGameState.h"
#include "GameInstance/ShooterGameViewportClient.h"
#include "GameSingletonExtensions.h"
#include "ProfileTestData.h"
#include "ShooterGameSingleton.h"
#include "GameInstance/ShooterGameUserSettings.h"
#include "ShooterGameAnalytics.h"
#include "WeaponItemProperty.h"
//#include "CrashlyticsBlueprintLibrary.h"
#include "ShooterSpectatorPawn.h"

AShooterPlayerController::AShooterPlayerController(const FObjectInitializer& ObjectInitializer) 
	: Super(ObjectInitializer)
	, LocalElapsedGameTime(0)
{
	PlayerCameraManagerClass = AShooterPlayerCameraManager::StaticClass();
	bAllowGameActions = true;
	bGameEndedFrame = false;
	LastDeathLocation = FVector::ZeroVector;

	ServerSayString = TEXT("Say");
	ShooterFriendUpdateTimer = 0.0f;
	bReplicates = true;
	MyLastKiller = nullptr;
}

void AShooterPlayerController::GetLifetimeReplicatedProps(TArray<class FLifetimeProperty>& OutLifetimeProps) const
{
	Super::GetLifetimeReplicatedProps(OutLifetimeProps);

	DOREPLIFETIME_CONDITION(AShooterPlayerController, MyLastKiller, COND_OwnerOnly);
}

//FString AShooterPlayerController::GetGameMapName() const
//{
//	return GetWorld()->GetCurrentLevel()->GetName();
//}

void AShooterPlayerController::SetupInputComponent()
{
	Super::SetupInputComponent();

	// voice chat
	InputComponent->BindAction("PushToTalk", IE_Pressed, this, &APlayerController::StartTalking);
	InputComponent->BindAction("PushToTalk", IE_Released, this, &APlayerController::StopTalking);
}


void AShooterPlayerController::PostInitializeComponents()
{
	Super::PostInitializeComponents();
	FShooterStyle::Initialize();
	ShooterFriendUpdateTimer = 0;
}

void AShooterPlayerController::TickActor(float DeltaTime, enum ELevelTick TickType, FActorTickFunction& ThisTickFunction)
{
	Super::TickActor(DeltaTime, TickType, ThisTickFunction);

	if (IsLocalPlayerController())
	{
		ProccesAutofire(DeltaTime);
	}

	// Is this the first frame after the game has ended
	if(bGameEndedFrame)
	{
		bGameEndedFrame = false;
	}

	const bool bLocallyControlled = IsLocalController();
	const uint32 UniqueID = GetUniqueID();
	FAudioThread::RunCommandOnAudioThread([UniqueID, bLocallyControlled]()
	{
		USoundNodeLocalPlayer::GetLocallyControlledActorCache().Add(UniqueID, bLocallyControlled);
	});
};

void AShooterPlayerController::BeginDestroy()
{
	Super::BeginDestroy();

	if (!GExitPurge)
	{
		const uint32 UniqueID = GetUniqueID();
		FAudioThread::RunCommandOnAudioThread([UniqueID]()
		{
			USoundNodeLocalPlayer::GetLocallyControlledActorCache().Remove(UniqueID);
		});
	}
}

void AShooterPlayerController::SetPlayer( UPlayer* InPlayer )
{
	Super::SetPlayer( InPlayer );

	if (ULocalPlayer* const LocalPlayer = GetValidObjectAs<ULocalPlayer>(Player))
	{
		FInputModeGameOnly InputMode;
		SetInputMode(InputMode);
	}
}

void AShooterPlayerController::QueryAchievements()
{
	// precache achievements
	//ULocalPlayer* LocalPlayer = GetValidObjectAs<ULocalPlayer>(Player);
	//if (LocalPlayer && LocalPlayer->GetControllerId() != -1)
	//{
	//	IOnlineSubsystem* OnlineSub = IOnlineSubsystem::Get();
	//	if(OnlineSub)
	//	{
	//		IOnlineIdentityPtr Identity = OnlineSub->GetIdentityInterface();
	//		if (Identity.IsValid())
	//		{
	//			auto UserId = Identity->GetUniquePlayerId(LocalPlayer->GetControllerId());
	//
	//			if (UserId.IsValid())
	//			{
	//				IOnlineAchievementsPtr Achievements = OnlineSub->GetAchievementsInterface();
	//
	//				if (Achievements.IsValid())
	//				{
	//					Achievements->QueryAchievements( *UserId.Get(), FOnQueryAchievementsCompleteDelegate::CreateUObject( this, &AShooterPlayerController::OnQueryAchievementsComplete ));
	//				}
	//			}
	//			else
	//			{
	//				UE_LOG(LogOnline, Warning, TEXT("No valid user id for this controller."));
	//			}
	//		}
	//		else
	//		{
	//			UE_LOG(LogOnline, Warning, TEXT("No valid identity interface."));
	//		}
	//	}
	//	else
	//	{
	//		UE_LOG(LogOnline, Warning, TEXT("No default online subsystem."));
	//	}
	//}
	//else
	//{
	//	UE_LOG(LogOnline, Warning, TEXT("No local player, cannot read achievements."));
	//}
}

void AShooterPlayerController::OnQueryAchievementsComplete(const FUniqueNetId& PlayerId, const bool bWasSuccessful )
{
	UE_LOG(LogOnline, Display, TEXT("AShooterPlayerController::OnQueryAchievementsComplete(bWasSuccessful = %s)"), bWasSuccessful ? TEXT("TRUE") : TEXT("FALSE"));
}

void AShooterPlayerController::UnFreeze()
{
	if (MyLastKiller == nullptr)
	{
		ServerRestartPlayer();
	}
}

void AShooterPlayerController::FailedToSpawnPawn()
{
	if(StateName == NAME_Inactive)
	{
		BeginInactiveState();
	}
	Super::FailedToSpawnPawn();
}

void AShooterPlayerController::PawnPendingDestroy(APawn* P)
{
	if (auto MyCharacter = Cast<AShooterCharacter>(P))
	{
		MyLastKiller = MyCharacter->GetTakeHitInfo().PawnInstigator.Get();
		ClientGotoState(NAME_Spectating);
	}

	//LastDeathLocation = P->GetActorLocation();
	//FVector CameraLocation = LastDeathLocation + FVector(0, 0, 300.0f);
	//FRotator CameraRotation(-90.0f, 0.0f, 0.0f);
	//FindDeathCameraSpot(CameraLocation, CameraRotation);

	Super::PawnPendingDestroy(P);

	//ClientSetSpectatorCamera(CameraLocation, CameraRotation);
}

void AShooterPlayerController::GameHasEnded(class AActor* EndGameFocus, bool bIsWinner)
{
	//=============================================
	FShooterGameAnalytics::RecordDesignEvent("UI:Lobby:IntoFight:GameHasEnded");
	//UCrashlyticsBlueprintLibrary::WriteLog("UI:Lobby:IntoFight:GameHasEnded");

	//=============================================
	Super::GameHasEnded(EndGameFocus, bIsWinner);
}

void AShooterPlayerController::ClientSetSpectatorCamera_Implementation(FVector CameraLocation, FRotator CameraRotation)
{
	SetInitialLocationAndRotation(CameraLocation, CameraRotation);
	SetViewTarget(this);
}

bool AShooterPlayerController::FindDeathCameraSpot(FVector& CameraLocation, FRotator& CameraRotation)
{
	const FVector PawnLocation = GetPawn()->GetActorLocation();
	FRotator ViewDir = GetControlRotation();
	ViewDir.Pitch = -45.0f;

	const float YawOffsets[] = { 0.0f, -180.0f, 90.0f, -90.0f, 45.0f, -45.0f, 135.0f, -135.0f };
	const float CameraOffset = 1000.0f;
	FCollisionQueryParams TraceParams(SCENE_QUERY_STAT(DeathCamera), true, GetPawn());

	FHitResult HitResult;
	for (int32 i = 0; i < ARRAY_COUNT(YawOffsets); i++)
	{
		FRotator CameraDir = ViewDir;
		CameraDir.Yaw += YawOffsets[i];
		CameraDir.Normalize();

		const FVector TestLocation = PawnLocation - CameraDir.Vector() * CameraOffset;
		
		const bool bBlocked = GetWorld()->LineTraceSingleByChannel(HitResult, PawnLocation, TestLocation, ECC_Camera, TraceParams);

		if (!bBlocked)
		{
			CameraLocation = TestLocation;
			CameraRotation = CameraDir;
			return true;
		}
	}

	return false;
}

void AShooterPlayerController::SimulateInputKey(FKey Key, bool bPressed)
{
	InputKey(Key, bPressed ? IE_Pressed : IE_Released, 1, false);
}

void AShooterPlayerController::OnDeathMessage(class AShooterPlayerState* KillerPlayerState, class AShooterPlayerState* KilledPlayerState, const UDamageType* KillerDamageType) 
{	
	//=============================================
	FShooterGameAnalytics::RecordDesignEvent("UI:Lobby:IntoFight:OnDeath");
	//UCrashlyticsBlueprintLibrary::WriteLog("UI:Lobby:IntoFight:OnDeath");

	ULocalPlayer* LocalPlayer = GetValidObjectAs<ULocalPlayer>(Player);
	if (LocalPlayer && LocalPlayer->GetCachedUniqueNetId().IsValid() && KilledPlayerState->UniqueId.IsValid())
	{
		// if this controller is the player who died, update the hero stat.
		if (*LocalPlayer->GetCachedUniqueNetId() == *KilledPlayerState->UniqueId)
		{
			const auto Events = Online::GetEventsInterface();
			const auto Identity = Online::GetIdentityInterface();

			if (Events.IsValid() && Identity.IsValid())
			{							
				const int32 UserIndex = LocalPlayer->GetControllerId();
				auto UniqueID = Identity->GetUniquePlayerId(UserIndex);
				if (UniqueID.IsValid())
				{				
					AShooterCharacter* ShooterChar = GetValidObjectAs<AShooterCharacter>(GetCharacter());
					AShooterWeapon* Weapon = ShooterChar ? ShooterChar->GetWeapon() : NULL;

					FVector Location = ShooterChar ? ShooterChar->GetActorLocation() : FVector::ZeroVector;
					//int32 WeaponType = Weapon ? (int32)Weapon->GetAmmoType() : 0;

					FOnlineEventParms Params;
					Params.Add( TEXT( "SectionId" ), FVariantData( (int32)0 ) ); // unused
					Params.Add( TEXT( "GameplayModeId" ), FVariantData( (int32)1 ) ); // @todo determine game mode (ffa v tdm)
					Params.Add( TEXT( "DifficultyLevelId" ), FVariantData( (int32)0 ) ); // unused

					Params.Add( TEXT( "PlayerRoleId" ), FVariantData( (int32)0 ) ); // unused
					//Params.Add( TEXT( "PlayerWeaponId" ), FVariantData( (int32)WeaponType ) );
					Params.Add( TEXT( "EnemyRoleId" ), FVariantData( (int32)0 ) ); // unused
					Params.Add( TEXT( "EnemyWeaponId" ), FVariantData( (int32)0 ) ); // untracked
				
					Params.Add( TEXT( "LocationX" ), FVariantData( Location.X ) );
					Params.Add( TEXT( "LocationY" ), FVariantData( Location.Y ) );
					Params.Add( TEXT( "LocationZ" ), FVariantData( Location.Z ) );
										
					Events->TriggerEvent(*UniqueID, TEXT("PlayerDeath"), Params);
				}
			}
		}
	}	
}

void AShooterPlayerController::UpdateAchievementProgress( const FString& Id, float Percent )
{
	ULocalPlayer* LocalPlayer = GetValidObjectAs<ULocalPlayer>(Player);
	if (LocalPlayer)
	{
		IOnlineSubsystem* OnlineSub = IOnlineSubsystem::Get();
		if(OnlineSub)
		{
			IOnlineIdentityPtr Identity = OnlineSub->GetIdentityInterface();
			if (Identity.IsValid())
			{
				auto UserId = LocalPlayer->GetCachedUniqueNetId();

				if (UserId.IsValid())
				{

					IOnlineAchievementsPtr Achievements = OnlineSub->GetAchievementsInterface();
					if (Achievements.IsValid() && (!WriteObject.IsValid() || WriteObject->WriteState != EOnlineAsyncTaskState::InProgress))
					{
						WriteObject = MakeShareable(new FOnlineAchievementsWrite());
						WriteObject->SetFloatStat(*Id, Percent);

						FOnlineAchievementsWriteRef WriteObjectRef = WriteObject.ToSharedRef();
						Achievements->WriteAchievements(*UserId, WriteObjectRef);
					}
					else
					{
						UE_LOG(LogOnline, Warning, TEXT("No valid achievement interface or another write is in progress."));
					}
				}
				else
				{
					UE_LOG(LogOnline, Warning, TEXT("No valid user id for this controller."));
				}
			}
			else
			{
				UE_LOG(LogOnline, Warning, TEXT("No valid identity interface."));
			}
		}
		else
		{
			UE_LOG(LogOnline, Warning, TEXT("No default online subsystem."));
		}
	}
	else
	{
		UE_LOG(LogOnline, Warning, TEXT("No local player, cannot update achievements."));
	}
}


void AShooterPlayerController::SetIsVibrationEnabled(bool bEnable)
{
	bIsVibrationEnabled = bEnable;
}

void AShooterPlayerController::ClientGameStarted_Implementation()
{
	//=============================================
	//UCrashlyticsBlueprintLibrary::LogFbCustomEvent("Battle_Started");
	//UCrashlyticsBlueprintLibrary::SetField("Match.ClientGameStarted", true);
	FShooterGameAnalytics::RecordDesignEvent("UI:Lobby:IntoFight:ClientGameStarted");
	//UCrashlyticsBlueprintLibrary::WriteLog("UI:Lobby:IntoFight:ClientGameStarted");

	//=============================================
	FShooterGameAnalytics::AddProgressionEvent(EProgressionStatus::Start, "Match", "TDM");

	//=======================================================
	//UCrashlyticsBlueprintLibrary::SetField("TravelSuccessfully", true);

	//=============================================
	bAllowGameActions = true;

	// Enable controls mode now the game has started
	SetIgnoreMoveInput(false);

	bGameEndedFrame = false;

	QueryAchievements();
}

/** Starts the online game using the session name in the PlayerState */
void AShooterPlayerController::ClientStartOnlineGame_Implementation()
{	
	//=============================================
	//UCrashlyticsBlueprintLibrary::LogFbCustomEvent("Battle_Started");
	//UCrashlyticsBlueprintLibrary::SetField("Match.ClientStartOnlineGame", true);
	FShooterGameAnalytics::RecordDesignEvent("UI:Lobby:IntoFight:ClientStartOnlineGame");
	//UCrashlyticsBlueprintLibrary::WriteLog("UI:Lobby:IntoFight:ClientStartOnlineGame");

	//=============================================
	if (!IsPrimaryPlayer())
		return;

	AShooterPlayerState* ShooterPlayerState = GetValidObjectAs<AShooterPlayerState>(PlayerState);
	if (ShooterPlayerState)
	{
		IOnlineSubsystem* OnlineSub = IOnlineSubsystem::Get();
		if (OnlineSub)
		{
			IOnlineSessionPtr Sessions = OnlineSub->GetSessionInterface();
			if (Sessions.IsValid() && (Sessions->GetNamedSession(ShooterPlayerState->SessionName) != nullptr))
			{
				UE_LOG(LogOnline, Log, TEXT("Starting session %s on client"), *ShooterPlayerState->SessionName.ToString() );
				Sessions->StartSession(ShooterPlayerState->SessionName);
			}
		}
	}
	else
	{
		// Keep retrying until player state is replicated
		GetWorld()->GetTimerManager().SetTimer(TimerHandle_ClientStartOnlineGame, this, &AShooterPlayerController::ClientStartOnlineGame_Implementation, 0.2f, false);
	}
}

/** Ends the online game using the session name in the PlayerState */
void AShooterPlayerController::ClientEndOnlineGame_Implementation()
{	
	//=============================================
	//UCrashlyticsBlueprintLibrary::LogFbCustomEvent("Battle_Ended");
	//UCrashlyticsBlueprintLibrary::SetField("Match.ClientEndOnlineGame", true);
	FShooterGameAnalytics::RecordDesignEvent("UI:Lobby:IntoFight:ClientEndOnlineGame");
	//UCrashlyticsBlueprintLibrary::WriteLog("UI:Lobby:IntoFight:ClientEndOnlineGame");

	if (!IsPrimaryPlayer())
		return;

	AShooterPlayerState* ShooterPlayerState = GetValidObjectAs<AShooterPlayerState>(PlayerState);
	if (ShooterPlayerState)
	{
		IOnlineSubsystem* OnlineSub = IOnlineSubsystem::Get();
		if (OnlineSub)
		{
			IOnlineSessionPtr Sessions = OnlineSub->GetSessionInterface();
			if (Sessions.IsValid() && (Sessions->GetNamedSession(ShooterPlayerState->SessionName) != nullptr))
			{
				UE_LOG(LogOnline, Log, TEXT("Ending session %s on client"), *ShooterPlayerState->SessionName.ToString() );
				Sessions->EndSession(ShooterPlayerState->SessionName);
			}
		}
	}
}

void AShooterPlayerController::ClientGameEnded_Implementation(class AActor* EndGameFocus, bool bIsWinner)
{	
	//=============================================
	//UCrashlyticsBlueprintLibrary::LogFbCustomEvent("Battle_Ended");
	FShooterGameAnalytics::RecordDesignEvent("UI:Lobby:IntoFight:ClientGameEnded");
	//UCrashlyticsBlueprintLibrary::WriteLog("UI:Lobby:IntoFight:ClientGameEnded");

	//=======================================================
	//UCrashlyticsBlueprintLibrary::SetField("Match.ClientGameEnded", true);
	//UCrashlyticsBlueprintLibrary::SetField("Match.CurrentMap", GetGameMapName());
	//UCrashlyticsBlueprintLibrary::SetField("Match.LastMap", GetGameMapName());
	//UCrashlyticsBlueprintLibrary::SetField("Match.TravelMap", "Lobby");

	//=============================================
	auto Score = 100;

	//=============================================
	if (bIsWinner)
	{
		FShooterGameAnalytics::AddProgressionEvent(EProgressionStatus::Complete, "Match", "TDM", Score);
	}
	else
	{
		FShooterGameAnalytics::AddProgressionEvent(EProgressionStatus::Fail, "Match", "TDM", Score);
	}


	Super::ClientGameEnded_Implementation(EndGameFocus, bIsWinner);
	
	// Disable controls now the game has ended
	SetIgnoreMoveInput(true);

	bAllowGameActions = false;

	// Make sure that we still have valid view target
	SetViewTarget(GetPawn());

	// Flag that the game has just ended (if it's ended due to host loss we want to wait for ClientReturnToMainMenu_Implementation first, incase we don't want to process)
	bGameEndedFrame = true;
}

void AShooterPlayerController::SetCinematicMode(bool bInCinematicMode, bool bHidePlayer, bool bAffectsHUD, bool bAffectsMovement, bool bAffectsTurning)
{
	Super::SetCinematicMode(bInCinematicMode, bHidePlayer, bAffectsHUD, bAffectsMovement, bAffectsTurning);

	// If we have a pawn we need to determine if we should show/hide the weapon
	AShooterCharacter* MyPawn = GetValidObjectAs<AShooterCharacter>(GetPawn());
	AShooterWeapon* MyWeapon = MyPawn ? MyPawn->GetWeapon() : NULL;
	if (MyWeapon)
	{
		if (bInCinematicMode && bHidePlayer)
		{
			MyWeapon->SetActorHiddenInGame(true);
		}
		else if (!bCinematicMode)
		{
			MyWeapon->SetActorHiddenInGame(false);
		}
	}
}

bool AShooterPlayerController::IsMoveInputIgnored() const
{
	if (IsInState(NAME_Spectating))
	{
		return false;
	}
	else
	{
		return Super::IsMoveInputIgnored();
	}
}

bool AShooterPlayerController::IsLookInputIgnored() const
{
	if (IsInState(NAME_Spectating))
	{
		return false;
	}
	else
	{
		return Super::IsLookInputIgnored();
	}
}

void AShooterPlayerController::InitInputSystem()
{
	Super::InitInputSystem();

	UShooterPersistentUser* PersistentUser = GetPersistentUser();
	if(PersistentUser)
	{
		PersistentUser->TellInputAboutKeybindings();
	}
}

void AShooterPlayerController::Suicide()
{
	if ( IsInState(NAME_Playing) )
	{
		ServerSuicide();
	}
}

bool AShooterPlayerController::ServerSuicide_Validate()
{
	return true;
}

void AShooterPlayerController::ServerSuicide_Implementation()
{
	if ( (GetPawn() != NULL) && ((GetWorld()->TimeSeconds - GetPawn()->CreationTime > 1) || (GetNetMode() == NM_Standalone)) )
	{
		AShooterCharacter* MyPawn = GetValidObjectAs<AShooterCharacter>(GetPawn());
		if (MyPawn)
		{
			MyPawn->Suicide();
		}
	}
}

bool AShooterPlayerController::IsVibrationEnabled() const
{
	return bIsVibrationEnabled;
}

bool AShooterPlayerController::IsGameInputAllowed() const
{
	return bAllowGameActions && !bCinematicMode;
}

AShooterHUD* AShooterPlayerController::GetShooterHUD() const
{
	return GetValidObjectAs<AShooterHUD>(GetHUD());
}


UShooterPersistentUser* AShooterPlayerController::GetPersistentUser() const
{
	UShooterLocalPlayer* const ShooterLocalPlayer = GetValidObjectAs<UShooterLocalPlayer>(Player);
	return ShooterLocalPlayer ? ShooterLocalPlayer->GetPersistentUser() : nullptr;
}

bool AShooterPlayerController::SetPause(bool bPause, FCanUnpause CanUnpauseDelegate /*= FCanUnpause()*/)
{
	const bool Result = APlayerController::SetPause(bPause, CanUnpauseDelegate);

	// Update rich presence.
	const auto PresenceInterface = Online::GetPresenceInterface();
	const auto Events = Online::GetEventsInterface();

	// Don't send pause events while online since the game doesn't actually pause
	if(GetNetMode() == NM_Standalone && Events.IsValid() && PlayerState->UniqueId.IsValid())
	{
		FOnlineEventParms Params;
		Params.Add( TEXT( "GameplayModeId" ), FVariantData( (int32)1 ) ); // @todo determine game mode (ffa v tdm)
		Params.Add( TEXT( "DifficultyLevelId" ), FVariantData( (int32)0 ) ); // unused
		if(Result && bPause)
		{
			Events->TriggerEvent(*PlayerState->UniqueId, TEXT("PlayerSessionPause"), Params);
		}
		else
		{
			Events->TriggerEvent(*PlayerState->UniqueId, TEXT("PlayerSessionResume"), Params);
		}
	}

	return Result;
}

FVector AShooterPlayerController::GetFocalLocation() const
{
	const AShooterCharacter* ShooterCharacter = GetValidObjectAs<AShooterCharacter>(GetPawn());

	// On death we want to use the player's death cam location rather than the location of where the pawn is at the moment
	// This guarantees that the clients see their death cam correctly, as their pawns have delayed destruction.
	if (ShooterCharacter && ShooterCharacter->bIsDying)
	{
		return GetSpawnLocation();
	}

	return Super::GetFocalLocation();
}

void AShooterPlayerController::InitPlayerState()
{
	Super::InitPlayerState();

#if WITH_EDITOR
	auto TargetPlayerState = GetValidObjectAs<AShooterPlayerState>(PlayerState);
	auto GameSingleton = FGameSingletonExtensions::Get();
	if (TargetPlayerState && GameSingleton && GameSingleton->GetDataAssets<UProfileTestData>().Num())
	{
		TargetPlayerState->InitTestData(GameSingleton->GetDataAssets<UProfileTestData>()[0]);
	}
#endif
}

ATeamInfo* AShooterPlayerController::GetTeam() const
{
	if (auto CastedState = GetValidObjectAs<AShooterPlayerState>(PlayerState))
	{
		return CastedState->GetTeam();
	}

	return nullptr;
}

uint8 AShooterPlayerController::GetTeamNum() const
{
	if (auto CastedState = GetValidObjectAs<AShooterPlayerState>(PlayerState))
	{
		return CastedState->GetTeamNum();
	}

	return NullTeam;
}

void AShooterPlayerController::OnRep_MyLastKiller()
{
	auto MySpectatorPawn = Cast<AShooterSpectatorPawn>(GetSpectatorPawn());
	if (MySpectatorPawn && MyLastKiller && LastCharacter)
	{
		if (auto MyLastChar = Cast<AShooterCharacter>(LastCharacter))
		{
			MyLastChar->GetMesh()->SetCollisionResponseToChannel(ECC_Camera, ECR_Ignore);
			MyLastChar->GetCapsuleComponent()->SetCollisionResponseToChannel(ECC_Camera, ECR_Ignore);
		}

		MySpectatorPawn->InitializeTarget();
	}
}

void AShooterPlayerController::CollectPerfomanceStatistic()
{
	//===============================================
	LocalElapsedGameTime++;

	//===============================================
	Super::CollectPerfomanceStatistic();

	//===============================================
	const auto GameMapName = GetGameMapName();

	//===============================================
	auto gs = GetGameStateAs<AShooterGameState>();
	if (gs)
	{
		//------------------------------------------
		auto RemainingTime = gs->GetRemainingTime();

		if (RemainingTime % 5 == 0)
		{
			//------------------------------------------
			auto TimeLimit = gs->GetTimeLimit();
			auto ElapsedTime = TimeLimit - RemainingTime;

			//------------------------------------------
			FShooterGameAnalytics::RecordDesignEventRange("Perfomance:PlayGameTime:Global:PlayedMore[Key]", ElapsedTime, { 300, 270, 240, 210, 180, 150, 120, 90, 60, 50, 45, 40, 35, 30, 25, 20, 15, 10, 5, 0 });

			//------------------------------------------
			const auto EventName3 = FString::Printf(TEXT("Perfomance:PlayGameTime:%s:PlayedMore[Key]"), *GameMapName);

			//------------------------------------------
			FShooterGameAnalytics::RecordDesignEventRange(EventName3, ElapsedTime, { 300, 270, 240, 210, 180, 150, 120, 90, 60, 50, 45, 40, 35, 30, 25, 20, 15, 10, 5, 0 });
		}

	}

	//===============================================
	if (LocalElapsedGameTime % 5 == 0)
	{
		//------------------------------------------
		FShooterGameAnalytics::RecordDesignEventRange("Perfomance:PlayGameTimeLocal:Global:PlayedMore[Key]secs", LocalElapsedGameTime, { 300, 270, 240, 210, 180, 150, 120, 90, 60, 50, 45, 40, 35, 30, 25, 20, 15, 10, 5, 0 });

		//------------------------------------------
		const auto EventName3 = FString::Printf(TEXT("Perfomance:PlayGameTimeLocal:%s:PlayedMore[Key]secs"), *GameMapName);

		//------------------------------------------
		FShooterGameAnalytics::RecordDesignEventRange(EventName3, LocalElapsedGameTime, { 300, 270, 240, 210, 180, 150, 120, 90, 60, 50, 45, 40, 35, 30, 25, 20, 15, 10, 5, 0 });
	}

}


void AShooterPlayerController::ProccesAutofire(float InDeltaTime)
{
	if (UShooterGameUserSettings::Get() && UShooterGameUserSettings::Get()->IsEnabledAutoFire())
	{
		AShooterCharacter* MyCharacter = GetValidObjectAs<AShooterCharacter>(GetCharacter());
		AShooterWeapon* MyWeapon = MyCharacter ? MyCharacter->GetWeapon() : nullptr;

		if (FMath::Fmod(InDeltaTime, .1f) == InDeltaTime && MyWeapon && MyWeapon->CanFire() && MyCharacter->IsRunning() == false)
		{
			if (const auto Config = MyWeapon->GetWeaponConfiguration())
			{
				const FVector AimDir = MyWeapon->GetAdjustedAim();
				const FVector StartTrace = MyWeapon->GetCameraDamageStartLocation(AimDir);
				const FVector EndTrace = StartTrace + AimDir * (Config->Range * 100.0f);
				const FHitResult HitResult = MyWeapon->WeaponTrace(StartTrace, EndTrace);

				if (auto TargetAsCharacter = GetValidObjectAs<AShooterCharacter>(HitResult.GetActor()))
				{
					if (auto MyGameState = GetWorld()->GetGameState<AShooterGameState>())
					{
						if (MyGameState->HasSameTeam(this, TargetAsCharacter))
						{
							if (MyWeapon->GetCurrentState() == EWeaponState::Firing)
							{
								MyCharacter->OnStopFire();
							}
						}
						else
						{
							if (MyWeapon->GetCurrentState() == EWeaponState::Idle)
							{
								if (MyCharacter->IsFiring()) MyCharacter->OnStopFire();
								MyCharacter->OnStartFire();
							}
						}
					}
				}
				else
				{
					if (MyWeapon->GetCurrentState() == EWeaponState::Firing)
					{
						MyCharacter->OnStopFire();
					}
				}
			}
		}
	}
}

void AShooterPlayerController::PreClientTravel(const FString& PendingURL, ETravelType TravelType, bool bIsSeamlessTravel)
{
	Super::PreClientTravel( PendingURL, TravelType, bIsSeamlessTravel );

	if ( GetWorld() != NULL )
	{
		UShooterGameViewportClient* ShooterViewport = Cast<UShooterGameViewportClient>( GetWorld()->GetGameViewport() );

		if ( ShooterViewport != NULL )
		{
			ShooterViewport->ShowLoadingScreen();
		}
	}
}
