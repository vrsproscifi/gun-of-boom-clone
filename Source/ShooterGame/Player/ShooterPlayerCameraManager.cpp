// Copyright 1998-2018 Epic Games, Inc. All Rights Reserved.

#include "ShooterGame.h"
#include "Player/ShooterPlayerCameraManager.h"
#include "ShooterCharacter.h"
#include "Inventory/ShooterWeapon.h"
#include "WeaponItemEntity.h"

AShooterPlayerCameraManager::AShooterPlayerCameraManager(const FObjectInitializer& ObjectInitializer) : Super(ObjectInitializer)
{
	NormalFOV = 105.0f;
	TargetingFOV = 75.0f;
	ViewPitchMin = -87.0f;
	ViewPitchMax = 87.0f;
	bAlwaysApplyModifiers = true;
}

void AShooterPlayerCameraManager::UpdateCamera(float DeltaTime)
{
	AShooterCharacter* MyPawn = PCOwner ? GetValidObjectAs<AShooterCharacter>(PCOwner->GetPawn()) : NULL;
	if (MyPawn && MyPawn->IsFirstPerson())
	{
		float TargetSpeed = 10.0f;
		float TargetFOV = MyPawn->IsTargeting() ? TargetingFOV : NormalFOV;;

		if (MyPawn->IsTargeting() && MyPawn->GetWeapon() && MyPawn->GetWeapon()->GetInstanceEntity())
		{
			const auto& EntityWeaponCfg = MyPawn->GetWeapon()->GetInstanceEntity()->GetWeaponConfiguration();
			TargetFOV = NormalFOV / (EntityWeaponCfg.TargetingZoomDistance / 50.0f);

			if (EntityWeaponCfg.HasScope)
			{
				TargetSpeed = 30.0f;
			}
		}

		if (auto MyMovement = MyPawn->GetCharacterMovement())
		{
			//const float CurrentSpeed = MyMovement->GetMaxSpeed();

			const bool bIsScopeTargeting = FMath::IsNearlyEqual(TargetSpeed, 30.0f, 2.0f);
			TargetFOV = FMath::Lerp(TargetFOV, bIsScopeTargeting ? TargetFOV : 100.0f, FMath::Clamp(MyMovement->GetLastUpdateVelocity().SizeSquared() / FMath::Square<float>(MyMovement->MaxWalkSpeed * MyPawn->GetRunningSpeedModifier()), .0f, 1.2f));
			if (bIsScopeTargeting == false)
			{
				TargetSpeed = 10.0f;
			}
		}

		SetFOV(FMath::FInterpTo(GetFOVAngle(), TargetFOV, DeltaTime, TargetSpeed));
	}

	Super::UpdateCamera(DeltaTime);

	if (MyPawn && MyPawn->IsFirstPerson())
	{
		MyPawn->OnCameraUpdate(GetCameraLocation(), GetCameraRotation());
	}

	if (IsValidObject(MovementShake))
	{
		if (MyPawn && MyPawn->IsFirstPerson())
		{
			if (auto MyMovement = MyPawn->GetCharacterMovement())
			{
				const float ShakeScale = FMath::Clamp(MyMovement->GetLastUpdateVelocity().SizeSquared() / FMath::Square<float>(MyMovement->MaxWalkSpeed * MyPawn->GetRunningSpeedModifier()), .0f, 1.2f);
				MovementShake->ShakeScale = ShakeScale;
			}
		}
	}
	else
	{
		if (MyPawn && MyPawn->IsFirstPerson())
		{
			MovementShake = PlayCameraShake(MyPawn->GetMovementShake());
		}
	}
}
