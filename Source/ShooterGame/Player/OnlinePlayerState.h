#pragma once
//=============================================
#include "BasicPlayerState.h"

//=============================================
#include "OnlinePlayerState.generated.h"


class SSettingsManager;
//=============================================
class UIdentityComponent;
class USquadComponent;
class UArsenalComponent;
class UProgressComponent;
class UTutorialPlayerComponent;
//=============================================
class UBasicPlayerItem;


//=============================================

//=============================================
UCLASS(BlueprintType, Blueprintable)
class AOnlinePlayerState : public ABasicPlayerState
{
	GENERATED_UCLASS_BODY()

#pragma region Components
private:
	UPROPERTY(Transient)	UIdentityComponent*			IdentityComponent;
	UPROPERTY(Transient)	UArsenalComponent*			InventoryComponent;
	UPROPERTY(Transient)	USquadComponent*			SquadComponent;
	UPROPERTY(Transient)	UProgressComponent*			ProgressComponent;
	UPROPERTY(Transient)	UTutorialPlayerComponent*	TutorialComponent;

public:
	UFUNCTION(BlueprintCallable, BlueprintPure, Category = Component)
	UIdentityComponent* GetIdentityComponent() const;

	UFUNCTION(BlueprintCallable, BlueprintPure, Category = Component)
	UArsenalComponent* GetInventoryComponent() const;

	UFUNCTION(BlueprintCallable, BlueprintPure, Category = Component)
	USquadComponent* GetSquadComponent() const;

	UFUNCTION(BlueprintCallable, BlueprintPure, Category = Component)
	UProgressComponent* GetProgressComponent() const;

	UFUNCTION(BlueprintCallable, BlueprintPure, Category = Component)
	UTutorialPlayerComponent* GetTutorialComponent() const;

	UFUNCTION(BlueprintCallable, BlueprintPure)
	bool HasPremiumAccount() const;

#pragma endregion

	virtual void EndPlay(const EEndPlayReason::Type EndPlayReason) override;

protected:

	TSharedPtr<SSettingsManager>		Slate_SettingsManager;

	UFUNCTION()		virtual void OnOwnerAuthorization(const FGuid& InOwnerToken);
	UFUNCTION()		virtual void OnInventoryUpdated(const TArray<UBasicPlayerItem*> InData, const bool IsInternal = true);

	/*	
	 *	Насильно обновляем информацию об игроке
	 */
	UFUNCTION()		virtual void SendRequestPlayerDataUpdate();

	virtual void OnFullyLoadedData() override;

	virtual void OnInitializeCharacter() override;

public:
	UFUNCTION(BlueprintCallable, Category = Inventory)
	void UsePlayerItem(const FGuid& InPlayerItemId, const int32& InAmount);

	virtual void OnCreateUserInterface() override;

	static bool CheckAndRequestAndroidPermission(const FString& InPermission, const FText& InMessage = FText::GetEmpty());

protected:
	
	static void OnGrantResultsAndroidPermission(const TArray<FString>& InPermissions, const TArray<bool>& InGrantResults);
	void RequestUpdateCurrencyRegion();
};