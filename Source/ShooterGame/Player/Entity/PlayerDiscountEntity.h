#pragma once

//=======================================================
#include "BasicPlayerEntity.h"
#include "ArsenalComponent/PlayerDiscountModel.h"

//=======================================================
#include "PlayerDiscountEntity.generated.h"

//=======================================================

UCLASS(ClassGroup = (Utility, Common), BlueprintType, hideCategories = (Trigger, PhysicsVolume), meta = (BlueprintSpawnableComponent))
class SHOOTERGAME_API UPlayerDiscountEntity : public UBasicPlayerEntity
{
	GENERATED_UCLASS_BODY()

	//-----------------------------------------------------------
public:
	virtual void OnRep_Instance() override;
	virtual void Initialize(const FPlayerDiscountModel& InModel);

	//-----------------------------------------------------------

#pragma region LastActiveDate
protected:
	UPROPERTY(VisibleInstanceOnly)
	FDateTime EndDiscountDate;

public:
	UFUNCTION(BlueprintCallable)
	void SetEndDiscountDate(const FDateTime& InDate);

	UFUNCTION()
	void SetEndDiscountDateFromUnix(const int64& InUnixDate);
	
	UFUNCTION(BlueprintCallable, BlueprintPure)
	FTimespan GetAvalibleTime() const;

	UFUNCTION(BlueprintCallable, BlueprintPure)
		bool IsAvalible() const;

#pragma endregion 

public:
	UFUNCTION(BlueprintCallable)
		void SetRequredUpdate(const bool& status);

	UFUNCTION(BlueprintCallable, BlueprintPure)
		bool IsRequredUpdate() const;

	UFUNCTION(BlueprintCallable)
	void SetDiscountCoupone(const EProductDiscountCoupone& InDiscount);

	UFUNCTION(BlueprintCallable, BlueprintPure)
	const EProductDiscountCoupone& GetDiscountCoupone() const;

protected:
	UPROPERTY(VisibleInstanceOnly)	bool bIsRequredUpdate;
	UPROPERTY(VisibleInstanceOnly)	EProductDiscountCoupone DiscountCoupone;
	//-----------------------------------------------------------

};