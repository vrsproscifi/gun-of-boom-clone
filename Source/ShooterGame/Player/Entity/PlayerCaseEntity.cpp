#include "ShooterGame.h"
#include "PlayerCaseEntity.h"
#include "Case/CaseEntity.h"


#include "ArsenalComponent/PlayerCaseModel.h"
#include "GameSingletonExtensions.h"

UPlayerCaseEntity::UPlayerCaseEntity(const FObjectInitializer& ObjectInitializer)
	: Super(ObjectInitializer)
	, Amount(0)
	, bIsRequredUpdate(true)
{

}

UCaseEntity* UPlayerCaseEntity::GetCaseEntity() const
{
	return GetEntity<UCaseEntity>();
}

UCaseEntity* UPlayerCaseEntity::GetCaseEntity()
{
	return GetEntity<UCaseEntity>();
}

const FCaseProperty* UPlayerCaseEntity::GetCaseEntityProperty() const
{
	if (const auto entity = GetCaseEntity())
	{
		return &entity->GetCaseProperty();
	}
	return nullptr;
}

bool UPlayerCaseEntity::AllowGetBonusCase()
{
	const auto CaseEntityProperty = GetCaseEntityProperty();
	if (CaseEntityProperty)
	{
		return bAllowGetBonusCase && CaseEntityProperty->bAllowGetBonusCase;
	}
	return false;
}
const FCaseProperty* UPlayerCaseEntity::GetCaseEntityProperty()
{
	if (const auto entity = GetCaseEntity())
	{
		return &entity->GetCaseProperty();
	}
	return nullptr;
}

bool UPlayerCaseEntity::IsEveryDayCase() const
{
	return GetCaseEntityProperty() && GetCaseEntityProperty()->IsEveryDayCase();
}

void UPlayerCaseEntity::Initialize(UCaseEntity* InEntity, const FPlayerCaseModel& InCaseModel)
{
	SetEntity(InEntity);
	Initialize(InCaseModel);
}

void UPlayerCaseEntity::Initialize(const FPlayerCaseModel& InCaseModel)
{
	SetLastActiveDateFromUnix(InCaseModel.LastActiveDate);
	SetEntityModel(InCaseModel.CaseId);
	SetEntityId(InCaseModel.EntityId);
	SetAmount(InCaseModel.Amount);
}

void UPlayerCaseEntity::OnRep_Instance()
{
	bool bHasEntityInstance = GetCaseEntity() != nullptr;
	if (const auto instance = FGameSingletonExtensions::FindCaseById(GetModelId()))
	{
		//==================================
		SetEntity_Intrenal(instance);

		//==================================
		if (bHasEntityInstance == false)
		{
			bAllowGetBonusCase = instance->GetCaseProperty().bAllowGetBonusCase;
		}
	}
}


void UPlayerCaseEntity::SetLastActiveDate(const FDateTime& InDate)
{
	if (LastActiveDate != InDate)
	{
		SetRequredUpdate(true);
	}
	LastActiveDate = InDate;


}

void UPlayerCaseEntity::SetLastActiveDateFromUnix(const int64& InUnixDate)
{
	SetLastActiveDate(FDateTime::FromUnixTimestamp(InUnixDate));
}


const FDateTime& UPlayerCaseEntity::GetLastActiveDateFromUnix() const
{
	return LastActiveDate;
}

bool UPlayerCaseEntity::IsElapsedLastActive(bool InDefautResult) const
{
	if (const auto property = GetCaseEntityProperty())
	{
		return GetWaitingTime() <= FTimespan::Zero();
	}
	return InDefautResult;
}

void UPlayerCaseEntity::SetRequredUpdate(const bool& status)
{
	bIsRequredUpdate = status;
}

bool UPlayerCaseEntity::IsRequredUpdate() const
{
	return bIsRequredUpdate;
}

FDateTime UPlayerCaseEntity::GetNextCaseAvalibleDate() const
{
	if (const auto property = GetCaseEntityProperty())
	{
		//=====================================
		//	12:00 + 0:30 = 12:30
		const auto NextCaseAvalibleDate = LastActiveDate + property->Delay;

		//=====================================
		return NextCaseAvalibleDate;
	}

	return LastActiveDate;
}

FTimespan UPlayerCaseEntity::GetWaitingTime() const
{
	//	12:00 + 0:30 = 12:30
	const auto NextCaseAvalibleDate = GetNextCaseAvalibleDate();
	
	//	12:30 - 12:00 - кейс можно будет использовать через 30 м
	//	12:30 - 14:00 - кейс можно использовать
	const auto WaitingTimeToNextAvalibleDate = NextCaseAvalibleDate - FDateTime::UtcNow();

	return WaitingTimeToNextAvalibleDate;
}

void UPlayerCaseEntity::SetAmount(const int32& InAmount)
{
	Amount = static_cast<int16>(FMath::Clamp<int32>(InAmount, 0, 65535));
}

int32 UPlayerCaseEntity::GetAmount() const
{
	return Amount;
}

bool UPlayerCaseEntity::AnyAmount() const
{
	return GetAmount() > 0;
}