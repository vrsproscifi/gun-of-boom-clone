#include "ShooterGame.h"
#include "PlayerDiscountEntity.h"
#include "Item/Product/ProductItemEntity.h"
#include "GameSingletonExtensions.h"

UPlayerDiscountEntity::UPlayerDiscountEntity(const FObjectInitializer& ObjectInitializer)
	: Super(ObjectInitializer)
	, bIsRequredUpdate(true)
	, DiscountCoupone(EProductDiscountCoupone::None)
{

}

void UPlayerDiscountEntity::Initialize(const FPlayerDiscountModel& InCaseModel)
{
	SetEndDiscountDateFromUnix(InCaseModel.EndDiscountDate);
	SetDiscountCoupone(InCaseModel.Coupone);
	SetEntityModel(InCaseModel.ModelId);
	SetEntityId(InCaseModel.EntityId);
}

void UPlayerDiscountEntity::OnRep_Instance()
{
	if (const auto instance = FGameSingletonExtensions::FindItemById(GetModelId()))
	{
		instance->SetActiveDiscount(GetDiscountCoupone(), EndDiscountDate);
		SetEntity_Intrenal(instance);
	}
}

void UPlayerDiscountEntity::SetEndDiscountDate(const FDateTime& InDate)
{
	if (EndDiscountDate != InDate)
	{
		SetRequredUpdate(true);
	}
	EndDiscountDate = InDate;
}

void UPlayerDiscountEntity::SetEndDiscountDateFromUnix(const int64& InUnixDate)
{
	SetEndDiscountDate(FDateTime::FromUnixTimestamp(InUnixDate));
}

void UPlayerDiscountEntity::SetRequredUpdate(const bool& status)
{
	bIsRequredUpdate = status;
}

bool UPlayerDiscountEntity::IsRequredUpdate() const
{
	return bIsRequredUpdate;
}

bool UPlayerDiscountEntity::IsAvalible() const
{
	return GetAvalibleTime() > FTimespan::Zero();
}

void UPlayerDiscountEntity::SetDiscountCoupone(const EProductDiscountCoupone& InDiscount)
{
	DiscountCoupone = InDiscount;
}

const EProductDiscountCoupone& UPlayerDiscountEntity::GetDiscountCoupone() const
{
	return DiscountCoupone;
}

FTimespan UPlayerDiscountEntity::GetAvalibleTime() const
{
	return EndDiscountDate - FDateTime::UtcNow();
}
