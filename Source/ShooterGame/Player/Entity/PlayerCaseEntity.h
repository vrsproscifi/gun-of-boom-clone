#pragma once

//=======================================================
#include "BasicPlayerEntity.h"

//=======================================================
#include "PlayerCaseEntity.generated.h"

struct FPlayerCaseModel;
//=======================================================
struct FCaseProperty;
class UCaseEntity;

//=======================================================

UCLASS(ClassGroup = (Utility, Common), BlueprintType, hideCategories = (Trigger, PhysicsVolume), meta = (BlueprintSpawnableComponent))
class SHOOTERGAME_API UPlayerCaseEntity : public UBasicPlayerEntity
{
	GENERATED_UCLASS_BODY()

	//-----------------------------------------------------------
#pragma region CaseEntity
public:
	virtual void OnRep_Instance() override;
	virtual void Initialize(UCaseEntity* InEntity, const FPlayerCaseModel& InCaseModel);
	virtual void Initialize(const FPlayerCaseModel& InCaseModel);

	UFUNCTION(BlueprintCallable)
	UCaseEntity* GetCaseEntity() const;
	UCaseEntity* GetCaseEntity();

	const FCaseProperty* GetCaseEntityProperty() const;
	const FCaseProperty* GetCaseEntityProperty();

	UFUNCTION(BlueprintCallable)
	bool AllowGetBonusCase();

	UFUNCTION(BlueprintCallable)
	bool IsEveryDayCase() const;

#pragma endregion 


	//-----------------------------------------------------------

public:
	void UseBonusCase()
	{
		bAllowGetBonusCase = false;
	}

#pragma region LastActiveDate
protected:
	UPROPERTY(VisibleInstanceOnly)
	bool bAllowGetBonusCase;

	UPROPERTY(VisibleInstanceOnly)
	FDateTime LastActiveDate;

public:
	UFUNCTION(BlueprintCallable)
	void SetLastActiveDate(const FDateTime& InDate);

	UFUNCTION()
	void SetLastActiveDateFromUnix(const int64& InUnixDate);

	UFUNCTION(BlueprintCallable, BlueprintPure)
		FDateTime GetNextCaseAvalibleDate() const;

	UFUNCTION(BlueprintCallable, BlueprintPure)
	const FDateTime& GetLastActiveDateFromUnix() const;

	UFUNCTION(BlueprintCallable, BlueprintPure)
		bool IsElapsedLastActive(bool InDefautResult = true) const;

	UFUNCTION(BlueprintCallable, BlueprintPure)
	FTimespan GetWaitingTime() const;

#pragma endregion 



	//-----------------------------------------------------------
#pragma region Amount
public:
	UFUNCTION(BlueprintCallable)
		void SetAmount(const int32& InAmount);
	
	UFUNCTION(BlueprintCallable, BlueprintPure)
		int32 GetAmount() const;
	
	UFUNCTION(BlueprintCallable, BlueprintPure)
		bool AnyAmount() const;

	UFUNCTION(BlueprintCallable)
		void SetRequredUpdate(const bool& status);

	UFUNCTION(BlueprintCallable, BlueprintPure)
		bool IsRequredUpdate() const;

protected:
	UPROPERTY(VisibleInstanceOnly)	int16 Amount;
	UPROPERTY(VisibleInstanceOnly)	bool bIsRequredUpdate;
#pragma endregion 
};