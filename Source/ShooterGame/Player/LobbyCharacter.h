// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "GameFramework/SpringArmComponent.h"
#include "Camera/CameraComponent.h"

#include "Player/BasicPlayerCharacter.h"
#include "LobbyCharacter.generated.h"

/**
 * 
 */
UCLASS()
class SHOOTERGAME_API ALobbyCharacter : public ABasicPlayerCharacter
{
	GENERATED_UCLASS_BODY()
	
	// ABasicPlayerCharacter overrides begin
	virtual void PostInitializeComponents() override;
	virtual void TurnAtRate(float InValue) override;
	virtual void TurnAtValue(float InValue) override;

	virtual void LookUpAtRate(float InValue) override;
	virtual void LookUpAtValue(float InValue) override;
	
	virtual void InitializeItem(UBasicPlayerItem* InItem, bool IsDefault = false) override;

	void SetArmMode(bool InIsAlternative);

	USkeletalMeshComponent* GetWeaponComponent() const { return WeaponPreview; }

protected:

	void Tick(float DeltaSeconds) override;

	virtual void OnRep_Instance() override;

	// ABasicPlayerCharacter overrides end

	UPROPERTY(VisibleAnywhere, BlueprintReadOnly)
	USpringArmComponent* SpringArm;

	UPROPERTY(VisibleAnywhere, BlueprintReadOnly)
	UCameraComponent* Camera;

	UPROPERTY(VisibleAnywhere, BlueprintReadOnly)
	USkeletalMeshComponent* WeaponPreview;

	UPROPERTY(EditDefaultsOnly, BlueprintReadOnly, Category = Camera)
	FVector ArmOffsetNormal;

	UPROPERTY(EditDefaultsOnly, BlueprintReadOnly, Category = Camera)
	FVector ArmOffsetAlternative;

	UPROPERTY()
	bool bIsArmAlternativeMode;
};
