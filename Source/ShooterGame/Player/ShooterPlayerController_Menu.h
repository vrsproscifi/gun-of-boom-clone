// Copyright 1998-2018 Epic Games, Inc. All Rights Reserved.

#pragma once

#include "Game/OnlinePlayerController.h"
#include "ShooterPlayerController_Menu.generated.h"


UCLASS()
class AShooterPlayerController_Menu : public AOnlinePlayerController
{
	GENERATED_UCLASS_BODY()

	/** After game is initialized */
	virtual void PostInitializeComponents() override;

	virtual void OnEscape() override;
};

