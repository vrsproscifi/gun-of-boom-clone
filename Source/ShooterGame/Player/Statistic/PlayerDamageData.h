#pragma once
#include "PlayerDamageData.generated.h"

class APlayerState;

USTRUCT(BlueprintType)
struct FPlayerDamageData
{
	GENERATED_USTRUCT_BODY();

	UPROPERTY(EditAnywhere, BlueprintReadWrite)
		int32 Weapon;

	UPROPERTY(EditAnywhere, BlueprintReadWrite)
		float Damage;

	UPROPERTY(EditAnywhere, BlueprintReadWrite)
		APlayerState* Owner;

	FPlayerDamageData()
		: Weapon(0)
		, Damage(0)
		, Owner(nullptr)
	{
		
	}

	static FPlayerDamageData GetMaxDamagedData(const TMap<int32, FPlayerDamageData>& InDamageMap)
	{
		//====================================
		FPlayerDamageData MaxDamage;

		//====================================
		TArray<FPlayerDamageData> DamageArray;

		//====================================
		InDamageMap.GenerateValueArray(DamageArray);

		//====================================
		for(const auto& damage : DamageArray)
		{
			if(damage.Damage >MaxDamage.Damage)
			{
				MaxDamage = damage;
			}
		}

		//====================================
		return MaxDamage;
	}

	FPlayerDamageData& operator+=(const FPlayerDamageData& Other)
	{
		this->Weapon = Other.Weapon;
		this->Damage += Other.Damage;
		this->Owner = Other.Owner;

		return *this;
	}
};