#pragma once
#include "PlayerShootData.generated.h"

USTRUCT(BlueprintType)
struct FPlayerShootData
{
	GENERATED_USTRUCT_BODY();

	UPROPERTY(EditAnywhere, BlueprintReadWrite)
		int32 Weapon;

	UPROPERTY(EditAnywhere, BlueprintReadWrite)
		uint8 Surface;

	UPROPERTY(EditAnywhere, BlueprintReadWrite)
		bool IsPawn_Friendly;

	UPROPERTY(EditAnywhere, BlueprintReadWrite)
		bool IsPawn_Enemy;

	FPlayerShootData() : Weapon(-1), Surface(0), IsPawn_Friendly(false), IsPawn_Enemy(false) {}
};
