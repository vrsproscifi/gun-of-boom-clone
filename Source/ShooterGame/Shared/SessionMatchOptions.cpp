#include "ShooterGame.h"
#include "SessionMatchOptions.h"

#include "Game/GameMapData.h"
#include "Game/GameModeData.h"


FString FSessionMatchOptions::ToString() const
{
	TMap<FString, FStringFormatArg> args;
	args.Add("Header", FString("\n ============= Options ============= \r\n"));

	auto GameMapInstance = UGameMapData::GetById(GameMap);
	auto GameModeInstance = UGameModeData::GetById(GameMode);

	if (GameMapInstance)
	{
		args.Add("GameMap", FStringFormatArg(FString::Printf(TEXT("%d | %s"), static_cast<int32>(GameMap), *GameMapInstance->GetName())));
	}
	else
	{
		args.Add("GameMap", FStringFormatArg(FString::Printf(TEXT("%d | null"), static_cast<int32>(GameMap))));
	}

	if (GameModeInstance)
	{
		args.Add("GameMode", FStringFormatArg(FString::Printf(TEXT("%d | %s"), static_cast<int32>(GameMode), *GameModeInstance->GetName())));
	}
	else
	{
		args.Add("GameMode", FStringFormatArg(FString::Printf(TEXT("%d | null"), static_cast<int32>(GameMode))));
	}

	args.Add("UniversalId", UniversalId.ToString());
	args.Add("RoundTime", RoundTimeInSeconds);

	args.Add("Difficulty", Difficulty);
	args.Add("AdditionalFlag", AdditionalFlag);

	args.Add("NumberOfBots", NumberOfBots);
	args.Add("ArmourLevel", ArmourLevel);

	//args.Add("Weapon", FStringFormatArg(FString::Printf(TEXT("%d | %s"), WeaponType, *EWeaponType(WeaponType).ToString())));

	return FString::Format(TEXT("{Header} GameMap: {GameMap}\r\n GameMode: {GameMode}\r\n UniversalId: {UniversalId}\r\n RoundTime: {RoundTime}\r\n Difficulty: {Difficulty}\r\n AdditionalFlag: {AdditionalFlag}\r\n NumberOfBots: {NumberOfBots}\r\n ArmourLevel: {ArmourLevel}\r\n Weapon: {Weapon}\r\n"), args);
}