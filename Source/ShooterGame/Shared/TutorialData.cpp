// Fill out your copyright notice in the Description page of Project Settings.

#include "ShooterGame.h"
#include "TutorialData.h"

UTutorialData::UTutorialData()
{
	Title = FText::FromString("Tutorial");
}

FText UTutorialData::GetTitle() const
{
	return Title;
}

int32 UTutorialData::GetStagesNum() const
{
	return Stages.Num();
}

const TArray<FTutorialInfo>& UTutorialData::GetStages() const
{
	return Stages;
}

const FTutorialInfo& UTutorialData::GetStage(const int32& InStage) const
{
	if (Stages.Num() && Stages.IsValidIndex(InStage))
	{
		return Stages[InStage];
	}

	static FTutorialInfo EmptyInfo;
	return EmptyInfo;
}
