// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "Engine/DataAsset.h"
#include "NotifySoundsData.generated.h"

/**
 * 
 */
UCLASS()
class SHOOTERGAME_API UNotifySoundsData : public UDataAsset
{
	GENERATED_BODY()
	
public:

	UPROPERTY(BlueprintReadOnly, EditDefaultsOnly, Category = Notify)		FSlateSound				Notify_ChangeMoney;
	UPROPERTY(BlueprintReadOnly, EditDefaultsOnly, Category = Notify)		FSlateSound				Notify_ChangeDonate;
	UPROPERTY(BlueprintReadOnly, EditDefaultsOnly, Category = Notify)		FSlateSound				Notify_BuyArsenalItem;
	UPROPERTY(BlueprintReadOnly, EditDefaultsOnly, Category = Notify)		FSlateSound				Notify_BuyShopItem;
	UPROPERTY(BlueprintReadOnly, EditDefaultsOnly, Category = Notify)		FSlateSound				Notify_BuyImprovement;
	UPROPERTY(BlueprintReadOnly, EditDefaultsOnly, Category = Notify)		FSlateSound				Notify_FightSearch;
	UPROPERTY(BlueprintReadOnly, EditDefaultsOnly, Category = Notify)		FSlateSound				Notify_FightFound;

	UPROPERTY(BlueprintReadOnly, EditDefaultsOnly, Category = Notify)		UForceFeedbackEffect*	Feedback_FightSearch;
	UPROPERTY(BlueprintReadOnly, EditDefaultsOnly, Category = Notify)		UForceFeedbackEffect*	Feedback_FightFound;
};
