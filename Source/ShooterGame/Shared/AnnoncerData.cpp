// Fill out your copyright notice in the Description page of Project Settings.

#include "ShooterGame.h"
#include "AnnoncerData.h"

UAnnoncerData::UAnnoncerData()
{
	
}

bool UAnnoncerData::IsExistSoundForTime(const int32& InSeconds) const
{
	return RemainingTimeSounds.Contains(InSeconds);
}

USoundCue* UAnnoncerData::GetSoundForTime(const int32& InSeconds) const
{
	if (RemainingTimeSounds.Contains(InSeconds))
	{
		return RemainingTimeSounds.FindChecked(InSeconds);
	}

	return nullptr;
}
