﻿#pragma once
#include "AttachAccountService.generated.h"

UENUM()
namespace EAccountService
{
	enum Type
	{
		None,
		Google,
		Facebook,
		Vkontakte,
		END,
	};
}

UENUM()
enum class EReAttachAccountServicePolicy : uint8
{
	None,

	// Использовать текущий
	UseCurrent,

	// Использовать предыдущий
	UsePrevious
};


USTRUCT()
struct FAttachAccountService
{
	GENERATED_USTRUCT_BODY()
	UPROPERTY()		TEnumAsByte<EAccountService::Type> Service;
	UPROPERTY()		FString Token;
	UPROPERTY()		FString Login;
};

USTRUCT()
struct FAttachAccountServiceExistSuccessfully
{
	GENERATED_USTRUCT_BODY()
	UPROPERTY()		FString Token;
	UPROPERTY()		FString Account;
};



USTRUCT()
struct FAttachAccountServiceExist : public FAttachAccountService
{
	GENERATED_USTRUCT_BODY()
		UPROPERTY()		FString Name;
	UPROPERTY()		int64 Level;
	UPROPERTY()		int64 Money;
	UPROPERTY()		int64 Donate;
	UPROPERTY()		int64 CreatedDate;
	UPROPERTY()		int64 LastActivityDate;

};


USTRUCT()
struct FReAttachAccountService : public FAttachAccountService
{
	GENERATED_USTRUCT_BODY()
	UPROPERTY()		EReAttachAccountServicePolicy Policy;
	
	FReAttachAccountService(){}
	FReAttachAccountService(const FAttachAccountService& account, bool UseLatestAccount)
	{
		if (UseLatestAccount)
		{
			Policy = EReAttachAccountServicePolicy::UsePrevious;
		}
		else
		{
			Policy = EReAttachAccountServicePolicy::UseCurrent;
		}

		Service = account.Service;
		Token = account.Token;
		Login = account.Login;

	}
	

};




DECLARE_DELEGATE_TwoParams(FOnAttachServiceSwitchClicked, const TEnumAsByte<EAccountService::Type>&, const bool&);
DECLARE_DELEGATE_RetVal_OneParam(bool, FOnGetServiceStatus, const TEnumAsByte<EAccountService::Type>&);