// Fill out your copyright notice in the Description page of Project Settings.

#include "ShooterGame.h"
#include "TeamColorsData.h"

UTeamColorsData::UTeamColorsData()
{
	LocalColor = FColorList::Yellow;
	FriendlyColor = FColorList::LightBlue;
	FriendlyColorParameters.Add(TEXT("FriendlyColor"));
	EnemyColor = FColorList::Orange;
	EnemyColorParameters.Add(TEXT("EnemyColor"));
}
