// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "Engine/DataAsset.h"
#include "ProfileTestData.generated.h"

/**
 * 
 */
UCLASS()
class SHOOTERGAME_API UProfileTestData : public UDataAsset
{
	GENERATED_BODY()
	
public:

	UProfileTestData();

	UFUNCTION(BlueprintCallable, BlueprintPure, Category = "Game|Profile Test Data")
	FORCEINLINE TArray<int32> GetItemsModelId() const { return ItemsModelId; }

protected:
	
	UPROPERTY(EditDefaultsOnly, Category = Character)
	TArray<int32> ItemsModelId;
	
};
