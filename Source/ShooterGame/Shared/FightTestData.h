// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "Engine/DataAsset.h"
#include "FightTestData.generated.h"

/**
 * 
 */
UCLASS()
class SHOOTERGAME_API UFightTestData : public UDataAsset
{
	GENERATED_BODY()
	
public:

	UFightTestData();
	
	UPROPERTY(EditDefaultsOnly, Category = GameMode)
	TSubclassOf<class AShooterGameMode> TargetGameMode;

	UPROPERTY(EditDefaultsOnly, Category = GameMode)
	int32 MatchTime;

	UPROPERTY(EditDefaultsOnly, Category = GameMode)
	uint8 NumTeams;

	UPROPERTY(EditDefaultsOnly, Category = GameMode)
	uint8 NumBots;

	UPROPERTY(EditDefaultsOnly, Category = GameMode)
	int32 GoalScore;
};
