#pragma once
#include "SessionMatchOptions.generated.h"

USTRUCT(BlueprintType)
struct FSessionMatchOptions
{
	GENERATED_USTRUCT_BODY()

	FSessionMatchOptions()
		: GameMap(0)
		, GameMode(0)
		, UniversalId()
		, RoundTimeInSeconds(0)
		//, WeaponType(EDuelWeaponTypes::All)
		, Difficulty(0)
		, AdditionalFlag(0)
		, NumberOfBots(0)
		, ArmourLevel(0)
	{

		
	}

	FString ToString() const;

	//============================================
	/// <summary>
	/// Целевой сервер
	/// </summary>
	UPROPERTY(EditAnywhere, BlueprintReadWrite) int32 TargetServerVersion;

	//============================================
	/// <summary>
	/// Игровая локация
	/// </summary>
	UPROPERTY(EditAnywhere, BlueprintReadWrite) int32 GameMap;


	/// <summary>
	/// Режим игры
	/// </summary>
	UPROPERTY(EditAnywhere, BlueprintReadWrite) int32 GameMode;

	/// <summary>
	/// Идентификатор игрока на которого нападают, которому было отправленно предложение на дуэль
	/// </summary>
	UPROPERTY() FLokaGuid UniversalId;

	/// <summary>
	/// Длительность раунда в секундах
	/// </summary>
	UPROPERTY(EditAnywhere, BlueprintReadWrite) int32 RoundTimeInSeconds;

	//============================================
	/// <summary>
	/// Тип оружия для дуэли / тренировки
	/// </summary>
	UPROPERTY()		int32 WeaponType;

	/// <summary>
	/// Уровень сложности ботов, если они есть
	/// </summary>
	UPROPERTY(EditAnywhere, BlueprintReadWrite) float Difficulty;

	float GetDifficulty() const
	{
		return FMath::Clamp<float>(Difficulty, 0.5, 7.9);
	}

	/// <summary>
	/// Дополнительные параметры матча
	/// <para>
	///  <see cref="GetNumberOfLives"/> - количество жизней для дуэли
	/// </para> 
	/// <para>
	///  <see cref="GetWaveNumber"/> - количество волн в PvE - режиме
	/// </para>         
	/// </summary>
	UPROPERTY(EditAnywhere, BlueprintReadWrite) int32 AdditionalFlag;

	/// <summary>
	/// Возвращает количество жизней для дуэли, полученное из AdditionalFlag
	/// </summary>
	int32 GetNumberOfLives() const
	{
		return AdditionalFlag;
	}

	UPROPERTY(EditAnywhere, BlueprintReadWrite) uint8 NumberOfBots;

	uint8 GetNumberOfBots() const
	{
		return NumberOfBots;
	}

	/// <summary>
	/// Уровень амуниции игроков
	/// </summary>
	UPROPERTY() uint8 ArmourLevel;


	/// <summary>
	/// Возвращает максимальное количество очков, полученное из AdditionalFlag
	/// </summary>
	int32 GetScoreLimit() const
	{
		return AdditionalFlag;
	}

	/// <summary>
	/// Возвращает номер волны, полученный из AdditionalFlag
	/// </summary>
	int32 GetWaveNumber() const
	{
		return AdditionalFlag;
	}

};