// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "Engine/DataAsset.h"
#include "TeamColorsData.generated.h"

/**
 * 
 */
UCLASS(Category = "Gameplay|Team")
class SHOOTERGAME_API UTeamColorsData : public UDataAsset
{
	GENERATED_BODY()
	
public:

	UTeamColorsData();
	
	UFUNCTION(BlueprintCallable, BlueprintPure, Category = Friendly)
	FORCEINLINE FLinearColor GetLocalColor() const { return LocalColor; }

	UFUNCTION(BlueprintCallable, BlueprintPure, Category = Friendly)
	FORCEINLINE FLinearColor GetFriendlyColor() const { return FriendlyColor; }

	UFUNCTION(BlueprintCallable, BlueprintPure, Category = Friendly)
	FORCEINLINE TArray<FName> GetFriendlyColorParameters() const { return FriendlyColorParameters; }

	UFUNCTION(BlueprintCallable, BlueprintPure, Category = Enemy)
	FORCEINLINE FLinearColor GetEnemyColor() const { return EnemyColor; }

	UFUNCTION(BlueprintCallable, BlueprintPure, Category = Enemy)
	FORCEINLINE TArray<FName> GetEnemyColorParameters() const { return EnemyColorParameters; }

protected:

	UPROPERTY(EditDefaultsOnly, Category = Friendly)	FLinearColor	LocalColor;
	UPROPERTY(EditDefaultsOnly, Category = Friendly)	FLinearColor	FriendlyColor;
	UPROPERTY(EditDefaultsOnly, Category = Friendly)	TArray<FName>	FriendlyColorParameters;
	UPROPERTY(EditDefaultsOnly, Category = Enemy)		FLinearColor	EnemyColor;
	UPROPERTY(EditDefaultsOnly, Category = Enemy)		TArray<FName>	EnemyColorParameters;
};
