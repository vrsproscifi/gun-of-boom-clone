// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "Engine/DataAsset.h"
#include "AnnoncerData.generated.h"

/**
 * 
 */
UCLASS()
class SHOOTERGAME_API UAnnoncerData : public UDataAsset
{
	GENERATED_BODY()

public:

	UAnnoncerData();

	UFUNCTION(BlueprintCallable, BlueprintPure, Category = "Annoncer|Time")
	bool IsExistSoundForTime(const int32& InSeconds) const;

	UFUNCTION(BlueprintCallable, BlueprintPure, Category = "Annoncer|Time")
	USoundCue* GetSoundForTime(const int32& InSeconds) const;

protected:

	UPROPERTY(EditDefaultsOnly, Category = "Annoncer|Time")
	TMap<int32, USoundCue*> RemainingTimeSounds;
};
