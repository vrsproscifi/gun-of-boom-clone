// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "Engine/DataAsset.h"
#include "HUDFontsData.generated.h"

/**
 * 
 */
UCLASS()
class SHOOTERGAME_API UHUDFontsData : public UDataAsset
{
	GENERATED_BODY()
	
public:

	UPROPERTY(EditDefaultsOnly, Category = Fonts)	FSlateFontInfo	FontSmall;
	UPROPERTY(EditDefaultsOnly, Category = Fonts)	FSlateFontInfo	FontNormal;
	UPROPERTY(EditDefaultsOnly, Category = Fonts)	FSlateFontInfo	FontLarge;

	UPROPERTY(EditDefaultsOnly, Category = Fonts)	FSlateFontInfo	FontNumbers;
	UPROPERTY(EditDefaultsOnly, Category = Fonts)	FSlateFontInfo	FontVersion;	
};
