// Fill out your copyright notice in the Description page of Project Settings.

#include "ShooterGame.h"
#include "GameUtilities.h"
#include "UObjectExtensions.h"
#include "WeaponItemEntity.h"

float FGameUtilities::GetWeaponCoefficient(UWeaponItemEntity* InWeaponItemEntity, const int32& InLevel)
{
	if (IsValidObject(InWeaponItemEntity))
	{
		// * From AShooterWeapon::GetWeaponCoefficient
		const float FireRate = 60000.0f / FTimespan::FromSeconds(InWeaponItemEntity->GetWeaponConfiguration().TimeBetweenShots()).GetTotalMilliseconds();
		return (InWeaponItemEntity->GetDynamicValueForLevel(InLevel) * InWeaponItemEntity->GetWeaponConfiguration().BulletsMin * FireRate) / 1000.0f;
	}

	return 0;
}
