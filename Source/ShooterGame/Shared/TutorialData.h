// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "Engine/DataAsset.h"
#include "TutorialData.generated.h"

UENUM(BlueprintType)
namespace ETutorialAction
{
	enum Type
	{
		Skip,
		Time,
		BuyItem,
		EquipItem,
		End UMETA(Hidden)
	};
}

UENUM(BlueprintType)
namespace ETutorialInfoShowAs
{
	enum Type
	{
		None,
		Notify,
		Message,
		Tip,
		End UMETA(Hidden)
	};
}

UENUM(BlueprintType)
namespace ETutorialInfoSound
{
	enum Type
	{
		Show,
		Hide,
		RewardShow,
		RewardHide,
		Voice,
		End
	};
}

USTRUCT(BlueprintType)
struct FTutorialInfo
{
	GENERATED_USTRUCT_BODY();

	UPROPERTY(EditAnywhere, BlueprintReadWrite)
	TEnumAsByte<ETutorialAction::Type> TargetAction;

	/*
	 *	For paste player name or other elemets now allow use format keys:
	 *	{PlayerName}
	 *	{PlayerMoney}
	 *	{PlayerDonate}
	 */
	UPROPERTY(EditAnywhere, BlueprintReadWrite, meta = (MultiLine = true))
	FText Title;

	/*
	 *	For paste player name or other elemets now allow use format keys:
	 *	{PlayerName}
	 *	{PlayerMoney}
	 *	{PlayerDonate}
	 */
	UPROPERTY(EditAnywhere, BlueprintReadWrite, meta = (MultiLine = true))
	FText Message;

	UPROPERTY(EditAnywhere, BlueprintReadWrite)
	TEnumAsByte<ETextJustify::Type> MessageJustification;

	/*
	 * Used for define display message as Message Box, Notify, Tip
	 * *** Tip currently not supported!
	 */
	UPROPERTY(EditAnywhere, BlueprintReadWrite)
	TEnumAsByte<ETutorialInfoShowAs::Type> ShowAs;

	/*
	 * Used for TargetAction
	 * if TargetAction = Time, this is time in seconds
	 * if TargetAction = BuyItem, this is item model index
	 * if TargetAction = EquipItem, this is item model index
	 */
	UPROPERTY(EditAnywhere, BlueprintReadWrite)
	int32 CompleteSpecifer;

	/*
	 * Used for play unique sound on action
	 */
	UPROPERTY(EditDefaultsOnly)
	FSlateSound Sounds[ETutorialInfoSound::End];

	/*
	* Used for Highlight any slate widgets by tag
	* Key as FName, this is widget tag.
	* Value as float, this is highlight timer, use 0 to infinity
	*/
	UPROPERTY(EditAnywhere, BlueprintReadWrite)
	TMap<FName, float> WidgetsHighlightList;

	/*
	* Used for Un Highlight any slate widgets by tag
	* List tags for Highlight off
	*/
	UPROPERTY(EditAnywhere, BlueprintReadWrite)
	TArray<FName> WidgetsUnhighlightList;
};

UCLASS()
class SHOOTERGAME_API UTutorialData : public UDataAsset
{
	GENERATED_BODY()

public:

	UTutorialData();

	FText GetTitle() const;
	int32 GetStagesNum() const;
	const TArray<FTutorialInfo>& GetStages() const;
	const FTutorialInfo& GetStage(const int32& InStage) const;

protected:

	// * The name of tutorial
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = Tutorial)
	FText Title;

	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = Stage)
	TArray<FTutorialInfo> Stages;
};
