// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

class UWeaponItemEntity;

struct FGameUtilities
{
	static float GetWeaponCoefficient(UWeaponItemEntity* InWeaponItemEntity, const int32& InLevel);
};
