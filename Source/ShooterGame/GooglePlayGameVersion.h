#pragma once

#ifndef H_GooglePlayGameVersion
#define H_GooglePlayGameVersion

//==============================
#ifdef GAME_STORE_VERSION
#undef GAME_STORE_VERSION
#endif

//==============================
#ifdef GAME_TARGET_SERVER
#undef GAME_TARGET_SERVER
#endif

//==============================
#ifdef GAME_TARGET_DEPLOY
#undef GAME_TARGET_DEPLOY
#endif

//==============================
#define GAME_STORE_VERSION 2682

#define GAME_TARGET_SERVER 482

#define GAME_TARGET_DEPLOY 1

#endif
