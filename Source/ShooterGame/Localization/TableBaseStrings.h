// Copyright 1998-2018 Epic Games, Inc. All Rights Reserved.

#pragma once

class FTableBaseStrings
{
public:

	FTableBaseStrings();
	~FTableBaseStrings();

	static FText GetDayOfWeek(const EDayOfWeek InDay);
	static FText GetMonthOfYear(const EMonthOfYear InMonth);

	enum EBaseStrings
	{
		All,
		Yes,
		No,
		Ok,
		Apply,
		Select,
		Next,
		Back,
		Continue,
		Cancel,
		Close,
		Accept,
		Decline,
		Exit,
		Unavailable,
		Equip,
		Equipped,
		Active,
		Score,
		//	������� �������
		GetExtraReward,
		End
	};

	static FText GetBaseText(const EBaseStrings InString);

#if !UE_SERVER
	static TSharedPtr<FTableBaseStrings> ThisInstance;
#endif
};

typedef FTableBaseStrings::EBaseStrings EBaseStrings;