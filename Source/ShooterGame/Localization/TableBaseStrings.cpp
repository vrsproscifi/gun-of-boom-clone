// Copyright 1998-2018 Epic Games, Inc. All Rights Reserved.

#include "ShooterGame.h"
#include "TableBaseStrings.h"
#include "StringTableRegistry.h"

FTableBaseStrings::FTableBaseStrings()
{
	if (FStringTableRegistry::Get().FindStringTable("BaseStrings").IsValid()) return;
	
	LOCTABLE_NEW("BaseStrings", "BaseStrings");

	LOCTABLE_SETSTRING("BaseStrings", "DayOfWeek.Monday", "Monday");
	LOCTABLE_SETSTRING("BaseStrings", "DayOfWeek.Tuesday", "Tuesday");
	LOCTABLE_SETSTRING("BaseStrings", "DayOfWeek.Wednesday", "Wednesday");
	LOCTABLE_SETSTRING("BaseStrings", "DayOfWeek.Thursday", "Thursday");
	LOCTABLE_SETSTRING("BaseStrings", "DayOfWeek.Friday", "Friday");
	LOCTABLE_SETSTRING("BaseStrings", "DayOfWeek.Saturday", "Saturday");
	LOCTABLE_SETSTRING("BaseStrings", "DayOfWeek.Sunday", "Sunday");
	// Month names
	LOCTABLE_SETSTRING("BaseStrings", "MonthOfYear.January", "January");
	LOCTABLE_SETSTRING("BaseStrings", "MonthOfYear.February", "February");
	LOCTABLE_SETSTRING("BaseStrings", "MonthOfYear.March", "March");
	LOCTABLE_SETSTRING("BaseStrings", "MonthOfYear.April", "April");
	LOCTABLE_SETSTRING("BaseStrings", "MonthOfYear.May", "May");
	LOCTABLE_SETSTRING("BaseStrings", "MonthOfYear.June", "June");
	LOCTABLE_SETSTRING("BaseStrings", "MonthOfYear.July", "July");
	LOCTABLE_SETSTRING("BaseStrings", "MonthOfYear.August", "August");
	LOCTABLE_SETSTRING("BaseStrings", "MonthOfYear.September", "September");
	LOCTABLE_SETSTRING("BaseStrings", "MonthOfYear.October", "October");
	LOCTABLE_SETSTRING("BaseStrings", "MonthOfYear.November", "November");
	LOCTABLE_SETSTRING("BaseStrings", "MonthOfYear.December", "December");
	// Base strings
	LOCTABLE_SETSTRING("BaseStrings", "BaseStrings.All", "All");
	LOCTABLE_SETSTRING("BaseStrings", "BaseStrings.Yes", "Yes");
	LOCTABLE_SETSTRING("BaseStrings", "BaseStrings.No", "No");
	LOCTABLE_SETSTRING("BaseStrings", "BaseStrings.Ok", "Ok");
	LOCTABLE_SETSTRING("BaseStrings", "BaseStrings.Apply", "Apply");
	LOCTABLE_SETSTRING("BaseStrings", "BaseStrings.Select", "Select");
	LOCTABLE_SETSTRING("BaseStrings", "BaseStrings.Next", "Next");
	LOCTABLE_SETSTRING("BaseStrings", "BaseStrings.Back", "Back");
	LOCTABLE_SETSTRING("BaseStrings", "BaseStrings.Continue", "Continue");
	LOCTABLE_SETSTRING("BaseStrings", "BaseStrings.Cancel", "Cancel");
	LOCTABLE_SETSTRING("BaseStrings", "BaseStrings.Close", "Close");
	LOCTABLE_SETSTRING("BaseStrings", "BaseStrings.Accept", "Accept");
	LOCTABLE_SETSTRING("BaseStrings", "BaseStrings.Decline", "Decline");
	LOCTABLE_SETSTRING("BaseStrings", "BaseStrings.Exit", "Exit");
	LOCTABLE_SETSTRING("BaseStrings", "BaseStrings.Unavailable", "Unavailable");
	LOCTABLE_SETSTRING("BaseStrings", "BaseStrings.Equip", "Equip");
	LOCTABLE_SETSTRING("BaseStrings", "BaseStrings.Equipped", "Equipped");
	LOCTABLE_SETSTRING("BaseStrings", "BaseStrings.Active", "Active");
	LOCTABLE_SETSTRING("BaseStrings", "BaseStrings.Score", "Score");
	LOCTABLE_SETSTRING("BaseStrings", "BaseStrings.GetExtraReward", "Get Extra Reward");
}

FTableBaseStrings::~FTableBaseStrings()
{
	FStringTableRegistry::Get().UnregisterStringTable("BaseStrings");
}

FText FTableBaseStrings::GetDayOfWeek(const EDayOfWeek InDay)
{
	switch (InDay)
	{
		case EDayOfWeek::Monday: 	return LOCTABLE("BaseStrings", "DayOfWeek.Monday");
		case EDayOfWeek::Tuesday: 	return LOCTABLE("BaseStrings", "DayOfWeek.Tuesday");
		case EDayOfWeek::Wednesday: return LOCTABLE("BaseStrings", "DayOfWeek.Wednesday");
		case EDayOfWeek::Thursday:	return LOCTABLE("BaseStrings", "DayOfWeek.Thursday");
		case EDayOfWeek::Friday: 	return LOCTABLE("BaseStrings", "DayOfWeek.Friday");
		case EDayOfWeek::Saturday:	return LOCTABLE("BaseStrings", "DayOfWeek.Saturday");
		case EDayOfWeek::Sunday:	return LOCTABLE("BaseStrings", "DayOfWeek.Sunday");
	}

	return FText::GetEmpty();
}

FText FTableBaseStrings::GetMonthOfYear(const EMonthOfYear InMonth)
{
	switch (InMonth)
	{
		case EMonthOfYear::January:		return LOCTABLE("BaseStrings", "MonthOfYear.January");
		case EMonthOfYear::February:	return LOCTABLE("BaseStrings", "MonthOfYear.February");
		case EMonthOfYear::March:		return LOCTABLE("BaseStrings", "MonthOfYear.March");
		case EMonthOfYear::April:		return LOCTABLE("BaseStrings", "MonthOfYear.April");
		case EMonthOfYear::May:			return LOCTABLE("BaseStrings", "MonthOfYear.May");
		case EMonthOfYear::June: 		return LOCTABLE("BaseStrings", "MonthOfYear.June");
		case EMonthOfYear::July: 		return LOCTABLE("BaseStrings", "MonthOfYear.July");
		case EMonthOfYear::August:		return LOCTABLE("BaseStrings", "MonthOfYear.August");
		case EMonthOfYear::September: 	return LOCTABLE("BaseStrings", "MonthOfYear.September");
		case EMonthOfYear::October:		return LOCTABLE("BaseStrings", "MonthOfYear.October");
		case EMonthOfYear::November: 	return LOCTABLE("BaseStrings", "MonthOfYear.November");
		case EMonthOfYear::December: 	return LOCTABLE("BaseStrings", "MonthOfYear.December");
	}

	return FText::GetEmpty();
}

FText FTableBaseStrings::GetBaseText(const EBaseStrings InString)
{
	switch (InString)
	{
		case All:			return	LOCTABLE("BaseStrings", "BaseStrings.All");
		case Yes:			return	LOCTABLE("BaseStrings", "BaseStrings.Yes");
		case No: 			return	LOCTABLE("BaseStrings", "BaseStrings.No");
		case Ok: 			return	LOCTABLE("BaseStrings", "BaseStrings.Ok");
		case Apply: 		return	LOCTABLE("BaseStrings", "BaseStrings.Apply");
		case Select: 		return	LOCTABLE("BaseStrings", "BaseStrings.Select");
		case Next:			return	LOCTABLE("BaseStrings", "BaseStrings.Next");
		case Back:			return	LOCTABLE("BaseStrings", "BaseStrings.Back");
		case Continue:		return	LOCTABLE("BaseStrings", "BaseStrings.Continue");
		case Cancel: 		return	LOCTABLE("BaseStrings", "BaseStrings.Cancel");
		case Close:			return	LOCTABLE("BaseStrings", "BaseStrings.Close");
		case Accept: 		return	LOCTABLE("BaseStrings", "BaseStrings.Accept");
		case Decline:		return	LOCTABLE("BaseStrings", "BaseStrings.Decline");
		case Exit:			return	LOCTABLE("BaseStrings", "BaseStrings.Exit");
		case Unavailable:	return	LOCTABLE("BaseStrings", "BaseStrings.Unavailable");
		case Equip:			return	LOCTABLE("BaseStrings", "BaseStrings.Equip");
		case Equipped:		return	LOCTABLE("BaseStrings", "BaseStrings.Equipped");
		case Active:		return	LOCTABLE("BaseStrings", "BaseStrings.Active");
		case Score:			return	LOCTABLE("BaseStrings", "BaseStrings.Score");
		case GetExtraReward:return	LOCTABLE("BaseStrings", "BaseStrings.GetExtraReward");
	}

	return FText::GetEmpty();
}

#if !UE_SERVER
TSharedPtr<FTableBaseStrings> FTableBaseStrings::ThisInstance = MakeShareable(new FTableBaseStrings());
#endif