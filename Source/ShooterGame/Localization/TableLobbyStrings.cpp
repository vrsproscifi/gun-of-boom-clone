// Copyright 1998-2018 Epic Games, Inc. All Rights Reserved.

#include "ShooterGame.h"
#include "TableLobbyStrings.h"
#include "StringTableRegistry.h"

FText FTableLobbyStrings::IntoFight;
FText FTableLobbyStrings::IntoArsenal;
FText FTableLobbyStrings::SearchOfFight;
FText FTableLobbyStrings::FightFound;

FTableLobbyStrings::FTableLobbyStrings()
{
	if (FStringTableRegistry::Get().FindStringTable("LobbyStrings").IsValid()) return;

	LOCTABLE_NEW("LobbyStrings", "LobbyStrings");

	LOCTABLE_SETSTRING("LobbyStrings", "LobbyStrings.IntoFight", "Battle!");
	LOCTABLE_SETSTRING("LobbyStrings", "LobbyStrings.IntoArsenal", "Arsenal");

	LOCTABLE_SETSTRING("LobbyStrings", "LobbyStrings.SearchOfFight", "Search Of Fight");
	LOCTABLE_SETSTRING("LobbyStrings", "LobbyStrings.FightFound", "Fight Found");

	IntoFight = LOCTABLE("LobbyStrings", "LobbyStrings.IntoFight");
	IntoArsenal = LOCTABLE("LobbyStrings", "LobbyStrings.IntoArsenal");
	SearchOfFight = LOCTABLE("LobbyStrings", "LobbyStrings.SearchOfFight");
	FightFound = LOCTABLE("LobbyStrings", "LobbyStrings.FightFound");
}

FTableLobbyStrings::~FTableLobbyStrings()
{
	FStringTableRegistry::Get().UnregisterStringTable("LobbyStrings");
}

#if !UE_SERVER
TSharedPtr<FTableLobbyStrings> FTableLobbyStrings::ThisInstance = MakeShareable(new FTableLobbyStrings());
#endif