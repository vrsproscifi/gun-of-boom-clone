// Copyright 1998-2018 Epic Games, Inc. All Rights Reserved.

#pragma once

class FTableLobbyStrings
{
public:

	FTableLobbyStrings();
	~FTableLobbyStrings();
	
	static FText IntoFight;
	static FText IntoArsenal;
	static FText SearchOfFight;
	static FText FightFound;

#if !UE_SERVER
	static TSharedPtr<FTableLobbyStrings> ThisInstance;
#endif
};