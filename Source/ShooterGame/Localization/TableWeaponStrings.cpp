// Copyright 1998-2018 Epic Games, Inc. All Rights Reserved.

#include "ShooterGame.h"
#include "TableWeaponStrings.h"
#include "StringTableRegistry.h"
#include "GameItemType.h"
#include "Product/ProductItemCategory.h"

FTableItemStrings::FTableItemStrings()
{
	if (FStringTableRegistry::Get().FindStringTable("ItemStrings").IsValid()) return;

	LOCTABLE_NEW("ItemStrings", "ItemStrings");

	// Armour Category (One name style)
	LOCTABLE_SETSTRING("ItemStrings", "ItemStrings.Head", "Head");
	LOCTABLE_SETSTRING("ItemStrings", "ItemStrings.Torso", "Torso");
	LOCTABLE_SETSTRING("ItemStrings", "ItemStrings.Legs", "Legs");

	// Support Category (One name style)
	LOCTABLE_SETSTRING("ItemStrings", "ItemStrings.Grenade", "Grenade");
	LOCTABLE_SETSTRING("ItemStrings", "ItemStrings.AidKit", "AidKit");
	LOCTABLE_SETSTRING("ItemStrings", "ItemStrings.Stimulant", "Stimulant");
	LOCTABLE_SETSTRING("ItemStrings", "ItemStrings.RepairKit", "RepairKit");

	// Visual Category (One name style)
	LOCTABLE_SETSTRING("ItemStrings", "ItemStrings.Skin", "Skin");
	LOCTABLE_SETSTRING("ItemStrings", "ItemStrings.Mask", "Mask");
	LOCTABLE_SETSTRING("ItemStrings", "ItemStrings.Costume", "Costume");

	// Weapon Category (One name style)
	LOCTABLE_SETSTRING("ItemStrings", "ItemStrings.AssaultRifle", "Assault Rifle");
	LOCTABLE_SETSTRING("ItemStrings", "ItemStrings.Shotgun", "Shotgun");
	LOCTABLE_SETSTRING("ItemStrings", "ItemStrings.SniperRifle", "Rifle");
	LOCTABLE_SETSTRING("ItemStrings", "ItemStrings.MachineGun", "Machine Gun");
	LOCTABLE_SETSTRING("ItemStrings", "ItemStrings.Pistol", "Pistol");
	LOCTABLE_SETSTRING("ItemStrings", "ItemStrings.Knife", "Knife");	

	// Weapon Category (Multi name style)
	LOCTABLE_SETSTRING("ItemStrings", "ItemStrings.AssaultRifles", "Assault Rifles");
	LOCTABLE_SETSTRING("ItemStrings", "ItemStrings.Shotguns", "Shotguns");
	LOCTABLE_SETSTRING("ItemStrings", "ItemStrings.SniperRifles", "Rifles");
	LOCTABLE_SETSTRING("ItemStrings", "ItemStrings.MachineGuns", "Machine Guns");
	LOCTABLE_SETSTRING("ItemStrings", "ItemStrings.Pistols", "Pistols");
	LOCTABLE_SETSTRING("ItemStrings", "ItemStrings.Knives", "Knives");

	// Product Category (One name style)
	LOCTABLE_SETSTRING("ItemStrings", "ItemStrings.Money", "Money");
	LOCTABLE_SETSTRING("ItemStrings", "ItemStrings.Donate", "Crystal");
	LOCTABLE_SETSTRING("ItemStrings", "ItemStrings.Booster", "Premium");
	LOCTABLE_SETSTRING("ItemStrings", "ItemStrings.Case", "Case");
	LOCTABLE_SETSTRING("ItemStrings", "ItemStrings.TrophyCase", "Trophy Case");
	LOCTABLE_SETSTRING("ItemStrings", "ItemStrings.BattleCoin", "Battle Coin");

	// Product Category (Multi name style)
	LOCTABLE_SETSTRING("ItemStrings", "ItemStrings.Moneys", "Moneys");
	LOCTABLE_SETSTRING("ItemStrings", "ItemStrings.Donates", "Crystals");
	LOCTABLE_SETSTRING("ItemStrings", "ItemStrings.Boosters", "Premiums");
	LOCTABLE_SETSTRING("ItemStrings", "ItemStrings.Cases", "Cases");
	LOCTABLE_SETSTRING("ItemStrings", "ItemStrings.TrophyCases", "Trophy Cases");
	LOCTABLE_SETSTRING("ItemStrings", "ItemStrings.BattleCoins", "Battle Coins");
}

FTableItemStrings::~FTableItemStrings()
{
	FStringTableRegistry::Get().UnregisterStringTable("ItemStrings");
}

FText FTableItemStrings::GetCategoryText(EGameItemType InType, bool IsMultiStyle)
{
	switch (InType) 
	{ 
		case EGameItemType::Rifle:			return IsMultiStyle ? LOCTABLE("ItemStrings", "ItemStrings.AssaultRifles") : LOCTABLE("ItemStrings", "ItemStrings.AssaultRifle");
		case EGameItemType::Shotgun:		return IsMultiStyle ? LOCTABLE("ItemStrings", "ItemStrings.Shotguns") : LOCTABLE("ItemStrings", "ItemStrings.Shotgun");
		case EGameItemType::SniperRifle:	return IsMultiStyle ? LOCTABLE("ItemStrings", "ItemStrings.SniperRifles") : LOCTABLE("ItemStrings", "ItemStrings.SniperRifle");
		case EGameItemType::MachineGun:		return IsMultiStyle ? LOCTABLE("ItemStrings", "ItemStrings.MachineGuns") : LOCTABLE("ItemStrings", "ItemStrings.MachineGun");
		case EGameItemType::Pistol:			return IsMultiStyle ? LOCTABLE("ItemStrings", "ItemStrings.Pistols") : LOCTABLE("ItemStrings", "ItemStrings.Pistol");
		case EGameItemType::Knife:			return IsMultiStyle ? LOCTABLE("ItemStrings", "ItemStrings.Knives") : LOCTABLE("ItemStrings", "ItemStrings.Knife");
		case EGameItemType::Torso:			return LOCTABLE("ItemStrings", "ItemStrings.Torso");
		case EGameItemType::Head:			return LOCTABLE("ItemStrings", "ItemStrings.Head");
		case EGameItemType::Legs:			return LOCTABLE("ItemStrings", "ItemStrings.Legs");
		case EGameItemType::Grenade:		return LOCTABLE("ItemStrings", "ItemStrings.Grenade");
		case EGameItemType::AidKit:			return LOCTABLE("ItemStrings", "ItemStrings.AidKit");
		case EGameItemType::Stimulant:		return LOCTABLE("ItemStrings", "ItemStrings.Stimulant");
		case EGameItemType::RepairKit:		return LOCTABLE("ItemStrings", "ItemStrings.RepairKit");
		case EGameItemType::Skin:			return LOCTABLE("ItemStrings", "ItemStrings.Skin");
		case EGameItemType::Mask:			return LOCTABLE("ItemStrings", "ItemStrings.Mask");
		case EGameItemType::Costume:		return LOCTABLE("ItemStrings", "ItemStrings.Costume");
	}

	return FText::GetEmpty();
}

FText FTableItemStrings::GetProductText(EProductItemCategory InType, bool IsMultiStyle)
{
	switch(InType) 
	{ 
		case EProductItemCategory::Money:			return IsMultiStyle ? LOCTABLE("ItemStrings", "ItemStrings.Moneys") : LOCTABLE("ItemStrings", "ItemStrings.Money");
		case EProductItemCategory::Donate:			return IsMultiStyle ? LOCTABLE("ItemStrings", "ItemStrings.Donates") : LOCTABLE("ItemStrings", "ItemStrings.Donate");
		case EProductItemCategory::Booster:			return IsMultiStyle ? LOCTABLE("ItemStrings", "ItemStrings.Boosters") : LOCTABLE("ItemStrings", "ItemStrings.Booster");
		case EProductItemCategory::Cases:			return IsMultiStyle ? LOCTABLE("ItemStrings", "ItemStrings.Cases") : LOCTABLE("ItemStrings", "ItemStrings.Case");
		case EProductItemCategory::TrophyCases:		return IsMultiStyle ? LOCTABLE("ItemStrings", "ItemStrings.TrophyCases") : LOCTABLE("ItemStrings", "ItemStrings.TrophyCase");
		case EProductItemCategory::BattleCoins:		return IsMultiStyle ? LOCTABLE("ItemStrings", "ItemStrings.BattleCoins") : LOCTABLE("ItemStrings", "ItemStrings.BattleCoin");
	}
	return FText::GetEmpty();
}

#if !UE_SERVER
TSharedPtr<FTableItemStrings> FTableItemStrings::ThisInstance = MakeShareable(new FTableItemStrings());
#endif