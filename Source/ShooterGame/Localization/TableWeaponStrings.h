// Copyright 1998-2018 Epic Games, Inc. All Rights Reserved.

#pragma once

enum class EProductItemCategory : uint8;
enum class EGameItemType : uint8;

class FTableItemStrings
{
public:

	FTableItemStrings();
	~FTableItemStrings();
	
	static FText GetCategoryText(EGameItemType InType, bool IsMultiStyle = false);
	static FText GetProductText(EProductItemCategory InType, bool IsMultiStyle = false);

#if !UE_SERVER
	static TSharedPtr<FTableItemStrings> ThisInstance;
#endif
};
