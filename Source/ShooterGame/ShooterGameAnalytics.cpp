#include "ShooterGame.h"
#include "ShooterGameAnalytics.h"
#include "GameAnalytics.h"
#include "GameItemType.h"
//#include "CrashlyticsBlueprintLibrary.h"

FGuid FShooterGameAnalytics::UserId;
int32 FShooterGameAnalytics::MatchesPerSession = 0;
int32 FShooterGameAnalytics::BadNetworkConnections = 0;
bool FShooterGameAnalytics::bHasSessionStarted = false;

void FShooterGameAnalytics::RecordMatchPerSession()
{
	//=================================================
	if (bHasSessionStarted == false)
	{
		return;
	}

	//=================================================
	FShooterGameAnalytics::MatchesPerSession++;
	RecordDesignEventRange("Match:MatchesPerSession:More[Key]", FShooterGameAnalytics::MatchesPerSession, {30, 25, 20, 17, 15, 12, 10, 9, 7, 5, 3, 2, 1, 0});
}

void FShooterGameAnalytics::SetUserId(const FGuid& InUserId)
{
	UserId = InUserId;
	//UCrashlyticsBlueprintLibrary::SetUserIdentifier(InUserId.ToString());
	FGenericPlatformMisc::SetEpicAccountId(InUserId.ToString());

	//=================================================
	if (bHasSessionStarted == false)
	{
		return;
	}

	//=================================================
	UGameAnalytics::configureUserId(TCHAR_TO_ANSI(*InUserId.ToString()));
}


void FShooterGameAnalytics::RecordErrorEvent(const EErrorSeverity& InErrorType, const FString& InErrorMessage)
{
	//=================================================
	if (bHasSessionStarted == false)
	{
		return;
	}

	//=================================================
	UGameAnalytics::addErrorEvent(static_cast<EGAErrorSeverity>(InErrorType), TCHAR_TO_ANSI(*InErrorMessage));

}

void FShooterGameAnalytics::AddResourceEvent(const EResourceFlowType& InFlowType, const FString& currency,
	const float& amount, const FString& itemType, const FString& itemId)
{
	//=================================================
	if (bHasSessionStarted == false)
	{
		return;
	}

	//=================================================
	UGameAnalytics::addResourceEvent
	(
		static_cast<EGAResourceFlowType>(InFlowType)
		, TCHAR_TO_ANSI(*currency)
		, amount
		, TCHAR_TO_ANSI(*itemType)
		, TCHAR_TO_ANSI(*itemId)
	);
}

void FShooterGameAnalytics::AddBusinessEvent(const FString&InItemId, const FString&InItemName, const FString&InItemType, const FString&currency, const int& InCostInCents)
{	
	//=================================================
	if (bHasSessionStarted == false)
	{
		return;
	}

	//=========================================================
	const auto CostInUsd = InCostInCents / 100.0f;

	//=========================================================
	//UCrashlyticsBlueprintLibrary::LogAddToCart(InItemId, InItemType, InItemName, currency, CostInUsd);
	//UCrashlyticsBlueprintLibrary::LogStartCheckout(currency, CostInUsd);
}


void FShooterGameAnalytics::AddBusinessEvent(const FString& currency, const int& InCostInCents, const FString& itemType, const FString& itemId, const FString& cartType, const FString& receipt, const FString& signature, const EBusinessPurchaseState& InPurchaseState)
{
	//=========================================================
	FString BusinessPurchaseState = "Unknown";

	//=========================================================
	if (InPurchaseState == EBusinessPurchaseState::Success)
	{
		BusinessPurchaseState = "Success";
	}	
	else if (InPurchaseState == EBusinessPurchaseState::Failed)
	{
		BusinessPurchaseState = "Failed";
	}
	else if (InPurchaseState == EBusinessPurchaseState::Cancelled)
	{
		BusinessPurchaseState = "Cancelled";
	}
	else if (InPurchaseState == EBusinessPurchaseState::Invalid)
	{
		BusinessPurchaseState = "Invalid";
	}
	else if (InPurchaseState == EBusinessPurchaseState::NotAllowed)
	{
		BusinessPurchaseState = "NotAllowed";
	}
	else if (InPurchaseState == EBusinessPurchaseState::Restored)
	{
		BusinessPurchaseState = "Restored";
	}
	else if (InPurchaseState == EBusinessPurchaseState::AlreadyOwned)
	{
		BusinessPurchaseState = "AlreadyOwned";
	}

	//=========================================================
	const auto CostInUsd = InCostInCents / 100.0f;

	//=========================================================
	//UCrashlyticsBlueprintLibrary::LogPurchase(itemId, cartType, itemId, BusinessPurchaseState, currency, CostInUsd, InPurchaseState == EBusinessPurchaseState::Success);

	//=================================================
	if (bHasSessionStarted == false)
	{
		return;
	}

	//=================================================
	if (InPurchaseState == EBusinessPurchaseState::Success)
	{
		UGameAnalytics::addBusinessEvent
		(
			TCHAR_TO_ANSI(*currency)
			, InCostInCents
			, TCHAR_TO_ANSI(*itemType)
			, TCHAR_TO_ANSI(*itemId)
			, TCHAR_TO_ANSI(*cartType)
#if PLATFORM_ANDROID
			, TCHAR_TO_ANSI(*receipt)
			, TCHAR_TO_ANSI(*signature)
#endif
		);
	}
}

void FShooterGameAnalytics::RecordDesignEventRange(const FString& InEventName, const int32& InAmount, const TArray<int32>& InRangeArray)
{
	//=================================================
	if (bHasSessionStarted == false)
	{
		return;
	}

	//=================================================
	for(const auto Range : InRangeArray)
	{
		if(InAmount >= Range)
		{
			const auto EventName = InEventName.Replace(TEXT("[Key]"), *FString::FormatAsNumber(Range));
			FShooterGameAnalytics::RecordDesignEvent(EventName);
			break;
		}
	}
}

void FShooterGameAnalytics::RecordContentSearch(const FString& InQuery)
{
	//UCrashlyticsBlueprintLibrary::LogContentSearch(InQuery);

	//=================================================
	if (bHasSessionStarted == false)
	{
		return;
	}

	//=================================================
	FShooterGameAnalytics::RecordDesignEvent(InQuery);
}

void FShooterGameAnalytics::RecordContentView(const FString& InContentId, const FString& InContentName, const FString& InContentType)
{
	//UCrashlyticsBlueprintLibrary::LogContentView(InContentId, InContentName, InContentType);
}

void FShooterGameAnalytics::RecordDesignEvent(const FString& InEventName)
{
	//UCrashlyticsBlueprintLibrary::LogCustomEvent(InEventName);

	//=================================================
	if (bHasSessionStarted == false)
	{
		return;
	}

	//=================================================
	UGameAnalytics::addDesignEvent(TCHAR_TO_ANSI(*InEventName));
}

void FShooterGameAnalytics::RecordDesignEvent(const FString& InEventName, const float& InAmount)
{
	//UCrashlyticsBlueprintLibrary::LogCustomEventEx(InEventName, InAmount);

	//=================================================
	if (bHasSessionStarted == false)
	{
		return;
	}

	//=================================================
	UGameAnalytics::addDesignEvent(TCHAR_TO_ANSI(*InEventName), InAmount);
}


void FShooterGameAnalytics::AddProgressionEvent(EProgressionStatus progressionStatus, const FString& progression01, const FString& progression02, const int32& InScore)
{
	//=================================================
	if (bHasSessionStarted == false)
	{
		return;
	}

	//=================================================
	if (InScore)
	{
		UGameAnalytics::addProgressionEvent(static_cast<EGAProgressionStatus>(progressionStatus), TCHAR_TO_ANSI(*progression01), TCHAR_TO_ANSI(*progression02), InScore);
	}
	else
	{
		UGameAnalytics::addProgressionEvent(static_cast<EGAProgressionStatus>(progressionStatus), TCHAR_TO_ANSI(*progression01), TCHAR_TO_ANSI(*progression02));
	}

	//=================================================
	if (progressionStatus == EProgressionStatus::Start)
	{
		//UCrashlyticsBlueprintLibrary::LogLevelStart(progression01 + ":" + progression02);
	}
	else
	{
		//UCrashlyticsBlueprintLibrary::LogLevelEnd(progression01 + ":" + progression02, InScore, progressionStatus == EProgressionStatus::Complete);
	}

}

void FShooterGameAnalytics::StartSession(const FString& InGameKey, const FString& InGameSecret)
{
	//============================================
	const auto GameItemTypeNames = FGameItemType::GetNames();

	//============================================
	TArray<FString> ResourceItemTypes = 
	{
		"Gameplay", "IAP", "Upgrade", "Store", "Arsenal", "MatchResult", "Booster", "Case",
		"BattleCoins", "Money", "Donate", "Experience", "Kills", "Deads", "Assist", "MatchesTotal", "MatchesWins", "MatchesLoses", "TDM"

	};
	for(const auto& ItemTypeName : GameItemTypeNames)
	{
		ResourceItemTypes.Add(ItemTypeName);
	}

	//============================================
	UGameAnalytics::configureAvailableResourceCurrencies
	({
		"BattleCoins", "Money", "Donate", "Experience", "Kills", "Deads", "Assist", "MatchesTotal", "MatchesWins", "MatchesLoses", "TDM"
	});

	//============================================
	UGameAnalytics::configureAvailableResourceItemTypes(ResourceItemTypes);

	//============================================
	UGameAnalytics::initialize(TCHAR_TO_ANSI(*InGameKey), TCHAR_TO_ANSI(*InGameSecret));
	UGameAnalytics::startSession();

	//============================================
	//UCrashlyticsBlueprintLibrary::WriteLog("Start Game Session");

	//============================================
	bHasSessionStarted = true;
}

void FShooterGameAnalytics::StopSession()
{
	if(bHasSessionStarted == false)
	{ 
		return;
	}
	UGameAnalytics::endSession();
}

