// Copyright 1998-2018 Epic Games, Inc. All Rights Reserved.

#include "ShooterGame.h"

#include "AssetRegistryModule.h"
#include "IAssetRegistry.h"

#include "Style/ShooterStyle.h"

#include "Localization/TableLobbyStrings.h"
#include "Localization/TableBaseStrings.h"
#include "Localization/TableWeaponStrings.h"

class FShooterGameModule : public FDefaultGameModuleImpl
{
	virtual void StartupModule() override
	{
		FAssetRegistryModule& AssetRegistryModule = FModuleManager::LoadModuleChecked<FAssetRegistryModule>(TEXT("AssetRegistry"));

		//Hot reload hack
		FSlateStyleRegistry::UnRegisterSlateStyle(FShooterStyle::GetStyleSetName());
		FShooterStyle::Initialize();
#if !UE_SERVER
		FTableLobbyStrings::ThisInstance.Reset();
		FTableBaseStrings::ThisInstance.Reset();
		FTableItemStrings::ThisInstance.Reset();

		FTableBaseStrings::ThisInstance = MakeShareable(new FTableBaseStrings());
		FTableLobbyStrings::ThisInstance = MakeShareable(new FTableLobbyStrings());
		FTableItemStrings::ThisInstance = MakeShareable(new FTableItemStrings());
#endif
	}

	virtual void ShutdownModule() override
	{
		FShooterStyle::Shutdown();
#if !UE_SERVER
		FTableLobbyStrings::ThisInstance.Reset();
		FTableBaseStrings::ThisInstance.Reset();
		FTableItemStrings::ThisInstance.Reset();
#endif
	}
};

IMPLEMENT_PRIMARY_GAME_MODULE(FShooterGameModule, ShooterGame, "GunsOfBoomClone");

DEFINE_LOG_CATEGORY(LogShooter)
DEFINE_LOG_CATEGORY(LogShooterWeapon)
