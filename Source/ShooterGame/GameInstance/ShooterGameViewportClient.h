// Copyright 1998-2018 Epic Games, Inc. All Rights Reserved.

#pragma once

#include "SlateMaterialBrush.h"
#include "ShooterGameViewportClient.generated.h"


class SShooterLoadingScreen : public SCompoundWidget
{
public:
	SLATE_BEGIN_ARGS(SShooterLoadingScreen) {}
	SLATE_END_ARGS()

	void Construct(const FArguments& InArgs);

private:
	EVisibility GetLoadIndicatorVisibility() const
	{
		return EVisibility::Visible;
	}

	/** loading screen image brush */
	TSharedPtr<FSlateMaterialBrush> LoadingScreenBrush;
};

class SWidget;
class SUsableCompoundWidget;

UCLASS(Within=Engine, transient, config=Engine)
class UShooterGameViewportClient : public UGameViewportClient
{
	GENERATED_UCLASS_BODY()

public:

	virtual void AddViewportWidgetContent(TSharedRef<class SWidget> InWidget, const int32 ZOrder = 0) override;
	virtual void AddViewportWidgetContent_AlwaysVisible(TSharedRef<class SWidget> InWidget, const int32 ZOrder = 0);

	virtual void AddUsableViewportWidgetContent(TSharedRef<SUsableCompoundWidget> InWidget, const int32 ZOrder = 0);
	virtual void AddUsableViewportWidgetContent_AlwaysVisible(TSharedRef<SUsableCompoundWidget> InWidget, const int32 ZOrder = 0);

	virtual void RemoveViewportWidgetContent(TSharedRef<class SWidget> InWidget) override;
	virtual void ToggleViewportWidgets(const bool InToggle);

//	void ToggleSlateControll(const bool InToggle) { OnToggleInteractiveAnyWidget(InToggle); }
	virtual void OnToggleInteractiveAnyWidget(const bool InToggle, const TSharedRef<class SUsableCompoundWidget>& InWidget);
	virtual void OnBackWidget();
	virtual void RemoveAllViewportWidgetsAlt();

	TSharedRef<SWidget> GetViewportWidgetByTag(const FName& InTag) const;
	TArray<TSharedRef<SWidget>> GetViewportWidgetsByTag(const FName& InTag) const;

	void HighlightWidgetByTag(const FName& InTag, const float InTimeSeconds = 0.0f);
	void UnHighlightWidgetByTag(const FName InTag);

 	// start UGameViewportClient interface
 	void NotifyPlayerAdded( int32 PlayerIndex, ULocalPlayer* AddedPlayer ) override;

	void ShowLoadingScreen();
	void HideLoadingScreen();

	static UShooterGameViewportClient* Get();

protected:

	bool bAddedStaticWidgets;

	TArray<TSharedRef<SWidget>>					ViewportContentStack;
	TMap<TSharedRef<SWidget>, int32>			ViewportContentStackZOrder;
	TArray<TSharedRef<SWidget>>					ViewportContentStackAlwaysVisible;
	TArray<TSharedRef<SWidget>>					ViewportContentStackHidden;
	TArray<TSharedRef<SUsableCompoundWidget>>	ViewportContentStackUsable;

	TArray<TSharedRef<SUsableCompoundWidget>>	LastWidgets;
	TArray<TSharedRef<SUsableCompoundWidget>>	CurrentWidget;

	TSharedPtr<SShooterLoadingScreen>				LoadingScreenWidget;

	TSharedPtr<class SWidgetHighlighting> WidgetHighlightingPanel;

	void SetCurrentWidget(TSharedRef<SUsableCompoundWidget> InWidget);
	TSharedRef<SUsableCompoundWidget> GetCurrentWidget() const;

	TArray<TSharedRef<SWidget>> GetViewportWidgetsByTag_Internal(const TSharedRef<SWidget>& InWidget, const FName& InTag) const;
};