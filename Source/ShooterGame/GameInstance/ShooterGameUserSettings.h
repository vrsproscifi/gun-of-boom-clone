// Copyright 1998-2018 Epic Games, Inc. All Rights Reserved.

#pragma once

#include "ShooterGameUserSettings.generated.h"

#define USE_GUID_USER_ID 1

UENUM(Blueprintable, BlueprintType, Category = "Game|Runtime|Graphics")
enum class EQualityLevels : uint8
{
	Low,
	Medium,
	High,
	Epic,
	Cinematic,
	Personal
};

UENUM(BlueprintType)
namespace ESoundClassType
{
	enum Type
	{
		Master,
		SFX,
		UI,
		Music,
		Voice,
		VOIP,
		End
	};
}

UCLASS()
class SHOOTERGAME_API UShooterGameUserSettings : public UGameUserSettings
{
	GENERATED_UCLASS_BODY()

public:
	static UShooterGameUserSettings* Get();

	virtual void SetToDefaults() override;
	virtual void ApplySettings(bool bCheckForCommandLineOverrides) override;

	virtual bool SetSoundClassVolumeByName(FString ClassName, float Volume);
	virtual float GetSoundClassVolumeByName(FString ClassName);

	virtual int32 GetAAMode();
	virtual void SetAAMode(int32 NewAAMode);
	static int32 ConvertAAScalabilityQualityToAAMode(int32 AAScalabilityQuality);

	static int32 GetCurrentLanguage();
	static FString GetCurrentLanguageTwoISO();
	void SetCurrentLanguage(const int32 &_lcid);

	FORCEINLINE bool IsShowPing() const { return bShowPing; }
	FORCEINLINE bool IsShowFPS() const { return bShowFPS; }

	FORCEINLINE void SetShowPing(const bool InShow) { bShowPing = InShow; }
	FORCEINLINE void SetShowFPS(const bool InShow) { bShowFPS = InShow; }

	void InitSettings();

	void ResetInputSettings();

	FORCEINLINE bool IsEnabledAmbientOcclusion() const { return bAmbientOcclusion; }
	FORCEINLINE bool IsEnabledMotionBlur() const { return bMotionBlur; }
	FORCEINLINE bool IsEnabledBloom() const { return bBloom; }
	FORCEINLINE bool IsEnabledDamageNumbers() const { return bShowDamageNumbers; }
	FORCEINLINE bool IsEnabledAutoFire() const { return bAutofire; }
	FORCEINLINE bool IsEnabledAutoTargeting() const { return bAutotarget; }
	FORCEINLINE bool IsEnabledDebugInfo() const { return bDebugInfo; }
	FORCEINLINE bool IsDynamicStick() const { return bDynamicStick; }

	void SetEnabledAmbientOcclusion(const bool);
	void SetEnabledMotionBlur(const bool);
	void SetEnabledBloom(const bool);
	void SetEnabledAutoFire(const bool);
	void SetEnabledAutoTargeting(const bool);
	void SetEnabledDebugInfo(const bool);
	void SetDynamicStick(const bool);

	FORCEINLINE void SetEnabledDamageNumbers(const bool InShowDamageNumbers) { bShowDamageNumbers = InShowDamageNumbers; }

	void SetPlayerAccountUUID(const FGuid& InUserId);

#if USE_GUID_USER_ID
	bool GetLoginUserId(FGuid& OutUserId) const;
	void SetLoginUserId(const FGuid& InUserId);
#else
	bool GetLoginUserId(FString& OutUserId) const;
	void SetLoginUserId(const FString& InUserId);
#endif

protected:
	static uint32 CurrentInputVersion;
	static uint32 CurrentGraphicVersion;

	UPROPERTY(config)
		FString PlayerAccountUUID;

	UPROPERTY(config)
		FString LoginUserId;

	UPROPERTY(config)
		bool bDebugInfo;

	UPROPERTY(config)
		uint32 InputVersion;

	UPROPERTY(config)
		uint32 GraphicVersion;

	UPROPERTY(config)
		float SoundClassVolumes[ESoundClassType::End];

	UPROPERTY(config)
		int32 AAMode;

	UPROPERTY(config)
		bool bAmbientOcclusion;

	UPROPERTY(config)
		bool bMotionBlur;

	UPROPERTY(config)
		bool bBloom;

	UPROPERTY(config)
		bool bAutofire;

	UPROPERTY(config)
		bool bAutotarget;

	UPROPERTY(config)
		bool bDynamicStick;

	/**
	* Current state of the initial benchmark
	*  -1: Not run yet
	*   0: Started, but not completed
	*   1: Completed
	*/
	UPROPERTY(config)
		int32 InitialBenchmarkState;

	FScreenResolutionArray ScreenResolutionArray;
	TArray<FCultureRef> SupportedLanguagesArray;

public:

	virtual void LoadSettings(bool bForceReload = false) override;


	UPROPERTY(config)
		bool bIsFirstRun;

	UPROPERTY(config)
		EQualityLevels GeneralQualityLevel;

	UPROPERTY(config)
		FString Language;

	UPROPERTY(config)
		bool bShowPing;

	UPROPERTY(config)
		bool bShowFPS;

	UPROPERTY(config)
		bool bShowDamageNumbers;

	FORCEINLINE FScreenResolutionArray GetScreenResolutions() const
	{
		return ScreenResolutionArray;
	}

	FORCEINLINE TArray<FCultureRef> GetLanguages() const
	{
		return SupportedLanguagesArray;
	}

public:

	float GetSoundVolume(const ESoundClassType::Type);
	void SetSoundVolume(const ESoundClassType::Type, const float);
};
