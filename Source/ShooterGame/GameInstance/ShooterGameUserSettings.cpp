// Copyright 1998-2018 Epic Games, Inc. All Rights Reserved.

#include "ShooterGame.h"
#include "ShooterGameUserSettings.h"

uint32 UShooterGameUserSettings::CurrentInputVersion = 5;
uint32 UShooterGameUserSettings::CurrentGraphicVersion = 1;


UShooterGameUserSettings::UShooterGameUserSettings(const class FObjectInitializer& ObjectInitializer)
	: Super(ObjectInitializer)
	, bDebugInfo(false)
	, AAMode(0)
	, bAutofire(true)
	, bAutotarget(false)
	, bDynamicStick(false)
	, InitialBenchmarkState(-1)
	, bShowPing(true)
	, bShowFPS(true)
	, bShowDamageNumbers(true)
{
	SetToDefaults();

	RHIGetAvailableResolutions(ScreenResolutionArray, true);
}

void UShooterGameUserSettings::SetPlayerAccountUUID(const FGuid& InUserId)
{
	PlayerAccountUUID = InUserId.ToString(EGuidFormats::DigitsWithHyphens);
	ApplySettings(false);
}

#if USE_GUID_USER_ID

bool UShooterGameUserSettings::GetLoginUserId(FGuid& OutUserId) const
{
	return FGuid::Parse(LoginUserId, OutUserId);
}

void UShooterGameUserSettings::SetLoginUserId(const FGuid& InUserId)
{
	LoginUserId = InUserId.ToString(EGuidFormats::DigitsWithHyphens);
	ApplySettings(false);
}

#else
bool UShooterGameUserSettings::GetLoginUserId(FString& OutUserId) const
{
	OutUserId = LoginUserId;
	return OutUserId.IsEmpty() == false;
}

void UShooterGameUserSettings::SetLoginUserId(const FString& InUserId)
{
	LoginUserId = InUserId;
	ApplySettings(false);
}
#endif



UShooterGameUserSettings* UShooterGameUserSettings::Get()
{
	return Cast<UShooterGameUserSettings>(GEngine->GameUserSettings);
}

void UShooterGameUserSettings::SetToDefaults()
{
	Super::SetToDefaults();

	SetFrameRateLimit(30);

	GeneralQualityLevel = EQualityLevels::Low;
	bIsFirstRun = true;
	AAMode = EAntiAliasingMethod::AAM_FXAA;

	SoundClassVolumes[ESoundClassType::Master] = 1.0f;
	SoundClassVolumes[ESoundClassType::SFX] = 1.0f;
	SoundClassVolumes[ESoundClassType::UI] = 1.0f;
	SoundClassVolumes[ESoundClassType::Music] = 0.5f;
	SoundClassVolumes[ESoundClassType::Voice] = 1.0f;
	SoundClassVolumes[ESoundClassType::VOIP] = 1.0f;
	FullscreenMode = EWindowMode::Fullscreen;
	InitialBenchmarkState = -1;
	InputVersion = -1;
	GraphicVersion = -1;
	bAutofire = true;

	Language = FInternationalization::Get().GetDefaultCulture()->GetTwoLetterISOLanguageName();
	UE_LOG(LogCore, Display, TEXT("UShooterGameUserSettings::SetToDefaults >> Language: %s"), *Language);
	FInternationalization::Get().SetCurrentCulture(Language);
}

void UShooterGameUserSettings::InitSettings()
{
	//======================================================================================================================================================

	SupportedLanguagesArray.Empty();

	auto internationalizationInstance = &FInternationalization::Get();
	checkf(internationalizationInstance, TEXT("internationalizationInstance was nullptr"));

	if (internationalizationInstance)
	{
		auto StringArray = FTextLocalizationManager::Get().GetLocalizedCultureNames(ELocalizationLoadFlags::Game);
		SupportedLanguagesArray = internationalizationInstance->GetAvailableCultures(StringArray, false);
		internationalizationInstance->SetCurrentCulture(Language);
	}

	//======================================================================================================================================================

	if (bIsFirstRun || GraphicVersion < UShooterGameUserSettings::CurrentGraphicVersion)
	{
		bIsFirstRun = false;
		GeneralQualityLevel = EQualityLevels::Personal;
		ScalabilityQuality = Scalability::BenchmarkQualityLevels(10);
		Scalability::SetQualityLevels(ScalabilityQuality);
		Scalability::SaveState(GGameUserSettingsIni);

		SetAAMode(EAntiAliasingMethod::AAM_FXAA);
		SetEnabledAmbientOcclusion((ScalabilityQuality.GetSingleQualityLevel() > 1));
		SetEnabledMotionBlur((ScalabilityQuality.GetSingleQualityLevel() > 1));
		SetEnabledBloom((ScalabilityQuality.GetSingleQualityLevel() > 1));

		ApplyResolutionSettings(false);

		GraphicVersion = UShooterGameUserSettings::CurrentGraphicVersion;

		SaveConfig(CPF_Config, *GGameUserSettingsIni);
	}

	if (EQualityLevels::Personal != GeneralQualityLevel)
	{
		ScalabilityQuality.SetFromSingleQualityLevel(static_cast<int32>(GeneralQualityLevel));
		Scalability::SetQualityLevels(ScalabilityQuality);
	}

	if (InputVersion < UShooterGameUserSettings::CurrentInputVersion) // Reset if updated
	{
		ResetInputSettings();
	}

	Super::ApplySettings(true);
}

int32 UShooterGameUserSettings::GetCurrentLanguage()
{
	return FInternationalization::Get().GetCurrentCulture()->GetLCID();
}

FString UShooterGameUserSettings::GetCurrentLanguageTwoISO()
{
	return FInternationalization::Get().GetCurrentCulture()->GetTwoLetterISOLanguageName();
}

void UShooterGameUserSettings::SetCurrentLanguage(const int32 &localeId)
{
	for (auto &locale : SupportedLanguagesArray)
	{
		if (locale->GetLCID() == localeId)
		{
			Language = locale->GetTwoLetterISOLanguageName();
			FInternationalization::Get().SetCurrentCulture(Language);

			break;
		}
	}
}

void UShooterGameUserSettings::LoadSettings(bool bForceReload)
{
	Super::LoadSettings(bForceReload);

	Scalability::LoadState(*GGameUserSettingsIni);
	ScalabilityQuality = Scalability::GetQualityLevels();
}

void UShooterGameUserSettings::ApplySettings(bool bCheckForCommandLineOverrides)
{
	Super::ApplySettings(bCheckForCommandLineOverrides);

	for (int32 i = 0; i < ARRAY_COUNT(SoundClassVolumes); i++)
	{
		SetSoundVolume(ESoundClassType::Type(i), SoundClassVolumes[i]);
	}

	SetAAMode(AAMode);
	SetEnabledAmbientOcclusion(bAmbientOcclusion);
	SetEnabledMotionBlur(bMotionBlur);
	SetEnabledBloom(bBloom);
}

bool UShooterGameUserSettings::SetSoundClassVolumeByName(FString ClassName, float Volume)
{
	auto AudioDevice = GEngine->GetMainAudioDevice();

	if (!AudioDevice) return false;

	FAudioThreadSuspendContext AudioThreadSuspend;
	PRAGMA_DISABLE_DEPRECATION_WARNINGS
	for (TMap<USoundClass*, FSoundClassProperties>::TConstIterator i(AudioDevice->GetSoundClassPropertyMap()); i; ++i)
	PRAGMA_ENABLE_DEPRECATION_WARNINGS
	{
		USoundClass* SoundClass = i.Key();
		FString SoundClassFullName = SoundClass->GetFullName();
		FString SoundClassName;
		if (SoundClassFullName.Split(".", nullptr, &SoundClassName, ESearchCase::IgnoreCase, ESearchDir::FromEnd))
		{
			if (SoundClassFullName.Contains("Game") && SoundClassName.Equals(ClassName, ESearchCase::IgnoreCase))
			{
				SoundClass->Properties.Volume = Volume;
				return true;
			}
		}
	}

	return false;
}

float UShooterGameUserSettings::GetSoundClassVolumeByName(FString ClassName)
{
	auto AudioDevice = GEngine->GetMainAudioDevice();

	if (!AudioDevice) return -1.0f;

	FAudioThreadSuspendContext AudioThreadSuspend;
	PRAGMA_DISABLE_DEPRECATION_WARNINGS
	for (TMap<USoundClass*, FSoundClassProperties>::TConstIterator i(AudioDevice->GetSoundClassPropertyMap()); i; ++i)
	PRAGMA_ENABLE_DEPRECATION_WARNINGS
	{
		USoundClass* SoundClass = i.Key();
		FString SoundClassName;

		if (SoundClassName.Contains("Game") && SoundClassName.Contains("." + ClassName))
		{
			return SoundClass->Properties.Volume;
		}
	}

	return -1.0f;
}

int32 UShooterGameUserSettings::GetAAMode()
{
	return AAMode;
}

void UShooterGameUserSettings::SetAAMode(int32 NewAAMode)
{
	AAMode = NewAAMode;
	if (auto AAModeCVar = IConsoleManager::Get().FindConsoleVariable(TEXT("r.DefaultFeature.AntiAliasing")))
	{
		if (AAMode >= EAntiAliasingMethod::AAM_MSAA && IsForwardShadingEnabled(GMaxRHIShaderPlatform))
		{
			AAModeCVar->Set(EAntiAliasingMethod::AAM_MSAA);
			if (auto CVar = IConsoleManager::Get().FindConsoleVariable(TEXT("r.MSAACount")))
			{
				CVar->Set((AAMode == EAntiAliasingMethod::AAM_MSAA) ? 2 : 4);
			}
		}
		else
		{
			AAModeCVar->Set((AAMode >= EAntiAliasingMethod::AAM_MSAA) ? EAntiAliasingMethod::AAM_FXAA : AAMode);
		}
	}
}

int32 UShooterGameUserSettings::ConvertAAScalabilityQualityToAAMode(int32 AAScalabilityQuality)
{
	const int32 AAScalabilityQualityToModeLookup[] = { 0, 2, 2, 2 };
	int32 AAQuality = FMath::Clamp(AAScalabilityQuality, 0, 3);

	return AAScalabilityQualityToModeLookup[AAQuality];
}

float UShooterGameUserSettings::GetSoundVolume(const ESoundClassType::Type SoundType)
{
	if (SoundType >= 0 && SoundType < ARRAY_COUNT(SoundClassVolumes))
	{
		return SoundClassVolumes[SoundType];
	}

	return -1.0f;
}

void UShooterGameUserSettings::SetSoundVolume(const ESoundClassType::Type SoundType, const float Volume)
{
	if (SoundType >= 0 && SoundType < ARRAY_COUNT(SoundClassVolumes))
	{
		SoundClassVolumes[SoundType] = Volume;

		switch (SoundType)
		{
		case ESoundClassType::Master:
			SetSoundClassVolumeByName("Master", SoundClassVolumes[SoundType]);
			break;
		case ESoundClassType::SFX:
			SetSoundClassVolumeByName("SFX", SoundClassVolumes[SoundType]);
			break;
		case ESoundClassType::UI:
			SetSoundClassVolumeByName("UI", SoundClassVolumes[SoundType]);
			break;
		case ESoundClassType::Music:
			SetSoundClassVolumeByName("Music", SoundClassVolumes[SoundType]);
			break;
		case ESoundClassType::Voice:
			SetSoundClassVolumeByName("Voice", SoundClassVolumes[SoundType]);
			break;
		case ESoundClassType::VOIP:
			SetSoundClassVolumeByName("VOIP", SoundClassVolumes[SoundType]);
			break;
		}
	}
}

void UShooterGameUserSettings::ResetInputSettings()
{
	auto InputSettings = GetMutableDefault<UInputSettings>();

	if (InputSettings && GConfig)
	{
		InputVersion = UShooterGameUserSettings::CurrentInputVersion;

		if (GConfig->AreFileOperationsDisabled())
		{
			GConfig->EnableFileOperations();
		}

		GConfig->LoadGlobalIniFile(GInputIni, TEXT("Input"), nullptr, true, true);

		InputSettings->ReloadConfig(UInputSettings::StaticClass(), *GInputIni);

		SaveConfig(CPF_Config, *GGameUserSettingsIni);
	}
}

void UShooterGameUserSettings::SetEnabledAmbientOcclusion(const bool InIsEnabled)
{
	bAmbientOcclusion = InIsEnabled;
	if (auto CVar = IConsoleManager::Get().FindConsoleVariable(TEXT("r.DefaultFeature.AmbientOcclusion")))
	{
		CVar->Set(bAmbientOcclusion);
	}
}

void UShooterGameUserSettings::SetEnabledMotionBlur(const bool InIsEnabled)
{
	bMotionBlur = InIsEnabled;
	if (auto CVar = IConsoleManager::Get().FindConsoleVariable(TEXT("r.DefaultFeature.MotionBlur")))
	{
		CVar->Set(bMotionBlur);
	}
}

void UShooterGameUserSettings::SetEnabledBloom(const bool InIsEnabled)
{
	bBloom = InIsEnabled;
	if (auto CVar = IConsoleManager::Get().FindConsoleVariable(TEXT("r.DefaultFeature.Bloom")))
	{
		CVar->Set(bBloom);
	}
}

void UShooterGameUserSettings::SetEnabledAutoFire(const bool InIsEnabled)
{
	bAutofire = InIsEnabled;
}

void UShooterGameUserSettings::SetEnabledAutoTargeting(const bool InIsEnabled)
{
	bAutotarget = InIsEnabled;
}

void UShooterGameUserSettings::SetEnabledDebugInfo(const bool InIsEnabled)
{
	bDebugInfo = InIsEnabled;
}

void UShooterGameUserSettings::SetDynamicStick(const bool InIsEnabled)
{
	bDynamicStick = InIsEnabled;
}
