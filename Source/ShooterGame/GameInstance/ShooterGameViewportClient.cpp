// Copyright 1998-2018 Epic Games, Inc. All Rights Reserved.

#include "ShooterGame.h"
#include "ShooterGameViewportClient.h"
#include "SSafeZone.h"
#include "SThrobber.h"
#include "Player/ShooterLocalPlayer.h"
#include "Notify/SMessageBox.h"
#include "LobbyCharacter.h"
#include "Notify/SNotifyContainer.h"
#include "Utilities/SWidgetHighlighting.h"

UShooterGameViewportClient::UShooterGameViewportClient(const FObjectInitializer& ObjectInitializer)
	: Super(ObjectInitializer)
{
	SetSuppressTransitionMessage(true);
	bAddedStaticWidgets = false;
}

void UShooterGameViewportClient::NotifyPlayerAdded(int32 PlayerIndex, ULocalPlayer* AddedPlayer)
{
	Super::NotifyPlayerAdded(PlayerIndex, AddedPlayer);

 	UShooterLocalPlayer* const ShooterLP = GetValidObjectAs<UShooterLocalPlayer>(AddedPlayer);
 	if (ShooterLP)
 	{
 		ShooterLP->LoadPersistentUser();
 	}
}

void UShooterGameViewportClient::AddViewportWidgetContent(TSharedRef<SWidget> InWidget, const int32 ZOrder)
{
#if !UE_SERVER
	Super::AddViewportWidgetContent(InWidget, ZOrder);
	ViewportContentStack.Add(InWidget);
	ViewportContentStackZOrder.Add(InWidget, ZOrder);

	UE_LOG(LogSlate, Log, TEXT("AddViewportWidgetContent >> Widget: %s"), *InWidget->GetTypeAsString());

	if (bAddedStaticWidgets == false)
	{
		bAddedStaticWidgets = true;

		SAssignNew(WidgetHighlightingPanel, SWidgetHighlighting);
		AddViewportWidgetContent_AlwaysVisible(WidgetHighlightingPanel.ToSharedRef(), 2000);
		AddUsableViewportWidgetContent_AlwaysVisible(SMessageBox::Get(), 1000);
		AddViewportWidgetContent(SNotifyContainer::Get(), 500);

		UE_LOG(LogSlate, Log, TEXT("AddedStaticWidgets"));
	}
#endif
}

void UShooterGameViewportClient::AddViewportWidgetContent_AlwaysVisible(TSharedRef<SWidget> InWidget, const int32 ZOrder)
{
#if !UE_SERVER
	AddViewportWidgetContent(InWidget, ZOrder);
	ViewportContentStackAlwaysVisible.Add(InWidget);

#endif
}

void UShooterGameViewportClient::AddUsableViewportWidgetContent(TSharedRef<SUsableCompoundWidget> InWidget, const int32 ZOrder)
{
#if !UE_SERVER
	if (ViewportContentStackUsable.AddUnique(InWidget) != INDEX_NONE)
	{
		AddViewportWidgetContent(InWidget, ZOrder);
		InWidget->OnToggleInteractive.BindUObject(this, &UShooterGameViewportClient::OnToggleInteractiveAnyWidget);
	}
#endif
}

void UShooterGameViewportClient::AddUsableViewportWidgetContent_AlwaysVisible(TSharedRef<SUsableCompoundWidget> InWidget, const int32 ZOrder)
{
#if !UE_SERVER
	if (ViewportContentStackUsable.AddUnique(InWidget) != INDEX_NONE)
	{
		AddViewportWidgetContent_AlwaysVisible(InWidget, ZOrder);
		InWidget->OnToggleInteractive.BindUObject(this, &UShooterGameViewportClient::OnToggleInteractiveAnyWidget);
	}
#endif
}

void UShooterGameViewportClient::RemoveViewportWidgetContent(TSharedRef<SWidget> InWidget)
{
#if !UE_SERVER
	Super::RemoveViewportWidgetContent(InWidget);
	ViewportContentStack.Remove(InWidget);
	ViewportContentStackHidden.Remove(InWidget);
	ViewportContentStackAlwaysVisible.Remove(InWidget);
	ViewportContentStackZOrder.Remove(InWidget);
	ViewportContentStackUsable.Remove(StaticCastSharedRef<SUsableCompoundWidget>(InWidget));
	LastWidgets.Remove(StaticCastSharedRef<SUsableCompoundWidget>(InWidget));
#endif
}

void UShooterGameViewportClient::RemoveAllViewportWidgetsAlt()
{
#if !UE_SERVER
	RemoveAllViewportWidgets();

	ViewportContentStack.Empty();
	ViewportContentStackZOrder.Empty();
	ViewportContentStackAlwaysVisible.Empty();
	ViewportContentStackHidden.Empty();
	ViewportContentStackUsable.Empty();
	LastWidgets.Empty();

	SMessageBox::ResetQueue();
	SNotifyContainer::ResetQueue();
	FWidgetBaseSupportQueue::ResetQueue();
	FWidgetOrderedSupportQueue::ResetQueue();

	AddUsableViewportWidgetContent_AlwaysVisible(SMessageBox::Get(), 1000);
	AddViewportWidgetContent(SNotifyContainer::Get(), 500);
	SMessageBox::Get()->ToggleWidget(false);

	if (WidgetHighlightingPanel.IsValid())
	{
		AddViewportWidgetContent_AlwaysVisible(WidgetHighlightingPanel.ToSharedRef(), 2000);
	}
#endif
}

TSharedRef<SWidget> UShooterGameViewportClient::GetViewportWidgetByTag(const FName& InTag) const
{
	TSharedRef<SWidget> ReturnWidget = SNullWidget::NullWidget;

	auto Widgets = GetViewportWidgetsByTag(InTag);
	if (Widgets.Num())
	{
		ReturnWidget = Widgets[0];
	}

	return ReturnWidget;
}

TArray<TSharedRef<SWidget>> UShooterGameViewportClient::GetViewportWidgetsByTag(const FName& InTag) const
{
	TArray<TSharedRef<SWidget>> ReturnWidgets;

	for (auto Widget : ViewportContentStack)
	{
		if (Widget->GetTag().IsEqual(InTag))
		{
			ReturnWidgets.Add(Widget);
		}
		else if (WidgetHighlightingPanel.IsValid())
		{
			FGeometry NullGeomatry = WidgetHighlightingPanel->GetCachedGeometry();
			FArrangedChildren Childs(EVisibility::Visible);
			Widget->ArrangeChildren(NullGeomatry, Childs);

			if (Childs.Num())
			{
				for (SIZE_T i = 0; i < Childs.Num(); ++i)
				{
					auto SubChilds = GetViewportWidgetsByTag_Internal(Childs[i].Widget, InTag);
					if (Childs[i].Widget->GetTag().IsEqual(InTag))
					{
						ReturnWidgets.Add(Childs[i].Widget);
					}

					ReturnWidgets.Append(SubChilds);
				}
			}
		}
	}

	return ReturnWidgets;
}

TArray<TSharedRef<SWidget>> UShooterGameViewportClient::GetViewportWidgetsByTag_Internal(const TSharedRef<SWidget>& InWidget, const FName& InTag) const
{
	TArray<TSharedRef<SWidget>> lReturnWidgets;

	FArrangedChildren ArrangedChildren(EVisibility::All);
	InWidget->ArrangeChildren(InWidget->GetCachedGeometry(), ArrangedChildren);

	for (int32 WidgetIndex = 0; WidgetIndex < ArrangedChildren.Num(); ++WidgetIndex)
	{
		if (ArrangedChildren[WidgetIndex].Widget->GetTag().IsEqual(InTag))
		{
			lReturnWidgets.Add(ArrangedChildren[WidgetIndex].Widget);
		}

		auto Childs = GetViewportWidgetsByTag_Internal(ArrangedChildren[WidgetIndex].Widget, InTag);
		for (auto child : Childs)
		{
			if (child->GetTag().IsEqual(InTag))
			{
				lReturnWidgets.Add(child);
			}
		}
	}

	return lReturnWidgets;
}

void UShooterGameViewportClient::HighlightWidgetByTag(const FName& InTag, const float InTimeSeconds)
{
	auto FoundWidget = GetViewportWidgetByTag(InTag);
	if (FoundWidget != SNullWidget::NullWidget && WidgetHighlightingPanel.IsValid())
	{
		WidgetHighlightingPanel->AddWidget(FoundWidget);

		if (FMath::IsNearlyEqual(InTimeSeconds, .0f) == false && GetWorld())
		{
			FTimerHandle _th;
			GetWorld()->GetTimerManager().SetTimer(_th, FTimerDelegate::CreateUObject(this, &UShooterGameViewportClient::UnHighlightWidgetByTag, InTag), InTimeSeconds, false);
		}
	}
}

void UShooterGameViewportClient::UnHighlightWidgetByTag(const FName InTag)
{
	auto FoundWidget = GetViewportWidgetByTag(InTag);
	if (FoundWidget != SNullWidget::NullWidget && WidgetHighlightingPanel.IsValid())
	{
		WidgetHighlightingPanel->RemoveWidget(FoundWidget);
	}
}

void UShooterGameViewportClient::ToggleViewportWidgets(const bool InToggle)
{
#if !UE_SERVER
	if (InToggle)
	{
		if (ViewportContentStackHidden.Num() > 0)
		{
			for (auto Widget : ViewportContentStackHidden)
			{
				ViewportContentStack.Add(Widget);
				if (ViewportContentStackZOrder.Contains(Widget))
				{
					Super::AddViewportWidgetContent(Widget, ViewportContentStackZOrder[Widget]);
				}
				else
				{
					Super::AddViewportWidgetContent(Widget, 0);
				}
			}

			ViewportContentStackHidden.Empty();
		}
	}
	else
	{
		if (ViewportContentStackHidden.Num() == 0)
		{
			for (auto Widget : ViewportContentStack)
			{
				ViewportContentStackHidden.Add(Widget);
				ViewportContentStack.Remove(Widget);
				Super::RemoveViewportWidgetContent(Widget);
			}
		}
	}
#endif
}


void UShooterGameViewportClient::OnToggleInteractiveAnyWidget(const bool InToggle, const TSharedRef<SUsableCompoundWidget>& InWidget)
{
	static bool localIsProccess = false;

	if (localIsProccess) return;
	localIsProccess = true;

	APlayerController* PlayerCtrl = (GetWorld() && GetWorld()->GetFirstPlayerController()) ? GetWorld()->GetFirstPlayerController() : nullptr;
	ACharacter* PlayerChar = PlayerCtrl ? PlayerCtrl->GetCharacter() : nullptr;

	UE_LOG(LogSlate, Log, TEXT("OnToggleInteractiveAnyWidget >> Widget: %s, Toggle: %s"), *InWidget->GetTypeAsString(), InToggle ? TEXT("True") : TEXT("False"));

	if (InToggle)
	{
		SetCurrentWidget(InWidget);

		if (InWidget->bIsAlternativeCamera)
		{
			if (auto TargetChar = GetValidObjectAs<ALobbyCharacter>(PlayerChar))
			{
				TargetChar->SetArmMode(true);
			}
		}

		for (auto MyWidget : ViewportContentStackUsable)
		{
			if (MyWidget->IsInInteractiveMode() && MyWidget->IsUniqueWidget() && InWidget->IsUniqueWidget() && LastWidgets.Contains(MyWidget) == false)
			{
				UE_LOG(LogSlate, Log, TEXT("OnToggleInteractiveAnyWidget >> Widget: %s, Toggle: %s, Toggle false: %s"), *InWidget->GetTypeAsString(), InToggle ? TEXT("True") : TEXT("False"), *MyWidget->GetTypeAsString());

				MyWidget->ToggleWidget(false);
				LastWidgets.AddUnique(MyWidget);
			}
		}
	}
	else
	{
		bool bIsArmMode = false;

		if (LastWidgets.Num() && LastWidgets.Last() != SNullWidget::NullWidget)
		{
			auto MyWidget = LastWidgets.Last();						

			if (InWidget->IsUniqueWidget())
			{				
				SetCurrentWidget(MyWidget);
				bIsArmMode = MyWidget->bIsAlternativeCamera;
				MyWidget->ToggleWidget(true);
				LastWidgets.Remove(MyWidget);
			}

			for (auto TargetWidget : ViewportContentStackUsable)
			{
				if (TargetWidget->IsInInteractiveMode() && TargetWidget->bIsAlternativeCamera && TargetWidget != InWidget)
				{
					bIsArmMode = true;
					break;
				}
			}

			UE_LOG(LogSlate, Log, TEXT("OnToggleInteractiveAnyWidget >> Widget: %s, Toggle: %s, Toggle true: %s"), *InWidget->GetTypeAsString(), InToggle ? TEXT("True") : TEXT("False"), *MyWidget->GetTypeAsString());
		}

		if (bIsArmMode == false)
		{
			if (auto TargetChar = GetValidObjectAs<ALobbyCharacter>(PlayerChar))
			{
				TargetChar->SetArmMode(false);
			}
		}
	}
	
	localIsProccess = false;

	if (InToggle)
	{
		if (PlayerCtrl)
		{
			PlayerCtrl->bShowMouseCursor = true;
			PlayerCtrl->SetInputMode(FInputModeGameAndUI().SetHideCursorDuringCapture(false));
		}

		SetCaptureMouseOnClick(EMouseCaptureMode::CaptureDuringMouseDown);
	}
	else
	{
		if (PlayerCtrl)
		{
			PlayerCtrl->SetInputMode(FInputModeGameAndUI().SetHideCursorDuringCapture(false));
		}
	}
}

void UShooterGameViewportClient::OnBackWidget()
{
	auto MyWidget = GetCurrentWidget();
	if (MyWidget != SNullWidget::NullWidget)
	{
		MyWidget->HideWidget();
		UE_LOG(LogSlate, Log, TEXT("OnBackWidget >> Widget: %s"), *MyWidget->GetTypeAsString());
	}
}

void UShooterGameViewportClient::ShowLoadingScreen()
{
	if ( LoadingScreenWidget.IsValid() )
	{
		return;
	}
	
	LoadingScreenWidget = SNew( SShooterLoadingScreen );

	AddViewportWidgetContent( LoadingScreenWidget.ToSharedRef() );
}

void UShooterGameViewportClient::HideLoadingScreen()
{
	if ( !LoadingScreenWidget.IsValid() )
	{
		return;
	}

	RemoveViewportWidgetContent( LoadingScreenWidget.ToSharedRef() );

	LoadingScreenWidget = nullptr;
}

UShooterGameViewportClient* UShooterGameViewportClient::Get()
{
	if (GEngine && GEngine->IsValidLowLevel() && GEngine->GameViewport)
	{
		return Cast<UShooterGameViewportClient>(GEngine->GameViewport);
	}

	return nullptr;
}

void UShooterGameViewportClient::SetCurrentWidget(TSharedRef<SUsableCompoundWidget> InWidget)
{
	if (!CurrentWidget.Num())
	{
		CurrentWidget.Add(InWidget);
	}

	CurrentWidget[0] = InWidget;
}

TSharedRef<SUsableCompoundWidget> UShooterGameViewportClient::GetCurrentWidget() const
{
	if (!CurrentWidget.Num())
	{
		return StaticCastSharedRef<SUsableCompoundWidget>(SNullWidget::NullWidget);
	}

	return CurrentWidget[0];
}

void SShooterLoadingScreen::Construct(const FArguments& InArgs)
{
	static const FName LoadingScreenName(TEXT("/Game/UserInterface/Menu/LoadingScreen.LoadingScreen"));

	//since we are not using game styles here, just load one image
	LoadingScreenBrush = MakeShareable(new FSlateMaterialBrush(*LoadObject<UMaterialInterface>(NULL, *LoadingScreenName.ToString()), FVector2D(1920, 1080)));

	ChildSlot
	[
		SNew(SOverlay)
		+SOverlay::Slot()
		.HAlign(HAlign_Fill)
		.VAlign(VAlign_Fill)
		[
			SNew(SImage)
			.Image(LoadingScreenBrush.Get())
		]
		+SOverlay::Slot()
		.HAlign(HAlign_Fill)
		.VAlign(VAlign_Fill)
		[
			SNew(SSafeZone)
			.VAlign(VAlign_Bottom)
			.HAlign(HAlign_Right)
			.Padding(10.0f)
			.IsTitleSafe(true)
			[
				SNew(SThrobber)
				.Visibility(this, &SShooterLoadingScreen::GetLoadIndicatorVisibility)
			]
		]
	];
}
