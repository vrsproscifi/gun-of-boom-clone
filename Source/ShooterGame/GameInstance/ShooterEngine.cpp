// Copyright 1998-2018 Epic Games, Inc. All Rights Reserved.

/*=============================================================================
	ShooterEngine.cpp: ShooterEngine c++ code.
=============================================================================*/

#include "ShooterGame.h"
#include "ShooterEngine.h"

#include "Game/Components/NodeComponent/NodeSessionMatch.h"


#include "Game/GameModeData.h"
#include "Game/GameMapData.h"


UShooterEngine::UShooterEngine(const FObjectInitializer& ObjectInitializer)
	: Super(ObjectInitializer)
{
}

void UShooterEngine::Init(IEngineLoop* InEngineLoop)
{
	const FString Commandline = FCommandLine::Get();
	UE_LOG(LogInit, Display, TEXT("UShooterEngine::Init | Commandline: %s"), *Commandline);


#if UE_SERVER
	if (Commandline.Contains(TEXT("nodeId=")))
	{
		FString nodeStringId;
		if (FParse::Value(FCommandLine::Get(), TEXT("nodeId="), nodeStringId))
		{
			FGuid nodeId;
			if (FGuid::Parse(nodeStringId, nodeId))
			{
				FNodeSessionMatch::TokenId = nodeId;
				const auto name = FString::Printf(TEXT("LOKA_%s"), *nodeId.ToString(EGuidFormats::Digits));
				ServerMatchSingleton = new FSystemWideCriticalSection(name);
				if (ServerMatchSingleton && ServerMatchSingleton->IsValid())
				{
					UE_LOG(LogInit, Warning, TEXT("Node Start was successfully: %s"), *nodeStringId);
				}
				else UE_LOG(LogInit, Fatal, TEXT("Node Start, Node %s was already started"), *nodeStringId);
			}
			else UE_LOG(LogInit, Fatal, TEXT("Node Start, Failed Parse GUID: %s"), *nodeStringId);
		}
		else UE_LOG(LogInit, Fatal, TEXT("Node Start, Failed Parse nodeId: %s"), *nodeStringId);
	}
	else UE_LOG(LogInit, Fatal, TEXT("Node Start, Failed Find nodeId"));
#endif

	Super::Init(InEngineLoop);
}

#define DEFAULT_GAME_MAP_OPTION_VALUE nullptr
#define DEFAULT_GAME_MODE_OPTION_VALUE nullptr

EBrowseReturnVal::Type UShooterEngine::Browse(FWorldContext& WorldContext, FURL URL, FString& Error)
{
	UE_LOG(LogInit, Display, TEXT("UShooterEngine::Browse | URL: %s | Error: %s"), *URL.ToString(true), *Error);

#if UE_SERVER

	auto getOldParamValue = [](const FString& Source, const FString& Match) -> int32
	{
		int32 foundIndex = Source.Find("?" + Match);
		if (foundIndex != INDEX_NONE)
		{
			int32 foundSpliter = Source.Find("=", ESearchCase::IgnoreCase, ESearchDir::FromStart, foundIndex);
			int32 foundNext = Source.Find("?", ESearchCase::IgnoreCase, ESearchDir::FromStart, foundSpliter);
			if (foundNext == INDEX_NONE)
			{
				foundNext = Source.Find(" ", ESearchCase::IgnoreCase, ESearchDir::FromStart, foundSpliter);
				if (foundNext == INDEX_NONE)
				{
					foundNext = Source.Len();
				}
			}

			FString value;
			for (int32 i = foundSpliter + 1; i < foundNext; ++i)
			{
				value += Source[i];
			}

			return FCString::Atoi(*value);
		}

		return INDEX_NONE;
	};
	

	const FString Commandline = FCommandLine::Get();
	
	static const auto GameMapOptionName = TEXT("omap");
	static const auto GameModeOptionName = TEXT("ogame");
	static const auto GameOptionName = TEXT("game");

	int32 gameMapValue = getOldParamValue(Commandline, GameMapOptionName);
	int32 gameModeValue = getOldParamValue(Commandline, GameModeOptionName);
	
	if (gameMapValue != INDEX_NONE)
	{
		UE_LOG(LogInit, Warning, TEXT("UShooterEngine::Browse:GameMapOptionName Detected "));

		const auto GameMap = UGameMapData::GetByFlagId(gameMapValue);
		if (GameMap)
		{
			const auto GameMapFileName = GameMap->GetName();
			UE_LOG(LogInit, Display, TEXT("UShooterEngine::Browse | Override Map | From %s to %s"), *URL.Map, *GameMapFileName);
			URL.Map = FString::Printf(TEXT("/Game/Maps/%s"), *GameMapFileName);
		}
		else
		{
			UE_LOG(LogInit, Error, TEXT("UShooterEngine::Browse:GameMapOptionName Detection failed #2"));
		}
	}
	else
	{
		UE_LOG(LogInit, Error, TEXT("UShooterEngine::Browse:GameMapOptionName Detection failed #1"));
	}

	if (gameModeValue != INDEX_NONE)
	{
		UE_LOG(LogInit, Warning, TEXT("UShooterEngine::Browse:GameModeOptionName Detected "));

		const auto GameMode = UGameModeData::GetByFlagId(gameModeValue);
		if (GameMode)
		{
			const FString GameModeFileName = GameMode->GetName();
			const FString GameModeOption = FString::Printf(TEXT("%s=%s"), GameOptionName, *GameModeFileName);
			//UE_LOG(LogInit, Display, TEXT("UShooterEngine::Browse | Override GameMode | From %s to %s"), URL.GetOption(GameOptionName, DEFAULT_GAME_MODE_OPTION_VALUE), *GameModeOption);

			URL.RemoveOption(GameOptionName);
			URL.AddOption(*GameModeOption);
		}
		else
		{
			UE_LOG(LogInit, Fatal, TEXT("UShooterEngine::Browse:GameModeOptionName Detection failed #2"));
		}
	}
	else
	{
		UE_LOG(LogInit, Fatal, TEXT("UShooterEngine::Browse:GameModeOptionName Detection failed #1"));
	}

#endif
	return Super::Browse(WorldContext, URL, Error);
}


void UShooterEngine::HandleNetworkFailure(UWorld *World, UNetDriver *NetDriver, ENetworkFailure::Type FailureType, const FString& ErrorString)
{
	UE_LOG(LogInit, Error, TEXT("HandleNetworkFailure | [FailureType: %d]"), static_cast<int32>(FailureType), *ErrorString);

	// Determine if we need to change the King state based on network failures.

	// Only handle failure at this level for game or pending net drivers.
	//FName NetDriverName = NetDriver ? NetDriver->NetDriverName : NAME_None; 
	//if (NetDriverName == NAME_GameNetDriver || NetDriverName == NAME_PendingNetDriver)
	//{
	//	// If this net driver has already been unregistered with this world, then don't handle it.
	//	//if (World)
	//	{
	//		//UNetDriver * NetDriver = FindNamedNetDriver(World, NetDriverName);
	//		if (NetDriver)
	//		{
	//			switch (FailureType)
	//			{
	//				case ENetworkFailure::FailureReceived:
	//				{
	//					UShooterGameInstance* const ShooterInstance = Cast<UShooterGameInstance>(GameInstance);
	//					if (ShooterInstance && NetDriver->GetNetMode() == NM_Client)
	//					{
	//						const FText OKButton = NSLOCTEXT( "DialogButtons", "OKAY", "OK" );
	//
	//						// NOTE - We pass in false here to not override the message if we are already going to the main menu
	//						// We're going to make the assumption that someone else has a better message than "Lost connection to host" if
	//						// this is the case
	//						ShooterInstance->ShowMessageThenGotoState( FText::FromString(ErrorString), OKButton, FText::GetEmpty(), ShooterGameInstanceState::MainMenu, false );
	//					}
	//					break;
	//				}
	//				case ENetworkFailure::PendingConnectionFailure:						
	//				{
	//					UShooterGameInstance* const GI = Cast<UShooterGameInstance>(GameInstance);
	//					if (GI && NetDriver->GetNetMode() == NM_Client)
	//					{
	//						const FText OKButton = NSLOCTEXT( "DialogButtons", "OKAY", "OK" );
	//
	//						// NOTE - We pass in false here to not override the message if we are already going to the main menu
	//						// We're going to make the assumption that someone else has a better message than "Lost connection to host" if
	//						// this is the case
	//						GI->ShowMessageThenGotoState( FText::FromString(ErrorString), OKButton, FText::GetEmpty(), ShooterGameInstanceState::MainMenu, false );
	//					}
	//					break;
	//				}
	//				case ENetworkFailure::ConnectionLost:						
	//				case ENetworkFailure::ConnectionTimeout:
	//				{
	//					UShooterGameInstance* const GI = Cast<UShooterGameInstance>(GameInstance);
	//					if (GI && NetDriver->GetNetMode() == NM_Client)
	//					{
	//						const FText ReturnReason	= NSLOCTEXT( "NetworkErrors", "HostDisconnect", "Lost connection to host." );
	//						const FText OKButton		= NSLOCTEXT( "DialogButtons", "OKAY", "OK" );
	//
	//						// NOTE - We pass in false here to not override the message if we are already going to the main menu
	//						// We're going to make the assumption that someone else has a better message than "Lost connection to host" if
	//						// this is the case
	//						GI->ShowMessageThenGotoState( ReturnReason, OKButton, FText::GetEmpty(), ShooterGameInstanceState::MainMenu, false );
	//					}
	//					break;
	//				}
	//				case ENetworkFailure::NetDriverAlreadyExists:
	//				case ENetworkFailure::NetDriverCreateFailure:
	//				case ENetworkFailure::OutdatedClient:
	//				case ENetworkFailure::OutdatedServer:
	//				default:
	//					break;
	//			}
	//		}
	//	}
	//}

	// standard failure handling.
	Super::HandleNetworkFailure(World, NetDriver, FailureType, ErrorString);
}

