package com.epicgames.ue4;
//package com.example.sentike.myapplication;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.app.AlarmManager;
import android.app.IntentService;
import android.app.PendingIntent;
import android.content.Context;
import android.content.Intent;
import android.os.PowerManager;
import android.os.SystemClock;
import com.android.volley.toolbox.JsonObjectRequest;
import com.android.volley.toolbox.RequestFuture;
import com.android.volley.toolbox.Volley;
import org.json.JSONException;
import org.json.JSONObject;
import com.android.volley.*;
import com.android.volley.toolbox.*;
import java.util.concurrent.ExecutionException;
import java.util.concurrent.TimeUnit;
import android.util.Log;
import android.os.Build;
import android.app.ActivityManager;
import java.util.Calendar;

import static android.content.Context.POWER_SERVICE;

public class DeviceBootNotifyReceiverService extends BroadcastReceiver
{	
	public static Logger Log = new Logger("UE4-Service");

	public static void ScheduleJobs(Context context)
	{
		Log.debug( "[DeviceBootNotifyReceiverService][ScheduleJobs]");	
		
		try
		{
			context.startService(new Intent(context, PlayerNotifyService.class));
		}
		catch (Exception e)
		{
			Log.error(String.format("[DeviceBootNotifyReceiverService][ScheduleJobs][0] | Exception: %s", e.getMessage()));
		}

	}
	
	public static void CancelAlarmReceiver(Context context)
	{
		try
		{	
			AlarmManager am = (AlarmManager) context.getSystemService(Context.ALARM_SERVICE);
			Intent intent = new Intent(context, DeviceBootNotifyReceiverService.class).setAction("REFRESH_THIS");

			PendingIntent pintent =  PendingIntent.getBroadcast(context, 0, intent, 0);
			am.cancel(pintent);
		}
		catch (Exception e)
		{
			Log.error(String.format("[DeviceBootNotifyReceiverService][CancelAlarmReceiver] | Exception: %s", e.getMessage()));
		}
	}
	
	public static void SheuldeAlarmReceiver(Context context, int InDelayInSeconds)
    {
		try
		{	
			long InDelayInMs = 1000 * InDelayInSeconds;
			AlarmManager am = (AlarmManager) context.getSystemService(Context.ALARM_SERVICE);
			Intent intent = new Intent(context, DeviceBootNotifyReceiverService.class).setAction("REFRESH_THIS");
            
            
			PendingIntent pintent =  PendingIntent.getBroadcast(context, InDelayInSeconds, intent, PendingIntent.FLAG_UPDATE_CURRENT);


            long SheuldedDate = SystemClock.elapsedRealtime() + InDelayInMs;

			int SDK_INT = Build.VERSION.SDK_INT;
			if (SDK_INT < Build.VERSION_CODES.KITKAT)
			{
				am.set(AlarmManager.ELAPSED_REALTIME_WAKEUP, SheuldedDate, pintent);
			}
			else if (Build.VERSION_CODES.KITKAT <= SDK_INT && SDK_INT < Build.VERSION_CODES.M)
			{
				am.setExact(AlarmManager.ELAPSED_REALTIME_WAKEUP, SheuldedDate, pintent);
			}
			else if (SDK_INT >= Build.VERSION_CODES.M) 
			{
				am.setExactAndAllowWhileIdle(AlarmManager.ELAPSED_REALTIME_WAKEUP, SheuldedDate, pintent);
			}
		}
		catch (Exception e)
		{
			Log.error(String.format("[DeviceBootNotifyReceiverService][SheuldeAlarmReceiver][%d] | Exception: %s", InDelayInSeconds, e.getMessage()));
		}
    }
	
	
	private static boolean isServiceRunning(Context context, Class<?> serviceClass)
    {
        ActivityManager manager = (ActivityManager) context.getSystemService(Context.ACTIVITY_SERVICE);
        for (ActivityManager.RunningServiceInfo service : manager.getRunningServices(Integer.MAX_VALUE)) {
            if (serviceClass.getName().equals(service.service.getClassName())) {
                return true;
            }
        }
        return false;
    }
	
	
    @Override
    public void onReceive(Context context, Intent intent)
    {

		try
		{
			String IntentAction = null;
			Calendar CurrentDate = Calendar.getInstance();
			int CurrentHour = CurrentDate.get(Calendar.HOUR_OF_DAY);
			int CurrentMinutes = CurrentDate.get(Calendar.MINUTE);

			if(intent != null)
			{
				IntentAction = intent.getAction();
			}

			Log.debug( String.format("[DeviceBootNotifyReceiverService][onReceive][Action: %s][CurrentHour: %d][CurrentMinutes: %d]", IntentAction, CurrentHour, CurrentMinutes));
			//DeviceBootNotifyReceiverService.SheuldeAlarmReceiver(context, 20 * 60);
			//DeviceBootNotifyReceiverService.SheuldeAlarmReceiver(context, 40 * 60);

			DeviceBootNotifyReceiverService.SheuldeAlarmReceiver(context, 10);
			DeviceBootNotifyReceiverService.SheuldeAlarmReceiver(context, 20);


			if(CurrentHour >= 8 && CurrentHour <= 22 )
			{
				PowerManager pm = (PowerManager) context.getSystemService(POWER_SERVICE);
				PowerManager.WakeLock wakeLock = pm.newWakeLock(PowerManager.PARTIAL_WAKE_LOCK, "LokaAW:Notify");
				wakeLock.acquire();
				ScheduleJobs(context);
				if (wakeLock.isHeld()) {
					wakeLock.release();
				}
			}
			else
			{
				Log.debug( String.format("[DeviceBootNotifyReceiverService][onReceive][ScheduleJobs][Night self][CurrentHour: %d][CurrentMinutes: %d]", CurrentHour, CurrentMinutes));
			}
		}
		catch (Exception e)
		{
			Log.error(String.format("[DeviceBootNotifyReceiverService][onReceive][0] | Exception: %s", e.getMessage()));
		}
    }
}