package com.epicgames.ue4;
//package com.example.sentike.myapplication;

//=========================================

//=========================================
import android.content.Intent;
import android.net.http.*;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import com.VRSPro.LOKA_AW.R;
import com.VRSPro.LOKA_AW.R.drawable;
import com.VRSPro.LOKA_AW.R.string;
import org.json.JSONArray;

import java.util.Random;
import android.content.Context;


import android.content.Intent;
import android.content.pm.PackageInfo;
import android.content.pm.PackageManager;
import android.net.http.*;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import com.VRSPro.LOKA_AW.R;
import com.VRSPro.LOKA_AW.R.drawable;
import com.VRSPro.LOKA_AW.R.string;
import org.json.JSONArray;

import java.util.Random;
import android.content.Context;
import org.json.JSONArray;
import java.util.regex.Matcher;
import java.util.regex.Pattern;


public class PlayerNotifyService extends AbstractNotifyPoolService
{
	public PlayerNotifyService()
	{
		super("PlayerNotifyService", 3000000);
	}

	public String StoreVersion = null;

	private String GetStoreVersion()
	{

		Log.debug( "[PlayerCheckGameVersion][GetStoreVersion]");

		try
        {
            //=================================================
			Context context = getApplicationContext();
			PackageManager manager = context.getPackageManager();

			PackageInfo info = manager.getPackageInfo(context.getPackageName(), 0);
			StoreVersion = Integer.toString(info.versionCode);

			//final Pattern p = Pattern.compile("StoreVersion=([0-9]*)");
			//StoreVersion = GetGameUserSettingsParam(p, true, null);
			
			//=================================================
			Log.debug( String.format("[PlayerCheckGameVersion][GetStoreVersion][StoreVersion][%s]", StoreVersion));

			//=================================================
			return StoreVersion;
        }
        catch (Exception e)
        {
            Log.error( String.format("[PlayerCheckGameVersion][GetStoreVersion][Exception]: %s", e.getMessage() ));
        }

        Log.debug( "[PlayerCheckGameVersion][GetStoreVersion][StoreVersion not found]");
        return null;	
	}

	@Override
	protected String GetAdditionalOperationUrlParams()
	{
		String version = GetStoreVersion();
		if(version != null)
		{
			return "&version=" + version;
		}
		
		return null;
	}
	
	@Override
	protected String GetOperationUrl()
	{
		return "NotifyV2/GetAvailablePlayersNotifyList";
	}

	@Override
	protected void SendRequest(String InUrl)
	{
		SendRequestJsonObject(InUrl);
	}

	public enum PlayerActivityNotificationType
	{
		None,

		/// <summary>
		/// Когда показывать следующее уведомление, после окончания боя
		/// </summary>
		AfterMatch,

		/// <summary>
		/// Когда показывать следующее уведомление, если игрок давно не заходил в игру
		/// </summary>
		LowActivity,

		/// <summary>
		/// Когда показывать следующее уведомления, если игрок почти получил свой уровень
		/// </summary>
		BeforeLevelUp,

		/// <summary>
		/// Когда показывать следующее уведомления после повышения уровня
		/// </summary>
		AfterLevelUp,
	}

	final Random RandomService = new Random();

	@Override
	protected void OnProcessComplete()
	{
		//ScheduleJobs(getApplicationContext());
	}


	public enum PlayersNotifyStatus
	{
		None,

		NoAvailableNotify,
		InvalidInputData,
		Successfully,
		Exception,

		PlayerNotFound,
		EntityNotFound,

		AllowNotificationBroadcastFalse,
		AllowNotificationTargetFalse,
		Invalid,
	};
	
	protected void NotifyPlayerReturn(JSONArray AvalibleNotifications)
	{
		for(int i=0; i < AvalibleNotifications.length(); i++)
		{
			try
			{
				//====================================================
				int NotificationRawType = AvalibleNotifications.getInt(i);
				PlayerActivityNotificationType NotificationType = PlayerActivityNotificationType.values()[NotificationRawType];

				//====================================================
				Log.debug(String.format("[PlayerReturnNotifyService][AvalibleNotifications][%d / %d] | NotificationRawType: %d / %s", i, AvalibleNotifications.length(), NotificationRawType, NotificationType.toString()));

				if(NotificationType == PlayerActivityNotificationType.LowActivity)
				{
					PushNotify(NotificationRawType, R.string.Return_LowActivity_Title, R.string.Return_LowActivity_Message);
				}
				else if(NotificationType == PlayerActivityNotificationType.AfterLevelUp)
				{
					if(RandomService.nextBoolean())
					{
						PushNotify(NotificationRawType, R.string.Return_AfterLevelUp_Weapon_Title, R.string.Return_AfterLevelUp_Weapon_Message);
					}
					else
					{
						PushNotify(NotificationRawType, R.string.Return_AfterLevelUp_Armour_Title, R.string.Return_AfterLevelUp_Armour_Message);
					}
				}
				else if(NotificationType == PlayerActivityNotificationType.BeforeLevelUp)
				{
					PushNotify(NotificationRawType, R.string.Return_BeforeLevelUp_Title, R.string.Return_BeforeLevelUp_Message);
				}
				else if(NotificationType == PlayerActivityNotificationType.AfterMatch)
				{
					PushNotify(NotificationRawType, R.string.Return_AfterMatch_Title, R.string.Return_AfterMatch_Message);
				}
			}
			catch (Exception e)
			{
				Log.debug(String.format("[PlayerReturnNotifyService][AvalibleNotifications][%d / %d] | [isNull: %b] | Exception: %s", i, AvalibleNotifications.length() - 1, AvalibleNotifications.isNull(i), e.getMessage()));
			}
		}
	}
	
	protected void NotifyPlayerCases(JSONArray AvalibleNotifications)
	{
		for(int i=0; i < AvalibleNotifications.length(); i++)
		{
			try 
			{
				int CaseId = AvalibleNotifications.getInt(i);

				Log.debug(String.format("[PlayerCaseNotifyService][AvalibleNotifications][%d / %d] | CaseId: %d", i, AvalibleNotifications.length()  - 1, CaseId));

				if (CaseId == 6) 
				{
					PushNotify(CaseId, R.string.Case_Box_Title, R.string.Case_Box_Message);
				} 
				else 
				{
					PushNotify(CaseId, R.string.Case_Trophy_Title, R.string.Case_Trophy_Message);
				}
			}
			catch (Exception e)
			{
				Log.debug(String.format("[PlayerCaseNotifyService][AvalibleNotifications][%d / %d] | [isNull: %b] | Exception: %s", i, AvalibleNotifications.length() - 1, AvalibleNotifications.isNull(i), e.getMessage()));
			}
		}
	}
	
	protected void NotifyPlayerRating(JSONArray AvalibleNotifications)
	{
		if(AvalibleNotifications != null && AvalibleNotifications.length() > 0)
		{
			Log.debug(String.format("[PlayerRatingNotifyService][AvalibleNotifications.length: %d]", AvalibleNotifications.length() ));
			try
			{
				//====================================================
				int LostRating = AvalibleNotifications.getInt(0);

				//====================================================
				Log.debug(String.format("[PlayerReturnNotifyService][AvalibleNotifications][LostRating: %d]", LostRating));

				//====================================================
				PushNotify(12345, R.string.Player_LostRating_Title, String.format(getResources().getString(R.string.Player_LostRating_Message), LostRating ));

			}
			catch (Exception e)
			{
				Log.debug(String.format("[PlayerReturnNotifyService][AvalibleNotifications][%d] | [isNull: %b] | Exception: %s", AvalibleNotifications.length(), AvalibleNotifications.isNull(0), e.getMessage()));
			}
		}
	}
	
	
	public enum CheckGameVersionResponse
	{
        None,
        UpdateNotExist,
        UpdateCheckException,
        UpdateAvailable,
        UpdateRequired,
        InvalidGameVersion,
	}
	
	protected void NotifyGameVersion(JSONArray AvalibleNotifications)
	{
		for(int i=0; i < AvalibleNotifications.length(); i++)
		{
			try
			{
				//====================================================
				int NotificationRawType = AvalibleNotifications.getInt(i);
				CheckGameVersionResponse NotificationType = CheckGameVersionResponse.values()[NotificationRawType];

				//====================================================
				Log.debug(String.format("[PlayerCheckGameVersion][AvalibleNotifications][%d / %d] | NotificationRawType: %d / %s", i, AvalibleNotifications.length(), NotificationRawType, NotificationType.toString()));

				if(NotificationType == CheckGameVersionResponse.UpdateRequired)
				{
					PushNotify(NotificationRawType, R.string.Update_Update_Title, R.string.Update_UpdateRequired_Message);
				}
				else if(NotificationType == CheckGameVersionResponse.UpdateAvailable)
				{
					PushNotify(NotificationRawType, R.string.Update_Update_Title, R.string.Update_UpdateAvailable_Message);
				}
			}
			catch (Exception e)
			{
				Log.debug(String.format("[PlayerCheckGameVersion][AvalibleNotifications][%d / %d] | [isNull: %b] | Exception: %s", i, AvalibleNotifications.length() - 1, AvalibleNotifications.isNull(i), e.getMessage()));
			}
		}
	}	
	
		
	public enum PlayersNotifyCategory
	{
		None,
		GameUpdate,
		PlayerRating,
		PlayerReturn,
		PlayerCases,
		Invalid,
	};
	
	@Override
	protected void OnProcessHandle(Object InObject) throws JSONException
	{
		JSONObject Object = (JSONObject)InObject;
		//=====================================
		int AvalibleNotificationsNum = -1;

		//=====================================
		int RawCategory = (int)Object.get("Category");
		int RawStatus = (int)Object.get("Status");

		//=====================================
		PlayersNotifyCategory Category = PlayersNotifyCategory.values()[RawCategory];
		PlayersNotifyStatus Status = PlayersNotifyStatus.values()[RawStatus];

		//=====================================
		JSONArray AvalibleNotifications = (JSONArray)Object.get("NotifyIds");
		if(AvalibleNotifications != null)
		{
			AvalibleNotificationsNum = AvalibleNotifications.length();
		}
			
		//=====================================
		Log.debug
		(
			String.format("[GetAvailablePlayersNotifyList][Category: %s / %d][Status: %s / %d][AvalibleNotifications: %d]",
			Category.toString(), RawCategory, Status.toString(), RawStatus, AvalibleNotificationsNum
		));
		
		//=====================================
		if(Status != PlayersNotifyStatus.Successfully)
		{
			return;
		}
		
		//=====================================
		if(Category == PlayersNotifyCategory.GameUpdate)
		{
			NotifyGameVersion(AvalibleNotifications);
		}
		else if(Category == PlayersNotifyCategory.PlayerRating)
		{
			NotifyPlayerRating(AvalibleNotifications);
		}
		else if(Category == PlayersNotifyCategory.PlayerReturn)
		{
			NotifyPlayerReturn(AvalibleNotifications);
		}
		else if(Category == PlayersNotifyCategory.PlayerCases)
		{
			NotifyPlayerCases(AvalibleNotifications);
		}
	}
}
