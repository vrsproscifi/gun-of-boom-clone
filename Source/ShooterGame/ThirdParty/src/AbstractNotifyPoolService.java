package com.epicgames.ue4;
//package com.example.sentike.myapplication;

//=========================================
//	For Json & Http
import android.app.AlarmManager;
import android.app.IntentService;
import android.app.PendingIntent;
import android.app.job.JobInfo;
import android.app.job.JobParameters;
import android.app.job.JobScheduler;
import android.app.job.JobService;
import android.content.ComponentName;
import android.content.Context;
import android.content.Intent;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.os.Build;
import android.os.PowerManager;
import android.os.SystemClock;
import com.android.volley.toolbox.JsonObjectRequest;
import com.android.volley.toolbox.RequestFuture;
import com.android.volley.toolbox.Volley;
import org.json.JSONException;
import org.json.JSONObject;
import com.android.volley.*;
import com.android.volley.toolbox.*;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileReader;
import java.io.IOException;
import java.util.UUID;
import java.util.concurrent.ExecutionException;
import java.util.concurrent.TimeUnit;
import android.util.Log;
import org.json.JSONArray;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

public abstract class AbstractNotifyPoolService extends IntentService
{
    public static Logger Log = new Logger("UE4-Service");


    static RequestQueue RequestQueue;
    final String ServerHost = "http://136.243.2.83:4444";
    int NotifyIdGrpoup = 1000000;


    public AbstractNotifyPoolService(String name, int InNextNotifyId)
    {
        super(name);
        NotifyIdGrpoup = InNextNotifyId;
    }

    public static void StartService(Context InContext, Class<?> InServiceClass, int InIntervalInSeconds)
    {
        //=====================================================
        int JobSchedulerId = InServiceClass.hashCode() + InIntervalInSeconds;

        //=====================================================
        Log.debug( String.format("[AbstractNotifyPoolService][StartService][InServiceClass: %s / %d][InIntervalInSeconds: %d][JobSchedulerId: %d]", InServiceClass.getSimpleName(), InServiceClass.hashCode(), InIntervalInSeconds, JobSchedulerId));

        if(RequestQueue == null)
        {
            Log.debug( String.format("[AbstractNotifyPoolService][StartService][newRequestQueue][begin][InServiceClass: %s]", InServiceClass.getSimpleName()));
            RequestQueue = Volley.newRequestQueue(InContext);
            Log.debug( String.format("[AbstractNotifyPoolService][StartService][newRequestQueue][end][InServiceClass: %s]", InServiceClass.getSimpleName()));
        }

        try
        {
            //=====================================================
            long InIntervalInMs =  InIntervalInSeconds * 1000;
            Intent ServiceClassIntent = new Intent(InContext, InServiceClass);

            //=====================================================
            ComponentName ServiceComponentName = new ComponentName(InContext.getPackageName(), InServiceClass.getName());

            //=====================================================
            JobScheduler JobSchedulerService = (JobScheduler)InContext.getSystemService(Context.JOB_SCHEDULER_SERVICE);

            //=====================================================
            JobInfo.Builder JobServiceBuilder = new JobInfo.Builder(JobSchedulerId, ServiceComponentName)
                    .setRequiredNetworkType(JobInfo.NETWORK_TYPE_ANY)
                    .setMinimumLatency(InIntervalInMs)
                    .setOverrideDeadline(TimeUnit.SECONDS.toMillis(5))
                    .setRequiresDeviceIdle(false)
                    .setRequiresCharging(false)
                    .setPersisted(true);

            //=====================================================
            int JobServiceScheduleStatus = JobSchedulerService.schedule(JobServiceBuilder.build());

            if(JobServiceScheduleStatus == JobScheduler.RESULT_SUCCESS)
            {
                Log.debug( String.format("[AbstractNotifyPoolService][StartService][InServiceClass: %s][Successfully JobServiceSchedule: %d]", InServiceClass.getSimpleName(), JobSchedulerId));
            }
            else
            {
                Log.error( String.format("[AbstractNotifyPoolService][StartService][InServiceClass: %s][Failed JobServiceSchedule: %d]", InServiceClass.getSimpleName(), JobSchedulerId));
            }
        }
        catch (Exception e)
        {
            Log.error( String.format("[AbstractNotifyPoolService][StartService][InServiceClass: %s][Exception: %s]", InServiceClass.getSimpleName(), e.getMessage()));
        }
    }


    protected void SendRequestJsonObject(String InUrl)
    {
		if(RequestQueue == null)
        {
            Log.debug( String.format("[AbstractNotifyPoolService][SendRequestJsonObject][newRequestQueue][begin][InServiceClass: %s]", this.getClass().getSimpleName()));
            RequestQueue = Volley.newRequestQueue(this);
            Log.debug( String.format("[AbstractNotifyPoolService][SendRequestJsonObject][newRequestQueue][end][InServiceClass: %s]", this.getClass().getSimpleName()));
        }

        Log.debug(String.format("[AbstractNotifyPoolService][SendRequestJsonObject][Url: %s]", InUrl));
        try
        {
            JsonObjectRequest request = new JsonObjectRequest
                    (
                            Request.Method.GET, InUrl, null,

                            new Response.Listener<JSONObject>()
                            {
                                @Override
                                public void onResponse(JSONObject response)
                                {
                                    OnProcessHandleProxy(response);
                                }
                            }, null
                    );

            RequestQueue.add(request);
        }
        catch (Exception e)
        {
            Log.error( String.format("[AbstractNotifyPoolService][SendRequestJsonArray][Url: %s][Exception: %s]", InUrl, e.getMessage()));
        }

    }

    protected void SendRequestJsonArray(String InUrl)
    {
        if(RequestQueue == null)
        {
            Log.debug( String.format("[AbstractNotifyPoolService][SendRequestJsonArray][newRequestQueue][begin][InServiceClass: %s]", this.getClass().getSimpleName()));
            RequestQueue = Volley.newRequestQueue(this);
            Log.debug( String.format("[AbstractNotifyPoolService][SendRequestJsonArray][newRequestQueue][end][InServiceClass: %s]", this.getClass().getSimpleName()));
        }

        Log.debug(String.format("[AbstractNotifyPoolService][SendRequestJsonArray][Url: %s]", InUrl));
        try
        {
            JsonArrayRequest request = new JsonArrayRequest
                    (
                            Request.Method.GET, InUrl, null,

                            new Response.Listener<JSONArray>()
                            {
                                @Override
                                public void onResponse(JSONArray response)
                                {
                                    OnProcessHandleProxy(response);
                                }
                            }, null
                    );

            RequestQueue.add(request);
        }
        catch (Exception e)
        {
            Log.error( String.format("[AbstractNotifyPoolService][SendRequestJsonArray][Url: %s][Exception: %s]", InUrl, e.getMessage()));
        }
    }

    protected abstract void SendRequest(String InUrl);

    public static String PlayerAccountId;

    protected boolean IsValidPlayerAccountId()
    {
        try {
            //=================================================
            if (PlayerAccountId == null || PlayerAccountId.isEmpty()) {
                return false;
            }

            //=================================================
            UUID uuid = UUID.fromString(PlayerAccountId);
            return true;
        }
        catch (Exception e)
        {
            return false;
        }
    }

	private boolean bApplicationUserSettingsFileLoaded = false;
	private String GameUserSettingsContentString = null;
	
	private void LoadGameUserSettings()
	{		
		//=================================================
		if(bApplicationUserSettingsFileLoaded && GameUserSettingsContentString != null)
		{
			return;
		}
		
		try
		{
			//=================================================
			String line;

			//=================================================
			StringBuilder GameUserSettingsContent = new StringBuilder(4096);
			
			//=================================================
			final File ApplicationRootDirectory = this.getFilesDir();

			Log.debug( String.format("[AbstractNotifyPoolService][LoadGameUserSettings][ApplicationRootDirectory][%s]", ApplicationRootDirectory.toString()));

			final File ApplicationUserSettingsFile = new File(ApplicationRootDirectory,"UE4Game/LOKA_AW/LOKA_AW/Saved/Config/Android/GameUserSettings.ini");

			final boolean ApplicationUserSettingsFileExist = ApplicationUserSettingsFile.exists();

			Log.debug( String.format("[AbstractNotifyPoolService][LoadGameUserSettings][ApplicationRootDirectory][%s][canRead: %b][isFile: %b][ApplicationUserSettingsFileExist: %b]", ApplicationUserSettingsFile.toString(), ApplicationUserSettingsFile.canRead(), ApplicationUserSettingsFile.isFile(), ApplicationUserSettingsFileExist ));

			//=================================================
			if(ApplicationUserSettingsFileExist == false)
			{
				return;
			}
			
			//=================================================
			BufferedReader br = new BufferedReader(new FileReader(ApplicationUserSettingsFile));

			//=================================================
			while ((line = br.readLine()) != null)
			{
				GameUserSettingsContent.append(line);
				GameUserSettingsContent.append('\n');
			}
			
			//=================================================
			br.close();

			//=================================================
			GameUserSettingsContentString = GameUserSettingsContent.toString();

			//=================================================
			Log.debug( String.format("[AbstractNotifyPoolService][LoadGameUserSettings][UserId][GameUserSettingsContentString.Len: %d]", GameUserSettingsContentString.length()));
			
			//=================================================
			bApplicationUserSettingsFileLoaded = GameUserSettingsContentString.length() > 0;
		}
		catch (Exception e)
        {
			Log.error( String.format("[AbstractNotifyPoolService][LoadGameUserSettings][Exception]: %s", e.getMessage() ));
        }
	}
	
	protected String GetGameUserSettingsParam(Pattern InPattern, boolean InAllowTrim, String InDefaultResult)
	{
		//=================================================
		LoadGameUserSettings();
		
		//=================================================
		try
		{
			Matcher m = InPattern.matcher(GameUserSettingsContentString);
			final boolean MatchFinded = m.find();

			//=================================================
			Log.debug( String.format("[AbstractNotifyPoolService][GetGameUserSettingsParam][%s / %b][Matcher.Len: %d][MatchFinded: %b]", InPattern.toString(), InAllowTrim, m.groupCount(), MatchFinded));

			//=================================================
			if(MatchFinded)
			{
				//=================================================
				String Result = m.group(1);
				
				//=================================================
				if(InAllowTrim)
				{
					Result = Result.trim();
				}

				//=================================================
				Log.debug( String.format("[AbstractNotifyPoolService][GetGameUserSettingsParam][%s][InAllowTrim: %b][%s]", InPattern.toString(), InAllowTrim, Result));

				//=================================================
				return Result;
			}
		}
		catch (Exception e)
        {
			Log.error( String.format("[AbstractNotifyPoolService][GetGameUserSettingsParam][%s][Exception]: %s", InPattern.toString(), e.getMessage() ));
        }
		return InDefaultResult;
	}
	
    protected String GetPlayerAccountId()
    {
        try
        {
            if(IsValidPlayerAccountId())
            {
                return PlayerAccountId;
            }

            //=================================================
            Log.debug( "[AbstractNotifyPoolService][GetPlayerAccountId]");

			final Pattern p = Pattern.compile("PlayerAccountUUID=([0-9a-fA-F]{8}\\-[0-9a-fA-F]{4}\\-[0-9a-fA-F]{4}\\-[0-9a-fA-F]{4}\\-[0-9a-fA-F]{12})");
			PlayerAccountId = GetGameUserSettingsParam(p, true, null);
			
			//=================================================
			Log.debug( String.format("[AbstractNotifyPoolService][GetPlayerAccountId][UserId][%s]", PlayerAccountId));

			//=================================================
			return PlayerAccountId;
        }
        catch (Exception e)
        {
            Log.error( String.format("[AbstractNotifyPoolService][GetPlayerAccountId][Exception]: %s", e.getMessage() ));
        }

        Log.debug( "[AbstractNotifyPoolService][GetPlayerAccountId][UserId not found]");
        return null;
    }
	
    protected abstract String GetAdditionalOperationUrlParams();
    protected abstract String GetOperationUrl();
    protected abstract void OnProcessHandle(Object InObject) throws JSONException;

    protected void PushNotify(int InNotifyId, String InNotifyTitle, String InNotifyMessage)
    {
        final int NotifyId = NotifyIdGrpoup + InNotifyId;
        try
        {
            Log.debug(String.format("[AbstractNotifyPoolService][PushNotify][NextNotifyId: %d][InNotifyTitle: %s][InNotifyMessage: %s]", NotifyId, InNotifyTitle,InNotifyMessage ));
            // Create callback for PendingIntent
            Intent notificationIntent = new Intent(this, LocalNotificationReceiver.class);

            // Add user-provided data
            notificationIntent.putExtra("local-notification-ID", NotifyId);
            notificationIntent.putExtra("local-notification-title", InNotifyTitle);
            notificationIntent.putExtra("local-notification-body", InNotifyMessage);

            // Designate the callback as a PendingIntent
            PendingIntent pendingIntent = PendingIntent.getBroadcast(this, NotifyId, notificationIntent, PendingIntent.FLAG_UPDATE_CURRENT);

            //	Показать уведомление через 500мс
            long futureTimeInMillis = SystemClock.elapsedRealtime() + 500;
            AlarmManager alarmManager = (AlarmManager)getSystemService(Context.ALARM_SERVICE);

            //Schedule the operation by using AlarmService
            alarmManager.set(AlarmManager.ELAPSED_REALTIME_WAKEUP, futureTimeInMillis, pendingIntent);
        }
        catch (Exception e)
        {
            Log.error(String.format("[AbstractNotifyPoolService][PushNotify][InNotifyId: %d / %d][InNotifyTitle: %s] | Exception: %s", InNotifyId, NotifyId, InNotifyTitle, e.getMessage()));
        }
    }

    protected void PushNotify(int InNotifyId, int InNotifyTitle, int InNotifyMessage)
    {
        final String NotifyTitle = getResources().getString(InNotifyTitle);
        final String NotifyMessage = getResources().getString(InNotifyMessage);
        PushNotify(InNotifyId, NotifyTitle, NotifyMessage);
    }
	
    protected void PushNotify(int InNotifyId, int InNotifyTitle, String InNotifyMessage)
    {
        final String NotifyTitle = getResources().getString(InNotifyTitle);
        PushNotify(InNotifyId, NotifyTitle, InNotifyMessage);
    }

    protected abstract void OnProcessComplete();


    @Override
    public void onDestroy()
    {
        Log.debug( String.format("[AbstractNotifyPoolService][onDestroy][InServiceClass: %s / %d]", this.getClass().getSimpleName(), this.getClass().hashCode()));
        super.onDestroy();
    }
	
    private void OnProcessHandleProxy(Object ResponseObject)
    {
        //========================================
        final String OperationUrl = GetOperationUrl();
        final String PlayerAccountId = GetPlayerAccountId();
		final String AdditionalOperationUrlParams = GetAdditionalOperationUrlParams();
        
		//========================================
		String OperationUrlRequest = ServerHost + "/" + OperationUrl + "/?Id=" + PlayerAccountId;

		if(AdditionalOperationUrlParams != null)
		{
			OperationUrlRequest += AdditionalOperationUrlParams;
		}

        //========================================
        Log.debug(String.format("[AbstractNotifyPoolService][OnProcessHandleProxy][OperationUrl: %s][PlayerAccountId: %s][OperationUrlRequest: %s][HasObject: %b][1]", OperationUrl, PlayerAccountId, OperationUrlRequest,  ResponseObject != null ));

        //========================================
        if(ResponseObject != null)
        {
            try
            {
                OnProcessHandle(ResponseObject);
            }
            catch (Exception e)
            {
                Log.error( String.format("[AbstractNotifyPoolService][OnProcessHandleProxy][Url: %s][Exception: %s]", OperationUrlRequest, e.getMessage()));
                e.printStackTrace();
            }
        }
    }

	protected boolean IsNetworkConnected()
	{
		try
		{
			final ConnectivityManager cm = (ConnectivityManager)getSystemService(CONNECTIVITY_SERVICE);
			final NetworkInfo activeNetwork = cm.getActiveNetworkInfo();
			final boolean isConnected = activeNetwork != null && activeNetwork.isConnectedOrConnecting();
			return isConnected;		
		}
		catch (Exception e)
		{
			Log.error( String.format("[AbstractNotifyPoolService][IsNetworkConnected][Exception: %s]", e.getMessage()));
			e.printStackTrace();
		}	
		return false;
	}
	
    @Override
    protected void onHandleIntent(Intent intent)
    {
        try
        {
			final boolean isConnected = IsNetworkConnected();

            //========================================
            Log.error( String.format("[AbstractNotifyPoolService][onHandleIntent][%s][isConnected: %b]", this.getClass().getSimpleName(), isConnected));

			//========================================
			if(isConnected == false)
			{
				return;
			}
			
            //========================================
            PowerManager.WakeLock wakeLock = null;

            //========================================
            try
            {
                final PowerManager pm = (PowerManager) getSystemService(POWER_SERVICE);
                wakeLock = pm.newWakeLock(PowerManager.PARTIAL_WAKE_LOCK, "LokaAW:Notify");
                wakeLock.acquire();
            }
            catch (Exception e)
            {
                Log.error( String.format("[AbstractNotifyPoolService][onStartJob][Thread][PowerManager][0][Exception]: %s", e.getMessage()));
            }

            //========================================
            try
            {
                //========================================
                final String OperationUrl = GetOperationUrl();
                final String PlayerAccountId = GetPlayerAccountId();
				final String AdditionalOperationUrlParams = GetAdditionalOperationUrlParams();

                //========================================
                String OperationUrlRequest = ServerHost + "/" + OperationUrl + "/?Id=" + PlayerAccountId;

				if(AdditionalOperationUrlParams != null)
				{
					OperationUrlRequest += AdditionalOperationUrlParams;
				}

                Log.debug(String.format("[AbstractNotifyPoolService][onStartJob][OperationUrl: %s][PlayerAccountId: %s][OperationUrlRequest: %s][0]", OperationUrl, PlayerAccountId, OperationUrlRequest ));

                //========================================
                if(PlayerAccountId != null)
                {
                    SendRequest(OperationUrlRequest);
                }
            }
            catch (Exception e)
            {
                Log.error( String.format("[AbstractNotifyPoolService][onStartJob][Thread][Exception]: %s", e.getMessage()));
            }

            //========================================
            try
            {
                if (wakeLock != null && wakeLock.isHeld())
                {
                    wakeLock.release();
                }
            }
            catch (Exception e)
            {
                Log.error( String.format("[AbstractNotifyPoolService][onStartJob][Thread][PowerManager][1][Exception]: %s", e.getMessage()));
            }

            //========================================
            try
            {
                OnProcessComplete();
            }
            catch (Exception e)
            {
                Log.error( String.format("[AbstractNotifyPoolService][onStartJob][Thread][OnProcessComplete][Exception]: %s", e.getMessage()));
            }

        }
        catch (Exception e)
        {
            Log.error( String.format("[AbstractNotifyPoolService][onStartJob][Exception]: %s", e.getMessage()));
        }
    }
}
