// Copyright 1998-2018 Epic Games, Inc. All Rights Reserved.
//
// package com.epicgames.ue4;
// 
// import android.app.Notification;
// import android.app.NotificationManager;
// import android.app.PendingIntent;
// import android.content.BroadcastReceiver;
// import android.content.Context;
// import android.content.Intent;
// import android.support.v4.app.NotificationCompat;
// 
// 
//
//
//import android.support.v4.app.NotificationCompat;
//
//import android.support.v4.app.NotificationCompat.Builder;
//import android.support.v4.app.NotificationManagerCompat;
//import android.app.NotificationChannel;
//import android.app.NotificationManager;
//
//import android.R;
//import android.R.drawable;
//
//import android.app.Notification;
//import android.app.NotificationManager;
//import android.app.PendingIntent;
//import android.content.BroadcastReceiver;
//import android.content.Context;
//import android.content.Intent;
//import android.support.v4.app.NotificationCompat;
//
//public class LocalNotificationReceiver extends BroadcastReceiver
//{
//	private static boolean bChannelExists = false;
//	private static final String NOTIFICATION_CHANNEL_ID = "ue4-push-notification-channel-id";
//	private static final CharSequence NOTICATION_CHANNEL_NAME = "ue4-push-notification-channel";
//
//	public void onReceive(Context context, Intent intent)
//	{
//		Notification  notification = intent.getParcelableExtra("notification");
//		String notificationId = intent.getStringExtra("notification_id");
//
//		NotificationManager notificationManager = (NotificationManager) context.getSystemService(Context.NOTIFICATION_SERVICE);
//		notificationManager.notify(notificationId, 0, notification);
//	}
//}

// Copyright 1998-2018 Epic Games, Inc. All Rights Reserved.

package com.epicgames.ue4;

import android.app.KeyguardManager;
import android.app.Notification;
import android.app.NotificationManager;
import android.app.NotificationChannel;
import android.app.PendingIntent;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.graphics.Color;
import android.media.AudioAttributes;
import android.net.Uri;
import android.os.PowerManager;
import android.support.v4.app.NotificationCompat;
import android.media.RingtoneManager;
import android.app.Notification;

import static android.content.Context.KEYGUARD_SERVICE;
import static android.content.Context.POWER_SERVICE;


public class LocalNotificationReceiver extends BroadcastReceiver {
    private static boolean bChannelExists = false;
    private static final String NOTIFICATION_CHANNEL_ID = "LokaAW-push-notification-channel";
    private static final String NOTIFICATION_CHANNEL_NEWID = "LokaAW-4a7d7c03-c29b-4a89-9ac1-fa751c59676f";
    private static final String NOTIFICATION_CHANNEL_OLDID = "LokaAW-3a7d7c03-c29b-4a89-9ac1-fa751c59675f\n";

    private static final CharSequence NOTICATION_CHANNEL_NAME = "LOKA-AW Notify";

    public static Logger Log = new Logger("UE4-Service");


    public void onReceive(Context context, Intent intent)
    {
        PowerManager.WakeLock wakeLock = null;

        //========================================
        try
        {
            PowerManager pm = (PowerManager) context.getSystemService(POWER_SERVICE);
            wakeLock = pm.newWakeLock(PowerManager.PARTIAL_WAKE_LOCK, "LokaAW:Notify");
            wakeLock.acquire();
        }
        catch (Exception e)
        {
            Log.error( String.format("[LocalNotificationReceiver][onReceive][0][Exception]: %s", e.getMessage()));
        }

        //========================================
        try
        {
            int notificationID = intent.getIntExtra("local-notification-ID", 0);
            String title = intent.getStringExtra("local-notification-title");
            String details = intent.getStringExtra("local-notification-body");

            if (title == null || details == null) {
                // Do not schedule any local notification if any allocation failed
                return;
            }

            // Open UE4 app if clicked
            Intent notificationIntent = new Intent(context, GameActivity.class);

            // launch if closed but resume if running
            notificationIntent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP | Intent.FLAG_ACTIVITY_SINGLE_TOP);
            notificationIntent.putExtra("localNotificationID", notificationID);
            notificationIntent.putExtra("localNotificationAppLaunched", true);

            int notificationIconID = context.getResources().getIdentifier("ic_notification", "drawable", context.getPackageName());
            if (notificationIconID == 0)
            {
                notificationIconID = context.getResources().getIdentifier("icon", "drawable", context.getPackageName());
            }
            PendingIntent pendingNotificationIntent = PendingIntent.getActivity(context, notificationID, notificationIntent, 0);


            NotificationManager notificationManager = (NotificationManager) context.getSystemService(Context.NOTIFICATION_SERVICE);
            NotificationCompat.Builder builder = new NotificationCompat.Builder(context, NOTIFICATION_CHANNEL_NEWID)
                    .setSmallIcon(notificationIconID)
                    //.setCategory(Notification.CATEGORY_PROMO)
                    .setContentIntent(pendingNotificationIntent)
                    .setSound(RingtoneManager.getDefaultUri(RingtoneManager.TYPE_NOTIFICATION))
                    .setWhen(System.currentTimeMillis())
                    .setTicker(details)        // note: will not show up on Lollipop up except for accessibility
                    .setContentText(details).setAutoCancel(true)
                    .setStyle(new NotificationCompat.BigTextStyle().bigText(details))
                    .setAutoCancel(true)
                    .setLights(Color.BLUE, 2000, 2000)
                    .setContentTitle(title)
                    .setPriority(NotificationCompat.PRIORITY_MAX)
                    .setVisibility(NotificationCompat.VISIBILITY_PUBLIC);

            if (android.os.Build.VERSION.SDK_INT >= 21)
            {
                builder.setColor(0xff0e1e43);
            }

            try
            {
                if (android.os.Build.VERSION.SDK_INT >= 26)
                {
                    if (!bChannelExists)
                    {
                        if(notificationManager.getNotificationChannel(NOTIFICATION_CHANNEL_ID) != null)
                        {
                            notificationManager.deleteNotificationChannel(NOTIFICATION_CHANNEL_ID);
                        }


                        if(notificationManager.getNotificationChannel(NOTIFICATION_CHANNEL_OLDID) != null)
                        {
                            notificationManager.deleteNotificationChannel(NOTIFICATION_CHANNEL_OLDID);
                        }


                        //if(notificationManager.getNotificationChannel(NOTIFICATION_CHANNEL_NEWID) != null)
                        //{
                        //    notificationManager.deleteNotificationChannel(NOTIFICATION_CHANNEL_NEWID);
                        //}

                    }
                }
            }
            catch (Exception e)
            {
                Log.error( String.format("[LocalNotificationReceiver][onReceive][1][Exception]: %s", e.getMessage()));
            }

            try
            {
                if (android.os.Build.VERSION.SDK_INT >= 26)
                {
                    if (!bChannelExists)
                    {
                        AudioAttributes att = new AudioAttributes.Builder()
                                .setUsage(AudioAttributes.USAGE_NOTIFICATION)
                                .setContentType(AudioAttributes.CONTENT_TYPE_SPEECH)
                                .build();

                        NotificationChannel channel = new NotificationChannel(NOTIFICATION_CHANNEL_NEWID, NOTICATION_CHANNEL_NAME, NotificationManager.IMPORTANCE_HIGH);
                        channel.setDescription("Notification chanel for Cases, Game progress & discounts notify");
                        channel.setImportance(NotificationManager.IMPORTANCE_HIGH);

                        channel.enableLights(true);
                        channel.setLightColor(Color.BLUE);


                        channel.enableVibration(true);
                        channel.setVibrationPattern(new long[]{100, 200, 300, 400, 500, 400, 300, 200, 400});


                        channel.setSound(RingtoneManager.getDefaultUri(RingtoneManager.TYPE_NOTIFICATION), att);
                        channel.setLockscreenVisibility(Notification.VISIBILITY_PUBLIC);
                        notificationManager.createNotificationChannel(channel);

                        bChannelExists = true;
                    }
                }
            }
            catch (Exception e)
            {
                Log.error( String.format("[LocalNotificationReceiver][onReceive][2][Exception]: %s", e.getMessage()));
            }
            Notification notification = builder.build();

            // Stick with the defaults
            notification.flags |= Notification.FLAG_AUTO_CANCEL;
            notification.defaults |= Notification.DEFAULT_SOUND | Notification.DEFAULT_VIBRATE;

            // show the notification
            notificationManager.notify(notificationID, notification);
        }
        catch (Exception e)
        {
            Log.error( String.format("[LocalNotificationReceiver][onReceive][3][Exception]: %s", e.getMessage()));
        }

        //========================================
        try
        {
            if (wakeLock != null && wakeLock.isHeld())
            {
                wakeLock.release();
            }
        }
        catch (Exception e)
        {
            Log.error( String.format("[LocalNotificationReceiver][onReceive][4][Exception]: %s", e.getMessage()));
        }
    }
}
 
