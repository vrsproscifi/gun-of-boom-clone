// Copyright 1998-2018 Epic Games, Inc. All Rights Reserved.

#pragma once

#include "BaseHUD.h"
#include "ShooterHUD.generated.h"


struct FHitData
{
	/** Last hit time. */
	float HitTime;
	
	/** strength of hit icon */
	float HitPercentage;

	/** Initialise defaults. */
	FHitData()
	{
		HitTime = 0.0f;
		HitPercentage = 0.0f;
	}
};

struct FDeathMessage
{
	/** Name of player scoring kill. */
	FString KillerDesc;

	/** Name of killed player. */
	FString VictimDesc;

	/** Killer is local player. */
	uint8 bKillerIsOwner : 1;
	
	/** Victim is local player. */
	uint8 bVictimIsOwner : 1;

	/** Team number of the killer. */
	int32 KillerTeamNum;

	/** Team number of the victim. */
	int32 VictimTeamNum; 

	/** timestamp for removing message */
	float HideTime;

	/** What killed the player. */
	TWeakObjectPtr<class UShooterDamageType> DamageType;

	/** Initialise defaults. */
	FDeathMessage()
		: bKillerIsOwner(false)
		, bVictimIsOwner(false)
		, KillerTeamNum(0)
		, VictimTeamNum(0)		
		, HideTime(0.f)
	{
	}
};

USTRUCT()
struct FDamageEnemyData
{
	GENERATED_USTRUCT_BODY();

	UPROPERTY()	FVector WorldLocation;
	UPROPERTY()	float Time;
	UPROPERTY()	float Damage;	
	UPROPERTY()	bool bIsKilled;
	UPROPERTY()	bool bIsCritical;

	FDamageEnemyData()
		: WorldLocation()
		, Time(.0f)
		, Damage(.0f)
		, bIsKilled(false)
		, bIsCritical(false)
	{ }

	bool operator==(const FDamageEnemyData& InB) const
	{
		return 
			this->WorldLocation == InB.WorldLocation && 
			this->Time == InB.Time && 
			this->Damage == InB.Damage && 
			this->bIsKilled == InB.bIsKilled && 
			this->bIsCritical == InB.bIsCritical;
	}
};


UCLASS()
class AShooterHUD : public ABaseHUD
{
	GENERATED_BODY()

public:

	AShooterHUD();

	virtual void EndPlay(const EEndPlayReason::Type EndPlayReason) override;

	/** Main HUD update loop. */
	virtual void DrawHUD() override;

	/**
	 * Sent from pawn hit, used to calculate hit notification overlay for drawing.
	 *
	 * @param	DamageTaken		The amount of damage.
	 * @param	DamageEvent		The actual damage event.
	 * @param	PawnInstigator	The pawn that did the damage.
	 */
	void NotifyWeaponHit(float DamageTaken, struct FDamageEvent const& DamageEvent, class APawn* PawnInstigator);

	/** Sent from ShooterWeapon, shows NO AMMO text. */
	void NotifyOutOfAmmo();

	/** Notifies we have hit the enemy. */
	void NotifyEnemyHit(const float& InDamage, const FVector& InWorldLocation, bool IsCritical = false, bool InIsKilled = false);
		
protected:
	/** Floor for automatic hud scaling. */
	static const float MinHudScale;

	/** Lighter HUD color. */
	FColor HUDLight;

	/** Darker HUD color. */
	FColor HUDDark;

	/** When we got last notice about out of ammo. */
	float NoAmmoNotifyTime;

	/** How long notice is fading out. */
	float NoAmmoFadeOutTime;

	/** Most recent hit time, used to check if we need to draw hit indicator at all. */
	float LastHitTime;

	/** How long till hit notify fades out completely. */
	float HitNotifyDisplayTime;

	/** When we last time hit the enemy. */
	float LastEnemyHitTime;

	/** How long till enemy hit notify fades out completely. */
	float LastEnemyHitDisplayTime;

	/** Icons for hit indicator. */
	UPROPERTY()
	FCanvasIcon HitNotifyIcon[8];

	/** UI scaling factor for other resolutions than Full HD. */
	float ScaleUI;

	/** Current animation pulse value. */
	float PulseValue;

	/** FontRenderInfo enabling casting shadow.s */
	FFontRenderInfo ShadowedFont;

	/** Big "KILLED [PLAYER]" message text above the crosshair. */
	FText CenteredKillMessage;

	/** last time we killed someone. */
	float LastKillTime;

	/** How long the message will fade out. */
	float KillFadeOutTime;

	/** Offsets to display hit indicator parts. */
	FVector2D Offsets[8];

	/** Texture for hit indicator. */
	UPROPERTY()
	UTexture2D* HitNotifyTexture;

	/** texture for HUD elements. */
	UPROPERTY()
	UTexture2D* HUDMainTexture;

	/** Texture for HUD elements. */
	UPROPERTY()
	UTexture2D* HUDAssets02Texture;

	/** Overlay shown when health is low. */
	UPROPERTY()
	UTexture2D* LowHealthOverlayTexture;


	/** General offset for HUD elements. */
	float Offset;

	/** Runtime data for hit indicator. */
	FHitData HitNotifyData[8];

	UPROPERTY()
	TArray<FDamageEnemyData> DamageNumbersArray;

	/** Array of information strings to render (Waiting to respawn etc) */
	TArray<FCanvasTextItem> InfoItems;

	/** Called every time game is started. */
	virtual void PostInitializeComponents() override;

	/** 
	 * Converts floating point seconds to MM:SS string.
	 *
	 * @param TimeSeconds		The time to get a string for.
	 */
	FString GetTimeString(float TimeSeconds);

	/** Draws weapon crosshair. */
	void DrawCrosshair();
	
	/** Draws hit indicator. */
	void DrawHitIndicator();

	void DrawDamageNumbers();

	/** Delegate for telling other methods when players have started/stopped talking */
//	FOnPlayerTalkingStateChangedDelegate OnPlayerTalkingStateChangedDelegate;
	void OnPlayerTalkingStateChanged(TSharedRef<const FUniqueNetId> TalkingPlayerId, bool bIsTalking);


	/** helper for getting uv coords in normalized top,left, bottom, right format */
	void MakeUV(FCanvasIcon& Icon, FVector2D& UV0, FVector2D& UV1, uint16 U, uint16 V, uint16 UL, uint16 VL);

	/*
	 * Add information string that will be displayed on the hud. They are added as required and rendered together to prevent overlaps 
	 * 
	 * @param InInfoString	InInfoString
	*/
	void AddMatchInfoString(const FCanvasTextItem InfoItem);

	/*
	* Render the info messages.
	*
	* @param YOffset	YOffset from top of canvas to start drawing the messages
	* @param ScaleUI	UI Scale factor
	* @param TextScale	Text scale factor
	*
	* @returns The next Y position to draw any further strings
	*/
	float ShowInfoItems(float YOffset, float TextScale);


	UPROPERTY(EditDefaultsOnly, Category = NotifyHit)
	float DisplayFloatingDamageDuration;
	
	UPROPERTY(EditDefaultsOnly, Category = NotifyHit)
	UMaterialInterface* HitNotifyShader;

	UPROPERTY(EditDefaultsOnly, Category = NotifyHit)
	FSlateFontInfo DamageNumbersFont;

	UPROPERTY(EditDefaultsOnly, Category = NotifyHit)
	UMaterialInterface* OutRangeWeaponShader;

	UPROPERTY()
	UMaterialInstanceDynamic* HitNotifyShaderDynamic;
};