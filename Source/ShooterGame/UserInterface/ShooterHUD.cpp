// Copyright 1998-2018 Epic Games, Inc. All Rights Reserved.

#include "ShooterGame.h"
#include "ShooterHUD.h"
#include "Inventory/ShooterWeapon.h"
#include "Misc/NetworkVersion.h"
#include "Player/ShooterPlayerController.h"
#include "Player/ShooterCharacter.h"
#include "WeaponItemProperty.h"
#include "GameState/ShooterGameState.h"
#include "GameInstance/ShooterGameUserSettings.h"

#define LOCTEXT_NAMESPACE "ShooterGame.HUD.Menu"

const float AShooterHUD::MinHudScale = 0.5f;

namespace EShooterHudPosition
{
	enum Type
	{
		Left = 0,
		FrontLeft = 1,
		Front = 2,
		FrontRight = 3,
		Right = 4,
		BackRight = 5,
		Back = 6,
		BackLeft = 7,
	};
}

namespace EShooterCrosshairDirection
{
	enum Type
	{
		Left = 0,
		Right = 1,
		Top = 2,
		Bottom = 3,
		Center = 4
	};
}


AShooterHUD::AShooterHUD() : Super()
{
	NoAmmoFadeOutTime =  1.0f;
	HitNotifyDisplayTime = 0.75f;
	KillFadeOutTime = 2.0f;
	LastEnemyHitDisplayTime = 0.2f;
	NoAmmoNotifyTime = -NoAmmoFadeOutTime;
	LastKillTime = - KillFadeOutTime;
	LastEnemyHitTime = -LastEnemyHitDisplayTime;
	DisplayFloatingDamageDuration = 2.0f;

	if (!HasAnyFlags(RF_ArchetypeObject | RF_DefaultSubObject))
	{
		//OnPlayerTalkingStateChangedDelegate = FOnPlayerTalkingStateChangedDelegate::CreateUObject(this, &AShooterHUD::OnPlayerTalkingStateChanged);

		static ConstructorHelpers::FObjectFinder<UTexture2D> HitTextureOb(TEXT("/Game/UserInterface/HUD/HitIndicator"));
		static ConstructorHelpers::FObjectFinder<UTexture2D> LowHealthOverlayTextureOb(TEXT("/Game/UserInterface/HUD/LowHealthOverlay"));

		HitNotifyTexture = HitTextureOb.Object;
		LowHealthOverlayTexture = LowHealthOverlayTextureOb.Object;

		HitNotifyIcon[EShooterHudPosition::Left] = UCanvas::MakeIcon(HitNotifyTexture, 158, 831, 585, 392);
		HitNotifyIcon[EShooterHudPosition::FrontLeft] = UCanvas::MakeIcon(HitNotifyTexture, 369, 434, 460, 378);
		HitNotifyIcon[EShooterHudPosition::Front] = UCanvas::MakeIcon(HitNotifyTexture, 848, 284, 361, 395);
		HitNotifyIcon[EShooterHudPosition::FrontRight] = UCanvas::MakeIcon(HitNotifyTexture, 1212, 397, 427, 394);
		HitNotifyIcon[EShooterHudPosition::Right] = UCanvas::MakeIcon(HitNotifyTexture, 1350, 844, 547, 321);
		HitNotifyIcon[EShooterHudPosition::BackRight] = UCanvas::MakeIcon(HitNotifyTexture, 1232, 1241, 458, 341);
		HitNotifyIcon[EShooterHudPosition::Back] = UCanvas::MakeIcon(HitNotifyTexture, 862, 1384, 353, 408);
		HitNotifyIcon[EShooterHudPosition::BackLeft] = UCanvas::MakeIcon(HitNotifyTexture, 454, 1251, 371, 410);

		Offsets[EShooterHudPosition::Left] = FVector2D(173, 0);
		Offsets[EShooterHudPosition::FrontLeft] = FVector2D(120, 125);
		Offsets[EShooterHudPosition::Front] = FVector2D(0, 173);
		Offsets[EShooterHudPosition::FrontRight] = FVector2D(-120, 125);
		Offsets[EShooterHudPosition::Right] = FVector2D(-173, 0);
		Offsets[EShooterHudPosition::BackRight] = FVector2D(-120, -125);
		Offsets[EShooterHudPosition::Back] = FVector2D(0, -173);
		Offsets[EShooterHudPosition::BackLeft] = FVector2D(120, -125);


	}

	Offset = 20.0f;
	HUDLight = FColor(175,202,213,255);
	HUDDark = FColor(110,124,131,255);
	ShadowedFont.bEnableShadow = true;
}

void AShooterHUD::EndPlay(const EEndPlayReason::Type EndPlayReason)
{
	AShooterPlayerController* ShooterPC = GetValidObjectAs<AShooterPlayerController>(PlayerOwner);
	if (ShooterPC != NULL )
	{
		// Reset the ignore input flags, so we can control the camera during warmup
		ShooterPC->SetCinematicMode(false,false,false,true,true);
	}

	Super::EndPlay(EndPlayReason);
}

FString AShooterHUD::GetTimeString(float TimeSeconds)
{
	// only minutes and seconds are relevant
	const int32 TotalSeconds = FMath::Max(0, FMath::TruncToInt(TimeSeconds) % 3600);
	const int32 NumMinutes = TotalSeconds / 60;
	const int32 NumSeconds = TotalSeconds % 60;

	const FString TimeDesc = FString::Printf(TEXT("%02d:%02d"), NumMinutes, NumSeconds);
	return TimeDesc;
}

void AShooterHUD::NotifyOutOfAmmo()
{
	NoAmmoNotifyTime = GetWorld()->GetTimeSeconds();
}

void AShooterHUD::NotifyEnemyHit(const float& InDamage, const FVector& InWorldLocation, bool IsCritical, bool InIsKilled)
{
	LastEnemyHitTime = GetWorld()->GetTimeSeconds();

	bool bFoundExist = false;
	for (auto& DamageData : DamageNumbersArray)
	{		
		if (FMath::IsNearlyEqual(DamageData.Time, LastEnemyHitTime, .2f))
		{
			DamageData.Damage += InDamage;
			DamageData.Time = LastEnemyHitTime;
			DamageData.WorldLocation = InWorldLocation;
			bFoundExist = true;
		}
	}

	if (!bFoundExist)
	{
		FDamageEnemyData DamageEnemyData;
		DamageEnemyData.Damage = InDamage == 0 ? 69 : InDamage;
		DamageEnemyData.WorldLocation = InWorldLocation;
		DamageEnemyData.bIsKilled = InIsKilled;
		DamageEnemyData.bIsCritical = IsCritical;
		DamageEnemyData.Time = LastEnemyHitTime;

		DamageNumbersArray.AddUnique(DamageEnemyData);
	}
}

void AShooterHUD::DrawHUD()
{
	Super::DrawHUD();
	if (Canvas == nullptr)
	{
		return;
	}
	ScaleUI = Canvas->ClipY / 1080.0f;

	// Empty the info item array
	InfoItems.Empty();
	float TextScale = 1.0f;
	// enforce min
	ScaleUI = FMath::Max(ScaleUI, MinHudScale);
	
	AShooterCharacter* MyPawn = GetValidObjectAs<AShooterCharacter>(GetOwningPawn());
	if (MyPawn && MyPawn->IsAlive() && MyPawn->Health.X < MyPawn->Health.Y * MyPawn->GetLowHealthPercentage())
	{
		const float AnimSpeedModifier = 1.0f + 5.0f * (1.0f - MyPawn->Health.X / (MyPawn->Health.Y * MyPawn->GetLowHealthPercentage()));
		int32 EffectValue = 32 + 72 * (1.0f - MyPawn->Health.X / (MyPawn->Health.Y * MyPawn->GetLowHealthPercentage()));
		PulseValue += GetWorld()->GetDeltaSeconds() * AnimSpeedModifier;
		float EffectAlpha = FMath::Abs(FMath::Sin(PulseValue));

		float AlphaValue = ( 1.0f / 255.0f ) * ( EffectAlpha * EffectValue );

		// Full screen low health overlay
		Canvas->PopSafeZoneTransform();
		FCanvasTileItem TileItem( FVector2D( 0, 0 ), LowHealthOverlayTexture->Resource, FVector2D( Canvas->ClipX, Canvas->ClipY ), FLinearColor( 1.0f, 0.0f, 0.0f, AlphaValue ) );
		TileItem.BlendMode = SE_BLEND_Translucent;
		Canvas->DrawItem( TileItem );
		Canvas->ApplySafeZoneTransform();
	}

	float MessageOffset = (Canvas->ClipY / 4.0)* ScaleUI;

	DrawCrosshair();
	DrawHitIndicator();
	DrawDamageNumbers();

	// Render the info messages such as wating to respawn - these will be drawn below any 'killed player' message.
	ShowInfoItems(MessageOffset, 1.0f);
	
}

void AShooterHUD::DrawCrosshair()
{
	AShooterPlayerController* PCOwner = GetValidObjectAs<AShooterPlayerController>(PlayerOwner);
	if (PCOwner)
	{
		AShooterCharacter* Pawn =  GetValidObjectAs<AShooterCharacter>(PCOwner->GetPawn());
		if (Pawn && Pawn->GetWeapon())
		{
			if (auto Config = Pawn->GetWeapon()->GetWeaponProperty())
			{
				if (Pawn && Pawn->GetWeapon() && !Pawn->IsRunning()
					&& (Pawn->IsTargeting() || (!Pawn->IsTargeting() && !Config->bHideCrosshairWhileNotAiming)))
				{
					const float SpreadMulti = 300;
					AShooterWeapon* MyWeapon = Pawn->GetWeapon();
					const float CurrentTime = GetWorld()->GetTimeSeconds();

					float AnimOffset = 0;
					if (MyWeapon)
					{
						const float EquipStartedTime = MyWeapon->GetEquipStartedTime();
						const float EquipDuration = MyWeapon->GetEquipDuration();
						AnimOffset = 300 * (1.0f - FMath::Min(1.0f, (CurrentTime - EquipStartedTime) / EquipDuration));
					}
					float CrossSpread = 2 + AnimOffset;
					if (MyWeapon)
					{
						CrossSpread += SpreadMulti * FMath::Tan(FMath::DegreesToRadians(MyWeapon->GetCurrentSpread()));
					}
					float CenterX = Canvas->ClipX / 2;
					float CenterY = Canvas->ClipY / 2;
					Canvas->SetDrawColor(255, 255, 255, 192);

					const FCanvasIcon* CurrentCrosshair[5];
					for (int32 i = 0; i < 5; i++)
					{
						if (Config->UseCustomAimingCrosshair && Pawn->IsTargeting())
						{
							CurrentCrosshair[i] = &Config->AimingCrosshair[i];
						}
						else if (Config->UseCustomCrosshair)
						{
							CurrentCrosshair[i] = &Config->Crosshair[i];
						}
						//else
						//{
						//	CurrentCrosshair[i] = &Crosshair[i];
						//}
					}

					if (MyWeapon && MyWeapon->IsValidLowLevel())
					{
						FVector ShootDir = MyWeapon->GetAdjustedAim();
						//FVector Origin = MyWeapon->GetMuzzleLocation();

						// trace from camera to check what's under crosshair
						const float ProjectileAdjustRange = 10000.0f;
						const FVector StartTrace = MyWeapon->GetCameraDamageStartLocation(ShootDir);
						const FVector EndTrace = StartTrace + (ShootDir * ProjectileAdjustRange);
						FHitResult TraceResult = MyWeapon->WeaponTrace(StartTrace, EndTrace);

						if (Pawn->IsTargeting() && Config->UseLaserDot)
						{
							Canvas->SetDrawColor(134, 10, 6, 192);
							Canvas->DrawIcon(*CurrentCrosshair[EShooterCrosshairDirection::Center],
								CenterX - (*CurrentCrosshair[EShooterCrosshairDirection::Center]).UL*ScaleUI / 2.0f,
								CenterY - (*CurrentCrosshair[EShooterCrosshairDirection::Center]).VL*ScaleUI / 2.0f, ScaleUI);
						}
						else
						{
							if (auto MyGameState = GetWorld()->GetGameState<AShooterGameState>())
							{
								if (auto ActorResult = TraceResult.GetActor())
								{
									if (auto OtherPawn = GetValidObjectAs<AShooterCharacter>(ActorResult))
									{
										const bool IsFriendlyTeam = MyGameState->HasSameTeam(Pawn, OtherPawn);
										if (IsFriendlyTeam)
										{
											Canvas->SetDrawColor(46, 204, 16, 192);
										}
										else
										{
											Canvas->SetDrawColor(134, 10, 6, 192);
										}
									}
								}
							}

							Canvas->DrawIcon(*CurrentCrosshair[EShooterCrosshairDirection::Center],
								CenterX - (*CurrentCrosshair[EShooterCrosshairDirection::Center]).UL*ScaleUI / 2.0f,
								CenterY - (*CurrentCrosshair[EShooterCrosshairDirection::Center]).VL*ScaleUI / 2.0f, ScaleUI);

							Canvas->DrawIcon(*CurrentCrosshair[EShooterCrosshairDirection::Left],
								CenterX - 1 - (*CurrentCrosshair[EShooterCrosshairDirection::Left]).UL * ScaleUI - CrossSpread * ScaleUI,
								CenterY - (*CurrentCrosshair[EShooterCrosshairDirection::Left]).VL*ScaleUI / 2.0f, ScaleUI);
							Canvas->DrawIcon(*CurrentCrosshair[EShooterCrosshairDirection::Right],
								CenterX + CrossSpread * ScaleUI,
								CenterY - (*CurrentCrosshair[EShooterCrosshairDirection::Right]).VL * ScaleUI / 2.0f, ScaleUI);

							Canvas->DrawIcon(*CurrentCrosshair[EShooterCrosshairDirection::Top],
								CenterX - (*CurrentCrosshair[EShooterCrosshairDirection::Top]).UL * ScaleUI / 2.0f,
								CenterY - 1 - (*CurrentCrosshair[EShooterCrosshairDirection::Top]).VL * ScaleUI - CrossSpread * ScaleUI, ScaleUI);
							Canvas->DrawIcon(*CurrentCrosshair[EShooterCrosshairDirection::Bottom],
								CenterX - (*CurrentCrosshair[EShooterCrosshairDirection::Bottom]).UL * ScaleUI / 2.0f,
								CenterY + CrossSpread * ScaleUI, ScaleUI);
						}

						if (auto MyWeaponProperty = MyWeapon->GetWeaponConfiguration())
						{
							if (GetValidObjectAs<AShooterCharacter>(TraceResult.GetActor()))
							{
								const float WeaponRange = MyWeaponProperty->Range * 100.0f;
								if (TraceResult.Distance > WeaponRange)
								{
									const float OverDistance = (TraceResult.Distance - WeaponRange) / 100.0f;								

									if (OutRangeWeaponShader && OutRangeWeaponShader->IsValidLowLevel())
									{
										FCanvasTileItem TileItem(FVector2D(CenterX - 32 * ScaleUI, CenterY - 32 * ScaleUI), OutRangeWeaponShader->GetRenderProxy(), FVector2D(64 * ScaleUI, 64 * ScaleUI));
										Canvas->DrawItem(TileItem);
									}

									FVector2D DrawPos = { CenterX + 40 * ScaleUI, CenterY - 25 * ScaleUI };
									FCanvasTextItem DamageText(DrawPos, FText::FromString(FString::Printf(TEXT("%.1f"), OverDistance)), DamageNumbersFont, FColor::Red);
									DamageText.Scale = FVector2D(1.0f * ScaleUI, 1.0f * ScaleUI);
									DamageText.bOutlined = true;
									DamageText.bCentreX = false;
									DamageText.bCentreY = true;
									DamageText.OutlineColor = FColor::Black.WithAlpha(128);

									Canvas->DrawItem(DamageText);

								}
							}
						}
					}

					if (DamageNumbersArray.Num())
					{
						float LastHitMagnitude = FMath::Clamp<float>(float(DamageNumbersArray.Last().Damage) / 100, 0.0f, 1.0f);
						float Duration = FMath::Clamp<float>(2.0f * LastHitMagnitude, 0.2f, 1.f);
						float Size = FMath::Clamp<float>(2.0f * LastHitMagnitude, 1.0f, 2.0f);
						float FlashTime = CurrentTime - LastEnemyHitTime;
						if (FlashTime < Duration)
						{
							if (!HitNotifyShaderDynamic && HitNotifyShader)
							{
								HitNotifyShaderDynamic = UMaterialInstanceDynamic::Create(HitNotifyShader, this);
							}

							if (HitNotifyShaderDynamic)
							{
								HitNotifyShaderDynamic->SetScalarParameterValue(TEXT("Scale"), FlashTime / Duration);
								float DrawSize = 400.0f * ScaleUI * Size;
								FVector2D DrawPos = { CenterX - (DrawSize / 2), CenterY - (DrawSize / 2) };

								FCanvasTileItem TileItem(DrawPos, HitNotifyShaderDynamic->GetRenderProxy(), FVector2D(DrawSize, DrawSize));
								Canvas->DrawItem(TileItem);
							}
						}
					}
				}
			}
		}
	}
}

void AShooterHUD::DrawDamageNumbers()
{
	const float CurrentTime = GetWorld()->GetTimeSeconds();

	for (SIZE_T i = 0; i < DamageNumbersArray.Num(); ++i)
	{
		const auto &DamageNumber = DamageNumbersArray[i];
		const float DisplayProgress = (CurrentTime - DamageNumber.Time) / DisplayFloatingDamageDuration;
		const float DisplayAlpha = FMath::Min(1.0f, 1.0f - DisplayProgress);

		if (UShooterGameUserSettings::Get() && UShooterGameUserSettings::Get()->IsEnabledDamageNumbers())
		{
			FVector2D ScreenPosition;
			if (DisplayAlpha >= 0.0f && CorrectProject(DamageNumber.WorldLocation, ScreenPosition))
			{				
				const auto TargetColor = DamageNumber.bIsCritical ? FColorList::Red.WithAlpha(255 * DisplayAlpha).ReinterpretAsLinear() : FColorList::White.WithAlpha(255 * DisplayAlpha).ReinterpretAsLinear();
				
				FCanvasTextItem DamageText(ScreenPosition + FVector2D(.0f, -300.0f + DisplayAlpha * 300.0f), FText::AsNumber(DamageNumber.Damage, &FNumberFormattingOptions::DefaultNoGrouping()), DamageNumbersFont, TargetColor);
				DamageText.Scale *= FMath::InterpEaseOut(0.f, DamageNumber.bIsKilled ? 2.0f : 1.0f, FMath::Clamp(DisplayProgress * 5, .0f, 1.0f), 2.);
				DamageText.bOutlined = true;
				DamageText.OutlineColor = FColor::Black.WithAlpha(128 * DisplayAlpha);
				DamageText.Depth = i;

				Canvas->DrawItem(DamageText);
			}
		}

		if (DisplayAlpha <= 0.0f)
		{
			DamageNumbersArray.RemoveAt(i, 1);
			--i;
		}
	}
}

void AShooterHUD::NotifyWeaponHit(float DamageTaken, struct FDamageEvent const& DamageEvent, class APawn* PawnInstigator)
{
	const float CurrentTime = GetWorld()->GetTimeSeconds();
	AShooterCharacter* MyPawn = (PlayerOwner) ? GetValidObjectAs<AShooterCharacter>(PlayerOwner->GetPawn()) : NULL;
	if (MyPawn)
	{
		if (CurrentTime - LastHitTime > HitNotifyDisplayTime) 
		{
			for (uint8 i = 0; i < 8; i++)
			{
				HitNotifyData[i].HitPercentage = 0;
			}
		}

		FVector ImpulseDir;    
		FHitResult Hit; 
		DamageEvent.GetBestHitInfo(this, PawnInstigator, Hit, ImpulseDir);

		//check hit vector against pre-defined direction vectors - left, front, right, back
		const FVector HitVector = FRotationMatrix(PlayerOwner->GetControlRotation()).InverseTransformVector(-ImpulseDir);

		FVector Dirs2[8] = { 
			FVector(0,-1,0) /*left*/, 
			FVector(1,-1,0) /*front left*/, 
			FVector(1,0,0) /*front*/, 
			FVector(1,1,0) /*front right*/, 
			FVector(0,1,0) /*right*/, 
			FVector(-1,1,0) /*back right*/, 
			FVector(-1,0,0), /*back*/
			FVector(-1,-1,0) /*back left*/ 
		};
		int32 DirIndex = -1;
		float HighestModifier = 0;

		for (uint8 i = 0; i < 8; i++)
		{
			//Normalize our direction vectors
			Dirs2[i].Normalize();
			const float DirModifier = FMath::Max(0.0f, FVector::DotProduct(Dirs2[i], HitVector));
			if (DirModifier > HighestModifier)
			{
				DirIndex = i;
				HighestModifier = DirModifier;
			}
		}
		if (DirIndex > -1)
		{
			const float DamageTakenPercentage = DamageTaken / (MyPawn->GetCurrentArmour() + MyPawn->Health.X);

			HitNotifyData[DirIndex].HitPercentage += DamageTakenPercentage * 5;
			HitNotifyData[DirIndex].HitPercentage = FMath::Clamp(HitNotifyData[DirIndex].HitPercentage, 0.0f, 1.0f);
			HitNotifyData[DirIndex].HitTime = CurrentTime;
		}

	}
	
	LastHitTime = CurrentTime;
}


void AShooterHUD::DrawHitIndicator()
{
	const float CurrentTime = GetWorld()->GetTimeSeconds();
	if (CurrentTime - LastHitTime <= HitNotifyDisplayTime)
	{
		const float StartX = Canvas->ClipX / 2.0f;
		const float StartY = Canvas->ClipY / 2.0f;

		for (uint8 i = 0; i < 8; i++)
		{
			const float TimeModifier = FMath::Max(0.0f, 1 - (CurrentTime - HitNotifyData[i].HitTime) / HitNotifyDisplayTime);
			const float Alpha = TimeModifier * HitNotifyData[i].HitPercentage;
			Canvas->SetDrawColor(255, 255, 255, FMath::Clamp(FMath::TruncToInt(Alpha * 255 * 1.5f), 0, 255));
			Canvas->DrawIcon(HitNotifyIcon[i], 
				StartX + (HitNotifyIcon[i].U - HitNotifyTexture->GetSizeX() / 2 + Offsets[i].X) * ScaleUI,
				StartY + (HitNotifyIcon[i].V - HitNotifyTexture->GetSizeY() / 2 + Offsets[i].Y) * ScaleUI,
				ScaleUI);
		}
	}
}

void AShooterHUD::OnPlayerTalkingStateChanged(TSharedRef<const FUniqueNetId> TalkingPlayerId, bool bIsTalking)
{

}

void AShooterHUD::PostInitializeComponents()
{
	Super::PostInitializeComponents();

	//IOnlineSubsystem* const OnlineSub = IOnlineSubsystem::Get();
	//if (OnlineSub)
	//{
	//	IOnlineVoicePtr Voice = OnlineSub->GetVoiceInterface();
	//	if (Voice.IsValid())
	//	{
	//		Voice->AddOnPlayerTalkingStateChangedDelegate_Handle(OnPlayerTalkingStateChangedDelegate);
	//	}
	//}
}

void AShooterHUD::MakeUV(FCanvasIcon& Icon, FVector2D& UV0, FVector2D& UV1, uint16 U, uint16 V, uint16 UL, uint16 VL)
{
	if (Icon.Texture)
	{
		const float Width = Icon.Texture->GetSurfaceWidth();
		const float Height = Icon.Texture->GetSurfaceHeight();
		UV0 = FVector2D(U / Width, V / Height);
		UV1 = UV0 + FVector2D(UL / Width, VL / Height);
	}
}

void AShooterHUD::AddMatchInfoString(const FCanvasTextItem InInfoItem )
{
	InfoItems.Add(InInfoItem);
}

float AShooterHUD::ShowInfoItems(float YOffset, float TextScale)
{
	float Y = YOffset;
	float CanvasCentre = Canvas->ClipX / 2.0f;

	for (int32 iItem = 0; iItem < InfoItems.Num() ; iItem++)
	{
		float X = 0.0f;
		float SizeX, SizeY; 
		Canvas->StrLen(InfoItems[iItem].Font, InfoItems[iItem].Text.ToString(), SizeX, SizeY);
		X = CanvasCentre - ( SizeX * InfoItems[iItem].Scale.X)/2.0f;
		Canvas->DrawItem(InfoItems[iItem], X, Y);
		Y += SizeY * InfoItems[iItem].Scale.Y;
	}
	return Y;
}


#undef LOCTEXT_NAMESPACE
