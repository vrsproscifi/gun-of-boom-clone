// Fill out your copyright notice in the Description page of Project Settings.

#include "ShooterGame.h"
#include "BaseHUD.h"
#include "GameSingletonExtensions.h"
#include "HUDFontsData.h"
#include "GameInstance/ShooterGameUserSettings.h"

ABaseHUD::ABaseHUD()
{

}

void ABaseHUD::DrawHUD()
{
	Super::DrawHUD();

	if (!FontsData)
	{
		FontsData = FGameSingletonExtension::GetDataAssets<UHUDFontsData>()[0];
	}

#if (PLATFORM_ANDROID || WITH_EDITOR)

	static FString ProjectVersion;

	if (ProjectVersion.IsEmpty())
	{
		GConfig->GetString(
			TEXT("/Script/AndroidRuntimeSettings.AndroidRuntimeSettings"),
			TEXT("VersionDisplayName"),
			ProjectVersion,
			GEngineIni
		);
	}

	FCanvasTextItem VersionItem(FVector2D(Canvas->SizeX - 100.0f - 20.0f, Canvas->SizeY - 10.0f - 2.0f), FText::FromString(ProjectVersion), FontsData->FontVersion, FColor::White.WithAlpha(128));
	VersionItem.bCentreY = true;
	Canvas->DrawItem(VersionItem);

#endif

	DrawDebugInformation();
}

void ABaseHUD::DrawDebugInformation()
{
	if (UShooterGameUserSettings::Get() && UShooterGameUserSettings::Get()->IsEnabledDebugInfo())
	{
		auto MyCtrl = GetValidObjectAs<APlayerController>(GetOwner());
		auto MyPS = MyCtrl ? MyCtrl->PlayerState : nullptr;

		if (MyPS)
		{
			FIntPoint Packets(0, 0);
			if (auto MyConn = MyPS->GetNetConnection())
			{
				Packets.X = MyConn->InPacketsLost;
				Packets.Y = MyConn->OutPacketsLost;
			}

			const float FPSValue = 1.0f / FSlateApplication::Get().GetAverageDeltaTime();
			FCanvasTextItem TextItem_FPS(FVector2D(40, (Canvas->SizeY / 2) - 200), FText::FromString(FString::Printf(TEXT("FPS: %.2f"), FPSValue)), FontsData->FontSmall, FColor::White);
			TextItem_FPS.SetColor(FPSValue >= 28.0f ? FColorList::LimeGreen : (FPSValue > 20.0f ? FColorList::Orange : FColorList::Red));
			Canvas->DrawItem(TextItem_FPS);

			const float PingValue = MyPS->ExactPing;
			FCanvasTextItem TextItem_Ping(FVector2D(40, (Canvas->SizeY / 2) - 180), FText::FromString(FString::Printf(TEXT("Ping: %.2f ms"), PingValue)), FontsData->FontSmall, FColor::White);
			TextItem_Ping.SetColor(PingValue < 100.0f ? FColorList::LimeGreen : (PingValue < 200.0f ? FColorList::Orange : FColorList::Red));
			Canvas->DrawItem(TextItem_Ping);

			FCanvasTextItem TextItem_Packets(FVector2D(40, (Canvas->SizeY / 2) - 160), FText::FromString(FString::Printf(TEXT("Packets Loss (In/Out): %d/%d"), Packets.X, Packets.Y)), FontsData->FontSmall, FColor::White);
			Canvas->DrawItem(TextItem_Packets);
		}
	}
}

bool ABaseHUD::CorrectProject(const FVector &Location, FVector2D &OutScreen)
{
	if (Canvas && Canvas->SceneView)
	{
		FPlane ScreenPosition3D = Canvas->SceneView->Project(Location);

		ScreenPosition3D.X = (Canvas->ClipX / 2.f) + (ScreenPosition3D.X*(Canvas->ClipX / 2.f));
		ScreenPosition3D.Y *= -1.f * GProjectionSignY;
		ScreenPosition3D.Y = (Canvas->ClipY / 2.f) + (ScreenPosition3D.Y*(Canvas->ClipY / 2.f));

		OutScreen.X = ScreenPosition3D.X;
		OutScreen.Y = ScreenPosition3D.Y;

		return ScreenPosition3D.W > .0f;
	}

	return false;
}
