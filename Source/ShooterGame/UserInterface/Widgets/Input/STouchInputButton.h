// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "STouchInput.h"
#include "UserInterface/Widgets/Styles/TouchInputButtonWidgetStyle.h"

namespace EButtonActivationMethod
{
	enum Type
	{
		Click,
		Toggle,
		End
	};
}

class SHOOTERGAME_API STouchInputButton : public STouchInput
{
public:
	SLATE_BEGIN_ARGS(STouchInputButton)
		: _Style(&FShooterStyle::Get().GetWidgetStyle<FTouchInputButtonStyle>("STouchInputButtonStyle"))
		, _ActivationMethod(EButtonActivationMethod::Click)
		, _Action(NAME_None)
		, _Amount(INDEX_NONE)
	{}
	SLATE_STYLE_ARGUMENT(FTouchInputButtonStyle, Style)
	SLATE_ATTRIBUTE(EButtonActivationMethod::Type, ActivationMethod)
	SLATE_ATTRIBUTE(FName, Action)
	SLATE_ATTRIBUTE(bool, IsActivated)
	SLATE_ATTRIBUTE(int32, Amount)
	SLATE_ATTRIBUTE(float, Progress)
	SLATE_EVENT(FOnClickedOutside, OnRawClicked)
	SLATE_END_ARGS()

	void Construct(const FArguments& InArgs);
	void ForceClearTouch();

protected:

	const FTouchInputButtonStyle* Style;

	FOnClickedOutside OnRawClicked;

	TAttribute<EButtonActivationMethod::Type> Method;
	TAttribute<FName> Action;
	TAttribute<bool> IsActivated;
	TAttribute<int32> Amount;
	TAttribute<float> Progress;

	TSharedPtr<class SSphereProgress> Widget_SphereProgress;

	int32 TouchIndex;

	virtual FReply OnTouchStarted(const FGeometry& MyGeometry, const FPointerEvent& InTouchEvent) override;
	virtual FReply OnTouchEnded(const FGeometry& MyGeometry, const FPointerEvent& InTouchEvent) override;
	virtual void OnMouseLeave(const FPointerEvent& MouseEvent) override;
	virtual void OnMouseCaptureLost(const FCaptureLostEvent& CaptureLostEvent) override;

	int32 OnPaint(const FPaintArgs& Args, const FGeometry& AllottedGeometry, const FSlateRect& MyCullingRect, FSlateWindowElementList& OutDrawElements, int32 LayerId, const FWidgetStyle& InWidgetStyle, bool bParentEnabled) const override;
};
