// VRSPRO

#pragma once


#include "Widgets/SCompoundWidget.h"
#include "Styles/SliderSpinWidgetStyle.h"

DECLARE_DELEGATE_OneParam(FOnListSliderValue, const int32&)

class SSliderSpin : public SCompoundWidget
{
public:
	SLATE_BEGIN_ARGS(SSliderSpin)
		: _ActiveValue(0)
		, _TextLeft(FText::FromString("<<"))
		, _TextRight(FText::FromString(">>"))
		, _Style(&FShooterStyle::Get().GetWidgetStyle<FSliderSpinStyle>("SSliderSpinStyle"))
	{}
	SLATE_ARGUMENT(TArray<FText>, Values)
	SLATE_ARGUMENT(int32, ActiveValue)
	SLATE_ARGUMENT(FText, TextLeft)
	SLATE_ARGUMENT(FText, TextRight)
	SLATE_STYLE_ARGUMENT(FSliderSpinStyle, Style)
	SLATE_EVENT(FOnListSliderValue, OnListValue)
	SLATE_EVENT(FOnListSliderValue, OnChangeValue)
	SLATE_END_ARGS()

	/** Constructs this widget with InArgs */
	void Construct(const FArguments& InArgs);

	int32 AddValue(const FText&);
	void SetValues(TArray<FText>);
	bool RemoveValue(const int32&);

	void SetActiveValue(const FText&);
	void SetActiveValueIndex(const int32 &Index, const bool IsOuterCalled = true);

	FORCEINLINE FText GetActiveValue() const
	{
		return ArrayValues[ActiveValue];
	}

	FORCEINLINE int32 GetActiveValueIndex() const
	{
		return ActiveValue;
	}

protected:
	
	FReply onClickedButton(const bool isRight);

	const FSliderSpinStyle *Style;
	TSharedPtr<class STextBlock> CenterText;

	TArray<FText> ArrayValues;
	int32 ActiveValue;

	FOnListSliderValue OnListValue;
	FOnListSliderValue OnChangeValue;
};
