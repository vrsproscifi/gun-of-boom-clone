// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "Widgets/SCompoundWidget.h"

/**
 * 
 */
class SHOOTERGAME_API STouchInput : public SCompoundWidget
{
public:
	SLATE_BEGIN_ARGS(STouchInput)
	{}
	SLATE_END_ARGS()

	void Construct(const FArguments& InArgs);

protected:

	float CurrentDeltaTime;

	virtual void Tick(const FGeometry& AllottedGeometry, const double InCurrentTime, const float InDeltaTime) override;

	virtual void ProccesInputAction(const FName& InActionName, EInputEvent InEvent);
	virtual void ProccesInputAxis(const FName& InActionName, float InScale);
};
