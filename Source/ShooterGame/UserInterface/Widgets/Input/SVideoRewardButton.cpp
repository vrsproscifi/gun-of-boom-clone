// Fill out your copyright notice in the Description page of Project Settings.

#include "ShooterGame.h"
#include "SVideoRewardButton.h"
#include "SlateOptMacros.h"

BEGIN_SLATE_FUNCTION_BUILD_OPTIMIZATION
void SVideoRewardButton::Construct(const FArguments& InArgs)
{
	Style = InArgs._Style;

	const TCHAR sVideo[] = { static_cast<TCHAR>(0xf167), 0 }; // fa-brand

	ChildSlot
	[
		SNew(SButton)
		.ButtonStyle(&Style->Button)
		.OnClicked(InArgs._OnClicked)
		.ContentPadding(FMargin(20))
		.HAlign(HAlign_Center)
		.VAlign(VAlign_Center)
		[
			SNew(SHorizontalBox)
			+ SHorizontalBox::Slot().VAlign(VAlign_Center).HAlign(HAlign_Right).AutoWidth().Padding(0, 0, 4, 0)
			[
				SNew(STextBlock)
				.TextStyle(&Style->FontAwesome)
				.Text(FText::FromString(sVideo))
			]
			+ SHorizontalBox::Slot().VAlign(VAlign_Center).HAlign(HAlign_Left).AutoWidth()
			[
				SNew(STextBlock)
				.TextStyle(&Style->FontName)
				.Text(InArgs._Text)
			]
		]
	];
}
END_SLATE_FUNCTION_BUILD_OPTIMIZATION
