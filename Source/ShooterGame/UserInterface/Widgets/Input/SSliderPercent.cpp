// VRSPRO

#include "ShooterGame.h"
#include "SSliderPercent.h"
#include "SlateOptMacros.h"

BEGIN_SLATE_FUNCTION_BUILD_OPTIMIZATION
void SSliderPercent::Construct(const FArguments& InArgs)
{
	SSliderNumber<float>::Construct(SSliderNumber<float>::FArguments()
									.Style(InArgs._Style)
									.TextLeft(InArgs._TextLeft)
									.TextRight(InArgs._TextRight)
									.ButtonsDelta(InArgs._ButtonsDelta)
									.OnValueChange(InArgs._OnValueChange)
									.CustomCenterText(this, &SSliderPercent::GetCurrentValueText_Custom)
									.SliderMinimum(.0f)
									.SliderMaximum(1.0f)
									);
}
END_SLATE_FUNCTION_BUILD_OPTIMIZATION

FText SSliderPercent::GetCurrentValueText_Custom() const
{
	return FText::AsPercent(CurrentValue);
}