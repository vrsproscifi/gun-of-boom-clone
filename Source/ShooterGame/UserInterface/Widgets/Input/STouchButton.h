// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "Widgets/SCompoundWidget.h"
#include "Styles/TocuhButtonWidgetStyle.h"

/**
 * 
 */
class SHOOTERGAME_API STouchButton : public SBorder
{
public:
	SLATE_BEGIN_ARGS(STouchButton)
		: _Style(&FShooterStyle::Get().GetWidgetStyle<FTocuhButtonStyle>("STocuhButtonStyle"))
	{
		_Visibility = EVisibility::Visible;
	}
	SLATE_STYLE_ARGUMENT(FTocuhButtonStyle, Style)
	SLATE_DEFAULT_SLOT(FArguments, Content)
	SLATE_ARGUMENT(EHorizontalAlignment, HAlign)
	SLATE_ARGUMENT(EVerticalAlignment, VAlign)
	SLATE_ATTRIBUTE(FMargin, Padding)
	SLATE_EVENT(FOnClicked, OnClicked)
	SLATE_END_ARGS()

	void Construct(const FArguments& InArgs);

protected:

	const FTocuhButtonStyle* Style;

	bool bIsPressed;
	int8 TouchIndex;

	FOnClicked OnClicked;

	const FSlateBrush* GetCurrentBrush() const;

	virtual FReply OnTouchStarted(const FGeometry& MyGeometry, const FPointerEvent& InTouchEvent) override;
	virtual FReply OnTouchEnded(const FGeometry& MyGeometry, const FPointerEvent& InTouchEvent) override;
	virtual void OnMouseCaptureLost(const FCaptureLostEvent& CaptureLostEvent) override;
};
