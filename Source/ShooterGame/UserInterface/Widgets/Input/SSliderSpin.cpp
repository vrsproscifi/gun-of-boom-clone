// VRSPRO

#include "ShooterGame.h"
#include "SSliderSpin.h"
#include "SlateOptMacros.h"

BEGIN_SLATE_FUNCTION_BUILD_OPTIMIZATION
void SSliderSpin::Construct(const FArguments& InArgs)
{
	ArrayValues = InArgs._Values;
	ActiveValue = InArgs._ActiveValue;
	Style = InArgs._Style;

	OnListValue = InArgs._OnListValue;
	OnChangeValue = InArgs._OnChangeValue;

	ChildSlot
	[
		SNew(SHorizontalBox)
		+ SHorizontalBox::Slot()
		.AutoWidth()
		[
			SNew(SButton)
			.ButtonStyle(&Style->ButtonLeft)
			.OnClicked(this, &SSliderSpin::onClickedButton, false)
			.VAlign(EVerticalAlignment::VAlign_Center)
			[
				SNew(STextBlock)
				.TextStyle(&Style->TextButtons)
				.Margin(Style->ButtonsTextPadding)
				.Justification(ETextJustify::Center)
				.Text(InArgs._TextLeft)
			]
		]
		+ SHorizontalBox::Slot()
		.FillWidth(1.0f)
		[
			SNew(SBorder)
			.Padding(0)
			.BorderImage(&Style->Background)
			.VAlign(EVerticalAlignment::VAlign_Center)
			[
				SAssignNew(CenterText, STextBlock)
				.TextStyle(&Style->TextCenter)
				.Margin(Style->CenterTextPadding)
				.Justification(ETextJustify::Center)
			]
		]
		+ SHorizontalBox::Slot()
		.AutoWidth()
		[
			SNew(SButton)
			.ButtonStyle(&Style->ButtonRight)
			.OnClicked(this, &SSliderSpin::onClickedButton, true)
			.VAlign(EVerticalAlignment::VAlign_Center)
			[
				SNew(STextBlock)
				.TextStyle(&Style->TextButtons)
				.Margin(FMargin(Style->ButtonsTextPadding.Right, Style->ButtonsTextPadding.Top, Style->ButtonsTextPadding.Left, Style->ButtonsTextPadding.Bottom))
				.Justification(ETextJustify::Center)
				.Text(InArgs._TextRight)
			]
		]
	];

	if (ArrayValues.IsValidIndex(ActiveValue))
	{
		CenterText->SetText(ArrayValues[ActiveValue]);
	}
}
END_SLATE_FUNCTION_BUILD_OPTIMIZATION

FReply SSliderSpin::onClickedButton(const bool isRight)
{
	if (isRight)
	{
		if (ArrayValues.IsValidIndex(ActiveValue + 1))
		{
			SetActiveValueIndex(ActiveValue + 1, false);
		}
		else
		{
			SetActiveValueIndex(0, false);
		}
	}
	else
	{
		if (ArrayValues.IsValidIndex(ActiveValue - 1))
		{
			SetActiveValueIndex(ActiveValue - 1, false);
		}
		else
		{
			SetActiveValueIndex(ArrayValues.Num() - 1, false);
		}
	}

	return FReply::Handled();
}

void SSliderSpin::SetActiveValue(const FText& Value)
{
	SIZE_T _count = 0;

	for (auto item : ArrayValues)
	{
		if (item.EqualTo(Value))
		{
			SetActiveValueIndex(_count);
			break;
		}

		++_count;
	}
}

void SSliderSpin::SetActiveValueIndex(const int32 &Index, const bool IsOuterCalled)
{
	if (ArrayValues.IsValidIndex(Index))
	{
		ActiveValue = Index;
		CenterText->SetText(ArrayValues[ActiveValue]);

		if (IsOuterCalled == false)	OnListValue.ExecuteIfBound(ActiveValue);
		OnChangeValue.ExecuteIfBound(ActiveValue);
	}
	else if(ArrayValues.Num())
	{
		int32 NewIndex = 0;
		
		if (Index < ArrayValues.Num())
		{
			NewIndex = ArrayValues.Num() - 1;
		}

		ActiveValue = NewIndex;
		CenterText->SetText(ArrayValues[ActiveValue]);

		if (IsOuterCalled == false)	OnListValue.ExecuteIfBound(ActiveValue);
		OnChangeValue.ExecuteIfBound(ActiveValue);
	}
}

int32 SSliderSpin::AddValue(const FText& Value)
{
	return ArrayValues.Add(Value);
}

void SSliderSpin::SetValues(TArray<FText> Values)
{
	ArrayValues = Values;
	SetActiveValueIndex(0);
}

bool SSliderSpin::RemoveValue(const int32& Index)
{
	if (ArrayValues.IsValidIndex(Index))
	{
		ArrayValues.RemoveAt(Index);
		return true;
	}

	return false;
}