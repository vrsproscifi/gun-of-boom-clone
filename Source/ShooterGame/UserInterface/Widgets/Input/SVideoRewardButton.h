// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "Widgets/SCompoundWidget.h"
#include "Styles/VideoRewardButtonWidgetStyle.h"

/**
 * 
 */
class SHOOTERGAME_API SVideoRewardButton : public SCompoundWidget
{
public:
	SLATE_BEGIN_ARGS(SVideoRewardButton)
		: _Text(NSLOCTEXT("SVideoRewardButton", "SVideoRewardButton.Text", "Multiply Reward"))
		, _Style(&FShooterStyle::Get().GetWidgetStyle<FVideoRewardButtonStyle>("SVideoRewardButtonStyle"))
	{}
	SLATE_EVENT(FOnClicked, OnClicked)
	SLATE_ATTRIBUTE(FText, Text)
	SLATE_STYLE_ARGUMENT(FVideoRewardButtonStyle, Style)
	SLATE_END_ARGS()

	void Construct(const FArguments& InArgs);

protected:

	const FVideoRewardButtonStyle* Style;
};
