// Fill out your copyright notice in the Description page of Project Settings.

#include "ShooterGame.h"
#include "SSocialButton.h"
#include "SlateOptMacros.h"

BEGIN_SLATE_FUNCTION_BUILD_OPTIMIZATION
void SSocialButton::Construct(const FArguments& InArgs)
{
	Style = InArgs._Style;

	ChildSlot
	[
		SAssignNew(Widget_Button, SButton)
		.ContentPadding(FMargin(0))
		[
			SNew(SHorizontalBox)
			+ SHorizontalBox::Slot().AutoWidth()
			[
				SNew(SBox).WidthOverride(60).HAlign(HAlign_Center).VAlign(VAlign_Center)
				[
					SAssignNew(Widget_Glyph, STextBlock)
					.TextStyle(&Style->FontAwesome)
				]
			]
			+ SHorizontalBox::Slot().AutoWidth().HAlign(HAlign_Center)
			[
				SNew(SSeparator)
				.Orientation(Orient_Vertical)
				.SeparatorImage(&Style->SeparatorBrush)
				.Thickness(Style->SeparatorThickness)
			]
			+ SHorizontalBox::Slot().AutoWidth().Padding(10, 2)
			[
				SAssignNew(Widget_Text, STextBlock)
				.TextStyle(&Style->FontText)
			]
		]
	];

	ButtonStyle.Normal = Style->Background;
	ButtonStyle.Hovered = Style->Background;
	ButtonStyle.Pressed = Style->Background;

	SetTargetSocial(InArgs._TargetSocial);
}
END_SLATE_FUNCTION_BUILD_OPTIMIZATION

void SSocialButton::SetTargetSocial(const FName& InSocial)
{
	if (Style->Buttons.Contains(InSocial))
	{
		auto TargetSocialStyle = &Style->Buttons.FindChecked(InSocial);

		ButtonStyle.Normal.TintColor = TargetSocialStyle->Normal;
		ButtonStyle.Hovered.TintColor = TargetSocialStyle->Hovered;
		ButtonStyle.Pressed.TintColor = TargetSocialStyle->Pressed;

		Widget_Glyph->SetText(FText::FromString(TargetSocialStyle->Glyph));
		Widget_Text->SetText(FText::Format(NSLOCTEXT("SSocialButton", "SSocialButton.SignIn", "Sign in with {0}"), FText::FromString(InSocial.ToString())));

		Widget_Button->SetButtonStyle(&ButtonStyle);
	}
}

