// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "STouchInputButton.h"
#include "WeaponPickupBetter.h"
#include "UserInterface/Widgets/Styles/TouchInputPickupWidgetStyle.h"

class SHOOTERGAME_API STouchInputPickup : public STouchInput
{
public:
	SLATE_BEGIN_ARGS(STouchInputPickup)
		: _Style(&FShooterStyle::Get().GetWidgetStyle<FTouchInputPickupStyle>("STouchInputPickupStyle"))
		, _ActivationMethod(EButtonActivationMethod::Click)
		, _Action(NAME_None)
	{}
	SLATE_STYLE_ARGUMENT(FTouchInputPickupStyle, Style)
	SLATE_ATTRIBUTE(EButtonActivationMethod::Type, ActivationMethod)
	SLATE_ATTRIBUTE(FName, Action)
	SLATE_ATTRIBUTE(bool, IsActivated)
	SLATE_ATTRIBUTE(const FSlateBrush*, PickupIcon)
	SLATE_ATTRIBUTE(EWeaponPickupBetter::Type, Better)
	SLATE_END_ARGS()

	void Construct(const FArguments& InArgs);

protected:

	const FTouchInputPickupStyle* Style;

	TAttribute<EButtonActivationMethod::Type> Method;
	TAttribute<FName> Action;
	TAttribute<const FSlateBrush*> PickupIcon;
	TAttribute<bool> IsActivated;
	TAttribute<EWeaponPickupBetter::Type> Better;

	int32 TouchIndex;

	virtual FReply OnTouchStarted(const FGeometry& MyGeometry, const FPointerEvent& InTouchEvent) override;
	virtual FReply OnTouchEnded(const FGeometry& MyGeometry, const FPointerEvent& InTouchEvent) override;
	virtual void OnMouseLeave(const FPointerEvent& MouseEvent) override;
	virtual void OnMouseCaptureLost(const FCaptureLostEvent& CaptureLostEvent) override;

	const FSlateBrush* GetCurrentBackground() const;
	const FSlateBrush* GetCurrentBetter() const;
};
