// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#ifndef __EWeaponPickupBetter_H__
#define __EWeaponPickupBetter_H__


namespace EWeaponPickupBetter
{
	enum Type
	{
		None,
		Better,
		Worse,
		End
	};
}

#endif