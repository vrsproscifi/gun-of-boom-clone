// Fill out your copyright notice in the Description page of Project Settings.

#include "ShooterGame.h"
#include "STouchInputButton.h"
#include "SlateOptMacros.h"
#include "Utilities/SlateUtilityTypes.h"
#include "Components/SSphereProgress.h"

BEGIN_SLATE_FUNCTION_BUILD_OPTIMIZATION
void STouchInputButton::Construct(const FArguments& InArgs)
{
	Style = InArgs._Style;
	Method = InArgs._ActivationMethod;
	Action = InArgs._Action;
	IsActivated = InArgs._IsActivated;
	Amount = InArgs._Amount;
	Progress = InArgs._Progress;
	OnRawClicked = InArgs._OnRawClicked;
	
	TouchIndex = INDEX_NONE;

	ChildSlot
	[
		SAssignNew(Widget_SphereProgress, SSphereProgress)
		.Rotation_Lambda([&]() { return Style->ProgressRotation; })
		.Style(&Style->ProgressStyle)
		.Progress(Progress)
	];
}
END_SLATE_FUNCTION_BUILD_OPTIMIZATION

void STouchInputButton::ForceClearTouch()
{
	TouchIndex = INDEX_NONE;
	IsActivated = false;
}

FReply STouchInputButton::OnTouchStarted(const FGeometry& MyGeometry, const FPointerEvent& InTouchEvent)
{
	if (TouchIndex == INDEX_NONE)
	{
		TouchIndex = InTouchEvent.GetPointerIndex();

		if (Method.Get() == EButtonActivationMethod::Toggle)
		{
			OnRawClicked.ExecuteIfBound();
			ProccesInputAction(Action.Get(), IsActivated.Get() ? IE_Released : IE_Pressed);
		}
		else if (Method.Get() == EButtonActivationMethod::Click)
		{
			IsActivated = true;
			ProccesInputAction(Action.Get(), IE_Pressed);
		}

		return FReply::Handled().CaptureMouse(AsShared());
	}

	return FReply::Unhandled();
}

FReply STouchInputButton::OnTouchEnded(const FGeometry& MyGeometry, const FPointerEvent& InTouchEvent)
{
	if (TouchIndex == InTouchEvent.GetPointerIndex())
	{
		TouchIndex = INDEX_NONE;

		if (Method.Get() == EButtonActivationMethod::Click)
		{
			OnRawClicked.ExecuteIfBound();
			ForceClearTouch();
			ProccesInputAction(Action.Get(), IE_Released);
		}	

		return FReply::Handled().ReleaseMouseCapture();
	}

	return FReply::Unhandled();
}

void STouchInputButton::OnMouseLeave(const FPointerEvent& MouseEvent)
{
	if (TouchIndex == MouseEvent.GetPointerIndex() && Method.Get() == EButtonActivationMethod::Click)
	{
		ForceClearTouch();
	}
}

void STouchInputButton::OnMouseCaptureLost(const FCaptureLostEvent& CaptureLostEvent)
{
	if (TouchIndex == CaptureLostEvent.PointerIndex)
	{
		ForceClearTouch();
	}
}

int32 STouchInputButton::OnPaint(const FPaintArgs& Args,
	const FGeometry& AllottedGeometry,
	const FSlateRect& MyCullingRect,
	FSlateWindowElementList& OutDrawElements,
	int32 LayerId,
	const FWidgetStyle& InWidgetStyle,
	bool bParentEnabled) const
{
	const bool bEnabled = ShouldBeEnabled(bParentEnabled);
	ESlateDrawEffect DrawEffects = !bEnabled ? ESlateDrawEffect::DisabledEffect : ESlateDrawEffect::None;

	const auto ButtonBrush = IsActivated.Get() ? &Style->ButtonActivated : &Style->ButtonNormal;
	const auto IconBrush = IsActivated.Get() ? &Style->IconActivated : &Style->IconNormal;
	
	if (ButtonBrush->DrawAs != ESlateBrushDrawType::NoDrawType && GetVisibility() != EVisibility::Collapsed)
	{
		FSlateDrawElement::MakeBox(
			OutDrawElements,
			++LayerId,
			AllottedGeometry.ToPaintGeometry(),
			ButtonBrush,
			DrawEffects,
			ButtonBrush->GetTint(InWidgetStyle) * InWidgetStyle.GetColorAndOpacityTint()
		);
	}

	if (IconBrush->DrawAs != ESlateBrushDrawType::NoDrawType && GetVisibility() != EVisibility::Collapsed)
	{
		FSlateDrawElement::MakeBox(
			OutDrawElements,
			++LayerId,
			AllottedGeometry.ToPaintGeometry(),
			IconBrush,
			DrawEffects,
			IconBrush->GetTint(InWidgetStyle) * InWidgetStyle.GetColorAndOpacityTint()
		);
	}

	int32 SuperPaint = SCompoundWidget::OnPaint(Args, AllottedGeometry, MyCullingRect, OutDrawElements, LayerId, InWidgetStyle, bEnabled);

	if (Amount.Get() != INDEX_NONE)
	{
		if (Style->AmountBrush.DrawAs != ESlateBrushDrawType::NoDrawType && GetVisibility() != EVisibility::Collapsed)
		{
			const FVector2D DrawPosition = FSlateWidgetMath::GetPositionInRadial(AllottedGeometry.GetLocalSize() / 2, Style->AmountDegress, Style->AmountRadius);

			FSlateDrawElement::MakeBox(
				OutDrawElements,
				++SuperPaint,
				AllottedGeometry.ToPaintGeometry(DrawPosition, Style->AmountBrush.ImageSize),
				&Style->AmountBrush,
				DrawEffects,
				Style->AmountBrush.GetTint(InWidgetStyle) * InWidgetStyle.GetColorAndOpacityTint()
			);

			FSlateDrawElement::MakeText(
				OutDrawElements,
				++SuperPaint,
				AllottedGeometry.ToPaintGeometry(DrawPosition + Style->AmountText.Font.Size, Style->AmountBrush.ImageSize),
				FText::AsNumber(Amount.Get(), &FNumberFormattingOptions::DefaultNoGrouping()),
				Style->AmountText.Font,
				DrawEffects,
				Style->AmountText.ColorAndOpacity.GetSpecifiedColor() * InWidgetStyle.GetColorAndOpacityTint()
			);
		}
	}	

	return SuperPaint;
}
