// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "STouchInput.h"
#include "Styles/TouchInputStickWidgetStyle.h"

/**
 * 
 */
class SHOOTERGAME_API STouchInputStick : public STouchInput
{
public:
	SLATE_BEGIN_ARGS(STouchInputStick)
		: _Style(&FShooterStyle::Get().GetWidgetStyle<FTouchInputStickStyle>("STouchInputStickStyle"))
		, _AxisX(NAME_None)
		, _AxisY(NAME_None)
		, _StickSize(.0f)
	{}
	SLATE_STYLE_ARGUMENT(FTouchInputStickStyle, Style)
	SLATE_ATTRIBUTE(FName, AxisX)
	SLATE_ATTRIBUTE(FName, AxisY)
	SLATE_ATTRIBUTE(FName, ActionTargetAxis)
	SLATE_ATTRIBUTE(FName, ActionOnFullAxis)
	SLATE_ATTRIBUTE(float, StickSize)
	SLATE_ATTRIBUTE(bool, IsDynamic)
	SLATE_END_ARGS()

	void Construct(const FArguments& InArgs);

protected:

	const FTouchInputStickStyle* Style;

	TAttribute<FName> AxisX;
	TAttribute<FName> AxisY;
	TAttribute<FName> ActionTargetAxis;
	TAttribute<FName> ActionOnFullAxis;
	TAttribute<float> StickSize;
	TAttribute<bool> IsDynamic;

	int32 TouchIndex;
	float NoInputTime;
	bool IsCreated;
	bool LastIsDynamic;

	FVector2D CurrentPosition;
	FVector2D LastPosition;
	FVector2D CurrentCenter;

	void ClearInput();

	virtual FReply OnTouchStarted(const FGeometry& MyGeometry, const FPointerEvent& InTouchEvent) override;
	virtual FReply OnTouchMoved(const FGeometry& MyGeometry, const FPointerEvent& InTouchEvent) override;
	virtual FReply OnTouchEnded(const FGeometry& MyGeometry, const FPointerEvent& InTouchEvent) override;
	virtual void OnMouseCaptureLost(const FCaptureLostEvent& CaptureLostEvent) override;

	virtual void Tick(const FGeometry& AllottedGeometry, const double InCurrentTime, const float InDeltaTime) override;

	int32 OnPaint(const FPaintArgs& Args, const FGeometry& AllottedGeometry, const FSlateRect& MyCullingRect, FSlateWindowElementList& OutDrawElements, int32 LayerId, const FWidgetStyle& InWidgetStyle, bool bParentEnabled) const override;
};
