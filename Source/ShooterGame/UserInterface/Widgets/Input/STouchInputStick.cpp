// Fill out your copyright notice in the Description page of Project Settings.

#include "ShooterGame.h"
#include "STouchInputStick.h"
#include "SlateOptMacros.h"

BEGIN_SLATE_FUNCTION_BUILD_OPTIMIZATION
void STouchInputStick::Construct(const FArguments& InArgs)
{
	Style = InArgs._Style;
	AxisX = InArgs._AxisX;
	AxisY = InArgs._AxisY;
	ActionTargetAxis = InArgs._ActionTargetAxis;
	ActionOnFullAxis = InArgs._ActionOnFullAxis;
	StickSize = InArgs._StickSize;
	IsDynamic = InArgs._IsDynamic;

	if (StickSize.IsSet() == false || StickSize.Get() == .0f)
	{
		StickSize = Style->StickSize;
	}

	TouchIndex = INDEX_NONE;
	IsCreated = false;
}
END_SLATE_FUNCTION_BUILD_OPTIMIZATION

void STouchInputStick::ClearInput()
{
	TouchIndex = INDEX_NONE;

	ProccesInputAxis(AxisX.Get(), .0f);
	ProccesInputAxis(AxisY.Get(), .0f);

	if (ActionTargetAxis.IsSet() && ActionOnFullAxis.IsSet())
	{
		ProccesInputAction(ActionOnFullAxis.Get(), IE_Released);
	}
}

FReply STouchInputStick::OnTouchStarted(const FGeometry& MyGeometry, const FPointerEvent& InTouchEvent)
{
	if (TouchIndex == INDEX_NONE)
	{
		NoInputTime = .0f;

		CurrentPosition = MyGeometry.AbsoluteToLocal(InTouchEvent.GetScreenSpacePosition());
		LastPosition = CurrentPosition;

		if (IsDynamic.Get())
		{			
			CurrentCenter = LastPosition;
		}
		
		TouchIndex = InTouchEvent.GetPointerIndex();

		OnTouchMoved(MyGeometry, InTouchEvent);

		return FReply::Handled().CaptureMouse(AsShared());
	}

	return FReply::Unhandled();
}

FReply STouchInputStick::OnTouchMoved(const FGeometry& MyGeometry, const FPointerEvent& InTouchEvent)
{
	if (TouchIndex == InTouchEvent.GetPointerIndex())
	{
		NoInputTime = .0f;
		CurrentPosition = MyGeometry.AbsoluteToLocal(InTouchEvent.GetScreenSpacePosition());

		const float DevideNumber = StickSize.Get() / 2;
		const float AxisXDelta = ((CurrentPosition.X - CurrentCenter.X) / DevideNumber) * -1.0f;
		const float AxisYDelta = ((CurrentPosition.Y - CurrentCenter.Y) / DevideNumber) * -1.0f;

		ProccesInputAxis(AxisX.Get(), FMath::Clamp(AxisXDelta, -1.0f, 1.0f));
		ProccesInputAxis(AxisY.Get(), FMath::Clamp(AxisYDelta, -1.0f, 1.0f));

		if (ActionTargetAxis.IsSet() && ActionOnFullAxis.IsSet())
		{
			float TargetValue = .0f;

			if (AxisX.Get() == ActionTargetAxis.Get())
			{
				TargetValue = AxisXDelta;
			}
			else if (AxisY.Get() == ActionTargetAxis.Get())
			{
				TargetValue = AxisYDelta;
			}

			if (TargetValue >= .9f)
			{
				ProccesInputAction(ActionOnFullAxis.Get(), IE_Pressed);
			}
			else
			{
				ProccesInputAction(ActionOnFullAxis.Get(), IE_Released);
			}
		}

		LastPosition = CurrentPosition;

		return FReply::Handled();		
	}

	return FReply::Unhandled();
}

FReply STouchInputStick::OnTouchEnded(const FGeometry& MyGeometry, const FPointerEvent& InTouchEvent)
{
	if (TouchIndex != INDEX_NONE)
	{
		NoInputTime = .0f;
		ClearInput();

		return FReply::Handled().ReleaseMouseCapture();
	}

	return FReply::Unhandled();
}

void STouchInputStick::OnMouseCaptureLost(const FCaptureLostEvent& CaptureLostEvent)
{
	if (CaptureLostEvent.PointerIndex == TouchIndex)
	{
		ClearInput();
	}
}

void STouchInputStick::Tick(const FGeometry& AllottedGeometry, const double InCurrentTime, const float InDeltaTime)
{
	STouchInput::Tick(AllottedGeometry, InCurrentTime, InDeltaTime);

	NoInputTime += InDeltaTime;

	if (LastIsDynamic != IsDynamic.Get())
	{
		LastIsDynamic = IsDynamic.Get();
		IsCreated = false;
	}

	if (IsCreated == false && AllottedGeometry.GetAbsoluteSize().IsNearlyZero(20.0f) == false)
	{
		IsCreated = true;
		const FVector2D LocalStickSize = AllottedGeometry.AbsoluteToLocal(FVector2D(StickSize.Get(), StickSize.Get()));

		CurrentCenter.X = LocalStickSize.X * 0.7f;
		CurrentCenter.Y = AllottedGeometry.GetLocalSize().Y - (LocalStickSize.Y * 0.7f);
		CurrentPosition = CurrentCenter;
	}

	if (TouchIndex == INDEX_NONE && FMath::IsNearlyZero(NoInputTime, 2.0f))
	{
		CurrentPosition = CurrentCenter;

		ClearInput();
	}
}

int32 STouchInputStick::OnPaint(const FPaintArgs& Args,
	const FGeometry& AllottedGeometry,
	const FSlateRect& MyCullingRect,
	FSlateWindowElementList& OutDrawElements,
	int32 LayerId,
	const FWidgetStyle& InWidgetStyle,
	bool bParentEnabled) const
{
	const bool bEnabled = ShouldBeEnabled(bParentEnabled);

	if ((IsDynamic.Get() && TouchIndex != INDEX_NONE) || IsDynamic.Get() == false)
	{		
		ESlateDrawEffect DrawEffects = !bEnabled ? ESlateDrawEffect::DisabledEffect : ESlateDrawEffect::None;

		if (Style->Background.DrawAs != ESlateBrushDrawType::NoDrawType)
		{
			FSlateDrawElement::MakeBox(
				OutDrawElements,
				++LayerId,
				AllottedGeometry.ToPaintGeometry(CurrentCenter - StickSize.Get() / 2, FVector2D(StickSize.Get(), StickSize.Get())),
				&Style->Background,
				DrawEffects,
				Style->Background.GetTint(InWidgetStyle) * InWidgetStyle.GetColorAndOpacityTint()
			);
		}

		if (Style->Stick.DrawAs != ESlateBrushDrawType::NoDrawType)
		{
			const float DevideNumber = StickSize.Get() / 2;
			const float AxisXDelta = FMath::Clamp((CurrentPosition.X - CurrentCenter.X) / DevideNumber, -1.0f, 1.0f);
			const float AxisYDelta = FMath::Clamp((CurrentPosition.Y - CurrentCenter.Y) / DevideNumber, -1.0f, 1.0f);

			FVector2D DrawPosition = CurrentCenter - StickSize.Get() / 2;
			FVector2D TargetPosition(AxisXDelta, AxisYDelta);

			// From https://stackoverflow.com/questions/38865608/2d-vector-movement-and-magnitude-inconsistencies?rq=1
			// Really cool solution, on ue4 resources only bad answers

			float L = FMath::Sqrt(AxisXDelta * AxisXDelta + AxisYDelta * AxisYDelta);

			if (L > 0) {
				float angle = FMath::Atan2(AxisYDelta, AxisXDelta);
				float kx = FMath::Abs(FMath::Cos(angle));
				float ky = FMath::Abs(FMath::Sin(angle));
				float k = FMath::Max(kx, ky);
				kx /= k; ky /= k;
				float ext = FMath::Sqrt(kx*kx + ky * ky);
				TargetPosition = FVector2D(AxisXDelta / ext, AxisYDelta / ext);
				//dx /= ext; dy /= ext;
			}

			GEngine->AddOnScreenDebugMessage(2, 1.0f, FColorList::LimeGreen, *FString::Printf(TEXT("TargetPosition [Percent]: %s"), *TargetPosition.ToString()), false);

			DrawPosition += TargetPosition * DevideNumber;

			GEngine->AddOnScreenDebugMessage(3, 1.0f, FColorList::OrangeRed, *FString::Printf(TEXT("TargetPosition [Corrected]: %s"), *DrawPosition.ToString()), false);

			FSlateDrawElement::MakeBox(
				OutDrawElements,
				++LayerId,
				AllottedGeometry.ToPaintGeometry(DrawPosition, FVector2D(StickSize.Get(), StickSize.Get())),
				&Style->Stick,
				DrawEffects,
				Style->Stick.GetTint(InWidgetStyle) * InWidgetStyle.GetColorAndOpacityTint()
			);
		}
	}

	return SCompoundWidget::OnPaint(Args, AllottedGeometry, MyCullingRect, OutDrawElements, LayerId, InWidgetStyle, bEnabled);
}
