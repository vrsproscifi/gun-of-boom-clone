// Fill out your copyright notice in the Description page of Project Settings.

#include "ShooterGame.h"
#include "STouchInputLook.h"
#include "SlateOptMacros.h"

BEGIN_SLATE_FUNCTION_BUILD_OPTIMIZATION
void STouchInputLook::Construct(const FArguments& InArgs)
{
	AxisX = InArgs._AxisX;
	AxisY = InArgs._AxisY;

	TouchIndex = INDEX_NONE;
}
END_SLATE_FUNCTION_BUILD_OPTIMIZATION

FReply STouchInputLook::OnTouchStarted(const FGeometry& MyGeometry, const FPointerEvent& InTouchEvent)
{
	if (TouchIndex == INDEX_NONE)
	{
		CurrenPoint = MyGeometry.AbsoluteToLocal(InTouchEvent.GetScreenSpacePosition());
		TouchIndex = InTouchEvent.GetPointerIndex();
		return FReply::Handled().CaptureMouse(AsShared());
	}

	return FReply::Unhandled();
}

FReply STouchInputLook::OnTouchMoved(const FGeometry& MyGeometry, const FPointerEvent& InTouchEvent)
{
	if (TouchIndex == InTouchEvent.GetPointerIndex())
	{
		const FVector2D CurrentPosition = MyGeometry.AbsoluteToLocal(InTouchEvent.GetScreenSpacePosition());

		CurrenInputDelta = (CurrentPosition - CurrenPoint) / 10.0f;

		ProccesInputAxis(AxisX.Get(), CurrenInputDelta.X);
		ProccesInputAxis(AxisY.Get(), CurrenInputDelta.Y);

		CurrenPoint = CurrentPosition;

		LastMovedTime = FSlateApplication::Get().GetCurrentTime();

		return FReply::Handled();		
	}

	return FReply::Unhandled();
}

FReply STouchInputLook::OnTouchEnded(const FGeometry& MyGeometry, const FPointerEvent& InTouchEvent)
{
	if (TouchIndex != INDEX_NONE)
	{
		TouchIndex = INDEX_NONE;

		return FReply::Handled().ReleaseMouseCapture();
	}

	return FReply::Unhandled();
}

void STouchInputLook::Tick(const FGeometry& AllottedGeometry, const double InCurrentTime, const float InDeltaTime)
{
	STouchInput::Tick(AllottedGeometry, InCurrentTime, InDeltaTime);

	if (FMath::IsNearlyEqual(LastMovedTime, FSlateApplication::Get().GetCurrentTime(), .05) == false)
	{
		ProccesInputAxis(AxisX.Get(), .0f);
		ProccesInputAxis(AxisY.Get(), .0f);
	}
}
