// Fill out your copyright notice in the Description page of Project Settings.

#include "ShooterGame.h"
#include "STouchInputPickup.h"
#include "SlateOptMacros.h"
#include "SScaleBox.h"

BEGIN_SLATE_FUNCTION_BUILD_OPTIMIZATION
void STouchInputPickup::Construct(const FArguments& InArgs)
{
	Style = InArgs._Style;
	Method = InArgs._ActivationMethod;
	Action = InArgs._Action;
	IsActivated = InArgs._IsActivated;
	PickupIcon = InArgs._PickupIcon;
	Better = InArgs._Better;

	ChildSlot
	[
		SNew(SBorder)
		.Clipping(EWidgetClipping::ClipToBoundsAlways)
		.BorderImage(this, &STouchInputPickup::GetCurrentBackground)
		.Visibility(EVisibility::HitTestInvisible)
		.Padding(0)
		[
			SNew(SVerticalBox)
			+ SVerticalBox::Slot().HAlign(HAlign_Center)
			[
				SNew(SHorizontalBox)
				+ SHorizontalBox::Slot().AutoWidth().Padding(5)
				[
					SNew(SScaleBox)					
					.Stretch(EStretch::ScaleToFit)
					[
						SNew(SImage)
						.Image(this, &STouchInputPickup::GetCurrentBetter)
					]
				]
				+ SHorizontalBox::Slot().AutoWidth()
				[
					SNew(SBox).WidthOverride(200)
					[
						SNew(SScaleBox)
						.Stretch(EStretch::ScaleToFitX)
						[
							SNew(SImage)
							.Image(PickupIcon)
						]
					]
				]
				+ SHorizontalBox::Slot().AutoWidth().Padding(5)
				[
					SNew(SScaleBox)
					.Stretch(EStretch::ScaleToFit)
					[
						SNew(SImage)
						.Image(this, &STouchInputPickup::GetCurrentBetter)
					]
				]
			]
			+ SVerticalBox::Slot().FillHeight(.5f).VAlign(VAlign_Center)
			[
				SNew(STextBlock)
				.Justification(ETextJustify::Center)
				.TextStyle(&Style->Text)
				.Text(NSLOCTEXT("STouchInputPickup", "STouchInputPickup.Pickup", "Pickup"))
			]
		]
	];

	TouchIndex = INDEX_NONE;
}
END_SLATE_FUNCTION_BUILD_OPTIMIZATION

FReply STouchInputPickup::OnTouchStarted(const FGeometry& MyGeometry, const FPointerEvent& InTouchEvent)
{
	if (TouchIndex == INDEX_NONE)
	{
		TouchIndex = InTouchEvent.GetPointerIndex();

		if (Method.Get() == EButtonActivationMethod::Toggle)
		{
			ProccesInputAction(Action.Get(), IsActivated.Get() ? IE_Released : IE_Pressed);
		}
		else if (Method.Get() == EButtonActivationMethod::Click)
		{
			IsActivated = true;
			ProccesInputAction(Action.Get(), IE_Pressed);
		}

		return FReply::Handled().CaptureMouse(AsShared());
	}

	return FReply::Unhandled();
}

FReply STouchInputPickup::OnTouchEnded(const FGeometry& MyGeometry, const FPointerEvent& InTouchEvent)
{
	if (TouchIndex == InTouchEvent.GetPointerIndex())
	{
		TouchIndex = INDEX_NONE;

		if (Method.Get() == EButtonActivationMethod::Click)
		{
			IsActivated = false;
			ProccesInputAction(Action.Get(), IE_Released);
		}

		return FReply::Handled().ReleaseMouseCapture();
	}

	return FReply::Unhandled();
}

void STouchInputPickup::OnMouseLeave(const FPointerEvent& MouseEvent)
{
	if (TouchIndex == MouseEvent.GetPointerIndex() && Method.Get() == EButtonActivationMethod::Click)
	{
		TouchIndex = INDEX_NONE;
		IsActivated = false;
	}
}

void STouchInputPickup::OnMouseCaptureLost(const FCaptureLostEvent& CaptureLostEvent)
{
	if (TouchIndex == CaptureLostEvent.PointerIndex)
	{
		TouchIndex = INDEX_NONE;
		IsActivated = false;
	}
}

const FSlateBrush* STouchInputPickup::GetCurrentBackground() const
{
	return IsActivated.Get() ? &Style->ActiveBrush : &Style->NormalBrush;
}

const FSlateBrush* STouchInputPickup::GetCurrentBetter() const
{
	switch (Better.Get()) 
	{ 
		case EWeaponPickupBetter::Better: return &Style->BrushBetter;
		case EWeaponPickupBetter::Worse:  return &Style->BrushWorse;
		default: return &Style->BrushSome;
	}
}
