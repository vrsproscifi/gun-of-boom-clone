// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "Widgets/SCompoundWidget.h"
#include "Styles/TocuhInputSwitchWidgetStyle.h"

class AShooterWeapon;
DECLARE_DELEGATE_RetVal(TArray<AShooterWeapon*>, FOnGetWeapons);
DECLARE_DELEGATE_OneParam(FOnSelectWeapon, AShooterWeapon*);

class SHOOTERGAME_API STouchInputSwitch : public SCompoundWidget
{
public:

	SLATE_BEGIN_ARGS(STouchInputSwitch)
		: _Style(&FShooterStyle::Get().GetWidgetStyle<FTocuhInputSwitchStyle>("STocuhInputSwitchStyle"))
	{}
	SLATE_STYLE_ARGUMENT(FTocuhInputSwitchStyle, Style)
	SLATE_EVENT(FOnGetWeapons, OnGetWeapons)
	SLATE_EVENT(FOnSelectWeapon, OnSelectWeapon)
	SLATE_ATTRIBUTE(AShooterWeapon*, CurrentWeapon)
	SLATE_END_ARGS()

	void Construct(const FArguments& InArgs);

protected:

	const FTocuhInputSwitchStyle* Style;

	TWeakObjectPtr<AShooterWeapon> LastHovered;
	TAttribute<AShooterWeapon*> CurrentWeapon;

	TSharedPtr<SMenuAnchor> ListContainer;
	TSharedPtr<SCheckBox> MainButton;

	FOnGetWeapons OnGetWeapons;
	FOnSelectWeapon OnSelectWeapon;

	TSharedRef<SWidget> GenerateWeaponsList();

	FReply OnTouchStarted(const FGeometry& MyGeometry, const FPointerEvent& InTouchEvent) override;
	FReply OnTouchMoved(const FGeometry& MyGeometry, const FPointerEvent& InTouchEvent) override;
	FReply OnTouchEnded(const FGeometry& MyGeometry, const FPointerEvent& InTouchEvent) override;
};
