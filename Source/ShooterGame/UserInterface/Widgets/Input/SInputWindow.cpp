// VRSPRO

#include "ShooterGame.h"
#include "SInputWindow.h"
#include "SlateOptMacros.h"
#include "Decorators/LokaSpaceDecorator.h"
#include "Decorators/LokaResourceDecorator.h"
#include "Decorators/LokaTextDecorator.h"

//#include "SLokaPopUpError.h"

BEGIN_SLATE_FUNCTION_BUILD_OPTIMIZATION
void SInputWindow::Construct(const FArguments& InArgs)
{
	Style = InArgs._Style;

	ChildSlot
	[
		SNew(SVerticalBox)
		+ SVerticalBox::Slot().AutoHeight()
		[
			SAssignNew(Widget_TextBlock, SRichTextBlock)
			.Margin(FMargin(10, 6))
			.AutoWrapText(true)
			//.Text(InArgs._Text)
			.TextStyle(&Style->Content.Text)
			.Justification(ETextJustify::Center)
			.DecoratorStyleSet(&FShooterStyle::Get())
			+ SRichTextBlock::Decorator(FLokaTextDecorator::Create(Style->Content.Text))
			+ SRichTextBlock::Decorator(FLokaResourceDecorator::Create())
			+ SRichTextBlock::Decorator(FLokaSpaceDecorator::Create())
			+ SRichTextBlock::Decorator(FHyperlinkDecorator::Create(TEXT(""), FSlateHyperlinkRun::FOnClick::CreateLambda([&](const FSlateHyperlinkRun::FMetadata& InData) {
				if (InData.Contains("url")) FPlatformProcess::LaunchURL(*InData.FindChecked("url"), nullptr, nullptr);
			})))
		]
		+ SVerticalBox::Slot().AutoHeight()
		[
			SAssignNew(Widget_EditableTextBox, SEditableTextBox)
			.Style(&Style->Content.Input)
			//.ErrorReporting(SNew(SLokaPopUpError))
			//.OnTextChanged_Lambda([&](const FText&) { Widget_EditableTextBox->SetError(""); })
		]
	];

	SetContent(InArgs._Text);
	SetInputText(InArgs._Value);
}
END_SLATE_FUNCTION_BUILD_OPTIMIZATION

void SInputWindow::SetContent(const FText& Content) 
{
	Widget_TextBlock->SetText(Content);
}

FText SInputWindow::GetInputText() const
{
	return Widget_EditableTextBox->GetText();
}

void SInputWindow::SetInputText(const FText& InText)
{
	Widget_EditableTextBox->SetText(InText);
}

void SInputWindow::SetInputError(const FText& InText)
{
	Widget_EditableTextBox->SetError(InText);
}