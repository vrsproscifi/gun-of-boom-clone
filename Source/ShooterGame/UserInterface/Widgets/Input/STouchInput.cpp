// Fill out your copyright notice in the Description page of Project Settings.

#include "ShooterGame.h"
#include "STouchInput.h"
#include "SlateOptMacros.h"

BEGIN_SLATE_FUNCTION_BUILD_OPTIMIZATION
void STouchInput::Construct(const FArguments& InArgs)
{
	/*
	ChildSlot
	[
		// Populate the widget
	];
	*/
}
END_SLATE_FUNCTION_BUILD_OPTIMIZATION

void STouchInput::Tick(const FGeometry& AllottedGeometry, const double InCurrentTime, const float InDeltaTime)
{
	SCompoundWidget::Tick(AllottedGeometry, InCurrentTime, InDeltaTime);

	CurrentDeltaTime = InDeltaTime;
}

void STouchInput::ProccesInputAction(const FName& InActionName, EInputEvent InEvent)
{
	for (TObjectIterator<UPlayerInput> It; It; ++It)
	{
		auto TargetAction = It->GetKeysForAction(InActionName);
		if (TargetAction.Num())
		{
			It->InputKey(TargetAction[0].Key, InEvent, 1, TargetAction[0].Key.IsGamepadKey());
		}
	}
}

void STouchInput::ProccesInputAxis(const FName& InActionName, float InScale)
{
	for (TObjectIterator<UPlayerInput> It; It; ++It)
	{
		auto TargetAxis = It->GetKeysForAxis(InActionName);
		if (TargetAxis.Num())
		{
			It->InputAxis(TargetAxis[0].Key, InScale, CurrentDeltaTime, 1, TargetAxis[0].Key.IsGamepadKey());
		}
	}
}
