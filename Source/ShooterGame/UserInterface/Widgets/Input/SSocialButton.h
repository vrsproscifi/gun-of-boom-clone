// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "Widgets/SCompoundWidget.h"
#include "SocialButtonWidgetStyle.h"

/**
 * 
 */
class SHOOTERGAME_API SSocialButton : public SCompoundWidget
{
public:
	SLATE_BEGIN_ARGS(SSocialButton)
		: _Style(&FShooterStyle::Get().GetWidgetStyle<FSocialButtonStyle>("SSocialButtonStyle"))
	{}
	SLATE_STYLE_ARGUMENT(FSocialButtonStyle, Style)
	SLATE_ARGUMENT(FName, TargetSocial)
	SLATE_END_ARGS()

	void Construct(const FArguments& InArgs);

	void SetTargetSocial(const FName& InSocial);

protected:

	const FSocialButtonStyle* Style;

	FButtonStyle ButtonStyle;

	TSharedPtr<STextBlock>	Widget_Glyph;
	TSharedPtr<STextBlock>	Widget_Text;
	TSharedPtr<SButton>		Widget_Button;
};
