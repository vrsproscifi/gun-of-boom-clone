// Fill out your copyright notice in the Description page of Project Settings.

#include "ShooterGame.h"
#include "STouchButton.h"
#include "SlateOptMacros.h"

BEGIN_SLATE_FUNCTION_BUILD_OPTIMIZATION
void STouchButton::Construct(const FArguments& InArgs)
{
	Style = InArgs._Style;
	OnClicked = InArgs._OnClicked;

	auto SuperArgs = SBorder::FArguments()
	.BorderImage(this, &STouchButton::GetCurrentBrush)
	.Padding(InArgs._Padding)
	.HAlign(InArgs._HAlign)
	.VAlign(InArgs._VAlign)
	.Content()[InArgs._Content.Widget];

	SBorder::Construct(SuperArgs);

	bIsPressed = false;
	TouchIndex = INDEX_NONE;
}
END_SLATE_FUNCTION_BUILD_OPTIMIZATION

const FSlateBrush* STouchButton::GetCurrentBrush() const
{
	return bIsPressed ? &Style->Pressed : &Style->Normal;
}

FReply STouchButton::OnTouchStarted(const FGeometry& MyGeometry, const FPointerEvent& InTouchEvent)
{
	if (TouchIndex == INDEX_NONE)
	{
		TouchIndex = InTouchEvent.GetPointerIndex();
		bIsPressed = true;

		if (Style && Style->PressedSound.GetResourceObject())
		{
			FSlateApplication::Get().PlaySound(Style->PressedSound);
		}

		return FReply::Handled().CaptureMouse(AsShared());
	}

	return FReply::Unhandled();
}

FReply STouchButton::OnTouchEnded(const FGeometry& MyGeometry, const FPointerEvent& InTouchEvent)
{
	if (TouchIndex != INDEX_NONE && TouchIndex == InTouchEvent.GetPointerIndex())
	{
		TouchIndex = INDEX_NONE;
		bIsPressed = false;		

		if (MyGeometry.GetLayoutBoundingRect().ContainsPoint(InTouchEvent.GetLastScreenSpacePosition()) && OnClicked.IsBound())
		{
			return OnClicked.Execute().ReleaseMouseCapture();
		}

		return FReply::Handled().ReleaseMouseCapture();
	}

	return FReply::Unhandled();
}

void STouchButton::OnMouseCaptureLost(const FCaptureLostEvent& CaptureLostEvent)
{
	if (TouchIndex == CaptureLostEvent.PointerIndex)
	{
		TouchIndex = INDEX_NONE;
		bIsPressed = false;
	}
}
