// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "STouchInput.h"

/**
 * 
 */
class SHOOTERGAME_API STouchInputLook : public STouchInput
{
public:
	SLATE_BEGIN_ARGS(STouchInputLook)
		: _AxisX(NAME_None)
		, _AxisY(NAME_None)
	{}
	SLATE_ATTRIBUTE(FName, AxisX)
	SLATE_ATTRIBUTE(FName, AxisY)
	SLATE_END_ARGS()

	void Construct(const FArguments& InArgs);

protected:

	TAttribute<FName> AxisX;
	TAttribute<FName> AxisY;

	double LastMovedTime;
	int32 TouchIndex;

	FVector2D CurrenPoint;
	FVector2D CurrenInputDelta;

	virtual FReply OnTouchStarted(const FGeometry& MyGeometry, const FPointerEvent& InTouchEvent) override;
	virtual FReply OnTouchMoved(const FGeometry& MyGeometry, const FPointerEvent& InTouchEvent) override;
	virtual FReply OnTouchEnded(const FGeometry& MyGeometry, const FPointerEvent& InTouchEvent) override;

	virtual void Tick(const FGeometry& AllottedGeometry, const double InCurrentTime, const float InDeltaTime) override;
};
