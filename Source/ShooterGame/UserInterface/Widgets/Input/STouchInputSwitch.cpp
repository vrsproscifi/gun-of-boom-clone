// Fill out your copyright notice in the Description page of Project Settings.

#include "ShooterGame.h"
#include "STouchInputSwitch.h"
#include "SlateOptMacros.h"
#include "Inventory/ShooterWeapon.h"
#include "WeaponPlayerItem.h"
#include "SScaleBox.h"
#include "WeaponItemEntity.h"
#include "ShooterGameAnalytics.h"
#include "ShooterGameAnalytics.h"

BEGIN_SLATE_FUNCTION_BUILD_OPTIMIZATION
void STouchInputSwitch::Construct(const FArguments& InArgs)
{
	Style = InArgs._Style;
	OnGetWeapons = InArgs._OnGetWeapons;
	OnSelectWeapon = InArgs._OnSelectWeapon;
	CurrentWeapon = InArgs._CurrentWeapon;

	ChildSlot
	[
		SAssignNew(ListContainer, SMenuAnchor)
		.Method(EPopupMethod::UseCurrentWindow)
		.Placement(MenuPlacement_CenteredAboveAnchor)
		.Content()
		[
			SAssignNew(MainButton, SCheckBox)
			.Style(&Style->Button)
			.Type(ESlateCheckBoxType::ToggleButton)
			.Visibility(EVisibility::HitTestInvisible)
			[
				SNew(SScaleBox)
				.Stretch(EStretch::ScaleToFit)
				[
					SNew(SImage)
					.Image(&Style->Icon)
				]
			]
		]
		.OnGetMenuContent(this, &STouchInputSwitch::GenerateWeaponsList)
	];
}
END_SLATE_FUNCTION_BUILD_OPTIMIZATION

TSharedRef<SWidget> STouchInputSwitch::GenerateWeaponsList()
{
	static FSlateBrush EmptyBrush = FSlateNoResource();

	if (OnGetWeapons.IsBound())
	{
		auto WeaponsList = OnGetWeapons.Execute();
		auto MyContainer = SNew(SVerticalBox);
		
		for (auto MyWeapon : WeaponsList)
		{
			if (MyWeapon && MyWeapon->IsValidLowLevel() && MyWeapon->Instance && MyWeapon->Instance->GetWeaponEntity())
			{
				const FSlateBrush* TargetBrush = &EmptyBrush;

				if (Style->WeaponTypes.Contains(MyWeapon->Instance->GetItemType()))
				{
					TargetBrush = &Style->WeaponTypes.FindChecked(MyWeapon->Instance->GetItemType());
				}

				MyContainer->AddSlot().AutoHeight()
				[
					SNew(SHorizontalBox)
					+ SHorizontalBox::Slot().AutoWidth()
					[
						SNew(SBox).WidthOverride(60).HeightOverride(60).Padding(4)
						[
							SNew(SScaleBox)
							.Stretch(EStretch::ScaleToFit)
							[
								SNew(SImage).Image(TargetBrush)
							]
						]
					]
					+ SHorizontalBox::Slot().AutoWidth()
					[
						SNew(SButton) // .IsChecked_Lambda([&, w = MyWeapon]() { return w == CurrentWeapon.Get(nullptr) ? ECheckBoxState::Checked : ECheckBoxState::Unchecked; })
						.OnHovered_Lambda([&, w = MyWeapon]() { LastHovered = w; })
						.ButtonStyle(&Style->Buttons)
						[
							SNew(SBox).WidthOverride(200).HeightOverride(60).Padding(4)
							[
								SNew(SScaleBox)
								.Stretch(EStretch::ScaleToFitX)
								[
									SNew(SImage).Image(&MyWeapon->Instance->GetWeaponEntity()->GetWeaponProperty().Icon)
								]
							]
						]
					]
				];
			}
		}

		return MyContainer;
	}

	return SNullWidget::NullWidget;
}

FReply STouchInputSwitch::OnTouchStarted(const FGeometry& MyGeometry, const FPointerEvent& InTouchEvent)
{
	ListContainer->SetIsOpen(true);
	MainButton->SetIsChecked(ECheckBoxState::Checked);

	return FReply::Handled();
}

FReply STouchInputSwitch::OnTouchMoved(const FGeometry& MyGeometry, const FPointerEvent& InTouchEvent)
{
	return FReply::Unhandled();
}

FReply STouchInputSwitch::OnTouchEnded(const FGeometry& MyGeometry, const FPointerEvent& InTouchEvent)
{
	ListContainer->SetIsOpen(false);
	MainButton->SetIsChecked(ECheckBoxState::Unchecked);

	FShooterGameAnalytics::RecordDesignEvent("UI:Game:SelectWeapon");
	if(OnSelectWeapon.ExecuteIfBound(LastHovered.Get()) == false)
	{
		FShooterGameAnalytics::RecordErrorEvent(EErrorSeverity::warning, "OnSelectWeapon | Event not bound");
	}

	

	return FReply::Handled();
}
