// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "SlateOptMacros.h"
#include "Widgets/SCompoundWidget.h"
#include "Styles/SpinNumberWidgetStyle.h"

/**
 * 
 */

template<typename NumberType>
class SSpinNumber : public SCompoundWidget
{
public:
	SSpinNumber<NumberType>()
	{
		static_assert(
			TIsSame<NumberType, int8>().Value ||
			TIsSame<NumberType, uint8>().Value ||
			TIsSame<NumberType, int16>().Value ||
			TIsSame<NumberType, uint16>().Value ||
			TIsSame<NumberType, int32>().Value ||
			TIsSame<NumberType, uint32>().Value ||
			TIsSame<NumberType, int64>().Value ||
			TIsSame<NumberType, uint64>().Value ||
			TIsSame<NumberType, float>().Value,
			"This widget only support number types with float not double!"
			);
	}	

	DECLARE_DELEGATE_OneParam(FOnValueChanged, const NumberType&);
	DECLARE_DELEGATE_TwoParams(FOnValueCommitted, const NumberType&, const ETextCommit::Type);

	SLATE_BEGIN_ARGS(SSpinNumber<NumberType>)
		: _Style(&FShooterStyle::Get().GetWidgetStyle<FSpinNumberStyle>("SSpinNumberStyle"))
		, _Delta(1)
		, _MinValue(0)
		, _MaxValue(10)
		, _InitValue(1)
	{}
	SLATE_STYLE_ARGUMENT(FSpinNumberStyle, Style)
	SLATE_ARGUMENT(NumberType, InitValue)
	SLATE_ATTRIBUTE(NumberType, Delta)
	SLATE_ATTRIBUTE(NumberType, MinValue)
	SLATE_ATTRIBUTE(NumberType, MaxValue)
	SLATE_EVENT(FOnValueChanged, OnValueChanged)
	SLATE_EVENT(FOnValueCommitted, OnValueCommitted)
	SLATE_ARGUMENT(TSharedPtr<INumericTypeInterface<NumberType>>, TypeInterface)
	SLATE_END_ARGS()

	BEGIN_SLATE_FUNCTION_BUILD_OPTIMIZATION
	void Construct(const FArguments& InArgs)
	{
		Style = InArgs._Style;
		OnValueChanged = InArgs._OnValueChanged;
		OnValueCommitted = InArgs._OnValueCommitted;
		Delta = InArgs._Delta;
		MinValue = InArgs._MinValue;
		MaxValue = InArgs._MaxValue;
		CurrentValue = InArgs._InitValue;
		Interface = InArgs._TypeInterface.IsValid() ? InArgs._TypeInterface : MakeShareable(new TDefaultNumericTypeInterface<NumberType>);

		bDragging = false;
		PointerDraggingSliderIndex = INDEX_NONE;

		ChildSlot
		[
			SNew(SHorizontalBox)
			+ SHorizontalBox::Slot()
			.Padding(Style->TextPadding)
			.FillWidth(1.0f)
			.HAlign(HAlign_Fill)
			.VAlign(VAlign_Center)
			[
				SAssignNew(Widget_TextBlock, STextBlock)
				.Text(this, &SSpinNumber<NumberType>::GetCurrentValueAsText)
				.TextStyle(&Style->TextBlock)
			]
			+ SHorizontalBox::Slot()
			.Padding(Style->TextPadding)
			.FillWidth(1.0f)
			.HAlign(HAlign_Fill)
			.VAlign(VAlign_Center)
			[
				SAssignNew(Widget_EditableText, SEditableText)
				.Style(&Style->EditableText)
				.Visibility(EVisibility::Collapsed)
				.SelectAllTextWhenFocused(true)
				.Text(this, &SSpinNumber<NumberType>::GetCurrentValueAsText)
				.OnIsTypedCharValid(this, &SSpinNumber<NumberType>::IsCharacterValid)
				.OnTextChanged(this, &SSpinNumber<NumberType>::TextField_OnTextChanged)
				.OnTextCommitted(this, &SSpinNumber<NumberType>::TextField_OnTextCommitted)
				.ClearKeyboardFocusOnCommit(true)
				.SelectAllTextOnCommit(false)
				.VirtualKeyboardType(EKeyboardType::Keyboard_Number)
			]
			+ SHorizontalBox::Slot()
			.AutoWidth()
			.HAlign(HAlign_Fill)
			.VAlign(VAlign_Center)
			[
				SNew(SImage)
				.Image(&Style->ArrowsImage)
			]
		];
	}
	END_SLATE_FUNCTION_BUILD_OPTIMIZATION

	void SetValue(const NumberType Value)
	{
		CurrentValue = FMath::Clamp(Value, MinValue.Get(), MaxValue.Get());
		OnValueChanged.ExecuteIfBound(CurrentValue);
	}

	NumberType GetValue() const
	{
		return CurrentValue;
	}

	void ToggleTextMode(const bool InToggle)
	{
		Widget_TextBlock->SetVisibility(InToggle ? EVisibility::Collapsed : EVisibility::Visible);
		Widget_EditableText->SetVisibility(!InToggle ? EVisibility::Collapsed : EVisibility::Visible);
	}

	virtual bool SupportsKeyboardFocus() const override
	{
		return true;
	}

	virtual bool HasKeyboardFocus() const override
	{
		return SCompoundWidget::HasKeyboardFocus() || (Widget_EditableText.IsValid() && Widget_EditableText->HasKeyboardFocus());
	}

	virtual FCursorReply OnCursorQuery(const FGeometry& MyGeometry, const FPointerEvent& CursorEvent) const override
	{
		return bDragging ?
			FCursorReply::Cursor(EMouseCursor::None) :
			FCursorReply::Cursor(EMouseCursor::ResizeLeftRight);
	}

protected:

	const FSpinNumberStyle* Style;

	FOnValueChanged OnValueChanged;
	FOnValueCommitted OnValueCommitted;

	TSharedPtr<INumericTypeInterface<NumberType>> Interface;
	NumberType CurrentValue;

	bool bDragging;
	FIntPoint CachedMousePosition;
	int32 PointerDraggingSliderIndex;
	float DistanceDragged;

	virtual FReply OnFocusReceived(const FGeometry& MyGeometry, const FFocusEvent& InFocusEvent) override
	{
		if (!bDragging && (InFocusEvent.GetCause() == EFocusCause::Navigation || InFocusEvent.GetCause() == EFocusCause::SetDirectly))
		{
			ToggleTextMode(true);
			return FReply::Handled().SetUserFocus(Widget_EditableText.ToSharedRef(), InFocusEvent.GetCause());
		}
		else
		{
			return FReply::Unhandled();
		}
	}

	virtual FReply OnMouseButtonDown(const FGeometry& MyGeometry, const FPointerEvent& MouseEvent) override
	{
		if (MouseEvent.GetEffectingButton() == EKeys::LeftMouseButton && PointerDraggingSliderIndex == INDEX_NONE)
		{
			DistanceDragged = .0f;
			PointerDraggingSliderIndex = MouseEvent.GetPointerIndex();
			CachedMousePosition = MouseEvent.GetScreenSpacePosition().IntPoint();
			return FReply::Handled().CaptureMouse(SharedThis(this)).UseHighPrecisionMouseMovement(SharedThis(this)).SetUserFocus(SharedThis(this), EFocusCause::Mouse);
		}
		else
		{

			return FReply::Unhandled();
		}
	}

	virtual FReply OnMouseMove(const FGeometry& MyGeometry, const FPointerEvent& MouseEvent) override
	{
		if (PointerDraggingSliderIndex == MouseEvent.GetPointerIndex() && this->HasMouseCapture())
		{
			if (bDragging)
			{
				const float MoveDelta = MouseEvent.GetCursorDelta().X;		
				const double CorrectMoveDelta = FMath::GridSnap(double(MoveDelta), double(Delta.Get()));
				SetValue(CurrentValue + CorrectMoveDelta);
			}
			else
			{
				DistanceDragged += FMath::Abs(MouseEvent.GetCursorDelta().X);
				if (DistanceDragged > FSlateApplication::Get().GetDragTriggerDistance())
				{
					ToggleTextMode(false);
					bDragging = true;
				}

				CachedMousePosition = MouseEvent.GetScreenSpacePosition().IntPoint();
			}
			return FReply::Handled();
		}

		return FReply::Unhandled();
	}

	virtual FReply OnMouseButtonUp(const FGeometry& MyGeometry, const FPointerEvent& MouseEvent) override
	{
		if (MouseEvent.GetEffectingButton() == EKeys::LeftMouseButton && PointerDraggingSliderIndex == MouseEvent.GetPointerIndex() && this->HasMouseCapture())
		{
			if (bDragging)
			{
				NumberType CurrentDelta = Delta.Get();
				if (CurrentDelta != 0)
				{
					CurrentValue = FMath::GridSnap(double(CurrentValue), double(CurrentDelta));
				}
			}

			bDragging = false;
			PointerDraggingSliderIndex = INDEX_NONE;

			FReply Reply = FReply::Handled().ReleaseMouseCapture();

			if (!MouseEvent.IsTouchEvent())
			{
				Reply.SetMousePos(CachedMousePosition);
			}

			if (DistanceDragged < FSlateApplication::Get().GetDragTriggerDistance())
			{
				ToggleTextMode(true);
				Reply.SetUserFocus(Widget_EditableText.ToSharedRef(), EFocusCause::SetDirectly);
			}

			OnValueCommitted.ExecuteIfBound(CurrentValue, ETextCommit::OnEnter);

			return Reply;
		}
		else
		{
			return FReply::Unhandled();
		}
	}

	virtual int32 OnPaint(const FPaintArgs& Args, const FGeometry& AllottedGeometry, const FSlateRect& MyCullingRect, FSlateWindowElementList& OutDrawElements, int32 LayerId, const FWidgetStyle& InWidgetStyle, bool bParentEnabled) const override
	{
		const bool bActiveFeedback = IsHovered() || bDragging;

		const FSlateBrush* BackgroundImage = bActiveFeedback ?
			&Style->HoveredBackgroundBrush :
			&Style->BackgroundBrush;

		const FSlateBrush* FillImage = bActiveFeedback ?
			&Style->ActiveFillBrush :
			&Style->InactiveFillBrush;

		const int32 BackgroundLayer = LayerId;

		const bool bEnabled = ShouldBeEnabled(bParentEnabled);
		const ESlateDrawEffect DrawEffects = bEnabled ? ESlateDrawEffect::None : ESlateDrawEffect::DisabledEffect;

		FSlateDrawElement::MakeBox(
			OutDrawElements,
			BackgroundLayer,
			AllottedGeometry.ToPaintGeometry(),
			BackgroundImage,
			DrawEffects,
			BackgroundImage->GetTint(InWidgetStyle) * InWidgetStyle.GetColorAndOpacityTint()
		);

		const int32 FilledLayer = BackgroundLayer + 1;

		NumberType Value = CurrentValue;
		NumberType CurrentDelta = Delta.Get();		
		if (FMath::IsNearlyZero(double(CurrentDelta)) == false)
		{
			double Result = FMath::GridSnap(double(Value), double(CurrentDelta));
			Value = FMath::Clamp<double>(Result, TNumericLimits<NumberType>::Lowest(), TNumericLimits<NumberType>::Max());
		}

		const double HalfMax = MaxValue.Get()*0.5;
		const double HalfMin = MinValue.Get()*0.5;
		const double HalfVal = Value*0.5;

		float FractionFilled = (float)FMath::Clamp((HalfVal - HalfMin) / (HalfMax - HalfMin), 0.0, 1.0);
		const FVector2D FillSize(AllottedGeometry.GetLocalSize().X * FractionFilled, AllottedGeometry.GetLocalSize().Y);

		if (Widget_TextBlock->GetVisibility().IsVisible())
		{
			FSlateDrawElement::MakeBox(
				OutDrawElements,
				FilledLayer,
				AllottedGeometry.ToPaintGeometry(FVector2D(0, 0), FillSize),
				FillImage,
				DrawEffects,
				FillImage->GetTint(InWidgetStyle) * InWidgetStyle.GetColorAndOpacityTint()
			);
		}

		return FMath::Max(FilledLayer, SCompoundWidget::OnPaint(Args, AllottedGeometry, MyCullingRect, OutDrawElements, FilledLayer, InWidgetStyle, bEnabled));
	}

	//void OnValueChangedEvent(float Value)
	//{
	//	const NumberType sMin = SliderMinimum.Get();
	//	const NumberType sMax = SliderMaximum.Get();
	//
	//	CurrentValue = FMath::Lerp<NumberType>(sMin, sMax, Value);
	//	OnValueChanged.ExecuteIfBound(CurrentValue);
	//}

	//TOptional<float> GetCurrentValue() const
	//{
	//	return float(CurrentValue) / float(SliderMaximum.Get());
	//}

	FString GetCurrentValueAsString() const
	{
		return Interface->ToString(CurrentValue);
	}

	FText GetCurrentValueAsText() const
	{
		return FText::FromString(GetCurrentValueAsString());
	}

	bool IsCharacterValid(TCHAR InChar) const
	{
		return Interface->IsCharacterValid(InChar);
	}

	void TextField_OnTextChanged(const FText& NewText)
	{
		FString Data = NewText.ToString();
		int32 NumValidChars = Data.Len();

		for (int32 Index = 0; Index < Data.Len(); ++Index)
		{
			if (!Interface->IsCharacterValid(Data[Index]))
			{
				NumValidChars = Index;
				break;
			}
		}

		if (NumValidChars < Data.Len())
		{
			FString ValidData = NumValidChars > 0 ? Data.Left(NumValidChars) : GetCurrentValueAsString();
			Widget_EditableText->SetText(FText::FromString(ValidData));
		}
	}

	void TextField_OnTextCommitted(const FText& NewText, ETextCommit::Type CommitInfo)
	{
		if (CommitInfo != ETextCommit::OnEnter)
		{
			ToggleTextMode(false);
		}

		TOptional<NumberType> NewValue = Interface->FromString(NewText.ToString(), CurrentValue);
		if (NewValue.IsSet())
		{
			SetValue(NewValue.GetValue());
		}
	}

	TAttribute<NumberType> Delta, MinValue, MaxValue;

	TSharedPtr<STextBlock> Widget_TextBlock;
	TSharedPtr<SEditableText> Widget_EditableText;
};
