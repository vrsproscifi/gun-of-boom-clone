// Fill out your copyright notice in the Description page of Project Settings.

#include "ShooterGame.h"
#include "SKillList.h"
#include "SlateOptMacros.h"
#include "SScaleBox.h"
#include "TeamColorsData.h"
#include "ShooterGameSingleton.h"
#include "GameState/ShooterPlayerState.h"
#include "BasicItemEntity.h"
#include "SRetainerWidget.h"

class SHOOTERGAME_API SKillListItem : public SCompoundWidget
{
public:
	SLATE_BEGIN_ARGS(SKillListItem)
		: _Style(&FShooterStyle::Get().GetWidgetStyle<FKillListStyle>("SKillListStyle"))
	{}
	SLATE_STYLE_ARGUMENT(FKillListStyle, Style)
	SLATE_ARGUMENT(TWeakObjectPtr<AShooterPlayerState>, Killer)
	SLATE_ARGUMENT(TWeakObjectPtr<AShooterPlayerState>, Killed)
	SLATE_ARGUMENT(TWeakObjectPtr<AShooterPlayerState>, Local)
	SLATE_ARGUMENT(int32, WeaponModel)
	SLATE_END_ARGS()

	BEGIN_SLATE_FUNCTION_BUILD_OPTIMIZATION
	void Construct(const FArguments& InArgs)
	{
		Style = InArgs._Style;
		Killer = InArgs._Killer;
		Killed = InArgs._Killed;
		Local = InArgs._Local;

		const FSlateBrush* TargetImage = new FSlateNoResource();
		if (auto FoundItem = UShooterGameSingleton::Get()->FindItemById(InArgs._WeaponModel))
		{
			TargetImage = &FoundItem->GetItemProperty().Icon;
		}

		ChildSlot
		[
			SNew(SRetainerWidget)
			.Phase(0)
			.PhaseCount(10)
			.RenderOnPhase(true)
			.RenderOnInvalidation(false)
			[
				SNew(SHorizontalBox)
				+ SHorizontalBox::Slot().AutoWidth().VAlign(VAlign_Center) // Killer
				[
					SNew(STextBlock)
					.TextStyle(&Style->PlayerText)
					.Justification(ETextJustify::Right)
					.Text(this, &SKillListItem::GetPlayerText, true)
					.ColorAndOpacity(this, &SKillListItem::GetPlayerColor, true)
				]
				+ SHorizontalBox::Slot().AutoWidth().VAlign(VAlign_Center) // Icon
				[
					SNew(SBox).WidthOverride(100).HeightOverride(50)
					[
						SNew(SScaleBox)
						.Stretch(EStretch::ScaleToFitX)
						.VAlign(VAlign_Center)
						.StretchDirection(EStretchDirection::Both)
						[
							SNew(SImage)
							.Image(TargetImage)
							.RenderTransform(FSlateRenderTransform(FScale2D(-1.0f, 1.0f)))
							.RenderTransformPivot(FVector2D(.5f, .5f))
						]
					]
				]
				+ SHorizontalBox::Slot().AutoWidth().VAlign(VAlign_Center) // Killed
				[
					SNew(STextBlock)
					.TextStyle(&Style->PlayerText)
					.Justification(ETextJustify::Left)
					.Text(this, &SKillListItem::GetPlayerText, false)
					.ColorAndOpacity(this, &SKillListItem::GetPlayerColor, false)
				]
			]
		];

		auto Assets = UShooterGameSingleton::Get()->GetDataAssets<UTeamColorsData>();
		if (Assets.Num())
		{
			TeamColorsPtr = Assets[0];
		}
	}
	END_SLATE_FUNCTION_BUILD_OPTIMIZATION

protected:

	const FKillListStyle* Style;

	TWeakObjectPtr<AShooterPlayerState> Killer, Killed, Local;
	TWeakObjectPtr<UTeamColorsData> TeamColorsPtr;

	FText GetPlayerText(bool IsKiller) const
	{
		if (IsKiller)
		{
			return Killer.IsValid() && Killer->IsValidLowLevel() ? FText::FromString(Killer->GetPlayerName()) : FText::GetEmpty();
		}
		else
		{
			return Killed.IsValid() && Killed->IsValidLowLevel() ? FText::FromString(Killed->GetPlayerName()) : FText::GetEmpty();
		}
	}

	FSlateColor GetPlayerColor(bool IsKiller) const
	{
		if (TeamColorsPtr.IsValid())
		{
			if (IsLocal(IsKiller))
			{
				return TeamColorsPtr->GetLocalColor();
			}
			else
			{
				return IsFriendly(IsKiller) ? TeamColorsPtr->GetFriendlyColor() : TeamColorsPtr->GetEnemyColor();
			}
		}

		return FLinearColor::White;
	}

	bool IsLocal(bool IsKiller) const
	{
		return IsKiller ? Killer == Local : Killed == Local;
	}

	bool IsFriendly(bool IsKiller) const
	{
		if (IsValidObject(Local))
		{
			if (IsKiller)
			{
				return Killer.IsValid() && Killer->IsValidLowLevel() && Killer->GetTeam() == Local->GetTeam();
			}
			else
			{
				return Killed.IsValid() && Killed->IsValidLowLevel() && Killed->GetTeam() == Local->GetTeam();
			}
		}
		return false;
	}
};


BEGIN_SLATE_FUNCTION_BUILD_OPTIMIZATION
void SKillList::Construct(const FArguments& InArgs)
{
	LocalState = InArgs._LocalState;

	ChildSlot.HAlign(HAlign_Right).VAlign(VAlign_Top)
	[
		SAssignNew(BoxContainer, SVerticalBox)
	];
}
END_SLATE_FUNCTION_BUILD_OPTIMIZATION

void SKillList::AddRow(AShooterPlayerState* InKilled, AShooterPlayerState* InKiller, const int32& InWeaponId)
{
	auto NewWidget = 
		SNew(SKillListItem)
		.Local(LocalState)
		.Killer(InKiller)
		.Killed(InKilled)
		.WeaponModel(InWeaponId);

	BoxContainer->AddSlot().AutoHeight()
		[
			NewWidget
		];

	RegisterActiveTimer(5.0f, FWidgetActiveTimerDelegate::CreateLambda([&, w = NewWidget] (double /*InCurrentTime*/, float /*InDeltaTime*/) -> EActiveTimerReturnType
	{
		if (BoxContainer.IsValid())
		{
			BoxContainer->RemoveSlot(w);
		}

		return EActiveTimerReturnType::Stop;
	}));
}