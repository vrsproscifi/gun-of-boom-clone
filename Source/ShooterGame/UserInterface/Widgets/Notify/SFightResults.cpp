// Fill out your copyright notice in the Description page of Project Settings.

#include "ShooterGame.h"
#include "SFightResults.h"
#include "SlateOptMacros.h"
#include "Utilities/SAnimatedBackground.h"
#include "Localization/TableBaseStrings.h"
#include "ProgressComponent/GameMatchMemberInformation.h"
#include "ProgressComponent/GameMatchInformation.h"
#include "ShooterGameAchievements.h"
#include "Utilities/SlateUtilityTypes.h"
#include "GameSingletonExtensions.h"
#include "SScaleBox.h"
#include "SlateMaterialBrush.h"
#include "WorldFlagsData.h"

FName SFightResults::Column_Name		= TEXT("pName");
FName SFightResults::Column_Kills		= TEXT("pKills");
FName SFightResults::Column_Deaths		= TEXT("pDeaths");
FName SFightResults::Column_Score		= TEXT("pScore");
FName SFightResults::Column_Elo			= TEXT("pElo");

float SFightResults::FillColumn_Name	= 1.0f;
float SFightResults::FillColumn_Kills	= 0.2f;
float SFightResults::FillColumn_Deaths	= 0.2f;
float SFightResults::FillColumn_Score	= 0.3f;
float SFightResults::FillColumn_Elo		= 0.3f;

class SHOOTERGAME_API SFightResults_Item : public SMultiColumnTableRow<TSharedPtr<FGameMatchMemberInformation>>
{
public:
	SLATE_BEGIN_ARGS(SFightResults_Item)
		: _Style(&FShooterStyle::Get().GetWidgetStyle<FFightResultsStyle>("SFightResultsStyle"))
	{}
	SLATE_STYLE_ARGUMENT(FFightResultsStyle, Style)
	SLATE_END_ARGS()

	BEGIN_SLATE_FUNCTION_BUILD_OPTIMIZATION
	void Construct(const FArguments& InArgs, const TSharedRef<STableViewBase>& OwnerTableView, const TSharedRef<FGameMatchMemberInformation>& InItem)
	{
		SCOPED_SUSPEND_RENDERING_THREAD(false);

		Style = InArgs._Style;
		Instance = InItem;

		auto FlagsData = FGameSingletonExtension::GetDataAssets<UWorldFlagsData>();
		if (FlagsData.Num() && FlagsData.IsValidIndex(0) && IsValidObject(FlagsData[0]))
		{
			FlagBrush = FSlateMaterialBrush(*FlagsData[0]->GetFlagMaterialByCode(Instance->Country), FVector2D(128, 128));
		}
		else
		{
			FlagBrush = FSlateNoResource();
		}

		const bool IsLocal = FGameSingletonExtension::GetLastLocalPlayerId() == Instance->PlayerId;

		auto SuperArgs = FSuperRowType::FArguments().Style(IsLocal ? &Style->RowTableLocal : &Style->RowTable).Padding(FMargin(2, 6));

		FSuperRowType::Construct(SuperArgs, OwnerTableView);
	}	
	END_SLATE_FUNCTION_BUILD_OPTIMIZATION

protected:

	const FFightResultsStyle* Style;

	TSharedRef<SWidget> GenerateWidgetForColumn(const FName& InColumnName) override
	{
		FPlayerGameAchievements Achievements(Instance->Achievements);

		if (SFightResults::Column_Name.IsEqual(InColumnName))
		{
			return SNew(SBox).VAlign(VAlign_Center)
				[
					SNew(SHorizontalBox)
					+ SHorizontalBox::Slot().AutoWidth()
					[
						SNew(SBox).WidthOverride(80).HeightOverride(50)
						[
							SNew(SScaleBox).Stretch(EStretch::ScaleToFit)
							[
								SNew(SImage).Image(&FlagBrush)
							]
						]
					]
					+ SHorizontalBox::Slot().VAlign(VAlign_Center).Padding(5, 0, 0, 0)
					[
						SNew(STextBlock).Text(FText::FromString(Instance->PlayerName)).TextStyle(&Style->RowName)
					]
				];
		}
		else if (SFightResults::Column_Kills.IsEqual(InColumnName))
		{
			if (auto AchivKills = Achievements.GetAchievementById(EShooterGameAchievements::Kills))
			{
				return SNew(STextBlock).Text(FText::AsNumber(AchivKills->Value, &FNumberFormattingOptions::DefaultNoGrouping())).TextStyle(&Style->RowNumber);
			}

			return SNew(STextBlock).Text(FText::AsNumber(0, &FNumberFormattingOptions::DefaultNoGrouping())).TextStyle(&Style->RowNumber);
		}
		else if (SFightResults::Column_Deaths.IsEqual(InColumnName))
		{
			if (auto AchivDeaths = Achievements.GetAchievementById(EShooterGameAchievements::Deads))
			{
				return SNew(STextBlock).Text(FText::AsNumber(AchivDeaths->Value, &FNumberFormattingOptions::DefaultNoGrouping())).TextStyle(&Style->RowNumber);
			}

			return SNew(STextBlock).Text(FText::AsNumber(0, &FNumberFormattingOptions::DefaultNoGrouping())).TextStyle(&Style->RowNumber);
		}
		else if (SFightResults::Column_Score.IsEqual(InColumnName))
		{
			if (auto AchivScore = Achievements.GetAchievementById(EShooterGameAchievements::Score))
			{
				return SNew(STextBlock).Text(FText::AsNumber(AchivScore->Value, &FNumberFormattingOptions::DefaultNoGrouping())).TextStyle(&Style->RowNumber);
			}

			return SNew(STextBlock).Text(FText::AsNumber(0, &FNumberFormattingOptions::DefaultNoGrouping())).TextStyle(&Style->RowNumber);
		}
		else if (SFightResults::Column_Elo.IsEqual(InColumnName))
		{
			return SNew(STextBlock).Text(FText::AsNumber(Instance->EloRatingScore, &FNumberFormattingOptions::DefaultNoGrouping())).TextStyle(&Style->RowNumber);
		}

		return SNullWidget::NullWidget;
	}

	TSharedPtr<FGameMatchMemberInformation> Instance;
	FSlateBrush FlagBrush;
};

BEGIN_SLATE_FUNCTION_BUILD_OPTIMIZATION
void SFightResults::Construct(const FArguments& InArgs)
{
	Style = InArgs._Style;
	OnPlayerSelected = InArgs._OnPlayerSelected;

	ChildSlot
	[
		SAssignNew(Back_Animation, SAnimatedBackground)
		.Visibility(EVisibility::Visible)
		.IsEnabledBlur(true)
		.Duration(.2f)
		.ShowAnimation(EAnimBackAnimation::UpFade)
		.HideAnimation(EAnimBackAnimation::DownFade)
		.InitAsHide(true)
		[
			SNew(SVerticalBox)
			+ SVerticalBox::Slot().AutoHeight().VAlign(VAlign_Center).HAlign(HAlign_Fill)
			[
				SNew(SHorizontalBox)
				+ SHorizontalBox::Slot().VAlign(VAlign_Center)
				[
					SAssignNew(Widget_LocalScore, STextBlock)
					.Justification(ETextJustify::Right)
					.Margin(FMargin(60, 0))
					.TextStyle(&Style->ScoreText)
				]
				+ SHorizontalBox::Slot().AutoWidth()
				[
					SAssignNew(Widget_WinStatus, STextBlock)
					.Margin(10)
					.TextStyle(&Style->WinStatusText)
				]
				+ SHorizontalBox::Slot().VAlign(VAlign_Center)
				[
					SAssignNew(Widget_EnemyScore, STextBlock)
					.Justification(ETextJustify::Left)
					.Margin(FMargin(60, 0))
					.TextStyle(&Style->ScoreText)
				]
			]
			+ SVerticalBox::Slot()
			[
				SNew(SHorizontalBox)
				+ SHorizontalBox::Slot().Padding(20, 20, 10, 80)
				[
					SAssignNew(PlayersTable_Friendly, SListView<TSharedPtr<FGameMatchMemberInformation>>)
					.ListItemsSource(&PlayersList_Friendly)
					.ScrollbarVisibility(EVisibility::Collapsed)
					.AllowOverscroll(EAllowOverscroll::Yes)
					.SelectionMode(ESelectionMode::Single)
					.HeaderRow
					(
						SNew(SHeaderRow).Style(&Style->RowHeader)
						+ SHeaderRow::Column(Column_Name).FillWidth(FillColumn_Name)		[SNew(STextBlock).TextStyle(&Style->RowHeaderText).Text(FText::FromString("Name"))]
						+ SHeaderRow::Column(Column_Kills).FillWidth(FillColumn_Kills)		[SNew(STextBlock).TextStyle(&Style->RowHeaderText).Text(FText::FromString("K"))]
						+ SHeaderRow::Column(Column_Deaths).FillWidth(FillColumn_Deaths)	[SNew(STextBlock).TextStyle(&Style->RowHeaderText).Text(FText::FromString("D"))]
						+ SHeaderRow::Column(Column_Score).FillWidth(FillColumn_Score)		[SNew(STextBlock).TextStyle(&Style->RowHeaderText).Text(FText::FromString("S"))]
						+ SHeaderRow::Column(Column_Elo).FillWidth(FillColumn_Elo)			[SNew(STextBlock).TextStyle(&Style->RowHeaderText).Text(FText::FromString("ELO"))]
					)
					.OnGenerateRow_Lambda([&](TSharedPtr<FGameMatchMemberInformation> InItem, const TSharedRef<STableViewBase>& OwnerTableView) -> TSharedRef<ITableRow>
					{
						return SNew(SFightResults_Item, OwnerTableView, InItem.ToSharedRef());
					})
					.OnSelectionChanged_Lambda([&](TSharedPtr<FGameMatchMemberInformation> InItem, ESelectInfo::Type InInfo)
					{
						if (InItem.IsValid())
						{
							OnPlayerSelected.ExecuteIfBound(InItem->PlayerId);
							PlayersTable_Friendly->SetItemSelection(InItem, false);
						}
					})
				]
				+ SHorizontalBox::Slot().Padding(10, 20, 20, 80)
				[
					SAssignNew(PlayersTable_Enemy, SListView<TSharedPtr<FGameMatchMemberInformation>>)
					.ListItemsSource(&PlayersList_Enemy)
					.ScrollbarVisibility(EVisibility::Collapsed)
					.AllowOverscroll(EAllowOverscroll::Yes)
					.SelectionMode(ESelectionMode::Single)
					.HeaderRow
					(
						SNew(SHeaderRow).Style(&Style->RowHeader)
						+ SHeaderRow::Column(Column_Name).FillWidth(FillColumn_Name)		[SNew(STextBlock).TextStyle(&Style->RowHeaderText).Text(FText::FromString("Name"))]
						+ SHeaderRow::Column(Column_Kills).FillWidth(FillColumn_Kills)		[SNew(STextBlock).TextStyle(&Style->RowHeaderText).Text(FText::FromString("K"))]
						+ SHeaderRow::Column(Column_Deaths).FillWidth(FillColumn_Deaths)	[SNew(STextBlock).TextStyle(&Style->RowHeaderText).Text(FText::FromString("D"))]
						+ SHeaderRow::Column(Column_Score).FillWidth(FillColumn_Score)		[SNew(STextBlock).TextStyle(&Style->RowHeaderText).Text(FText::FromString("S"))]
						+ SHeaderRow::Column(Column_Elo).FillWidth(FillColumn_Elo)			[SNew(STextBlock).TextStyle(&Style->RowHeaderText).Text(FText::FromString("ELO"))]
					)
					.OnGenerateRow_Lambda([&](TSharedPtr<FGameMatchMemberInformation> InItem, const TSharedRef<STableViewBase>& OwnerTableView) -> TSharedRef<ITableRow>
					{
						return SNew(SFightResults_Item, OwnerTableView, InItem.ToSharedRef());
					})
					.OnSelectionChanged_Lambda([&](TSharedPtr<FGameMatchMemberInformation> InItem, ESelectInfo::Type InInfo)
					{
						if (InItem.IsValid())
						{
							OnPlayerSelected.ExecuteIfBound(InItem->PlayerId);
							PlayersTable_Enemy->SetItemSelection(InItem, false);
						}
					})
				]
			]
			+ SVerticalBox::Slot().AutoHeight().Padding(20, 0, 20, 20).HAlign(HAlign_Center).VAlign(VAlign_Center)
			[
				SNew(SButton)
				.ContentPadding(FMargin(20))
				.TextStyle(&Style->TextContinue)
				.ButtonStyle(&FShooterStyle::Get().GetWidgetStyle<FButtonStyle>("Default_ActionButton"))
				.HAlign(HAlign_Center)
				.VAlign(VAlign_Center)
				.Text(FTableBaseStrings::GetBaseText(EBaseStrings::Continue))
				.OnClicked_Lambda([&]()
				{
					HideWidget();
					return FReply::Handled();
				})
			]
		]
	];
}
END_SLATE_FUNCTION_BUILD_OPTIMIZATION

void SFightResults::SetInformation(const FGameMatchInformation& InInformation, const FGuid& InLocalPlayerId)
{
	PlayersList_Friendly.Empty();
	PlayersList_Enemy.Empty();

	//FPlayerGameAchievements(A.Achievements).ContainsAchievement()

	auto CurrentMember = InInformation.Members.FindByPredicate([&](const FGameMatchMemberInformation& InMember)
	{
		return InMember.PlayerId == InLocalPlayerId;
	});

	if (CurrentMember)
	{
		CachedIsWin = false;

		if (FPlayerGameAchievements(CurrentMember->Achievements).ContainsAchievement(EShooterGameAchievements::MatchesWins))
		{
			Widget_WinStatus->SetText(NSLOCTEXT("SFightResults", "SFightResults.Win", "The Victory"));
			CachedIsWin = true;			
		}
		else if (FPlayerGameAchievements(CurrentMember->Achievements).ContainsAchievement(EShooterGameAchievements::MatchesLoses))
		{
			Widget_WinStatus->SetText(NSLOCTEXT("SFightResults", "SFightResults.Lose", "The Defeat"));
		}
		else
		{
			Widget_WinStatus->SetText(NSLOCTEXT("SFightResults", "SFightResults.Draw", "The Draw"));
		}
	}
	else
	{
		Widget_WinStatus->SetText(NSLOCTEXT("SFightResults", "SFightResults.Draw", "The Draw"));
	}

	auto Members = InInformation.Members;
	Members.Sort([](const FGameMatchMemberInformation& A, const FGameMatchMemberInformation& B)
	{
		const auto ScoreA = FPlayerGameAchievements(A.Achievements).GetAchievementById(EShooterGameAchievements::Score);
		const auto ScoreB = FPlayerGameAchievements(B.Achievements).GetAchievementById(EShooterGameAchievements::Score);

		if (ScoreA && ScoreB)
		{
			return ScoreA->Value > ScoreB->Value;
		}

		return false;
	});

	for (auto Member : Members)
	{
		if (Member.TeamId == InInformation.OwnerTeamId)
		{
			PlayersList_Friendly.Add(MakeShareable(new auto(Member)));
		}
		else
		{
			PlayersList_Enemy.Add(MakeShareable(new auto(Member)));
		}
	}

	for (auto ScorePair : InInformation.Scores)
	{
		if (ScorePair.Key == InInformation.OwnerTeamId)
		{
			Widget_LocalScore->SetText(FText::AsNumber(ScorePair.Value, &FNumberFormattingOptions::DefaultNoGrouping()));
		}
		else
		{
			Widget_EnemyScore->SetText(FText::AsNumber(ScorePair.Value, &FNumberFormattingOptions::DefaultNoGrouping()));
		}
	}

	PlayersTable_Friendly->RequestListRefresh();
	PlayersTable_Enemy->RequestListRefresh();
}

void SFightResults::ToggleWidget(const bool InToggle)
{
	if (InToggle)
	{
		if (FWidgetOrderedSupportQueue::GetCurrentQueueId() == TEXT("SFightResults"))
		{
			SUsableCompoundWidget::ToggleWidget(InToggle);
			Back_Animation->ToggleWidget(InToggle);
			PlayWinLoseSound();
		}

		FWidgetOrderedSupportQueue::AddQueue(TEXT("SFightResults"), FOnClickedOutside::CreateLambda([&, t = InToggle]() {
			SUsableCompoundWidget::ToggleWidget(t);
			Back_Animation->ToggleWidget(t);
			PlayWinLoseSound();
		}), ORDERED_QUEUE_FIGHTRESULTS, true);
	}
	else
	{
		SUsableCompoundWidget::ToggleWidget(InToggle);
		Back_Animation->ToggleWidget(InToggle);
	}
}

bool SFightResults::IsInInteractiveMode() const
{
	return Back_Animation->IsAnimAtEnd();
}

void SFightResults::HideWidget()
{
	SUsableCompoundWidget::HideWidget();
	FWidgetOrderedSupportQueue::RemoveQueue(TEXT("SFightResults"));
}

void SFightResults::PlayWinLoseSound()
{
	if (CachedIsWin)
	{
		FSlateApplication::Get().PlaySound(Style->SoundWin);
	}
	else
	{
		FSlateApplication::Get().PlaySound(Style->SoundLose);
	}
}
