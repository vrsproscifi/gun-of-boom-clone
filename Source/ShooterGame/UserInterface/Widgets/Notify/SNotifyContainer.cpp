// Fill out your copyright notice in the Description page of Project Settings.

#include "ShooterGame.h"
#include "SNotifyContainer.h"
#include "SlateOptMacros.h"
#include "Utilities/SAnimatedBackground.h"
#include "Decorators/LokaTextDecorator.h"
#include "Decorators/LokaResourceDecorator.h"
#include "Private/Application/ActiveTimerHandle.h"

BEGIN_SLATE_FUNCTION_BUILD_OPTIMIZATION
void SNotifyContainer::Construct(const FArguments& InArgs)
{
	Style = InArgs._Style;

	ChildSlot.VAlign(VAlign_Top).HAlign(HAlign_Center)
	[
		SAssignNew(Anim_Back, SAnimatedBackground)
		.ShowAnimation(EAnimBackAnimation::UpFade)
		.HideAnimation(EAnimBackAnimation::UpFade)
		.Duration(.5f)
		.IsRelative(true)
		.IsEnabledBlur(false)
		.WithColor(false)
		[
			SNew(SBox).MinDesiredHeight(60)
			[
				SNew(SBorder).Padding(FMargin(40, 0)).BorderImage(&Style->Background).VAlign(VAlign_Center)
				[
					SAssignNew(Widget_Content, SRichTextBlock)
					.AutoWrapText(true)
					.TextStyle(&Style->MessageText)
					.Justification(ETextJustify::Center)
					.DecoratorStyleSet(&FShooterStyle::Get())
					+ SRichTextBlock::Decorator(FLokaTextDecorator::Create(Style->MessageText))
					+ SRichTextBlock::Decorator(FLokaResourceDecorator::Create())
					+ SRichTextBlock::Decorator(FHyperlinkDecorator::Create(TEXT(""), FSlateHyperlinkRun::FOnClick::CreateLambda([&](const FSlateHyperlinkRun::FMetadata& InData) {
						if (InData.Contains("url")) FPlatformProcess::LaunchURL(*InData.FindChecked("url"), nullptr, nullptr);
					})))
				]
			]
		]
	];

	ResetDisplayTime();
}
END_SLATE_FUNCTION_BUILD_OPTIMIZATION


void SNotifyContainer::ShowNotify(const FName& InId, const FString& InMessage)
{
	ShowNotify(InId, FText::FromString(InMessage));
}

void SNotifyContainer::ShowNotify(const FName& InId, const FText& InMessage)
{
	QueueBegin(SNotifyContainer, InId, InMessage)
		SNotifyContainer::Get()->SetContent(InMessage);
		SNotifyContainer::Get()->ToggleWidget(true);
	QueueEnd
}

void SNotifyContainer::ToggleWidget(const bool InToggle)
{
	SUsableCompoundWidget::ToggleWidget(InToggle);
	Anim_Back->ToggleWidget(InToggle);

	if (InToggle)
	{
		Timer_HideWidget = RegisterActiveTimer(DisplayTime, FWidgetActiveTimerDelegate::CreateLambda([&] (double /*InCurrentTime*/, float /*InDeltaTime*/) -> EActiveTimerReturnType
		{
			ToggleWidget(false);
			return EActiveTimerReturnType::Stop;
		}));

		Timer_RemoveQueue = RegisterActiveTimer(DisplayTime + 1.0f, FWidgetActiveTimerDelegate::CreateLambda([&](double /*InCurrentTime*/, float /*InDeltaTime*/) -> EActiveTimerReturnType
		{
			RemoveQueue(GetQueueId());
			return EActiveTimerReturnType::Stop;
		}));

		ResetDisplayTime();
	}
}

bool SNotifyContainer::IsInInteractiveMode() const
{
	return Anim_Back->IsAnimAtEnd();
}

bool SNotifyContainer::IsUniqueWidget() const
{
	return false;
}

void SNotifyContainer::SetContent(const FText& InContent)
{
	Widget_Content->SetText(InContent);
}

void SNotifyContainer::SetDisplayTime(const float& InTime)
{
	DisplayTime = InTime;
}

void SNotifyContainer::ResetDisplayTime()
{
	DisplayTime = 5.0f;
}

FReply SNotifyContainer::OnMouseButtonDown(const FGeometry& MyGeometry, const FPointerEvent& MouseEvent)
{
	if (Timer_HideWidget.IsValid())
	{
		UnRegisterActiveTimer(Timer_HideWidget.ToSharedRef());
		ToggleWidget(false);
	}

	if (Timer_RemoveQueue.IsValid())
	{
		UnRegisterActiveTimer(Timer_RemoveQueue.ToSharedRef());
		RemoveQueue(GetQueueId());
	}
	
	return FReply::Handled();
}
