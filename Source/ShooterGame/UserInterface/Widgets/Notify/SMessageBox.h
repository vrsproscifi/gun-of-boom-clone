// VRSPRO

#pragma once

#include "MessageBoxCfg.h"
#include "Widgets/SCompoundWidget.h"
#include "Styles/MessageBoxWidgetStyle.h"
#include "Utilities/SUsableCompoundWidget.h"
#include "Utilities/SlateUtilityTypes.h"
#include "Localization/TableBaseStrings.h"

class STextBlockButton;
struct FBackgroundBlurStyle;
typedef MessageBoxButton::Type EMessageBoxButton;

DECLARE_MULTICAST_DELEGATE(FOnMessageBoxNativeHidden);
DECLARE_MULTICAST_DELEGATE_OneParam(FOnMessageBoxNativeButton, const EMessageBoxButton&);

class SMessageBox 
	: public SUsableCompoundWidget
	, public TStaticSlateWidget<SMessageBox>
	, public TWidgetSupportQueue<SMessageBox>
{
public:
	static void ShowErrorMessageText(const FString& InId, const FText& InMessage);

	SLATE_BEGIN_ARGS(SMessageBox)
		: _Style(&FShooterStyle::Get().GetWidgetStyle<FMessageBoxStyle>("SMessageBoxStyle"))
	{
		_Visibility = EVisibility::SelfHitTestInvisible;
	}
	SLATE_STYLE_ARGUMENT(FMessageBoxStyle, Style)
	SLATE_END_ARGS()

	void Construct(const FArguments& InArgs);

	virtual void ToggleWidget(const bool) override;
	virtual bool IsInInteractiveMode() const override;
	virtual bool IsUniqueWidget() const override { return false; }

	void SetButtonText(const EMessageBoxButton, const FText& = FText::GetEmpty());
	void SetButtonsText(const FText& = FText::GetEmpty(), const FText& = FText::GetEmpty(), const FText& = FText::GetEmpty());
	void SetHeaderText(const FText&);

	void SetContent(const FText&, const ETextJustify::Type = ETextJustify::Center);
	void SetContent(TSharedPtr<SWidget>);

	// Use after SetContent if need, SetContent reset this to default
	void SetAllowedSize(const FBox2D&);

	void SetEnableClose(const bool);
	void SetSound(const FSlateSound& InSound, const bool IsShowSound = true);

	void SetIsOnceClickButton(const EMessageBoxButton, const bool);
	void SetIsOnceClickButtons(const bool);
	void ResetOnceClickButton(const EMessageBoxButton);
	void ResetOnceClickButtons();

	void SetFromCfg(const UMessageBoxCfg*);
	void SetFromCfg(const FMessageBoxMsg&);

	FOnMessageBoxButton OnMessageBoxDynamicButton;
	FOnMessageBoxHidden OnMessageBoxDynamicHidden;

	FOnMessageBoxNativeHidden OnMessageBoxHidden;
	FOnMessageBoxNativeButton OnMessageBoxButton;

	FOnInt32ValueChanged OnInt32ValueChanged;

	static bool AddQueueMessage(const FName&, const FOnClickedOutside&);
	static bool RemoveQueueMessage(const FName&);
	static void ResetQueueMessages();

protected:

	TSharedPtr<SRichTextBlock>  Widget_HeadText;
	TSharedPtr<STextBlockButton> Widget_CloseText;

	TSharedPtr<class SAnimatedBackground> Widget_AnimBox, Widget_AnimClose, Widget_AnimBack;

	TSharedPtr<SButton> Widget_Button[MessageBoxButton::Type::End];
	TSharedPtr<SRichTextBlock> Widget_ButtonText[MessageBoxButton::Type::End];

	SVerticalBox::FSlot* ContentSlot;

	const FMessageBoxStyle* Style;
	const FBackgroundBlurStyle* BackgroundBlurStyle;

	FReply OnClickedClose();
	FReply OnClickedButtonEvent(const EMessageBoxButton);
	virtual FReply OnMouseMove(const FGeometry& MyGeometry, const FPointerEvent& MouseEvent) override;

	EVisibility GetCloseVisibility() const;
	EVisibility GetBackVisibility() const;
	FSlateColor GetBackColor() const;
	ETextJustify::Type GetHeadTextAlignment() const;

	// Storage variables
	ETextJustify::Type HeadTextAlignment;

	bool IsEnabledClose
		, IsAutoUnbind_OnMessageBoxButton
		, IsAutoUnbind_OnMessageBoxHidden
		, IsAutoUnbind_OnInt32ValueChanged;

	enum ESizeParameter
	{
		MinDesiredWidth,
		MaxDesiredWidth,
		MinDesiredHeight,
		MaxDesiredHeight
	};

	FOptionalSize GetBoxSizeParameter(const ESizeParameter) const;

	FBox2D BoxSize;
	FSlateSound ShowSound, HideSound;

	bool IsOnceClickButton[MessageBoxButton::Type::End];
};

//=================================================

#ifndef SMessageBox_H_ShowErrorMessage
#define SMessageBox_H_ShowErrorMessage

//-----------------------------------------------------------------------
extern void ShowErrorMessage(const FName& Id, const FString& Message);

//#ifdef ShowErrorMessage
//#undef ShowErrorMessage
//#endif


//-----------------------------------------------------------------------
#ifdef ShowRestarGameErrorMessage
#undef ShowRestarGameErrorMessage
#endif

#define ShowRestarGameErrorMessage(Id, Title, Message)\
QueueBegin(SMessageBox, Id)\
SMessageBox::Get()->SetHeaderText(Title);\
SMessageBox::Get()->SetContent(Message);\
SMessageBox::Get()->SetButtonsText(FTableBaseStrings::GetBaseText(EBaseStrings::Continue));\
SMessageBox::Get()->OnMessageBoxButton.AddLambda([&](EMessageBoxButton InButton)\
{\
	if(auto PC = GetBaseController()) \
	{ \
		PC->PlayerTravelToLobby(); \
	} \
	else \
	{ \
		FGenericPlatformMisc::RequestExit(false); \
	} \
});\
SMessageBox::Get()->ToggleWidget(true);\
QueueEnd

//extern void ShowRestarGameErrorMessage(const FName& Id, const FText& Title, const FText& Message);


//-----------------------------------------------------------------------





#endif