// Fill out your copyright notice in the Description page of Project Settings.

#include "ShooterGame.h"
#include "SFightAction.h"
#include "SlateOptMacros.h"
#include "Utilities/SAnimatedBackground.h"
#include "BasicGameAchievement.h"
#include "Localization/TableBaseStrings.h"

class SHOOTERGAME_API SFightAction_Item : public SCompoundWidget
{
public:
	SLATE_BEGIN_ARGS(SFightAction_Item)
		: _Style(&FShooterStyle::Get().GetWidgetStyle<FFightActionStyle>("SFightActionStyle"))
	{
		_Visibility = EVisibility::HitTestInvisible;
	}
	SLATE_STYLE_ARGUMENT(FFightActionStyle, Style)
	SLATE_END_ARGS()

	BEGIN_SLATE_FUNCTION_BUILD_OPTIMIZATION
	void Construct(const FArguments& InArgs)
	{
		Style = InArgs._Style;

		ChildSlot
		[
			SNew(SVerticalBox)
			+ SVerticalBox::Slot().AutoHeight()
			[
				SAssignNew(Anim_BigText, SAnimatedBackground)
				.InitAsHide(true)
				.IsEnabledBlur(false)
				.IsRelative(true)
				.Duration(.5f)
				.ShowAnimation(EAnimBackAnimation::LeftFade)
				.HideAnimation(EAnimBackAnimation::RightFade)
				[
					SNew(STextBlock)
					.TextStyle(&Style->BigText)
					.Text(this, &SFightAction_Item::GetBigText)
					.Justification(ETextJustify::Center)
				]
			]
			+ SVerticalBox::Slot().AutoHeight().Padding(6)
			[
				SAssignNew(Anim_ScoreText, SAnimatedBackground)
				.InitAsHide(true)
				.IsEnabledBlur(false)
				.IsRelative(true)
				.Duration(.5f)
				.ShowAnimation(EAnimBackAnimation::RightFade)
				.HideAnimation(EAnimBackAnimation::LeftFade)
				[
					SNew(STextBlock)
					.TextStyle(&Style->ScoreText)
					.Text(this, &SFightAction_Item::GetScoreText)
					.Justification(ETextJustify::Center)
				]
			]
		];
	}
	END_SLATE_FUNCTION_BUILD_OPTIMIZATION

	FNotifyPlayerAchievement Item;

	void ToggleWidget(bool InToggle)
	{
		Anim_BigText->ToggleWidget(InToggle);
		Anim_ScoreText->ToggleWidget(InToggle);
	}

protected:

	const FFightActionStyle* Style;	

	TSharedPtr<SAnimatedBackground> Anim_BigText;
	TSharedPtr<SAnimatedBackground> Anim_ScoreText;

	FText GetBigText() const
	{
		return Item->GetAchievementProperty().Title;
	}

	FText GetScoreText() const
	{
		int32 ScorePoints = 0;
		const auto ItemProperty = Item->GetAchievementProperty();

		if (ItemProperty.bAllowOverride)
		{
			ScorePoints = Item.Score;
		}
		else
		{
			ScorePoints = ItemProperty.ScorePoints;
		}

		if (ScorePoints > 0)
		{
			return FText::Format(FText::FromString("{0} +{1}"), FTableBaseStrings::GetBaseText(EBaseStrings::Score), FText::AsNumber(ScorePoints, &FNumberFormattingOptions::DefaultNoGrouping()));
		}
		else
		{
			return FText::Format(FText::FromString("{0} {1}"), FTableBaseStrings::GetBaseText(EBaseStrings::Score), FText::AsNumber(ScorePoints, &FNumberFormattingOptions::DefaultNoGrouping()));
		}
	}
};

BEGIN_SLATE_FUNCTION_BUILD_OPTIMIZATION
void SFightAction::Construct(const FArguments& InArgs)
{
	Style = InArgs._Style;

	ChildSlot.VAlign(VAlign_Top).HAlign(HAlign_Center).Padding(0, 120)
	[
		SNew(SOverlay)
		+ SOverlay::Slot()
		[
			SAssignNew(Widget_Achiv[0], SFightAction_Item)
		]
		+ SOverlay::Slot()
		[
			SAssignNew(Widget_Achiv[1], SFightAction_Item)
		]
	];

	CurrentWidget = false;
}
END_SLATE_FUNCTION_BUILD_OPTIMIZATION

void SFightAction::AddAchievement(const FNotifyPlayerAchievement& InAchievement)
{
	if (InAchievement.Achievement.IsValid() && InAchievement.Achievement->IsValidLowLevel())
	{
		if (Queue.IsEmpty())
		{
			LastReRegisterTimerTime = FSlateApplication::Get().GetCurrentTime();
			QueueTimerHandle = RegisterActiveTimer(3.0f, FWidgetActiveTimerDelegate::CreateSP(this, &SFightAction::OnDequeue));

			Widget_Achiv[CurrentWidget]->Item = InAchievement;
			Widget_Achiv[CurrentWidget]->ToggleWidget(true);
		}
		else if (QueueTimerHandle.IsValid() && LastReRegisterTimerTime + 1.0f > FSlateApplication::Get().GetCurrentTime())
		{
			LastReRegisterTimerTime = FSlateApplication::Get().GetCurrentTime();
			UnRegisterActiveTimer(QueueTimerHandle.ToSharedRef());
			QueueTimerHandle = RegisterActiveTimer(1.0f, FWidgetActiveTimerDelegate::CreateSP(this, &SFightAction::OnDequeue));
		}

		Queue.Enqueue(InAchievement);
	}
}

EActiveTimerReturnType SFightAction::OnDequeue(double InCurrentTime, float InDeltaTime)
{
	Widget_Achiv[CurrentWidget]->ToggleWidget(false);

	CurrentWidget = !CurrentWidget;

	Queue.Pop();

	if (Queue.IsEmpty() == false)
	{
		Queue.Peek(Widget_Achiv[CurrentWidget]->Item);
		Widget_Achiv[CurrentWidget]->ToggleWidget(true);

		return EActiveTimerReturnType::Continue;
	}
	
	return EActiveTimerReturnType::Stop;
}

bool FNotifyPlayerAchievement::IsValid() const
{
	return 	Achievement.IsValid() && Achievement->IsValidLowLevel();
}

UBasicGameAchievement* FNotifyPlayerAchievement::operator->() const
{
	if (IsValid())
	{
		return Achievement.Get();
	}
	return nullptr;
}
