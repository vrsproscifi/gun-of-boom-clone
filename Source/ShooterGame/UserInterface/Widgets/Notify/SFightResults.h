// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "Utilities/SlateUtilityTypes.h"
#include "Utilities/SUsableCompoundWidget.h"
#include "Styles/FightResultsWidgetStyle.h"

class SAnimatedBackground;
struct FGameMatchMemberInformation;
struct FGameMatchInformation;
/**
 * 
 */
class SHOOTERGAME_API SFightResults : public SUsableCompoundWidget
{
public:
	SLATE_BEGIN_ARGS(SFightResults)
		: _Style(&FShooterStyle::Get().GetWidgetStyle<FFightResultsStyle>("SFightResultsStyle"))
	{
		_Visibility = EVisibility::SelfHitTestInvisible;
	}
	SLATE_STYLE_ARGUMENT(FFightResultsStyle, Style)
	SLATE_EVENT(FOnGuid, OnPlayerSelected)
	SLATE_END_ARGS()

	void Construct(const FArguments& InArgs);
	void SetInformation(const FGameMatchInformation& InInformation, const FGuid& InLocalPlayerId);

	void ToggleWidget(const bool InToggle) override;
	bool IsInInteractiveMode() const override;
	virtual void HideWidget() override;

	static FName Column_Name;
	static FName Column_Kills;
	static FName Column_Deaths;
	static FName Column_Score;
	static FName Column_Elo;

	static float FillColumn_Name;
	static float FillColumn_Kills;
	static float FillColumn_Deaths;
	static float FillColumn_Score;
	static float FillColumn_Elo;

protected:

	const FFightResultsStyle* Style;

	FOnGuid OnPlayerSelected;
	bool CachedIsWin;

	void PlayWinLoseSound();

	TArray<TSharedPtr<FGameMatchMemberInformation>> PlayersList_Friendly, PlayersList_Enemy;
	TSharedPtr<SListView<TSharedPtr<FGameMatchMemberInformation>>> PlayersTable_Friendly, PlayersTable_Enemy;
	TSharedPtr<STextBlock> Widget_WinStatus;
	TSharedPtr<STextBlock> Widget_LocalScore;
	TSharedPtr<STextBlock> Widget_EnemyScore;
	TSharedPtr<SAnimatedBackground> Back_Animation;

};
