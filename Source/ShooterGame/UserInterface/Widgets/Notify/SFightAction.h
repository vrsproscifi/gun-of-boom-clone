// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "Widgets/SCompoundWidget.h"
#include "Styles/FightActionWidgetStyle.h"

class SAnimatedBackground;
class UBasicGameAchievement;
class SFightAction_Item;

struct FNotifyPlayerAchievement
{
	FNotifyPlayerAchievement(){}
	FNotifyPlayerAchievement(const TWeakObjectPtr<UBasicGameAchievement>& InAchievement, const int32& InScore)
		: Achievement(InAchievement)
		, Score(InScore)
	{
		
	}

	bool IsValid() const;

	FORCEINLINE UBasicGameAchievement* operator->() const;

	TWeakObjectPtr<UBasicGameAchievement> Achievement;
	int32 Score;
};


class SHOOTERGAME_API SFightAction : public SCompoundWidget
{
public:
	SLATE_BEGIN_ARGS(SFightAction)
		: _Style(&FShooterStyle::Get().GetWidgetStyle<FFightActionStyle>("SFightActionStyle"))
	{
		_Visibility = EVisibility::HitTestInvisible;
	}
	SLATE_STYLE_ARGUMENT(FFightActionStyle, Style)
	SLATE_END_ARGS()

	void Construct(const FArguments& InArgs);

	void AddAchievement(const FNotifyPlayerAchievement& InAchievement);

protected:

	const FFightActionStyle* Style;

	TSharedPtr<SFightAction_Item> Widget_Achiv[2];
	bool CurrentWidget;
	double LastReRegisterTimerTime;

	TSharedPtr<FActiveTimerHandle> QueueTimerHandle;

	TQueue<FNotifyPlayerAchievement> Queue;

	EActiveTimerReturnType OnDequeue(double InCurrentTime, float InDeltaTime);
};
