// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "Utilities/SlateUtilityTypes.h"
#include "Utilities/SUsableCompoundWidget.h"
#include "Styles/MessageTipWidgetStyle.h"

class SAnimatedBackground;

/**
 * 
 */
class SMessageTip 
	: public SUsableCompoundWidget
	, public TStaticSlateWidget<SMessageTip>
	, public TWidgetSupportQueue<SMessageTip>
{
public:
	SLATE_BEGIN_ARGS(SMessageTip)
		: _Style(&FShooterStyle::Get().GetWidgetStyle<FMessageTipStyle>("SMessageTipStyle"))
	{
		_Visibility = EVisibility::SelfHitTestInvisible;
	}
	SLATE_STYLE_ARGUMENT(FMessageTipStyle, Style)
	SLATE_END_ARGS()

	void Construct(const FArguments& InArgs);

	virtual void ToggleWidget(const bool InToggle) override;
	virtual bool IsInInteractiveMode() const override { return false; }

	void SetTitle(const FText&);
	void SetContent(const FText&, const ETextJustify::Type = ETextJustify::Center);
	void SetContent(TSharedPtr<SWidget>);

	// Use after SetContent if need, SetContent reset this to default
	void SetAllowedSize(const FBox2D&);

	FSlateHyperlinkRun::FOnClick OnHyperlink;

protected:

	const FMessageTipStyle* Style;

	SVerticalBox::FSlot* ContentSlot;
	TSharedPtr<SRichTextBlock> Widget_Title;
	TSharedPtr<SAnimatedBackground> Widget_Animation;

	enum ESizeParameter
	{
		MinDesiredWidth,
		MaxDesiredWidth,
		MinDesiredHeight,
		MaxDesiredHeight
	};

	FOptionalSize GetBoxSizeParameter(const ESizeParameter) const;

	FBox2D BoxSize;
	FSlateSound ShowSound, HideSound;
};
