// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "Widgets/SCompoundWidget.h"
#include "Styles/KillListWidgetStyle.h"

class AShooterPlayerState;
/**
 * 
 */
class SHOOTERGAME_API SKillList : public SCompoundWidget
{
public:
	SLATE_BEGIN_ARGS(SKillList)
	{}
	SLATE_ARGUMENT(TWeakObjectPtr<AShooterPlayerState>, LocalState)
	SLATE_END_ARGS()

	void Construct(const FArguments& InArgs);

	void AddRow(AShooterPlayerState* InKilled, AShooterPlayerState* InKiller = nullptr, const int32& InWeaponId = INDEX_NONE);

protected:

	TWeakObjectPtr<AShooterPlayerState> LocalState;

	TSharedPtr<SVerticalBox>	BoxContainer;
};
