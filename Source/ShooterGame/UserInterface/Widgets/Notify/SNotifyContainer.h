// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "Utilities/SlateUtilityTypes.h"
#include "Utilities/SUsableCompoundWidget.h"
#include "Styles/NotifyContainerWidgetStyle.h"

class SAnimatedBackground;
/**
 * 
 */
class SHOOTERGAME_API SNotifyContainer 
	: public SUsableCompoundWidget
	, public TStaticSlateWidget<SNotifyContainer>
	, public TWidgetSupportQueue<SNotifyContainer>
{
public:
	SLATE_BEGIN_ARGS(SNotifyContainer)
		: _Style(&FShooterStyle::Get().GetWidgetStyle<FNotifyContainerStyle>("SNotifyContainerStyle"))
	{
		_Visibility = EVisibility::SelfHitTestInvisible;
	}
	SLATE_STYLE_ARGUMENT(FNotifyContainerStyle, Style)
	SLATE_END_ARGS()

	void Construct(const FArguments& InArgs);

	virtual void ToggleWidget(const bool InToggle) override;
	virtual bool IsInInteractiveMode() const override;
	virtual bool IsUniqueWidget() const override;

	void SetContent(const FText& InContent);
	void SetDisplayTime(const float& InTime);
	void ResetDisplayTime();

	static void ShowNotify(const FName& InId, const FString& InMessage);
	static void ShowNotify(const FName& InId, const FText& InMessage);

protected:

	const FNotifyContainerStyle* Style;
	float DisplayTime;

	TSharedPtr<SAnimatedBackground> Anim_Back;
	TSharedPtr<SRichTextBlock>	Widget_Content;

	TSharedPtr<FActiveTimerHandle>	Timer_HideWidget, Timer_RemoveQueue;

	virtual FReply OnMouseButtonDown(const FGeometry& MyGeometry, const FPointerEvent& MouseEvent) override;
};
