// VRSPRO

#pragma once

#include "Components/SResourceTextBoxValue.h"

class FResourceDecoratorHelper
{
public:

	FResourceDecoratorHelper();
	FResourceDecoratorHelper(const int64& InAmount, const EResourceTextBoxType::Type& InType = EResourceTextBoxType::Money, const EResourceTextBoxDisplayMethod& InMethod = EResourceTextBoxDisplayMethod::Numeric);

	FResourceDecoratorHelper& SetType(const EResourceTextBoxType::Type& InType);
	FResourceDecoratorHelper& SetAmount(const int64& InAmount);
	FResourceDecoratorHelper& SetDisplayMethod(const EResourceTextBoxDisplayMethod& InMethod);
	FResourceDecoratorHelper& SetSize(const FVector& InSize);
	FResourceDecoratorHelper& SetFontSize(const float& InSize);
	FResourceDecoratorHelper& SetIconSize(const FVector2D& InSize);
	FResourceDecoratorHelper& SetIconSize(const float& InSize);

	FString ToString() const;
	FText ToText() const;

protected:

	FResourceTextBoxValue Value;
	FVector Size;
};

class FTextDecoratorHelper
{
public:

	static TArray<FName> GetSupportFontNames();

	FTextDecoratorHelper();
	FTextDecoratorHelper(const FString& InValue);

	FTextDecoratorHelper& SetValue(const FString& InValue);
	FTextDecoratorHelper& SetValue(const FText& InValue);
	FTextDecoratorHelper& SetFont(const FString& InFontName);
	FTextDecoratorHelper& SetFont(const FName& InFontName);
	FTextDecoratorHelper& SetSize(const uint8& InSize);
	FTextDecoratorHelper& SetColor(const FString& InColor);
	FTextDecoratorHelper& SetColor(const FColor& InColor);
	FTextDecoratorHelper& SetColor(const FLinearColor& InColor);

	FString ToString() const;
	FText ToText() const;

protected:

	TMap<FName, FString> Parameters;
	FString Value;
};