
#include "ShooterGame.h"
#include "LokaResourceDecorator.h"
#include "Components/SResourceTextBox.h"
#include "Components/SResourceTextBoxType.h"

FLokaResourceDecorator::FLokaResourceDecorator()
{
}

TSharedRef<FLokaResourceDecorator> FLokaResourceDecorator::Create()
{
	return MakeShareable(new FLokaResourceDecorator());
}

bool FLokaResourceDecorator::Supports(const FTextRunParseResults& RunParseResult, const FString& Text) const
{
	return (RunParseResult.Name == TEXT("resource"));
}

TSharedRef<ISlateRun> FLokaResourceDecorator::Create(const TSharedRef<FTextLayout>& TextLayout, const FTextRunParseResults& RunParseResult, const FString& OriginalText, const TSharedRef<FString>& InOutModelText, const ISlateStyle* Style)
{
	FTextRange ModelRange;
	ModelRange.BeginIndex = InOutModelText->Len();
	*InOutModelText += TEXT('\x200B'); // Zero-Width Breaking Space
	ModelRange.EndIndex = InOutModelText->Len();

	FTextRunInfo RunInfo(RunParseResult.Name, FText::FromString(OriginalText.Mid(RunParseResult.ContentRange.BeginIndex, RunParseResult.ContentRange.EndIndex - RunParseResult.ContentRange.BeginIndex)));
	for (const TPair<FString, FTextRange>& Pair : RunParseResult.MetaData)
	{
		RunInfo.MetaData.Add(Pair.Key, OriginalText.Mid(Pair.Value.BeginIndex, Pair.Value.EndIndex - Pair.Value.BeginIndex));
	}

	//==============================================================================================================================]

	EResourceTextBoxType::Type ResourceTextBoxType = EResourceTextBoxType::Money;
	auto TargetBoxType = RunInfo.MetaData.Find("type");
	if (TargetBoxType && TargetBoxType->IsEmpty() == false)
	{
		if (UEnum* pEnum = FindObject<UEnum>(ANY_PACKAGE, TEXT("EResourceTextBoxType"), true))
		{
			ResourceTextBoxType = static_cast<EResourceTextBoxType::Type>(pEnum->GetIndexByNameString(*TargetBoxType));
		}
	}

	EResourceTextBoxDisplayMethod DisplayMethod = EResourceTextBoxDisplayMethod::Numeric;
	auto TargetDisplayMethod = RunInfo.MetaData.Find("display");
	if (TargetDisplayMethod && TargetDisplayMethod->IsEmpty() == false)
	{
		if (UEnum* pEnum = FindObject<UEnum>(ANY_PACKAGE, TEXT("EResourceTextBoxDisplayMethod"), true))
		{
			DisplayMethod = static_cast<EResourceTextBoxDisplayMethod>(pEnum->GetIndexByNameString(*TargetDisplayMethod));
		}
	}

	int32 ResourceTextBoxValue = 0;
	if (RunInfo.Content.IsNumeric())
	{
		ResourceTextBoxValue = FCString::Atoi(*RunInfo.Content.ToString());
	}

	FVector ResourceTextBoxSize = FVector::ZeroVector;
	auto TargetBoxSize = RunInfo.MetaData.Find("size");
	if (TargetBoxSize && !ResourceTextBoxSize.InitFromString(*TargetBoxSize))
	{
		FString strX, strY, strZ;

		TargetBoxSize->Split(",", &strX, &strY);
		TargetBoxSize->Split(",", &strY, &strZ);

		if (!strX.IsEmpty() && !strY.IsEmpty() && !strZ.IsEmpty())
		{
			ResourceTextBoxSize = FVector(FCString::Atof(*strX), FCString::Atof(*strY), FCString::Atof(*strZ));
		}
	}

	TSharedRef<SWidget> ResultWidget = SNew(SResourceTextBox)
		.TypeAndValue(FResourceTextBoxValue(ResourceTextBoxType, ResourceTextBoxValue, DisplayMethod))
		.CustomSize(ResourceTextBoxSize);

	//TSharedRef< FSlateFontMeasure > FontMeasure = FSlateApplication::Get().GetRenderer()->GetFontMeasureService();
	//int16 Baseline = FontMeasure->GetBaseline(Style->Content.Text.Font);

	return FSlateWidgetRun::Create(TextLayout, RunInfo, InOutModelText, FSlateWidgetRun::FWidgetRunInfo(ResultWidget, 0), ModelRange);
}