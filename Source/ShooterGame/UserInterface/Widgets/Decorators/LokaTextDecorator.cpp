
#include "ShooterGame.h"
#include "LokaTextDecorator.h"
#include "Styles/LokaFontsListWidgetStyle.h"

FLokaTextDecorator::FLokaTextDecorator(const TMap<FName, FSlateFontInfo>& InFonts, const FSlateFontInfo& InDefaultFont, const FLinearColor& InDefaultColor)
	: Fonts(InFonts)
	, DefaultFont(InDefaultFont)
	, DefaultColor(InDefaultColor)
{
	auto Style = &FShooterStyle::Get().GetWidgetStyle<FLokaFontsListStyle>("SFontsListStyle");
	Fonts.Append(Style->Fonts);
}

TSharedRef<FLokaTextDecorator> FLokaTextDecorator::Create(const TMap<FName, FSlateFontInfo>& InFonts, const FSlateFontInfo& InDefaultFont, const FLinearColor& InDefaultColor)
{
	return MakeShareable(new FLokaTextDecorator(InFonts, InDefaultFont, InDefaultColor));
}

TSharedRef<FLokaTextDecorator> FLokaTextDecorator::Create(const FTextBlockStyle& InTextBlockStyle, const TMap<FName, FSlateFontInfo>& InFonts)
{
	return FLokaTextDecorator::Create(InFonts, InTextBlockStyle.Font, InTextBlockStyle.ColorAndOpacity.GetSpecifiedColor());
}

bool FLokaTextDecorator::Supports(const FTextRunParseResults& RunParseResult, const FString& Text) const
{
	return (RunParseResult.Name == TEXT("text"));
}

TSharedRef<ISlateRun> FLokaTextDecorator::Create(const TSharedRef<FTextLayout>& TextLayout, const FTextRunParseResults& RunParseResult, const FString& OriginalText, const TSharedRef<FString>& InOutModelText, const ISlateStyle* Style)
{
	FRunInfo RunInfo(RunParseResult.Name);
	for (const TPair<FString, FTextRange>& Pair : RunParseResult.MetaData)
	{
		RunInfo.MetaData.Add(Pair.Key, OriginalText.Mid(Pair.Value.BeginIndex, Pair.Value.EndIndex - Pair.Value.BeginIndex));
	}

	FTextRange ModelRange;
	ModelRange.BeginIndex = InOutModelText->Len();
	*InOutModelText += OriginalText.Mid(RunParseResult.ContentRange.BeginIndex, RunParseResult.ContentRange.EndIndex - RunParseResult.ContentRange.BeginIndex);
	ModelRange.EndIndex = InOutModelText->Len();

	return FSlateTextRun::Create(RunInfo, InOutModelText, CreateTextBlockStyle(RunInfo), ModelRange);
}

FTextBlockStyle FLokaTextDecorator::CreateTextBlockStyle(const FRunInfo& InRunInfo) const
{
	FTextBlockStyle TextBlockStyle;
	FLinearColor LinearColor;

	TextBlockStyle.Font = DefaultFont;
	auto TargetFontName = InRunInfo.MetaData.Find(TEXT("font"));
	if (TargetFontName && Fonts.Contains(**TargetFontName))
	{
		TextBlockStyle.Font = Fonts.FindChecked(**TargetFontName);
	}

	TextBlockStyle.Font.Size = DefaultFont.Size;
	auto TargetFontSize = InRunInfo.MetaData.Find(TEXT("size"));
	if (TargetFontSize)
	{
		TextBlockStyle.Font.Size = static_cast<uint16>(FPlatformString::Atoi(**TargetFontSize));
	}

	TextBlockStyle.Font.OutlineSettings.OutlineSize = DefaultFont.OutlineSettings.OutlineSize;
	auto TargetOutlineSize = InRunInfo.MetaData.Find(TEXT("outlinesize"));
	if (TargetOutlineSize)
	{
		TextBlockStyle.Font.OutlineSettings.OutlineSize = static_cast<uint16>(FPlatformString::Atoi(**TargetOutlineSize));
	}

	LinearColor = DefaultColor;
	auto TargetFontColor = InRunInfo.MetaData.Find(TEXT("color"));
	if (TargetFontColor && !LinearColor.InitFromString(*TargetFontColor))
	{
		LinearColor = FColor::FromHex(*TargetFontColor).ReinterpretAsLinear();
	}

	TextBlockStyle.Font.OutlineSettings.OutlineColor = DefaultFont.OutlineSettings.OutlineColor;
	auto TargetOutlineColor = InRunInfo.MetaData.Find(TEXT("outlinecolor"));
	if (TargetOutlineColor && !TextBlockStyle.Font.OutlineSettings.OutlineColor.InitFromString(*TargetOutlineColor))
	{
		TextBlockStyle.Font.OutlineSettings.OutlineColor = FColor::FromHex(*TargetOutlineColor).ReinterpretAsLinear();
	}

	TextBlockStyle.SetColorAndOpacity(LinearColor);
	return TextBlockStyle;
}