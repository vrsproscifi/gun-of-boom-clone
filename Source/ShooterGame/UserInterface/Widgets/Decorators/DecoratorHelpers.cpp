
#include "ShooterGame.h"
#include "DecoratorHelpers.h"
#include "LokaSpaceDecorator.h"
#include "LokaResourceDecorator.h"
#include "LokaTextDecorator.h"
#include "Styles/LokaFontsListWidgetStyle.h"

FResourceDecoratorHelper::FResourceDecoratorHelper()
	: Value(FResourceTextBoxValue(EResourceTextBoxType::Money, 0))
	, Size(FVector::ZeroVector)
{

}

FResourceDecoratorHelper::FResourceDecoratorHelper
(
	const int64& InAmount,
	const EResourceTextBoxType::Type& InType,
	const EResourceTextBoxDisplayMethod& InMethod
)
	: Value(FResourceTextBoxValue(InType, InAmount, InMethod))
	, Size(FVector::ZeroVector)
{

}

FResourceDecoratorHelper& FResourceDecoratorHelper::SetType(const EResourceTextBoxType::Type& InType)
{
	Value.Type = InType;
	return *this;
}

FResourceDecoratorHelper& FResourceDecoratorHelper::SetAmount(const int64& InAmount)
{
	Value.Value = InAmount;
	return *this;
}

FResourceDecoratorHelper& FResourceDecoratorHelper::SetDisplayMethod(const EResourceTextBoxDisplayMethod& InMethod)
{
	Value.DisplayMethod = InMethod;
	return *this;
}

FResourceDecoratorHelper& FResourceDecoratorHelper::SetSize(const FVector& InSize)
{
	Size = InSize;
	return *this;
}

FResourceDecoratorHelper& FResourceDecoratorHelper::SetFontSize(const float& InSize)
{
	Size.Z = InSize;
	return *this;
}

FResourceDecoratorHelper& FResourceDecoratorHelper::SetIconSize(const FVector2D& InSize)
{
	Size.X = InSize.X;
	Size.Y = InSize.Y;
	return *this;
}

FResourceDecoratorHelper& FResourceDecoratorHelper::SetIconSize(const float& InSize)
{
	Size.X = InSize;
	Size.Y = InSize;
	return *this;
}

FString FResourceDecoratorHelper::ToString() const
{
	FString OutString("<resource ");

	if (UEnum* pEnum = FindObject<UEnum>(ANY_PACKAGE, TEXT("EResourceTextBoxType"), true))
	{
		OutString.Append(FString("type") + FString("=\"") + pEnum->GetNameStringByIndex(Value.Type) + FString("\" "));
	}

	if (UEnum* pEnum = FindObject<UEnum>(ANY_PACKAGE, TEXT("EResourceTextBoxDisplayMethod"), true))
	{
		OutString.Append(FString("display") + FString("=\"") + pEnum->GetNameStringByIndex(static_cast<int32>(Value.DisplayMethod)) + FString("\" "));
	}

	OutString.Append(FString("size") + FString("=\"") + Size.ToString() + FString("\" "));

	OutString.RemoveFromEnd(" ");
	OutString.Append(">");
	OutString.Append(FString::Printf(TEXT("%lld"), Value.Value));
	OutString.Append("</>");

	return OutString;
}

FText FResourceDecoratorHelper::ToText() const
{
	return FText::FromString(ToString());
}

TArray<FName> FTextDecoratorHelper::GetSupportFontNames()
{
	TArray<FName> FontNames;

	auto Style = &FShooterStyle::Get().GetWidgetStyle<FLokaFontsListStyle>("SFontsListStyle");
	Style->Fonts.GetKeys(FontNames);

	return FontNames;
}

FTextDecoratorHelper::FTextDecoratorHelper()
{
	
}

FTextDecoratorHelper::FTextDecoratorHelper(const FString& InValue)
	: Value(InValue)
{

}

FTextDecoratorHelper& FTextDecoratorHelper::SetValue(const FString& InValue)
{
	Value = InValue;
	return *this;
}

FTextDecoratorHelper& FTextDecoratorHelper::SetValue(const FText& InValue)
{
	Value = InValue.ToString();
	return *this;
}

FTextDecoratorHelper& FTextDecoratorHelper::SetFont(const FString& InFontName)
{
	FString& fValue = Parameters.FindOrAdd("font");
	fValue = InFontName;
	return *this;
}

FTextDecoratorHelper& FTextDecoratorHelper::SetFont(const FName& InFontName)
{
	FString& fValue = Parameters.FindOrAdd("font");
	fValue = InFontName.ToString();
	return *this;
}

FTextDecoratorHelper& FTextDecoratorHelper::SetSize(const uint8& InSize)
{
	FString& fValue = Parameters.FindOrAdd("size");
	fValue = FString::FromInt(InSize);
	return *this;
}

FTextDecoratorHelper& FTextDecoratorHelper::SetColor(const FString& InColor)
{
	FString& fValue = Parameters.FindOrAdd("color");
	fValue = InColor;
	return *this;
}

FTextDecoratorHelper& FTextDecoratorHelper::SetColor(const FColor& InColor)
{
	FString& fValue = Parameters.FindOrAdd("color");
	fValue = InColor.ToString();
	return *this;
}

FTextDecoratorHelper& FTextDecoratorHelper::SetColor(const FLinearColor& InColor)
{
	FString& fValue = Parameters.FindOrAdd("color");
	fValue = InColor.ToString();
	return *this;
}

FString FTextDecoratorHelper::ToString() const
{
	FString OutString("<text ");

	for (auto i : Parameters)
	{
		OutString.Append(i.Key.ToString() + "=\"" + i.Value + "\" ");
	}

	OutString.RemoveFromEnd(" ");
	OutString.Append(">");
	OutString.Append(Value);
	OutString.Append("</>");

	return OutString;
}

FText FTextDecoratorHelper::ToText() const
{
	return FText::FromString(ToString());
}
