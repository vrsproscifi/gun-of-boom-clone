
#include "ShooterGame.h"
#include "LokaSpaceDecorator.h"

FLokaSpaceDecorator::FLokaSpaceDecorator()
{
}

TSharedRef<FLokaSpaceDecorator> FLokaSpaceDecorator::Create()
{
	return MakeShareable(new FLokaSpaceDecorator());
}

bool FLokaSpaceDecorator::Supports(const FTextRunParseResults& RunParseResult, const FString& Text) const
{
	return (RunParseResult.Name == TEXT("space"));
}

TSharedRef<ISlateRun> FLokaSpaceDecorator::Create(const TSharedRef<FTextLayout>& TextLayout, const FTextRunParseResults& RunParseResult, const FString& OriginalText, const TSharedRef<FString>& InOutModelText, const ISlateStyle* Style)
{
	FTextRange ModelRange;
	ModelRange.BeginIndex = InOutModelText->Len();
	*InOutModelText += TEXT('\x200B'); // Zero-Width Breaking Space
	ModelRange.EndIndex = InOutModelText->Len();

	FTextRunInfo RunInfo(RunParseResult.Name, FText::FromString(OriginalText.Mid(RunParseResult.ContentRange.BeginIndex, RunParseResult.ContentRange.EndIndex - RunParseResult.ContentRange.BeginIndex)));
	for (const TPair<FString, FTextRange>& Pair : RunParseResult.MetaData)
	{
		RunInfo.MetaData.Add(Pair.Key, OriginalText.Mid(Pair.Value.BeginIndex, Pair.Value.EndIndex - Pair.Value.BeginIndex));
	}

	//==============================================================================================================================]

	FVector2D SpacerSize(0, 0);
	//TSharedRef<SWidget> ResultWidget = SNullWidget::NullWidget;

	if (auto TargetHFill = RunInfo.MetaData.Find("h"))
	{
		if (TargetHFill->Equals(TEXT("Fill"), ESearchCase::IgnoreCase))
		{
			for (auto v : TextLayout->GetLineViews())
			{
				SpacerSize.X += v.Size.X;
			}
		}
		else
		{
			TTypeFromString<float>().FromString(SpacerSize.X, **TargetHFill);
		}
	}

	if (auto TargetVFill = RunInfo.MetaData.Find("v"))
	{
		if (TargetVFill->Equals(TEXT("Fill"), ESearchCase::IgnoreCase))
		{
			SpacerSize.Y = TextLayout->GetWrappedSize().Y;
		}
		else
		{
			TTypeFromString<float>().FromString(SpacerSize.Y, **TargetVFill);
		}
	}

	TSharedRef<SWidget> ResultWidget = SNew(SSpacer).Size(SpacerSize);

	//TSharedRef< FSlateFontMeasure > FontMeasure = FSlateApplication::Get().GetRenderer()->GetFontMeasureService();
	//int16 Baseline = FontMeasure->GetBaseline(Style->Content.Text.Font);

	return FSlateWidgetRun::Create(TextLayout, RunInfo, InOutModelText, FSlateWidgetRun::FWidgetRunInfo(ResultWidget, 0), ModelRange);
}