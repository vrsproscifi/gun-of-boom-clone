// VRSPRO

#pragma once

//#include "WebImageCache.h"

template<class TSlateWidget>
class TStaticSlateWidget
{
public:

	TStaticSlateWidget() {}
	~TStaticSlateWidget()
	{
		if (ThisInstance.GetSharedReferenceCount() <= 1)
		{
			Reset();
		}
	}

	static TSharedRef<TSlateWidget> Get()
	{
		if (!ThisInstance.IsValid() || ThisInstance.ToSharedRef() == SNullWidget::NullWidget)
		{
			SAssignNew(ThisInstance, TSlateWidget);
		}

		return ThisInstance.ToSharedRef();
	}

	static void Reset()
	{
		ThisInstance.Reset();
	}

protected:

	static TSharedPtr<TSlateWidget> ThisInstance;
};

template<class TSlateWidget>
TSharedPtr<TSlateWidget> TStaticSlateWidget<TSlateWidget>::ThisInstance = StaticCastSharedRef<TSlateWidget>(SNullWidget::NullWidget);

struct FOrderQueueData
{
public:

	FOrderQueueData();
	FOrderQueueData(const FOnClickedOutside& InEvent, const uint32& InOrder = 0, const bool& InWaiting = false);

	FOnClickedOutside& GetEvent();
	uint32 GetOrder() const;
	void SetOrder(const uint32& InOrder = 0);
	bool IsWaiting() const;

protected:

	FOnClickedOutside Event;
	uint32 Order;
	bool WaitingPrev;
};

class FWidgetBaseSupportQueue
{
public:

	static bool AddQueue(const FName& InIndex, const FOnClickedOutside& InEvent);
	static bool RemoveQueue(const FName& InIndex);
	static void ResetQueue();
	static FName GetCurrentQueueId();

protected:

	static TMap<FName, FOnClickedOutside> Queue;
	static FName CurrentQueueId;
};

class FWidgetOrderedSupportQueue : public FTickableGameObject
{
public:

	static bool AddQueue(const FName& InIndex, const FOrderQueueData& InData);
	static bool AddQueue(const FName& InIndex, const FOnClickedOutside& InEvent, const uint32& InOrder, const bool& InWaiting = false);
	static bool RemoveQueue(const FName& InIndex);
	static void ResetQueue();
	static FName GetCurrentQueueId();

protected:

	virtual void Tick(float DeltaTime) override;
	virtual TStatId GetStatId() const override;
	
	static void SortByOrder();

	static TMap<FName, FOrderQueueData> Queue;
	static FName CurrentQueueId;

private:
	
	static TSharedPtr<FWidgetOrderedSupportQueue> ThisInstance;
};

template<class TSlateWidget>
class TWidgetSupportQueue
{
public:

	static bool AddQueue(const FName& InIndex, const FOnClickedOutside& InEvent)
	{
		if (!Queue.Contains(InIndex))
		{
			Queue.Add(InIndex, InEvent);

			if (Queue.Num() == 1)
			{
				Queue.FindChecked(InIndex).ExecuteIfBound();
				TSlateWidget::Get()->QueueId = InIndex;
			}

			return true;
		}

		return false;
	}

	static bool RemoveQueue(const FName& InIndex)
	{
		if (Queue.Contains(InIndex))
		{
			Queue.Remove(InIndex);

			if (TSlateWidget::Get()->QueueId == InIndex)
			{
				TSlateWidget::Get()->ToggleWidget(false);
			}

			if (Queue.Num())
			{
				for (auto &i : Queue)
				{
					i.Value.ExecuteIfBound();
					TSlateWidget::Get()->QueueId = i.Key;
					break;
				}
			}

			return true;
		}

		return false;
	}

	static void ResetQueue()
	{
		Queue.Empty();
	}

	FName GetQueueId() const { return QueueId; }

protected:

	static TMap<FName, FOnClickedOutside> Queue;
	FName QueueId;
};

template<class TSlateWidget>
TMap<FName, FOnClickedOutside> TWidgetSupportQueue<TSlateWidget>::Queue = TMap<FName, FOnClickedOutside>();

#define QueueBegin(TWidget, Index, ...) TWidget::AddQueue(Index, FOnClickedOutside::CreateLambda([&, ##__VA_ARGS__ ]() {
#define QueueEnd }));

class FSlateWidgetMath
{
public:

	static FVector2D ClampToRadialPosition(const FVector2D& InPosition, const float& InRadius);
	static FVector2D GetPositionInRadial(const FVector2D& InCenter, const float& InDegress, const float& InRadius);
};

DECLARE_DELEGATE_OneParam(FOnGuid, const FGuid&);

// ���� ������ ��� ����, ����� ����� ��� ����.
#define ORDERED_QUEUE_FIGHTRESULTS 1
#define ORDERED_QUEUE_PERSONALFIGHTRESULT 2
#define ORDERED_QUEUE_PERSONALPROGRESS 3
#define ORDERED_QUEUE_NEWLEVEL 4