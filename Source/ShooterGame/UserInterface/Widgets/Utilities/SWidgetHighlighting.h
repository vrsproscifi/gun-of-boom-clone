// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "Widgets/SCompoundWidget.h"
#include "Styles/WidgetHighlightingWidgetStyle.h"

/**
 * 
 */
class SWidgetHighlighting : public SCompoundWidget
{
public:
	SLATE_BEGIN_ARGS(SWidgetHighlighting)
		: _Style(&FShooterStyle::Get().GetWidgetStyle<FWidgetHighlightingStyle>(TEXT("SWidgetHighlightingStyle")))
	{
		_Visibility = EVisibility::HitTestInvisible;
	}
	SLATE_STYLE_ARGUMENT(FWidgetHighlightingStyle, Style)
	SLATE_END_ARGS()

	/** Constructs this widget with InArgs */
	void Construct(const FArguments& InArgs);

	void AddWidget(const TSharedRef<SWidget>& InWidget);
	void RemoveWidget(const TSharedRef<SWidget>& InWidget);

protected:

	const FWidgetHighlightingStyle* Style;

	TArray<TSharedRef<SWidget>> HighlightWidgets;

	virtual int32 OnPaint(const FPaintArgs& Args, const FGeometry& AllottedGeometry, const FSlateRect& MyCullingRect, FSlateWindowElementList& OutDrawElements, int32 LayerId, const FWidgetStyle& InWidgetStyle, bool bParentEnabled) const override;
};
