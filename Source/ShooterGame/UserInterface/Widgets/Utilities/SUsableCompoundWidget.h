// VRSPRO

#pragma once

#include "Widgets/SCompoundWidget.h"

DECLARE_DELEGATE_TwoParams(FOnToggleInteractive, const bool, const TSharedRef<class SUsableCompoundWidget>&);

class SUsableCompoundWidget : public SCompoundWidget
{
	friend class UShooterGameViewportClient;
public:
	SLATE_BEGIN_ARGS(SUsableCompoundWidget)
	{}
	SLATE_END_ARGS()

	SUsableCompoundWidget() : bIsAlternativeCamera(false) {}

	void Construct(const FArguments& InArgs) {}

	virtual void ToggleWidget(const bool InToggle)
	{
		if (InToggle && IsInInteractiveMode()) return;
		OnToggleInteractive.ExecuteIfBound(InToggle, StaticCastSharedRef<SUsableCompoundWidget>(AsShared()));
	}

	virtual void HideWidget()
	{
		ToggleWidget(false);
	}

	virtual bool IsInInteractiveMode() const { return GetVisibility().IsVisible(); }
	virtual bool IsUniqueWidget() const { return true; }
	virtual void SetSupportAlternativeCamera(bool InToggle) { bIsAlternativeCamera = InToggle; }

protected:

	FOnToggleInteractive OnToggleInteractive;
	bool bIsAlternativeCamera;
};
