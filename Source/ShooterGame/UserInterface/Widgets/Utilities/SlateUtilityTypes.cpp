// VRSPRO

#include "ShooterGame.h"
#include "SlateUtilityTypes.h"

TMap<FName, FOnClickedOutside> FWidgetBaseSupportQueue::Queue = TMap<FName, FOnClickedOutside>();
FName FWidgetBaseSupportQueue::CurrentQueueId = NAME_None;

TMap<FName, FOrderQueueData> FWidgetOrderedSupportQueue::Queue = TMap<FName, FOrderQueueData>();
FName FWidgetOrderedSupportQueue::CurrentQueueId = NAME_None;
TSharedPtr<FWidgetOrderedSupportQueue> FWidgetOrderedSupportQueue::ThisInstance;

FOrderQueueData::FOrderQueueData()
{
	
}

FOrderQueueData::FOrderQueueData(const FOnClickedOutside& InEvent, const uint32& InOrder, const bool& InWaiting)
	: Event(InEvent)
	, Order(InOrder)
	, WaitingPrev(InWaiting)
{
	
}

FOnClickedOutside& FOrderQueueData::GetEvent()
{
	return Event;
}

uint32 FOrderQueueData::GetOrder() const
{
	return Order;
}

void FOrderQueueData::SetOrder(const uint32& InOrder)
{
	Order = InOrder;
}

bool FOrderQueueData::IsWaiting() const
{
	return WaitingPrev;
}

bool FWidgetBaseSupportQueue::AddQueue(const FName& InIndex, const FOnClickedOutside& InEvent)
{
	if (!Queue.Contains(InIndex))
	{
		Queue.Add(InIndex, InEvent);
		if (Queue.Num() == 1)
		{
			CurrentQueueId = InIndex;
			Queue.FindChecked(InIndex).ExecuteIfBound();
		}
		return true;
	}
	return false;
}

bool FWidgetBaseSupportQueue::RemoveQueue(const FName& InIndex)
{
	if (Queue.Contains(InIndex))
	{
		Queue.Remove(InIndex);
		if (Queue.Num())
		{
			for (auto& i : Queue)
			{
				CurrentQueueId = i.Key;
				i.Value.ExecuteIfBound();
				break;
			}
		}
		else
		{
			CurrentQueueId = NAME_None;
		}
		return true;
	}
	return false;
}

void FWidgetBaseSupportQueue::ResetQueue()
{
	CurrentQueueId = NAME_None;
	Queue.Empty();
}

FName FWidgetBaseSupportQueue::GetCurrentQueueId()
{
	return CurrentQueueId;
}

bool FWidgetOrderedSupportQueue::AddQueue(const FName& InIndex, const FOrderQueueData& InData)
{
	if (ThisInstance.IsValid() == false)
	{
		ThisInstance = MakeShareable(new FWidgetOrderedSupportQueue());
	}
	
	if (!Queue.Contains(InIndex))
	{
		Queue.Add(InIndex, InData);
		SortByOrder();

		for (auto pair : Queue)
		{
			if (pair.Value.IsWaiting() == false)
			{
				CurrentQueueId = pair.Key;
				pair.Value.GetEvent().ExecuteIfBound();
				break;
			}
		}			

		return true;
	}
	return false;
}

bool FWidgetOrderedSupportQueue::AddQueue(const FName& InIndex, const FOnClickedOutside& InEvent, const uint32& InOrder, const bool& InWaiting)
{
	FOrderQueueData OrderQueueData(InEvent, InOrder, InWaiting);
	return AddQueue(InIndex, OrderQueueData);
}

bool FWidgetOrderedSupportQueue::RemoveQueue(const FName& InIndex)
{
	if (Queue.Contains(InIndex))
	{
		Queue.Remove(InIndex);
		SortByOrder();

		if (Queue.Num())
		{
			for (auto& i : Queue)
			{
				CurrentQueueId = i.Key;
				i.Value.GetEvent().ExecuteIfBound();
				break;
			}
		}
		else
		{
			CurrentQueueId = NAME_None;
		}
		return true;
	}
	return false;
}

void FWidgetOrderedSupportQueue::ResetQueue()
{
	CurrentQueueId = NAME_None;
	Queue.Empty();
	ThisInstance.Reset();
}

FName FWidgetOrderedSupportQueue::GetCurrentQueueId()
{
	return CurrentQueueId;
}

void FWidgetOrderedSupportQueue::Tick(float DeltaTime)
{
	static float CurrentTime = .0f;
	CurrentTime += DeltaTime;

	if (CurrentTime >= 2.0f)
	{
		CurrentTime = .0f;
		SortByOrder();		

		if (CurrentQueueId == NAME_None)
		{
			for (auto& i : Queue)
			{
				CurrentQueueId = i.Key;
				i.Value.GetEvent().ExecuteIfBound();
				break;
			}
		}
	}
}

TStatId FWidgetOrderedSupportQueue::GetStatId() const
{
	RETURN_QUICK_DECLARE_CYCLE_STAT(FWidgetOrderedSupportQueue, STATGROUP_Tickables);
}

void FWidgetOrderedSupportQueue::SortByOrder()
{
	Queue.ValueSort([](const FOrderQueueData& A, const FOrderQueueData& B)
	{
		return A.GetOrder() < B.GetOrder();
	});
}

FVector2D FSlateWidgetMath::ClampToRadialPosition(const FVector2D& InPosition, const float& InRadius)
{
	const float DevideNumber = InRadius / 2;
	const float AxisXDelta = FMath::Clamp(InPosition.X / DevideNumber, -1.0f, 1.0f);
	const float AxisYDelta = FMath::Clamp(InPosition.Y / DevideNumber, -1.0f, 1.0f);

	FVector2D DrawPosition = FVector2D(InRadius, InRadius) / 2;
	FVector2D TargetPosition(AxisXDelta, AxisYDelta);

	float L = FMath::Sqrt(AxisXDelta * AxisXDelta + AxisYDelta * AxisYDelta);

	if (L > 0) {
		float angle = FMath::Atan2(AxisYDelta, AxisXDelta);
		float kx = FMath::Abs(FMath::Cos(angle));
		float ky = FMath::Abs(FMath::Sin(angle));
		float k = FMath::Max(kx, ky);
		kx /= k; ky /= k;
		float ext = FMath::Sqrt(kx*kx + ky * ky);
		TargetPosition = FVector2D(AxisXDelta / ext, AxisYDelta / ext);
		//dx /= ext; dy /= ext;
	}

	DrawPosition += TargetPosition * DevideNumber;
	return  DrawPosition;
}

FVector2D FSlateWidgetMath::GetPositionInRadial(const FVector2D& InCenter, const float& InDegress, const float& InRadius)
{
	FVector2D sPosition(InRadius, InRadius);

	const float sCosSinSource = FMath::DegreesToRadians(InDegress);
	const float cCos = FMath::Cos(sCosSinSource);
	const float cSin = FMath::Sin(sCosSinSource);

	sPosition += FVector2D(InRadius * cCos, InRadius * cSin);
	return sPosition;
}
