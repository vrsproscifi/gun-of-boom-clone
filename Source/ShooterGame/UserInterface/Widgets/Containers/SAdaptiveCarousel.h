// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "Widgets/SCompoundWidget.h"

/**
 * 
 */
class SHOOTERGAME_API SAdaptiveCarouselPanel : public SPanel
{
	friend class SAdaptiveCarousel;

public:
	class FSlot
		: public TSlotBase<FSlot>
		, public TSupportsContentAlignmentMixin<FSlot>
		, public TSupportsContentPaddingMixin<FSlot>
	{
		friend class SAdaptiveCarousel;
		friend class SAdaptiveCarouselPanel;

	public:

		FSlot();
		FSlot& OnActivatedSlotEvent(const FOnClickedOutside& InEvent);


	protected:

		FOnClickedOutside OnActivatedSlot;
	};

	SLATE_BEGIN_ARGS(SAdaptiveCarouselPanel)
		: _DisplayedCells(3)
	{}
	SLATE_SUPPORTS_SLOT(SAdaptiveCarouselPanel::FSlot)
	SLATE_ATTRIBUTE(uint8, DisplayedCells)
	SLATE_END_ARGS()

	SAdaptiveCarouselPanel();

	void Construct(const FArguments& InArgs, const TArray<SAdaptiveCarouselPanel::FSlot*>& InSlots);

	static FSlot& Slot()
	{
		return *(new FSlot());
	}

	FSlot& AddSlot()
	{
		auto NewSlot = new FSlot();
		Children.Add(NewSlot);
		return *NewSlot;
	}

	virtual void OnArrangeChildren(const FGeometry& AllottedGeometry, FArrangedChildren& ArrangedChildren) const override;
	virtual FVector2D ComputeDesiredSize(float) const override;
	virtual FChildren* GetChildren() override;


protected:

	bool bLockAutoRevert;
	bool bForceAutoRevert;

	TAttribute<uint8> DisplayedCells;

	TPanelChildren<FSlot> Children;

	float CurrentOffset;
	float TargetOffset;
	float ForceOffset;

	uint16 CurrentCell;
	float CurrentCellFloat;

	virtual void Tick(const FGeometry& AllottedGeometry, const double InCurrentTime, const float InDeltaTime) override;

	float GetCellSize(const FVector2D& InLocalSize) const;
};

class SHOOTERGAME_API SAdaptiveCarousel : public SCompoundWidget
{
public:

	SLATE_BEGIN_ARGS(SAdaptiveCarousel)
		: _DisplayedCells(3)
	{}
	SLATE_SUPPORTS_SLOT(SAdaptiveCarouselPanel::FSlot)
	SLATE_ATTRIBUTE(uint8, DisplayedCells)
	SLATE_EVENT(FOnClickedOutside, OnDoubleTouch)
	SLATE_END_ARGS()

	void Construct(const FArguments& InArgs);

	static SAdaptiveCarouselPanel::FSlot& Slot()
	{
		return *(new SAdaptiveCarouselPanel::FSlot());
	}

	SAdaptiveCarouselPanel::FSlot& AddSlot();

	int32 GetNumChildrens() const;
	void RemoveSlot(const TSharedRef<SWidget>& WidgetToRemove);
	void ClearChildren();

	uint16 GetActiveSlotIndex() const;
	TSharedRef<SWidget> GetActiveSlotWidget() const;
	float GetCurrentSlot() const;

	bool ScrollToSlot(const int32& InSlotIndex, bool InAllowRecursiveSearchSlot = true);
	bool ScrollToWidget(const TSharedRef<SWidget>& WidgetToScroll);
	virtual FChildren* GetChildren() override;

protected:

	FOnClickedOutside OnDoubleTouch;

	TSharedPtr<SAdaptiveCarouselPanel> InnerPanel;

	int32 TouchIndex;
	float MovedDelta;
	double LastTouchEndTime;

	virtual FReply OnTouchStarted(const FGeometry& MyGeometry, const FPointerEvent& InTouchEvent) override;
	virtual FReply OnTouchMoved(const FGeometry& MyGeometry, const FPointerEvent& InTouchEvent) override;
	virtual FReply OnTouchEnded(const FGeometry& MyGeometry, const FPointerEvent& InTouchEvent) override;
};
