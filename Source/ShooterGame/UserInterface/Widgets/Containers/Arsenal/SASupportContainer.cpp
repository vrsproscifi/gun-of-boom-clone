// Fill out your copyright notice in the Description page of Project Settings.

#include "ShooterGame.h"
#include "SASupportContainer.h"
#include "SlateOptMacros.h"
#include "BasicPlayerItem.h"
#include "Localization/TableWeaponStrings.h"
#include "Widgets/Containers/STabbedContainer.h"
#include "Widgets/Containers/SAdaptiveCarousel.h"
#include "Components/SResourceTextBox.h"
#include "IdentityComponent.h"
#include "Localization/TableBaseStrings.h"
#include "Components/SArsenalItem.h"
#include "ShooterGameAnalytics.h"
#include "GrenadeItemEntity.h"
#include "AidItemEntity.h"
#include "SScaleBox.h"

BEGIN_SLATE_FUNCTION_BUILD_OPTIMIZATION
void SASupportContainer::Construct(const FArguments& InArgs)
{
	SBasicArsenalContainer::Construct
	(
		SBasicArsenalContainer::FArguments()
		.IdentityComponent(InArgs._IdentityComponent)
		.OnRequestUpgrade(InArgs._OnRequestUpgrade)
		.OnRequestBuy(InArgs._OnRequestBuy)
		.OnRequestSelect(InArgs._OnRequestSelect)
	);
}
END_SLATE_FUNCTION_BUILD_OPTIMIZATION

void SASupportContainer::FillCaruselTabs()
{
	AddCaruselTab(EGameItemType::Grenade, "UI:Lobby:Arsenal:Defends:Offensive");
	AddCaruselTab(EGameItemType::Stimulant, "UI:Lobby:Arsenal:Defends:Support");
}

EVisibility SASupportContainer::GetDiscountBlockVisibilityState() const
{
	auto TargetItem = GetActivePlayerItem();
	if (TargetItem.IsValid())
	{
		auto ItemEntity = TargetItem->GetEntityBase();
		if (ItemEntity->IsDiscountedProduct())
		{
			return EVisibility::HitTestInvisible;
		}
	}
	return EVisibility::Collapsed;
}


TSharedRef<SWidget> SASupportContainer::FactoryItemParametersBox()
{
	return
	SNew(SWidgetSwitcher)
	.WidgetIndex_Lambda([&]() { return Widget_TabbedContainer->GetActiveSlot(); })
	+ SWidgetSwitcher::Slot()
	[
		SNew(SHorizontalBox).Visibility(EVisibility::HitTestInvisible)
		+ SHorizontalBox::Slot().FillWidth(1).VAlign(VAlign_Bottom)
		[
			SNew(SVerticalBox)
			+ SVerticalBox::Slot().AutoHeight()[SNew(STextBlock).TextStyle(&Style->BigParameterName).Text(this, &SASupportContainer::GetOffensiveParameterName, EOffensiveParameter::Damage)]
			+ SVerticalBox::Slot().AutoHeight()[SNew(STextBlock).TextStyle(&Style->BigParameterValue).Text(this, &SASupportContainer::GetOffensiveParameterValue, EOffensiveParameter::Damage)]
		]
		+ SHorizontalBox::Slot().FillWidth(.9f).VAlign(VAlign_Bottom)
		[
			SNew(SVerticalBox)
			+ SVerticalBox::Slot().AutoHeight()[SNew(STextBlock).TextStyle(&Style->ParameterName).Text(this, &SASupportContainer::GetOffensiveParameterName, EOffensiveParameter::Radius)]
			+ SVerticalBox::Slot().AutoHeight()[SNew(STextBlock).TextStyle(&Style->ParameterValue).Text(this, &SASupportContainer::GetOffensiveParameterValue, EOffensiveParameter::Radius)]
		]
		+ SHorizontalBox::Slot().FillWidth(.9f).VAlign(VAlign_Bottom)
		[
			SNew(SVerticalBox)
			+ SVerticalBox::Slot().AutoHeight()[SNew(STextBlock).TextStyle(&Style->ParameterName).Text(this, &SASupportContainer::GetOffensiveParameterName, EOffensiveParameter::ActivateIn)]
			+ SVerticalBox::Slot().AutoHeight()[SNew(STextBlock).TextStyle(&Style->ParameterValue).Text(this, &SASupportContainer::GetOffensiveParameterValue, EOffensiveParameter::ActivateIn)]
		]
		//+ SHorizontalBox::Slot().FillWidth(.9f).VAlign(VAlign_Bottom)
	]
	+ SWidgetSwitcher::Slot()
	[
		SNew(SHorizontalBox).Visibility(EVisibility::HitTestInvisible)
		+ SHorizontalBox::Slot().FillWidth(1).VAlign(VAlign_Bottom)
		[
			SNew(SVerticalBox)
			+ SVerticalBox::Slot().AutoHeight()[SNew(STextBlock).TextStyle(&Style->BigParameterName).Text(this, &SASupportContainer::GetSupportParameterName, ESupportParameter::Health)]
			+ SVerticalBox::Slot().AutoHeight()[SNew(STextBlock).TextStyle(&Style->BigParameterValue).Text(this, &SASupportContainer::GetSupportParameterValue, ESupportParameter::Health)]
		]
		+ SHorizontalBox::Slot().FillWidth(.9f).VAlign(VAlign_Bottom)
		[
			SNew(SVerticalBox)
			+ SVerticalBox::Slot().AutoHeight()[SNew(STextBlock).TextStyle(&Style->ParameterName).Text(this, &SASupportContainer::GetSupportParameterName, ESupportParameter::ActiveTime)]
			+ SVerticalBox::Slot().AutoHeight()[SNew(STextBlock).TextStyle(&Style->ParameterValue).Text(this, &SASupportContainer::GetSupportParameterValue, ESupportParameter::ActiveTime)]
		]
		//+ SHorizontalBox::Slot().FillWidth(.9f).VAlign(VAlign_Bottom)
		//+ SHorizontalBox::Slot().FillWidth(.9f).VAlign(VAlign_Bottom)
	];
}

int32 SASupportContainer::GetActiveWidgetIndex() const
{
	auto TargetItem = GetActivePlayerItem();
	if (IsValidObject(TargetItem) && IsValidObject(IdentityComponent) && TargetItem->GetEntityBase())
	{
		if (TargetItem->HasIgnoreRequirements() || TargetItem->GetEntityBase()->GetItemProperty().Level <= static_cast<uint32>(IdentityComponent->GetPlayerInfo().Experience.Level))
		{
			if (TargetItem->HasAllowSomeBought())
			{
				return 1;
			}

			if (TargetItem->GetEntityId().IsValid())
			{
				return 0;
			}

			return 1;
		}
	}

	return 0;
}

FText SASupportContainer::GetOffensiveParameterName(EOffensiveParameter InTarget) const
{
	switch (InTarget)
	{
		case Damage:			return NSLOCTEXT("SASupportContainer", "SASupportContainer.Damage", "Damage");
		case Radius: 			return NSLOCTEXT("SASupportContainer", "SASupportContainer.Radius", "Radius");
		case ActivateIn: 		return NSLOCTEXT("SASupportContainer", "SASupportContainer.ActivateIn", "Activate In");
	}

	return FText::GetEmpty();
}

FText SASupportContainer::GetOffensiveParameterValue(EOffensiveParameter InTarget) const
{
	auto PlayerItem = GetActivePlayerItem();
	if (PlayerItem.IsValid())
	{
		if (auto TargetItem = PlayerItem->GetEntity<UGrenadeItemEntity>())
		{
			switch (InTarget)
			{
				case Damage:		return FText::AsNumber(TargetItem->GetGrenadeProperty().Damage, &FNumberFormattingOptions::DefaultNoGrouping());
				case Radius:		return FText::AsNumber(TargetItem->GetGrenadeProperty().DamageRadius / 100.0f, &FNumberFormattingOptions::DefaultNoGrouping());
				case ActivateIn:	return FText::AsNumber(TargetItem->GetGrenadeProperty().DetonationDelay, &FNumberFormattingOptions::DefaultNoGrouping());
			}
		}
	}

	return FText::GetEmpty();
}

FText SASupportContainer::GetSupportParameterName(ESupportParameter InTarget) const
{
	switch (InTarget)
	{
		case Health:				return NSLOCTEXT("SASupportContainer", "SASupportContainer.Health", "Health");
		case ActiveTime: 			return NSLOCTEXT("SASupportContainer", "SASupportContainer.ActiveTime", "Time");
	}

	return FText::GetEmpty();
}

FText SASupportContainer::GetSupportParameterValue(ESupportParameter InTarget) const
{
	auto PlayerItem = GetActivePlayerItem();
	if (PlayerItem.IsValid())
	{
		if (auto TargetItem = PlayerItem->GetEntity<UAidItemEntity>())
		{
			switch (InTarget)
			{
				case Health:		return FText::AsNumber(TargetItem->GetAidProperty().Amount, &FNumberFormattingOptions::DefaultNoGrouping());
				case ActiveTime:	return FText::AsNumber(TargetItem->GetAidProperty().ActivationTime, &FNumberFormattingOptions::DefaultNoGrouping());
			}
		}
	}

	return FText::GetEmpty();
}
