// Fill out your copyright notice in the Description page of Project Settings.

#include "ShooterGame.h"
#include "SBasicArsenalContainer.h"
#include "SlateOptMacros.h"
#include "Localization/TableWeaponStrings.h"
#include "Widgets/Containers/STabbedContainer.h"
#include "Widgets/Containers/SAdaptiveCarousel.h"
#include "BasicPlayerItem.h"
#include "Components/SResourceTextBox.h"
#include "Components/SArsenalItem.h"
#include "WeaponItemEntity.h"
#include "Localization/TableBaseStrings.h"
#include "IdentityComponent.h"
#include "ShooterGameAnalytics.h"
#include "Components/SDiscountBlock.h"
#include "SScaleBox.h"

BEGIN_SLATE_FUNCTION_BUILD_OPTIMIZATION
void SBasicArsenalContainer::Construct(const FArguments& InArgs)
{
	Style = InArgs._Style;
	IdentityComponent = InArgs._IdentityComponent;
	OnRequestBuy = InArgs._OnRequestBuy;
	OnRequestSelect = InArgs._OnRequestSelect;
	OnRequestUpgrade = InArgs._OnRequestUpgrade;
	OnActiveItem = InArgs._OnActiveItem;

	ChildSlot
	[
		SNew(SOverlay)
		+ SOverlay::Slot()
		[
			SNew(SVerticalBox)
			+ SVerticalBox::Slot() // Carusel
			[
				SAssignNew(Widget_TabbedContainer, STabbedContainer).Orientation(Orient_Horizontal).IsCenteredControlls(false)
			]
			+ SVerticalBox::Slot().AutoHeight() // Equipment|Buy/Equip|Skins Buttons
			[
				SNew(SHorizontalBox)
				+ SHorizontalBox::Slot()
				+ SHorizontalBox::Slot()
				[
					SNew(SButton).Tag(TEXT("SBasicArsenalContainer.Button.Action"))
					.IsEnabled(this, &SBasicArsenalContainer::GetActiveIsEnabled)
					.OnClicked(this, &SBasicArsenalContainer::OnClickedMultiButton)
					.HAlign(HAlign_Center)
					.VAlign(VAlign_Center)
					.ContentPadding(0)
					.ButtonStyle(&FShooterStyle::Get().GetWidgetStyle<FButtonStyle>("Default_ActionButton"))
					[
						SNew(SBox).HeightOverride(80).HAlign(HAlign_Center).VAlign(VAlign_Center)
						[
							SNew(SWidgetSwitcher)
							.WidgetIndex(this, &SBasicArsenalContainer::GetActiveWidgetIndex)
							+ SWidgetSwitcher::Slot() // Action text | Equip
							[
								SNew(STextBlock)
								.TextStyle(&FShooterStyle::Get().GetWidgetStyle<FTextBlockStyle>("Default_ActionText"))
								.Text(this, &SBasicArsenalContainer::GetActiveActionText)
							]
							+ SWidgetSwitcher::Slot() // Resource for buy
							[
								SNew(SHorizontalBox)
								+ SHorizontalBox::Slot().VAlign(VAlign_Top).AutoWidth()
								[
									SNew(SResourceTextBox)
									.IsEnabled(false)
									.CustomSize(FVector(32, 32, 14))
									.TypeAndValue(this, &SBasicArsenalContainer::GetOriginalItemCost)
									.Visibility(this, &SBasicArsenalContainer::GetOriginalItemCostVisibility)
									.OverlayMethod(EResourceTextBoxOverlayMethod::StrikeOut)
								]
								+ SHorizontalBox::Slot().AutoWidth()
								[
									SNew(SResourceTextBox)
									.CustomSize(FVector(64, 64, 28))								
									.TypeAndValue(this, &SBasicArsenalContainer::GetActualItemCost)
								]
							]
						]
					]
				]
				+ SHorizontalBox::Slot()
			]
		]
		+ SOverlay::Slot().VAlign(VAlign_Bottom).Padding(0, 140)
		[
			SNew(SHorizontalBox)
			.Visibility(this, &SBasicArsenalContainer::GetUpgrade2BlockVisibilityState)
			+ SHorizontalBox::Slot()
			+ SHorizontalBox::Slot().FillWidth(1.5f)
			[
				SNew(SVerticalBox)
				+ SVerticalBox::Slot().AutoHeight()
				[
					FactoryItemParametersBox()
				]
				+ SVerticalBox::Slot().AutoHeight().HAlign(HAlign_Right).Padding(90, 4)
				[
					SNew(SButton).Tag(TEXT("SBasicArsenalContainer.Button.Improve"))
					.TextStyle(&Style->ParameterUpgrade)
					.Text(NSLOCTEXT("SBasicArsenalContainer","SBasicArsenalContainer.Improve","Improve"))
					.ButtonStyle(&FShooterStyle::Get().GetWidgetStyle<FButtonStyle>("Default_ImproveButton"))
					.ContentPadding(10)
					.HAlign(HAlign_Center)
					.VAlign(VAlign_Center)
					.OnClicked(this, &SBasicArsenalContainer::OnUpgradeClicked)
					.Visibility(this, &SBasicArsenalContainer::GetUpgradeBlockVisibilityState)
				]
			]
			+ SHorizontalBox::Slot()
		]
		+ SOverlay::Slot().VAlign(VAlign_Center).HAlign(HAlign_Center).Padding(10)
		[
			SNew(SOverlay)
			.Visibility(this, &SBasicArsenalContainer::GetDisallowBlockVisibilityState)
			+ SOverlay::Slot()
			[
				SNew(SScaleBox)
				.Stretch(EStretch::ScaleToFit)
				[
					SNew(SImage).Image(&Style->DisallowItem)
				]
			]
			+ SOverlay::Slot().VAlign(VAlign_Center).HAlign(HAlign_Center)
			[
				SNew(SVerticalBox)
				+ SVerticalBox::Slot().AutoHeight()
				[
					SNew(STextBlock).Text(NSLOCTEXT("SArsenalContainer", "SArsenalContainer.DisallowItem", "Required"))
					.TextStyle(&Style->DisallowItemText)
					.Justification(ETextJustify::Center)
				]
				+ SVerticalBox::Slot().AutoHeight().Padding(10).HAlign(HAlign_Center)
				[
					SNew(SResourceTextBox)
					.Type(EResourceTextBoxType::Level)
					.Value(this, &SBasicArsenalContainer::GetItemLevel)
					.CustomSize(FVector(64, 64, 34))
				]
			]
		]
		+ SOverlay::Slot()
		[
			SNew(SHorizontalBox)
			.Visibility(this, &SBasicArsenalContainer::GetDiscountBlockVisibilityState)
			+ SHorizontalBox::Slot()
			+ SHorizontalBox::Slot().VAlign(VAlign_Center).HAlign(HAlign_Left)
			[
				SNew(SDiscountBlock)
				.Product(this, &SBasicArsenalContainer::GetActiveEntityItem)
			]
			+ SHorizontalBox::Slot()
		]
	];

	FillCaruselTabs();
}
END_SLATE_FUNCTION_BUILD_OPTIMIZATION

TSharedRef<SWidget> SBasicArsenalContainer::FactoryItemParametersBox()
{
	return SNew(SHorizontalBox).Visibility(EVisibility::HitTestInvisible);
}

int32 SBasicArsenalContainer::GetIndexOfTabWithItemType(const EGameItemType& InGameItemType) const
{
	auto Result = TabbedMap.Find(InGameItemType);
	if (Result)
	{
		return *Result;
	}
	return -1;
}


bool SBasicArsenalContainer::IsExistTabWithItemType(const EGameItemType& InGameItemType) const
{
	return GetIndexOfTabWithItemType(InGameItemType) >= 0;
}

bool SBasicArsenalContainer::SetActiveSlot(const int32& InSlotIndex)
{
	return Widget_TabbedContainer.IsValid() && Widget_TabbedContainer->SetActiveSlot(InSlotIndex);
}

TSharedPtr<SAdaptiveCarousel> SBasicArsenalContainer::GetItemsContainerByItemType(const EGameItemType& InGameItemType) const
{
	return Widget_ItemsContainer[static_cast<uint8>(InGameItemType)];
}



void SBasicArsenalContainer::FillCaruselTabs()
{

}

void SBasicArsenalContainer::AddCaruselTab(const EGameItemType& InItemType, const FString& InDescignEventName)
{
	//====================================================
	TabbedMap.Add(InItemType, TabbedMap.Num());

	//====================================================
	auto ItemTypeRaw = static_cast<uint8>(InItemType);

	//====================================================
	Widget_TabbedContainer->AddSlot
	(
		STabbedContainer::Slot().Name(FTableItemStrings::GetCategoryText(InItemType, true))
		.OnActivated_Lambda([&, InDescignEventName]() { FShooterGameAnalytics::RecordContentSearch(InDescignEventName); })
		[
			SAssignNew(Widget_ItemsContainer[ItemTypeRaw], SAdaptiveCarousel).OnDoubleTouch(this, &SBasicArsenalContainer::OnDoubleClickedItem)
		]
	);
}

EVisibility SBasicArsenalContainer::GetUpgrade2BlockVisibilityState() const
{
	if (GetActiveIsAllow())
	{
		return EVisibility::SelfHitTestInvisible;
	}
	return EVisibility::Collapsed;
}


EVisibility SBasicArsenalContainer::GetDisallowBlockVisibilityState() const
{
	if(GetActiveIsAllow())
	{ 
		return EVisibility::Collapsed;
	}
	return EVisibility::HitTestInvisible;
}


EVisibility SBasicArsenalContainer::GetDiscountBlockVisibilityState() const
{
	auto TargetItem = GetActivePlayerItem();
	if (TargetItem.IsValid())
	{
		if (TargetItem->GetState() == EPlayerItemState::None)
		{
			auto ItemEntity = TargetItem->GetEntityBase();
			if (ItemEntity->IsDiscountedProduct())
			{
				return EVisibility::HitTestInvisible;
			}
		}
	}
	return EVisibility::Collapsed;
}


EVisibility SBasicArsenalContainer::GetUpgradeBlockVisibilityState() const
{
	auto TargetItem = GetActivePlayerItem();
	if (IsValidObject(TargetItem) && TargetItem->GetEntityBase())
	{
		return TargetItem->GetEntityBase()->GetItemProperty().Modifications.Num() > 0 ? EVisibility::Visible : EVisibility::Hidden;
	}
	return EVisibility::Collapsed;
}

FReply SBasicArsenalContainer::OnUpgradeClicked()
{
	if (IsValidObject(GetActivePlayerItem()))
	{
		OnRequestUpgrade.ExecuteIfBound(GetActivePlayerItem().Get());
	}
	return FReply::Handled();
}

void SBasicArsenalContainer::OnFill(const TArray<UBasicPlayerItem*>& InItems)
{
	for (uint8 i = 0; i < static_cast<uint8>(EGameItemType::End); ++i)
	{
		if (Widget_ItemsContainer[i].IsValid())
		{
			Widget_ItemsContainer[i]->ClearChildren();
		}
	}

	uint8 _count[static_cast<uint8>(EGameItemType::End)] = { 0 };
	for (auto Item : InItems)
	{
		if (Item && Item->IsValidLowLevel())
		{
			if (Item->GetEntityBase())
			{
				auto Type = static_cast<uint8>(Item->GetEntityBase()->GetItemProperty().Type);
				if (Widget_ItemsContainer[Type].IsValid())
				{
					Widget_ItemsContainer[Type]->AddSlot().OnActivatedSlotEvent(FOnClickedOutside::CreateSP(this, &SBasicArsenalContainer::OnActiveSlot))
					[
						SNew(SArsenalItem, Item).ActiveCoefficient_Lambda([&, t = Type, i = _count[Type]]() -> float
						{
							const float fA = Widget_ItemsContainer[t]->GetCurrentSlot();
							const float fB = float(i);

							return FMath::Clamp(FMath::Abs<float>(fA - fB), .0f, 1.0f);
						})
					];

					++_count[Type];
				}
			}
		}
	}
}

void SBasicArsenalContainer::ScrollToActive(UBasicPlayerItem* InItem)
{
	if (InItem && InItem->IsValidLowLevel())
	{
		const auto TargetType = InItem->GetItemType();
		if (auto ActiveTab = TabbedMap.Find(TargetType))
		{
			Widget_TabbedContainer->SetActiveSlot(*ActiveTab);
			if (Widget_ItemsContainer[static_cast<uint8>(TargetType)].IsValid())
			{
				const auto Childs = Widget_ItemsContainer[static_cast<uint8>(TargetType)]->GetChildren();

				for (SIZE_T i = 0; i < Childs->Num(); ++i)
				{
					TSharedRef<SArsenalItem> MyWidget = StaticCastSharedRef<SArsenalItem>(Childs->GetChildAt(i));
					if (MyWidget != SNullWidget::NullWidget)
					{
						if (MyWidget->GetInstance().IsValid() && MyWidget->GetInstance().Get() == InItem)
						{
							Widget_ItemsContainer[static_cast<uint8>(TargetType)]->ScrollToWidget(MyWidget);
							break;
						}
					}
				}
			}
		}
	}
}

UBasicItemEntity* SBasicArsenalContainer::GetActiveEntityItem() const
{
	auto TargetItem = GetActivePlayerItem();
	if (IsValidObject(TargetItem))
	{
		return TargetItem->GetEntityBase();
	}

	return nullptr;
}

TWeakObjectPtr<UBasicPlayerItem> SBasicArsenalContainer::GetActivePlayerItem() const
{
	if (auto ActiveType = TabbedMap.FindKey(Widget_TabbedContainer->GetActiveSlot()))
	{
		auto MyType = static_cast<uint8>(*ActiveType);
		if (Widget_ItemsContainer[MyType].IsValid())
		{
			TSharedRef<SArsenalItem> MyWidget = StaticCastSharedRef<SArsenalItem>(Widget_ItemsContainer[MyType]->GetActiveSlotWidget());
			return MyWidget->GetInstance();
		}
	}

	return nullptr;
}

bool SBasicArsenalContainer::IsItemDiscounted() const
{
	auto MyItem = GetActiveEntityItem();
	if (MyItem)
	{
		return MyItem->IsDiscountedProduct();
	}
	return false;
}

EVisibility SBasicArsenalContainer::GetOriginalItemCostVisibility() const
{
	if (IsItemDiscounted())
	{
		return EVisibility::Visible;
	}
	return EVisibility::Collapsed;
}

FResourceTextBoxValue SBasicArsenalContainer::GetActualItemCost() const
{
	auto MyItem = GetActiveEntityItem();
	if (MyItem)
	{
		if (MyItem->IsDiscountedProduct())
		{
			auto Cost = MyItem->GetDiscountedCost();
			if (Cost)
			{
				return FResourceTextBoxValue::Factory(*Cost);
			}
		}

		return GetOriginalItemCost();
	}

	return FResourceTextBoxValue::Money(0);
}

FResourceTextBoxValue SBasicArsenalContainer::GetOriginalItemCost() const
{
	auto MyItem = GetActiveEntityItem();
	if (MyItem)
	{
		const auto cost = MyItem->GetNonDiscountedCost();
		return FResourceTextBoxValue::Factory(*cost);
	}

	return FResourceTextBoxValue::Money(0);
}

int32 SBasicArsenalContainer::GetActiveWidgetIndex() const
{
	auto TargetItem = GetActivePlayerItem();
	if (IsValidObject(TargetItem) && IsValidObject(IdentityComponent) && TargetItem->GetEntityBase())
	{
		if (TargetItem->GetEntityBase()->GetItemProperty().Level <= static_cast<uint32>(IdentityComponent->GetPlayerInfo().Experience.Level))
		{
			return TargetItem->GetEntityId().IsValid() == false;
		}
	}

	return 0;
}

FText SBasicArsenalContainer::GetActiveActionText() const
{
	auto TargetItem = GetActivePlayerItem();
	if (GetActiveIsEnabled() || (IsValidObject(TargetItem) && TargetItem->GetState() == EPlayerItemState::Active))
	{
		switch (TargetItem->GetState().GetValue())
		{
		case EPlayerItemState::Purchased: return FTableBaseStrings::GetBaseText(EBaseStrings::Equip);
		case EPlayerItemState::Equipped: return FTableBaseStrings::GetBaseText(EBaseStrings::Select);
		case EPlayerItemState::Active: return FTableBaseStrings::GetBaseText(EBaseStrings::Active);
		}
	}

	return FTableBaseStrings::GetBaseText(EBaseStrings::Unavailable);
}

bool SBasicArsenalContainer::GetActiveIsEnabled() const
{
	auto TargetItem = GetActivePlayerItem();
	if (IsValidObject(TargetItem) && IsValidObject(IdentityComponent) && TargetItem->GetEntityBase())
	{
		if (TargetItem->GetEntityBase()->GetItemProperty().Level <= static_cast<uint32>(IdentityComponent->GetPlayerInfo().Experience.Level) && TargetItem->GetState() != EPlayerItemState::Active)
		{
			return true;
		}
	}

	return false;
}

bool SBasicArsenalContainer::GetActiveIsAllow() const
{
	auto TargetItem = GetActivePlayerItem();
	if (IsValidObject(TargetItem) && IsValidObject(IdentityComponent) && TargetItem->GetEntityBase())
	{
		if (TargetItem->GetEntityBase()->GetItemProperty().Level <= static_cast<uint32>(IdentityComponent->GetPlayerInfo().Experience.Level))
		{
			return true;
		}
	}

	return false;
}

FReply SBasicArsenalContainer::OnClickedMultiButton()
{
	bool IsHandled = false;
	auto TargetItem = GetActivePlayerItem();
	if (IsValidObject(TargetItem))
	{
		if (GetActiveWidgetIndex() == 0)
		{
			IsHandled = OnRequestSelect.ExecuteIfBound(GetActivePlayerItem().Get());
		}
		else if (GetActiveWidgetIndex() == 1)
		{
			IsHandled = OnRequestBuy.ExecuteIfBound(GetActivePlayerItem().Get());
		}
	}

	return IsHandled ? FReply::Handled() : FReply::Unhandled();
}

void SBasicArsenalContainer::OnDoubleClickedItem()
{
	OnClickedMultiButton();
}

void SBasicArsenalContainer::OnActiveSlot()
{
	OnActiveItem.ExecuteIfBound(GetActivePlayerItem().Get());
}

int64 SBasicArsenalContainer::GetItemLevel() const
{
	if (auto EntityItem = GetValidObject(GetActiveEntityItem()))
	{
		return EntityItem->GetItemProperty().Level;
	}

	return 0;
}
