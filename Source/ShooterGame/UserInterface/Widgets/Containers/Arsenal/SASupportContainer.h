// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "SBasicArsenalContainer.h"


class SHOOTERGAME_API SASupportContainer : public SBasicArsenalContainer
{
public:
	SLATE_BEGIN_ARGS(SASupportContainer) { }
	SLATE_STYLE_ARGUMENT(FArsenalContainerStyle, Style)
	SLATE_EVENT(FOnItemAction, OnRequestBuy)
	SLATE_EVENT(FOnItemAction, OnRequestSelect)
	SLATE_EVENT(FOnItemAction, OnRequestUpgrade)
	SLATE_ARGUMENT(TWeakObjectPtr<UIdentityComponent>, IdentityComponent)
	SLATE_END_ARGS()

	void Construct(const FArguments& InArgs);

protected:

	virtual void FillCaruselTabs() override;
	virtual TSharedRef<SWidget> FactoryItemParametersBox() override;
	virtual int32 GetActiveWidgetIndex() const override;


	enum EOffensiveParameter
	{
		Damage,
		Radius,
		ActivateIn,
		OffensiveEnd
	};

	enum ESupportParameter
	{
		Health,
		ActiveTime,
		SupportEnd
	};

	virtual EVisibility GetDiscountBlockVisibilityState() const override;

	FText GetOffensiveParameterName(EOffensiveParameter InTarget) const;
	FText GetOffensiveParameterValue(EOffensiveParameter InTarget) const;

	FText GetSupportParameterName(ESupportParameter InTarget) const;
	FText GetSupportParameterValue(ESupportParameter InTarget) const;
};