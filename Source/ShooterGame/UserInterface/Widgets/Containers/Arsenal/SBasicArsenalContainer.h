// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "Input/Reply.h"
#include "Widgets/SCompoundWidget.h"
#include "Styles/ArsenalContainerWidgetStyle.h"
#include "SImprovementContainer.h"

class UBasicPlayerItem;
/**
 * 
 */
class SHOOTERGAME_API SBasicArsenalContainer : public SCompoundWidget
{
public:
	SLATE_BEGIN_ARGS(SBasicArsenalContainer)
		: _Style(&FShooterStyle::Get().GetWidgetStyle<FArsenalContainerStyle>("SArsenalContainerStyle"))
	{
		_Visibility = EVisibility::SelfHitTestInvisible;
	}
	SLATE_STYLE_ARGUMENT(FArsenalContainerStyle, Style)
	SLATE_EVENT(FOnItemAction, OnRequestBuy)
	SLATE_EVENT(FOnItemAction, OnRequestSelect)
	SLATE_EVENT(FOnItemAction, OnRequestUpgrade)
	SLATE_EVENT(FOnItemAction, OnActiveItem)
	SLATE_ARGUMENT(TWeakObjectPtr<UIdentityComponent>, IdentityComponent)
	SLATE_END_ARGS()

	void Construct(const FArguments& InArgs);

	void OnFill(const TArray<UBasicPlayerItem*>& InItems);
	void ScrollToActive(UBasicPlayerItem* InItem);

	bool SetActiveSlot(const int32& InSlotIndex);
	bool IsExistTabWithItemType(const EGameItemType& InGameItemType) const;
	int32 GetIndexOfTabWithItemType(const EGameItemType& InGameItemType) const;

	TSharedPtr<SAdaptiveCarousel> GetItemsContainerByItemType(const EGameItemType& InGameItemType) const;

protected:
	//==========================================
	const FArsenalContainerStyle* Style;

	//==========================================
	TMap<EGameItemType, uint8>			TabbedMap;
	TSharedPtr<STabbedContainer>		Widget_TabbedContainer;
	TSharedPtr<SAdaptiveCarousel>		Widget_ItemsContainer[static_cast<uint8>(EGameItemType::End)];

	//==========================================
	TWeakObjectPtr<UIdentityComponent> IdentityComponent;

	//==========================================
	FOnItemAction OnRequestBuy;
	FOnItemAction OnRequestSelect;
	FOnItemAction OnRequestUpgrade;
	FOnItemAction OnActiveItem;
	
	//==========================================
	virtual void FillCaruselTabs();
	virtual void AddCaruselTab(const EGameItemType& InItemType, const FString& InDescignEventName);

	//==========================================
	virtual TSharedRef<SWidget> FactoryItemParametersBox();

	//==========================================
	UBasicItemEntity* GetActiveEntityItem() const;
	TWeakObjectPtr<UBasicPlayerItem> GetActivePlayerItem() const;

	//==========================================
	virtual EVisibility GetDiscountBlockVisibilityState() const;
	EVisibility GetOriginalItemCostVisibility() const;
	EVisibility GetUpgradeBlockVisibilityState() const;
	EVisibility GetUpgrade2BlockVisibilityState() const;
	EVisibility GetDisallowBlockVisibilityState() const;

	//==========================================
	FResourceTextBoxValue GetOriginalItemCost() const;
	FResourceTextBoxValue GetActualItemCost() const;

	//==========================================
	bool IsItemDiscounted() const;
	int64 GetItemLevel() const;


	//==========================================
	virtual int32 GetActiveWidgetIndex() const;
	virtual FText GetActiveActionText() const;

	//==========================================
	bool GetActiveIsEnabled() const;
	bool GetActiveIsAllow() const;

	//==========================================
	FReply OnUpgradeClicked();
	FReply OnClickedMultiButton();
	void OnDoubleClickedItem();
	void OnActiveSlot();

};
