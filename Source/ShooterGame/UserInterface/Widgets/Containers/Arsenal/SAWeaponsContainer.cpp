// Fill out your copyright notice in the Description page of Project Settings.

#include "ShooterGame.h"
#include "SAWeaponsContainer.h"
#include "SlateOptMacros.h"
#include "Localization/TableWeaponStrings.h"
#include "Widgets/Containers/STabbedContainer.h"
#include "Widgets/Containers/SAdaptiveCarousel.h"
#include "BasicPlayerItem.h"
#include "Components/SResourceTextBox.h"
#include "Components/SArsenalItem.h"
#include "WeaponItemEntity.h"
#include "Localization/TableBaseStrings.h"
#include "IdentityComponent.h"
#include "ShooterGameAnalytics.h"
#include "SScaleBox.h"
#include "KnifeItemEntity.h"

BEGIN_SLATE_FUNCTION_BUILD_OPTIMIZATION
void SAWeaponsContainer::Construct(const FArguments& InArgs)
{
	SBasicArsenalContainer::Construct
	(
		SBasicArsenalContainer::FArguments()
		.IdentityComponent(InArgs._IdentityComponent)
		.OnRequestUpgrade(InArgs._OnRequestUpgrade)
		.OnRequestBuy(InArgs._OnRequestBuy)
		.OnRequestSelect(InArgs._OnRequestSelect)
		.OnActiveItem(InArgs._OnActiveItem)
	);
}
END_SLATE_FUNCTION_BUILD_OPTIMIZATION

void SAWeaponsContainer::FillCaruselTabs()
{
	AddCaruselTab(EGameItemType::Rifle, "UI:Lobby:Arsenal:Weapons:Rifle");
	AddCaruselTab(EGameItemType::Shotgun, "UI:Lobby:Arsenal:Weapons:Shotgun");
	AddCaruselTab(EGameItemType::SniperRifle, "UI:Lobby:Arsenal:Weapons:SniperRifle");
	AddCaruselTab(EGameItemType::MachineGun, "UI:Lobby:Arsenal:Weapons:MachineGun");
	AddCaruselTab(EGameItemType::Pistol, "UI:Lobby:Arsenal:Weapons:Pistol");
	AddCaruselTab(EGameItemType::Knife, "UI:Lobby:Arsenal:Weapons:Knife");
}

TSharedRef<SWidget> SAWeaponsContainer::FactoryItemParametersBox()
{
	return
	SNew(SWidgetSwitcher)
	.WidgetIndex_Lambda([&]()
	{
		return Cast<UWeaponItemEntity>(GetActiveEntityItem()) != nullptr ? 0 : 1;
	})
	+ SWidgetSwitcher::Slot()
	[
		SNew(SHorizontalBox).Visibility(EVisibility::HitTestInvisible)
		+ SHorizontalBox::Slot().FillWidth(1).VAlign(VAlign_Bottom)
		[
			SNew(SVerticalBox)
			+ SVerticalBox::Slot().AutoHeight()[SNew(STextBlock).TextStyle(&Style->BigParameterName).Text(this, &SAWeaponsContainer::GetWeaponParameterName, EWeaponParameter::Damage)]
			+ SVerticalBox::Slot().AutoHeight()[SNew(STextBlock).TextStyle(&Style->BigParameterValue).Text(this, &SAWeaponsContainer::GetWeaponParameterValue, EWeaponParameter::Damage)]
		]
		+ SHorizontalBox::Slot().FillWidth(.9f).VAlign(VAlign_Bottom)
		[
			SNew(SVerticalBox)
			+ SVerticalBox::Slot().AutoHeight()[SNew(STextBlock).TextStyle(&Style->ParameterName).Text(this, &SAWeaponsContainer::GetWeaponParameterName, EWeaponParameter::Accurancy)]
			+ SVerticalBox::Slot().AutoHeight()[SNew(STextBlock).TextStyle(&Style->ParameterValue).Text(this, &SAWeaponsContainer::GetWeaponParameterValue, EWeaponParameter::Accurancy)]
		]
		+ SHorizontalBox::Slot().FillWidth(.9f).VAlign(VAlign_Bottom)
		[
			SNew(SVerticalBox)
			+ SVerticalBox::Slot().AutoHeight()[SNew(STextBlock).TextStyle(&Style->ParameterName).Text(this, &SAWeaponsContainer::GetWeaponParameterName, EWeaponParameter::Distance)]
			+ SVerticalBox::Slot().AutoHeight()[SNew(STextBlock).TextStyle(&Style->ParameterValue).Text(this, &SAWeaponsContainer::GetWeaponParameterValue, EWeaponParameter::Distance)]
		]
		+ SHorizontalBox::Slot().FillWidth(.9f).VAlign(VAlign_Bottom)
		[
			SNew(SVerticalBox)
			+ SVerticalBox::Slot().AutoHeight()[SNew(STextBlock).TextStyle(&Style->ParameterName).Text(this, &SAWeaponsContainer::GetWeaponParameterName, EWeaponParameter::Clip)]
			+ SVerticalBox::Slot().AutoHeight()[SNew(STextBlock).TextStyle(&Style->ParameterValue).Text(this, &SAWeaponsContainer::GetWeaponParameterValue, EWeaponParameter::Clip)]
		]
	]
	+ SWidgetSwitcher::Slot()
	[
		SNew(SHorizontalBox).Visibility(EVisibility::HitTestInvisible)
		+ SHorizontalBox::Slot().FillWidth(1).VAlign(VAlign_Bottom)
		[
			SNew(SVerticalBox)
			+ SVerticalBox::Slot().AutoHeight()[SNew(STextBlock).TextStyle(&Style->BigParameterName).Text(this, &SAWeaponsContainer::GetKnifeParameterName, EKnifeParameter::KnifeDamage)]
			+ SVerticalBox::Slot().AutoHeight()[SNew(STextBlock).TextStyle(&Style->BigParameterValue).Text(this, &SAWeaponsContainer::GetKnifeParameterValue, EKnifeParameter::KnifeDamage)]
		]
		+ SHorizontalBox::Slot().FillWidth(.9f).VAlign(VAlign_Bottom)
		[
			SNew(SVerticalBox)
			+ SVerticalBox::Slot().AutoHeight()[SNew(STextBlock).TextStyle(&Style->ParameterName).Text(this, &SAWeaponsContainer::GetKnifeParameterName, EKnifeParameter::KnifeTime)]
			+ SVerticalBox::Slot().AutoHeight()[SNew(STextBlock).TextStyle(&Style->ParameterValue).Text(this, &SAWeaponsContainer::GetKnifeParameterValue, EKnifeParameter::KnifeTime)]
		]
	];
}

FText SAWeaponsContainer::GetWeaponParameterName(EWeaponParameter InTarget) const
{
	if (auto TargetItem = Cast<UWeaponItemEntity>(GetActiveEntityItem()))
	{
		switch (InTarget)
		{
			case Damage:		return NSLOCTEXT("SAWeaponsContainer","SAWeaponsContainer.Damage","Damage");
			case Accurancy: 	return NSLOCTEXT("SAWeaponsContainer","SAWeaponsContainer.Accurancy","Accurancy");
			case Distance: 		return NSLOCTEXT("SAWeaponsContainer","SAWeaponsContainer.Distance","Distance");
			case Clip: 			return NSLOCTEXT("SAWeaponsContainer","SAWeaponsContainer.Clip","Clip");
		}
	}

	return FText::GetEmpty();
}

FText SAWeaponsContainer::GetWeaponParameterValue(EWeaponParameter InTarget) const
{
	auto PlayerItem = GetActivePlayerItem();
	if (PlayerItem.IsValid())
	{
		if (auto TargetItem = PlayerItem->GetEntity<UWeaponItemEntity>())
		{
			switch (InTarget)
			{
				case Damage:
				{
					const auto TargetSlot = PlayerItem->GetLevel();
					return FText::AsNumber(TargetItem->GetDynamicValueForLevel(TargetSlot) * TargetItem->GetWeaponConfiguration().BulletsMin, &FNumberFormattingOptions::DefaultNoGrouping());
				}
				case Accurancy:		return FText::AsNumber(TargetItem->GetWeaponConfiguration().Spread, &FNumberFormattingOptions::DefaultNoGrouping());
				case Distance:		return FText::AsNumber(TargetItem->GetWeaponConfiguration().Range, &FNumberFormattingOptions::DefaultNoGrouping());
				case Clip:			return FText::AsNumber(TargetItem->GetWeaponConfiguration().AmmoPerClip, &FNumberFormattingOptions::DefaultNoGrouping());
			}
		}
	}

	return FText::GetEmpty();
}

FText SAWeaponsContainer::GetKnifeParameterName(EKnifeParameter InTarget) const
{
	if (auto TargetItem = Cast<UKnifeItemEntity>(GetActiveEntityItem()))
	{
		switch (InTarget)
		{
			case KnifeDamage:		return NSLOCTEXT("SAWeaponsContainer", "SAWeaponsContainer.Damage", "Damage");
			case KnifeTime: 		return NSLOCTEXT("SAWeaponsContainer", "SAWeaponsContainer.KnifeTime", "Activate Time");
		}
	}

	return FText::GetEmpty();
}

FText SAWeaponsContainer::GetKnifeParameterValue(EKnifeParameter InTarget) const
{
	auto PlayerItem = GetActivePlayerItem();
	if (PlayerItem.IsValid())
	{
		if (auto TargetItem = PlayerItem->GetEntity<UKnifeItemEntity>())
		{
			switch (InTarget)
			{
				case KnifeDamage:
				{
					const auto TargetSlot = PlayerItem->GetLevel();
					return FText::AsNumber(TargetItem->GetDynamicValueForLevel(TargetSlot), &FNumberFormattingOptions::DefaultNoGrouping());
				}
				case KnifeTime:		return FText::AsNumber(TargetItem->GetKnifeProperty().ActivationInterval, &FNumberFormattingOptions::DefaultNoGrouping());
			}
		}
	}

	return FText::GetEmpty();
}
