// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "Utilities/SUsableCompoundWidget.h"
#include "Components/SResourceTextBoxValue.h"
#include "BasicItemEntity.h"
#include "Lobby/SArsenalContainer.h"

DECLARE_DELEGATE_TwoParams(FOnItemUpgradeAction, const UBasicPlayerItem*, const int32&);


class UBasicPlayerItem;
class SAdaptiveCarousel;
class SAnimatedBackground;
/**
 * 
 */
class SHOOTERGAME_API SImprovementContainer : public SUsableCompoundWidget
{
public:
	SLATE_BEGIN_ARGS(SImprovementContainer)
	{
		_Visibility = EVisibility::SelfHitTestInvisible;
	}
	SLATE_EVENT(FOnItemUpgradeAction, OnRequestBuy)
	SLATE_END_ARGS()

	void Construct(const FArguments& InArgs);

	virtual void ToggleWidget(const bool InToggle) override;
	virtual bool IsInInteractiveMode() const override;

	void RequestScroll();

	TWeakObjectPtr<UBasicPlayerItem> GetActivePlayerItem() const;
	UBasicItemEntity* GetActiveEntityItem() const;
	void SetInstance(const TWeakObjectPtr<UBasicPlayerItem>& InInstance);

protected:

	FOnItemUpgradeAction OnRequestBuy;

	TWeakObjectPtr<UBasicPlayerItem>	Instance;

	TSharedPtr<SAnimatedBackground>		Anim_Back;
	TSharedPtr<SAdaptiveCarousel>		Widget_ItemsContainer;	

	FText GetItemName() const;
	FText GetBuyButtonText() const;
	FResourceTextBoxValue GetActiveItemCost() const;
	bool IsAllowBuy() const;
	EVisibility IsImprovement() const;
	bool IsEnabled() const;

	float GetImprovementValue() const;
	FSlateColor GetImprovementValueColor() const;

	FReply OnClickedBuyButton();


	enum EWeaponParameter
	{
		Damage,
		Accurancy,
		Distance,
		Clip,
		End
	};

	enum EKnifeParameter
	{
		KnifeDamage,
		KnifeTime,
		KnifeEnd
	};

	FText GetWeaponParameterName(EWeaponParameter InParameter) const;
	FText GetWeaponParameterValue(EWeaponParameter InParameter) const;
	FText GetImprovementValueText() const;

	FText GetKnifeParameterName(EKnifeParameter InTarget) const;
	FText GetKnifeParameterValue(EKnifeParameter InTarget) const;
};
