// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "SBasicArsenalContainer.h"

class UBasicPlayerItem;
/**
 * 
 */
class SHOOTERGAME_API SADefendContainer : public SBasicArsenalContainer
{
public:
	SLATE_BEGIN_ARGS(SADefendContainer){}
	SLATE_EVENT(FOnItemAction, OnRequestBuy)
	SLATE_EVENT(FOnItemAction, OnRequestSelect)
	SLATE_EVENT(FOnItemAction, OnRequestUpgrade)
	SLATE_ARGUMENT(TWeakObjectPtr<UIdentityComponent>, IdentityComponent)
	SLATE_END_ARGS()

	void Construct(const FArguments& InArgs);

protected:

	virtual void FillCaruselTabs() override;
	virtual TSharedRef<SWidget> FactoryItemParametersBox() override;

	virtual int32 GetActiveWidgetIndex() const override;
	virtual FText GetActiveActionText() const override;

	enum EArmourParameter
	{
		Amount,
		ArmourEnd
	};

	FText GetArmourParameterName(EArmourParameter InTarget) const;
	FText GetArmourParameterValue(EArmourParameter InTarget) const;
};
