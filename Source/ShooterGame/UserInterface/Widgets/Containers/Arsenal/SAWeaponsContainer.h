// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "SBasicArsenalContainer.h"

class SHOOTERGAME_API SAWeaponsContainer : public SBasicArsenalContainer
{
public:
	SLATE_BEGIN_ARGS(SAWeaponsContainer){}
	SLATE_EVENT(FOnItemAction, OnRequestBuy)
	SLATE_EVENT(FOnItemAction, OnRequestSelect)
	SLATE_EVENT(FOnItemAction, OnRequestUpgrade)
	SLATE_EVENT(FOnItemAction, OnActiveItem)
	SLATE_ARGUMENT(TWeakObjectPtr<UIdentityComponent>, IdentityComponent)
	SLATE_END_ARGS()

	void Construct(const FArguments& InArgs);

protected:

	virtual void FillCaruselTabs() override;
	virtual TSharedRef<SWidget> FactoryItemParametersBox() override;


	enum EWeaponParameter
	{
		Damage,
		Accurancy,
		Distance,
		Clip,
		End
	};

	enum EKnifeParameter
	{
		KnifeDamage,
		KnifeTime,
		KnifeEnd
	};

	FText GetWeaponParameterName(EWeaponParameter InTarget) const;
	FText GetWeaponParameterValue(EWeaponParameter InTarget) const;

	FText GetKnifeParameterName(EKnifeParameter InTarget) const;
	FText GetKnifeParameterValue(EKnifeParameter InTarget) const;
};
