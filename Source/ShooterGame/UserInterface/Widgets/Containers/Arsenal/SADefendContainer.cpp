// Fill out your copyright notice in the Description page of Project Settings.

#include "ShooterGame.h"
#include "SADefendContainer.h"
#include "SlateOptMacros.h"
#include "Localization/TableWeaponStrings.h"
#include "Widgets/Containers/STabbedContainer.h"
#include "Widgets/Containers/SAdaptiveCarousel.h"
#include "BasicPlayerItem.h"
#include "Components/SArsenalItem.h"
#include "Components/SResourceTextBox.h"
#include "IdentityComponent.h"
#include "Localization/TableBaseStrings.h"
#include "ShooterGameAnalytics.h"
#include "ShooterGameAnalytics.h"
#include "ArmourItemEntity.h"
#include "SScaleBox.h"

BEGIN_SLATE_FUNCTION_BUILD_OPTIMIZATION
void SADefendContainer::Construct(const FArguments& InArgs)
{
	SBasicArsenalContainer::Construct
	(
		SBasicArsenalContainer::FArguments()
		.IdentityComponent(InArgs._IdentityComponent)
		.OnRequestUpgrade(InArgs._OnRequestUpgrade)
		.OnRequestBuy(InArgs._OnRequestBuy)
		.OnRequestSelect(InArgs._OnRequestSelect)
	);
}
END_SLATE_FUNCTION_BUILD_OPTIMIZATION

void SADefendContainer::FillCaruselTabs()
{
	AddCaruselTab(EGameItemType::Head, "UI:Lobby:Arsenal:Defends:Head");
	AddCaruselTab(EGameItemType::Torso, "UI:Lobby:Arsenal:Defends:Torso");
	AddCaruselTab(EGameItemType::Legs, "UI:Lobby:Arsenal:Defends:Legs");
}

TSharedRef<SWidget> SADefendContainer::FactoryItemParametersBox()
{
	return
	SNew(SHorizontalBox).Visibility(EVisibility::HitTestInvisible)
	+ SHorizontalBox::Slot().FillWidth(1).VAlign(VAlign_Bottom)
	[
		SNew(SVerticalBox)
		+ SVerticalBox::Slot().AutoHeight()[SNew(STextBlock).TextStyle(&Style->BigParameterName).Text(this, &SADefendContainer::GetArmourParameterName, EArmourParameter::Amount)]
		+ SVerticalBox::Slot().AutoHeight()[SNew(STextBlock).TextStyle(&Style->BigParameterValue).Text(this, &SADefendContainer::GetArmourParameterValue, EArmourParameter::Amount)]
	];
}

int32 SADefendContainer::GetActiveWidgetIndex() const
{
	auto TargetItem = GetActivePlayerItem();
	if (IsValidObject(TargetItem) && IsValidObject(IdentityComponent) && TargetItem->GetEntityBase())
	{
		if (TargetItem->HasIgnoreRequirements() || TargetItem->GetEntityBase()->GetItemProperty().Level <= static_cast<uint32>(IdentityComponent->GetPlayerInfo().Experience.Level))
		{
			if (TargetItem->HasAllowSomeBought())
			{
				return 1;
			}

			if (TargetItem->GetEntityId().IsValid())
			{
				return 0;
			}

			return 1;
		}
	}

	return 0;
}

FText SADefendContainer::GetActiveActionText() const
{
	auto TargetItem = GetActivePlayerItem();
	if (IsValidObject(TargetItem) && IsValidObject(IdentityComponent) && TargetItem->GetEntityBase())
	{
		switch (TargetItem->GetState().GetValue())
		{
			case EPlayerItemState::Purchased: return FTableBaseStrings::GetBaseText(EBaseStrings::Equip);
			case EPlayerItemState::Equipped: return FTableBaseStrings::GetBaseText(EBaseStrings::Equipped);
			case EPlayerItemState::Active: return FTableBaseStrings::GetBaseText(EBaseStrings::Active);
		}
	}

	return FTableBaseStrings::GetBaseText(EBaseStrings::Unavailable);
}

FText SADefendContainer::GetArmourParameterName(EArmourParameter InTarget) const
{
	switch (InTarget)
	{
		case Amount:			return NSLOCTEXT("SADefendContainer", "SADefendContainer.Amount", "Armour");
	}

	return FText::GetEmpty();
}

FText SADefendContainer::GetArmourParameterValue(EArmourParameter InTarget) const
{
	auto PlayerItem = GetActivePlayerItem();
	if (PlayerItem.IsValid())
	{
		if (auto TargetItem = PlayerItem->GetEntity<UArmourItemEntity>())
		{
			switch (InTarget)
			{
				case Amount:		return FText::AsNumber(TargetItem->GetArmourProperty().Armour, &FNumberFormattingOptions::DefaultNoGrouping());
			}
		}
	}

	return FText::GetEmpty();
}
