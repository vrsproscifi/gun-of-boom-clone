// Fill out your copyright notice in the Description page of Project Settings.

#include "ShooterGame.h"
#include "SImprovementContainer.h"
#include "SlateOptMacros.h"
#include "Utilities/SAnimatedBackground.h"
#include "Components/SCurrencyBlock.h"
#include "Widgets/Containers/SAdaptiveCarousel.h"
#include "Components/SResourceTextBox.h"
#include "Decorators/LokaTextDecorator.h"
#include "Decorators/LokaResourceDecorator.h"
#include "Components/SArsenalUpgradeItem.h"
#include "BasicPlayerItem.h"
#include "BasicItemEntity.h"
#include "WeaponItemEntity.h"
#include "KnifeItemEntity.h"

BEGIN_SLATE_FUNCTION_BUILD_OPTIMIZATION
void SImprovementContainer::Construct(const FArguments& InArgs)
{
	OnRequestBuy = InArgs._OnRequestBuy;

	auto ShopItemStyle = &FShooterStyle::Get().GetWidgetStyle<FShopItemStyle>("SShopItemStyle");
	auto ArsenalContainerStyle = &FShooterStyle::Get().GetWidgetStyle<FArsenalContainerStyle>("SArsenalContainerStyle");

	ChildSlot
	[
		SAssignNew(Anim_Back, SAnimatedBackground)
		.Visibility(EVisibility::Visible)
		.IsEnabledBlur(true)
		.Duration(.5f)
		.ShowAnimation(EAnimBackAnimation::UpFade)
		.HideAnimation(EAnimBackAnimation::DownFade)
		.InitAsHide(true)
		[
			SNew(SOverlay)
			+ SOverlay::Slot()
			[
				SNew(SVerticalBox)
				+ SVerticalBox::Slot().AutoHeight() // Back btn
				[
					SNew(SOverlay)
					+ SOverlay::Slot().VAlign(VAlign_Center)
					[
						SNew(SHorizontalBox)
						+ SHorizontalBox::Slot().AutoWidth()
						[
							SNew(SButton)
							.ButtonStyle(&FShooterStyle::Get().GetWidgetStyle<FButtonStyle>("Default_BackButton"))
							.ContentPadding(80)
							.HAlign(HAlign_Center)
							.VAlign(VAlign_Center)
							.OnClicked_Lambda([&]()
							{
								ToggleWidget(false);
								return FReply::Handled();
							})
						]
						+ SHorizontalBox::Slot()
						+ SHorizontalBox::Slot().AutoWidth()
						[
							SCurrencyBlock::Get()->Generate(true)
						]
					]
					+ SOverlay::Slot().HAlign(HAlign_Center).Padding(40)
					[
						SNew(SRichTextBlock)
						.Justification(ETextJustify::Center)
						.Text(this, &SImprovementContainer::GetItemName)
						.TextStyle(&ShopItemStyle->Title)
						.AutoWrapText(true)
						+ SRichTextBlock::Decorator(FLokaTextDecorator::Create(ShopItemStyle->Title))
						+ SRichTextBlock::Decorator(FLokaResourceDecorator::Create())
					]
				]
				+ SVerticalBox::Slot() // Carusel
				[
					SAssignNew(Widget_ItemsContainer, SAdaptiveCarousel)
				]
				+ SVerticalBox::Slot().AutoHeight()
				[
					SNew(SHorizontalBox)
					+ SHorizontalBox::Slot()
					+ SHorizontalBox::Slot()
					[
						SNew(SButton)
						.Visibility(this, &SImprovementContainer::IsImprovement)
						.IsEnabled_Raw(this, &SImprovementContainer::IsEnabled)
						.ButtonStyle(&FShooterStyle::Get().GetWidgetStyle<FButtonStyle>("Default_ActionButton"))
						.OnClicked(this, &SImprovementContainer::OnClickedBuyButton)
						.HAlign(HAlign_Center)
						.VAlign(VAlign_Center)
						.ContentPadding(0)
						[
							SNew(SBox).HeightOverride(80).HAlign(HAlign_Center).VAlign(VAlign_Center)
							[
								SNew(SResourceTextBox)
								.CustomSize(FVector(64, 64, 28))
								.TypeAndValue(this, &SImprovementContainer::GetActiveItemCost)
							]
						]
					]
					+ SHorizontalBox::Slot()
				]
			]
			+ SOverlay::Slot().VAlign(VAlign_Bottom).Padding(0, 180)
			[
				SNew(SHorizontalBox)
				+ SHorizontalBox::Slot()
				+ SHorizontalBox::Slot().FillWidth(1.5f)
				[
					SNew(SVerticalBox)
					+ SVerticalBox::Slot().AutoHeight()
					[
						SNew(STextBlock)
						.TextStyle(&ArsenalContainerStyle->ParameterValue)
						.ColorAndOpacity(this, &SImprovementContainer::GetImprovementValueColor)
						.Text(this, &SImprovementContainer::GetImprovementValueText)
					]
					+ SVerticalBox::Slot().AutoHeight()
					[
						SNew(SWidgetSwitcher)
						.WidgetIndex_Lambda([&]()
						{
							return Cast<UWeaponItemEntity>(GetActiveEntityItem()) != nullptr ? 0 : 1;
						})
						+ SWidgetSwitcher::Slot()
						[
							SNew(SHorizontalBox).Visibility(EVisibility::HitTestInvisible)
							+ SHorizontalBox::Slot().FillWidth(1).VAlign(VAlign_Bottom)
							[
								SNew(SVerticalBox)
								+ SVerticalBox::Slot().AutoHeight()[SNew(STextBlock).TextStyle(&ArsenalContainerStyle->BigParameterName).Text(this, &SImprovementContainer::GetWeaponParameterName, EWeaponParameter::Damage)]
								+ SVerticalBox::Slot().AutoHeight()[SNew(STextBlock).TextStyle(&ArsenalContainerStyle->BigParameterValue).Text(this, &SImprovementContainer::GetWeaponParameterValue, EWeaponParameter::Damage)]
							]
							+ SHorizontalBox::Slot().FillWidth(.9f).VAlign(VAlign_Bottom)
							[
								SNew(SVerticalBox)
								+ SVerticalBox::Slot().AutoHeight()[SNew(STextBlock).TextStyle(&ArsenalContainerStyle->ParameterName).Text(this, &SImprovementContainer::GetWeaponParameterName, EWeaponParameter::Accurancy)]
								+ SVerticalBox::Slot().AutoHeight()[SNew(STextBlock).TextStyle(&ArsenalContainerStyle->ParameterValue).Text(this, &SImprovementContainer::GetWeaponParameterValue, EWeaponParameter::Accurancy)]
							]
							+ SHorizontalBox::Slot().FillWidth(.9f).VAlign(VAlign_Bottom)
							[
								SNew(SVerticalBox)
								+ SVerticalBox::Slot().AutoHeight()[SNew(STextBlock).TextStyle(&ArsenalContainerStyle->ParameterName).Text(this, &SImprovementContainer::GetWeaponParameterName, EWeaponParameter::Distance)]
								+ SVerticalBox::Slot().AutoHeight()[SNew(STextBlock).TextStyle(&ArsenalContainerStyle->ParameterValue).Text(this, &SImprovementContainer::GetWeaponParameterValue, EWeaponParameter::Distance)]
							]
							+ SHorizontalBox::Slot().FillWidth(.9f).VAlign(VAlign_Bottom)
							[
								SNew(SVerticalBox)
								+ SVerticalBox::Slot().AutoHeight()[SNew(STextBlock).TextStyle(&ArsenalContainerStyle->ParameterName).Text(this, &SImprovementContainer::GetWeaponParameterName, EWeaponParameter::Clip)]
								+ SVerticalBox::Slot().AutoHeight()[SNew(STextBlock).TextStyle(&ArsenalContainerStyle->ParameterValue).Text(this, &SImprovementContainer::GetWeaponParameterValue, EWeaponParameter::Clip)]
							]
						]
						+ SWidgetSwitcher::Slot()
						[
							SNew(SHorizontalBox).Visibility(EVisibility::HitTestInvisible)
							+ SHorizontalBox::Slot().FillWidth(1).VAlign(VAlign_Bottom)
							[
								SNew(SVerticalBox)
								+ SVerticalBox::Slot().AutoHeight()[SNew(STextBlock).TextStyle(&ArsenalContainerStyle->BigParameterName).Text(this, &SImprovementContainer::GetKnifeParameterName, EKnifeParameter::KnifeDamage)]
								+ SVerticalBox::Slot().AutoHeight()[SNew(STextBlock).TextStyle(&ArsenalContainerStyle->BigParameterValue).Text(this, &SImprovementContainer::GetKnifeParameterValue, EKnifeParameter::KnifeDamage)]
							]
							+ SHorizontalBox::Slot().FillWidth(.9f).VAlign(VAlign_Bottom)
							[
								SNew(SVerticalBox)
								+ SVerticalBox::Slot().AutoHeight()[SNew(STextBlock).TextStyle(&ArsenalContainerStyle->ParameterName).Text(this, &SImprovementContainer::GetKnifeParameterName, EKnifeParameter::KnifeTime)]
								+ SVerticalBox::Slot().AutoHeight()[SNew(STextBlock).TextStyle(&ArsenalContainerStyle->ParameterValue).Text(this, &SImprovementContainer::GetKnifeParameterValue, EKnifeParameter::KnifeTime)]
							]
						]
					]
				]
				+ SHorizontalBox::Slot()
			]
		]
	];
}
END_SLATE_FUNCTION_BUILD_OPTIMIZATION

void SImprovementContainer::ToggleWidget(const bool InToggle)
{
	SUsableCompoundWidget::ToggleWidget(InToggle);
	Anim_Back->ToggleWidget(InToggle);

	if (InToggle)
	{
		RequestScroll();
	}
}

bool SImprovementContainer::IsInInteractiveMode() const
{
	return Anim_Back->IsAnimAtEnd();
}

void SImprovementContainer::RequestScroll()
{
	auto ActiveItem = GetActivePlayerItem();
	auto ChildrenItems = Widget_ItemsContainer->GetChildren();

	if (ChildrenItems && ActiveItem.IsValid())
	{
		const auto NumWidgets = ChildrenItems->Num();
		for (SIZE_T i = 0; i < NumWidgets; ++i)
		{
			if (ActiveItem->AnyAmount() == false)
			{
				Widget_ItemsContainer->ScrollToWidget(ChildrenItems->GetChildAt(0));
				break;
			}
			else if (ActiveItem->GetLevel() == i && NumWidgets > ActiveItem->GetLevel() + 1)
			{
				Widget_ItemsContainer->ScrollToWidget(ChildrenItems->GetChildAt(i + 1));
				break;
			}
			else if (ActiveItem->GetLevel() == i)
			{
				Widget_ItemsContainer->ScrollToWidget(ChildrenItems->GetChildAt(i));
				break;
			}
		}
	}
}

TWeakObjectPtr<UBasicPlayerItem> SImprovementContainer::GetActivePlayerItem() const
{
	if (Widget_ItemsContainer.IsValid())
	{
		TSharedRef<SArsenalItem> MyWidget = StaticCastSharedRef<SArsenalUpgradeItem>(Widget_ItemsContainer->GetActiveSlotWidget());
		return MyWidget->GetInstance();
	}

	return nullptr;
}

UBasicItemEntity* SImprovementContainer::GetActiveEntityItem() const
{
	auto TargetItem = GetActivePlayerItem();
	if (IsValidObject(TargetItem))
	{
		return TargetItem->GetEntityBase();
	}

	return nullptr;
}

void SImprovementContainer::SetInstance(const TWeakObjectPtr<UBasicPlayerItem>& InInstance)
{
	Instance = InInstance;
	Widget_ItemsContainer->ClearChildren();
	
	if (IsValidObject(Instance) && Instance->GetEntityBase())
	{
		for (SIZE_T i = 0; i < Instance->GetEntityBase()->GetItemProperty().Modifications.Num(); ++i)
		{
			Widget_ItemsContainer->AddSlot()
			[
				SNew(SArsenalUpgradeItem, Instance).Level(i).ActiveCoefficient_Lambda([&, idx = i]() -> float
				{
					const float fA = Widget_ItemsContainer->GetCurrentSlot();
					const float fB = float(idx);

					return FMath::Clamp(FMath::Abs<float>(fA - fB), .5f, 1.0f);
				})
			];
		}
	}
}

FText SImprovementContainer::GetItemName() const
{
	if (IsValidObject(Instance) && Instance->GetEntityBase())
	{
		return Instance->GetEntityBase()->GetItemProperty().Title;
	}

	return FText::GetEmpty();
}

FText SImprovementContainer::GetBuyButtonText() const
{
	return FText::GetEmpty();
}

FResourceTextBoxValue SImprovementContainer::GetActiveItemCost() const
{
	if (auto MyItem = GetActiveEntityItem())
	{
		auto TargetItem = GetActivePlayerItem();
		if (IsValidObject(TargetItem) && TargetItem->IsInValidEntityId() && Widget_ItemsContainer->GetActiveSlotIndex() == 0)
		{
			return FResourceTextBoxValue(MyItem->GetItemCost());
		}
		else
		{
			const auto TargetSlot = Widget_ItemsContainer->GetActiveSlotIndex();
			const auto& ModsArray = MyItem->GetItemProperty().Modifications;

			if (ModsArray.Num() && ModsArray.IsValidIndex(TargetSlot))
			{
				const auto& TargetCost = ModsArray[TargetSlot].Cost;
				return FResourceTextBoxValue(TargetCost);
			}
		}
	}

	return FResourceTextBoxValue::Money(0);
}

bool SImprovementContainer::IsAllowBuy() const
{
	return true;
}

bool SImprovementContainer::IsEnabled() const
{
	auto TargetItem = GetActivePlayerItem();
	if(IsValidObject(TargetItem))
	{
		if(TargetItem->IsValidEntityId() && Widget_ItemsContainer->GetActiveSlotIndex() == TargetItem->GetLevel() + 1)
		{
			return true;
		}

		if (TargetItem->IsInValidEntityId() && Widget_ItemsContainer->GetActiveSlotIndex() == 0)
		{
			return true;
		}
	}
	return false;
}


EVisibility SImprovementContainer::IsImprovement() const
{
	auto TargetItem = GetActivePlayerItem();
	if (IsValidObject(TargetItem))
	{
		if (Widget_ItemsContainer->GetActiveSlotIndex() <= TargetItem->GetLevel())
		{
			if (TargetItem->IsValidEntityId())
			{
				return EVisibility::Hidden;
			}
		}
		return EVisibility::Visible;
	}

	return EVisibility::Collapsed;
}

float SImprovementContainer::GetImprovementValue() const
{
	if (auto PlayerItem = GetActivePlayerItem().Get())
	{
		if (auto TargetItem = Cast<UWeaponItemEntity>(GetActiveEntityItem()))
		{
			const auto TargetSlot = Widget_ItemsContainer->GetActiveSlotIndex();
			const auto& ModsArray = TargetItem->GetItemProperty().Modifications;

			if (ModsArray.Num() && TargetSlot > 0 && ModsArray.IsValidIndex(TargetSlot))
			{
				auto selected = TargetItem->GetDynamicValueForLevel(TargetSlot);
				auto actual = TargetItem->GetDynamicValueForLevel(PlayerItem->GetLevel());
				return (selected - actual) * TargetItem->GetWeaponConfiguration().BulletsMin;
			}
		}
		else if (auto KnifeTargetItem = Cast<UKnifeItemEntity>(GetActiveEntityItem()))
		{
			const auto TargetSlot = Widget_ItemsContainer->GetActiveSlotIndex();
			const auto& ModsArray = KnifeTargetItem->GetItemProperty().Modifications;

			if (ModsArray.Num() && TargetSlot > 0 && ModsArray.IsValidIndex(TargetSlot))
			{
				auto selected = KnifeTargetItem->GetDynamicValueForLevel(TargetSlot);
				auto actual = KnifeTargetItem->GetDynamicValueForLevel(PlayerItem->GetLevel());
				return selected - actual;
			}
		}
	}

	return .0f;
}

FSlateColor SImprovementContainer::GetImprovementValueColor() const
{
	return FSlateColor(GetImprovementValue() > 0 ? FColorList::LimeGreen : FColorList::OrangeRed);
}

FReply SImprovementContainer::OnClickedBuyButton()
{
	auto TargetItem = GetActivePlayerItem();
	if (IsValidObject(TargetItem))
	{
		OnRequestBuy.ExecuteIfBound(TargetItem.Get(), Widget_ItemsContainer->GetActiveSlotIndex());
		return FReply::Handled();
	}

	return FReply::Unhandled();
}

FText SImprovementContainer::GetWeaponParameterName(EWeaponParameter InParameter) const
{
	if (auto TargetItem = Cast<UWeaponItemEntity>(GetActiveEntityItem()))
	{
		switch (InParameter)
		{
			case Damage:		return NSLOCTEXT("SAWeaponsContainer", "SAWeaponsContainer.Damage", "Damage");
			case Accurancy: 	return NSLOCTEXT("SAWeaponsContainer", "SAWeaponsContainer.Accurancy", "Accurancy");
			case Distance: 		return NSLOCTEXT("SAWeaponsContainer", "SAWeaponsContainer.Distance", "Distance");
			case Clip: 			return NSLOCTEXT("SAWeaponsContainer", "SAWeaponsContainer.Clip", "Clip");
		}
	}

	return FText::GetEmpty();
}

FText SImprovementContainer::GetWeaponParameterValue(EWeaponParameter InParameter) const
{
	if (auto TargetItem = Cast<UWeaponItemEntity>(GetActiveEntityItem()))
	{
		switch (InParameter)
		{
			case Damage:		return FText::AsNumber(TargetItem->GetDynamicValueForLevel(Widget_ItemsContainer->GetActiveSlotIndex()) * TargetItem->GetWeaponConfiguration().BulletsMin, &FNumberFormattingOptions::DefaultNoGrouping());
			case Accurancy:		return FText::AsNumber(TargetItem->GetWeaponConfiguration().Spread, &FNumberFormattingOptions::DefaultNoGrouping());
			case Distance:		return FText::AsNumber(TargetItem->GetWeaponConfiguration().Range, &FNumberFormattingOptions::DefaultNoGrouping());
			case Clip:			return FText::AsNumber(TargetItem->GetWeaponConfiguration().AmmoPerClip, &FNumberFormattingOptions::DefaultNoGrouping());
		}
	}

	return FText::GetEmpty();
}

FText SImprovementContainer::GetImprovementValueText() const
{
	auto ImproveValue = GetImprovementValue();
	if (ImproveValue > 0)
	{
		return FText::Format(FText::FromString("+{0}"), FText::AsNumber(ImproveValue, &FNumberFormattingOptions::DefaultNoGrouping()));
	}

	return FText::Format(FText::FromString("{0}"), FText::AsNumber(ImproveValue, &FNumberFormattingOptions::DefaultNoGrouping()));
}

FText SImprovementContainer::GetKnifeParameterName(EKnifeParameter InTarget) const
{
	if (auto TargetItem = Cast<UKnifeItemEntity>(GetActiveEntityItem()))
	{
		switch (InTarget)
		{
			case KnifeDamage:		return NSLOCTEXT("SAWeaponsContainer", "SAWeaponsContainer.Damage", "Damage");
			case KnifeTime: 		return NSLOCTEXT("SAWeaponsContainer", "SAWeaponsContainer.KnifeTime", "Activate Time");
		}
	}

	return FText::GetEmpty();
}

FText SImprovementContainer::GetKnifeParameterValue(EKnifeParameter InTarget) const
{
	auto PlayerItem = GetActivePlayerItem();
	if (PlayerItem.IsValid())
	{
		if (auto TargetItem = PlayerItem->GetEntity<UKnifeItemEntity>())
		{
			switch (InTarget)
			{
				case KnifeDamage:	return FText::AsNumber(TargetItem->GetDynamicValueForLevel(Widget_ItemsContainer->GetActiveSlotIndex()), &FNumberFormattingOptions::DefaultNoGrouping());
				case KnifeTime:		return FText::AsNumber(TargetItem->GetKnifeProperty().ActivationInterval, &FNumberFormattingOptions::DefaultNoGrouping());
			}
		}
	}

	return FText::GetEmpty();
}


