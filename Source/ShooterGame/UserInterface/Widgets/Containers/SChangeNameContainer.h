// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "Widgets/SCompoundWidget.h"
#include "Styles/MessageBoxWidgetStyle.h"

/**
 * 
 */
class SHOOTERGAME_API SChangeNameContainer : public SCompoundWidget
{
public:
	static const int64 ChangeNameCost;

	SLATE_BEGIN_ARGS(SChangeNameContainer)
		: _Style(&FShooterStyle::Get().GetWidgetStyle<FMessageBoxStyle>("SMessageBoxStyle"))
		, _AllowWidgetToggle(true)
	{}
	SLATE_STYLE_ARGUMENT(FMessageBoxStyle, Style)
	SLATE_EVENT(FOnTextChanged, OnChangeName)
	SLATE_ARGUMENT(FText, ChangeNameError)
	SLATE_ATTRIBUTE(int32, PlayerNameTickets)
	SLATE_ATTRIBUTE(FText, PlayerName)
	SLATE_ARGUMENT(bool, AllowWidgetToggle)

	SLATE_END_ARGS()

	void Construct(const FArguments& InArgs);
	void SetPlayerName(const FString& InPlayerName);
	void SetPlayerNameTickets(const TAttribute<int32>& InPlayerNameTickets);

protected:
	FText GetChangeNameFreeText() const;

	EVisibility GetChangeNameFreeVisibility() const;
	EVisibility GetChangeNamePaymentVisibility() const;

	TAttribute<int32> PlayerNameTickets;
	TSharedPtr<class SInputWindow> Widget_InputWindow;
	const FMessageBoxStyle* Style;
};
