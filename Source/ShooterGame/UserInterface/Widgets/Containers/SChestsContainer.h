// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "Utilities/SUsableCompoundWidget.h"
#include "ChestWidgetsWidgetStyle.h"
#include "Components/SChestItem.h"

class UPlayerCaseEntity;
class SAnimatedBackground;
/**
 * 
 */
class SHOOTERGAME_API SChestsContainer : public SUsableCompoundWidget
{
public:
	SLATE_BEGIN_ARGS(SChestsContainer)
		: _Style(&FShooterStyle::Get().GetWidgetStyle<FChestWidgetsStyle>("SChestWidgetsStyle"))
	{
		_Visibility = EVisibility::SelfHitTestInvisible;
	}
	SLATE_STYLE_ARGUMENT(FChestWidgetsStyle, Style)
	SLATE_EVENT(FOnClickedChest, OnClickedChest)
	SLATE_END_ARGS()

	void Construct(const FArguments& InArgs);

	virtual void ToggleWidget(const bool InToggle) override;
	virtual bool IsInInteractiveMode() const override;

	void OnFill(const TArray<UPlayerCaseEntity*>& InChests);

protected:

	const FChestWidgetsStyle* Style;

	FOnClickedChest OnClickedChest;

	TSharedPtr<SAnimatedBackground> Anim_Back;
	TSharedPtr<SScrollBox>	Widget_Container;
};
