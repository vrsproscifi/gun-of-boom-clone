// Fill out your copyright notice in the Description page of Project Settings.

#include "ShooterGame.h"
#include "SAttachAccountServiceContainer.h"
#include "Components/SResourceTextBox.h"
#include "Input/SInputWindow.h"

#include "SlateOptMacros.h"
#include "Decorators/LokaTextDecorator.h"
#include "Decorators/LokaResourceDecorator.h"
#include "Decorators/DecoratorHelpers.h"
#include "SScaleBox.h"

BEGIN_SLATE_FUNCTION_BUILD_OPTIMIZATION
void SAttachAccountServiceContainer::Construct(const FArguments& InArgs)
{

	Style = InArgs._Style;
	AccountService = InArgs._AccountService;
	AccountServiceAttached = InArgs._AccountServiceAttached;

	OnClickedSwitchStatus = InArgs._OnClickedSwitchStatus;

	auto ActiveServiceStyle = &Style->Services[AccountService];



	ChildSlot.Padding(30, 30)
	[
		SNew(SHorizontalBox)
		+ SHorizontalBox::Slot().AutoWidth()
		[
			SNew(SScaleBox)
			.Stretch(EStretch::ScaleToFit)
			.Visibility(EVisibility::HitTestInvisible)
			[
				SNew(SImage)
				.Image(&ActiveServiceStyle->Icon)
			]
		]
		+ SHorizontalBox::Slot()
		[
			SNew(SVerticalBox)
			+ SVerticalBox::Slot().AutoHeight().Padding(15, 0)
			[
				SNew(STextBlock)
				.AutoWrapText(true)
				.TextStyle(&Style->Header)
				.Text(ActiveServiceStyle->Name)
			]
			+ SVerticalBox::Slot().AutoHeight().Padding(15, 0)
			[
				SNew(SRichTextBlock)
				.Text(ActiveServiceStyle->Description)
				.TextStyle(&Style->Text)
				.AutoWrapText(true)
				.DecoratorStyleSet(&FShooterStyle::Get())
				+ SRichTextBlock::Decorator(FLokaTextDecorator::Create(Style->Text))
				+ SRichTextBlock::Decorator(FLokaResourceDecorator::Create())
			]
		]
		+ SHorizontalBox::Slot().AutoWidth().HAlign(HAlign_Center).VAlign(EVerticalAlignment::VAlign_Center)
		[
			SNew(SButton)
			.HAlign(HAlign_Center)
			.VAlign(VAlign_Center)
			.ContentPadding(FMargin(10, 6))
			.Visibility(this,&SAttachAccountServiceContainer::GetButtonVisibility)
			.OnClicked(this, &SAttachAccountServiceContainer::OnButtonClicked)
			.ButtonStyle(&Style->Button)
			[
				SNew(STextBlock)
				.AutoWrapText(true)
				.TextStyle(&Style->Text)
				.Justification(ETextJustify::Center)
				.Text(this, &SAttachAccountServiceContainer::GetButtonText)
			]
		]
	];
}
END_SLATE_FUNCTION_BUILD_OPTIMIZATION

EVisibility SAttachAccountServiceContainer::GetButtonVisibility() const
{
	if (AccountService == EAccountService::Google)
	{
		return EVisibility::Visible;
	}
	return EVisibility::Collapsed;
}


FText SAttachAccountServiceContainer::GetButtonText() const
{
	if (AccountServiceAttached.Execute(AccountService))
	{
		return NSLOCTEXT("IdentityComponent", "IdentityComponent.AttachAccount.Connected", "Connected");
	}
	return NSLOCTEXT("IdentityComponent", "IdentityComponent.AttachAccount.Connect", "Connect");
}

FReply SAttachAccountServiceContainer::OnButtonClicked() const
{
	OnClickedSwitchStatus.ExecuteIfBound(AccountService, !AccountServiceAttached.Execute(AccountService));
	return FReply::Handled();
}