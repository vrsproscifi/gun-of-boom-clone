// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "Widgets/SCompoundWidget.h"
#include "Styles/AttachAccountServiceWidgetStyle.h"




class SHOOTERGAME_API SAttachAccountServiceContainer : public SCompoundWidget
{
public:

	SLATE_BEGIN_ARGS(SAttachAccountServiceContainer)
		: _Style(&FShooterStyle::Get().GetWidgetStyle<FAttachAccountServiceStyle>("SAttachAccountServiceStyle"))
		, _AccountService(EAccountService::None)
	{
	}
	SLATE_STYLE_ARGUMENT(FAttachAccountServiceStyle, Style)
	SLATE_EVENT(FOnAttachServiceSwitchClicked, OnClickedSwitchStatus)
	SLATE_ARGUMENT(TEnumAsByte<EAccountService::Type>, AccountService)
		SLATE_EVENT(FOnGetServiceStatus, AccountServiceAttached)


	SLATE_END_ARGS()

	void Construct(const FArguments& InArgs);

protected:
	FText GetButtonText() const;
	FReply OnButtonClicked() const;
	EVisibility GetButtonVisibility() const;

	FOnAttachServiceSwitchClicked OnClickedSwitchStatus;

	FOnGetServiceStatus AccountServiceAttached;
	TEnumAsByte<EAccountService::Type> AccountService;

	TSharedPtr<class SInputWindow> Widget_InputWindow;
	const FAttachAccountServiceStyle* Style;
};
