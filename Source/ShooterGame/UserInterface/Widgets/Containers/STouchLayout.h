// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "Widgets/SCompoundWidget.h"

class SHOOTERGAME_API STouchLayout : public SPanel
{
public:

	class FSlot 
		: public TSlotBase<FSlot>
		, public TSupportsContentAlignmentMixin<FSlot>
	{
		friend class STouchLayout;

	public:

		FSlot(const FName& InTag);
		FSlot& SetPosition(const FVector2D& InPosition);
		FSlot& SetSize(const FVector2D& InSize);

	protected:

		FVector2D Position;
		FVector2D Size;
		FName SlotTag;
	};

	SLATE_BEGIN_ARGS(STouchLayout)
	{}
	SLATE_SUPPORTS_SLOT(STouchLayout::FSlot)
	SLATE_END_ARGS()

	STouchLayout();

	void Construct(const FArguments& InArgs);

	static FSlot& Slot(const FName& InTag)
	{
		return *(new FSlot(InTag));
	}

protected:

	// SPanel overrides

	virtual void OnArrangeChildren(const FGeometry& AllottedGeometry, FArrangedChildren& ArrangedChildren) const override;
	virtual int32 OnPaint(const FPaintArgs& Args, const FGeometry& AllottedGeometry, const FSlateRect& MyClippingRect, FSlateWindowElementList& OutDrawElements, int32 LayerId, const FWidgetStyle& InWidgetStyle, bool bParentEnabled) const override;
	virtual FVector2D ComputeDesiredSize(float) const override;
	virtual FChildren* GetChildren() override;

	TPanelChildren<FSlot> Children;
};
