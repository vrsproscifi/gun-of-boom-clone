// Fill out your copyright notice in the Description page of Project Settings.

#include "ShooterGame.h"
#include "SAdaptiveCarousel.h"
#include "SlateOptMacros.h"

SAdaptiveCarouselPanel::FSlot::FSlot()
	: TSupportsContentAlignmentMixin<SAdaptiveCarouselPanel::FSlot>(HAlign_Fill, VAlign_Fill)
{ 

}

SAdaptiveCarouselPanel::FSlot& SAdaptiveCarouselPanel::FSlot::OnActivatedSlotEvent(const FOnClickedOutside& InEvent)
{
	OnActivatedSlot = InEvent;
	return *this;
}

SAdaptiveCarouselPanel::SAdaptiveCarouselPanel()
	: Children(this)
{
	
}

BEGIN_SLATE_FUNCTION_BUILD_OPTIMIZATION
void SAdaptiveCarouselPanel::Construct(const FArguments& InArgs, const TArray<SAdaptiveCarouselPanel::FSlot*>& InSlots)
{
	DisplayedCells = InArgs._DisplayedCells;

	for (auto TargetSlot : InArgs.Slots)
	{
		Children.Add(TargetSlot);
	}

	for (auto TargetSlot : InSlots)
	{
		Children.Add(TargetSlot);
	}

	CurrentOffset = .0f;
	bLockAutoRevert = false;
	bForceAutoRevert = false;
}
END_SLATE_FUNCTION_BUILD_OPTIMIZATION

void SAdaptiveCarouselPanel::OnArrangeChildren(const FGeometry& AllottedGeometry, FArrangedChildren& ArrangedChildren) const
{
	float CellSize = GetCellSize(AllottedGeometry.GetLocalSize());

	for (int32 SlotIndex = 0; SlotIndex < Children.Num(); ++SlotIndex)
	{
		const FSlot& ThisSlot = Children[SlotIndex];
		const EVisibility ChildVisibility = ThisSlot.GetWidget()->GetVisibility();

		if (ChildVisibility != EVisibility::Collapsed)
		{
			const FMargin& ThisPadding = ThisSlot.SlotPadding.Get();

			AlignmentArrangeResult XAlignmentResult = AlignChild<Orient_Horizontal>(CellSize, ThisSlot, ThisPadding);
			AlignmentArrangeResult YAlignmentResult = AlignChild<Orient_Vertical>(AllottedGeometry.GetLocalSize().Y, ThisSlot, ThisPadding);

			ArrangedChildren.AddWidget(AllottedGeometry.MakeChild(ThisSlot.GetWidget(), FVector2D(XAlignmentResult.Offset + (CellSize * SlotIndex + CellSize) - CurrentOffset, YAlignmentResult.Offset), FVector2D(XAlignmentResult.Size, YAlignmentResult.Size)));
		}
	}
}

FVector2D SAdaptiveCarouselPanel::ComputeDesiredSize(float) const
{
	return FVector2D::ZeroVector;
}

FChildren* SAdaptiveCarouselPanel::GetChildren()
{
	return &Children;
}



void SAdaptiveCarouselPanel::Tick(const FGeometry& AllottedGeometry, const double InCurrentTime, const float InDeltaTime)
{
	const float CellSize = GetCellSize(AllottedGeometry.GetLocalSize());
	CurrentCellFloat = CurrentOffset / CellSize;

	if (bLockAutoRevert == false || bForceAutoRevert == true)
	{
		//=================================
		const int32 ChildrenNum = Children.Num();	// 10		
		const float HalfCellSize = CellSize * 0.5f;

		//=================================
		const float MinTargetOffset = 0.0f;
		const float MaxTargetOffset = CellSize * (ChildrenNum - 1);

		//=================================
		if (bForceAutoRevert)
		{
			TargetOffset = FMath::Clamp(FMath::GridSnap(ForceOffset, CellSize), MinTargetOffset, MaxTargetOffset);
		}
		else
		{
			TargetOffset = FMath::Clamp(FMath::GridSnap(CurrentOffset, CellSize), MinTargetOffset, MaxTargetOffset);
		}

		//=================================
		CurrentOffset = FMath::FInterpTo(CurrentOffset, TargetOffset, InDeltaTime, 10.0f);	
		
		//=================================
		if (bForceAutoRevert && FMath::IsNearlyEqual(ForceOffset, CurrentOffset, HalfCellSize))
		{
			bForceAutoRevert = false;
		}

		//=================================
		const int32 DisplayedCellsNum = DisplayedCells.Get();	//	3

		//=================================
		const float ChildrenNum_DisplayedCellsNum = ChildrenNum / float(DisplayedCellsNum);
		const float TargetOffset_CellSize = TargetOffset / CellSize;

		//=================================
		const float Result = TargetOffset_CellSize /** ChildrenNum_DisplayedCellsNum*/;
		const uint16 FlooredResult = FMath::CeilToInt(Result);

		//=================================
		GEngine->AddOnScreenDebugMessage(4, 1.0f, FColorList::BlueViolet, *FString::Printf(TEXT("Result [SAdaptiveCarouselPanel]: %.2f[%d]"), Result, FlooredResult), false);

		//=================================
		if (CurrentCell != FlooredResult)
		{
			CurrentCell = FlooredResult;
			if (Children.IsValidIndex(CurrentCell))
			{
				Children[CurrentCell].OnActivatedSlot.ExecuteIfBound();
			}
		}		
	}
}

float SAdaptiveCarouselPanel::GetCellSize(const FVector2D& InLocalSize) const
{
	return InLocalSize.X / float(DisplayedCells.Get());
}



BEGIN_SLATE_FUNCTION_BUILD_OPTIMIZATION
void SAdaptiveCarousel::Construct(const FArguments& InArgs)
{
	OnDoubleTouch = InArgs._OnDoubleTouch;

	ChildSlot
	[
		SAssignNew(InnerPanel, SAdaptiveCarouselPanel, InArgs.Slots)
		.DisplayedCells(InArgs._DisplayedCells)
	];

	MovedDelta = .0f;
	TouchIndex = INDEX_NONE;
}
END_SLATE_FUNCTION_BUILD_OPTIMIZATION

SAdaptiveCarouselPanel::FSlot& SAdaptiveCarousel::AddSlot()
{
	SAdaptiveCarouselPanel::FSlot& NewSlot = *new SAdaptiveCarouselPanel::FSlot();
	InnerPanel->Children.Add(&NewSlot);

	return NewSlot;
}

void SAdaptiveCarousel::RemoveSlot(const TSharedRef<SWidget>& WidgetToRemove)
{
	TPanelChildren<SAdaptiveCarouselPanel::FSlot>& Children = InnerPanel->Children;
	for (int32 SlotIndex = 0; SlotIndex < Children.Num(); ++SlotIndex)
	{
		if (Children[SlotIndex].GetWidget() == WidgetToRemove)
		{
			Children.RemoveAt(SlotIndex);
			return;
		}
	}
}

void SAdaptiveCarousel::ClearChildren()
{
	InnerPanel->Children.Empty();
}

uint16 SAdaptiveCarousel::GetActiveSlotIndex() const
{
	return InnerPanel->CurrentCell;
}

TSharedRef<SWidget> SAdaptiveCarousel::GetActiveSlotWidget() const
{
	const auto SlotIdx = GetActiveSlotIndex();
	if (InnerPanel->Children.IsValidIndex(SlotIdx))
	{
		return InnerPanel->Children[SlotIdx].GetWidget();
	}
	return SNullWidget::NullWidget;
}

float SAdaptiveCarousel::GetCurrentSlot() const
{
	return InnerPanel->CurrentCellFloat;
}

bool SAdaptiveCarousel::ScrollToSlot(const int32& InSlotIndex, bool InAllowRecursiveSearchSlot)
{
	//========================================
	TPanelChildren<SAdaptiveCarouselPanel::FSlot>& Children = InnerPanel->Children;
	const auto ChildrenNum = Children.Num();

	//========================================
	const float XSize = GetCachedGeometry().GetLocalSize().X == .0f ? 1920.f : GetCachedGeometry().GetLocalSize().X;
	const float CellSize = XSize / float(InnerPanel->DisplayedCells.Get());

	//========================================
	if (InSlotIndex < ChildrenNum)
	{
		InnerPanel->ForceOffset = CellSize * InSlotIndex;
		InnerPanel->bForceAutoRevert = true;
		return true;
	}
	else if(InAllowRecursiveSearchSlot && !!ChildrenNum)
	{
		InnerPanel->ForceOffset = CellSize * ChildrenNum - 1;
		InnerPanel->bForceAutoRevert = true;
		return true;
	}

	return false;
}

bool SAdaptiveCarousel::ScrollToWidget(const TSharedRef<SWidget>& WidgetToScroll)
{
	const float XSize = GetCachedGeometry().GetLocalSize().X == .0f ? 1920.f : GetCachedGeometry().GetLocalSize().X;
	const float CellSize = XSize / float(InnerPanel->DisplayedCells.Get());
	TPanelChildren<SAdaptiveCarouselPanel::FSlot>& Children = InnerPanel->Children;
	for (int32 SlotIndex = 0; SlotIndex < Children.Num(); ++SlotIndex)
	{
		if (Children[SlotIndex].GetWidget() == WidgetToScroll)
		{			
			InnerPanel->ForceOffset = CellSize * SlotIndex;
			InnerPanel->bForceAutoRevert = true;
			return true;
		}
	}
	return false;
}

FChildren* SAdaptiveCarousel::GetChildren() 
{
	return &InnerPanel->Children;
}

int32 SAdaptiveCarousel::GetNumChildrens() const
{
	if (InnerPanel.IsValid())
	{
		return InnerPanel->Children.Num();
	}
	return 0;
}

FReply SAdaptiveCarousel::OnTouchStarted(const FGeometry& MyGeometry, const FPointerEvent& InTouchEvent)
{
	if (TouchIndex == INDEX_NONE)
	{
		TouchIndex = InTouchEvent.GetPointerIndex();
		InnerPanel->bLockAutoRevert = true;
		return FReply::Handled().CaptureMouse(AsShared());
	}

	return FReply::Unhandled();
}

FReply SAdaptiveCarousel::OnTouchMoved(const FGeometry& MyGeometry, const FPointerEvent& InTouchEvent)
{
	if (TouchIndex == InTouchEvent.GetPointerIndex())
	{
		InnerPanel->CurrentOffset += -InTouchEvent.GetCursorDelta().X;
		MovedDelta += InTouchEvent.GetCursorDelta().X;
		return FReply::Handled();
	}

	return FReply::Unhandled();
}

FReply SAdaptiveCarousel::OnTouchEnded(const FGeometry& MyGeometry, const FPointerEvent& InTouchEvent)
{
	if (TouchIndex == InTouchEvent.GetPointerIndex())
	{		
		TouchIndex = INDEX_NONE;		
		InnerPanel->bLockAutoRevert = false;		

		if (FMath::IsNearlyEqual(MovedDelta, .0f, 5.0f))
		{
			const FVector2D TouchPosition = MyGeometry.AbsoluteToLocal(InTouchEvent.GetScreenSpacePosition());
			const FVector2D LocalSize = MyGeometry.GetLocalSize();
			const float CellSize = MyGeometry.GetLocalSize().X / float(InnerPanel->DisplayedCells.Get());

			if (FMath::IsNearlyEqual(TouchPosition.X, LocalSize.X / 2, CellSize / 2))
			{
				const auto CurrentTime = FSlateApplication::Get().GetCurrentTime();
				if (LastTouchEndTime + .6f >= CurrentTime)
				{
					OnDoubleTouch.ExecuteIfBound();
				}

				LastTouchEndTime = CurrentTime;
			}
			else if (FMath::IsNearlyEqual(TouchPosition.X, .0f, CellSize))
			{
				InnerPanel->ForceOffset = InnerPanel->CurrentOffset - CellSize;
				InnerPanel->bForceAutoRevert = true;
			}
			else if (FMath::IsNearlyEqual(TouchPosition.X, LocalSize.X, CellSize))
			{
				InnerPanel->ForceOffset = InnerPanel->CurrentOffset + CellSize;
				InnerPanel->bForceAutoRevert = true;
			} 
		}
		
		MovedDelta = .0f;
		return FReply::Handled().ReleaseMouseCapture();
	}

	return FReply::Unhandled();
}