// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "Shared/AttachAccountService.h"

#include "Utilities/SUsableCompoundWidget.h"
#include "Styles/ProfileContainerWidgetStyle.h"
#include "GameItemType.h"

struct FPlayerStatisticModel;
class SPlayerNameAndProgress;
class UArsenalComponent;
class UIdentityComponent;
class SAnimatedBackground;
class SRadioButtonsBox;
class SChangeNameContainer;

/**
 *
 */
class SHOOTERGAME_API SProfileContainer : public SUsableCompoundWidget
{
public:
	SLATE_BEGIN_ARGS(SProfileContainer)
		: _Style(&FShooterStyle::Get().GetWidgetStyle<FProfileContainerStyle>("SProfileContainerStyle"))
	{
		_Visibility = EVisibility::SelfHitTestInvisible;
	}
	SLATE_STYLE_ARGUMENT(FProfileContainerStyle, Style)
	SLATE_ARGUMENT(TWeakObjectPtr<UIdentityComponent>, IdentityComponent)
	SLATE_ARGUMENT(TWeakObjectPtr<UArsenalComponent>, ArsenalComponent)
		
	SLATE_EVENT(FOnGetServiceStatus, IsAccountServiceAttached)
	SLATE_EVENT(FOnAttachServiceSwitchClicked, OnClickedSwitchServiceStatus)
	SLATE_EVENT(FOnTextChanged, OnClickedChangeName)
	SLATE_EVENT(FOnClickedOutside, OnClosed)
	SLATE_END_ARGS()

	void Construct(const FArguments& InArgs);

	virtual void ToggleWidget(const bool InToggle) override;
	virtual bool IsInInteractiveMode() const override;

	void SetInformation(const FPlayerStatisticModel& InStat);

public:
	FOnTextChanged OnClickedChangeName;

protected:

	const FProfileContainerStyle* Style;

	TSharedPtr<SAnimatedBackground>		Anim_Back;
	TSharedPtr<SPlayerNameAndProgress>	Widget_PlayerName;
	TSharedPtr<STextBlock>				Widget_PlayerID;
	TSharedPtr<STextBlock>				Widget_RegistrationDate;
	TSharedPtr<STextBlock>				Widget_LastActivityDate;
	TSharedPtr<STextBlock>				Widget_PremiumEndDate;
	TSharedPtr<SRadioButtonsBox>		Widget_Controlls;
	TSharedPtr<SChangeNameContainer>	Widget_ChangeName;

	TWeakObjectPtr<UIdentityComponent> IdentityComponent;
	TWeakObjectPtr<UArsenalComponent> ArsenalComponent;

	int32 GetChangeNameTickets() const;
	FText GetChangeNameCurrentName() const;

	// Cached data
	bool IsLocalPlayer;

	float AvgWL;
	float AvgKD;
	float AvgRealKD;

	FText TextFightTotal;
	FText TextFightWins;

	FText TextActiveWeaponType;
	FText TextActiveWeapon;
	FText TextActiveArmour[static_cast<uint8>(EGameItemType::End)];

	FText TextBestScore;
	FText TextInGame;
	FText TextMaxKills;

	// Gethers
	float GetProgressPercent(bool IsWinLose) const;
	FText GetProgressValue(bool IsWinLose) const;

	FText GetFightTotalText() const;
	FText GetFightWinsText() const;

	FText GetActiveWeaponTypeText() const;
	FText GetActiveWeaponText() const;

	FText GetActiveArmourText(EGameItemType InTarget) const;

	FText GetBestScoreText() const;
	FText GetInGameText() const;
	FText GetMaxKillsText() const;

	int32 GetActiveWidgetIndex() const;
	EVisibility GetAccountVisibility() const;
};
