// Fill out your copyright notice in the Description page of Project Settings.

#include "ShooterGame.h"
#include "SProfileContainer.h"
#include "SAttachAccountServiceContainer.h"
#include "SChangeNameContainer.h"



#include "SlateOptMacros.h"
#include "Utilities/SAnimatedBackground.h"
#include "Components/SPlayerNameAndProgress.h"
#include "IdentityComponent.h"
#include "Components/SSphereProgress.h"
#include "IdentityComponent/PlayerStatisticModel.h"
#include "ShooterGameAchievements.h"
#include "Components/STitleHeader.h"
#include "Components/SRadioButtonsBox.h"
#include "Localization/TableWeaponStrings.h"
#include "Decorators/LokaTextDecorator.h"
#include "Decorators/LokaResourceDecorator.h"
#include "ArsenalComponent.h"
#include "BasicPlayerItem.h"
#include "GameSingletonExtensions.h"
#include "BasicItemEntity.h"
#include "Input/SSocialButton.h"

BEGIN_SLATE_FUNCTION_BUILD_OPTIMIZATION
void SProfileContainer::Construct(const FArguments& InArgs)
{
	Style = InArgs._Style;
	IdentityComponent = InArgs._IdentityComponent;
	ArsenalComponent = InArgs._ArsenalComponent;
	OnClickedChangeName = InArgs._OnClickedChangeName;

	const TCHAR sEdit[] = { static_cast<TCHAR>(0xf044), 0 }; // fa-solid

	ChildSlot
	[
		SAssignNew(Anim_Back, SAnimatedBackground)
		.Visibility(EVisibility::SelfHitTestInvisible)
		.IsEnabledBlur(false)
		.Duration(.5f)
		.ShowAnimation(EAnimBackAnimation::UpFade)
		.HideAnimation(EAnimBackAnimation::DownFade)
		.InitAsHide(true)
		[
			SNew(SVerticalBox)
			+ SVerticalBox::Slot().AutoHeight() // Back btn
			[
				SNew(STitleHeader)
				.ShowCurrency(false)
				.Header(NSLOCTEXT("SProfileContainer", "SProfileContainer.Title", "Profile"))
				.OnClickedBack_Lambda([&, e = InArgs._OnClosed]() { e.ExecuteIfBound(); ToggleWidget(false); })
			]
			+ SVerticalBox::Slot()
			[
				SNew(SHorizontalBox)
				+ SHorizontalBox::Slot().VAlign(VAlign_Top).HAlign(HAlign_Left)
				[
					SAssignNew(Widget_Controlls, SRadioButtonsBox)
					.ActiveIndex(0)
					.IsAutoSize(true)
					.Visibility(this, &SProfileContainer::GetAccountVisibility)
					.ButtonsPadding(FMargin(20, 5, 0, 5))
					.Orientation(Orient_Vertical)
					.ButtonsText
					({
						NSLOCTEXT("SRaitingsContainer", "SProfileContainer.Tabs.Stat", "Statistics"),
						NSLOCTEXT("SRaitingsContainer", "SProfileContainer.Tabs.ChangeName", "Change name"),
						NSLOCTEXT("SRaitingsContainer", "SProfileContainer.Tabs.Account", "Account"),
					})
				]
				+ SHorizontalBox::Slot().FillWidth(1.2f)
				[
					SNew(SAnimatedBackground)
					.Visibility(EVisibility::Visible)
					.IsEnabledBlur(true)
					.InitAsHide(false)
					[
						SNew(SWidgetSwitcher)
						.WidgetIndex(this, &SProfileContainer::GetActiveWidgetIndex)
						+ SWidgetSwitcher::Slot()
						[
							SNew(SVerticalBox)
							+ SVerticalBox::Slot().AutoHeight().Padding(10, 10, 10, 0)
							[
								SNew(SHorizontalBox)
								+ SHorizontalBox::Slot().AutoWidth()
								[
									SAssignNew(Widget_PlayerName, SPlayerNameAndProgress)
									.IdentityComponent(IdentityComponent)
								]
								+ SHorizontalBox::Slot().HAlign(HAlign_Right)
								[
									SNew(SVerticalBox)
									+ SVerticalBox::Slot().AutoHeight()
									[
										SAssignNew(Widget_PlayerID, STextBlock)
									]
									+ SVerticalBox::Slot().AutoHeight()
									[
										SAssignNew(Widget_RegistrationDate, STextBlock)
									]
									+ SVerticalBox::Slot().AutoHeight()
									[
										SAssignNew(Widget_LastActivityDate, STextBlock)
									]
									+ SVerticalBox::Slot().AutoHeight()
									[
										SAssignNew(Widget_PremiumEndDate, STextBlock)
									]
								]
							]
							+ SVerticalBox::Slot().AutoHeight()
							[
								SNew(SHorizontalBox)
								+ SHorizontalBox::Slot().AutoWidth().Padding(10, 0, 10, 10)
								[
									SNew(SBox).WidthOverride(200)
									[
										SNew(SVerticalBox)
										+ SVerticalBox::Slot().VAlign(VAlign_Bottom)[SNew(STextBlock).TextStyle(&Style->StatTitle).Text(NSLOCTEXT("SProfileContainer", "SProfileContainer.BestScore", "Best Score"))]
										+ SVerticalBox::Slot().VAlign(VAlign_Top)[SNew(STextBlock).TextStyle(&Style->StatValue).Text(this, &SProfileContainer::GetBestScoreText)]
									]
								]
								+ SHorizontalBox::Slot().AutoWidth().Padding(10, 0, 10, 10)
								[
									SNew(SBox).WidthOverride(200)
									[
										SNew(SVerticalBox)
										+ SVerticalBox::Slot().VAlign(VAlign_Bottom)[SNew(STextBlock).TextStyle(&Style->StatTitle).Text(NSLOCTEXT("SProfileContainer", "SProfileContainer.InGame", "In Game"))]
										+ SVerticalBox::Slot().VAlign(VAlign_Top)[SNew(STextBlock).TextStyle(&Style->StatValue).Text(this, &SProfileContainer::GetInGameText)]
									]
								]
								+ SHorizontalBox::Slot().AutoWidth().Padding(10, 0, 10, 10)
								[
									SNew(SVerticalBox)
									+ SVerticalBox::Slot().VAlign(VAlign_Bottom)[SNew(STextBlock).TextStyle(&Style->StatTitle).Text(NSLOCTEXT("SProfileContainer", "SProfileContainer.MaxKills", "Max. Kills"))]
									+ SVerticalBox::Slot().VAlign(VAlign_Top)[SNew(STextBlock).TextStyle(&Style->StatValue).Text(this, &SProfileContainer::GetMaxKillsText)]
								]
							]
							+ SVerticalBox::Slot().AutoHeight()
							[
								SNew(SHorizontalBox)
								+ SHorizontalBox::Slot().AutoWidth().Padding(10)
								[
									SNew(SBox).WidthOverride(150).HeightOverride(150)
									[
										SNew(SSphereProgress)
										.Title(FText::FromString("K/D"))
										.Progress(this, &SProfileContainer::GetProgressPercent, false)
										.ProgressText(this, &SProfileContainer::GetProgressValue, false)
									]
								]
								+ SHorizontalBox::Slot().AutoWidth().Padding(60, 10, 10, 10)
								[
									SNew(SBox).WidthOverride(150).HeightOverride(150)
									[
										SNew(SSphereProgress)
										.Title(FText::FromString("W/L"))
										.Progress(this, &SProfileContainer::GetProgressPercent, true)
										.ProgressText(this, &SProfileContainer::GetProgressValue, true)
									]
								]
								+ SHorizontalBox::Slot().AutoWidth().Padding(60, 0)
								[
									SNew(SVerticalBox)
									+ SVerticalBox::Slot().VAlign(VAlign_Bottom)[SNew(STextBlock).TextStyle(&Style->StatTitle).Text(NSLOCTEXT("SProfileContainer", "SProfileContainer.TotalFights", "Total Fights"))]
									+ SVerticalBox::Slot().VAlign(VAlign_Top)[SNew(STextBlock).TextStyle(&Style->StatValue).Text(this, &SProfileContainer::GetFightTotalText)]
									+ SVerticalBox::Slot().VAlign(VAlign_Bottom)[SNew(STextBlock).TextStyle(&Style->StatTitle).Text(NSLOCTEXT("SProfileContainer", "SProfileContainer.WinFights", "Win Fights"))]
									+ SVerticalBox::Slot().VAlign(VAlign_Top)[SNew(STextBlock).TextStyle(&Style->StatValue).Text(this, &SProfileContainer::GetFightWinsText)]
								]
							]
							+ SVerticalBox::Slot().AutoHeight().Padding(0, 20).VAlign(VAlign_Center)
							[
								SNew(SSeparator)
								.SeparatorImage(&Style->SeparatorBrush)
								.Thickness(Style->SeparatorThickness)
							]
							+ SVerticalBox::Slot().AutoHeight().Padding(10, 0, 10, 10)
							[
								SNew(SHorizontalBox)
								+ SHorizontalBox::Slot().HAlign(HAlign_Left)
								[
									SNew(SVerticalBox)
									+ SVerticalBox::Slot().AutoHeight()[SNew(STextBlock).TextStyle(&Style->EqiupTitle).Text(FTableItemStrings::GetCategoryText(EGameItemType::Head))]
									+ SVerticalBox::Slot().AutoHeight()
									[
										SNew(SRichTextBlock)
										.Justification(ETextJustify::Center)
										.Text(this, &SProfileContainer::GetActiveArmourText, EGameItemType::Head)
										.TextStyle(&Style->EqiupValue)
										+ SRichTextBlock::Decorator(FLokaTextDecorator::Create(Style->EqiupValue))
										+ SRichTextBlock::Decorator(FLokaResourceDecorator::Create())
									]
								]
								+ SHorizontalBox::Slot().HAlign(HAlign_Left)
								[
									SNew(SVerticalBox)
									+ SVerticalBox::Slot().AutoHeight()[SNew(STextBlock).TextStyle(&Style->EqiupTitle).Text(this, &SProfileContainer::GetActiveWeaponTypeText)]
									+ SVerticalBox::Slot().AutoHeight()
									[
										SNew(SRichTextBlock)
										.Justification(ETextJustify::Center)
										.Text(this, &SProfileContainer::GetActiveWeaponText)
										.TextStyle(&Style->EqiupValue)
										+ SRichTextBlock::Decorator(FLokaTextDecorator::Create(Style->EqiupValue))
										+ SRichTextBlock::Decorator(FLokaResourceDecorator::Create())
									]
								]
							]
							+ SVerticalBox::Slot().AutoHeight().Padding(10)
							[
								SNew(SHorizontalBox)
								+ SHorizontalBox::Slot().HAlign(HAlign_Left)
								[
									SNew(SVerticalBox)
									+ SVerticalBox::Slot().AutoHeight()[SNew(STextBlock).TextStyle(&Style->EqiupTitle).Text(FTableItemStrings::GetCategoryText(EGameItemType::Torso))]
									+ SVerticalBox::Slot().AutoHeight()
									[
										SNew(SRichTextBlock)
										.Justification(ETextJustify::Center)
										.Text(this, &SProfileContainer::GetActiveArmourText, EGameItemType::Torso)
										.TextStyle(&Style->EqiupValue)
										+ SRichTextBlock::Decorator(FLokaTextDecorator::Create(Style->EqiupValue))
										+ SRichTextBlock::Decorator(FLokaResourceDecorator::Create())
									]
								]
								+ SHorizontalBox::Slot().HAlign(HAlign_Left)
								[
									SNew(SVerticalBox)
									+ SVerticalBox::Slot().AutoHeight()[SNew(STextBlock).TextStyle(&Style->EqiupTitle).Text(FTableItemStrings::GetCategoryText(EGameItemType::Legs))]
									+ SVerticalBox::Slot().AutoHeight()
									[
										SNew(SRichTextBlock)
										.Justification(ETextJustify::Center)
										.Text(this, &SProfileContainer::GetActiveArmourText, EGameItemType::Legs)
										.TextStyle(&Style->EqiupValue)
										+ SRichTextBlock::Decorator(FLokaTextDecorator::Create(Style->EqiupValue))
										+ SRichTextBlock::Decorator(FLokaResourceDecorator::Create())
									]
								]
							]
						]
						+ SWidgetSwitcher::Slot().Padding(30, 0)
						[
							SAssignNew(Widget_ChangeName, SChangeNameContainer)
							.AllowWidgetToggle(false)
							.OnChangeName(OnClickedChangeName)
							.PlayerNameTickets(this, &SProfileContainer::GetChangeNameTickets)
							.PlayerName(this, &SProfileContainer::GetChangeNameCurrentName)
						]
						+ SWidgetSwitcher::Slot()
						[
							SNew(SVerticalBox)
							+ SVerticalBox::Slot().AutoHeight()
							[
								SNew(SAttachAccountServiceContainer).AccountService(EAccountService::Google).OnClickedSwitchStatus(InArgs._OnClickedSwitchServiceStatus).AccountServiceAttached(InArgs._IsAccountServiceAttached)
							]
							+ SVerticalBox::Slot().AutoHeight()
							[
								SNew(SAttachAccountServiceContainer).AccountService(EAccountService::Facebook).OnClickedSwitchStatus(InArgs._OnClickedSwitchServiceStatus).AccountServiceAttached(InArgs._IsAccountServiceAttached)
							]
							+ SVerticalBox::Slot().AutoHeight()
							[
								SNew(SAttachAccountServiceContainer).AccountService(EAccountService::Vkontakte).OnClickedSwitchStatus(InArgs._OnClickedSwitchServiceStatus).AccountServiceAttached(InArgs._IsAccountServiceAttached)
							]
							+ SVerticalBox::Slot()
						]
					]
				]
			]
		]
	];
}
END_SLATE_FUNCTION_BUILD_OPTIMIZATION

int32 SProfileContainer::GetChangeNameTickets() const
{
	if (IsValidObject(IdentityComponent))
	{
		return IdentityComponent->GetPlayerInfo().Experience.ChangeNameTickets;
	}
	return -1;
}

FText SProfileContainer::GetChangeNameCurrentName() const
{
	if (IsValidObject(IdentityComponent))
	{
		return FText::FromString(IdentityComponent->GetPlayerInfo().Login);
	}
	return FText::GetEmpty();
}

void SProfileContainer::ToggleWidget(const bool InToggle)
{
	SUsableCompoundWidget::ToggleWidget(InToggle);
	Anim_Back->ToggleWidget(InToggle);
}

bool SProfileContainer::IsInInteractiveMode() const
{
	return Anim_Back->IsAnimAtEnd();
}


void SProfileContainer::SetInformation(const FPlayerStatisticModel& InStat)
{
	// Reset vars

	Widget_Controlls->SetActiveIndex(0);

	IsLocalPlayer = false;
	AvgWL = .0f;
	AvgRealKD = .0f;
	AvgKD = .0f;

	TextFightTotal = FText::FromString("-");
	TextFightWins = FText::FromString("-");

	TextActiveWeaponType = FText::FromString("-");
	TextActiveWeapon = FText::FromString("-");

	for (uint8 i = 0; i < static_cast<uint8>(EGameItemType::End); ++i)
	{
		TextActiveArmour[i] = FText::FromString("-");
	}

	TextBestScore = FText::FromString("-");
	TextInGame = FText::FromString("-");
	TextMaxKills = FText::FromString("-");

	if (IsValidObject(IdentityComponent))
	{
		const auto &TargetStat = IdentityComponent->Statistic;
		FPlayerGameAchievements Achiv(TargetStat.Achievements);

		FPlayerNameAndProgressData TargetData
		(
			TargetStat.PlayerName, 
			TargetStat.Experience.Level,
			TargetStat.Experience.Experience,
			TargetStat.Experience.NextExperience,
			FDateTime::FromUnixTimestamp(TargetStat.PremiumEndDate) > FDateTime::UtcNow(),
			TargetStat.EloRatingScore
		);

		TargetData.Country = TargetStat.Country;

		IsLocalPlayer = TargetStat.EntityId == IdentityComponent->GetPlayerInfo().PlayerId;
		
		Widget_PlayerName->SetOverrideData(TargetData);
		Widget_PlayerID->SetText(FText::FromString(FString::Printf(TEXT("ID:%s"), *TargetStat.EntityId.ToString())));

		if (Achiv.ContainsAchievement(EShooterGameAchievements::MatchesWins) && Achiv.ContainsAchievement(EShooterGameAchievements::MatchesTotal))
		{
			AvgWL = float(Achiv.GetAchievementById(EShooterGameAchievements::MatchesWins)->Value) / float(Achiv.GetAchievementById(EShooterGameAchievements::MatchesTotal)->Value);
		}

		if (Achiv.ContainsAchievement(EShooterGameAchievements::Kills) && Achiv.ContainsAchievement(EShooterGameAchievements::Deads))
		{
			AvgRealKD = float(Achiv.GetAchievementById(EShooterGameAchievements::Kills)->Value) / float(Achiv.GetAchievementById(EShooterGameAchievements::Deads)->Value);
			AvgKD = AvgRealKD / 2.0f;
		}

		if (Achiv.ContainsAchievement(EShooterGameAchievements::MatchesTotal))
		{
			TextFightTotal = FText::AsNumber(Achiv.GetAchievementById(EShooterGameAchievements::MatchesTotal)->Value, &FNumberFormattingOptions::DefaultNoGrouping());
		}

		if (Achiv.ContainsAchievement(EShooterGameAchievements::MatchesWins))
		{
			TextFightWins = FText::AsNumber(Achiv.GetAchievementById(EShooterGameAchievements::MatchesWins)->Value, &FNumberFormattingOptions::DefaultNoGrouping());
		}

		{
			if (auto FoundItem = FGameSingletonExtension::FindItemById(TargetStat.DefaulItemModelId))
			{
				TextActiveWeaponType = FTableItemStrings::GetCategoryText(FoundItem->GetItemType());
			}
		}

		{
			if (auto FoundItem = FGameSingletonExtension::FindItemById(TargetStat.DefaulItemModelId))
			{
				TextActiveWeapon = FoundItem->GetItemProperty().Title;
			}
		}

		auto GetItemFromSlotLambad = [](const TArray<FPlayerStatisticItemModel>& InItems, EGameItemType InSlot) -> FText
		{
			for (auto Item : InItems)
			{
				if (Item.SlotId == static_cast<uint8>(InSlot))
				{											
					if (auto FoundItem = FGameSingletonExtension::FindItemById(Item.ModelId))
					{
						return FoundItem->GetItemProperty().Title;
					}
				}
			}

			return FText::FromString("-");
		};

		TextActiveArmour[static_cast<uint8>(EGameItemType::Head)] = GetItemFromSlotLambad(TargetStat.Items, EGameItemType::Head);
		TextActiveArmour[static_cast<uint8>(EGameItemType::Torso)] = GetItemFromSlotLambad(TargetStat.Items, EGameItemType::Torso);
		TextActiveArmour[static_cast<uint8>(EGameItemType::Legs)] = GetItemFromSlotLambad(TargetStat.Items, EGameItemType::Legs);

		if (Achiv.ContainsAchievement(EShooterGameAchievements::BestScore))
		{
			TextBestScore = FText::AsNumber(Achiv.GetAchievementById(EShooterGameAchievements::BestScore)->Value, &FNumberFormattingOptions::DefaultNoGrouping());
		}

		if (Achiv.ContainsAchievement(EShooterGameAchievements::TimeInGame))
		{
			FString Timespan = FTimespan::FromSeconds(Achiv.GetAchievementById(EShooterGameAchievements::TimeInGame)->Value).ToString(TEXT("%hh %mm"));
			Timespan.RemoveAt(0);
			TextInGame = FText::FromString(Timespan);
		}

		if (Achiv.ContainsAchievement(EShooterGameAchievements::BestKills))
		{
			TextMaxKills = FText::AsNumber(Achiv.GetAchievementById(EShooterGameAchievements::BestKills)->Value, &FNumberFormattingOptions::DefaultNoGrouping());
		}

		// For Debug
		Widget_RegistrationDate->SetText(FText::Format(FText::FromString("RegistrationDate: {0}"), FText::AsDateTime(FDateTime::FromUnixTimestamp(TargetStat.RegistrationDate))));
		Widget_LastActivityDate->SetText(FText::Format(FText::FromString("LastActivityDate: {0}"), FText::AsDateTime(FDateTime::FromUnixTimestamp(TargetStat.LastActivityDate))));
		Widget_PremiumEndDate->SetText(FText::Format(FText::FromString("PremiumEndDate: {0}"), FText::AsDateTime(FDateTime::FromUnixTimestamp(TargetStat.PremiumEndDate))));

		if (Widget_ChangeName.IsValid())
		{
			Widget_ChangeName->SetPlayerName(TargetData.Name);
		}
	}
}

float SProfileContainer::GetProgressPercent(bool IsWinLose) const
{
	if (IsWinLose)
	{
		return AvgWL;
	}

	return AvgKD;
}

FText SProfileContainer::GetProgressValue(bool IsWinLose) const
{
	if (IsWinLose)
	{
		return FText::AsPercent(AvgWL);
	}

	return FText::AsNumber(AvgRealKD, &FNumberFormattingOptions::DefaultNoGrouping());
}

FText SProfileContainer::GetFightTotalText() const
{
	return TextFightTotal;
}

FText SProfileContainer::GetFightWinsText() const
{
	return TextFightWins;
}

FText SProfileContainer::GetActiveWeaponTypeText() const
{
	return TextActiveWeaponType;
}

FText SProfileContainer::GetActiveWeaponText() const
{
	return TextActiveWeapon;
}

FText SProfileContainer::GetActiveArmourText(EGameItemType InTarget) const
{
	return TextActiveArmour[static_cast<uint8>(InTarget)];
}

FText SProfileContainer::GetBestScoreText() const
{
	return TextBestScore;
}

FText SProfileContainer::GetInGameText() const
{
	return TextInGame;
}

FText SProfileContainer::GetMaxKillsText() const
{
	return TextMaxKills;
}

int32 SProfileContainer::GetActiveWidgetIndex() const
{
	return Widget_Controlls->GetActiveIndex();
}

EVisibility SProfileContainer::GetAccountVisibility() const
{
	return IsLocalPlayer ? EVisibility::Visible : EVisibility::Hidden;
}
