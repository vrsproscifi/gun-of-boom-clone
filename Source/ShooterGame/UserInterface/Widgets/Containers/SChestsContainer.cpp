// Fill out your copyright notice in the Description page of Project Settings.

#include "ShooterGame.h"
#include "SChestsContainer.h"
#include "SlateOptMacros.h"
#include "Components/STitleHeader.h"
#include "Utilities/SAnimatedBackground.h"
#include "Decorators/LokaTextDecorator.h"
#include "Decorators/LokaResourceDecorator.h"
#include "Entity/PlayerCaseEntity.h"
#include "CaseProperty.h"

BEGIN_SLATE_FUNCTION_BUILD_OPTIMIZATION
void SChestsContainer::Construct(const FArguments& InArgs)
{
	Style = InArgs._Style;
	OnClickedChest = InArgs._OnClickedChest;

	ChildSlot
	[
		SAssignNew(Anim_Back, SAnimatedBackground)
		.Visibility(EVisibility::Visible)
		.IsEnabledBlur(true)
		.Duration(.5f)
		.ShowAnimation(EAnimBackAnimation::UpFade)
		.HideAnimation(EAnimBackAnimation::DownFade)
		.InitAsHide(true)
		[
			SNew(SVerticalBox)
			+ SVerticalBox::Slot().AutoHeight()
			[
				SNew(STitleHeader).Header(NSLOCTEXT("SChestsContainer", "SChestsContainer.Title", "Chests"))
				.OnClickedBack(this, &SChestsContainer::ToggleWidget, false)
			]
			+ SVerticalBox::Slot()
			[
				SNew(SVerticalBox)
				+ SVerticalBox::Slot().AutoHeight().Padding(50, 20)			// Description
				[
					SNew(SRichTextBlock)
					.Justification(ETextJustify::Center)
					.Text(Style->DescText)
					.TextStyle(&Style->DescFont)
					+ SRichTextBlock::Decorator(FLokaTextDecorator::Create(Style->DescFont))
					+ SRichTextBlock::Decorator(FLokaResourceDecorator::Create())
				]
				+ SVerticalBox::Slot().Padding(4, 10).HAlign(HAlign_Center)	// Content
				[
					SAssignNew(Widget_Container, SScrollBox)
					.Orientation(Orient_Horizontal)
					.AllowOverscroll(EAllowOverscroll::Yes)
					.NavigationDestination(EDescendantScrollDestination::Center)
				]
			]
		]
	];
}
END_SLATE_FUNCTION_BUILD_OPTIMIZATION

void SChestsContainer::ToggleWidget(const bool InToggle)
{
	SUsableCompoundWidget::ToggleWidget(InToggle);
	Anim_Back->ToggleWidget(InToggle);
}

bool SChestsContainer::IsInInteractiveMode() const
{
	return Anim_Back->IsAnimAtEnd();
}

void SChestsContainer::OnFill(const TArray<UPlayerCaseEntity*>& InChests)
{
	Widget_Container->ClearChildren();
	
	auto ChestArray = InChests.FilterByPredicate([&](const UPlayerCaseEntity* PlayerCase)
	{
		return PlayerCase && PlayerCase->GetEntity() && PlayerCase->GetCaseEntity() && PlayerCase->GetCaseEntityProperty()->bAllowDisplayOnHomeScreen;
	});

	ChestArray.Sort([](const UPlayerCaseEntity& InA, const UPlayerCaseEntity& InB)
	{
		if (InA.IsValidLowLevel() && InB.IsValidLowLevel())
		{
			const auto A = InA.GetCaseEntityProperty();
			const auto B = InB.GetCaseEntityProperty();
			if (A && B)
			{
				return A->DisplayPositionOnHomeScreen > B->DisplayPositionOnHomeScreen;
			}
		}
		return false;
	});

	for (auto MyChest : ChestArray)
	{
		Widget_Container->AddSlot()
		[
			SNew(SChestItem, MyChest).OnClickedChest(OnClickedChest)
		];
	}
}
