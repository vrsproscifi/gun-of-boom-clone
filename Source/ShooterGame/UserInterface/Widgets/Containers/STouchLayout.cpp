// Fill out your copyright notice in the Description page of Project Settings.

#include "ShooterGame.h"
#include "STouchLayout.h"
#include "SlateOptMacros.h"

STouchLayout::FSlot::FSlot(const FName& InTag)
	: TSupportsContentAlignmentMixin(HAlign_Left, VAlign_Top)
	, Position(FVector2D::ZeroVector)
	, Size(FVector2D::ZeroVector)
	, SlotTag(InTag)
{
	
}

STouchLayout::FSlot& STouchLayout::FSlot::SetPosition(const FVector2D& InPosition)
{
	Position = InPosition;
	return *this;
}

STouchLayout::FSlot& STouchLayout::FSlot::SetSize(const FVector2D& InSize)
{
	Size = InSize;
	return *this;
}

STouchLayout::STouchLayout()
	: Children(this)
{
	
}

BEGIN_SLATE_FUNCTION_BUILD_OPTIMIZATION
void STouchLayout::Construct(const FArguments& InArgs)
{
	const int32 NumSlots = InArgs.Slots.Num();
	for (int32 SlotIndex = 0; SlotIndex < NumSlots; ++SlotIndex)
	{
		Children.Add(InArgs.Slots[SlotIndex]);
	}
}
END_SLATE_FUNCTION_BUILD_OPTIMIZATION

void STouchLayout::OnArrangeChildren(const FGeometry& AllottedGeometry, FArrangedChildren& ArrangedChildren) const
{
	for (int32 ChildIndex = 0; ChildIndex < Children.Num(); ++ChildIndex)
	{
		const FSlot& CurChild = Children[ChildIndex];
		const EVisibility ChildVisibility = CurChild.GetWidget()->GetVisibility();
		if (ArrangedChildren.Accepts(ChildVisibility))
		{
			const FVector2D LocalSize = AllottedGeometry.GetLocalSize();

			FVector2D WidgetPosition = FVector2D::ZeroVector;
			FVector2D WidgetSize = CurChild.Size;

			switch(CurChild.HAlignment) 
			{ 
				case HAlign_Fill: 
					WidgetSize.X = LocalSize.X;
				break;
				case HAlign_Left: 
					WidgetPosition.X = CurChild.Position.X;
				break;
				case HAlign_Center: 
					WidgetPosition.X = (LocalSize.X / 2 - CurChild.Size.X / 2) + CurChild.Position.X;
				break;
				case HAlign_Right: 
					WidgetPosition.X = LocalSize.X - CurChild.Position.X - CurChild.Size.X;
				break;
			}

			switch (CurChild.VAlignment) 
			{
				case VAlign_Fill: 
					WidgetSize.Y = LocalSize.Y;
				break;
				case VAlign_Top: 
					WidgetPosition.Y = CurChild.Position.Y;
				break;
				case VAlign_Center: 
					WidgetPosition.Y = (LocalSize.Y / 2 - CurChild.Size.Y / 2) + CurChild.Position.Y;
				break;
				case VAlign_Bottom: 
					WidgetPosition.Y = LocalSize.Y - CurChild.Position.Y - CurChild.Size.Y;
				break;
			}

			ArrangedChildren.AddWidget(ChildVisibility, AllottedGeometry.MakeChild(
				CurChild.GetWidget(),
				WidgetPosition,
				WidgetSize
			));
		}
	}
}

int32 STouchLayout::OnPaint(const FPaintArgs& Args,
	const FGeometry& AllottedGeometry,
	const FSlateRect& MyClippingRect,
	FSlateWindowElementList& OutDrawElements,
	int32 LayerId,
	const FWidgetStyle& InWidgetStyle,
	bool bParentEnabled) const
{
	FArrangedChildren ArrangedChildren(EVisibility::Visible);
	{
		// The box panel has no visualization of its own; it just visualizes its children.
		this->ArrangeChildren(AllottedGeometry, ArrangedChildren);
	}

	// Because we paint multiple children, we must track the maximum layer id that they produced in case one of our parents
	// wants to an overlay for all of its contents.
	int32 MaxLayerId = LayerId;

	const FPaintArgs NewArgs = Args.WithNewParent(this);

	for (int32 ChildIndex = 0; ChildIndex < ArrangedChildren.Num(); ++ChildIndex)
	{
		FArrangedWidget& CurWidget = ArrangedChildren[ChildIndex];

		const int32 CurWidgetsMaxLayerId =
			CurWidget.Widget->Paint(
				NewArgs,
				CurWidget.Geometry,
				MyClippingRect,
				OutDrawElements,
				MaxLayerId + 1,
				InWidgetStyle,
				ShouldBeEnabled(bParentEnabled));

		MaxLayerId = FMath::Max(MaxLayerId, CurWidgetsMaxLayerId);
	}

	return MaxLayerId;
}

FVector2D STouchLayout::ComputeDesiredSize(float) const
{
	FVector2D MaxSize(0, 0);
	for (int32 ChildIndex = 0; ChildIndex < Children.Num(); ++ChildIndex)
	{
		const FSlot& CurSlot = Children[ChildIndex];
		const EVisibility ChildVisibilty = CurSlot.GetWidget()->GetVisibility();
		if (ChildVisibilty != EVisibility::Collapsed)
		{
			FVector2D ChildDesiredSize = CurSlot.Position + CurSlot.Size;
			MaxSize.X = FMath::Max(MaxSize.X, ChildDesiredSize.X);
			MaxSize.Y = FMath::Max(MaxSize.Y, ChildDesiredSize.Y);
		}
	}

	return MaxSize;
}

FChildren* STouchLayout::GetChildren()
{
	return &Children;
}
