// Fill out your copyright notice in the Description page of Project Settings.

#include "ShooterGame.h"
#include "SKilledContainer.h"
#include "SlateOptMacros.h"
#include "Utilities/SAnimatedBackground.h"
#include "GameState/ShooterPlayerState.h"
#include "GameSingletonExtensions.h"
#include "Components/SKillerItem.h"
#include "BasicItemEntity.h"
#include "Localization/TableWeaponStrings.h"
#include "ArsenalComponent.h"
#include "BasicPlayerItem.h"
#include "ShooterCharacter.h"
#include "State/SLiveBar.h"
#include "WeaponItemEntity.h"
#include "Localization/TableLobbyStrings.h"
#include "TeamColorsData.h"
#include "GameUtilities.h"

BEGIN_SLATE_FUNCTION_BUILD_OPTIMIZATION
void SKilledContainer::Construct(const FArguments& InArgs)
{
	Style = InArgs._Style;

	FButtonStyle* BackgroundButtonStyle = new FButtonStyle();
	BackgroundButtonStyle->Normal = FSlateNoResource();
	BackgroundButtonStyle->Hovered = FSlateNoResource();
	BackgroundButtonStyle->Pressed = FSlateNoResource();

	ChildSlot
	[
		SNew(SOverlay)
		+ SOverlay::Slot()
		[
			SNew(SButton)
			.Visibility(this, &SKilledContainer::GetBackButtonVis)
			.ButtonStyle(BackgroundButtonStyle)
			.OnClicked_Lambda([e = InArgs._OnClickedRespawn]()
			{
				e.ExecuteIfBound();
				return FReply::Handled();
			})
		]
		+ SOverlay::Slot().Padding(0, 100, 0, 20)
		[
			SNew(SHorizontalBox)	
			+ SHorizontalBox::Slot().HAlign(HAlign_Left) //=====================] Local
			[
				SAssignNew(Anim_Back[1], SAnimatedBackground)
				.ShowAnimation(EAnimBackAnimation::LeftFade)
				.HideAnimation(EAnimBackAnimation::LeftFade)
				.Duration(.5f)
				.IsRelative(true)
				[
					SNew(SVerticalBox)
					+ SVerticalBox::Slot().AutoHeight() // me
					[
						SAssignNew(Widget_HeaderLocal, SBorder).BorderImage(&Style->Image_NameBackground)
						[
							SNew(SVerticalBox)
							+ SVerticalBox::Slot().AutoHeight().Padding(10, 10, 0, 2)
							[
								SNew(STextBlock)
								.TextStyle(&Style->Font_KilledBy)
								.Text(NSLOCTEXT("SKilledContainer", "SKilledContainer.Local", "You"))
							]
							+ SVerticalBox::Slot().AutoHeight().Padding(10, 2, 0, 10)
							[
								SAssignNew(Widget_TextLocal, STextBlock)
								.TextStyle(&Style->Font_Killer)
							]
						]
					]
					+ SVerticalBox::Slot().AutoHeight().Expose(Slot_LocalWeapon) // weapon
					+ SVerticalBox::Slot().AutoHeight().Padding(10, 0) // head
					[
						SNew(SVerticalBox)
						+ SVerticalBox::Slot().AutoHeight().Padding(0, 10, 0, 2)
						[
							SNew(STextBlock)
							.TextStyle(&Style->Font_ItemType)
							.Text(FTableItemStrings::GetCategoryText(EGameItemType::Head))
						]
						+ SVerticalBox::Slot().AutoHeight().Padding(0, 2, 0, 10)
						[
							SAssignNew(Widget_TextLocalHead, STextBlock)
							.TextStyle(&Style->Font_ItemTitle)
						]
					]
					+ SVerticalBox::Slot().AutoHeight().Padding(10, 0) // torso
					[
						SNew(SVerticalBox)
						+ SVerticalBox::Slot().AutoHeight().Padding(0, 10, 0, 2)
						[
							SNew(STextBlock)
							.TextStyle(&Style->Font_ItemType)
							.Text(FTableItemStrings::GetCategoryText(EGameItemType::Torso))
						]
						+ SVerticalBox::Slot().AutoHeight().Padding(0, 2, 0, 10)
						[
							SAssignNew(Widget_TextLocalTorso, STextBlock)
							.TextStyle(&Style->Font_ItemTitle)
						]
					]
					+ SVerticalBox::Slot().AutoHeight().Padding(10, 0) // legs
					[
						SNew(SVerticalBox)
						+ SVerticalBox::Slot().AutoHeight().Padding(0, 10, 0, 2)
						[
							SNew(STextBlock)
							.TextStyle(&Style->Font_ItemType)
							.Text(FTableItemStrings::GetCategoryText(EGameItemType::Legs))
						]
						+ SVerticalBox::Slot().AutoHeight().Padding(0, 2, 0, 10)
						[
							SAssignNew(Widget_TextLocalLegs, STextBlock)
							.TextStyle(&Style->Font_ItemTitle)
						]
					]
					+ SVerticalBox::Slot()
					+ SVerticalBox::Slot().AutoHeight().Padding(10, 0) // live
				]
			]
			+ SHorizontalBox::Slot().HAlign(HAlign_Right)
			[
				SAssignNew(Anim_Back[0], SAnimatedBackground)
				.ShowAnimation(EAnimBackAnimation::RightFade)
				.HideAnimation(EAnimBackAnimation::RightFade)
				.Duration(.5f)
				.IsRelative(true)
				[
					SNew(SVerticalBox)
					+ SVerticalBox::Slot().AutoHeight().Padding(0, 0) // killed by
					[
						SAssignNew(Widget_HeaderKiller, SBorder).BorderImage(&Style->Image_NameBackground)
						[
							SNew(SVerticalBox)
							+ SVerticalBox::Slot().AutoHeight().Padding(0, 10, 10, 2)
							[
								SNew(STextBlock)
								.Justification(ETextJustify::Right)
								.TextStyle(&Style->Font_KilledBy)
								.Text(NSLOCTEXT("SKilledContainer", "SKilledContainer.KilledBy", "Killer"))
							]
							+ SVerticalBox::Slot().AutoHeight().Padding(0, 2, 10, 10)
							[
								SAssignNew(Widget_TextKiller, STextBlock)
								.Justification(ETextJustify::Right)
								.TextStyle(&Style->Font_Killer)
							]
						]
					]
					+ SVerticalBox::Slot().AutoHeight().Expose(Slot_Weapon) // weapon
					+ SVerticalBox::Slot().AutoHeight().Padding(10, 0) // head
					[
						SNew(SVerticalBox)
						+ SVerticalBox::Slot().AutoHeight().Padding(0, 10, 0, 2)
						[
							SNew(STextBlock)
							.Justification(ETextJustify::Right)
							.TextStyle(&Style->Font_ItemType)
							.Text(FTableItemStrings::GetCategoryText(EGameItemType::Head))
						]
						+ SVerticalBox::Slot().AutoHeight().Padding(0, 2, 0, 10)
						[
							SAssignNew(Widget_TextHead, STextBlock)
							.Justification(ETextJustify::Right)
							.TextStyle(&Style->Font_ItemTitle)
						]
					]
					+ SVerticalBox::Slot().AutoHeight().Padding(10, 0) // torso
					[
						SNew(SVerticalBox)
						+ SVerticalBox::Slot().AutoHeight().Padding(0, 10, 0, 2)
						[
							SNew(STextBlock)
							.Justification(ETextJustify::Right)
							.TextStyle(&Style->Font_ItemType)
							.Text(FTableItemStrings::GetCategoryText(EGameItemType::Torso))
						]
						+ SVerticalBox::Slot().AutoHeight().Padding(0, 2, 0, 10)
						[
							SAssignNew(Widget_TextTorso, STextBlock)
							.Justification(ETextJustify::Right)
							.TextStyle(&Style->Font_ItemTitle)
						]
					]
					+ SVerticalBox::Slot().AutoHeight().Padding(10, 0) // legs
					[
						SNew(SVerticalBox)
						+ SVerticalBox::Slot().AutoHeight().Padding(0, 10, 0, 2)
						[
							SNew(STextBlock)
							.Justification(ETextJustify::Right)
							.TextStyle(&Style->Font_ItemType)
							.Text(FTableItemStrings::GetCategoryText(EGameItemType::Legs))
						]
						+ SVerticalBox::Slot().AutoHeight().Padding(0, 2, 0, 10)
						[
							SAssignNew(Widget_TextLegs, STextBlock)
							.Justification(ETextJustify::Right)
							.TextStyle(&Style->Font_ItemTitle)
						]
					]
					+ SVerticalBox::Slot()
					+ SVerticalBox::Slot().AutoHeight().Padding(10, 0) // live
					[
						SNew(SButton).Tag(TEXT("SKilledContainer.Button.IntoFight"))
						.HAlign(HAlign_Center)
						.VAlign(VAlign_Center)
						.ContentPadding(FMargin(80, 21))
						.ButtonStyle(&FShooterStyle::Get().GetWidgetStyle<FButtonStyle>("Default_ActionButton"))
						.TextStyle(&FShooterStyle::Get().GetWidgetStyle<FTextBlockStyle>("Default_ActionText"))
						.Text(FTableLobbyStrings::IntoFight)
						.OnClicked_Lambda([e = InArgs._OnClickedRespawn]()
						{
							e.ExecuteIfBound();
							return FReply::Handled();
						})
					]
				]
			]
		]
	];

	auto Assets = FGameSingletonExtension::GetDataAssets<UTeamColorsData>();
	if (Assets.Num())
	{
		TeamColorsPtr = Assets[0];
	}
}
END_SLATE_FUNCTION_BUILD_OPTIMIZATION

void SKilledContainer::ToggleWidget(const bool InToggle)
{
	SUsableCompoundWidget::ToggleWidget(InToggle);
	Anim_Back[0]->ToggleWidget(InToggle);
	Anim_Back[1]->ToggleWidget(InToggle);
}

bool SKilledContainer::IsInInteractiveMode() const
{
	return Anim_Back[0]->IsAnimAtEnd();
}

void SKilledContainer::SetInformation(AShooterPlayerState* InKiller, const FKillerInfo& InKillerInfo, AShooterPlayerState* InLocal, const FKillerInfo& InLocalInfo)
{
	TSharedPtr<SKillerItem> WeaponWidgetKiller;
	TSharedPtr<SKillerItem> WeaponWidgetLocal;

	float WeaponDamageKiller = .0f;
	float WeaponDamageLocal = .0f;

	FLinearColor ColorLocal, ColorKiller;

	if (TeamColorsPtr.IsValid())
	{
		ColorLocal = TeamColorsPtr->GetFriendlyColor();
		ColorKiller = TeamColorsPtr->GetEnemyColor();
	}

	if (InKiller && InKiller->IsValidLowLevel())
	{
		if (auto FoundItem = FGameSingletonExtension::FindItemById(InKillerInfo.Weapon.ModelId))
		{
			WeaponDamageKiller = FGameUtilities::GetWeaponCoefficient(GetValidObjectAs<UWeaponItemEntity>(FoundItem), InKillerInfo.Weapon.Level);

			auto& MySlot = *Slot_Weapon;

			MySlot.Padding(0, 0, 0, 10)
			[
				SNew(SBorder).BorderImage(&Style->Image_WeaponBackground)/*.BorderBackgroundColor(ColorKiller)*/.Padding(FMargin(30, 2))
				[
					SNew(SBox).HeightOverride(120).WidthOverride(250).HAlign(HAlign_Center)
					[
						SAssignNew(WeaponWidgetKiller, SKillerItem, FoundItem).ActiveCoefficient(.0f).InItemLevel(InKillerInfo.Weapon.Level)
						.Style(&FShooterStyle::Get().GetWidgetStyle<FShopItemStyle>("SShopItemStyle_Fight"))
					]
				]
			];
		}

		Widget_HeaderKiller->SetBorderBackgroundColor(ColorKiller);

		Widget_TextKiller->SetText(FText::FromString(InKiller->GetShortPlayerName()));

		Widget_TextHead->SetText(GetItemName(InKillerInfo.Head));
		Widget_TextTorso->SetText(GetItemName(InKillerInfo.Torso));
		Widget_TextLegs->SetText(GetItemName(InKillerInfo.Legs));
	}

	if (InLocal && InLocal->IsValidLowLevel())
	{
		if (auto FoundItem = FGameSingletonExtension::FindItemById(InLocalInfo.Weapon.ModelId))
		{
			WeaponDamageLocal = FGameUtilities::GetWeaponCoefficient(GetValidObjectAs<UWeaponItemEntity>(FoundItem), InLocalInfo.Weapon.Level);

			auto& MySlot = *Slot_LocalWeapon;

			MySlot.Padding(0, 0, 0, 10)
				[
					SNew(SBorder).BorderImage(&Style->Image_WeaponBackground)/*.BorderBackgroundColor(ColorLocal)*/.Padding(FMargin(30, 2))
					[
						SNew(SBox).HeightOverride(120).WidthOverride(250).HAlign(HAlign_Center)
						[
							SAssignNew(WeaponWidgetLocal, SKillerItem, FoundItem).ActiveCoefficient(.0f).InItemLevel(InLocalInfo.Weapon.Level)
							.Style(&FShooterStyle::Get().GetWidgetStyle<FShopItemStyle>("SShopItemStyle_Fight"))
						]
					]
				];
		}

		Widget_HeaderLocal->SetBorderBackgroundColor(ColorLocal);

		Widget_TextLocal->SetText(FText::FromString(InLocal->GetShortPlayerName()));

		Widget_TextLocalHead->SetText(GetItemName(InLocalInfo.Head));
		Widget_TextLocalTorso->SetText(GetItemName(InLocalInfo.Torso));
		Widget_TextLocalLegs->SetText(GetItemName(InLocalInfo.Legs));

		//Widget_KillerHealth->OverrideHealth(InLocalInfo.Health);
		//Widget_KillerHealth->OverrideArmour(InLocalInfo.Armour);
	}

	if (Style->CurveDamageColor && WeaponWidgetKiller.IsValid() && WeaponWidgetLocal.IsValid() && WeaponDamageKiller != WeaponDamageLocal)
	{
		const float MinValue = FMath::Min(WeaponDamageKiller, WeaponDamageLocal);
		const float MaxValue = FMath::Max(WeaponDamageKiller, WeaponDamageLocal);

		const float DeltaRateValue = 1.0f - MinValue / MaxValue;

		if (WeaponDamageKiller > WeaponDamageLocal)
		{
			WeaponWidgetKiller->DamageColor = Style->CurveDamageColor->GetLinearColorValue(DeltaRateValue);
		}
		else
		{
			WeaponWidgetLocal->DamageColor = Style->CurveDamageColor->GetLinearColorValue(DeltaRateValue);
		}
	}
}

FText SKilledContainer::GetItemName(const FKillerItemInfo& InItemInfo) const
{
	if (auto FoundItem = FGameSingletonExtension::FindItemById(InItemInfo.ModelId))
	{
		FText ItemName = FoundItem->GetItemProperty().Title;;
		if (InItemInfo.Level)
		{
			ItemName = FText::Format(NSLOCTEXT("KillerItem", "KillerItem.NameWithLevel", "{0} lvl {1}"), ItemName, FText::AsNumber(InItemInfo.Level, &FNumberFormattingOptions::DefaultNoGrouping()));
		}
		return ItemName;
	}

	return FText::FromString("-");
}

EVisibility SKilledContainer::GetBackButtonVis() const
{
	return IsInInteractiveMode() ? EVisibility::Visible : EVisibility::HitTestInvisible;
}
