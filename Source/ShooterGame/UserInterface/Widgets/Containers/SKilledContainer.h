// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "Utilities/SUsableCompoundWidget.h"
#include "Styles/KilledContainerWidgetStyle.h"

class UTeamColorsData;
struct FKillerInfo;
enum class EGameItemType : uint8;
class AShooterPlayerState;
class SAnimatedBackground;


struct FKillerItemInfo;

/**
 * 
 */
class SHOOTERGAME_API SKilledContainer : public SUsableCompoundWidget
{
public:
	SLATE_BEGIN_ARGS(SKilledContainer)
		: _Style(&FShooterStyle::Get().GetWidgetStyle<FKilledContainerStyle>("SKilledContainerStyle"))
	{
		_Visibility = EVisibility::SelfHitTestInvisible;
	}
	SLATE_STYLE_ARGUMENT(FKilledContainerStyle, Style)
	SLATE_EVENT(FOnClickedOutside, OnClickedRespawn)
	SLATE_END_ARGS()

	void Construct(const FArguments& InArgs);

	virtual void ToggleWidget(const bool InToggle) override;
	virtual bool IsInInteractiveMode() const override;

	void SetInformation(AShooterPlayerState* InKiller, const FKillerInfo& InKillerInfo, AShooterPlayerState* InLocal, const FKillerInfo& InLocalInfo);

protected:

	const FKilledContainerStyle* Style;

	TWeakObjectPtr<UTeamColorsData> TeamColorsPtr;

	TSharedPtr<SAnimatedBackground> Anim_Back[2];

	TSharedPtr<STextBlock>	Widget_TextKiller;
	TSharedPtr<STextBlock>	Widget_TextHead;
	TSharedPtr<STextBlock>	Widget_TextTorso;
	TSharedPtr<STextBlock>	Widget_TextLegs;

	TSharedPtr<STextBlock>	Widget_TextLocal;
	TSharedPtr<STextBlock>	Widget_TextLocalHead;
	TSharedPtr<STextBlock>	Widget_TextLocalTorso;
	TSharedPtr<STextBlock>	Widget_TextLocalLegs;

	TSharedPtr<SBorder>		Widget_HeaderLocal;
	TSharedPtr<SBorder>		Widget_HeaderKiller;
	 

	SVerticalBox::FSlot* Slot_Weapon;
	SVerticalBox::FSlot* Slot_LocalWeapon;

	FText GetItemName(const FKillerItemInfo& InItemInfo) const;
	EVisibility GetBackButtonVis() const;
};
