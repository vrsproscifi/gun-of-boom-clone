// Fill out your copyright notice in the Description page of Project Settings.

#include "ShooterGame.h"
#include "SChangeNameContainer.h"
#include "Components/SResourceTextBox.h"
#include "Input/SInputWindow.h"

#include "SlateOptMacros.h"
#include "Decorators/LokaTextDecorator.h"
#include "Decorators/LokaResourceDecorator.h"
#include "Decorators/DecoratorHelpers.h"
#include "Notify/SMessageBox.h"

const int64 SChangeNameContainer::ChangeNameCost = 50;

BEGIN_SLATE_FUNCTION_BUILD_OPTIMIZATION
void SChangeNameContainer::Construct(const FArguments& InArgs)
{
	Style = InArgs._Style;
	PlayerNameTickets = InArgs._PlayerNameTickets;

	//================================================
	FText ChangeNameError = FText::GetEmpty();

	//================================================
	//	��� �������� ������� ������� ����� ����� ������� � ����� ��� �����
	if(InArgs._ChangeNameError.IsEmptyOrWhitespace() == false)
	{
		ChangeNameError = FText::Format(FText::FromString("\n<text color=\"#FB0000FF\">{0}</>"), InArgs._ChangeNameError);
	}

	const auto CostChangeName = FResourceDecoratorHelper(ChangeNameCost, EResourceTextBoxType::Donate).ToText();
	const auto PaymentChangeName = FText::Format(FText::FromString("{0} {1}"), NSLOCTEXT("IdentityComponent", "IdentityComponent.ChangeName.Title", "Change Name"), CostChangeName);

	ChildSlot
	[
		SNew(SBox).WidthOverride(700)
		[
			SNew(SVerticalBox)
			+ SVerticalBox::Slot().AutoHeight()
			[
				SAssignNew(Widget_InputWindow, SInputWindow)
				.Value(InArgs._PlayerName.Get())
				.Text(FText::Format(NSLOCTEXT("IdentityComponent", "IdentityComponent.ChangeName.Desc", "Enter your name or confirm current.{0}"), ChangeNameError))
			]
			+ SVerticalBox::Slot().AutoHeight().Padding(0, 15)
			[
				SNew(STextBlock)
				.TextStyle(&Style->Content.Text)
				.AutoWrapText(true)
				//.Justification(ETextJustify::Right)
				.Text(NSLOCTEXT("IdentityComponent", "IdentityComponent.ChangeName.Rules", "Player's name must not be shorter than 4 and longer than 32 characters. You can use any symbols, letters and numbers. Player's name should not contain obscene words and insult other players."))
			]
			+ SVerticalBox::Slot().AutoHeight().Padding(0, 15)
			[
				SNew(STextBlock)
				.Visibility(this, &SChangeNameContainer::GetChangeNameFreeVisibility)
				.TextStyle(&Style->Content.Text)
				.AutoWrapText(true)
				//.Justification(ETextJustify::Right)
				.Text(this, &SChangeNameContainer::GetChangeNameFreeText)
			]
			+ SVerticalBox::Slot().AutoHeight().Padding(0, 15)
			[
				SNew(STextBlock)
				.Visibility(this, &SChangeNameContainer::GetChangeNamePaymentVisibility)
				.TextStyle(&Style->Content.Text)
				.AutoWrapText(true)
				//.Justification(ETextJustify::Right)
				.Text(FText::Format(NSLOCTEXT("IdentityComponent", "IdentityComponent.ChangeName.Payment", "You have run out of free tickets to change your name. The cost of changing the name of {0} crystals."), ChangeNameCost))
			]
			+ SVerticalBox::Slot().AutoHeight().Padding(150, 15)
			[
				SNew(SButton)
				.ButtonStyle(&Style->Buttons.Button)
				.ContentPadding(FMargin(0))
				.OnClicked_Lambda([&, OnChangeName = InArgs._OnChangeName, AllowWidgetToggle = InArgs._AllowWidgetToggle]()
				{
					if (AllowWidgetToggle)
					{
						SMessageBox::Get()->ToggleWidget(false);
					}
					if (Widget_InputWindow.IsValid())
					{
						const auto Name = Widget_InputWindow->GetInputText();
						OnChangeName.ExecuteIfBound(Name);
					}
					return FReply::Handled();
				})
				.HAlign(HAlign_Center)
				[
					SNew(SHorizontalBox)
					+ SHorizontalBox::Slot().VAlign(EVerticalAlignment::VAlign_Center)
					[
						SNew(STextBlock)
						.AutoWrapText(true)
						.TextStyle(&Style->Content.Text)
						.Visibility(this, &SChangeNameContainer::GetChangeNameFreeVisibility)
						.Text(NSLOCTEXT("IdentityComponent", "IdentityComponent.ChangeName.Title", "Change Name"))
					]
					+ SHorizontalBox::Slot().VAlign(EVerticalAlignment::VAlign_Center)
					[
						SNew(SRichTextBlock)
						.TextStyle(&Style->Content.Text)
						.AutoWrapText(true)
						.Visibility(this, &SChangeNameContainer::GetChangeNamePaymentVisibility)
						.DecoratorStyleSet(&FShooterStyle::Get())
						.Text(PaymentChangeName)
						+ SRichTextBlock::Decorator(FLokaTextDecorator::Create(Style->Head.Text))
						+ SRichTextBlock::Decorator(FLokaResourceDecorator::Create())
					]
				]
			]
		]
	];
}
END_SLATE_FUNCTION_BUILD_OPTIMIZATION

void SChangeNameContainer::SetPlayerName(const FString& InPlayerName)
{
	Widget_InputWindow->SetInputText(FText::FromString(InPlayerName));
}

void SChangeNameContainer::SetPlayerNameTickets(const TAttribute<int32>& InPlayerNameTickets)
{
	PlayerNameTickets = InPlayerNameTickets;
}

EVisibility SChangeNameContainer::GetChangeNameFreeVisibility() const
{
	const auto tickets = PlayerNameTickets.Get();
	if (tickets > 0)
	{
		return EVisibility::HitTestInvisible;
	}
	return EVisibility::Collapsed;
}

EVisibility SChangeNameContainer::GetChangeNamePaymentVisibility() const
{
	const auto tickets = PlayerNameTickets.Get();
	if (tickets > 0)
	{
		return EVisibility::Collapsed;
	}
	return EVisibility::HitTestInvisible;
}

FText SChangeNameContainer::GetChangeNameFreeText() const
{
	const auto tickets = PlayerNameTickets.Get();
	return FText::Format(NSLOCTEXT("IdentityComponent", "IdentityComponent.ChangeName.Free", "You can change the name of the player for free {0} times. After that, the cost of changing the name will be {1} crystals"), tickets, ChangeNameCost);
}
