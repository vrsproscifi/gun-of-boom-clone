// VRSPRO

#include "ShooterGame.h"
#include "SSettingsManager_Check.h"
#include "SlateOptMacros.h"

BEGIN_SLATE_FUNCTION_BUILD_OPTIMIZATION
void SSettingsManager_Check::Construct(const FArguments& InArgs)
{
	Style = InArgs._Style;
	OnValueChange = InArgs._OnValueChange;

	ChildSlot
	[
		SNew(SVerticalBox)
		+ SVerticalBox::Slot().AutoHeight()
		[
			SNew(SHorizontalBox)
			+ SHorizontalBox::Slot().VAlign(VAlign_Center)
			[
				SAssignNew(Widget_TextBlock, STextBlock).Text(InArgs._Name)
				.TextStyle(&Style->Names)
			]
			+ SHorizontalBox::Slot()
			[
				SNew(SBox).HeightOverride(Style->HeightOverride)
				[
					SAssignNew(Widget_CheckBox, SCheckBox)
					.Type(ESlateCheckBoxType::CheckBox)
					.Style(&Style->Check)
					.OnCheckStateChanged_Lambda([&](ECheckBoxState State) { OnValueChange.ExecuteIfBound(State == ECheckBoxState::Checked); })
				]
			]
		]
		+ SVerticalBox::Slot().AutoHeight()
		[
			SNew(SSpacer).Size(FVector2D(0, Style->BottomRowPadding))
		]
	];
}
END_SLATE_FUNCTION_BUILD_OPTIMIZATION

void SSettingsManager_Check::SetName(const FText& Name)
{
	Widget_TextBlock->SetText(Name);
}

void SSettingsManager_Check::SetIsChecked(const bool IsCheck)
{
	Widget_CheckBox->SetIsChecked(IsCheck ? ECheckBoxState::Checked : ECheckBoxState::Unchecked);
}

bool SSettingsManager_Check::IsChecked() const
{
	return Widget_CheckBox->IsChecked();
}