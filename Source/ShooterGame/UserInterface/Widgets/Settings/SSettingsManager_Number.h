// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "Widgets/SCompoundWidget.h"
#include "Styles/SettingsManagerWidgetStyle.h"
#include "Input/SSliderNumber.h"

/**
 * 
 */
class SHOOTERGAME_API SSettingsManager_Number : public SCompoundWidget
{
public:
	SLATE_BEGIN_ARGS(SSettingsManager_Number)
		: _Style(&FShooterStyle::Get().GetWidgetStyle<FSettingsManagerStyle>("SSettingsManagerStyle"))
	{}
	SLATE_STYLE_ARGUMENT(FSettingsManagerStyle, Style)
	SLATE_ARGUMENT(FText, Name)
	SLATE_ARGUMENT(TArray<FText>, Values)
	SLATE_ATTRIBUTE(int32, Delta)
	SLATE_ATTRIBUTE(int32, Minimum)
	SLATE_ATTRIBUTE(int32, Maximum)
	SLATE_EVENT(SSliderNumber<int32>::FOnSliderValueChange, OnValueChange)
	SLATE_END_ARGS()

	void Construct(const FArguments& InArgs);

	void SetName(const FText&) const;
	FORCEINLINE FText GetName() const { return Widget_TextBlock->GetText(); }

	void SetValue(const int32& InValue) const;
	int32 GetValue() const;

protected:

	const FSettingsManagerStyle* Style;

	TSharedPtr<SSliderNumber<int32>> Widget_SliderNum;
	TSharedPtr<STextBlock> Widget_TextBlock;
};
