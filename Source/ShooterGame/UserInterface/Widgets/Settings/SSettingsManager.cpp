// VRSPRO

#include "ShooterGame.h"
#include "SSettingsManager.h"
#include "SlateOptMacros.h"

#include "GameInstance/ShooterGameUserSettings.h"

#include "SSettingsManager_Spin.h"
#include "SSettingsManager_Percent.h"
#include "SSettingsManager_Check.h"
#include "SSettingsManager_Controll.h"
#include "SSettingsManager_Dialog.h"

#include "Notify/SMessageBox.h"
#include "Containers/STabbedContainer.h"
#include "Utilities/SAnimatedBackground.h"
#include "Localization/TableBaseStrings.h"
#include "SSettingsManager_Number.h"

BEGIN_SLATE_FUNCTION_BUILD_OPTIMIZATION
void SSettingsManager::Construct(const FArguments& InArgs)
{
	Style = InArgs._Style;
	GameUserSettings = /*InArgs._GameUserSettings ? InArgs._GameUserSettings :*/ Cast<UShooterGameUserSettings>(GEngine->GameUserSettings);

#pragma region WidgetConstruction	

	auto ContentWidget = SNew(SOverlay)
			+ SOverlay::Slot().VAlign(VAlign_Top)
			[
				SNew(SHorizontalBox)
				+ SHorizontalBox::Slot().AutoWidth()
				[
					SNew(SButton)
					.ButtonStyle(&FShooterStyle::Get().GetWidgetStyle<FButtonStyle>("Default_BackButton"))
					.ContentPadding(80)
					.HAlign(HAlign_Center)
					.VAlign(VAlign_Center)
					.OnClicked(this, &SSettingsManager::OnClickedBack)
				]
				+ SHorizontalBox::Slot()
				+ SHorizontalBox::Slot()
			]
			+ SOverlay::Slot().VAlign(VAlign_Top).HAlign(HAlign_Center)
			[
				SNew(SBox).HeightOverride(100).VAlign(VAlign_Center)
				[
					SNew(STextBlock)
					.Text(NSLOCTEXT("Settings", "Settings.Title", "Settings"))
					.TextStyle(&FShooterStyle::Get().GetWidgetStyle<FTextBlockStyle>("Default_HeaderText"))
				]
			]
			+ SOverlay::Slot().Padding(0, 100, 0, 0)
			[
				SAssignNew(Widget_TabbedContainer, STabbedContainer).Orientation(Orient_Vertical)
				+ STabbedContainer::Slot().Name(NSLOCTEXT("Settings", "Settings.General", "General")).Padding(FMargin(10, 0))
				[
					SNew(SScrollBox)
					.ScrollBarStyle(&FShooterStyle::Get().GetWidgetStyle<FScrollBarStyle>("Default_ScrollBar")).ScrollBarThickness(FVector2D(4, 4))
					+ SScrollBox::Slot()[SNew(SBox).HeightOverride(10)] // Top Padding
					+ SScrollBox::Slot()
					[
						SAssignNew(Widget_Language, SSettingsManager_Spin).Name(NSLOCTEXT("Settings", "Settings.Language", "Language")).IsGraphics(false)
						.OnValueChange_Lambda([&](int32) { if (IsAllowChanged) { Widgets_Changed.AddUnique("Language"); } })
					]
					+ SScrollBox::Slot()
					[
						SAssignNew(Widget_CheckDamageNumbers, SSettingsManager_Check).Name(NSLOCTEXT("Settings", "Settings.ShowDamageNumbers", "Show Damage Numbers"))
						.OnValueChange_Lambda([&](bool) { if (IsAllowChanged) { Widgets_Changed.AddUnique("ShowDamageNumbers"); } })
					]
					+ SScrollBox::Slot()
					[
						SAssignNew(Widget_CheckEnableAutofire, SSettingsManager_Check).Name(NSLOCTEXT("Settings", "Settings.EnableAutoFire", "Enable Auto-Fire"))
						.OnValueChange_Lambda([&](bool) { if (IsAllowChanged) { Widgets_Changed.AddUnique("EnableAutoFire"); } })
					]
					+ SScrollBox::Slot()
					[
						SAssignNew(Widget_CheckEnableAutoTarget, SSettingsManager_Check).Name(NSLOCTEXT("Settings", "Settings.EnableAutoTarget", "Enable Auto-Targeting"))
						.OnValueChange_Lambda([&](bool) { if (IsAllowChanged) { Widgets_Changed.AddUnique("EnableAutoTarget"); } })
					]
					+ SScrollBox::Slot()
					[
						SAssignNew(Widget_CheckEnableDynamicStick, SSettingsManager_Check).Name(NSLOCTEXT("Settings", "Settings.DynamicStick", "Dynamic Stick"))
						.OnValueChange_Lambda([&](bool) { if (IsAllowChanged) { Widgets_Changed.AddUnique("EnableDynamicStick"); } })
					]
					+ SScrollBox::Slot()
					[
						SAssignNew(Widget_CheckEnableDebugInfo, SSettingsManager_Check).Name(NSLOCTEXT("Settings", "Settings.EnableDebugInfo", "Enable Debug Information (FPS, Ping)"))
						.OnValueChange_Lambda([&](bool) { if (IsAllowChanged) { Widgets_Changed.AddUnique("EnableDebugInfo"); } })
					]
				]
				+ STabbedContainer::Slot().Name(NSLOCTEXT("Settings", "Settings.Graphics", "Graphics")).Padding(FMargin(10, 0))
				[
					SNew(SScrollBox)
					.ScrollBarStyle(&FShooterStyle::Get().GetWidgetStyle<FScrollBarStyle>("Default_ScrollBar")).ScrollBarThickness(FVector2D(4, 4))
					+ SScrollBox::Slot()[SNew(SBox).HeightOverride(10)] // Top Padding
					+ SScrollBox::Slot()
					[
						SAssignNew(Widget_GeneralQuality, SSettingsManager_Spin).Name(NSLOCTEXT("Settings", "Settings.GeneralQuality", "General Quality"))
						.OnValueChange_Lambda([&](int32) { if (IsAllowChanged) { Widgets_Changed.AddUnique("GeneralQuality"); } })
					]
					+ SScrollBox::Slot()
					[
						SNew(SSpacer).Size(FVector2D(.0f, 40.0f))
					]
					+ SScrollBox::Slot()
					[
						SAssignNew(Widget_NumFPSLimit, SSettingsManager_Number).Name(NSLOCTEXT("Settings", "Settings.FPSLimit", "FPS Limit")).Delta(10).Minimum(30).Maximum(120)
						.OnValueChange_Lambda([&](int32) { if (IsAllowChanged) { Widgets_Changed.AddUnique("FPSLimit"); } })						
					]
					+ SScrollBox::Slot()
					[
						SAssignNew(Widget_AntiAliasingMethod, SSettingsManager_Spin).Name(NSLOCTEXT("Settings", "Settings.AntiAliasingMethod", "Anti-Aliasing Method")).IsGraphics(false)
						.OnValueChange_Lambda([&](int32) { if (IsAllowChanged) { Widgets_Changed.AddUnique("AntiAliasingMethod"); } })
					]
					+ SScrollBox::Slot()
					[
						SNew(SSpacer).Size(FVector2D(.0f, 20.0f))
					]
					+ SScrollBox::Slot()
					[
						SAssignNew(Widget_ResolutionQuality, SSettingsManager_Percent).Name(NSLOCTEXT("Settings", "Settings.ResolutionQuality", "Resolution Quality")).IsEnabled_Lambda([&]() { return Widget_GeneralQuality->GetActiveValueIndex() == static_cast<int32>(EQualityLevels::Personal); })
						.OnValueChange_Lambda([&](float) { if (IsAllowChanged) { Widgets_Changed.AddUnique("ResolutionQuality"); } })
					]
					+ SScrollBox::Slot()
					[
						SAssignNew(Widget_ViewDistance, SSettingsManager_Spin).Name(NSLOCTEXT("Settings", "Settings.ViewDistanceQuality", "View Distance")).IsEnabled_Lambda([&]() { return Widget_GeneralQuality->GetActiveValueIndex() == static_cast<int32>(EQualityLevels::Personal); })
						.OnValueChange_Lambda([&](int32) { if (IsAllowChanged) { Widgets_Changed.AddUnique("ViewDistanceQuality"); } })
					]
					+ SScrollBox::Slot()
					[
						SAssignNew(Widget_AntiAliasing, SSettingsManager_Spin).Name(NSLOCTEXT("Settings", "Settings.AntiAliasingQuality", "Anti-Aliasing")).IsEnabled_Lambda([&]() { return Widget_GeneralQuality->GetActiveValueIndex() == static_cast<int32>(EQualityLevels::Personal); })
						.OnValueChange_Lambda([&](int32) { if (IsAllowChanged) { Widgets_Changed.AddUnique("AntiAliasingQuality"); } })
					]
					+ SScrollBox::Slot()
					[
						SAssignNew(Widget_ShadowQuality, SSettingsManager_Spin).Name(NSLOCTEXT("Settings", "Settings.ShadowQuality", "Shadow Quality")).IsEnabled_Lambda([&]() { return Widget_GeneralQuality->GetActiveValueIndex() == static_cast<int32>(EQualityLevels::Personal); })
						.OnValueChange_Lambda([&](int32) { if (IsAllowChanged) { Widgets_Changed.AddUnique("ShadowQuality"); } })
					]
					+ SScrollBox::Slot()
					[
						SAssignNew(Widget_PostProcess, SSettingsManager_Spin).Name(NSLOCTEXT("Settings", "Settings.PostProcessQuality", "Post-Processing")).IsEnabled_Lambda([&]() { return Widget_GeneralQuality->GetActiveValueIndex() == static_cast<int32>(EQualityLevels::Personal); })
						.OnValueChange_Lambda([&](int32) { if (IsAllowChanged) { Widgets_Changed.AddUnique("PostProcessQuality"); } })
					]
					+ SScrollBox::Slot()
					[
						SAssignNew(Widget_TextureQuality, SSettingsManager_Spin).Name(NSLOCTEXT("Settings", "Settings.TextureQuality", "Texture Quality")).IsEnabled_Lambda([&]() { return Widget_GeneralQuality->GetActiveValueIndex() == static_cast<int32>(EQualityLevels::Personal); })
						.OnValueChange_Lambda([&](int32) { if (IsAllowChanged) { Widgets_Changed.AddUnique("TextureQuality"); } })
					]
					+ SScrollBox::Slot()
					[
						SAssignNew(Widget_EffectsQuality, SSettingsManager_Spin).Name(NSLOCTEXT("Settings", "Settings.EffectsQuality", "Effects Quality")).IsEnabled_Lambda([&]() { return Widget_GeneralQuality->GetActiveValueIndex() == static_cast<int32>(EQualityLevels::Personal); })
						.OnValueChange_Lambda([&](int32) { if (IsAllowChanged) { Widgets_Changed.AddUnique("EffectsQuality"); } })
					]
					+ SScrollBox::Slot()
					[
						SAssignNew(Widget_FoliageQuality, SSettingsManager_Spin).Name(NSLOCTEXT("Settings", "Settings.FoliageQuality", "Foliage Quality")).IsEnabled_Lambda([&]() { return Widget_GeneralQuality->GetActiveValueIndex() == static_cast<int32>(EQualityLevels::Personal); })
						.OnValueChange_Lambda([&](int32) { if (IsAllowChanged) { Widgets_Changed.AddUnique("FoliageQuality"); } })
					]
					+ SScrollBox::Slot()
					[
						SAssignNew(Widget_CheckVSync, SSettingsManager_Check).Name(NSLOCTEXT("Settings", "Settings.VSync", "VSync"))
						.OnValueChange_Lambda([&](bool) { if (IsAllowChanged) { Widgets_Changed.AddUnique("VSync"); } })
					]
					+ SScrollBox::Slot()
					[
						SAssignNew(Widget_CheckAmbientOcclusion, SSettingsManager_Check).Name(NSLOCTEXT("Settings", "Settings.AmbientOcclusion", "Ambient Occlusion"))
						.OnValueChange_Lambda([&](bool) { if (IsAllowChanged) { Widgets_Changed.AddUnique("AmbientOcclusion"); } })
					]
					+ SScrollBox::Slot()
					[
						SAssignNew(Widget_CheckMotionBlur, SSettingsManager_Check).Name(NSLOCTEXT("Settings", "Settings.MotionBlur", "Motion Blur"))
						.OnValueChange_Lambda([&](bool) { if (IsAllowChanged) { Widgets_Changed.AddUnique("MotionBlur"); } })
					]
					+ SScrollBox::Slot()
					[
						SAssignNew(Widget_CheckBloom, SSettingsManager_Check).Name(NSLOCTEXT("Settings", "Settings.Bloom", "Bloom"))
						.OnValueChange_Lambda([&](bool) { if (IsAllowChanged) { Widgets_Changed.AddUnique("Bloom"); } })
					]
				]
				+ STabbedContainer::Slot().Name(NSLOCTEXT("Settings", "Settings.Sound", "Sound")).Padding(FMargin(10, 0))
				[
					SNew(SScrollBox)
					.ScrollBarStyle(&FShooterStyle::Get().GetWidgetStyle<FScrollBarStyle>("Default_ScrollBar")).ScrollBarThickness(FVector2D(4, 4))
					+ SScrollBox::Slot()[SNew(SBox).HeightOverride(10)] // Top Padding
					+ SScrollBox::Slot()
					[
						SAssignNew(Widget_SoundMaster, SSettingsManager_Percent).Name(NSLOCTEXT("Settings", "Settings.SoundMaster", "Master"))
						.OnValueChange_Lambda([&](float) { if (IsAllowChanged) { Widgets_Changed.AddUnique("SoundMaster"); } })
					]
					+ SScrollBox::Slot()
					[
						SNew(SSpacer).Size(FVector2D(.0f, 40.0f))
					]
					+ SScrollBox::Slot()
					[
						SAssignNew(Widget_SoundSFX, SSettingsManager_Percent).Name(NSLOCTEXT("Settings", "Settings.SoundSFX", "Effects"))
						.OnValueChange_Lambda([&](float) { if (IsAllowChanged) { Widgets_Changed.AddUnique("SoundSFX"); } })
					]
					+ SScrollBox::Slot()
					[
						SAssignNew(Widget_SoundUI, SSettingsManager_Percent).Name(NSLOCTEXT("Settings", "Settings.SoundUI", "UI Effects"))
						.OnValueChange_Lambda([&](float) { if (IsAllowChanged) { Widgets_Changed.AddUnique("SoundUI"); } })
					]
					+ SScrollBox::Slot()
					[
						SAssignNew(Widget_SoundMusic, SSettingsManager_Percent).Name(NSLOCTEXT("Settings", "Settings.SoundMusic", "Music"))
						.OnValueChange_Lambda([&](float) { if (IsAllowChanged) { Widgets_Changed.AddUnique("SoundMusic"); } })
					]
					+ SScrollBox::Slot()
					[
						SAssignNew(Widget_SoundVoice, SSettingsManager_Percent).Name(NSLOCTEXT("Settings", "Settings.SoundVoice", "Voices (screams)"))
						.OnValueChange_Lambda([&](float) { if (IsAllowChanged) { Widgets_Changed.AddUnique("SoundVoice"); } })
					]
				]
				+ STabbedContainer::Slot().Name(NSLOCTEXT("Settings", "Settings.Controlls", "Controlls")).Padding(FMargin(10, 0))
				[
					SNew(SScrollBox)
					.ScrollBarStyle(&FShooterStyle::Get().GetWidgetStyle<FScrollBarStyle>("Default_ScrollBar")).ScrollBarThickness(FVector2D(4, 4))
					+ SScrollBox::Slot()[SNew(SBox).HeightOverride(10)] // Top Padding
					+ SScrollBox::Slot()
					[
						SAssignNew(Widget_CheckSmoothMouse, SSettingsManager_Check).Name(NSLOCTEXT("Settings", "Settings.SmoothMouse", "Smooth"))
						.OnValueChange_Lambda([&](bool) { if (IsAllowChanged) { Widgets_Changed.AddUnique("SmoothMouse"); } })
					]
					+ SScrollBox::Slot()
					[
						SAssignNew(Widget_CheckInvertMouseX, SSettingsManager_Check).Name(NSLOCTEXT("Settings", "Settings.InvertMouseX", "Invert Axis X"))
						.OnValueChange_Lambda([&](bool) { if (IsAllowChanged) { Widgets_Changed.AddUnique("InvertMouseX"); } })
					]
					+ SScrollBox::Slot()
					[
						SAssignNew(Widget_CheckInvertMouseY, SSettingsManager_Check).Name(NSLOCTEXT("Settings", "Settings.InvertMouseY", "Invert Axis Y"))
						.OnValueChange_Lambda([&](bool) { if (IsAllowChanged) { Widgets_Changed.AddUnique("InvertMouseY"); } })
					]
					+ SScrollBox::Slot()
					[
						SAssignNew(Widget_MouseSens, SSettingsManager_Percent).Name(NSLOCTEXT("Settings", "Settings.MouseSens", "Sensitivity"))
						.OnValueChange_Lambda([&](float) { if (IsAllowChanged) { Widgets_Changed.AddUnique("MouseSens"); } })
						.ButtonsDelta(.01f)
					]
					//+ SScrollBox::Slot()[SNew(SBox).HeightOverride(10)] // Top Padding
					//+ SScrollBox::Slot()
					//[
					//	SNew(STextBlock).TextStyle(&Style->Names).Text(NSLOCTEXT("Settings", "Settings.Keyboard.Title", "Keyboard or Gamepad")).Justification(ETextJustify::Center)
					//]
					//+ SScrollBox::Slot()[SNew(SBox).HeightOverride(10)] // Top Padding
					//+ SScrollBox::Slot()
					//[
					//	SAssignNew(ControllsContainer, SVerticalBox)
					//]
				]
			];

#pragma endregion 

	ChildSlot
		[
			SAssignNew(Anim_Back, SAnimatedBackground)
			.Visibility(EVisibility::Visible)
			.IsEnabledBlur(true)
			.Duration(.5f)
			.ShowAnimation(EAnimBackAnimation::UpFade)
			.HideAnimation(EAnimBackAnimation::DownFade)
			.InitAsHide(true)
			[
				ContentWidget
			]
		];

	//Widget_KeyDialog->SetVisibility(EVisibility::Hidden);
	Widget_GeneralQuality->AddValue(NSLOCTEXT("Settings", "Settings.Personal", "Personal"));

#pragma region WidgetInputSettings

	//ControllWidgets.Add(FControllWidget(NSLOCTEXT("Settings", "Settings.MoveForward", "Move Forward"), "MoveForward+"));
	//ControllWidgets.Add(FControllWidget(NSLOCTEXT("Settings", "Settings.MoveBackward", "Move Backward"), "MoveForward-"));
	//ControllWidgets.Add(FControllWidget(NSLOCTEXT("Settings", "Settings.MoveLeft", "Move Left"), "MoveRight-"));
	//ControllWidgets.Add(FControllWidget(NSLOCTEXT("Settings", "Settings.MoveRight", "Move Right"), "MoveRight+"));

	//ControllWidgets.Add(FControllWidget(NSLOCTEXT("Settings", "Settings.Jump", "Jump"), "Jump"));
	//ControllWidgets.Add(FControllWidget(NSLOCTEXT("Settings", "Settings.Run", "Run"), "Run"));
	//ControllWidgets.Add(FControllWidget(NSLOCTEXT("Settings", "Settings.ToggleRun", "Run (Switch)"), "ToggleRun"));

	//ControllWidgets.Add(FControllWidget(NSLOCTEXT("Settings", "Settings.TapForward", "Strafe Forward"), "TapForward"));
	//ControllWidgets.Add(FControllWidget(NSLOCTEXT("Settings", "Settings.TapBack", "Strafe Back"), "TapBack"));
	//ControllWidgets.Add(FControllWidget(NSLOCTEXT("Settings", "Settings.TapLeft", "Strafe Left"), "TapLeft"));
	//ControllWidgets.Add(FControllWidget(NSLOCTEXT("Settings", "Settings.TapRight", "Strafe Right"), "TapRight"));

	//ControllWidgets.Add(FControllWidget(NSLOCTEXT("Settings", "Settings.WeaponSwitchUp", "Next Weapon"), "WeaponSwitchUp"));
	//ControllWidgets.Add(FControllWidget(NSLOCTEXT("Settings", "Settings.WeaponSwitchDown", "Previus Weapon"), "WeaponSwitchDown"));
	//ControllWidgets.Add(FControllWidget(NSLOCTEXT("Settings", "Settings.Fire", "Weapon Fire"), "Fire"));
	//ControllWidgets.Add(FControllWidget(NSLOCTEXT("Settings", "Settings.InGameAiming", "Weapon Aiming"), "InGameAiming"));
	//ControllWidgets.Add(FControllWidget(NSLOCTEXT("Settings", "Settings.InGameAimingToggle", "Weapon Aiming (Switch)"), "InGameAimingToggle"));
	//ControllWidgets.Add(FControllWidget(NSLOCTEXT("Settings", "Settings.InGameChat", "Open Chat"), "InGameChat"));
	//ControllWidgets.Add(FControllWidget(NSLOCTEXT("Settings", "Settings.InGameESC", "Open Menu"), "InGameESC"));
	//ControllWidgets.Add(FControllWidget(NSLOCTEXT("Settings", "Settings.InGameTable", "Open Table"), "InGameTable"));

	//ControllWidgets.Add(FControllWidget(NSLOCTEXT("Settings", "Settings.ShowNames", "Show Names"), "ShowNames"));
	//ControllWidgets.Add(FControllWidget(NSLOCTEXT("Settings", "Settings.WeaponReload", "Reload Weapon"), "WeaponReload"));
	//ControllWidgets.Add(FControllWidget(NSLOCTEXT("Settings", "Settings.UseAny", "Use"), "UseAny"));
	//ControllWidgets.Add(FControllWidget(NSLOCTEXT("Settings", "Settings.UseGrenade", "Grenade"), "UseGrenade"));
	//ControllWidgets.Add(FControllWidget(NSLOCTEXT("Settings", "Settings.UseAbility", "Ability"), "UseAbility"));
	//ControllWidgets.Add(FControllWidget(NSLOCTEXT("Settings", "Settings.Crouch", "Crouch"), "Crouch"));
	//ControllWidgets.Add(FControllWidget(NSLOCTEXT("Settings", "Settings.CrouchToggle", "Crouch Toggle"), "CrouchToggle"));
	//ControllWidgets.Add(FControllWidget(NSLOCTEXT("Settings", "Settings.ToggleHUD", "ON/OFF HUD (In Fight)"), "ToggleHUD"));
	//ControllWidgets.Add(FControllWidget(NSLOCTEXT("Settings", "Settings.TiltLeft", "Tilt Left"), "TiltLeft"));
	//ControllWidgets.Add(FControllWidget(NSLOCTEXT("Settings", "Settings.TiltRight", "Tilt Right"), "TiltRight"));
	//ControllWidgets.Add(FControllWidget(NSLOCTEXT("Settings", "Settings.TiltLeftToggle", "Tilt Left (Switch)"), "TiltLeftToggle"));
	//ControllWidgets.Add(FControllWidget(NSLOCTEXT("Settings", "Settings.TiltRightToggle", "Tilt Right (Switch)"), "TiltRightToggle"));
	//ControllWidgets.Add(FControllWidget(NSLOCTEXT("Settings", "Settings.HudHand", "HUD Hand"), "HudHand"));
	//ControllWidgets.Add(FControllWidget(NSLOCTEXT("Settings", "Settings.ToggleHudHand", "HUD Hand (Switch)"), "ToggleHudHand"));
	//ControllWidgets.Add(FControllWidget(NSLOCTEXT("Settings", "Settings.SwitchView", "3P/1P View (Switch)"), "SwitchView"));


	//for (auto& i : ControllWidgets)
	//{
	//	ControllsContainer->AddSlot().AutoHeight()
	//		[
	//			SAssignNew(i.Widget, SSettingsManager_Controll)
	//			.Name(i.Desc).Key(i.Key)
	//			.OnCallChangeKey(this, &SSettingsManager::OnShowChangeKeyDialog)
	//			.OnRemoveKey(this, &SSettingsManager::OnRemoveKey)
	//		];
	//}

#pragma endregion 

	IsAllowChanged = true;

	if (GameUserSettings && GameUserSettings->IsValidLowLevel())
	{
		GameUserSettings->InitSettings();
		LoadSettings();
	}
	
}
END_SLATE_FUNCTION_BUILD_OPTIMIZATION

void SSettingsManager::ToggleWidget(const bool InToggle)
{
	SUsableCompoundWidget::ToggleWidget(InToggle);
	Anim_Back->ToggleWidget(InToggle);

	if (InToggle)
	{
		LoadSettings();
	}
}

bool SSettingsManager::IsInInteractiveMode() const
{
	return Anim_Back->IsAnimAtEnd();
}

FReply SSettingsManager::OnClickedGeneralButton(const bool IsApply)
{
	if (IsApply)
	{
		SaveSettings();
	}
	else
	{
		LoadSettings();
	}

	return FReply::Handled();
}

void SSettingsManager::OnRemoveKey(const FString& Key, const bool IsAlternative)
{
	if (Key.Contains("+") || Key.Contains("-"))
	{
		FindAxisBind(Key, IsAlternative)->Key = EKeys::Invalid;
	}
	else
	{
		FindKeyBind(Key, IsAlternative)->Key = EKeys::Invalid;
	}

	FillKeys();
}

void SSettingsManager::OnShowChangeKeyDialog(const FString& Key, const bool IsAlternative)
{
	//Widget_KeyDialog->SetVisibility(EVisibility::Visible);
	//FSlateApplication::Get().SetKeyboardFocus(Widget_KeyDialog);

	//if (Key.Contains("+") || Key.Contains("-"))
	//{
	//	IsTargetAxis = true;
	//	Input_AxisSelected = FindAxisBind(Key, IsAlternative);
	//}
	//else
	//{
	//	IsTargetAxis = false;
	//	Input_ActionSelected = FindKeyBind(Key, IsAlternative);
	//}
}

void SSettingsManager::OnKeyChanged(const FKey& Key)
{
	//Widget_KeyDialog->SetVisibility(EVisibility::Hidden);

	//if (Key != EKeys::Invalid)
	//{
	//	if (IsTargetAxis)
	//	{
	//		if (Input_AxisSelected)
	//			Input_AxisSelected->Key = Key;
	//	}
	//	else
	//	{
	//		if (Input_ActionSelected)
	//			Input_ActionSelected->Key = Key;
	//	}

	//	FillKeys();
	//}
}

FInputActionKeyMapping* SSettingsManager::FindKeyBind(const FString& Key, const bool IsAlternative)
{
	bool IsFound = false;

	for (SSIZE_T i = 0; i < Input_ActionMappings.Num(); ++i)
	{
		if (Input_ActionMappings[i].ActionName.ToString().Equals(Key))
		{
			if (!IsAlternative || IsFound)
			{
				return &Input_ActionMappings[i];
			}
			else
			{
				IsFound = true;
			}
		}
	}

	Input_ActionMappings.Add(FInputActionKeyMapping(*Key, EKeys::Invalid));
	return &Input_ActionMappings.Last();
}

FInputAxisKeyMapping* SSettingsManager::FindAxisBind(const FString& Key, const bool IsAlternative)
{
	float Scale = 1.0f;
	FString KeyString = Key;

	if (KeyString.Contains("-"))
	{
		KeyString.RemoveAt(KeyString.Find("-"));
		Scale = -1.0f;
	}
	else
	{
		KeyString.RemoveAt(KeyString.Find("+"));
	}

	bool IsFound = false;

	for (SSIZE_T i = 0; i < Input_AxisMappings.Num(); ++i)
	{
		if (Input_AxisMappings[i].AxisName.ToString().Equals(KeyString) && FMath::IsNearlyEqual(Input_AxisMappings[i].Scale, Scale))
		{
			if (!IsAlternative || IsFound)
			{
				return &Input_AxisMappings[i];
			}
			else
			{
				IsFound = true;
			}
		}
	}

	Input_AxisMappings.Add(FInputAxisKeyMapping(*KeyString, EKeys::Invalid, Scale));
	return &Input_AxisMappings.Last();
}

void SSettingsManager::FillKeys()
{
	//for (auto& i : ControllWidgets)
	//{
	//	if (i.Key.Contains("+") || i.Key.Contains("-"))
	//	{
	//		i.FirstKey = FindAxisBind(i.Key, false)->Key;
	//		i.Widget->SetKeyName(i.FirstKey.GetDisplayName());
	//		i.SecondKey = FindAxisBind(i.Key, true)->Key;
	//		i.Widget->SetKeyName(i.SecondKey.GetDisplayName(), true);
	//	}
	//	else
	//	{
	//		i.FirstKey = FindKeyBind(i.Key, false)->Key;
	//		i.Widget->SetKeyName(i.FirstKey.GetDisplayName());
	//		i.SecondKey = FindKeyBind(i.Key, true)->Key;
	//		i.Widget->SetKeyName(i.SecondKey.GetDisplayName(), true);
	//	}
	//}
}

FReply SSettingsManager::OnClickedBack()
{
	if (Widgets_Changed.Num())
	{
		QueueBegin(SMessageBox, "SaveSettingsDialog")
			SMessageBox::Get()->SetHeaderText(NSLOCTEXT("Settings", "Settings.Title", "Settings"));
			SMessageBox::Get()->SetContent(NSLOCTEXT("Settings", "Settings.Changed.Desc", "Some options are changed, do you want to save it?"));
			SMessageBox::Get()->SetButtonsText(FTableBaseStrings::GetBaseText(EBaseStrings::Yes), FTableBaseStrings::GetBaseText(EBaseStrings::No));
			SMessageBox::Get()->OnMessageBoxButton.AddLambda([&](EMessageBoxButton InButton)
			{
				if (InButton == EMessageBoxButton::Left)
				{
					SaveSettings();
				}
				SMessageBox::Get()->ToggleWidget(false);
				ToggleWidget(false);
			});
			SMessageBox::Get()->SetEnableClose(false);
			SMessageBox::Get()->ToggleWidget(true);
		QueueEnd
	}
	else
	{
		ToggleWidget(false);
	}

	return FReply::Handled();
}

void SSettingsManager::LoadSettings()
{
	IsAllowChanged = false;

	Widgets_Changed.Empty();

	Widget_Language->SetValues(TArray<FText>());
	Widget_AntiAliasingMethod->SetValues(TArray<FText>());

	Widget_AntiAliasingMethod->AddValue(NSLOCTEXT("Settings", "Settings.AntiAliasingMethod.None", "None"));
	Widget_AntiAliasingMethod->AddValue(NSLOCTEXT("Settings", "Settings.AntiAliasingMethod.FXAA", "FXAA"));
	//Widget_AntiAliasingMethod->AddValue(NSLOCTEXT("Settings", "Settings.AntiAliasingMethod.TemporalAA", "TemporalAA"));

	//if (IsForwardShadingEnabled(GMaxRHIShaderPlatform))
	//{
	//	Widget_AntiAliasingMethod->AddValue(NSLOCTEXT("Settings", "Settings.AntiAliasingMethod.MSAA2", "2xMSAA"));
	//	Widget_AntiAliasingMethod->AddValue(NSLOCTEXT("Settings", "Settings.AntiAliasingMethod.MSAA4", "4xMSAA"));
	//}

	Widget_AntiAliasingMethod->SetActiveValueIndex(GameUserSettings->GetAAMode());

	SIZE_T _count = 0;
	for (auto loc : GameUserSettings->GetLanguages())
	{
		Widget_Language->AddValue(FText::FromString(loc->GetEnglishName()));

		if (GameUserSettings->GetCurrentLanguage() == loc->GetLCID())
		{
			Widget_Language->SetActiveValueIndex(_count);
		}

		++_count;
	}		

	auto Scalability = GameUserSettings->ScalabilityQuality;
	auto ScreenResolutions = GameUserSettings->GetScreenResolutions();

	Widget_GeneralQuality			->SetActiveValueIndex(static_cast<int32>(GameUserSettings->GeneralQualityLevel));
	Widget_ViewDistance				->SetActiveValueIndex(Scalability.ViewDistanceQuality);
	Widget_AntiAliasing				->SetActiveValueIndex(Scalability.AntiAliasingQuality);
	Widget_ShadowQuality			->SetActiveValueIndex(Scalability.ShadowQuality);
	Widget_PostProcess				->SetActiveValueIndex(Scalability.PostProcessQuality);
	Widget_TextureQuality			->SetActiveValueIndex(Scalability.TextureQuality);
	Widget_EffectsQuality			->SetActiveValueIndex(Scalability.EffectsQuality);
	Widget_ResolutionQuality		->SetValue(float(Scalability.ResolutionQuality) / 100.0f);
	Widget_FoliageQuality			->SetActiveValueIndex(Scalability.FoliageQuality);

	Widget_SoundMaster				->SetValue(GameUserSettings->GetSoundVolume(ESoundClassType::Master));
	Widget_SoundSFX					->SetValue(GameUserSettings->GetSoundVolume(ESoundClassType::SFX));
	Widget_SoundUI					->SetValue(GameUserSettings->GetSoundVolume(ESoundClassType::UI));
	Widget_SoundMusic				->SetValue(GameUserSettings->GetSoundVolume(ESoundClassType::Music));
	Widget_SoundVoice				->SetValue(GameUserSettings->GetSoundVolume(ESoundClassType::Voice));

	Widget_CheckVSync				->SetIsChecked(GameUserSettings->IsVSyncEnabled());

	Widget_CheckAmbientOcclusion	->SetIsChecked(GameUserSettings->IsEnabledAmbientOcclusion());
	Widget_CheckMotionBlur			->SetIsChecked(GameUserSettings->IsEnabledMotionBlur());
	Widget_CheckBloom				->SetIsChecked(GameUserSettings->IsEnabledBloom());
	Widget_CheckDamageNumbers		->SetIsChecked(GameUserSettings->IsEnabledDamageNumbers());
	Widget_CheckEnableAutofire		->SetIsChecked(GameUserSettings->IsEnabledAutoFire());
	Widget_CheckEnableAutoTarget	->SetIsChecked(GameUserSettings->IsEnabledAutoTargeting());
	Widget_CheckEnableDebugInfo		->SetIsChecked(GameUserSettings->IsEnabledDebugInfo());
	Widget_CheckEnableDynamicStick	->SetIsChecked(GameUserSettings->IsDynamicStick());

	Widget_NumFPSLimit				->SetValue(FMath::FloorToInt(GameUserSettings->GetFrameRateLimit()));

	// Input bEnableMouseSmoothing
	auto InputSettings = GetDefault<UInputSettings>();

	Widget_CheckSmoothMouse			->SetIsChecked(InputSettings->bEnableMouseSmoothing);

	//Input_ActionMappings = InputSettings->ActionMappings;
	//Input_AxisMappings = InputSettings->AxisMappings;

	for (TObjectIterator<UPlayerInput> It; It; ++It)
	{
		Widget_CheckInvertMouseX->SetIsChecked(It->GetInvertAxisKey(EKeys::MouseX));
		Widget_CheckInvertMouseY->SetIsChecked(It->GetInvertAxisKey(EKeys::MouseY));

		Widget_MouseSens->SetValue(It->GetMouseSensitivityX() / 3.0f);
		break;
	}

	FillKeys();

	IsAllowChanged = true;
}

void SSettingsManager::SaveSettings()
{
	IsAllowChanged = false;

	//if (!CheckInputBinds())
	//{
	//	//show message
	//	SMessageBox::AddQueueMessage("SSettingsManager.KeyWarning", FOnClickedOutside::CreateLambda([&]() {
	//		SMessageBox::Get()->SetHeaderText(NSLOCTEXT("SSettingsManager", "SSettingsManager.KeyWarning.Title", "Duplicate keyword"));
	//		SMessageBox::Get()->SetContent(NSLOCTEXT("SSettingsManager", "SSettingsManager.KeyWarning.Desc", "Duplicate keyboard or gamepad key was found, maybe not working some keys..."));
	//		SMessageBox::Get()->SetButtonsText(NSLOCTEXT("All", "All.Ok", "OK"));
	//		SMessageBox::Get()->SetEnableClose(false);
	//		SMessageBox::Get()->OnMessageBoxButton.AddLambda([&](EMessageBoxButton Button) {
	//			SMessageBox::RemoveQueueMessage("SSettingsManager.KeyWarning");
	//			SMessageBox::Get()->ToggleWidget(false);
	//		});
	//		SMessageBox::Get()->ToggleWidget(true);
	//	}));
	//}

	auto Scalability = &GameUserSettings->ScalabilityQuality;

	if (Widgets_Changed.Contains("GeneralQuality"))
	{
		GameUserSettings->GeneralQualityLevel = static_cast<EQualityLevels>(Widget_GeneralQuality->GetActiveValueIndex());
	}
	
	if (GameUserSettings->GeneralQualityLevel == EQualityLevels::Personal)
	{
		if (Widgets_Changed.Contains("ViewDistanceQuality"))	Scalability->ViewDistanceQuality	= Widget_ViewDistance->GetActiveValueIndex();
		if (Widgets_Changed.Contains("AntiAliasingQuality"))	Scalability->AntiAliasingQuality	= Widget_AntiAliasing->GetActiveValueIndex();
		if (Widgets_Changed.Contains("ShadowQuality"))			Scalability->ShadowQuality			= Widget_ShadowQuality->GetActiveValueIndex();
		if (Widgets_Changed.Contains("PostProcessQuality"))		Scalability->PostProcessQuality		= Widget_PostProcess->GetActiveValueIndex();
		if (Widgets_Changed.Contains("TextureQuality"))			Scalability->TextureQuality			= Widget_TextureQuality->GetActiveValueIndex();
		if (Widgets_Changed.Contains("EffectsQuality"))			Scalability->EffectsQuality			= Widget_EffectsQuality->GetActiveValueIndex();
		if (Widgets_Changed.Contains("ResolutionQuality"))		Scalability->ResolutionQuality		= Widget_ResolutionQuality->GetValue() * 100.0f;
		if (Widgets_Changed.Contains("FoliageQuality"))			Scalability->FoliageQuality			= Widget_FoliageQuality->GetActiveValueIndex();		
	}
	else
	{		
		Scalability->SetFromSingleQualityLevel(static_cast<int32>(GameUserSettings->GeneralQualityLevel));
	}

	if (Widgets_Changed.Contains("Language"))
	{
		for (auto loc : GameUserSettings->GetLanguages())
		{
			if (loc->GetEnglishName() == Widget_Language->GetActiveValue().ToString())
			{
				GameUserSettings->SetCurrentLanguage(loc->GetLCID());
			}
		}
	}

	if (Widgets_Changed.Contains("AntiAliasingMethod"))
	{
		GameUserSettings->SetAAMode(Widget_AntiAliasingMethod->GetActiveValueIndex());
	}

	if (Widgets_Changed.Contains("SoundMaster"))			GameUserSettings->SetSoundVolume(ESoundClassType::Master, Widget_SoundMaster->GetValue());
	if (Widgets_Changed.Contains("SoundSFX"))				GameUserSettings->SetSoundVolume(ESoundClassType::SFX, Widget_SoundSFX->GetValue());
	if (Widgets_Changed.Contains("SoundUI"))				GameUserSettings->SetSoundVolume(ESoundClassType::UI, Widget_SoundUI->GetValue());
	if (Widgets_Changed.Contains("SoundMusic"))				GameUserSettings->SetSoundVolume(ESoundClassType::Music, Widget_SoundMusic->GetValue());
	if (Widgets_Changed.Contains("SoundVoice"))				GameUserSettings->SetSoundVolume(ESoundClassType::Voice, Widget_SoundVoice->GetValue());

	if (Widgets_Changed.Contains("VSync"))					GameUserSettings->SetVSyncEnabled(Widget_CheckVSync->IsChecked());

	if (Widgets_Changed.Contains("AmbientOcclusion"))		GameUserSettings->SetEnabledAmbientOcclusion(Widget_CheckAmbientOcclusion->IsChecked());
	if (Widgets_Changed.Contains("MotionBlur"))				GameUserSettings->SetEnabledMotionBlur(Widget_CheckMotionBlur->IsChecked());
	if (Widgets_Changed.Contains("Bloom"))					GameUserSettings->SetEnabledBloom(Widget_CheckBloom->IsChecked());
	if (Widgets_Changed.Contains("ShowDamageNumbers"))		GameUserSettings->SetEnabledDamageNumbers(Widget_CheckDamageNumbers->IsChecked());
	if (Widgets_Changed.Contains("EnableAutoFire"))			GameUserSettings->SetEnabledAutoFire(Widget_CheckEnableAutofire->IsChecked());
	if (Widgets_Changed.Contains("EnableAutoTarget"))		GameUserSettings->SetEnabledAutoTargeting(Widget_CheckEnableAutoTarget->IsChecked());
	if (Widgets_Changed.Contains("EnableDebugInfo"))		GameUserSettings->SetEnabledDebugInfo(Widget_CheckEnableDebugInfo->IsChecked());
	if (Widgets_Changed.Contains("EnableDynamicStick"))		GameUserSettings->SetDynamicStick(Widget_CheckEnableDynamicStick->IsChecked());

	if (Widgets_Changed.Contains("FPSLimit"))				GameUserSettings->SetFrameRateLimit(Widget_NumFPSLimit->GetValue());

	GameUserSettings->ApplySettings(false);

	// Input
	auto InputSettings = GetMutableDefault<UInputSettings>();

	//InputSettings->ActionMappings = Input_ActionMappings;
	//InputSettings->AxisMappings = Input_AxisMappings;

	if (Widgets_Changed.Contains("SmoothMouse"))	InputSettings->bEnableMouseSmoothing = Widget_CheckSmoothMouse->IsChecked();

	if (Widgets_Changed.Contains("InvertMouseX"))
	{
		FInputAxisProperties AxisKeyProperties;
		for (TObjectIterator<UPlayerInput> It; It; ++It)
		{			
			if (It->GetAxisProperties(EKeys::MouseX, AxisKeyProperties))
			{
				AxisKeyProperties.bInvert = Widget_CheckInvertMouseX->IsChecked();
				It->SetAxisProperties(EKeys::MouseX, AxisKeyProperties);				
			}
		}

		for (auto& Entry : InputSettings->AxisConfig)
		{
			if (Entry.AxisKeyName == EKeys::MouseX)
			{
				Entry.AxisProperties.bInvert = AxisKeyProperties.bInvert;
			}
		}
	}

	if (Widgets_Changed.Contains("InvertMouseY"))
	{
		FInputAxisProperties AxisKeyProperties;
		for (TObjectIterator<UPlayerInput> It; It; ++It)
		{			
			if (It->GetAxisProperties(EKeys::MouseY, AxisKeyProperties))
			{
				AxisKeyProperties.bInvert = Widget_CheckInvertMouseY->IsChecked();
				It->SetAxisProperties(EKeys::MouseY, AxisKeyProperties);				
			}
		}

		for (auto& Entry : InputSettings->AxisConfig)
		{
			if (Entry.AxisKeyName == EKeys::MouseY)
			{
				Entry.AxisProperties.bInvert = AxisKeyProperties.bInvert;
			}
		}
	}

	if (Widgets_Changed.Contains("MouseSens"))
	{
		for (TObjectIterator<UPlayerInput> It; It; ++It)
		{
			It->SetMouseSensitivity(Widget_MouseSens->GetValue() * 3.0f);
		}

		for (auto& Entry : InputSettings->AxisConfig)
		{
			if (Entry.AxisKeyName == EKeys::MouseX || Entry.AxisKeyName == EKeys::MouseY)
			{
				Entry.AxisProperties.Sensitivity = Widget_MouseSens->GetValue() * 3.0f;
			}
		}
	}

	for (TObjectIterator<UPlayerInput> It; It; ++It)
	{
		It->ForceRebuildingKeyMaps(true);
	}

	GameUserSettings->SaveSettings();
	InputSettings->SaveConfig();

	LoadSettings();
}

bool SSettingsManager::CheckInputBinds()
{
	for (auto &i : ControllWidgets)
	{
		for (auto &k : ControllWidgets)
		{
			const bool bIsFirstKeys = (k.Key != i.Key && k.FirstKey == i.FirstKey && k.FirstKey.IsValid() && i.FirstKey.IsValid());
			const bool bIsSecondKeys = (k.Key != i.Key && k.SecondKey == i.SecondKey && k.SecondKey.IsValid() && i.SecondKey.IsValid());
			const bool bIsFirstSecondKeys = (k.Key != i.Key && k.SecondKey == i.FirstKey && k.SecondKey.IsValid() && i.FirstKey.IsValid());

			if (bIsFirstKeys || bIsSecondKeys || bIsFirstSecondKeys)
			{
				return false;
			}
		}
	}

	return true;
}