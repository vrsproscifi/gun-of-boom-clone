// VRSPRO

#pragma once


#include "Widgets/SCompoundWidget.h"
#include "Styles/SettingsManagerWidgetStyle.h"
DECLARE_DELEGATE_OneParam(FOnKeyChanged, const FKey&);

class SSettingsManager_Dialog : public SCompoundWidget
{
public:
	SLATE_BEGIN_ARGS(SSettingsManager_Dialog)
		: _Style(&FShooterStyle::Get().GetWidgetStyle<FSettingsManagerStyle>("SSettingsManagerStyle"))
	{}
	SLATE_STYLE_ARGUMENT(FSettingsManagerStyle, Style)
	SLATE_EVENT(FOnKeyChanged, OnKeyChanged)
	SLATE_END_ARGS()

	void Construct(const FArguments& InArgs);

protected:

	const FSettingsManagerStyle* Style;

	FOnKeyChanged OnKeyChanged;
	FKey CurrentKey;

	virtual FReply OnMouseButtonDown(const FGeometry& MyGeometry, const FPointerEvent& MouseEvent) override;
	virtual FReply OnPreviewKeyDown(const FGeometry& MyGeometry, const FKeyEvent& InKeyEvent) override;
	virtual FReply OnMouseWheel(const FGeometry& MyGeometry, const FPointerEvent& MouseEvent) override;

	virtual bool SupportsKeyboardFocus() const override;

	FReply OnClickedButtonEvent(const bool);

	TSharedPtr<STextBlock> Widget_TestKeyName;
};
