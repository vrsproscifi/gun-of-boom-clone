// VRSPRO

#pragma once


#include "Widgets/SCompoundWidget.h"
#include "Styles/SettingsManagerWidgetStyle.h"
/**
 * 
 */
class SSettingsManager_Check : public SCompoundWidget
{
public:
	SLATE_BEGIN_ARGS(SSettingsManager_Check)
		: _Style(&FShooterStyle::Get().GetWidgetStyle<FSettingsManagerStyle>("SSettingsManagerStyle"))
	{}
	SLATE_STYLE_ARGUMENT(FSettingsManagerStyle, Style)
	SLATE_EVENT(FOnBooleanValueChanged, OnValueChange)
	SLATE_ARGUMENT(FText, Name)
	SLATE_END_ARGS()

	void Construct(const FArguments& InArgs);

	void SetName(const FText&);
	FORCEINLINE FText GetName() const { return Widget_TextBlock->GetText(); }

	void SetIsChecked(const bool);
	bool IsChecked() const;

protected:

	const FSettingsManagerStyle* Style;

	FOnBooleanValueChanged OnValueChange;

	TSharedPtr<STextBlock> Widget_TextBlock;
	TSharedPtr<SCheckBox> Widget_CheckBox;
};
