// Fill out your copyright notice in the Description page of Project Settings.

#include "ShooterGame.h"
#include "SSettingsManager_Number.h"
#include "SlateOptMacros.h"

BEGIN_SLATE_FUNCTION_BUILD_OPTIMIZATION
void SSettingsManager_Number::Construct(const FArguments& InArgs)
{
	Style = InArgs._Style;

	ChildSlot
	[
		SNew(SVerticalBox)
		+ SVerticalBox::Slot().AutoHeight()
		[
			SNew(SHorizontalBox)
			+ SHorizontalBox::Slot().VAlign(VAlign_Center)
			[
				SAssignNew(Widget_TextBlock, STextBlock).Text(InArgs._Name)
				.TextStyle(&Style->Names)
			]
			+ SHorizontalBox::Slot()
			[
				SNew(SBox).HeightOverride(Style->HeightOverride)
				[
					SAssignNew(Widget_SliderNum, SSliderNumber<int32>)
					.ButtonsDelta(InArgs._Delta)
					.SliderMinimum(InArgs._Minimum)
					.SliderMaximum(InArgs._Maximum)
					.OnValueChange(InArgs._OnValueChange)
					.Style(&Style->Percent)
				]
			]
		]
		+ SVerticalBox::Slot().AutoHeight()
		[
			SNew(SSpacer).Size(FVector2D(0, Style->BottomRowPadding))
		]
	];
}
END_SLATE_FUNCTION_BUILD_OPTIMIZATION

void SSettingsManager_Number::SetName(const FText& Name) const
{
	Widget_TextBlock->SetText(Name);
}

void SSettingsManager_Number::SetValue(const int32& InValue) const
{
	Widget_SliderNum->SetValue(InValue);
}

int32 SSettingsManager_Number::GetValue() const
{
	return Widget_SliderNum->GetValue();
}
