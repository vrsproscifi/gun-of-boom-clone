// VRSPRO

#include "ShooterGame.h"
#include "SSettingsManager_Percent.h"
#include "SlateOptMacros.h"

BEGIN_SLATE_FUNCTION_BUILD_OPTIMIZATION
void SSettingsManager_Percent::Construct(const FArguments& InArgs)
{
	Style = InArgs._Style;
	OnValueChange = InArgs._OnValueChange;

	ChildSlot
	[
		SNew(SVerticalBox)
		+ SVerticalBox::Slot().AutoHeight()
		[
			SNew(SHorizontalBox)
			+ SHorizontalBox::Slot().VAlign(VAlign_Center)
			[
				SAssignNew(Widget_TextBlock, STextBlock).Text(InArgs._Name)
				.TextStyle(&Style->Names)
			]
			+ SHorizontalBox::Slot()
			[
				SNew(SBox).HeightOverride(Style->HeightOverride)
				[
					SAssignNew(Widget_SliderPercent, SSliderPercent)
					.Style(&Style->Percent)
					.OnValueChange(OnValueChange)
					.ButtonsDelta(InArgs._ButtonsDelta)
				]
			]
		]
		+ SVerticalBox::Slot().AutoHeight()
		[
			SNew(SSpacer).Size(FVector2D(0, Style->BottomRowPadding))
		]
	];
}
END_SLATE_FUNCTION_BUILD_OPTIMIZATION

void SSettingsManager_Percent::SetName(const FText& Name)
{
	Widget_TextBlock->SetText(Name);
}

void SSettingsManager_Percent::SetValue(const float Value)
{
	Widget_SliderPercent->SetValue(Value);
}

float SSettingsManager_Percent::GetValue() const
{
	return Widget_SliderPercent->GetValue();
}