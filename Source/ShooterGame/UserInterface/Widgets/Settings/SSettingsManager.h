// VRSPRO

#pragma once

#include "Styles/SettingsManagerWidgetStyle.h"
#include "Utilities/SUsableCompoundWidget.h"

class SSettingsManager_Number;
class SAnimatedBackground;
class STabbedContainer;
class UShooterGameUserSettings;

class SSettingsManager_Spin;
class SSettingsManager_Percent;
class SSettingsManager_Check;
class SSettingsManager_Controll;
class SSettingsManager_Dialog;

struct FInputActionKeyMapping;
struct FInputAxisKeyMapping;

struct FControllWidget
{
	FText Desc;
	FString Key;
	TSharedPtr<SSettingsManager_Controll> Widget;
	FKey FirstKey;
	FKey SecondKey;

	FControllWidget(const FText& InDesc, const FString& InKey) : Desc(InDesc), Key(InKey), Widget(), FirstKey(), SecondKey() {}
};

class SSettingsManager : public SUsableCompoundWidget
{
public:
	SLATE_BEGIN_ARGS(SSettingsManager)
		: _Style(&FShooterStyle::Get().GetWidgetStyle<FSettingsManagerStyle>("SSettingsManagerStyle"))
		, _IsCentered(false)
	{
		_Visibility = EVisibility::SelfHitTestInvisible;
	}
	SLATE_STYLE_ARGUMENT(FSettingsManagerStyle, Style)
	SLATE_ARGUMENT(UShooterGameUserSettings*, GameUserSettings)
	SLATE_ARGUMENT(bool, IsCentered)
	SLATE_END_ARGS()

	void Construct(const FArguments& InArgs);

	virtual void ToggleWidget(const bool InToggle) override;
	virtual bool IsInInteractiveMode() const override;

	void LoadSettings();
	void SaveSettings();

	bool CheckInputBinds();

	UShooterGameUserSettings* GameUserSettings;

protected:

	const FSettingsManagerStyle* Style;

	TArray<FInputActionKeyMapping> Input_ActionMappings;
	TArray<FInputAxisKeyMapping> Input_AxisMappings;

	bool IsTargetAxis;
	FInputActionKeyMapping* Input_ActionSelected;
	FInputAxisKeyMapping* Input_AxisSelected;

	FReply OnClickedGeneralButton(const bool);
	void OnShowChangeKeyDialog(const FString&, const bool);
	void OnRemoveKey(const FString&, const bool);
	void OnKeyChanged(const FKey&);

	void FillKeys();

	FReply OnClickedBack();

	FInputActionKeyMapping* FindKeyBind(const FString&, const bool);
	FInputAxisKeyMapping* FindAxisBind(const FString&, const bool);

	TArray<FName> Widgets_Changed;
	bool IsAllowChanged;

	TSharedPtr<SAnimatedBackground> Anim_Back;
	TSharedPtr<STabbedContainer> Widget_TabbedContainer;

	TSharedPtr<SSettingsManager_Spin> 
		  Widget_Language
		, Widget_GeneralQuality
		, Widget_ViewDistance
		, Widget_AntiAliasingMethod
		, Widget_AntiAliasing
		, Widget_ShadowQuality
		, Widget_PostProcess
		, Widget_TextureQuality
		, Widget_EffectsQuality
		, Widget_FoliageQuality;

	TSharedPtr<SSettingsManager_Percent>
		  Widget_SoundMaster
		, Widget_SoundSFX
		, Widget_SoundUI
		, Widget_SoundMusic
		, Widget_SoundVoice
		, Widget_ResolutionQuality
		, Widget_MouseSens;

	TSharedPtr<SSettingsManager_Dialog>
		  Widget_KeyDialog;

	TSharedPtr<SSettingsManager_Check>
		  Widget_CheckVSync
		, Widget_CheckSmoothMouse
		, Widget_CheckInvertMouseX
		, Widget_CheckInvertMouseY
		, Widget_CheckAmbientOcclusion
		, Widget_CheckMotionBlur
		, Widget_CheckBloom
		, Widget_CheckDamageNumbers
		, Widget_CheckEnableAutofire
		, Widget_CheckEnableAutoTarget
		, Widget_CheckEnableDebugInfo
		, Widget_CheckEnableDynamicStick;

	TSharedPtr<SSettingsManager_Number>
		  Widget_NumFPSLimit;

	TSharedPtr<SVerticalBox> ControllsContainer;
	TArray<FControllWidget> ControllWidgets;
};
