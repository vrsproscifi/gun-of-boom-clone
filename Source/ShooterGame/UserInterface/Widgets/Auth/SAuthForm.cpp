// VRSPRO

#include "ShooterGame.h"
#include "SAuthForm.h"
#include "SlateOptMacros.h"

#include "Notify/SMessageBox.h"
#include "Utilities/SAnimatedBackground.h"
#include "SBackgroundBlur.h"
#include "Localization/TableBaseStrings.h"

#define LOCTEXT_NAMESPACE "SAuthForm"

BEGIN_SLATE_FUNCTION_BUILD_OPTIMIZATION
void SAuthForm::Construct(const FArguments& InArgs)
{
	Style = InArgs._Style;
	OnQuit = InArgs._OnQuit;
	OnAuthentication = InArgs._OnAuthentication;

	ChildSlot
	[
		SAssignNew(AnimatedBackground[0], SAnimatedBackground)
		.ShowAnimation(EAnimBackAnimation::Color)
		.HideAnimation(EAnimBackAnimation::Color)
		.IsRelative(true)
		.Duration(1.6f)
		[
			SNew(SBackgroundBlur)
			.Padding(FMargin(100, 0))
			.bApplyAlphaToBlur(Style->bApplyAlphaToBlur)
			.BlurStrength(Style->BlurStrength)
			.BlurRadius(Style->BlurRadius)
			.LowQualityFallbackBrush(new FSlateNoResource())
			.HAlign(HAlign_Center)
			.VAlign(VAlign_Center)
			[
				SAssignNew(AnimatedBackground[1], SAnimatedBackground)
				.ShowAnimation(EAnimBackAnimation::ZoomOut)
				.HideAnimation(EAnimBackAnimation::ZoomIn)
				.IsRelative(true)
				.Duration(1.5f)
				[
					SNew(SBorder)
					.BorderImage(&Style->Background)
					.Padding(0)
					[
						SNew(SBorder)
						.BorderImage(&Style->Border)
						.Padding(FMargin(10, 20))
						[
							SNew(SVerticalBox)
							+ SVerticalBox::Slot().AutoHeight().VAlign(VAlign_Center).HAlign(HAlign_Center).Padding(40)
							[
								SNew(STextBlock).Text(LOCTEXT("Welcome", "Welcome to LOKA: Alternative World"))
								.TextStyle(&Style->Header)
							]
							+ SVerticalBox::Slot().AutoHeight().VAlign(VAlign_Center).Padding(2)
							[
								SAssignNew(Widget_InputEmail, SEditableTextBox)
								.Style(&Style->Inputs)
								.HintText(LOCTEXT("E-Mail", "E-Mail"))
							]
							+ SVerticalBox::Slot().AutoHeight().VAlign(VAlign_Center).Padding(2)
							[
								SAssignNew(Widget_InputPass, SEditableTextBox)
								.Style(&Style->Inputs)
								.HintText(LOCTEXT("Password", "Password"))
								.IsPassword(true)
							]
							+ SVerticalBox::Slot().AutoHeight().VAlign(VAlign_Center).Padding(2, 20)
							[
								SAssignNew(Widget_IsRememberMe, SCheckBox)
								.Style(&Style->CheckBox)
								[
									SNew(STextBlock).Text(LOCTEXT("RememberMe", "Remember me"))
									.TextStyle(&Style->CheckBoxText)
								]
							]
							+ SVerticalBox::Slot().AutoHeight().VAlign(VAlign_Center)
							[
								SNew(SHorizontalBox)
								+ SHorizontalBox::Slot()
								[
									SNew(SButton).Text(LOCTEXT("SignIn", "SIGN IN"))
									.OnClicked(this, &SAuthForm::OnClickedSign, true)
									.HAlign(HAlign_Center)
									.VAlign(VAlign_Center)
									.TextStyle(&Style->ButtonsText)
									.ButtonStyle(&Style->ButtonLeft)
								]
								+ SHorizontalBox::Slot()
								[
									SNew(SButton).Text(LOCTEXT("SignUp", "SIGN UP"))
									.OnClicked(this, &SAuthForm::OnClickedSign, false)
									.HAlign(HAlign_Center)
									.VAlign(VAlign_Center)
									.TextStyle(&Style->ButtonsText)
									.ButtonStyle(&Style->ButtonRight)
								]
							]
							+ SVerticalBox::Slot().AutoHeight().VAlign(VAlign_Center)
							[
								SNew(SBox).MinDesiredHeight(120)
								[
									SAssignNew(AnimatedError, SAnimatedBackground)
									.ShowAnimation(EAnimBackAnimation::ZoomIn)
									.HideAnimation(EAnimBackAnimation::ZoomOut)
									.Duration(.5f)
									.InitAsHide(true)
									[
										SNew(SBorder)
										.BorderImage(&Style->ErrorBackground)
										.Padding(FMargin(8, 4))
										[
											SAssignNew(Widget_ErrorMessage, STextBlock).TextStyle(&Style->ErrorText).AutoWrapText(true)
										]
									]
								]
							]
						]
					]
				]
			]
		]
	];

	LoadAccount();

	AnimatedBackground[0]->ToggleWidget(true);
}
END_SLATE_FUNCTION_BUILD_OPTIMIZATION

FReply SAuthForm::OnClickedSign(const bool IsSignIn)
{
	if (IsSignIn == false)
	{
		FPlatformProcess::LaunchURL(TEXT("http://lokagame.com/Account/Register"), nullptr, nullptr);
		if (FSlateApplication::Get().GetActiveTopLevelWindow().IsValid())
		{
			FSlateApplication::Get().GetActiveTopLevelWindow()->Minimize();
		}
	}
	else
	{
		OnAuthentication.ExecuteIfBound(FPlayerAuthorizationRequest(Widget_InputEmail->GetText().ToString(), Widget_InputPass->GetText().ToString(), IsSignIn));
		SaveAccount();
	}

	return FReply::Handled();
}

void SAuthForm::ToggleWidget(const bool InToggle)
{
	AnimatedBackground[0]->ToggleWidget(InToggle);
	AnimatedBackground[1]->ToggleWidget(InToggle);

	SUsableCompoundWidget::ToggleWidget(InToggle);
}

bool SAuthForm::IsInInteractiveMode() const
{
	return AnimatedBackground[0]->IsAnimAtEnd();
}

void SAuthForm::OnErrorMessage(const FText& Message)
{
	if (!Message.IsEmpty())
	{
		Widget_ErrorMessage->SetText(Message);
		AnimatedError->ToggleWidget(true);

		RegisterActiveTimer(15.0f, FWidgetActiveTimerDelegate::CreateSP(this, &SAuthForm::OnErrorMessageHide));
	}
	else
	{
		AnimatedError->ToggleWidget(false);
	}
}

EActiveTimerReturnType SAuthForm::OnErrorMessageHide(double InCurrentTime, float InDeltaTime)
{
	AnimatedError->ToggleWidget(false);
	return EActiveTimerReturnType::Stop;
}

void SAuthForm::SaveAccount()
{
	const FString
		_login = Widget_IsRememberMe->IsChecked() ? Widget_InputEmail->GetText().ToString() : FString(),
		_passw = Widget_IsRememberMe->IsChecked() ? Widget_InputPass->GetText().ToString() : FString();

	const auto SaveFileName = FPaths::GeneratedConfigDir() + "Account.ini";
	auto SaveFile = FConfigFile();
	SaveFile.SetString(TEXT("Account"), TEXT("RememberMe"), Widget_IsRememberMe->IsChecked() ? TEXT("True") : TEXT("False"));
	SaveFile.SetString(TEXT("Account"), TEXT("Login"), *_login);
	SaveFile.SetString(TEXT("Account"), TEXT("Password"), *_passw);

	if (!SaveFile.Write(SaveFileName))
	{
		SaveFile.UpdateSections(*SaveFileName, TEXT("Account"));
	}
}

void SAuthForm::LoadAccount()
{
	FString _remember("False"), _login, _passw;

	auto SaveFile = FConfigFile();
	SaveFile.Read(FPaths::GeneratedConfigDir() + "Account.ini");
	SaveFile.GetString(TEXT("Account"), TEXT("RememberMe"), _remember);
	SaveFile.GetString(TEXT("Account"), TEXT("Login"), _login);
	SaveFile.GetString(TEXT("Account"), TEXT("Password"), _passw);

	Widget_IsRememberMe->SetIsChecked(_remember.Equals("True", ESearchCase::IgnoreCase) ? ECheckBoxState::Checked : ECheckBoxState::Unchecked);
	Widget_InputEmail->SetText(FText::FromString(_login));
	Widget_InputPass->SetText(FText::FromString(_passw));
}

#undef LOCTEXT_NAMESPACE
