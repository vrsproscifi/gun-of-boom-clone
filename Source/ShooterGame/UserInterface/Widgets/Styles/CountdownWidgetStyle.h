// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "Styling/SlateWidgetStyle.h"
#include "SlateWidgetStyleContainerBase.h"

#include "CountdownWidgetStyle.generated.h"

/**
 * 
 */
USTRUCT()
struct SHOOTERGAME_API FCountdownStyle : public FSlateWidgetStyle
{
	GENERATED_USTRUCT_BODY()

	UPROPERTY(Category = Appearance, EditAnywhere)		FTextBlockStyle		NumberFont;

	UPROPERTY(Category = Appearance, EditAnywhere)		UCurveLinearColor*	CurveColor;
	UPROPERTY(Category = Appearance, EditAnywhere)		UCurveFloat*		CurveScale;

	FCountdownStyle();
	virtual ~FCountdownStyle();

	// FSlateWidgetStyle
	virtual void GetResources(TArray<const FSlateBrush*>& OutBrushes) const override;
	static const FName TypeName;
	virtual const FName GetTypeName() const override { return TypeName; };
	static const FCountdownStyle& GetDefault();
};

/**
 */
UCLASS(hidecategories=Object, MinimalAPI)
class UCountdownWidgetStyle : public USlateWidgetStyleContainerBase
{
	GENERATED_BODY()

public:
	/** The actual data describing the widget appearance. */
	UPROPERTY(Category=Appearance, EditAnywhere, meta=(ShowOnlyInnerProperties))
	FCountdownStyle WidgetStyle;

	virtual const struct FSlateWidgetStyle* const GetStyle() const override
	{
		return static_cast< const struct FSlateWidgetStyle* >( &WidgetStyle );
	}
};
