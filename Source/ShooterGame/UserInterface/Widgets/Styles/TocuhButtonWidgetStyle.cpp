// Fill out your copyright notice in the Description page of Project Settings.

#include "ShooterGame.h"
#include "TocuhButtonWidgetStyle.h"


FTocuhButtonStyle::FTocuhButtonStyle()
{
}

FTocuhButtonStyle::~FTocuhButtonStyle()
{
}

const FName FTocuhButtonStyle::TypeName(TEXT("FTocuhButtonStyle"));

const FTocuhButtonStyle& FTocuhButtonStyle::GetDefault()
{
	static FTocuhButtonStyle Default;
	return Default;
}

void FTocuhButtonStyle::GetResources(TArray<const FSlateBrush*>& OutBrushes) const
{
	// Add any brush resources here so that Slate can correctly atlas and reference them
}

