// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "Styling/SlateWidgetStyle.h"
#include "SlateWidgetStyleContainerBase.h"
#include "GameItemType.h"
#include "TocuhInputSwitchWidgetStyle.generated.h"


/**
 * 
 */
USTRUCT()
struct SHOOTERGAME_API FTocuhInputSwitchStyle : public FSlateWidgetStyle
{
	GENERATED_USTRUCT_BODY()

	UPROPERTY(Category = Appearance, EditAnywhere)	FCheckBoxStyle						Button;
	UPROPERTY(Category = Appearance, EditAnywhere)	FSlateBrush							Icon;
	UPROPERTY(Category = Appearance, EditAnywhere)	FButtonStyle						Buttons;
	UPROPERTY(Category = Appearance, EditAnywhere)	TMap<EGameItemType, FSlateBrush>	WeaponTypes;

	FTocuhInputSwitchStyle();
	virtual ~FTocuhInputSwitchStyle();

	// FSlateWidgetStyle
	virtual void GetResources(TArray<const FSlateBrush*>& OutBrushes) const override;
	static const FName TypeName;
	virtual const FName GetTypeName() const override { return TypeName; };
	static const FTocuhInputSwitchStyle& GetDefault();
};

/**
 */
UCLASS(hidecategories=Object, MinimalAPI)
class UTocuhInputSwitchWidgetStyle : public USlateWidgetStyleContainerBase
{
	GENERATED_BODY()

public:
	/** The actual data describing the widget appearance. */
	UPROPERTY(Category=Appearance, EditAnywhere, meta=(ShowOnlyInnerProperties))
	FTocuhInputSwitchStyle WidgetStyle;

	virtual const struct FSlateWidgetStyle* const GetStyle() const override
	{
		return static_cast< const struct FSlateWidgetStyle* >( &WidgetStyle );
	}
};
