// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "Styling/SlateWidgetStyle.h"
#include "SlateWidgetStyleContainerBase.h"

#include "SearchFightScreenWidgetStyle.generated.h"

/**
 * 
 */
USTRUCT()
struct SHOOTERGAME_API FSearchFightScreenStyle : public FSlateWidgetStyle
{
	GENERATED_USTRUCT_BODY()

	UPROPERTY(Category = Appearance, EditAnywhere) FSlateBrush		Backgound;
	UPROPERTY(Category = Appearance, EditAnywhere) FSlateBrush		BackgoundMapName;
	UPROPERTY(Category = Appearance, EditAnywhere) FSlateBrush		BackgoundMapDesc;

	UPROPERTY(Category = Appearance, EditAnywhere) FTextBlockStyle	StatusText;
	UPROPERTY(Category = Appearance, EditAnywhere) FTextBlockStyle	MapNameText;
	UPROPERTY(Category = Appearance, EditAnywhere) FTextBlockStyle	MapDescText;

	UPROPERTY(Category = Appearance, EditAnywhere) FTextBlockStyle	CancelText;
	UPROPERTY(Category = Appearance, EditAnywhere) FButtonStyle		CancelButton;

	UPROPERTY(Category = Experimental, EditAnywhere)	TArray<TSubclassOf<UUserWidget>>  UserWidgetsOverlay;

	FSearchFightScreenStyle();
	virtual ~FSearchFightScreenStyle();

	// FSlateWidgetStyle
	virtual void GetResources(TArray<const FSlateBrush*>& OutBrushes) const override;
	static const FName TypeName;
	virtual const FName GetTypeName() const override { return TypeName; };
	static const FSearchFightScreenStyle& GetDefault();
};

/**
 */
UCLASS(hidecategories=Object, MinimalAPI)
class USearchFightScreenWidgetStyle : public USlateWidgetStyleContainerBase
{
	GENERATED_BODY()

public:
	/** The actual data describing the widget appearance. */
	UPROPERTY(Category=Appearance, EditAnywhere, meta=(ShowOnlyInnerProperties))
	FSearchFightScreenStyle WidgetStyle;

	virtual const struct FSlateWidgetStyle* const GetStyle() const override
	{
		return static_cast< const struct FSlateWidgetStyle* >( &WidgetStyle );
	}
};
