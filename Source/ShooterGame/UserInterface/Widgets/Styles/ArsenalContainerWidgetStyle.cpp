// Fill out your copyright notice in the Description page of Project Settings.

#include "ShooterGame.h"
#include "ArsenalContainerWidgetStyle.h"


FArsenalContainerStyle::FArsenalContainerStyle()
{
}

FArsenalContainerStyle::~FArsenalContainerStyle()
{
}

const FName FArsenalContainerStyle::TypeName(TEXT("FArsenalContainerStyle"));

const FArsenalContainerStyle& FArsenalContainerStyle::GetDefault()
{
	static FArsenalContainerStyle Default;
	return Default;
}

void FArsenalContainerStyle::GetResources(TArray<const FSlateBrush*>& OutBrushes) const
{
	// Add any brush resources here so that Slate can correctly atlas and reference them
}

