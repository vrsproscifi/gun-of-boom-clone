// Fill out your copyright notice in the Description page of Project Settings.

#include "ShooterGame.h"
#include "TocuhInputSwitchWidgetStyle.h"


FTocuhInputSwitchStyle::FTocuhInputSwitchStyle()
{
}

FTocuhInputSwitchStyle::~FTocuhInputSwitchStyle()
{
}

const FName FTocuhInputSwitchStyle::TypeName(TEXT("FTocuhInputSwitchStyle"));

const FTocuhInputSwitchStyle& FTocuhInputSwitchStyle::GetDefault()
{
	static FTocuhInputSwitchStyle Default;
	return Default;
}

void FTocuhInputSwitchStyle::GetResources(TArray<const FSlateBrush*>& OutBrushes) const
{
	// Add any brush resources here so that Slate can correctly atlas and reference them
}

