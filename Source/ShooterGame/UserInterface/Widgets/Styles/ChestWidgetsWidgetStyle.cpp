// Fill out your copyright notice in the Description page of Project Settings.

#include "ShooterGame.h"
#include "ChestWidgetsWidgetStyle.h"

FChestWidgetsStyle::FChestWidgetsStyle()
{
}

FChestWidgetsStyle::~FChestWidgetsStyle()
{
}

const FName FChestWidgetsStyle::TypeName(TEXT("FChestWidgetsStyle"));

const FChestWidgetsStyle& FChestWidgetsStyle::GetDefault()
{
	static FChestWidgetsStyle Default;
	return Default;
}

void FChestWidgetsStyle::GetResources(TArray<const FSlateBrush*>& OutBrushes) const
{
	// Add any brush resources here so that Slate can correctly atlas and reference them
}

