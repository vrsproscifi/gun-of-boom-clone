// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "Styling/SlateWidgetStyle.h"
#include "SlateWidgetStyleContainerBase.h"

#include "KilledContainerWidgetStyle.generated.h"

/**
 * 
 */
USTRUCT()
struct SHOOTERGAME_API FKilledContainerStyle : public FSlateWidgetStyle
{
	GENERATED_USTRUCT_BODY()

	UPROPERTY(Category = Appearance, EditAnywhere)		FTextBlockStyle		Font_KilledBy;
	UPROPERTY(Category = Appearance, EditAnywhere)		FTextBlockStyle		Font_Killer;

	UPROPERTY(Category = Appearance, EditAnywhere)		FTextBlockStyle		Font_ItemType;
	UPROPERTY(Category = Appearance, EditAnywhere)		FTextBlockStyle		Font_ItemTitle;

	UPROPERTY(Category = Appearance, EditAnywhere)		FSlateBrush			Image_WeaponBackground;
	UPROPERTY(Category = Appearance, EditAnywhere)		FSlateBrush			Image_NameBackground;
	UPROPERTY(Category = Appearance, EditAnywhere)		UCurveLinearColor*	CurveDamageColor;

	FKilledContainerStyle();
	virtual ~FKilledContainerStyle();

	// FSlateWidgetStyle
	virtual void GetResources(TArray<const FSlateBrush*>& OutBrushes) const override;
	static const FName TypeName;
	virtual const FName GetTypeName() const override { return TypeName; };
	static const FKilledContainerStyle& GetDefault();
};

/**
 */
UCLASS(hidecategories=Object, MinimalAPI)
class UKilledContainerWidgetStyle : public USlateWidgetStyleContainerBase
{
	GENERATED_BODY()

public:
	/** The actual data describing the widget appearance. */
	UPROPERTY(Category=Appearance, EditAnywhere, meta=(ShowOnlyInnerProperties))
	FKilledContainerStyle WidgetStyle;

	virtual const struct FSlateWidgetStyle* const GetStyle() const override
	{
		return static_cast< const struct FSlateWidgetStyle* >( &WidgetStyle );
	}
};
