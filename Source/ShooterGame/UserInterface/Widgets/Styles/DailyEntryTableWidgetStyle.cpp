// Fill out your copyright notice in the Description page of Project Settings.

#include "ShooterGame.h"
#include "DailyEntryTableWidgetStyle.h"

FDailyEntryTableStyle::FDailyEntryTableStyle()
{
}

FDailyEntryTableStyle::~FDailyEntryTableStyle()
{
}

const FName FDailyEntryTableStyle::TypeName(TEXT("FDailyEntryTableStyle"));

const FDailyEntryTableStyle& FDailyEntryTableStyle::GetDefault()
{
	static FDailyEntryTableStyle Default;
	return Default;
}

void FDailyEntryTableStyle::GetResources(TArray<const FSlateBrush*>& OutBrushes) const
{
	// Add any brush resources here so that Slate can correctly atlas and reference them
}

