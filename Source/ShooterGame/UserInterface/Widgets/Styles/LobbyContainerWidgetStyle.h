// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "Styling/SlateWidgetStyle.h"
#include "SlateWidgetStyleContainerBase.h"

#include "LobbyContainerWidgetStyle.generated.h"


USTRUCT()
struct SHOOTERGAME_API FLobbyContainerStyle_Chest
{
	GENERATED_USTRUCT_BODY()

	UPROPERTY(Category = Appearance, EditAnywhere)	FButtonStyle Button;
	UPROPERTY(Category = Appearance, EditAnywhere)	FTextBlockStyle Text;
	UPROPERTY(Category = Appearance, EditAnywhere)	FSlateBrush Brush;
};

USTRUCT()
struct SHOOTERGAME_API FLobbyContainerStyle : public FSlateWidgetStyle
{
	GENERATED_USTRUCT_BODY()

	UPROPERTY(Category = Appearance, EditAnywhere)	FButtonStyle ButtonIntoFight;
	UPROPERTY(Category = Appearance, EditAnywhere)	FButtonStyle ButtonIntoArsenal;
	UPROPERTY(Category = Appearance, EditAnywhere)	FButtonStyle ButtonCash;

	UPROPERTY(Category = Appearance, EditAnywhere)	FTextBlockStyle FontIntoFight;
	UPROPERTY(Category = Appearance, EditAnywhere)	FTextBlockStyle FontIntoArsenal;

	UPROPERTY(Category = Appearance, EditAnywhere)	FTextBlockStyle FontPlayerName;
	UPROPERTY(Category = Appearance, EditAnywhere)	FTextBlockStyle FontPlayerProgress;

	UPROPERTY(Category = Appearance, EditAnywhere)	FProgressBarStyle BarPlayerProgress;

	UPROPERTY(Category = Appearance, EditAnywhere)	FSlateBrush		CurrencyBackground;
	UPROPERTY(Category = Appearance, EditAnywhere)	FTextBlockStyle FontAwesome;
	UPROPERTY(Category = Appearance, EditAnywhere)	FTextBlockStyle FontAwesome2;

	UPROPERTY(Category = Appearance, EditAnywhere)	FSlateBrush			BonusBackground;
	UPROPERTY(Category = Appearance, EditAnywhere)	FTextBlockStyle		BonusBigNum;
	UPROPERTY(Category = Appearance, EditAnywhere)	FTextBlockStyle		BonusDesc;

	UPROPERTY(Category = Chest, EditAnywhere)		FLobbyContainerStyle_Chest	Chest;

	FLobbyContainerStyle();
	virtual ~FLobbyContainerStyle();

	// FSlateWidgetStyle
	virtual void GetResources(TArray<const FSlateBrush*>& OutBrushes) const override;
	static const FName TypeName;
	virtual const FName GetTypeName() const override { return TypeName; };
	static const FLobbyContainerStyle& GetDefault();
};

/**
 */
UCLASS(hidecategories=Object, MinimalAPI)
class ULobbyContainerWidgetStyle : public USlateWidgetStyleContainerBase
{
	GENERATED_BODY()

public:
	/** The actual data describing the widget appearance. */
	UPROPERTY(Category=Appearance, EditAnywhere, meta=(ShowOnlyInnerProperties))
	FLobbyContainerStyle WidgetStyle;

	virtual const struct FSlateWidgetStyle* const GetStyle() const override
	{
		return static_cast< const struct FSlateWidgetStyle* >( &WidgetStyle );
	}
};
