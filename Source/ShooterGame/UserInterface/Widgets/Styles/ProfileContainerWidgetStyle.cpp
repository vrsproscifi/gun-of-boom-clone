// Fill out your copyright notice in the Description page of Project Settings.

#include "ShooterGame.h"
#include "ProfileContainerWidgetStyle.h"


FProfileContainerStyle::FProfileContainerStyle()
{
}

FProfileContainerStyle::~FProfileContainerStyle()
{
}

const FName FProfileContainerStyle::TypeName(TEXT("FProfileContainerStyle"));

const FProfileContainerStyle& FProfileContainerStyle::GetDefault()
{
	static FProfileContainerStyle Default;
	return Default;
}

void FProfileContainerStyle::GetResources(TArray<const FSlateBrush*>& OutBrushes) const
{
	// Add any brush resources here so that Slate can correctly atlas and reference them
}

