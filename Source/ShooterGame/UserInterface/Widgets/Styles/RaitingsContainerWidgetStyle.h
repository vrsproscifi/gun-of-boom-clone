// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "Styling/SlateWidgetStyle.h"
#include "SlateWidgetStyleContainerBase.h"

#include "RaitingsContainerWidgetStyle.generated.h"

/**
 * 
 */
USTRUCT()
struct SHOOTERGAME_API FRaitingsContainerStyle : public FSlateWidgetStyle
{
	GENERATED_USTRUCT_BODY()

	UPROPERTY(Category = Appearance, EditAnywhere)		FTableRowStyle		TableRowStyle;
	UPROPERTY(Category = Appearance, EditAnywhere)		FTableRowStyle		TableRowStyleLocal;
	UPROPERTY(Category = Appearance, EditAnywhere)		FHeaderRowStyle		HeaderRowStyle;

	UPROPERTY(Category = Appearance, EditAnywhere)		FTextBlockStyle		HeaderTextStyle;
	UPROPERTY(Category = Appearance, EditAnywhere)		FTextBlockStyle		RowNumberTextStyle;
	UPROPERTY(Category = Appearance, EditAnywhere)		FTextBlockStyle		RowTextStyle;

	FRaitingsContainerStyle();
	virtual ~FRaitingsContainerStyle();

	// FSlateWidgetStyle
	virtual void GetResources(TArray<const FSlateBrush*>& OutBrushes) const override;
	static const FName TypeName;
	virtual const FName GetTypeName() const override { return TypeName; };
	static const FRaitingsContainerStyle& GetDefault();
};

/**
 */
UCLASS(hidecategories=Object, MinimalAPI)
class URaitingsContainerWidgetStyle : public USlateWidgetStyleContainerBase
{
	GENERATED_BODY()

public:
	/** The actual data describing the widget appearance. */
	UPROPERTY(Category=Appearance, EditAnywhere, meta=(ShowOnlyInnerProperties))
	FRaitingsContainerStyle WidgetStyle;

	virtual const struct FSlateWidgetStyle* const GetStyle() const override
	{
		return static_cast< const struct FSlateWidgetStyle* >( &WidgetStyle );
	}
};
