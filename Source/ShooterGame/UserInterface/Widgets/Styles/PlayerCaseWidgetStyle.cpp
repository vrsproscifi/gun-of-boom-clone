// Fill out your copyright notice in the Description page of Project Settings.

#include "ShooterGame.h"
#include "PlayerCaseWidgetStyle.h"


FPlayerCaseStyle::FPlayerCaseStyle()
{
}

FPlayerCaseStyle::~FPlayerCaseStyle()
{
}

const FName FPlayerCaseStyle::TypeName(TEXT("FPlayerCaseStyle"));

const FPlayerCaseStyle& FPlayerCaseStyle::GetDefault()
{
	static FPlayerCaseStyle Default;
	return Default;
}

void FPlayerCaseStyle::GetResources(TArray<const FSlateBrush*>& OutBrushes) const
{
	// Add any brush resources here so that Slate can correctly atlas and reference them
}

