// VRSPRO

#pragma once


#include "Styling/SlateWidgetStyle.h"
#include "SlateWidgetStyleContainerBase.h"
#include "TextBlockButtonWidgetStyle.h"
#include "MessageBoxWidgetStyle.generated.h"


USTRUCT()
struct FMessageBoxStyle_Buttons
{
	GENERATED_USTRUCT_BODY()

	UPROPERTY(EditAnywhere)
	FButtonStyle Button;

	UPROPERTY(EditAnywhere)
	FTextBlockStyle Text;
};

USTRUCT()
struct FMessageBoxStyle_Head
{
	GENERATED_USTRUCT_BODY()

	UPROPERTY(EditAnywhere)
	FSlateBrush Background;

	UPROPERTY(EditAnywhere)
	FTextBlockStyle Text;

	UPROPERTY(EditAnywhere)
	FTextBlockButtonStyle Close;
};

USTRUCT()
struct FMessageBoxStyle_Separator
{
	GENERATED_USTRUCT_BODY()

	UPROPERTY(EditAnywhere)
	FSlateBrush SeparatorImage;
		
	UPROPERTY(EditAnywhere)
	TEnumAsByte<EOrientation> Orientation;
	
	UPROPERTY(EditAnywhere)
	float Thickness;

	UPROPERTY(EditAnywhere)
	FLinearColor ColorAndOpacity;
};

USTRUCT()
struct FMessageBoxStyle_Box
{
	GENERATED_USTRUCT_BODY()

	UPROPERTY(EditAnywhere)
	FSlateBrush Background;

	UPROPERTY(EditAnywhere)
	FSlateBrush Border;

	UPROPERTY(EditAnywhere)
	FSlateBrush Shadow;

	UPROPERTY(EditAnywhere)
	FVector2D MinSize;

	UPROPERTY(EditAnywhere)
	FVector2D MaxSize;

	UPROPERTY(EditAnywhere)
	float Duration;
};

USTRUCT()
struct FMessageBoxStyle_Content
{
	GENERATED_USTRUCT_BODY()

	UPROPERTY(EditAnywhere)
	FTextBlockStyle Text;

	UPROPERTY(EditAnywhere)
	FEditableTextBoxStyle Input;
};

USTRUCT()
struct FMessageBoxStyle_Back
{
	GENERATED_USTRUCT_BODY()

	UPROPERTY(EditAnywhere)
	FLinearColor Color;

	UPROPERTY(EditAnywhere)
	float Duration;

	UPROPERTY(EditAnywhere)
	FTextBlockStyle CloseFont;
};

USTRUCT()
struct FMessageBoxStyle : public FSlateWidgetStyle
{
	GENERATED_USTRUCT_BODY()

	UPROPERTY(Category = Appearance, EditAnywhere)
	FMessageBoxStyle_Back Back;

	UPROPERTY(Category = Appearance, EditAnywhere)
	FMessageBoxStyle_Box Box;

	UPROPERTY(Category = Appearance, EditAnywhere)
	FMessageBoxStyle_Head Head;

	UPROPERTY(Category = Appearance, EditAnywhere)
	FMessageBoxStyle_Buttons Buttons;

	UPROPERTY(Category = Appearance, EditAnywhere)
	FMessageBoxStyle_Content Content;

	UPROPERTY(Category = Appearance, EditAnywhere)
	FMessageBoxStyle_Separator Separator;

	UPROPERTY(Category = Appearance, EditAnywhere)
	FSlateSound ShowSound;

	UPROPERTY(Category = Appearance, EditAnywhere)
	FSlateSound HideSound;

	FMessageBoxStyle();
	virtual ~FMessageBoxStyle();

	// FSlateWidgetStyle
	virtual void GetResources(TArray<const FSlateBrush*>& OutBrushes) const override;
	static const FName TypeName;
	virtual const FName GetTypeName() const override { return TypeName; };
	static const FMessageBoxStyle& GetDefault();
};

/**
 */
UCLASS(hidecategories=Object, MinimalAPI)
class UMessageBoxWidgetStyle : public USlateWidgetStyleContainerBase
{
	GENERATED_BODY()

public:
	/** The actual data describing the widget appearance. */
	UPROPERTY(Category=Appearance, EditAnywhere, meta=(ShowOnlyInnerProperties))
	FMessageBoxStyle WidgetStyle;

	virtual const struct FSlateWidgetStyle* const GetStyle() const override
	{
		return static_cast< const struct FSlateWidgetStyle* >( &WidgetStyle );
	}
};
