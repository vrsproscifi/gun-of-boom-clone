// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "Styling/SlateWidgetStyle.h"
#include "SlateWidgetStyleContainerBase.h"

#include "PlayerCaseWidgetStyle.generated.h"

/**
 * 
 */
USTRUCT()
struct SHOOTERGAME_API FPlayerCaseStyle : public FSlateWidgetStyle
{
	GENERATED_USTRUCT_BODY()

	UPROPERTY(Category = Appearance, EditAnywhere)	FButtonStyle Button;
	UPROPERTY(Category = Appearance, EditAnywhere)	FSlateBrush Background;
	UPROPERTY(Category = Appearance, EditAnywhere)	FSlateBrush BackgroundUp;
	UPROPERTY(Category = Appearance, EditAnywhere)	FSlateBrush BackgroundUpGetFree;
	UPROPERTY(Category = Appearance, EditAnywhere)	FSlateBrush BackgroundDown;
	UPROPERTY(Category = Appearance, EditAnywhere)	FTextBlockStyle TextGetFree;
	UPROPERTY(Category = Appearance, EditAnywhere)	FTextBlockStyle TextTime;
	UPROPERTY(Category = Appearance, EditAnywhere)	FTextBlockStyle TextTitle;
	UPROPERTY(Category = Configuration, EditAnywhere)	FVector2D Height;

	FPlayerCaseStyle();
	virtual ~FPlayerCaseStyle();

	// FSlateWidgetStyle
	virtual void GetResources(TArray<const FSlateBrush*>& OutBrushes) const override;
	static const FName TypeName;
	virtual const FName GetTypeName() const override { return TypeName; };
	static const FPlayerCaseStyle& GetDefault();
};

/**
 */
UCLASS(hidecategories=Object, MinimalAPI)
class UPlayerCaseWidgetStyle : public USlateWidgetStyleContainerBase
{
	GENERATED_BODY()

public:
	/** The actual data describing the widget appearance. */
	UPROPERTY(Category=Appearance, EditAnywhere, meta=(ShowOnlyInnerProperties))
	FPlayerCaseStyle WidgetStyle;

	virtual const struct FSlateWidgetStyle* const GetStyle() const override
	{
		return static_cast< const struct FSlateWidgetStyle* >( &WidgetStyle );
	}
};
