// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "Styling/SlateWidgetStyle.h"
#include "SlateWidgetStyleContainerBase.h"
#include "SphereProgressWidgetStyle.h"
#include "TouchInputButtonWidgetStyle.generated.h"


/**
 * 
 */
USTRUCT()
struct SHOOTERGAME_API FTouchInputButtonStyle : public FSlateWidgetStyle
{
	GENERATED_USTRUCT_BODY()

	UPROPERTY(Category = Appearance, EditAnywhere) FSlateBrush ButtonNormal;
	UPROPERTY(Category = Appearance, EditAnywhere) FSlateBrush ButtonActivated;

	UPROPERTY(Category = Appearance, EditAnywhere) FSlateBrush IconNormal;
	UPROPERTY(Category = Appearance, EditAnywhere) FSlateBrush IconActivated;

	UPROPERTY(Category = Appearance, EditAnywhere) FSlateBrush AmountBrush;
	UPROPERTY(Category = Appearance, EditAnywhere) FTextBlockStyle AmountText;

	UPROPERTY(Category = Appearance, EditAnywhere) float AmountDegress;
	UPROPERTY(Category = Appearance, EditAnywhere) float AmountRadius;

	UPROPERTY(Category = Appearance, EditAnywhere) FSphereProgressStyle ProgressStyle;
	UPROPERTY(Category = Appearance, EditAnywhere) float ProgressRotation;

	FTouchInputButtonStyle();
	virtual ~FTouchInputButtonStyle();

	// FSlateWidgetStyle
	virtual void GetResources(TArray<const FSlateBrush*>& OutBrushes) const override;
	static const FName TypeName;
	virtual const FName GetTypeName() const override { return TypeName; };
	static const FTouchInputButtonStyle& GetDefault();
};

/**
 */
UCLASS(hidecategories=Object, MinimalAPI)
class UTouchInputButtonWidgetStyle : public USlateWidgetStyleContainerBase
{
	GENERATED_BODY()

public:
	/** The actual data describing the widget appearance. */
	UPROPERTY(Category=Appearance, EditAnywhere, meta=(ShowOnlyInnerProperties))
	FTouchInputButtonStyle WidgetStyle;

	virtual const struct FSlateWidgetStyle* const GetStyle() const override
	{
		return static_cast< const struct FSlateWidgetStyle* >( &WidgetStyle );
	}
};
