// VRSPRO

#include "ShooterGame.h"
#include "AttachAccountServiceWidgetStyle.h"


FAttachAccountServiceStyle::FAttachAccountServiceStyle()
{
}

FAttachAccountServiceStyle::~FAttachAccountServiceStyle()
{
}

const FName FAttachAccountServiceStyle::TypeName(TEXT("FAttachAccountServiceStyle"));

const FAttachAccountServiceStyle& FAttachAccountServiceStyle::GetDefault()
{
	static FAttachAccountServiceStyle Default;
	return Default;
}

void FAttachAccountServiceStyle::GetResources(TArray<const FSlateBrush*>& OutBrushes) const
{
	// Add any brush resources here so that Slate can correctly atlas and reference them
}

