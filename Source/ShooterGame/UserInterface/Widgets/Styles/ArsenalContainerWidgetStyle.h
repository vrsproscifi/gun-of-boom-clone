// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "Styling/SlateWidgetStyle.h"
#include "SlateWidgetStyleContainerBase.h"

#include "ArsenalContainerWidgetStyle.generated.h"

/**
 * 
 */
USTRUCT()
struct SHOOTERGAME_API FArsenalContainerStyle : public FSlateWidgetStyle
{
	GENERATED_USTRUCT_BODY()

	UPROPERTY(Category = Appearance, EditAnywhere)	FTextBlockStyle BigParameterName;
	UPROPERTY(Category = Appearance, EditAnywhere)	FTextBlockStyle BigParameterValue;

	UPROPERTY(Category = Appearance, EditAnywhere)	FTextBlockStyle ParameterName;
	UPROPERTY(Category = Appearance, EditAnywhere)	FTextBlockStyle ParameterValue;

	UPROPERTY(Category = Appearance, EditAnywhere)	FTextBlockStyle ParameterUpgrade;

	UPROPERTY(Category = Appearance, EditAnywhere)	FSlateBrush		CatWeapons;
	UPROPERTY(Category = Appearance, EditAnywhere)	FSlateBrush		CatDefend;
	UPROPERTY(Category = Appearance, EditAnywhere)	FSlateBrush		CatSupport;

	UPROPERTY(Category = Appearance, EditAnywhere)	FSlateBrush		DisallowItem;
	UPROPERTY(Category = Appearance, EditAnywhere)	FTextBlockStyle DisallowItemText;

	FArsenalContainerStyle();
	virtual ~FArsenalContainerStyle();

	// FSlateWidgetStyle
	virtual void GetResources(TArray<const FSlateBrush*>& OutBrushes) const override;
	static const FName TypeName;
	virtual const FName GetTypeName() const override { return TypeName; };
	static const FArsenalContainerStyle& GetDefault();
};

/**
 */
UCLASS(hidecategories=Object, MinimalAPI)
class UArsenalContainerWidgetStyle : public USlateWidgetStyleContainerBase
{
	GENERATED_BODY()

public:
	/** The actual data describing the widget appearance. */
	UPROPERTY(Category=Appearance, EditAnywhere, meta=(ShowOnlyInnerProperties))
	FArsenalContainerStyle WidgetStyle;

	virtual const struct FSlateWidgetStyle* const GetStyle() const override
	{
		return static_cast< const struct FSlateWidgetStyle* >( &WidgetStyle );
	}
};
