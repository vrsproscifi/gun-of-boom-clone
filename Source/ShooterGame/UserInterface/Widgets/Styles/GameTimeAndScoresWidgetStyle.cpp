// Fill out your copyright notice in the Description page of Project Settings.

#include "ShooterGame.h"
#include "GameTimeAndScoresWidgetStyle.h"


FGameTimeAndScoresStyle::FGameTimeAndScoresStyle()
{
}

FGameTimeAndScoresStyle::~FGameTimeAndScoresStyle()
{
}

const FName FGameTimeAndScoresStyle::TypeName(TEXT("FGameTimeAndScoresStyle"));

const FGameTimeAndScoresStyle& FGameTimeAndScoresStyle::GetDefault()
{
	static FGameTimeAndScoresStyle Default;
	return Default;
}

void FGameTimeAndScoresStyle::GetResources(TArray<const FSlateBrush*>& OutBrushes) const
{
	// Add any brush resources here so that Slate can correctly atlas and reference them
}

