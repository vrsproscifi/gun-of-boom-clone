// Fill out your copyright notice in the Description page of Project Settings.

#include "ShooterGame.h"
#include "ShopItemWidgetStyle.h"


FShopItemStyle::FShopItemStyle()
{
	FontScaleActive = 1.0f;
	FontScaleInactive = 1.0f;
}

FShopItemStyle::~FShopItemStyle()
{
}

const FName FShopItemStyle::TypeName(TEXT("FShopItemStyle"));

const FShopItemStyle& FShopItemStyle::GetDefault()
{
	static FShopItemStyle Default;
	return Default;
}

void FShopItemStyle::GetResources(TArray<const FSlateBrush*>& OutBrushes) const
{
	// Add any brush resources here so that Slate can correctly atlas and reference them
}

