// Fill out your copyright notice in the Description page of Project Settings.

#include "ShooterGame.h"
#include "LobbyContainerWidgetStyle.h"


FLobbyContainerStyle::FLobbyContainerStyle()
{
}

FLobbyContainerStyle::~FLobbyContainerStyle()
{
}

const FName FLobbyContainerStyle::TypeName(TEXT("FLobbyContainerStyle"));

const FLobbyContainerStyle& FLobbyContainerStyle::GetDefault()
{
	static FLobbyContainerStyle Default;
	return Default;
}

void FLobbyContainerStyle::GetResources(TArray<const FSlateBrush*>& OutBrushes) const
{
	// Add any brush resources here so that Slate can correctly atlas and reference them
}

