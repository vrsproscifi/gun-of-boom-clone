// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "Styling/SlateWidgetStyle.h"
#include "SlateWidgetStyleContainerBase.h"

#include "ChestWidgetsWidgetStyle.generated.h"


USTRUCT()
struct FChestWidgetsStyle_Item
{
	GENERATED_USTRUCT_BODY()

	UPROPERTY(Category = Appearance, EditAnywhere)	FSlateBrush			Header;
	UPROPERTY(Category = Appearance, EditAnywhere)	FSlateBrush			Content;
	UPROPERTY(Category = Appearance, EditAnywhere)	FTextBlockStyle		HeaderFont;
	UPROPERTY(Category = Appearance, EditAnywhere)	FTextBlockStyle		ContentFont;
};


USTRUCT()
struct SHOOTERGAME_API FChestWidgetsStyle : public FSlateWidgetStyle
{
	GENERATED_USTRUCT_BODY()

	UPROPERTY(Category = Appearance, EditAnywhere)		FText						DescText;
	UPROPERTY(Category = Appearance, EditAnywhere)		FTextBlockStyle				DescFont;

	UPROPERTY(Category = Appearance, EditAnywhere)		FButtonStyle				ButtonItem;
	UPROPERTY(Category = Appearance, EditAnywhere)		FChestWidgetsStyle_Item		AllowItem;
	UPROPERTY(Category = Appearance, EditAnywhere)		FChestWidgetsStyle_Item		DisallowItem;

	FChestWidgetsStyle();
	virtual ~FChestWidgetsStyle();

	// FSlateWidgetStyle
	virtual void GetResources(TArray<const FSlateBrush*>& OutBrushes) const override;
	static const FName TypeName;
	virtual const FName GetTypeName() const override { return TypeName; };
	static const FChestWidgetsStyle& GetDefault();
};

/**
 */
UCLASS(hidecategories=Object, MinimalAPI)
class UChestWidgetsWidgetStyle : public USlateWidgetStyleContainerBase
{
	GENERATED_BODY()

public:
	/** The actual data describing the widget appearance. */
	UPROPERTY(Category=Appearance, EditAnywhere, meta=(ShowOnlyInnerProperties))
	FChestWidgetsStyle WidgetStyle;

	virtual const struct FSlateWidgetStyle* const GetStyle() const override
	{
		return static_cast< const struct FSlateWidgetStyle* >( &WidgetStyle );
	}
};
