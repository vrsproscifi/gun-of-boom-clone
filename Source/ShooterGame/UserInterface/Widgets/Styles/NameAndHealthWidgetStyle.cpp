// Fill out your copyright notice in the Description page of Project Settings.

#include "ShooterGame.h"
#include "NameAndHealthWidgetStyle.h"


FNameAndHealthStyle::FNameAndHealthStyle()
{
}

FNameAndHealthStyle::~FNameAndHealthStyle()
{
}

const FName FNameAndHealthStyle::TypeName(TEXT("FNameAndHealthStyle"));

const FNameAndHealthStyle& FNameAndHealthStyle::GetDefault()
{
	static FNameAndHealthStyle Default;
	return Default;
}

void FNameAndHealthStyle::GetResources(TArray<const FSlateBrush*>& OutBrushes) const
{
	// Add any brush resources here so that Slate can correctly atlas and reference them
}

