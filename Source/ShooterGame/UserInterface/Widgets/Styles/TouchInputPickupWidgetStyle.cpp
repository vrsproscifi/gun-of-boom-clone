// Fill out your copyright notice in the Description page of Project Settings.

#include "ShooterGame.h"
#include "TouchInputPickupWidgetStyle.h"


FTouchInputPickupStyle::FTouchInputPickupStyle()
{
}

FTouchInputPickupStyle::~FTouchInputPickupStyle()
{
}

const FName FTouchInputPickupStyle::TypeName(TEXT("FTouchInputPickupStyle"));

const FTouchInputPickupStyle& FTouchInputPickupStyle::GetDefault()
{
	static FTouchInputPickupStyle Default;
	return Default;
}

void FTouchInputPickupStyle::GetResources(TArray<const FSlateBrush*>& OutBrushes) const
{
	// Add any brush resources here so that Slate can correctly atlas and reference them
}

