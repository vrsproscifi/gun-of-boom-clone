// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "Styling/SlateWidgetStyle.h"
#include "SlateWidgetStyleContainerBase.h"

#include "DiscountBlockWidgetStyle.generated.h"

/**
 * 
 */
USTRUCT()
struct SHOOTERGAME_API FDiscountBlockStyle : public FSlateWidgetStyle
{
	GENERATED_USTRUCT_BODY()

	UPROPERTY(Category = Appearance, EditAnywhere)	FSlateBrush			Background;
	UPROPERTY(Category = Appearance, EditAnywhere)	FTextBlockStyle		Title;

	FDiscountBlockStyle();
	virtual ~FDiscountBlockStyle();

	// FSlateWidgetStyle
	virtual void GetResources(TArray<const FSlateBrush*>& OutBrushes) const override;
	static const FName TypeName;
	virtual const FName GetTypeName() const override { return TypeName; };
	static const FDiscountBlockStyle& GetDefault();
};

/**
 */
UCLASS(hidecategories=Object, MinimalAPI)
class UDiscountBlockWidgetStyle : public USlateWidgetStyleContainerBase
{
	GENERATED_BODY()

public:
	/** The actual data describing the widget appearance. */
	UPROPERTY(Category=Appearance, EditAnywhere, meta=(ShowOnlyInnerProperties))
	FDiscountBlockStyle WidgetStyle;

	virtual const struct FSlateWidgetStyle* const GetStyle() const override
	{
		return static_cast< const struct FSlateWidgetStyle* >( &WidgetStyle );
	}
};
