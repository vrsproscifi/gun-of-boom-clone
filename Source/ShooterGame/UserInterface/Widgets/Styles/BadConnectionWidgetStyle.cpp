// Fill out your copyright notice in the Description page of Project Settings.

#include "ShooterGame.h"
#include "BadConnectionWidgetStyle.h"


FBadConnectionStyle::FBadConnectionStyle()
{
}

FBadConnectionStyle::~FBadConnectionStyle()
{
}

const FName FBadConnectionStyle::TypeName(TEXT("FBadConnectionStyle"));

const FBadConnectionStyle& FBadConnectionStyle::GetDefault()
{
	static FBadConnectionStyle Default;
	return Default;
}

void FBadConnectionStyle::GetResources(TArray<const FSlateBrush*>& OutBrushes) const
{
	// Add any brush resources here so that Slate can correctly atlas and reference them
}

