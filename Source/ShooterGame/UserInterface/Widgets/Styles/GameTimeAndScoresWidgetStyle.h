// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "Styling/SlateWidgetStyle.h"
#include "SlateWidgetStyleContainerBase.h"

#include "GameTimeAndScoresWidgetStyle.generated.h"

/**
 * 
 */
USTRUCT()
struct SHOOTERGAME_API FGameTimeAndScoresStyle : public FSlateWidgetStyle
{
	GENERATED_USTRUCT_BODY()

	UPROPERTY(Category = Appearance, EditAnywhere)	FProgressBarStyle	FriendlyProgressBar;
	UPROPERTY(Category = Appearance, EditAnywhere)	FProgressBarStyle	EnemyProgressBar;
	UPROPERTY(Category = Appearance, EditAnywhere)	FProgressBarStyle	TimeProgressBar;

	UPROPERTY(Category = Appearance, EditAnywhere)	FTextBlockStyle		FriendlyProgressText;
	UPROPERTY(Category = Appearance, EditAnywhere)	FTextBlockStyle		EnemyProgressText;
	UPROPERTY(Category = Appearance, EditAnywhere)	FTextBlockStyle		TimeProgressText;

	FGameTimeAndScoresStyle();
	virtual ~FGameTimeAndScoresStyle();

	// FSlateWidgetStyle
	virtual void GetResources(TArray<const FSlateBrush*>& OutBrushes) const override;
	static const FName TypeName;
	virtual const FName GetTypeName() const override { return TypeName; };
	static const FGameTimeAndScoresStyle& GetDefault();
};

/**
 */
UCLASS(hidecategories=Object, MinimalAPI)
class UGameTimeAndScoresWidgetStyle : public USlateWidgetStyleContainerBase
{
	GENERATED_BODY()

public:
	/** The actual data describing the widget appearance. */
	UPROPERTY(Category=Appearance, EditAnywhere, meta=(ShowOnlyInnerProperties))
	FGameTimeAndScoresStyle WidgetStyle;

	virtual const struct FSlateWidgetStyle* const GetStyle() const override
	{
		return static_cast< const struct FSlateWidgetStyle* >( &WidgetStyle );
	}
};
