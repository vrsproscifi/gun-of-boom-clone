// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "Styling/SlateWidgetStyle.h"
#include "SlateWidgetStyleContainerBase.h"

#include "SocialButtonWidgetStyle.generated.h"

USTRUCT()
struct SHOOTERGAME_API FSocialButtonStyle_Button
{
	GENERATED_USTRUCT_BODY()

	UPROPERTY(Category = Appearance, EditAnywhere)		FString			Glyph;
	UPROPERTY(Category = Appearance, EditAnywhere)		FSlateColor		Normal;
	UPROPERTY(Category = Appearance, EditAnywhere)		FSlateColor		Hovered;
	UPROPERTY(Category = Appearance, EditAnywhere)		FSlateColor		Pressed;
};

USTRUCT()
struct SHOOTERGAME_API FSocialButtonStyle : public FSlateWidgetStyle
{
	GENERATED_USTRUCT_BODY()

	UPROPERTY(Category = Appearance, EditAnywhere)		FTextBlockStyle		FontAwesome;
	UPROPERTY(Category = Appearance, EditAnywhere)		FTextBlockStyle		FontText;
	UPROPERTY(Category = Appearance, EditAnywhere)		FSlateBrush			Background;
	UPROPERTY(Category = Appearance, EditAnywhere)		FSlateBrush			SeparatorBrush;
	UPROPERTY(Category = Appearance, EditAnywhere)		float				SeparatorThickness;

	UPROPERTY(Category = Appearance, EditAnywhere)		TMap<FName, FSocialButtonStyle_Button>	Buttons;

	FSocialButtonStyle();
	virtual ~FSocialButtonStyle();

	// FSlateWidgetStyle
	virtual void GetResources(TArray<const FSlateBrush*>& OutBrushes) const override;
	static const FName TypeName;
	virtual const FName GetTypeName() const override { return TypeName; };
	static const FSocialButtonStyle& GetDefault();
};

/**
 */
UCLASS(hidecategories=Object, MinimalAPI)
class USocialButtonWidgetStyle : public USlateWidgetStyleContainerBase
{
	GENERATED_BODY()

public:
	/** The actual data describing the widget appearance. */
	UPROPERTY(Category=Appearance, EditAnywhere, meta=(ShowOnlyInnerProperties))
	FSocialButtonStyle WidgetStyle;

	virtual const struct FSlateWidgetStyle* const GetStyle() const override
	{
		return static_cast< const struct FSlateWidgetStyle* >( &WidgetStyle );
	}
};
