// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "Styling/SlateWidgetStyle.h"
#include "SlateWidgetStyleContainerBase.h"

#include "PlayerNameAndProgressWidgetStyle.generated.h"

/**
 * 
 */
USTRUCT()
struct SHOOTERGAME_API FPlayerNameAndProgressStyle : public FSlateWidgetStyle
{
	GENERATED_USTRUCT_BODY()

	UPROPERTY(Category = Appearance, EditAnywhere)		FTextBlockStyle		NameNormal;
	UPROPERTY(Category = Appearance, EditAnywhere)		FTextBlockStyle		NamePremium;

	UPROPERTY(Category = Appearance, EditAnywhere)		FTextBlockStyle		ProgressNumbers;
	UPROPERTY(Category = Appearance, EditAnywhere)		FProgressBarStyle	ProgressBar;

	UPROPERTY(Category = Appearance, EditAnywhere)		FSlateBrush			ELOBrush;

	UPROPERTY(Category = Avatar, EditAnywhere)			UMaterialInterface*	Material;
	UPROPERTY(Category = Avatar, EditAnywhere)			FName				ParameterTexture;
	UPROPERTY(Category = Avatar, EditAnywhere)			FName				ParameterProgress;
	UPROPERTY(Category = Avatar, EditAnywhere)			FName				ParameterFlag;
	UPROPERTY(Category = Avatar, EditAnywhere)			FName				ParameterFlagSubImages;
	UPROPERTY(Category = Avatar, EditAnywhere)			FName				ParameterFlagTexture;

	FPlayerNameAndProgressStyle();
	virtual ~FPlayerNameAndProgressStyle();

	// FSlateWidgetStyle
	virtual void GetResources(TArray<const FSlateBrush*>& OutBrushes) const override;
	static const FName TypeName;
	virtual const FName GetTypeName() const override { return TypeName; };
	static const FPlayerNameAndProgressStyle& GetDefault();
};

/**
 */
UCLASS(hidecategories=Object, MinimalAPI)
class UPlayerNameAndProgressWidgetStyle : public USlateWidgetStyleContainerBase
{
	GENERATED_BODY()

public:
	/** The actual data describing the widget appearance. */
	UPROPERTY(Category=Appearance, EditAnywhere, meta=(ShowOnlyInnerProperties))
	FPlayerNameAndProgressStyle WidgetStyle;

	virtual const struct FSlateWidgetStyle* const GetStyle() const override
	{
		return static_cast< const struct FSlateWidgetStyle* >( &WidgetStyle );
	}
};
