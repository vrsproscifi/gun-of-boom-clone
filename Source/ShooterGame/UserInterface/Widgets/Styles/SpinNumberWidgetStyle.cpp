// Fill out your copyright notice in the Description page of Project Settings.

#include "ShooterGame.h"
#include "SpinNumberWidgetStyle.h"


FSpinNumberStyle::FSpinNumberStyle()
{
}

FSpinNumberStyle::~FSpinNumberStyle()
{
}

const FName FSpinNumberStyle::TypeName(TEXT("FSpinNumberStyle"));

const FSpinNumberStyle& FSpinNumberStyle::GetDefault()
{
	static FSpinNumberStyle Default;
	return Default;
}

void FSpinNumberStyle::GetResources(TArray<const FSlateBrush*>& OutBrushes) const
{
	// Add any brush resources here so that Slate can correctly atlas and reference them
}

