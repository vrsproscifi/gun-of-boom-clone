// Fill out your copyright notice in the Description page of Project Settings.

#include "ShooterGame.h"
#include "TouchInputButtonWidgetStyle.h"


FTouchInputButtonStyle::FTouchInputButtonStyle()
{
}

FTouchInputButtonStyle::~FTouchInputButtonStyle()
{
}

const FName FTouchInputButtonStyle::TypeName(TEXT("FTouchInputButtonStyle"));

const FTouchInputButtonStyle& FTouchInputButtonStyle::GetDefault()
{
	static FTouchInputButtonStyle Default;
	return Default;
}

void FTouchInputButtonStyle::GetResources(TArray<const FSlateBrush*>& OutBrushes) const
{
	// Add any brush resources here so that Slate can correctly atlas and reference them
}

