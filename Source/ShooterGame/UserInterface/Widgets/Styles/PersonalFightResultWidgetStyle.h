// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "Styling/SlateWidgetStyle.h"
#include "SlateWidgetStyleContainerBase.h"

#include "PersonalFightResultWidgetStyle.generated.h"

/**
 * 
 */
USTRUCT()
struct SHOOTERGAME_API FPersonalFightResultStyle : public FSlateWidgetStyle
{
	GENERATED_USTRUCT_BODY()

	UPROPERTY(Category = Appearance, EditAnywhere)	FTextBlockStyle StatusText;

	UPROPERTY(Category = Appearance, EditAnywhere)	FTextBlockStyle ButtonText;
	UPROPERTY(Category = Appearance, EditAnywhere)	FButtonStyle	ButtonStyle;

	UPROPERTY(Category = Appearance, EditAnywhere)	FTableRowStyle	TableRow;
	UPROPERTY(Category = Appearance, EditAnywhere)	FTextBlockStyle ColumnAmount;
	UPROPERTY(Category = Appearance, EditAnywhere)	FTextBlockStyle ColumnName;

	UPROPERTY(Category = Appearance, EditAnywhere)	FSlateBrush		RewardBG;
	UPROPERTY(Category = Appearance, EditAnywhere)	FSlateBrush		Days7BG;

	UPROPERTY(Category = Appearance, EditAnywhere)	FTextBlockStyle RewardText;
	UPROPERTY(Category = Appearance, EditAnywhere)	FTextBlockStyle Days7Text;

	UPROPERTY(Category = Configuration, EditAnywhere)	TArray<int32>	PremiumModels;

	UPROPERTY(Category = Animation, EditAnywhere)		float			NumbersDuration;
	UPROPERTY(Category = Animation, EditAnywhere)		UCurveFloat*	NumbersLerpCurve;
	UPROPERTY(Category = Animation, EditAnywhere)		UCurveFloat*	NumbersScaleCurve;

	UPROPERTY(Category = Animation, EditAnywhere)		float			MultiplerDuration;
	UPROPERTY(Category = Animation, EditAnywhere)		UCurveFloat*	MultiplerScaleCurve;

	UPROPERTY(Category = Animation, EditAnywhere)		float			OtherDuration;
	UPROPERTY(Category = Animation, EditAnywhere)		UCurveVector*	OtherCurve;

	FPersonalFightResultStyle();
	virtual ~FPersonalFightResultStyle();

	// FSlateWidgetStyle
	virtual void GetResources(TArray<const FSlateBrush*>& OutBrushes) const override;
	static const FName TypeName;
	virtual const FName GetTypeName() const override { return TypeName; };
	static const FPersonalFightResultStyle& GetDefault();
};

/**
 */
UCLASS(hidecategories=Object, MinimalAPI)
class UPersonalFightResultWidgetStyle : public USlateWidgetStyleContainerBase
{
	GENERATED_BODY()

public:
	/** The actual data describing the widget appearance. */
	UPROPERTY(Category=Appearance, EditAnywhere, meta=(ShowOnlyInnerProperties))
	FPersonalFightResultStyle WidgetStyle;

	virtual const struct FSlateWidgetStyle* const GetStyle() const override
	{
		return static_cast< const struct FSlateWidgetStyle* >( &WidgetStyle );
	}
};
