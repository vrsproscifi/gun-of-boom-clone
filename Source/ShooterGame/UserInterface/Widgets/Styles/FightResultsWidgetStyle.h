// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "Styling/SlateWidgetStyle.h"
#include "SlateWidgetStyleContainerBase.h"

#include "FightResultsWidgetStyle.generated.h"

/**
 * 
 */
USTRUCT()
struct SHOOTERGAME_API FFightResultsStyle : public FSlateWidgetStyle
{
	GENERATED_USTRUCT_BODY()

	UPROPERTY(Category = Appearance, EditAnywhere)		FButtonStyle ButtonContinue;
	UPROPERTY(Category = Appearance, EditAnywhere)		FTextBlockStyle TextContinue;

	UPROPERTY(Category = Appearance, EditAnywhere)		FTextBlockStyle WinStatusText;
	UPROPERTY(Category = Appearance, EditAnywhere)		FTextBlockStyle	RowName;
	UPROPERTY(Category = Appearance, EditAnywhere)		FTextBlockStyle	RowNumber;
	UPROPERTY(Category = Appearance, EditAnywhere)		FTextBlockStyle	RowHeaderText;

	UPROPERTY(Category = Appearance, EditAnywhere)		FHeaderRowStyle RowHeader;
	UPROPERTY(Category = Appearance, EditAnywhere)		FTableRowStyle	RowTable;
	UPROPERTY(Category = Appearance, EditAnywhere)		FTableRowStyle	RowTableLocal;
	UPROPERTY(Category = Appearance, EditAnywhere)		FTextBlockStyle ScoreText;

	UPROPERTY(Category = Sound, EditAnywhere)			FSlateSound SoundWin;
	UPROPERTY(Category = Sound, EditAnywhere)			FSlateSound SoundLose;

	FFightResultsStyle();
	virtual ~FFightResultsStyle();

	// FSlateWidgetStyle
	virtual void GetResources(TArray<const FSlateBrush*>& OutBrushes) const override;
	static const FName TypeName;
	virtual const FName GetTypeName() const override { return TypeName; };
	static const FFightResultsStyle& GetDefault();
};

/**
 */
UCLASS(hidecategories=Object, MinimalAPI)
class UFightResultsWidgetStyle : public USlateWidgetStyleContainerBase
{
	GENERATED_BODY()

public:
	/** The actual data describing the widget appearance. */
	UPROPERTY(Category=Appearance, EditAnywhere, meta=(ShowOnlyInnerProperties))
	FFightResultsStyle WidgetStyle;

	virtual const struct FSlateWidgetStyle* const GetStyle() const override
	{
		return static_cast< const struct FSlateWidgetStyle* >( &WidgetStyle );
	}
};
