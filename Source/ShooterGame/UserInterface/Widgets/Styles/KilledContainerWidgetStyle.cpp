// Fill out your copyright notice in the Description page of Project Settings.

#include "ShooterGame.h"
#include "KilledContainerWidgetStyle.h"


FKilledContainerStyle::FKilledContainerStyle()
{
}

FKilledContainerStyle::~FKilledContainerStyle()
{
}

const FName FKilledContainerStyle::TypeName(TEXT("FKilledContainerStyle"));

const FKilledContainerStyle& FKilledContainerStyle::GetDefault()
{
	static FKilledContainerStyle Default;
	return Default;
}

void FKilledContainerStyle::GetResources(TArray<const FSlateBrush*>& OutBrushes) const
{
	// Add any brush resources here so that Slate can correctly atlas and reference them
}

