// Fill out your copyright notice in the Description page of Project Settings.

#include "ShooterGame.h"
#include "PlayerNameAndProgressWidgetStyle.h"


FPlayerNameAndProgressStyle::FPlayerNameAndProgressStyle()
{
}

FPlayerNameAndProgressStyle::~FPlayerNameAndProgressStyle()
{
}

const FName FPlayerNameAndProgressStyle::TypeName(TEXT("FPlayerNameAndProgressStyle"));

const FPlayerNameAndProgressStyle& FPlayerNameAndProgressStyle::GetDefault()
{
	static FPlayerNameAndProgressStyle Default;
	return Default;
}

void FPlayerNameAndProgressStyle::GetResources(TArray<const FSlateBrush*>& OutBrushes) const
{
	// Add any brush resources here so that Slate can correctly atlas and reference them
}

