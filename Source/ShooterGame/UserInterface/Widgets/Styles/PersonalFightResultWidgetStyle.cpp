// Fill out your copyright notice in the Description page of Project Settings.

#include "ShooterGame.h"
#include "PersonalFightResultWidgetStyle.h"


FPersonalFightResultStyle::FPersonalFightResultStyle()
{
}

FPersonalFightResultStyle::~FPersonalFightResultStyle()
{
}

const FName FPersonalFightResultStyle::TypeName(TEXT("FPersonalFightResultStyle"));

const FPersonalFightResultStyle& FPersonalFightResultStyle::GetDefault()
{
	static FPersonalFightResultStyle Default;
	return Default;
}

void FPersonalFightResultStyle::GetResources(TArray<const FSlateBrush*>& OutBrushes) const
{
	// Add any brush resources here so that Slate can correctly atlas and reference them
}

