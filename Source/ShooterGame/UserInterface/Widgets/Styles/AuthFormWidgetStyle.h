// VRSPRO

#pragma once


#include "Styling/SlateWidgetStyle.h"
#include "SlateWidgetStyleContainerBase.h"

#include "AuthFormWidgetStyle.generated.h"

/**
 * 
 */
USTRUCT()
struct FAuthFormStyle : public FSlateWidgetStyle
{
	GENERATED_USTRUCT_BODY()

	UPROPERTY(Category = Appearance, EditAnywhere)
	FSlateBrush Background;

	UPROPERTY(Category = Appearance, EditAnywhere)
	FSlateBrush Border;

	UPROPERTY(Category = Appearance, EditAnywhere)
	FTextBlockStyle Header;

	UPROPERTY(Category = Appearance, EditAnywhere)
	FEditableTextBoxStyle Inputs;

	UPROPERTY(Category = Appearance, EditAnywhere)
	FCheckBoxStyle CheckBox;

	UPROPERTY(Category = Appearance, EditAnywhere)
	FTextBlockStyle CheckBoxText;

	UPROPERTY(Category = Appearance, EditAnywhere)
	FButtonStyle ButtonLeft;

	UPROPERTY(Category = Appearance, EditAnywhere)
	FButtonStyle ButtonRight;

	UPROPERTY(Category = Appearance, EditAnywhere)
	FTextBlockStyle ButtonsText;

	UPROPERTY(Category = Appearance, EditAnywhere)
	FSlateBrush ErrorBackground;

	UPROPERTY(Category = Appearance, EditAnywhere)
	FTextBlockStyle ErrorText;

	UPROPERTY(Category = Blur, EditAnywhere)	bool		bApplyAlphaToBlur;
	UPROPERTY(Category = Blur, EditAnywhere)	float		BlurStrength;
	UPROPERTY(Category = Blur, EditAnywhere)	int32		BlurRadius;

	FAuthFormStyle();
	virtual ~FAuthFormStyle();

	// FSlateWidgetStyle
	virtual void GetResources(TArray<const FSlateBrush*>& OutBrushes) const override;
	static const FName TypeName;
	virtual const FName GetTypeName() const override { return TypeName; };
	static const FAuthFormStyle& GetDefault();
};

/**
 */
UCLASS(hidecategories=Object, MinimalAPI)
class UAuthFormWidgetStyle : public USlateWidgetStyleContainerBase
{
	GENERATED_BODY()

public:
	/** The actual data describing the widget appearance. */
	UPROPERTY(Category=Appearance, EditAnywhere, meta=(ShowOnlyInnerProperties))
	FAuthFormStyle WidgetStyle;

	virtual const struct FSlateWidgetStyle* const GetStyle() const override
	{
		return static_cast< const struct FSlateWidgetStyle* >( &WidgetStyle );
	}
};
