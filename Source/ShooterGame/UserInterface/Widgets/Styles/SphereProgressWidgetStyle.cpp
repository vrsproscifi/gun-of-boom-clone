// Fill out your copyright notice in the Description page of Project Settings.

#include "ShooterGame.h"
#include "SphereProgressWidgetStyle.h"


FSphereProgressStyle::FSphereProgressStyle()
{
}

FSphereProgressStyle::~FSphereProgressStyle()
{
}

const FName FSphereProgressStyle::TypeName(TEXT("FSphereProgressStyle"));

const FSphereProgressStyle& FSphereProgressStyle::GetDefault()
{
	static FSphereProgressStyle Default;
	return Default;
}

void FSphereProgressStyle::GetResources(TArray<const FSlateBrush*>& OutBrushes) const
{
	// Add any brush resources here so that Slate can correctly atlas and reference them
}

