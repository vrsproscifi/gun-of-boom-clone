// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "Styling/SlateWidgetStyle.h"
#include "SlateWidgetStyleContainerBase.h"

#include "ShopItemWidgetStyle.generated.h"

/**
 * 
 */
USTRUCT()
struct SHOOTERGAME_API FShopItemStyle : public FSlateWidgetStyle
{
	GENERATED_USTRUCT_BODY()

	UPROPERTY(Category = Appearance, EditAnywhere)	FTextBlockStyle Title;
	UPROPERTY(Category = Appearance, EditAnywhere)	FTextBlockStyle Number;

	UPROPERTY(Category = Appearance, EditAnywhere)	FMargin TitlePadding;
	UPROPERTY(Category = Appearance, EditAnywhere)	FMargin ImagePaddingActive;
	UPROPERTY(Category = Appearance, EditAnywhere)	FMargin ImagePaddingInactive;

	UPROPERTY(Category = Appearance, EditAnywhere)	FVector2D ImageTranslucently;

	UPROPERTY(Category = Appearance, EditAnywhere)	float		FontScaleActive;
	UPROPERTY(Category = Appearance, EditAnywhere)	float		FontScaleInactive;

	UPROPERTY(Category = Expiremental, EditAnywhere)	FSlateBrush	PreviewBrush;

	FShopItemStyle();
	virtual ~FShopItemStyle();

	// FSlateWidgetStyle
	virtual void GetResources(TArray<const FSlateBrush*>& OutBrushes) const override;
	static const FName TypeName;
	virtual const FName GetTypeName() const override { return TypeName; };
	static const FShopItemStyle& GetDefault();
};

/**
 */
UCLASS(hidecategories=Object, MinimalAPI)
class UShopItemWidgetStyle : public USlateWidgetStyleContainerBase
{
	GENERATED_BODY()

public:
	/** The actual data describing the widget appearance. */
	UPROPERTY(Category=Appearance, EditAnywhere, meta=(ShowOnlyInnerProperties))
	FShopItemStyle WidgetStyle;

	virtual const struct FSlateWidgetStyle* const GetStyle() const override
	{
		return static_cast< const struct FSlateWidgetStyle* >( &WidgetStyle );
	}
};
