// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "Styling/SlateWidgetStyle.h"
#include "SlateWidgetStyleContainerBase.h"

#include "SpinNumberWidgetStyle.generated.h"

/**
 * 
 */
USTRUCT()
struct FSpinNumberStyle : public FSlateWidgetStyle
{
	GENERATED_USTRUCT_BODY()

	UPROPERTY(EditAnywhere, Category = Appearance)		FSlateBrush BackgroundBrush;
	UPROPERTY(EditAnywhere, Category = Appearance)		FSlateBrush HoveredBackgroundBrush;
	UPROPERTY(EditAnywhere, Category = Appearance)		FSlateBrush ActiveFillBrush;
	UPROPERTY(EditAnywhere, Category = Appearance)		FSlateBrush InactiveFillBrush;
	UPROPERTY(EditAnywhere, Category = Appearance)		FSlateBrush ArrowsImage;
	UPROPERTY(EditAnywhere, Category = Appearance)		FTextBlockStyle TextBlock;
	UPROPERTY(EditAnywhere, Category = Appearance)		FEditableTextStyle EditableText;
	UPROPERTY(EditAnywhere, Category = Appearance)		FMargin TextPadding;

	FSpinNumberStyle();
	virtual ~FSpinNumberStyle();

	// FSlateWidgetStyle
	virtual void GetResources(TArray<const FSlateBrush*>& OutBrushes) const override;
	static const FName TypeName;
	virtual const FName GetTypeName() const override { return TypeName; };
	static const FSpinNumberStyle& GetDefault();
};

/**
 */
UCLASS(hidecategories=Object, MinimalAPI)
class USpinNumberWidgetStyle : public USlateWidgetStyleContainerBase
{
	GENERATED_BODY()

public:
	/** The actual data describing the widget appearance. */
	UPROPERTY(Category=Appearance, EditAnywhere, meta=(ShowOnlyInnerProperties))
	FSpinNumberStyle WidgetStyle;

	virtual const struct FSlateWidgetStyle* const GetStyle() const override
	{
		return static_cast< const struct FSlateWidgetStyle* >( &WidgetStyle );
	}
};
