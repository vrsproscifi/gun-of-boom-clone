// Fill out your copyright notice in the Description page of Project Settings.

#include "ShooterGame.h"
#include "FightActionWidgetStyle.h"


FFightActionStyle::FFightActionStyle()
{
}

FFightActionStyle::~FFightActionStyle()
{
}

const FName FFightActionStyle::TypeName(TEXT("FFightActionStyle"));

const FFightActionStyle& FFightActionStyle::GetDefault()
{
	static FFightActionStyle Default;
	return Default;
}

void FFightActionStyle::GetResources(TArray<const FSlateBrush*>& OutBrushes) const
{
	// Add any brush resources here so that Slate can correctly atlas and reference them
}

