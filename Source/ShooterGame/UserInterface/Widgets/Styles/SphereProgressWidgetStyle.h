// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "Styling/SlateWidgetStyle.h"
#include "SlateWidgetStyleContainerBase.h"

#include "SphereProgressWidgetStyle.generated.h"

/**
 * 
 */
USTRUCT()
struct SHOOTERGAME_API FSphereProgressStyle : public FSlateWidgetStyle
{
	GENERATED_USTRUCT_BODY()

	UPROPERTY(Category = Appearance, EditAnywhere)	UMaterialInterface* SourceMaterial;

	UPROPERTY(Category = Appearance, EditAnywhere)	FTextBlockStyle		TitleFont;
	UPROPERTY(Category = Appearance, EditAnywhere)	FTextBlockStyle		ProgressFont;

	UPROPERTY(Category = Appearance, EditAnywhere)	UCurveLinearColor*	CurveBackground;
	UPROPERTY(Category = Appearance, EditAnywhere)	UCurveLinearColor*	CurveForeground;

	UPROPERTY(Category = Configuration, EditAnywhere)	FName			ParameterNameColor;
	UPROPERTY(Category = Configuration, EditAnywhere)	FName			ParameterNameProgress;
	UPROPERTY(Category = Configuration, EditAnywhere)	FName			ParameterNameRotation;

	FSphereProgressStyle();
	virtual ~FSphereProgressStyle();

	// FSlateWidgetStyle
	virtual void GetResources(TArray<const FSlateBrush*>& OutBrushes) const override;
	static const FName TypeName;
	virtual const FName GetTypeName() const override { return TypeName; };
	static const FSphereProgressStyle& GetDefault();
};

/**
 */
UCLASS(hidecategories=Object, MinimalAPI)
class USphereProgressWidgetStyle : public USlateWidgetStyleContainerBase
{
	GENERATED_BODY()

public:
	/** The actual data describing the widget appearance. */
	UPROPERTY(Category=Appearance, EditAnywhere, meta=(ShowOnlyInnerProperties))
	FSphereProgressStyle WidgetStyle;

	virtual const struct FSlateWidgetStyle* const GetStyle() const override
	{
		return static_cast< const struct FSlateWidgetStyle* >( &WidgetStyle );
	}
};
