// Fill out your copyright notice in the Description page of Project Settings.

#include "ShooterGame.h"
#include "SocialButtonWidgetStyle.h"

FSocialButtonStyle::FSocialButtonStyle()
{
}

FSocialButtonStyle::~FSocialButtonStyle()
{
}

const FName FSocialButtonStyle::TypeName(TEXT("FSocialButtonStyle"));

const FSocialButtonStyle& FSocialButtonStyle::GetDefault()
{
	static FSocialButtonStyle Default;
	return Default;
}

void FSocialButtonStyle::GetResources(TArray<const FSlateBrush*>& OutBrushes) const
{
	// Add any brush resources here so that Slate can correctly atlas and reference them
}

