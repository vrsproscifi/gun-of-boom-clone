// Fill out your copyright notice in the Description page of Project Settings.

#include "ShooterGame.h"
#include "CurrencyBlockWidgetStyle.h"


FCurrencyBlockStyle::FCurrencyBlockStyle()
{
}

FCurrencyBlockStyle::~FCurrencyBlockStyle()
{
}

const FName FCurrencyBlockStyle::TypeName(TEXT("FCurrencyBlockStyle"));

const FCurrencyBlockStyle& FCurrencyBlockStyle::GetDefault()
{
	static FCurrencyBlockStyle Default;
	return Default;
}

void FCurrencyBlockStyle::GetResources(TArray<const FSlateBrush*>& OutBrushes) const
{
	// Add any brush resources here so that Slate can correctly atlas and reference them
}

