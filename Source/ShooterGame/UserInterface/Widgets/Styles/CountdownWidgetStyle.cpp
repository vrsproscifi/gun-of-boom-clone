// Fill out your copyright notice in the Description page of Project Settings.

#include "ShooterGame.h"
#include "CountdownWidgetStyle.h"


FCountdownStyle::FCountdownStyle()
{
}

FCountdownStyle::~FCountdownStyle()
{
}

const FName FCountdownStyle::TypeName(TEXT("FCountdownStyle"));

const FCountdownStyle& FCountdownStyle::GetDefault()
{
	static FCountdownStyle Default;
	return Default;
}

void FCountdownStyle::GetResources(TArray<const FSlateBrush*>& OutBrushes) const
{
	// Add any brush resources here so that Slate can correctly atlas and reference them
}

