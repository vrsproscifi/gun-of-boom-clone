// Fill out your copyright notice in the Description page of Project Settings.

#include "ShooterGame.h"
#include "VerticalMenuBuilderWidgetStyle.h"


FVerticalMenuBuilderStyle::FVerticalMenuBuilderStyle()
	: ContainerButtonsPadding(FVector2D(4, 10))
	, ButtonsTextPadding(FVector2D(0, 4))
	, AnimationTransform(FVector2D(0.0f, 1.5f))
	, AnimationContainerColor(FVector2D(0.5f, 1.5f))
	, AnimationBorderColor(FVector2D(0.5f, 1.5f))
	, AnimationSubBorders(FVector2D(0.0f, 1.0f))
	, AnimationButtons(FVector2D(0.0f, 1.0f))
	, ButtonsSize(FVector2D::ZeroVector)
	, ButtonsPadding(FVector2D::ZeroVector)
{
}

FVerticalMenuBuilderStyle::~FVerticalMenuBuilderStyle()
{
}

const FName FVerticalMenuBuilderStyle::TypeName(TEXT("VerticalMenuBuilderStyle"));

const FVerticalMenuBuilderStyle& FVerticalMenuBuilderStyle::GetDefault()
{
	static FVerticalMenuBuilderStyle Default;
	return Default;
}

void FVerticalMenuBuilderStyle::GetResources(TArray<const FSlateBrush*>& OutBrushes) const
{
	OutBrushes.Add(&Background);
	OutBrushes.Add(&Border);
	OutBrushes.Add(&SubBorder);

	ButtonsStyle.GetResources(OutBrushes);
	ButtonsTextStyle.GetResources(OutBrushes);
}

