// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "Styling/SlateWidgetStyle.h"
#include "SlateWidgetStyleContainerBase.h"

#include "TocuhButtonWidgetStyle.generated.h"

/**
 * 
 */
USTRUCT()
struct SHOOTERGAME_API FTocuhButtonStyle : public FSlateWidgetStyle
{
	GENERATED_USTRUCT_BODY()

	UPROPERTY(Category = Appearance, EditAnywhere)		FSlateBrush Normal;
	UPROPERTY(Category = Appearance, EditAnywhere)		FSlateBrush Pressed;

	UPROPERTY(Category = Appearance, EditAnywhere)		FSlateSound PressedSound;

	FTocuhButtonStyle();
	virtual ~FTocuhButtonStyle();

	// FSlateWidgetStyle
	virtual void GetResources(TArray<const FSlateBrush*>& OutBrushes) const override;
	static const FName TypeName;
	virtual const FName GetTypeName() const override { return TypeName; };
	static const FTocuhButtonStyle& GetDefault();
};

/**
 */
UCLASS(hidecategories=Object, MinimalAPI)
class UTocuhButtonWidgetStyle : public USlateWidgetStyleContainerBase
{
	GENERATED_BODY()

public:
	/** The actual data describing the widget appearance. */
	UPROPERTY(Category=Appearance, EditAnywhere, meta=(ShowOnlyInnerProperties))
	FTocuhButtonStyle WidgetStyle;

	virtual const struct FSlateWidgetStyle* const GetStyle() const override
	{
		return static_cast< const struct FSlateWidgetStyle* >( &WidgetStyle );
	}
};
