// Fill out your copyright notice in the Description page of Project Settings.

#include "ShooterGame.h"
#include "NotifyContainerWidgetStyle.h"


FNotifyContainerStyle::FNotifyContainerStyle()
{
}

FNotifyContainerStyle::~FNotifyContainerStyle()
{
}

const FName FNotifyContainerStyle::TypeName(TEXT("FNotifyContainerStyle"));

const FNotifyContainerStyle& FNotifyContainerStyle::GetDefault()
{
	static FNotifyContainerStyle Default;
	return Default;
}

void FNotifyContainerStyle::GetResources(TArray<const FSlateBrush*>& OutBrushes) const
{
	// Add any brush resources here so that Slate can correctly atlas and reference them
}

