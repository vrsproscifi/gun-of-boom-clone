// Fill out your copyright notice in the Description page of Project Settings.

#include "ShooterGame.h"
#include "KillListWidgetStyle.h"


FKillListStyle::FKillListStyle()
{
}

FKillListStyle::~FKillListStyle()
{
}

const FName FKillListStyle::TypeName(TEXT("FKillListStyle"));

const FKillListStyle& FKillListStyle::GetDefault()
{
	static FKillListStyle Default;
	return Default;
}

void FKillListStyle::GetResources(TArray<const FSlateBrush*>& OutBrushes) const
{
	// Add any brush resources here so that Slate can correctly atlas and reference them
}

