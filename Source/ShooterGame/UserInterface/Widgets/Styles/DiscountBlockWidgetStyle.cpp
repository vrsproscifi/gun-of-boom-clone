// Fill out your copyright notice in the Description page of Project Settings.

#include "ShooterGame.h"
#include "DiscountBlockWidgetStyle.h"


FDiscountBlockStyle::FDiscountBlockStyle()
{
}

FDiscountBlockStyle::~FDiscountBlockStyle()
{
}

const FName FDiscountBlockStyle::TypeName(TEXT("FDiscountBlockStyle"));

const FDiscountBlockStyle& FDiscountBlockStyle::GetDefault()
{
	static FDiscountBlockStyle Default;
	return Default;
}

void FDiscountBlockStyle::GetResources(TArray<const FSlateBrush*>& OutBrushes) const
{
	// Add any brush resources here so that Slate can correctly atlas and reference them
}

