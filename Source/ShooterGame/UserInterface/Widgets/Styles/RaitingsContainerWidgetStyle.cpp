// Fill out your copyright notice in the Description page of Project Settings.

#include "ShooterGame.h"
#include "RaitingsContainerWidgetStyle.h"


FRaitingsContainerStyle::FRaitingsContainerStyle()
{
}

FRaitingsContainerStyle::~FRaitingsContainerStyle()
{
}

const FName FRaitingsContainerStyle::TypeName(TEXT("FRaitingsContainerStyle"));

const FRaitingsContainerStyle& FRaitingsContainerStyle::GetDefault()
{
	static FRaitingsContainerStyle Default;
	return Default;
}

void FRaitingsContainerStyle::GetResources(TArray<const FSlateBrush*>& OutBrushes) const
{
	// Add any brush resources here so that Slate can correctly atlas and reference them
}

