// VRSPRO

#include "ShooterGame.h"
#include "TextBlockButtonWidgetStyle.h"


FTextBlockButtonStyle::FTextBlockButtonStyle()
{
}

FTextBlockButtonStyle::~FTextBlockButtonStyle()
{
}

const FName FTextBlockButtonStyle::TypeName(TEXT("FTextBlockButtonStyle"));

const FTextBlockButtonStyle& FTextBlockButtonStyle::GetDefault()
{
	static FTextBlockButtonStyle Default;
	return Default;
}

void FTextBlockButtonStyle::GetResources(TArray<const FSlateBrush*>& OutBrushes) const
{
	// Add any brush resources here so that Slate can correctly atlas and reference them
}

