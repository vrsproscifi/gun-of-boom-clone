// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "Styling/SlateWidgetStyle.h"
#include "SlateWidgetStyleContainerBase.h"

#include "ProfileContainerWidgetStyle.generated.h"

/**
 * 
 */
USTRUCT()
struct SHOOTERGAME_API FProfileContainerStyle : public FSlateWidgetStyle
{
	GENERATED_USTRUCT_BODY()

	UPROPERTY(Category = Appearance, EditAnywhere)		FTextBlockStyle		StatTitle;
	UPROPERTY(Category = Appearance, EditAnywhere)		FTextBlockStyle		StatValue;

	UPROPERTY(Category = Appearance, EditAnywhere)		FSlateBrush			SeparatorBrush;
	UPROPERTY(Category = Appearance, EditAnywhere)		float				SeparatorThickness;

	UPROPERTY(Category = Appearance, EditAnywhere)		FTextBlockStyle		EqiupTitle;
	UPROPERTY(Category = Appearance, EditAnywhere)		FTextBlockStyle		EqiupValue;

	UPROPERTY(Category = Account, EditAnywhere)			FTextBlockStyle			FontAwesome;
	UPROPERTY(Category = Account, EditAnywhere)			FEditableTextBoxStyle	Input;
	UPROPERTY(Category = Account, EditAnywhere)			FButtonStyle			InputButton;

	UPROPERTY(Category = Account, EditAnywhere)			FSlateBrush			IconGoogle;
	UPROPERTY(Category = Account, EditAnywhere)			FSlateBrush			IconFacebook;

	FProfileContainerStyle();
	virtual ~FProfileContainerStyle();

	// FSlateWidgetStyle
	virtual void GetResources(TArray<const FSlateBrush*>& OutBrushes) const override;
	static const FName TypeName;
	virtual const FName GetTypeName() const override { return TypeName; };
	static const FProfileContainerStyle& GetDefault();
};

/**
 */
UCLASS(hidecategories=Object, MinimalAPI)
class UProfileContainerWidgetStyle : public USlateWidgetStyleContainerBase
{
	GENERATED_BODY()

public:
	/** The actual data describing the widget appearance. */
	UPROPERTY(Category=Appearance, EditAnywhere, meta=(ShowOnlyInnerProperties))
	FProfileContainerStyle WidgetStyle;

	virtual const struct FSlateWidgetStyle* const GetStyle() const override
	{
		return static_cast< const struct FSlateWidgetStyle* >( &WidgetStyle );
	}
};
