// VRSPRO

#pragma once


#include "Styling/SlateWidgetStyle.h"
#include "SlateWidgetStyleContainerBase.h"
#include "Shared/AttachAccountService.h"

#include "AttachAccountServiceWidgetStyle.generated.h"



USTRUCT()
struct FAccountServiceStyle
{
	GENERATED_USTRUCT_BODY()

	UPROPERTY(Category = Appearance, EditAnywhere)
	FSlateBrush Icon;

	UPROPERTY(Category = Appearance, EditAnywhere)
	FText Name;

	UPROPERTY(Category = Appearance, EditAnywhere)
	FText Description;

};

USTRUCT()
struct FAttachAccountServiceStyle : public FSlateWidgetStyle
{
	GENERATED_USTRUCT_BODY()

	UPROPERTY(Category = Appearance, EditAnywhere)
	FAccountServiceStyle Services[EAccountService::END];

	UPROPERTY(Category = Appearance, EditAnywhere)
	FTextBlockStyle Text;

	UPROPERTY(Category = Appearance, EditAnywhere)
	FTextBlockStyle Header;

	UPROPERTY(Category = Appearance, EditAnywhere)
	FButtonStyle Button;

	FAttachAccountServiceStyle();
	virtual ~FAttachAccountServiceStyle();

	// FSlateWidgetStyle
	virtual void GetResources(TArray<const FSlateBrush*>& OutBrushes) const override;
	static const FName TypeName;
	virtual const FName GetTypeName() const override { return TypeName; };
	static const FAttachAccountServiceStyle& GetDefault();
};

/**
 */
UCLASS(hidecategories=Object, MinimalAPI)
class UAttachAccountServiceWidgetStyle : public USlateWidgetStyleContainerBase
{
	GENERATED_BODY()

public:
	/** The actual data describing the widget appearance. */
	UPROPERTY(Category=Appearance, EditAnywhere, meta=(ShowOnlyInnerProperties))
	FAttachAccountServiceStyle WidgetStyle;

	virtual const struct FSlateWidgetStyle* const GetStyle() const override
	{
		return static_cast< const struct FSlateWidgetStyle* >( &WidgetStyle );
	}
};
