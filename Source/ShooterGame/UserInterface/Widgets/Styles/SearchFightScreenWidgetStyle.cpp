// Fill out your copyright notice in the Description page of Project Settings.

#include "ShooterGame.h"
#include "SearchFightScreenWidgetStyle.h"


FSearchFightScreenStyle::FSearchFightScreenStyle()
{
}

FSearchFightScreenStyle::~FSearchFightScreenStyle()
{
}

const FName FSearchFightScreenStyle::TypeName(TEXT("FSearchFightScreenStyle"));

const FSearchFightScreenStyle& FSearchFightScreenStyle::GetDefault()
{
	static FSearchFightScreenStyle Default;
	return Default;
}

void FSearchFightScreenStyle::GetResources(TArray<const FSlateBrush*>& OutBrushes) const
{
	// Add any brush resources here so that Slate can correctly atlas and reference them
}

