// Fill out your copyright notice in the Description page of Project Settings.

#include "ShooterGame.h"
#include "FightResultsWidgetStyle.h"


FFightResultsStyle::FFightResultsStyle()
{
}

FFightResultsStyle::~FFightResultsStyle()
{
}

const FName FFightResultsStyle::TypeName(TEXT("FFightResultsStyle"));

const FFightResultsStyle& FFightResultsStyle::GetDefault()
{
	static FFightResultsStyle Default;
	return Default;
}

void FFightResultsStyle::GetResources(TArray<const FSlateBrush*>& OutBrushes) const
{
	// Add any brush resources here so that Slate can correctly atlas and reference them
}

