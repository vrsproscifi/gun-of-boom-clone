// VRSPRO

#pragma once


#include "Styling/SlateWidgetStyle.h"
#include "SlateWidgetStyleContainerBase.h"
#include "Components/SResourceTextBoxType.h"
#include "ResourceTextBoxWidgetStyle.generated.h"


USTRUCT()
struct FRTBStyles
{
	GENERATED_USTRUCT_BODY()

	UPROPERTY(Category = Appearance, EditAnywhere)
		FSlateBrush Image;

	UPROPERTY(Category = Appearance, EditAnywhere)
		FTextBlockStyle TextBlock;
};

USTRUCT()
struct FResourceTextBoxStyle : public FSlateWidgetStyle
{
	GENERATED_USTRUCT_BODY()

	FResourceTextBoxStyle();
	virtual ~FResourceTextBoxStyle();

	// FSlateWidgetStyle
	virtual void GetResources(TArray<const FSlateBrush*>& OutBrushes) const override;
	static const FName TypeName;
	virtual const FName GetTypeName() const override { return TypeName; };
	static const FResourceTextBoxStyle& GetDefault();

	UPROPERTY(Category = Appearance, EditAnywhere)
	TMap<TEnumAsByte<EResourceTextBoxType::Type>, FRTBStyles> DefaultStyles;

	UPROPERTY(Category = Appearance, EditAnywhere)
	TMap<int32, FRTBStyles> CustomStyles;

	UPROPERTY(Category = Appearance, EditAnywhere)
	TMap<EResourceTextBoxOverlayMethod, FSlateBrush> OverlayLines;
};

/**
 */
UCLASS(hidecategories=Object, MinimalAPI)
class UResourceTextBoxWidgetStyle : public USlateWidgetStyleContainerBase
{
	GENERATED_BODY()

public:
	/** The actual data describing the widget appearance. */
	UPROPERTY(Category=Appearance, EditAnywhere, meta=(ShowOnlyInnerProperties))
	FResourceTextBoxStyle WidgetStyle;

	virtual const struct FSlateWidgetStyle* const GetStyle() const override
	{
		return static_cast< const struct FSlateWidgetStyle* >( &WidgetStyle );
	}
};
