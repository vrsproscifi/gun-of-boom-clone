// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "Styling/SlateWidgetStyle.h"
#include "SlateWidgetStyleContainerBase.h"

#include "TouchInputPickupWidgetStyle.generated.h"

/**
 * 
 */
USTRUCT()
struct SHOOTERGAME_API FTouchInputPickupStyle : public FSlateWidgetStyle
{
	GENERATED_USTRUCT_BODY()

	UPROPERTY(Category = Appearance, EditAnywhere)	FSlateBrush NormalBrush;
	UPROPERTY(Category = Appearance, EditAnywhere)	FSlateBrush ActiveBrush;

	UPROPERTY(Category = Appearance, EditAnywhere)	FTextBlockStyle Text;

	UPROPERTY(Category = Appearance, EditAnywhere)	FSlateBrush BrushBetter;
	UPROPERTY(Category = Appearance, EditAnywhere)	FSlateBrush BrushSome;
	UPROPERTY(Category = Appearance, EditAnywhere)	FSlateBrush BrushWorse;

	FTouchInputPickupStyle();
	virtual ~FTouchInputPickupStyle();

	// FSlateWidgetStyle
	virtual void GetResources(TArray<const FSlateBrush*>& OutBrushes) const override;
	static const FName TypeName;
	virtual const FName GetTypeName() const override { return TypeName; };
	static const FTouchInputPickupStyle& GetDefault();
};

/**
 */
UCLASS(hidecategories=Object, MinimalAPI)
class UTouchInputPickupWidgetStyle : public USlateWidgetStyleContainerBase
{
	GENERATED_BODY()

public:
	/** The actual data describing the widget appearance. */
	UPROPERTY(Category=Appearance, EditAnywhere, meta=(ShowOnlyInnerProperties))
	FTouchInputPickupStyle WidgetStyle;

	virtual const struct FSlateWidgetStyle* const GetStyle() const override
	{
		return static_cast< const struct FSlateWidgetStyle* >( &WidgetStyle );
	}
};
