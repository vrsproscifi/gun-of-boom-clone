// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "Styling/SlateWidgetStyle.h"
#include "SlateWidgetStyleContainerBase.h"

#include "LiveBarWidgetStyle.generated.h"

/**
 * 
 */
USTRUCT()
struct SHOOTERGAME_API FLiveBarStyle : public FSlateWidgetStyle
{
	GENERATED_USTRUCT_BODY()

	UPROPERTY(Category = Appearance, EditAnywhere)	FProgressBarStyle HealthBar;
	UPROPERTY(Category = Appearance, EditAnywhere)	FTextBlockStyle HealthFont;
	UPROPERTY(Category = Appearance, EditAnywhere)	FTextBlockStyle ArmorFont;
	UPROPERTY(Category = Appearance, EditAnywhere)	FProgressBarStyle ArmorBar;

	UPROPERTY(Category = Appearance, EditAnywhere)	UCurveLinearColor* RealoadCurve;

	FLiveBarStyle();
	virtual ~FLiveBarStyle();

	// FSlateWidgetStyle
	virtual void GetResources(TArray<const FSlateBrush*>& OutBrushes) const override;
	static const FName TypeName;
	virtual const FName GetTypeName() const override { return TypeName; };
	static const FLiveBarStyle& GetDefault();
};

/**
 */
UCLASS(hidecategories=Object, MinimalAPI)
class ULiveBarWidgetStyle : public USlateWidgetStyleContainerBase
{
	GENERATED_BODY()

public:
	/** The actual data describing the widget appearance. */
	UPROPERTY(Category=Appearance, EditAnywhere, meta=(ShowOnlyInnerProperties))
	FLiveBarStyle WidgetStyle;

	virtual const struct FSlateWidgetStyle* const GetStyle() const override
	{
		return static_cast< const struct FSlateWidgetStyle* >( &WidgetStyle );
	}
};
