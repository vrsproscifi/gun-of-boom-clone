// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "Styling/SlateWidgetStyle.h"
#include "SlateWidgetStyleContainerBase.h"

#include "MessageTipWidgetStyle.generated.h"


USTRUCT()
struct FMessageTipStyle_Separator
{
	GENERATED_USTRUCT_BODY()

	UPROPERTY(EditAnywhere)	FSlateBrush					SeparatorImage;
	UPROPERTY(EditAnywhere)	TEnumAsByte<EOrientation>	Orientation;
	UPROPERTY(EditAnywhere)	float						Thickness;
	UPROPERTY(EditAnywhere)	FLinearColor				ColorAndOpacity;
};

USTRUCT()
struct FMessageTipStyle : public FSlateWidgetStyle
{
	GENERATED_USTRUCT_BODY()

	UPROPERTY(Category = Appearance, EditAnywhere)		FSlateBrush					Background;
	UPROPERTY(Category = Appearance, EditAnywhere)		FMessageTipStyle_Separator	Separator;
	UPROPERTY(Category = Appearance, EditAnywhere)		FTextBlockStyle				TitleFont;
	UPROPERTY(Category = Appearance, EditAnywhere)		FTextBlockStyle				ContentFont;
	UPROPERTY(Category = Appearance, EditAnywhere)		FSlateSound					ShowSound;
	UPROPERTY(Category = Appearance, EditAnywhere)		FSlateSound					HideSound;
	UPROPERTY(Category = Configuration, EditAnywhere)	FVector2D					MinSize;
	UPROPERTY(Category = Configuration, EditAnywhere)	FVector2D					MaxSize;

	FMessageTipStyle();
	virtual ~FMessageTipStyle();

	// FSlateWidgetStyle
	virtual void GetResources(TArray<const FSlateBrush*>& OutBrushes) const override;
	static const FName TypeName;
	virtual const FName GetTypeName() const override { return TypeName; };
	static const FMessageTipStyle& GetDefault();
};

/**
 */
UCLASS(hidecategories=Object, MinimalAPI)
class UMessageTipWidgetStyle : public USlateWidgetStyleContainerBase
{
	GENERATED_BODY()

public:
	/** The actual data describing the widget appearance. */
	UPROPERTY(Category=Appearance, EditAnywhere, meta=(ShowOnlyInnerProperties))
	FMessageTipStyle WidgetStyle;

	virtual const struct FSlateWidgetStyle* const GetStyle() const override
	{
		return static_cast< const struct FSlateWidgetStyle* >( &WidgetStyle );
	}
};
