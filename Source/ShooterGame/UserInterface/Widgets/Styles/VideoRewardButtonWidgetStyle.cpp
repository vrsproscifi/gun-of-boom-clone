// Fill out your copyright notice in the Description page of Project Settings.

#include "ShooterGame.h"
#include "VideoRewardButtonWidgetStyle.h"

FVideoRewardButtonStyle::FVideoRewardButtonStyle()
{
}

FVideoRewardButtonStyle::~FVideoRewardButtonStyle()
{
}

const FName FVideoRewardButtonStyle::TypeName(TEXT("FVideoRewardButtonStyle"));

const FVideoRewardButtonStyle& FVideoRewardButtonStyle::GetDefault()
{
	static FVideoRewardButtonStyle Default;
	return Default;
}

void FVideoRewardButtonStyle::GetResources(TArray<const FSlateBrush*>& OutBrushes) const
{
	// Add any brush resources here so that Slate can correctly atlas and reference them
}

