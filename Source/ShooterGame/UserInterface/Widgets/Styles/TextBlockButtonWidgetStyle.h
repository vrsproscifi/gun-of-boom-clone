// VRSPRO

#pragma once


#include "Styling/SlateWidgetStyle.h"
#include "SlateWidgetStyleContainerBase.h"

#include "TextBlockButtonWidgetStyle.generated.h"

/**
 * 
 */
USTRUCT(BlueprintType)
struct SHOOTERGAME_API FTextBlockButtonStyle : public FSlateWidgetStyle
{
	GENERATED_USTRUCT_BODY()

	FTextBlockButtonStyle();
	virtual ~FTextBlockButtonStyle();

	// FSlateWidgetStyle
	virtual void GetResources(TArray<const FSlateBrush*>& OutBrushes) const override;
	static const FName TypeName;
	virtual const FName GetTypeName() const override { return TypeName; };
	static const FTextBlockButtonStyle& GetDefault();

	UPROPERTY(Category = Visual, EditAnywhere)
		FTextBlockStyle Normal;

	UPROPERTY(Category = Visual, EditAnywhere)
		FTextBlockStyle Hovered;

	UPROPERTY(Category = Visual, EditAnywhere)
		FTextBlockStyle Pressed;

	UPROPERTY(Category = Visual, EditAnywhere)
		FTextBlockStyle Disabled;

	UPROPERTY(Category = Configuration, EditAnywhere)
		FMargin NormalPadding;

	UPROPERTY(Category = Configuration, EditAnywhere)
		FMargin HoveredPadding;

	UPROPERTY(Category = Configuration, EditAnywhere)
		FMargin PressedPadding;

	UPROPERTY(Category = Sound, EditAnywhere)
		FSlateSound HoveredSound;

	UPROPERTY(Category = Sound, EditAnywhere)
		FSlateSound PressedSound;
};

/**
 */
UCLASS(hidecategories=Object, MinimalAPI)
class UTextBlockButtonWidgetStyle : public USlateWidgetStyleContainerBase
{
	GENERATED_BODY()

public:
	/** The actual data describing the widget appearance. */
	UPROPERTY(Category=Appearance, EditAnywhere, meta=(ShowOnlyInnerProperties))
	FTextBlockButtonStyle WidgetStyle;

	virtual const struct FSlateWidgetStyle* const GetStyle() const override
	{
		return static_cast< const struct FSlateWidgetStyle* >( &WidgetStyle );
	}
};
