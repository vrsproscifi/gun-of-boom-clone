// Fill out your copyright notice in the Description page of Project Settings.

#include "ShooterGame.h"
#include "TouchInputStickWidgetStyle.h"


FTouchInputStickStyle::FTouchInputStickStyle()
{
}

FTouchInputStickStyle::~FTouchInputStickStyle()
{
}

const FName FTouchInputStickStyle::TypeName(TEXT("FTouchInputStickStyle"));

const FTouchInputStickStyle& FTouchInputStickStyle::GetDefault()
{
	static FTouchInputStickStyle Default;
	return Default;
}

void FTouchInputStickStyle::GetResources(TArray<const FSlateBrush*>& OutBrushes) const
{
	// Add any brush resources here so that Slate can correctly atlas and reference them
}

