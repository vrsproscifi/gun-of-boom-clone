// Fill out your copyright notice in the Description page of Project Settings.

#include "ShooterGame.h"
#include "LiveBarWidgetStyle.h"


FLiveBarStyle::FLiveBarStyle()
{
}

FLiveBarStyle::~FLiveBarStyle()
{
}

const FName FLiveBarStyle::TypeName(TEXT("FLiveBarStyle"));

const FLiveBarStyle& FLiveBarStyle::GetDefault()
{
	static FLiveBarStyle Default;
	return Default;
}

void FLiveBarStyle::GetResources(TArray<const FSlateBrush*>& OutBrushes) const
{
	// Add any brush resources here so that Slate can correctly atlas and reference them
}

