// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "SArsenalItem.h"

class UBasicPlayerItem;
/**
 * 
 */
class SHOOTERGAME_API SArsenalUpgradeItem : public SArsenalItem
{
public:
	SLATE_BEGIN_ARGS(SArsenalUpgradeItem)
	{}
	SLATE_ATTRIBUTE(float, ActiveCoefficient)
	SLATE_ARGUMENT(uint8, Level)
	SLATE_END_ARGS()

	void Construct(const FArguments& InArgs, TWeakObjectPtr<UBasicPlayerItem> InInstance);

protected:

	uint8 Level;

	virtual FText GetItemName() const override;
};
