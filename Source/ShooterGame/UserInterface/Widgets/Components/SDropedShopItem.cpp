// Fill out your copyright notice in the Description page of Project Settings.

#include "ShooterGame.h"
#include "SDropedShopItem.h"
#include "SlateOptMacros.h"
#include "SScaleBox.h"
#include "BasicItemEntity.h"

BEGIN_SLATE_FUNCTION_BUILD_OPTIMIZATION
void SDropedShopItem::Construct(const FArguments& InArgs, TWeakObjectPtr<UBasicItemEntity> InInstance)
{
	SShopItem::Construct(SShopItem::FArguments().ActiveCoefficient(0), InInstance);

	if (InArgs._Range.GetMax())
	{
		Container->AddSlot().HAlign(HAlign_Right).VAlign(VAlign_Bottom).Padding(80)
		[
			SNew(STextBlock)
			.Justification(ETextJustify::Center)
			.Text(FText::Format(FText::FromString("x{0} - x{1}"), FText::AsNumber(InArgs._Range.X, &FNumberFormattingOptions::DefaultNoGrouping()), FText::AsNumber(InArgs._Range.Y, &FNumberFormattingOptions::DefaultNoGrouping())))
			.TextStyle(&Style->Number)
		];
	}
	else
	{
		Container->AddSlot().HAlign(HAlign_Right).VAlign(VAlign_Bottom).Padding(80)
		[
			SNew(STextBlock)
			.Justification(ETextJustify::Center)
			.Text(FText::Format(FText::FromString("x{0}"), FText::AsNumber(InArgs._Num)))
			.TextStyle(&Style->Number).Clipping(EWidgetClipping::Inherit)
		];
	}
}
END_SLATE_FUNCTION_BUILD_OPTIMIZATION
