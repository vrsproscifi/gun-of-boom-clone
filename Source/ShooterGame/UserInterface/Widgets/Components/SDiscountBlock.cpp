// Fill out your copyright notice in the Description page of Project Settings.

#include "ShooterGame.h"
#include "SDiscountBlock.h"
#include "SlateOptMacros.h"
#include "UObjectExtensions.h"
#include "BasicItemEntity.h"
#include "SResourceTextBox.h"

BEGIN_SLATE_FUNCTION_BUILD_OPTIMIZATION
void SDiscountBlock::Construct(const FArguments& InArgs)
{
	Style = InArgs._Style;

	ChildSlot
	[
		SNew(SBorder)
		.Padding(8)
		.BorderImage(&Style->Background)
		[
			SNew(SVerticalBox)
			+ SVerticalBox::Slot().AutoHeight()
			[
				SNew(STextBlock)
				.Text(this, &SDiscountBlock::GetDiscountText)
				.TextStyle(&Style->Title)
			]
			+ SVerticalBox::Slot().AutoHeight()
			[
				SNew(SResourceTextBox)
				.Type(EResourceTextBoxType::Time)
				.Value(this, &SDiscountBlock::GetDiscountTime)
			]
		]
	];

	SetProduct(InArgs._Product);
}

END_SLATE_FUNCTION_BUILD_OPTIMIZATION

void SDiscountBlock::SetProduct(const TAttribute<UBasicItemEntity*>& InProduct)
{
	Attr_Product = InProduct;
}

FText SDiscountBlock::GetDiscountText() const
{
	if (auto ProductTarget = GetValidObject(Attr_Product.Get()))
	{
		return FText::Format(NSLOCTEXT("SDiscountBlock", "SDiscountBlock.Discount", "Discount {0}%"),
			FText::AsNumber(ProductTarget->GetActiveDiscountPercent(), &FNumberFormattingOptions::DefaultNoGrouping())
		);
	}

	return FText::GetEmpty();
}

int64 SDiscountBlock::GetDiscountTime() const
{
	if (auto ProductTarget = GetValidObject(Attr_Product.Get()))
	{
		return ProductTarget->GetAvalibleDiscountTime().GetTotalSeconds();
	}

	return 0;
}
