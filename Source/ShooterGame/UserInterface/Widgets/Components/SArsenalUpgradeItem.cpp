// Fill out your copyright notice in the Description page of Project Settings.

#include "ShooterGame.h"
#include "SArsenalUpgradeItem.h"
#include "SlateOptMacros.h"
#include "SScaleBox.h"

#include "BasicPlayerItem.h"
#include "BasicItemEntity.h"

BEGIN_SLATE_FUNCTION_BUILD_OPTIMIZATION
void SArsenalUpgradeItem::Construct(const FArguments& InArgs, TWeakObjectPtr<UBasicPlayerItem> InInstance)
{
	InstanceItem = InInstance;
	Level = InArgs._Level;

	if (IsValidObject(InstanceItem) && InstanceItem->GetEntityBase())
	{
		SArsenalItem::Construct(SArsenalItem::FArguments().ActiveCoefficient(InArgs._ActiveCoefficient), InstanceItem);
	}
}
END_SLATE_FUNCTION_BUILD_OPTIMIZATION

FText SArsenalUpgradeItem::GetItemName() const
{
	if (IsValidObject(InstanceItem) && IsValidObject(GetInstanceBasic()))
	{
		return FText::Format(FText::FromString("<text font=\"Oswald.Medium\">{0}|{1}</>"), FText::AsNumber(Level, &FNumberFormattingOptions::DefaultNoGrouping()), FText::AsNumber(GetInstanceBasic()->GetItemProperty().Modifications.Num() - 1, &FNumberFormattingOptions::DefaultNoGrouping()));
	}

	return SArsenalItem::GetItemName();
}
