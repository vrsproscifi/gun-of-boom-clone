// Fill out your copyright notice in the Description page of Project Settings.

#include "ShooterGame.h"
#include "SRadioButtonsBox.h"
#include "SlateOptMacros.h"

BEGIN_SLATE_FUNCTION_BUILD_OPTIMIZATION
void SRadioButtonsBox::Construct(const FArguments& InArgs)
{
	Style = InArgs._Style;
	Orientation = InArgs._Orientation;
	ButtonsText = InArgs._ButtonsText;
	ButtonsWidget = InArgs._ButtonsWidget;
	OnIndexChanged = InArgs._OnIndexChanged;
	ButtonsPadding = InArgs._ButtonsPadding;
	IsAutoSize = InArgs._IsAutoSize;

	SetActiveIndex(InArgs._ActiveIndex);
	RebuildWidget();
}

void SRadioButtonsBox::RebuildWidget()
{
	TArray<TSharedRef<SWidget>> DoneWidgets;

	for (SIZE_T MyWidgetIdx = 0; MyWidgetIdx < ButtonsWidget.Num(); ++MyWidgetIdx)
	{
		if (ButtonsWidget.IsValidIndex(MyWidgetIdx) && ButtonsWidget[MyWidgetIdx].ToSharedRef() != SNullWidget::NullWidget)
		{
			DoneWidgets.Insert(ButtonsWidget[MyWidgetIdx].ToSharedRef(), MyWidgetIdx);
		}
	}

	for (SIZE_T MyTextIdx = 0; MyTextIdx < ButtonsText.Num(); ++MyTextIdx)
	{
		if (ButtonsText.IsValidIndex(MyTextIdx))
		{
			TSharedRef<SWidget> TargetWidget = SNew(SBox).HAlign(HAlign_Center).VAlign(VAlign_Center)
			[
				SNew(STextBlock)
				.Justification(ETextJustify::Center)
				.Text(ButtonsText[MyTextIdx])
				.TextStyle(Orientation == Orient_Vertical ? &Style->VerticalControlls.Text : &Style->HorizontalControlls.Text)
				.Margin(Orientation == Orient_Vertical ? Style->VerticalControlls.TextMargin : Style->HorizontalControlls.TextMargin)
			];

			DoneWidgets.Insert(TargetWidget, MyTextIdx);
		}
	}

	TSharedRef<SHorizontalBox> HorizontalBox = SNew(SHorizontalBox);
	TSharedRef<SVerticalBox> VerticalBox = SNew(SVerticalBox);

	int32 _count = 0;
	for (auto MyWidget : DoneWidgets)
	{
		const FCheckBoxStyle* TargetStyle_CheckBox = nullptr;

		if (Orientation == Orient_Vertical)
		{
			if (_count == 0)
			{
				TargetStyle_CheckBox = &Style->VerticalControlls.First;
			}
			else if (_count == DoneWidgets.Num() - 1)
			{
				TargetStyle_CheckBox = &Style->VerticalControlls.Last;
			}
			else
			{
				TargetStyle_CheckBox = &Style->VerticalControlls.Middle;
			}
		}
		else
		{
			if (_count == 0)
			{
				TargetStyle_CheckBox = &Style->HorizontalControlls.First;
			}
			else if (_count == DoneWidgets.Num() - 1)
			{
				TargetStyle_CheckBox = &Style->HorizontalControlls.Last;
			}
			else
			{
				TargetStyle_CheckBox = &Style->HorizontalControlls.Middle;
			}
		}

		auto CtrlWidget = SNew(SCheckBox)
			.Type(ESlateCheckBoxType::ToggleButton)
			.Style(TargetStyle_CheckBox)
			.OnCheckStateChanged(this, &SRadioButtonsBox::OnCheckStateChanged, static_cast<uint8>(_count))
			.IsChecked(this, &SRadioButtonsBox::GetCheckStateForIndex, static_cast<uint8>(_count))
			[
				MyWidget
			];

		if (Orientation == Orient_Vertical)
		{
			if (IsAutoSize)
			{
				VerticalBox->AddSlot().AutoHeight().Padding(ButtonsPadding)[CtrlWidget];
			}
			else
			{
				VerticalBox->AddSlot().Padding(ButtonsPadding)[CtrlWidget];
			}
		}
		else
		{
			if (IsAutoSize)
			{
				HorizontalBox->AddSlot().AutoWidth().Padding(ButtonsPadding)[CtrlWidget];
			}
			else
			{
				HorizontalBox->AddSlot().Padding(ButtonsPadding)[CtrlWidget];
			}
		}

		++_count;
	}

	if (Orientation == Orient_Vertical)
	{
		ChildSlot[VerticalBox];
	}
	else
	{
		ChildSlot[HorizontalBox];
	}
}
END_SLATE_FUNCTION_BUILD_OPTIMIZATION

void SRadioButtonsBox::AddButton(const FText& InContent, const int8& InIndex)
{
	if (InIndex == INDEX_NONE)
	{
		ButtonsText.Add(InContent);
	}
	else
	{
		ButtonsText.Insert(InContent, InIndex);
	}

	RebuildWidget();
}

void SRadioButtonsBox::AddButton(const TSharedRef<SWidget>& InContent, const int8& InIndex)
{
	if (InIndex == INDEX_NONE)
	{
		ButtonsWidget.Add(InContent);
	}
	else
	{
		ButtonsWidget.Insert(InContent, InIndex);
	}

	RebuildWidget();
}

void SRadioButtonsBox::SetActiveIndex(const uint8& InIndex)
{
	ActiveIndex = InIndex;
}

uint8 SRadioButtonsBox::GetActiveIndex() const
{
	return ActiveIndex;
}

bool SRadioButtonsBox::RemoveButton(const uint8& InIndex)
{
	if (ButtonsWidget.IsValidIndex(InIndex))
	{
		ButtonsWidget.RemoveAt(InIndex, 1, false);
		RebuildWidget();
		return true;
	}
	else if (ButtonsText.IsValidIndex(InIndex))
	{
		ButtonsText.RemoveAt(InIndex, 1, false);
		RebuildWidget();
		return true;
	}

	return false;
}

void SRadioButtonsBox::OnCheckStateChanged(ECheckBoxState InState, uint8 InIndex)
{
	ActiveIndex = InIndex;
	OnIndexChanged.ExecuteIfBound(ActiveIndex);
}

ECheckBoxState SRadioButtonsBox::GetCheckStateForIndex(uint8 InIndex) const
{
	return GetActiveIndex() == InIndex ? ECheckBoxState::Checked : ECheckBoxState::Unchecked;
}