// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "Widgets/SCompoundWidget.h"
#include "Styles/DiscountBlockWidgetStyle.h"

class UBasicItemEntity;
/**
 * 
 */
class SHOOTERGAME_API SDiscountBlock : public SCompoundWidget
{
public:
	SLATE_BEGIN_ARGS(SDiscountBlock)
		: _Style(&FShooterStyle::Get().GetWidgetStyle<FDiscountBlockStyle>("SDiscountBlockStyle"))
	{}
	SLATE_STYLE_ARGUMENT(FDiscountBlockStyle, Style)
	SLATE_ATTRIBUTE(UBasicItemEntity*, Product)
	SLATE_END_ARGS()

	void Construct(const FArguments& InArgs);

	void SetProduct(const TAttribute<UBasicItemEntity*>& InProduct);

protected:

	const FDiscountBlockStyle* Style;

	TAttribute<UBasicItemEntity*> Attr_Product;

	FText GetDiscountText() const;
	int64 GetDiscountTime() const;
};
