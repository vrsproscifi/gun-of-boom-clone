// Fill out your copyright notice in the Description page of Project Settings.

#include "ShooterGame.h"
#include "SArsenalItem.h"
#include "SlateOptMacros.h"
#include "SScaleBox.h"

#include "BasicPlayerItem.h"
#include "BasicItemEntity.h"

BEGIN_SLATE_FUNCTION_BUILD_OPTIMIZATION
void SArsenalItem::Construct(const FArguments& InArgs, TWeakObjectPtr<UBasicPlayerItem> InInstance)
{
	InstanceItem = InInstance;

	if (IsValidObject(InstanceItem) && InstanceItem->GetEntityBase())
	{
		SShopItem::Construct(SShopItem::FArguments().ActiveCoefficient(InArgs._ActiveCoefficient), InstanceItem->GetEntityBase());
	}
}
END_SLATE_FUNCTION_BUILD_OPTIMIZATION

TWeakObjectPtr<UBasicPlayerItem> SArsenalItem::GetInstance() const
{
	return GetValidObject(InstanceItem);
}

FText SArsenalItem::GetItemName() const
{
	if (IsValidObject(InstanceItem) && IsValidObject(GetInstanceBasic()))
	{
		if (InstanceItem->HasAllowSomeBought())
		{
			return FText::Format(FText::FromString("{0} <text font=\"Oswald.Medium\">x{1}</>"), SShopItem::GetItemName(), FText::AsNumber(InstanceItem->GetAmount(), &FNumberFormattingOptions::DefaultNoGrouping()));
		}
		else if (GetInstanceBasic()->GetItemProperty().Modifications.Num() > 0)
		{
			return FText::Format(FText::FromString("{0} <text font=\"Oswald.Medium\">{1}|{2}</>"), SShopItem::GetItemName(), FText::AsNumber(InstanceItem->GetLevel(), &FNumberFormattingOptions::DefaultNoGrouping()), FText::AsNumber(GetInstanceBasic()->GetItemProperty().Modifications.Num() - 1, &FNumberFormattingOptions::DefaultNoGrouping()));
		}
	}

	return SShopItem::GetItemName(); 
}
