// Fill out your copyright notice in the Description page of Project Settings.

#include "ShooterGame.h"
#include "SSphereProgress.h"
#include "SlateOptMacros.h"
#include "SlateMaterialBrush.h"

BEGIN_SLATE_FUNCTION_BUILD_OPTIMIZATION
void SSphereProgress::Construct(const FArguments& InArgs)
{
	Style = InArgs._Style;
	Attr_Progress = InArgs._Progress;
	Attr_TitleText = InArgs._Title;
	Attr_ProgressText = InArgs._ProgressText;
	Attr_Rotation = InArgs._Rotation;

	ProgressBackground = UMaterialInstanceDynamic::Create(Style->SourceMaterial, GetTransientPackage());
	ProgressBackgroundBrush = FSlateMaterialBrush(*ProgressBackground, FVector2D(128, 128));
	ProgressForeground = UMaterialInstanceDynamic::Create(Style->SourceMaterial, GetTransientPackage());
	ProgressForegroundBrush = FSlateMaterialBrush(*ProgressForeground, FVector2D(128, 128));

	ChildSlot
	[
		SNew(SBorder)
		.BorderImage(&ProgressBackgroundBrush)
		.Padding(0)
		[
			SNew(SBorder)
			.BorderImage(&ProgressForegroundBrush)
			.Padding(0)
			.HAlign(HAlign_Center)
			.VAlign(VAlign_Center)
			[
				SNew(SVerticalBox)
				+ SVerticalBox::Slot().AutoHeight().Padding(2)
				[
					SNew(STextBlock)
					.Justification(ETextJustify::Center)
					.TextStyle(&Style->TitleFont)
					.Text_Lambda([&]() { return Attr_TitleText.Get(); })
				]
				+ SVerticalBox::Slot().AutoHeight().Padding(6)
				[
					SNew(STextBlock)
					.Justification(ETextJustify::Center)
					.TextStyle(&Style->ProgressFont)
					.Text_Lambda([&]() { return Attr_ProgressText.Get(); })
				]
			]
		]
	];


}
END_SLATE_FUNCTION_BUILD_OPTIMIZATION

void SSphereProgress::AddReferencedObjects(FReferenceCollector& Collector)
{
	Collector.AddReferencedObject(ProgressBackground);
	Collector.AddReferencedObject(ProgressForeground);
}

void SSphereProgress::Tick(const FGeometry& AllottedGeometry, const double InCurrentTime, const float InDeltaTime)
{
	SCompoundWidget::Tick(AllottedGeometry, InCurrentTime, InDeltaTime);

	if (ProgressBackground && ProgressBackground->IsValidLowLevel() && ProgressForeground && ProgressForeground->IsValidLowLevel())
	{
		const auto Rotation = Attr_Rotation.Get();
		const auto Progress = Attr_Progress.Get();

		ProgressBackground->SetScalarParameterValue(Style->ParameterNameRotation, Rotation);
		ProgressForeground->SetScalarParameterValue(Style->ParameterNameRotation, Rotation);

		ProgressBackground->SetScalarParameterValue(Style->ParameterNameProgress, 1.0f);
		ProgressForeground->SetScalarParameterValue(Style->ParameterNameProgress, Progress);

		if (Style->CurveBackground)
		{
			ProgressBackground->SetVectorParameterValue(Style->ParameterNameColor, Style->CurveBackground->GetLinearColorValue(Progress));
		}

		if (Style->CurveForeground)
		{
			ProgressForeground->SetVectorParameterValue(Style->ParameterNameColor, Style->CurveForeground->GetLinearColorValue(Progress));
		}
	}
}
