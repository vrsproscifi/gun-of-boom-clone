// VRSPRO

#pragma once
#include "Item/GameItemCost.h"
#include "SResourceTextBoxType.h"

struct FResourceTextBoxValue
{
	int64 Value;
	EResourceTextBoxType::Type Type;
	EResourceTextBoxDisplayMethod DisplayMethod;

	FResourceTextBoxValue()
		: Value(-1)
		, Type(EResourceTextBoxType::Money)
		, DisplayMethod(EResourceTextBoxDisplayMethod::None)
	{
		
	}

	FResourceTextBoxValue(const EResourceTextBoxType::Type& InType) 
		: Value(0)
		, Type(InType)
		, DisplayMethod(EResourceTextBoxDisplayMethod::None)
	{
		
	}

	FResourceTextBoxValue(
			const EResourceTextBoxType::Type& InType,
			const int64& InValue, 
			const EResourceTextBoxDisplayMethod& InDisplayMethod = EResourceTextBoxDisplayMethod::None)
		: Value(InValue)
		, Type(InType) 
		, DisplayMethod(InDisplayMethod)
	{

	}

	FResourceTextBoxValue(const FGameItemCost& InCost)
		: Value(InCost.Amount)
		, Type(InCost.Currency == EGameCurrency::Money ? EResourceTextBoxType::Money : EResourceTextBoxType::Donate)
		, DisplayMethod(EResourceTextBoxDisplayMethod::None)
	{
		
	}

	static FResourceTextBoxValue Factory(const FGameItemCost& InCost);

	static FResourceTextBoxValue Money(const int64& InValue);
	static FResourceTextBoxValue Donate(const int64& InValue);
};

Expose_TNameOf(FResourceTextBoxValue)