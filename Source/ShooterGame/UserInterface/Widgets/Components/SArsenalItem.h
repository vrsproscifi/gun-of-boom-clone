// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "SShopItem.h"

class UBasicPlayerItem;
/**
 * 
 */
class SHOOTERGAME_API SArsenalItem : public SShopItem
{
public:
	SLATE_BEGIN_ARGS(SArsenalItem)
	{}
	SLATE_ATTRIBUTE(float, ActiveCoefficient)
	SLATE_END_ARGS()

	void Construct(const FArguments& InArgs, TWeakObjectPtr<UBasicPlayerItem> InInstance);

	TWeakObjectPtr<UBasicPlayerItem> GetInstance() const;

protected:

	TWeakObjectPtr<UBasicPlayerItem> InstanceItem;

	virtual FText GetItemName() const override;
};
