// Fill out your copyright notice in the Description page of Project Settings.

#include "ShooterGame.h"
#include "SVerticalMenuBuilder.h"
// Blur
#include "SBackgroundBlur.h"
#include "Utilities/ToggableWidgetHelper.h"
#include "Utilities/SAnimatedBackground.h"

BEGIN_SLATE_FUNCTION_BUILD_OPTIMIZATION
void SVerticalMenuBuilder::Construct(const FArguments& InArgs)
{
	Style = InArgs._Style;
	ButtonsStyle = (InArgs._ButtonsStyle == nullptr) ? &Style->ButtonsStyle : InArgs._ButtonsStyle; 
	ButtonsTextStyle = (InArgs._ButtonsTextStyle == nullptr) ? &Style->ButtonsTextStyle : InArgs._ButtonsTextStyle;

	OnClickAnyButton = InArgs._OnClickAnyButton;

	bIsDoubleClickProtection = InArgs._IsDoubleClickProtection;

	ChildSlot
	[
		SAssignNew(Anim_Back, SAnimatedBackground)
		.Visibility(EVisibility::Visible)
		.BlurStyle(InArgs._BlurStyle)
		.IsEnabledBlur(true)
		.Duration(Style->AnimationTransform.Y)
		.ShowAnimation(EAnimBackAnimation::Color)
		.HideAnimation(EAnimBackAnimation::Color)
		.InitAsHide(true)
		.Padding(0)
		.HAlign(HAlign_Center)
		.VAlign(VAlign_Center)
		[
			SNew(SBorder)
			.Padding(0)
			.BorderImage(&Style->Background)
			.RenderTransformPivot(FVector2D(0.5, 0.5))
			.RenderTransform(this, &SVerticalMenuBuilder::WidgetScale)
			.BorderBackgroundColor(this, &SVerticalMenuBuilder::WidgetContainerColor)
			[
				SNew(SBorder)
				.Padding(0)
				.BorderImage(&Style->Border)
				.BorderBackgroundColor(this, &SVerticalMenuBuilder::WidgetBorderColor)
				[
					SNew(SVerticalBox)
					+ SVerticalBox::Slot()
					.AutoHeight()
					[
						SNew(SBox)
						.HeightOverride(40)
						[
							SNew(SBorder)
							.BorderImage(&Style->SubBorder)
							.BorderBackgroundColor(this, &SVerticalMenuBuilder::WidgetBordersColor)
						]
					]
					+ SVerticalBox::Slot()
					.FillHeight(1)
					.Padding(FMargin(Style->ContainerButtonsPadding.X, Style->ContainerButtonsPadding.Y))
					[
						SAssignNew(ContainerButtons, SVerticalBox)
					]
					+ SVerticalBox::Slot()
					.AutoHeight()
					[
						SNew(SBox)
						.HeightOverride(40)
						[
							SNew(SBorder)
							.RenderTransformPivot(FVector2D(0.5, 0.5))
							.RenderTransform(FSlateRenderTransform(FScale2D(-1.0f), FVector2D::ZeroVector))
							.BorderImage(&Style->SubBorder)
							.BorderBackgroundColor(this, &SVerticalMenuBuilder::WidgetBordersColor)
						]
					]
				]
			]
		]
	];

	SetButtons(InArgs._ButtonsText);
	
	ShowTransHandle = ShowAnim.AddCurve(Style->AnimationTransform.X, Style->AnimationTransform.Y, ECurveEaseFunction::QuadInOut);
	ShowContainerColorHandle = ShowAnim.AddCurve(Style->AnimationContainerColor.X, Style->AnimationContainerColor.Y, ECurveEaseFunction::QuadInOut);
	ShowBorderColorHandle = ShowAnim.AddCurve(Style->AnimationBorderColor.X, Style->AnimationBorderColor.Y, ECurveEaseFunction::QuadOut);
	ShowButtonsColorHandle = ShowAnim.AddCurve(Style->AnimationButtons.X, Style->AnimationButtons.Y, ECurveEaseFunction::QuadIn);
	ShowBordersColorHandle = ShowAnim.AddCurve(Style->AnimationSubBorders.X, Style->AnimationSubBorders.Y, ECurveEaseFunction::Linear);
}
END_SLATE_FUNCTION_BUILD_OPTIMIZATION

void SVerticalMenuBuilder::SetButtons(const TArray<FText>& InButtons)
{
	ContainerButtons->ClearChildren();
	Buttons.Empty();	

	SIZE_T _count = 0;
	for (auto _buttonText : InButtons)
	{
		TSharedPtr<SButton> _button;

		ContainerButtons->AddSlot().Padding(FMargin(Style->ButtonsPadding.X, Style->ButtonsPadding.Y))
		[
			SAssignNew(_button, SButton)
			.ButtonStyle(ButtonsStyle)
			.ButtonColorAndOpacity(this, &SVerticalMenuBuilder::WidgetButtonsColor)
			.OnClicked(this, &SVerticalMenuBuilder::OnClickMenuButtonSender, _count)
			[
				SNew(SBox)
				.WidthOverride((FMath::IsNearlyEqual(Style->ButtonsSize.X, 0)) ? FOptionalSize() : Style->ButtonsSize.X)
				.HeightOverride((FMath::IsNearlyEqual(Style->ButtonsSize.Y, 0)) ? FOptionalSize() : Style->ButtonsSize.Y)
				.Padding(FMargin(Style->ButtonsTextPadding.X, Style->ButtonsTextPadding.Y))
				.HAlign(EHorizontalAlignment::HAlign_Fill)
				.VAlign(EVerticalAlignment::VAlign_Fill)
				[
					SNew(STextBlock)
					.ColorAndOpacity(this, &SVerticalMenuBuilder::WidgetButtonsTextColor)
					.TextStyle(ButtonsTextStyle)
					.Text(_buttonText)
					.Justification(ETextJustify::Center)
				]
			]
		];

		Buttons.Add(_button);

		++_count;
	}
}

bool SVerticalMenuBuilder::IsVisible() const
{
	return ShowAnim.IsAtEnd();
}

FReply SVerticalMenuBuilder::OnClickMenuButtonSender(const SIZE_T Index)
{
	if (Buttons.IsValidIndex(Index))
	{
		if (bIsDoubleClickProtection)
		{
			Buttons[Index]->SetEnabled(false);
		}

		if (OnClickAnyButton.IsBound())
		{
			OnClickAnyButton.Execute(Index);
		}

		return FReply::Handled();
	}

	return FReply::Unhandled();
}

void SVerticalMenuBuilder::ToggleWidget(const bool InToggle)
{
	SUsableCompoundWidget::ToggleWidget(InToggle);

	Anim_Back->ToggleWidget(InToggle);
	FToggableWidgetHelper::ToggleWidget(this->AsShared(), ShowAnim, InToggle);
	FSlateApplication::Get().PlaySound(InToggle ? Style->SoundOpen : Style->SoundClose);

	if (InToggle)
	{
		EnableButtons();
	}
}

void SVerticalMenuBuilder::EnableButtons()
{
	for (auto _button : Buttons)
	{
		_button->SetEnabled(true);
	}
}

TOptional<FSlateRenderTransform> SVerticalMenuBuilder::WidgetScale() const
{
	return FSlateRenderTransform(FScale2D(1.0f, ShowTransHandle.GetLerp()), FVector2D::ZeroVector);
}

FSlateColor SVerticalMenuBuilder::WidgetContainerColor() const
{
	return FSlateColor(FLinearColor(1, 1, 1, ShowContainerColorHandle.GetLerp()));
}

FSlateColor SVerticalMenuBuilder::WidgetBorderColor() const
{
	return FSlateColor(FLinearColor(1, 1, 1, ShowBorderColorHandle.GetLerp()));
}

FSlateColor SVerticalMenuBuilder::WidgetBordersColor() const
{
	return FSlateColor(FLinearColor(1, 1, 1, ShowBordersColorHandle.GetLerp()));
}

FSlateColor SVerticalMenuBuilder::WidgetButtonsColor() const
{
	return FSlateColor(FLinearColor(1, 1, 1, ShowButtonsColorHandle.GetLerp()));
}

FSlateColor SVerticalMenuBuilder::WidgetButtonsTextColor() const
{
	return FSlateColor(FLinearColor(ButtonsTextStyle->ColorAndOpacity.GetSpecifiedColor().R, ButtonsTextStyle->ColorAndOpacity.GetSpecifiedColor().G, ButtonsTextStyle->ColorAndOpacity.GetSpecifiedColor().B, ShowButtonsColorHandle.GetLerp()));
}