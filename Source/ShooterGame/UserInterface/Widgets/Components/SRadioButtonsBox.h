// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "Widgets/SCompoundWidget.h"
#include "Styles/TabbedContainerWidgetStyle.h"

/**
 * 
 */
class SHOOTERGAME_API SRadioButtonsBox : public SCompoundWidget
{
public:
	SLATE_BEGIN_ARGS(SRadioButtonsBox)
		: _Style(&FShooterStyle::Get().GetWidgetStyle<FTabbedContainerStyle>("SRadioButtonsBoxStyle"))
		, _Orientation(Orient_Horizontal)
		, _IsAutoSize(false)
	{
		_Visibility = EVisibility::SelfHitTestInvisible;
	}
	SLATE_STYLE_ARGUMENT(FTabbedContainerStyle, Style)
	SLATE_ARGUMENT(uint8, ActiveIndex)
	SLATE_ARGUMENT(EOrientation, Orientation)
	SLATE_ARGUMENT(TArray<FText>, ButtonsText)
	SLATE_ARGUMENT(TArray<TSharedPtr<SWidget>>, ButtonsWidget)
	SLATE_ARGUMENT(FMargin, ButtonsPadding)
	SLATE_ARGUMENT(bool, IsAutoSize)
	SLATE_EVENT(FOnInt32ValueChanged, OnIndexChanged)
	SLATE_END_ARGS()

	void Construct(const FArguments& InArgs);

	void AddButton(const FText& InContent, const int8& InIndex = INDEX_NONE);
	void AddButton(const TSharedRef<SWidget>& InContent, const int8& InIndex = INDEX_NONE);

	void SetActiveIndex(const uint8& InIndex);
	uint8 GetActiveIndex() const;

	bool RemoveButton(const uint8& InIndex);

protected:

	const FTabbedContainerStyle* Style;

	FMargin ButtonsPadding;
	bool IsAutoSize;

	EOrientation Orientation;
	uint8 ActiveIndex;

	TArray<FText> ButtonsText;
	TArray<TSharedPtr<SWidget>> ButtonsWidget;

	FOnInt32ValueChanged OnIndexChanged;

	void RebuildWidget();

	void OnCheckStateChanged(ECheckBoxState InState, uint8 InIndex);
	ECheckBoxState GetCheckStateForIndex(uint8 InIndex) const;
};
