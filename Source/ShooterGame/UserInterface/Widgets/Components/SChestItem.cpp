// Fill out your copyright notice in the Description page of Project Settings.

#include "ShooterGame.h"
#include "SChestItem.h"
#include "SlateOptMacros.h"
#include "Decorators/LokaTextDecorator.h"
#include "Decorators/LokaResourceDecorator.h"
#include "SScaleBox.h"
#include "UObjectExtensions.h"
#include "Entity/PlayerCaseEntity.h"
#include "Case/CaseEntity.h"
#include "DateTimeExtensions.h"

FSlateBrush SChestItem::EmptyBrush = FSlateNoResource();

BEGIN_SLATE_FUNCTION_BUILD_OPTIMIZATION
void SChestItem::Construct(const FArguments& InArgs, const TWeakObjectPtr<UPlayerCaseEntity>& InInstance)
{
	Instance = InInstance;
	Style = InArgs._Style;
	OnClickedChest = InArgs._OnClickedChest;

	ChildSlot.Padding(4, 2)
	[
		SNew(SButton)
		.Visibility(this, &SChestItem::GetVisibilityChest)
		.ButtonStyle(&Style->ButtonItem)
		.OnClicked(this, &SChestItem::OnClicked)
		.ContentPadding(FMargin(0))
		[
			SNew(SBox).WidthOverride(360)
			[
				SNew(SVerticalBox)
				+ SVerticalBox::Slot()
				.AutoHeight()
				[
					SNew(SBorder).Padding(0).BorderImage(this, &SChestItem::GetHeaderBackground)
					[
						SNew(SRichTextBlock)
						.Margin(FMargin(0, 5))
						.Justification(ETextJustify::Center)
						.Text(this, &SChestItem::GetHeaderText)
						.TextStyle(&Style->DisallowItem.HeaderFont)
						+ SRichTextBlock::Decorator(FLokaTextDecorator::Create(Style->DisallowItem.HeaderFont))
						+ SRichTextBlock::Decorator(FLokaResourceDecorator::Create())
					]
				]
				+ SVerticalBox::Slot()
				[
					SNew(SBorder).Padding(0).BorderImage(this, &SChestItem::GetContentBackground)
					[
						SNew(SOverlay)
						+ SOverlay::Slot()
						[
							SNew(SScaleBox)
							.Stretch(EStretch::ScaleToFit)
							.Visibility(EVisibility::HitTestInvisible)
							[
								SNew(SImage)
								.Image(this, &SChestItem::GetItemImage)
							]
						]
						+ SOverlay::Slot().VAlign(VAlign_Bottom).Padding(0, 10)
						[
							SNew(SRichTextBlock)
							.Justification(ETextJustify::Center)
							.Text(this, &SChestItem::GetContentText)
							.TextStyle(&Style->DisallowItem.ContentFont)
							+ SRichTextBlock::Decorator(FLokaTextDecorator::Create(Style->DisallowItem.ContentFont))
							+ SRichTextBlock::Decorator(FLokaResourceDecorator::Create())
						]
					]
				]
			]
		]
	];
}
END_SLATE_FUNCTION_BUILD_OPTIMIZATION

FReply SChestItem::OnClicked()
{
	OnClickedChest.ExecuteIfBound(Instance);
	return FReply::Handled();
}

FText SChestItem::GetHeaderText() const
{
	if (IsValidObject(Instance))
	{
		if (Instance->IsEveryDayCase())
		{
			if (Instance->IsElapsedLastActive())
			{
				return NSLOCTEXT("SPlayerCase", "SPlayerCase.Free", "FREE");
			}

			return FDateTimeExtensions::AsFormatedTimespan(Instance->GetWaitingTime());
		}

		return FText::Format(FText::FromString("x{0}"), FText::AsNumber(Instance->GetAmount(), &FNumberFormattingOptions::DefaultNoGrouping()));
	}

	return FText::GetEmpty();
}

FText SChestItem::GetContentText() const
{
	if (IsValidObject(Instance))
	{
		return Instance->GetCaseEntityProperty()->Title;
	}

	return FText::GetEmpty();
}

const FSlateBrush* SChestItem::GetHeaderBackground() const
{
	if (IsValidObject(Instance) && (Instance->IsEveryDayCase() && Instance->IsElapsedLastActive()) || (Instance->AnyAmount() && !Instance->IsEveryDayCase()))
	{
		return &Style->AllowItem.Header;
	}

	return &Style->DisallowItem.Header;
}

const FSlateBrush* SChestItem::GetContentBackground() const
{
	if (IsValidObject(Instance) && (Instance->IsEveryDayCase() && Instance->IsElapsedLastActive()) || (Instance->AnyAmount() && !Instance->IsEveryDayCase()))
	{
		return &Style->AllowItem.Content;
	}

	return &Style->DisallowItem.Content;
}

const FSlateBrush* SChestItem::GetItemImage() const
{
	if (IsValidObject(Instance))
	{
		return &Instance->GetCaseEntityProperty()->Image;
	}

	return &EmptyBrush;
}

EVisibility SChestItem::GetVisibilityChest() const
{
	if (IsValidObject(Instance))
	{
		if (Instance->IsEveryDayCase() || Instance->AnyAmount())
		{
			return EVisibility::Visible;
		}
	}

	return EVisibility::Collapsed;
}
