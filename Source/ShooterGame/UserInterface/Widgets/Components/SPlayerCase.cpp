// Fill out your copyright notice in the Description page of Project Settings.

#include "ShooterGame.h"
#include "SPlayerCase.h"
#include "SlateOptMacros.h"
#include "SScaleBox.h"

#include "Entity/PlayerCaseEntity.h"
#include "CaseProperty.h"
#include "SRetainerWidget.h"
#include "DateTimeExtensions.h"


BEGIN_SLATE_FUNCTION_BUILD_OPTIMIZATION
void SPlayerCase::Construct(const FArguments& InArgs, TWeakObjectPtr<UPlayerCaseEntity> InPlayerCaseEntity)
{
	PlayerCaseEntity	= InPlayerCaseEntity;
	Style				= InArgs._Style;
	OnOpenPlayerCase	= InArgs._OnOpenPlayerCase;

	ChildSlot
	[
		SNew(SRetainerWidget)
		.Phase(0)
		.PhaseCount(10)
		.RenderOnPhase(true)
		.RenderOnInvalidation(false)
		[
			SNew(SBox).WidthOverride(150).HeightOverride(this, &SPlayerCase::GetHeight)
			[
				SNew(SOverlay)
				+ SOverlay::Slot()
				[
					SNew(SImage)
					.Image(&Style->Background)
				]
				+ SOverlay::Slot()
				[
					SNew(SButton)
					.ButtonStyle(&Style->Button)
					.OnClicked(this, &SPlayerCase::OnOpenPlayerCaseClicked)
					[
						SNew(SVerticalBox)
						+ SVerticalBox::Slot()
						[
							SNew(SOverlay)
							+ SOverlay::Slot()
							[
								SNew(SScaleBox)
								.Stretch(EStretch::ScaleToFit)
								[
									SNew(SImage)
									.Image(&PlayerCaseEntity->GetCaseEntityProperty()->Image)
								]
							]
							+ SOverlay::Slot().VAlign(VAlign_Bottom).HAlign(HAlign_Fill)
							[
								SNew(SBorder)
								.Visibility(EVisibility::HitTestInvisible)
								.Padding(FMargin(4, 0))
								.BorderImage(this, &SPlayerCase::GetDeliveryBackground)
								[
									SNew(STextBlock)
									.AutoWrapText(true)
									.Clipping(EWidgetClipping::Inherit)
									.Justification(ETextJustify::Center)
									.Text(this, &SPlayerCase::GetDeliveryText)
									.Font(this, &SPlayerCase::GetDeliveryFont)
									.ColorAndOpacity(this, &SPlayerCase::GetDeliveryColor)
								]
							]
						]
						+ SVerticalBox::Slot().AutoHeight().HAlign(HAlign_Fill)
						[
							SNew(SBorder)
							.Visibility(EVisibility::HitTestInvisible)
							.Padding(FMargin(4, 0))
							.BorderImage(&Style->BackgroundDown)
							[
								SNew(STextBlock)
								.AutoWrapText(true)
								.Clipping(EWidgetClipping::Inherit)
								.TextStyle(&Style->TextTitle)
								.Justification(ETextJustify::Center)
								.Text_Lambda([i = PlayerCaseEntity]()
								{
									if (IsValidObject(i))
									{
										return i->GetCaseEntityProperty()->Title;
									}
									return FText::GetEmpty();
								})
							]
						]
					]
				]
			]
		]
	];

	SetClipping(EWidgetClipping::Inherit);

	SetVisibility(TAttribute<EVisibility>::Create(TAttribute<EVisibility>::FGetter::CreateLambda([&]()
	{
		return (IsValidObject(PlayerCaseEntity) && PlayerCaseEntity->AnyAmount()) || PlayerCaseEntity->IsEveryDayCase() ? EVisibility::SelfHitTestInvisible : EVisibility::Collapsed;
	})));
}
END_SLATE_FUNCTION_BUILD_OPTIMIZATION

FText SPlayerCase::GetDeliveryText() const
{
	if (const auto entity = GetCaseEntity())
	{
		if (entity->IsEveryDayCase())
		{
			if (entity->IsElapsedLastActive())
			{
				return NSLOCTEXT("SPlayerCase", "SPlayerCase.Free", "FREE");
			}

			return FDateTimeExtensions::AsFormatedTimespan(entity->GetWaitingTime());
		}

		return FText::Format(FText::FromString("x{0}"), FText::AsNumber(entity->GetAmount(), &FNumberFormattingOptions::DefaultNoGrouping()));
	}

	return FText::GetEmpty();
}

FSlateFontInfo SPlayerCase::GetDeliveryFont() const
{
	if (const auto entity = GetCaseEntity())
	{
		if (entity->IsEveryDayCase() && entity->IsElapsedLastActive())
		{
			return Style->TextGetFree.Font;
		}
	}

	return Style->TextTime.Font;
}

FSlateColor SPlayerCase::GetDeliveryColor() const
{
	if (const auto entity = GetCaseEntity())
	{
		if (entity->IsEveryDayCase() && entity->IsElapsedLastActive())
		{
			return Style->TextGetFree.ColorAndOpacity;
		}
	}

	return Style->TextTime.ColorAndOpacity;
}

const FSlateBrush* SPlayerCase::GetDeliveryBackground() const
{
	if (const auto entity = GetCaseEntity())
	{
		if (entity->IsEveryDayCase() && entity->IsElapsedLastActive())
		{
			return &Style->BackgroundUpGetFree;
		}
	}

	return &Style->BackgroundUp;
}

FOptionalSize SPlayerCase::GetHeight() const
{
	if (const auto entity = GetCaseEntity())
	{
		if (entity->IsEveryDayCase() && entity->IsElapsedLastActive())
		{
			return Style->Height.Y;
		}
	}

	return Style->Height.X;
}

FReply SPlayerCase::OnOpenPlayerCaseClicked() const
{
	if (const auto entity = GetCaseEntity())
	{
		OnOpenPlayerCase.ExecuteIfBound(entity->GetEntityId());
	}

	return FReply::Handled();
}

const UPlayerCaseEntity* SPlayerCase::GetCaseEntity() const
{
	return GetValidObject(PlayerCaseEntity);
}

const FCaseProperty* SPlayerCase::GetCaseEntityProperty() const
{
	if(const auto entity = GetCaseEntity())
	{
		return entity->GetCaseEntityProperty();
	}
	return nullptr;
}
