// VRSPRO

#pragma once

#include "Widgets/SCompoundWidget.h"
#include "Styles/ResourceTextBoxWidgetStyle.h" 
#include "SResourceTextBoxValue.h"


class SResourceTextBox : public SCompoundWidget
{
	static FNumberFormattingOptions CurrencyFormattingOptions;

public:
	SLATE_BEGIN_ARGS(SResourceTextBox)
		: _Style(&FShooterStyle::Get().GetWidgetStyle<FResourceTextBoxStyle>("SResourceTextBoxStyle"))
		, _Type(EResourceTextBoxType::Money)
		, _Value(0)
		, _CustomSize(FVector::ZeroVector)
		, _TextVAlign(VAlign_Center)
		, _TextHAlign(HAlign_Fill)
		, _ImageVAlign(VAlign_Center)
		, _ImageHAlign(HAlign_Center)
	{}

	SLATE_STYLE_ARGUMENT(FResourceTextBoxStyle, Style)

	// * Value type and amount pair
	SLATE_ATTRIBUTE(FResourceTextBoxValue, TypeAndValue)

	// * Value type
	SLATE_ATTRIBUTE(EResourceTextBoxType::Type, Type)
	

	SLATE_ATTRIBUTE(EResourceTextBoxDisplayMethod, DisplayMethod)
	SLATE_ATTRIBUTE(EResourceTextBoxOverlayMethod, OverlayMethod)

	// * Value amount
	SLATE_ATTRIBUTE(int64, Value)

	// * X,Y - Image size, Z - Font size
	SLATE_ARGUMENT(FVector, CustomSize)	
	
	SLATE_ARGUMENT(EVerticalAlignment, TextVAlign)
	SLATE_ARGUMENT(EHorizontalAlignment, TextHAlign)

	SLATE_ARGUMENT(EVerticalAlignment, ImageVAlign)
	SLATE_ARGUMENT(EHorizontalAlignment, ImageHAlign)
	SLATE_ATTRIBUTE(float, MinDesiredWidth)


	SLATE_EVENT(FOnClicked, OnClicked)
	SLATE_END_ARGS()

	void Construct(const FArguments& InArgs);	

	void SetTypeAndValue(const TAttribute<FResourceTextBoxValue>& InTypeAndValueAttr);
	void SetType(const TAttribute<EResourceTextBoxType::Type>& InTypeAttr);
	void SetValue(const TAttribute<int64>& InValueAttr);
	void SetValue(const FText& InCustomText);	

	void SetDisplayMethod(const TAttribute<EResourceTextBoxDisplayMethod>& InDisplayMethod);
	void SetOverlayMethod(const TAttribute<EResourceTextBoxOverlayMethod>& InOverlayMethod);

	const int64 GetValue() const;
	const EResourceTextBoxType::Type GetValueType() const;
	const EResourceTextBoxDisplayMethod GetDisplayMethod() const;

	void SetCustomSize(const FVector& = FVector::ZeroVector);

protected:

	const FRTBStyles* GetResourceStyle() const;

	TAttribute<FResourceTextBoxValue> Attr_TypeAndValue;
	TAttribute<EResourceTextBoxType::Type> Attr_ValueType;

	TAttribute<EResourceTextBoxDisplayMethod> Attr_DisplayMethod;
	TAttribute<EResourceTextBoxOverlayMethod> Attr_OverlayMethod;
	TAttribute<int64> Attr_ValueAmount;

	TSharedPtr<SImage> Image; 
	TSharedPtr<STextBlock> TextBlock;

	FResourceTextBoxStyle *Style;
	FOnClicked OnClicked;

	FReply OnClickedOnImage(const FGeometry& InGeometry, const FPointerEvent& InEvent);

	FText GetCurrentValue() const;
	FText GetCurrentValueAsTime() const;
	const FSlateBrush* GetCurrentImage() const;
	const FSlateBrush* GetOverlayImage() const;
	FSlateFontInfo GetCurrentFont() const;
	FSlateColor GetCurrentFontColor() const;

	EVisibility GetCurrentVisibilityText() const;
};
