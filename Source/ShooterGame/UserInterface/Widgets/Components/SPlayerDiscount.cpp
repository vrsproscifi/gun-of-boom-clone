// Fill out your copyright notice in the Description page of Project Settings.

#include "ShooterGame.h"
#include "SPlayerDiscount.h"
#include "SlateOptMacros.h"
#include "SRetainerWidget.h"
#include "SScaleBox.h"
#include "Entity/PlayerDiscountEntity.h"
#include "SPlayerCase.h"
#include "ProductItemEntity.h"
#include "Decorators/LokaResourceDecorator.h"
#include "Decorators/LokaTextDecorator.h"
#include "DateTimeExtensions.h"
#include "Localization/TableWeaponStrings.h"

BEGIN_SLATE_FUNCTION_BUILD_OPTIMIZATION
void SPlayerDiscount::Construct(const FArguments& InArgs, TWeakObjectPtr<UPlayerDiscountEntity> InPlayerDiscountEntity)
{
	PlayerDiscountEntity = InPlayerDiscountEntity;
	Style = InArgs._Style;

	ChildSlot
	[
		SNew(SRetainerWidget)
		.Phase(0)
		.PhaseCount(10)
		.RenderOnPhase(true)
		.RenderOnInvalidation(false)
		[
			SNew(SBox).WidthOverride(150).HeightOverride(150)
			[
				SNew(SOverlay)
				+ SOverlay::Slot()
				[
					SNew(SImage)
					.Image(&Style->Background)
				]
				+ SOverlay::Slot()
				[
					SNew(SButton)
					.ButtonStyle(&Style->Button)
					[
						SNew(SScaleBox)
						.Stretch(EStretch::ScaleToFit)
						[
							SNew(SImage)
							.Image(this, &SPlayerDiscount::GetDiscountImage)
						]
					]
					.OnClicked_Lambda([&, e = InArgs._OnClicked]()
					{
						if (auto MyDiscount = GetValidObject(PlayerDiscountEntity))
						{
							if (auto ProductTarget = MyDiscount->GetEntity<UBasicItemEntity>())
							{
								e.ExecuteIfBound(ProductTarget);
							}
						}
						
						return FReply::Handled();
					})
				]
				+ SOverlay::Slot().VAlign(VAlign_Top).HAlign(HAlign_Fill)
				[
					SNew(SBorder)
					.Visibility(EVisibility::HitTestInvisible)
					.Padding(FMargin(10, 4))
					.BorderImage(&Style->BackgroundUp)
					[
						SNew(STextBlock)
						.AutoWrapText(true)
						.Clipping(EWidgetClipping::Inherit)
						.Justification(ETextJustify::Center)
						.Text(this, &SPlayerDiscount::GetDiscountAvalibleTime)
						.TextStyle(&Style->TextTime)
					]
				]
				+ SOverlay::Slot().VAlign(VAlign_Bottom).HAlign(HAlign_Fill)
				[
					/*SNew(SVerticalBox)
					+ SVerticalBox::Slot().AutoHeight()
					[*/
						/*SNew(SBorder)
						.Visibility(EVisibility::HitTestInvisible)
						.Padding(FMargin(10, 4))
						.BorderImage(&Style->BackgroundDown)
						[
							SNew(STextBlock)
							.AutoWrapText(true)
							.Clipping(EWidgetClipping::Inherit)
							.TextStyle(&Style->TextTitle)
							.Justification(ETextJustify::Center)
							.Text(this, &SPlayerDiscount::GetDiscountTargetText)
						]*/
					//]
					/*+ SVerticalBox::Slot().AutoHeight()
					[*/
						SNew(SBorder)
						.Visibility(EVisibility::HitTestInvisible)
						.Padding(FMargin(10, 4))
						.BorderImage(&Style->BackgroundUpGetFree)
						[
							SNew(STextBlock)
							.AutoWrapText(true)
							.Clipping(EWidgetClipping::Inherit)
							.Justification(ETextJustify::Center)
							.Text(NSLOCTEXT("SDiscountBlock", "SDiscountBlock.Discount2", "Discount"))							
							.TextStyle(&Style->TextGetFree)
						]
					//]
				]
			]
		]
	];

	SetClipping(EWidgetClipping::Inherit);
}
END_SLATE_FUNCTION_BUILD_OPTIMIZATION

FText SPlayerDiscount::GetDiscountAvalibleTime() const
{
	if (auto MyDiscount = GetValidObject(PlayerDiscountEntity))
	{
		return FDateTimeExtensions::AsFormatedTimespan(MyDiscount->GetAvalibleTime());
	}
	return FText::GetEmpty();
}

FText SPlayerDiscount::GetDiscountTargetText() const
{
	if (auto MyDiscount = GetValidObject(PlayerDiscountEntity))
	{
		if (auto ProductTarget = MyDiscount->GetEntity<UBaseProductItemEntity>())
		{
			return FTableItemStrings::GetProductText(ProductTarget->GetBaseProductProperty().ProductCategory);
		}
		else if (auto ItemTarget = MyDiscount->GetEntity<UBasicItemEntity>())
		{
			return FTableItemStrings::GetCategoryText(ItemTarget->GetItemProperty().Type);
		}	
	}

	return FText::GetEmpty();
}

const FSlateBrush* SPlayerDiscount::GetDiscountImage() const
{
	if (auto MyDiscount = GetValidObject(PlayerDiscountEntity))
	{
		if (auto ProductTarget = MyDiscount->GetEntity<UBasicItemEntity>())
		{
			return &ProductTarget->GetItemProperty().Image;
		}
	}

	static FSlateBrush EmptyBrush = FSlateNoResource();
	return &EmptyBrush;
}
