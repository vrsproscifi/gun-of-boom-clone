#include "ShooterGame.h"
#include "SResourceTextBoxValue.h"
#include "SResourceTextBoxType.h"

FResourceTextBoxValue FResourceTextBoxValue::Money(const int64& InValue)
{
	return FResourceTextBoxValue(EResourceTextBoxType::Money, InValue);
}

FResourceTextBoxValue FResourceTextBoxValue::Donate(const int64& InValue)
{
	return FResourceTextBoxValue(EResourceTextBoxType::Donate, InValue);
}

FResourceTextBoxValue FResourceTextBoxValue::Factory(const FGameItemCost& InCost)
{
	switch (InCost.Currency)
	{
		case EGameCurrency::BattleCoins:	return FResourceTextBoxValue(EResourceTextBoxType::BattleCoins, InCost.Amount, EResourceTextBoxDisplayMethod::Numeric);
		case EGameCurrency::Donate:			return FResourceTextBoxValue(EResourceTextBoxType::Donate, InCost.Amount, EResourceTextBoxDisplayMethod::Numeric);
		case EGameCurrency::Money:			return FResourceTextBoxValue(EResourceTextBoxType::Money, InCost.Amount, EResourceTextBoxDisplayMethod::Numeric);

		case EGameCurrency::RUB:	return FResourceTextBoxValue(EResourceTextBoxType::RUB, InCost.Amount, EResourceTextBoxDisplayMethod::Currency);
		case EGameCurrency::USD:	return FResourceTextBoxValue(EResourceTextBoxType::USD, InCost.Amount, EResourceTextBoxDisplayMethod::Currency);
		case EGameCurrency::EUR:	return FResourceTextBoxValue(EResourceTextBoxType::EUR, InCost.Amount, EResourceTextBoxDisplayMethod::Currency);
		case EGameCurrency::UAH:	return FResourceTextBoxValue(EResourceTextBoxType::UAH, InCost.Amount, EResourceTextBoxDisplayMethod::Currency);
		case EGameCurrency::KZT:	return FResourceTextBoxValue(EResourceTextBoxType::KZT, InCost.Amount, EResourceTextBoxDisplayMethod::Currency);
	}
	return FResourceTextBoxValue::Money(0);
}
