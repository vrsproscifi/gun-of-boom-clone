// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "Widgets/SCompoundWidget.h"
#include "Styles/PlayerCaseWidgetStyle.h"

class UBasicItemEntity;
class UPlayerDiscountEntity;

DECLARE_DELEGATE_OneParam(FOnProductAction, UBasicItemEntity*);
/**
 * 
 */
class SHOOTERGAME_API SPlayerDiscount : public SCompoundWidget
{
public:
	SLATE_BEGIN_ARGS(SPlayerDiscount)
		: _Style(&FShooterStyle::Get().GetWidgetStyle<FPlayerCaseStyle>("SPlayerCaseStyle"))
	{}
	SLATE_STYLE_ARGUMENT(FPlayerCaseStyle, Style)
	SLATE_EVENT(FOnProductAction, OnClicked)
	SLATE_END_ARGS()

	void Construct(const FArguments& InArgs, TWeakObjectPtr<UPlayerDiscountEntity> InPlayerDiscountEntity);

protected:

	const FPlayerCaseStyle* Style;

	TWeakObjectPtr<UPlayerDiscountEntity> PlayerDiscountEntity;

	FText GetDiscountAvalibleTime() const;
	FText GetDiscountTargetText() const;
	const FSlateBrush* GetDiscountImage() const;
};
