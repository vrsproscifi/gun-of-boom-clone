// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "Utilities/SlateUtilityTypes.h"
#include "Utilities/SUsableCompoundWidget.h"
#include "Styles/CurrencyBlockWidgetStyle.h"

class UArsenalComponent;
class UIdentityComponent;
enum class EGameCurrency : uint8;
class SAnimatedBackground;

DECLARE_DELEGATE_OneParam(FOnClickedCash, EGameCurrency);

class SHOOTERGAME_API SCurrencyBlock 
	: public SCompoundWidget
	, public TStaticSlateWidget<SCurrencyBlock>
{
public:
	SLATE_BEGIN_ARGS(SCurrencyBlock)
		: _Style(&FShooterStyle::Get().GetWidgetStyle<FCurrencyBlockStyle>("SCurrencyBlockStyle"))
	{
		_Visibility = EVisibility::SelfHitTestInvisible;
	}
	SLATE_STYLE_ARGUMENT(FCurrencyBlockStyle, Style)
	SLATE_END_ARGS()

	void Construct(const FArguments& InArgs);

	void SetIdentityComponent(UIdentityComponent* InComponent);
	void SetArsenalComponent(UArsenalComponent* InComponent);

	FOnClickedCash OnClickedCash;
	FOnClickedOutside OnClickedHealth;
	FOnClickedOutside OnClickedArmour;
	FOnClickedOutside OnClickedGrenade;

	TSharedRef<SWidget> Generate(bool bHideBooster = false);

protected:

	const FCurrencyBlockStyle* Style;

	TWeakObjectPtr<UIdentityComponent> IdentityComponent;
	TWeakObjectPtr<UArsenalComponent> ArsenalComponent;

	TSharedPtr<SAnimatedBackground> Anim_Back;

	int64 GetPlayerCash(EGameCurrency InCurrency) const;
	FReply OnClickedCashEvent(EGameCurrency InCurrency);

	int64 GetPlayerPremiumTime() const;
	FReply OnClickedPremiumTimeEvent();

	int64 GetPlayerHealth() const;
	FReply OnClickedHealthEvent();

	int64 GetPlayerArmour() const;
	FReply OnClickedArmourEvent();

	int64 GetPlayerGrenade() const;
	FReply OnClickedGrenadeEvent();
};
