// Fill out your copyright notice in the Description page of Project Settings.

#include "ShooterGame.h"
#include "SCurrencyBlock.h"
#include "SlateOptMacros.h"
#include "GameItemCost.h"
#include "SResourceTextBox.h"
#include "Utilities/SAnimatedBackground.h"
#include "IdentityComponent.h"
#include "Input/STouchButton.h"
#include "Notify/SNotifyContainer.h"
#include "Notify/SMessageBox.h"
#include "Localization/TableWeaponStrings.h"
#include "Product/ProductItemCategory.h"
#include "DateTimeExtensions.h"
#include "ArsenalComponent.h"

BEGIN_SLATE_FUNCTION_BUILD_OPTIMIZATION
void SCurrencyBlock::Construct(const FArguments& InArgs)
{
	Style = InArgs._Style;

	ChildSlot.HAlign(HAlign_Right).VAlign(VAlign_Top).Padding(20)
	[
		Generate()
	];	
}
END_SLATE_FUNCTION_BUILD_OPTIMIZATION

void SCurrencyBlock::SetIdentityComponent(UIdentityComponent* InComponent)
{
	IdentityComponent = InComponent;
}

void SCurrencyBlock::SetArsenalComponent(UArsenalComponent* InComponent)
{
	ArsenalComponent = InComponent;
}

TSharedRef<SWidget> SCurrencyBlock::Generate(bool bHideBooster)
{
	static float HPadding = 10.0f; // 20
	static float SubHPadding = 2.0f; // 8

	static FName Tag_Block = TEXT("SCurrencyBlock");
	static FName Tag_Health = TEXT("SCurrencyBlock.Health");
	static FName Tag_Grenade = TEXT("SCurrencyBlock.Grenade");
	static FName Tag_Armour = TEXT("SCurrencyBlock.Armour");
	static FName Tag_Premium = TEXT("SCurrencyBlock.Premium");
	static FName Tag_Money = TEXT("SCurrencyBlock.Money");
	static FName Tag_Donate = TEXT("SCurrencyBlock.Donate");

	return SNew(SBox).HAlign(HAlign_Right).VAlign(VAlign_Top).Padding(FMargin(HPadding, 20))
		[
			SNew(SOverlay)
			+ SOverlay::Slot()
			[
				SNew(SBorder).BorderImage(&Style->Background).Padding(0).Tag(Tag_Block)
			]
			+ SOverlay::Slot()
			[
				SNew(SHorizontalBox)
			//=================================[ Health ]==============================================
				+ SHorizontalBox::Slot().AutoWidth().Padding(bHideBooster ? 0 : HPadding, 0)
				[
					SNew(STouchButton).Tag(Tag_Health)
					.HAlign(HAlign_Center)
					.VAlign(VAlign_Center)
					.Padding(FMargin(SubHPadding, 16))
					.Style(&Style->Buttons)
					.Visibility(bHideBooster ? EVisibility::Collapsed : EVisibility::Visible)
					[
						SNew(SResourceTextBox)
						.Type(EResourceTextBoxType::Health)
						.Value(this, &SCurrencyBlock::GetPlayerHealth)
						.Visibility(EVisibility::HitTestInvisible)
					]
					.OnClicked(this, &SCurrencyBlock::OnClickedHealthEvent)
				]
			//=================================[ Grenade ]==============================================
				+ SHorizontalBox::Slot().AutoWidth().Padding(bHideBooster ? 0 : HPadding, 0)
				[
					SNew(STouchButton).Tag(Tag_Grenade)
					.HAlign(HAlign_Center)
					.VAlign(VAlign_Center)
					.Padding(FMargin(SubHPadding, 16))
					.Style(&Style->Buttons)
					.Visibility(bHideBooster ? EVisibility::Collapsed : EVisibility::Visible)
					[
						SNew(SResourceTextBox)
						.Type(EResourceTextBoxType::Grenade)
						.Value(this, &SCurrencyBlock::GetPlayerGrenade)
						.Visibility(EVisibility::HitTestInvisible)
					]
					.OnClicked(this, &SCurrencyBlock::OnClickedGrenadeEvent)
				]
			//=================================[ Armour ]==============================================
				+ SHorizontalBox::Slot().AutoWidth().Padding(bHideBooster ? 0 : HPadding, 0)
				[
					SNew(STouchButton).Tag(Tag_Armour)
					.HAlign(HAlign_Center)
					.VAlign(VAlign_Center)
					.Padding(FMargin(SubHPadding, 16))
					.Style(&Style->Buttons)
					.Visibility(bHideBooster ? EVisibility::Collapsed : EVisibility::Visible)
					[
						SNew(SResourceTextBox)
						.Type(EResourceTextBoxType::Armour)
						.Value(this, &SCurrencyBlock::GetPlayerArmour)
						.Visibility(EVisibility::HitTestInvisible)
					]
					.OnClicked(this, &SCurrencyBlock::OnClickedArmourEvent)
				]
			//=================================[ Premium ]==============================================
				+ SHorizontalBox::Slot().AutoWidth().Padding(bHideBooster ? 0 : HPadding, 0)
				[
					SNew(STouchButton).Tag(Tag_Premium)
					.HAlign(HAlign_Center)
					.VAlign(VAlign_Center)
					.Padding(FMargin(SubHPadding, 16))
					.Style(&Style->Buttons)
					.Visibility(bHideBooster ? EVisibility::Collapsed : EVisibility::Visible)
					[
						SNew(SResourceTextBox)
						.Type(EResourceTextBoxType::PremiumTime)
						.Value(this, &SCurrencyBlock::GetPlayerPremiumTime)
						.Visibility(EVisibility::HitTestInvisible)
					]
					.OnClicked(this, &SCurrencyBlock::OnClickedPremiumTimeEvent)
				]
				+ SHorizontalBox::Slot().AutoWidth().Padding(HPadding, 0)
				[
					SNew(STouchButton).Tag(Tag_Money)
					.HAlign(HAlign_Center)
					.VAlign(VAlign_Center)
					.Padding(FMargin(SubHPadding, 16))
					.Style(&Style->Buttons)
					[
						SNew(SResourceTextBox)
						.Type(EResourceTextBoxType::Money)
						.Value(this, &SCurrencyBlock::GetPlayerCash, EGameCurrency::Money)
						.Visibility(EVisibility::HitTestInvisible)
					]
					.OnClicked(this, &SCurrencyBlock::OnClickedCashEvent, EGameCurrency::Money)
				]
				+ SHorizontalBox::Slot().AutoWidth().Padding(HPadding, 0)
				[
					SNew(STouchButton).Tag(Tag_Donate)
					.HAlign(HAlign_Center)
					.VAlign(VAlign_Center)
					.Padding(FMargin(SubHPadding, 16))
					.Style(&Style->Buttons)
					[
						SNew(SResourceTextBox)
						.Type(EResourceTextBoxType::Donate)
						.Value(this, &SCurrencyBlock::GetPlayerCash, EGameCurrency::Donate)
						.Visibility(EVisibility::HitTestInvisible)
					]
					.OnClicked(this, &SCurrencyBlock::OnClickedCashEvent, EGameCurrency::Donate)
				]
			]
		];
}

int64 SCurrencyBlock::GetPlayerCash(EGameCurrency InCurrency) const
{
	if (IsValidObject(IdentityComponent))
	{
		return IdentityComponent->GetPlayerInfo().TakeCash(InCurrency);
	}

	return 0;
}

FReply SCurrencyBlock::OnClickedCashEvent(EGameCurrency InCurrency)
{
	OnClickedCash.ExecuteIfBound(InCurrency);
	return FReply::Handled();
}

int64 SCurrencyBlock::GetPlayerPremiumTime() const
{
	if (IsValidObject(IdentityComponent) && IdentityComponent->HasPremiumAccount())
	{
		return (FDateTime::FromUnixTimestamp(IdentityComponent->GetPlayerInfo().PremiumEndDate) - FDateTime::UtcNow()).GetTotalSeconds();
	}

	return 0;
}

FReply SCurrencyBlock::OnClickedPremiumTimeEvent()
{
	QueueBegin(SMessageBox, "PremiumNotify")
		SMessageBox::Get()->SetHeaderText(FTableItemStrings::GetProductText(EProductItemCategory::Booster));
		SMessageBox::Get()->SetContent(
			FText::Format(NSLOCTEXT("SCurrencyBlock", "SCurrencyBlock.PremiumDesc", "With the accelerator, you get a reward for the fight 2 times bigger,\n<img src=\"img_Premium\"></> the accelerator's lifetime {0}."),
			FDateTimeExtensions::AsFormatedTimespan(FTimespan::FromSeconds(GetPlayerPremiumTime()))
		));
		SMessageBox::Get()->SetButtonsText(NSLOCTEXT("SCurrencyBlock", "SCurrencyBlock.PremiumBtn", "Buy accelerator"), FTableBaseStrings::GetBaseText(EBaseStrings::Close));
		SMessageBox::Get()->OnMessageBoxButton.AddLambda([&](EMessageBoxButton InButton)
		{
			if (InButton == EMessageBoxButton::Left)
			{
				OnClickedCash.ExecuteIfBound(EGameCurrency::BattleCoins);
			}

			SMessageBox::Get()->ToggleWidget(false);
		});
		SMessageBox::Get()->ToggleWidget(true);
	QueueEnd

	return FReply::Handled();
}

int64 SCurrencyBlock::GetPlayerHealth() const
{
	if (IsValidObject(ArsenalComponent))
	{
		return ArsenalComponent->GetCachedAids();
	}

	return 0;
}

FReply SCurrencyBlock::OnClickedHealthEvent()
{
	QueueBegin(SMessageBox, "HealthNotify")
		SMessageBox::Get()->SetHeaderText(NSLOCTEXT("All", "All.Health", "Health"));
		SMessageBox::Get()->SetContent(NSLOCTEXT("SCurrencyBlock", "SCurrencyBlock.HealthDesc", "Health is primary indicator in battle, you can fill it with a aid kit."));
		SMessageBox::Get()->SetButtonsText(NSLOCTEXT("SCurrencyBlock", "SCurrencyBlock.HealthBtn", "Buy aid"), FTableBaseStrings::GetBaseText(EBaseStrings::Close));
		SMessageBox::Get()->OnMessageBoxButton.AddLambda([&](EMessageBoxButton InButton)
		{
			if (InButton == EMessageBoxButton::Left)
			{
				OnClickedHealth.ExecuteIfBound();
			}

			SMessageBox::Get()->ToggleWidget(false);
		});
		SMessageBox::Get()->ToggleWidget(true);
	QueueEnd
	
	return FReply::Handled();
}

int64 SCurrencyBlock::GetPlayerArmour() const
{
	if (IsValidObject(ArsenalComponent))
	{
		return FMath::CeilToInt(ArsenalComponent->GetCachedArmour());
	}

	return 0;
}

FReply SCurrencyBlock::OnClickedArmourEvent()
{
	QueueBegin(SMessageBox, "ArmourNotify")
		SMessageBox::Get()->SetHeaderText(NSLOCTEXT("All", "All.Armour", "Armour"));
		SMessageBox::Get()->SetContent(NSLOCTEXT("SCurrencyBlock", "SCurrencyBlock.ArmourDesc", "Armour protects after a rebirth, it cannot be replenished as health, but it can and should be increased."));
		SMessageBox::Get()->SetButtonsText(NSLOCTEXT("SCurrencyBlock", "SCurrencyBlock.ArmourBtn", "Buy armour"), FTableBaseStrings::GetBaseText(EBaseStrings::Close));
		SMessageBox::Get()->OnMessageBoxButton.AddLambda([&](EMessageBoxButton InButton)
		{
			if (InButton == EMessageBoxButton::Left)
			{
				OnClickedArmour.ExecuteIfBound();
			}

			SMessageBox::Get()->ToggleWidget(false);
		});
		SMessageBox::Get()->ToggleWidget(true);
	QueueEnd
	
	return FReply::Handled();
}

int64 SCurrencyBlock::GetPlayerGrenade() const
{
	if (IsValidObject(ArsenalComponent))
	{
		return ArsenalComponent->GetCachedGrenades();
	}

	return 0;
}

FReply SCurrencyBlock::OnClickedGrenadeEvent()
{
	QueueBegin(SMessageBox, "GrenadeNotify")
		SMessageBox::Get()->SetHeaderText(NSLOCTEXT("All", "All.Grenade", "Grenade"));
		SMessageBox::Get()->SetContent(NSLOCTEXT("SCurrencyBlock", "SCurrencyBlock.GrenadeDesc", "Grenades are an effective medium combat weapon for mass destruction."));
		SMessageBox::Get()->SetButtonsText(NSLOCTEXT("SCurrencyBlock", "SCurrencyBlock.GrenadeBtn", "Buy Grenade"), FTableBaseStrings::GetBaseText(EBaseStrings::Close));
		SMessageBox::Get()->OnMessageBoxButton.AddLambda([&](EMessageBoxButton InButton)
		{
			if (InButton == EMessageBoxButton::Left)
			{
				OnClickedGrenade.ExecuteIfBound();
			}

			SMessageBox::Get()->ToggleWidget(false);
		});
		SMessageBox::Get()->ToggleWidget(true);
	QueueEnd

	return FReply::Handled();
}
