// Fill out your copyright notice in the Description page of Project Settings.

#include "ShooterGame.h"
#include "SPlayerNameAndProgress.h"
#include "SlateOptMacros.h"
#include "SResourceTextBox.h"
#include "IdentityComponent.h"
#include "SRetainerWidget.h"
#include "SScaleBox.h"
#include "SlateMaterialBrush.h"
#include "GameSingletonExtensions.h"
#include "WorldFlagsData.h"

FPlayerNameAndProgressData::FPlayerNameAndProgressData()
	: CurrentExp(0)
	, NeedExp(0)
	, ELO(0)
	, Level(0)
	, HasPremium(false)
{
	
}

FPlayerNameAndProgressData::FPlayerNameAndProgressData(const FString& InName,
	const uint8& InLevel,
	const float& InCurrentExp,
	const float& InNeedExp,
	const bool& InHasPremium,
	const uint32& InELO)
	: Name(InName)
	, CurrentExp(InCurrentExp)
	, NeedExp(InNeedExp)
	, ELO(InELO)
	, Level(InLevel)
	, HasPremium(InHasPremium)
{
	
}

bool FPlayerNameAndProgressData::IsValid() const
{
	static FPlayerNameAndProgressData InvalidData = FPlayerNameAndProgressData();
	return *this != InvalidData;
}

bool FPlayerNameAndProgressData::operator==(const FPlayerNameAndProgressData& InOther) const
{
	return
		this->Name == InOther.Name &&
		this->Level == InOther.Level &&
		this->CurrentExp == InOther.CurrentExp &&
		this->NeedExp == InOther.NeedExp &&
		this->ELO == InOther.ELO &&
		this->HasPremium == InOther.HasPremium;
}

bool FPlayerNameAndProgressData::operator!=(const FPlayerNameAndProgressData& InOther) const
{
	return (*this == InOther) == false;
}

BEGIN_SLATE_FUNCTION_BUILD_OPTIMIZATION
SPlayerNameAndProgress::~SPlayerNameAndProgress()
{
	AvatarMID->RemoveFromRoot();
}

void SPlayerNameAndProgress::Construct(const FArguments& InArgs)
{
	Style = InArgs._Style;
	IdentityComponent = InArgs._IdentityComponent;

	AvatarMID = UMaterialInstanceDynamic::Create(Style->Material, GetTransientPackage());
	AvatarMID->AddToRoot();
	AvatarBrush = FSlateMaterialBrush(*AvatarMID, FVector2D(100, 100));

	ChildSlot
	[
		SNew(SRetainerWidget)
		.Phase(0)
		.PhaseCount(5)
		.RenderOnPhase(true)
		.RenderOnInvalidation(false)
		.Visibility(EVisibility::SelfHitTestInvisible)
		[
			SNew(SHorizontalBox)
			+ SHorizontalBox::Slot().AutoWidth()
			[
				SNew(SScaleBox).Stretch(EStretch::ScaleToFit)
				[
					SNew(SImage).Image(&AvatarBrush)
				]
			]
			+ SHorizontalBox::Slot().HAlign(HAlign_Left) // Level, avatar, progress
			[
				SNew(SVerticalBox)
				+ SVerticalBox::Slot().AutoHeight()
				[
					SNew(STextBlock)
					.Justification(ETextJustify::Left)
					.Text(this, &SPlayerNameAndProgress::GetNameText)
					.Font(this, &SPlayerNameAndProgress::GetNameFont)
					.ColorAndOpacity(this, &SPlayerNameAndProgress::GetNameColor)
				]
				+ SVerticalBox::Slot().AutoHeight()
				[
					SNew(SHorizontalBox)
					+ SHorizontalBox::Slot().AutoWidth()
					[
						SNew(SResourceTextBox)
						.Visibility(EVisibility::HitTestInvisible)
						.Type(EResourceTextBoxType::Level)
						.Value(this, &SPlayerNameAndProgress::GetLevelNumber)
					]
					+ SHorizontalBox::Slot().AutoWidth().Padding(10, 0)
					[
						SNew(SResourceTextBox)
						.Visibility(EVisibility::HitTestInvisible)
						.Type(EResourceTextBoxType::Elo)
						.Value(this, &SPlayerNameAndProgress::GetEloNumber)
					]
				]
			]
		]
	];

	LastRefreshTime = .0f;
	RefreshCountryData();
}
END_SLATE_FUNCTION_BUILD_OPTIMIZATION

void SPlayerNameAndProgress::SetOverrideData(const FPlayerNameAndProgressData& InData)
{
	OverrideData = InData;
	RefreshCountryData();
}

void SPlayerNameAndProgress::RefreshCountryData()
{
	if (IsValidObject(AvatarMID))
	{
		AvatarMID->SetScalarParameterValue(Style->ParameterProgress, GetProgressPercent().GetValue());

		auto FlagsData = FGameSingletonExtension::GetDataAssets<UWorldFlagsData>();
		if (FlagsData.Num() && FlagsData.IsValidIndex(0) && IsValidObject(FlagsData[0]))
		{
			AvatarMID->SetTextureParameterValue(Style->ParameterFlagTexture, FlagsData[0]->GetFlagsTexture());
			AvatarMID->SetVectorParameterValue(Style->ParameterFlagSubImages, FVector(FlagsData[0]->GetFlagsTextureSubImages(), 0));
			AvatarMID->SetScalarParameterValue(Style->ParameterFlag, FlagsData[0]->GetFlagIndexByCode(GetCountry()));
		}		
	}
}

FSlateFontInfo SPlayerNameAndProgress::GetNameFont() const
{
	if (OverrideData.IsValid())
	{
		if (OverrideData.HasPremium)
		{
			return Style->NamePremium.Font;
		}
	}
	else if (IsValidObject(IdentityComponent) && IdentityComponent->HasPremiumAccount())
	{
		return Style->NamePremium.Font;
	}

	return Style->NameNormal.Font;
}

FSlateColor SPlayerNameAndProgress::GetNameColor() const
{
	if (OverrideData.IsValid())
	{
		if (OverrideData.HasPremium)
		{
			return Style->NamePremium.ColorAndOpacity;
		}
	}
	else if (IsValidObject(IdentityComponent) && IdentityComponent->HasPremiumAccount())
	{
		return Style->NamePremium.ColorAndOpacity;
	}

	return Style->NameNormal.ColorAndOpacity;
}

FText SPlayerNameAndProgress::GetNameText() const
{
	if (OverrideData.IsValid())
	{
		return FText::FromString(OverrideData.Name);
	}
	else if (IsValidObject(IdentityComponent))
	{
		return FText::FromString(IdentityComponent->GetPlayerInfo().Login);
	}

	return FText::GetEmpty();
}

int64 SPlayerNameAndProgress::GetLevelNumber() const
{
	if (OverrideData.IsValid())
	{
		return OverrideData.Level;
	}
	else if (IsValidObject(IdentityComponent))
	{
		return IdentityComponent->GetPlayerInfo().Experience.Level;
	}

	return INDEX_NONE;
}

int64 SPlayerNameAndProgress::GetEloNumber() const
{
	if (OverrideData.IsValid())
	{
		return OverrideData.ELO;
	}
	else if (IsValidObject(IdentityComponent))
	{
		return IdentityComponent->GetPlayerInfo().EloRatingScore;
	}

	return 0;
}

FText SPlayerNameAndProgress::GetProgressText() const
{
	if (OverrideData.IsValid())
	{
		return FText::Format(FText::FromString("{0}|{1}"),
			FText::AsNumber(OverrideData.CurrentExp, &FNumberFormattingOptions::DefaultNoGrouping()),
			FText::AsNumber(OverrideData.NeedExp, &FNumberFormattingOptions::DefaultNoGrouping())
		);
	}
	else if (IsValidObject(IdentityComponent))
	{
		return FText::Format(FText::FromString("{0}|{1}"),
			FText::AsNumber(IdentityComponent->GetPlayerInfo().Experience.Experience, &FNumberFormattingOptions::DefaultNoGrouping()),
			FText::AsNumber(IdentityComponent->GetPlayerInfo().Experience.NextExperience, &FNumberFormattingOptions::DefaultNoGrouping())
		);
	}

	return FText::GetEmpty();
}

TOptional<float> SPlayerNameAndProgress::GetProgressPercent() const
{
	if (OverrideData.IsValid())
	{
		return
			float(OverrideData.CurrentExp) /
			float(OverrideData.NeedExp);
	}
	else if (IsValidObject(IdentityComponent))
	{
		return
			float(IdentityComponent->GetPlayerInfo().Experience.Experience) /
			float(IdentityComponent->GetPlayerInfo().Experience.NextExperience);
	}
	
	return .0f;
}

EIsoCountry SPlayerNameAndProgress::GetCountry() const
{
	if (OverrideData.IsValid())
	{
		return OverrideData.Country;
	}
	else if (IsValidObject(IdentityComponent))
	{
		return IdentityComponent->GetPlayerInfo().Country;
	}

	return EIsoCountry::None;
}

void SPlayerNameAndProgress::Tick(const FGeometry& AllottedGeometry, const double InCurrentTime, const float InDeltaTime)
{
	SCompoundWidget::Tick(AllottedGeometry, InCurrentTime, InDeltaTime);

	if (uint64(InCurrentTime) % 10 == 0 && LastRefreshTime + 2.0f < InCurrentTime)
	{
		LastRefreshTime = InCurrentTime;
		RefreshCountryData();
	}
}
