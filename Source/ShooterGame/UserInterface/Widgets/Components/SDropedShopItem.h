// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "SShopItem.h"

class UBasicItemEntity;
/**
 * 
 */
class SHOOTERGAME_API SDropedShopItem : public SShopItem
{
public:
	SLATE_BEGIN_ARGS(SDropedShopItem)
		: _Range(0, 0)
	{}
	SLATE_ARGUMENT(int32, Num)
	SLATE_ARGUMENT(FIntPoint, Range)
	SLATE_END_ARGS()

	void Construct(const FArguments& InArgs, TWeakObjectPtr<UBasicItemEntity> InInstance);

protected:

};
