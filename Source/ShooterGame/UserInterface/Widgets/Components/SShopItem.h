// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "Widgets/SCompoundWidget.h"
#include "Styles/ShopItemWidgetStyle.h"
#include "Extensions/UObjectExtensions.h"

class UBasicItemEntity;
/**
 * 
 */
class SHOOTERGAME_API SShopItem : public SCompoundWidget
{
public:
	SLATE_BEGIN_ARGS(SShopItem)
		: _Style(&FShooterStyle::Get().GetWidgetStyle<FShopItemStyle>("SShopItemStyle"))
		, _ActiveCoefficient(1.0f)
	{}
	SLATE_STYLE_ARGUMENT(FShopItemStyle, Style)
	SLATE_ATTRIBUTE(float, ActiveCoefficient)
	SLATE_END_ARGS()

	void Construct(const FArguments& InArgs, TWeakObjectPtr<UBasicItemEntity> InInstance);

	TWeakObjectPtr<UBasicItemEntity> GetInstanceBasic() const;
	
	template<typename T = UBasicItemEntity>
	T* GetInstanceBasic() const
	{
		return GetValidObjectAs<T>(GetInstanceBasic().Get());
	}


protected:

	const FShopItemStyle* Style;

	TAttribute<float> ActiveCoefficient;

	TSharedPtr<SOverlay> Container;
	TWeakObjectPtr<UBasicItemEntity> Instance;

	virtual const FSlateBrush* GetItemImage() const;
	virtual FText GetItemName() const;
	virtual FMargin GetImagePadding() const;
	virtual FMargin GetTitlePadding() const;
	virtual FSlateColor GetImageColor() const;
	virtual float GetFontScale() const;
};
