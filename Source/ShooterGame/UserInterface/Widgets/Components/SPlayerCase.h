// Fill out your copyright notice in the Description page of Project Settings.

#pragma once
#include "Widgets/SCompoundWidget.h"
#include "Styles/PlayerCaseWidgetStyle.h"

class UPlayerCaseEntity;
struct FCaseProperty;


DECLARE_DELEGATE_OneParam(FOnOpenPlayerCase, const FGuid& /* InPlayerCaseId*/);


class SHOOTERGAME_API SPlayerCase : public SCompoundWidget
{
public:
	SLATE_BEGIN_ARGS(SPlayerCase)
		: _Style(&FShooterStyle::Get().GetWidgetStyle<FPlayerCaseStyle>("SPlayerCaseStyle"))
	{}
	SLATE_STYLE_ARGUMENT(FPlayerCaseStyle, Style)
	SLATE_EVENT(FOnOpenPlayerCase, OnOpenPlayerCase)
	SLATE_END_ARGS()

	void Construct(const FArguments& InArgs, TWeakObjectPtr<UPlayerCaseEntity> InPlayerCaseEntity);

	const UPlayerCaseEntity* GetCaseEntity() const;
	const FCaseProperty* GetCaseEntityProperty() const;

	
protected:

	const FPlayerCaseStyle* Style;

	FText GetDeliveryText() const;
	FSlateFontInfo GetDeliveryFont() const;
	FSlateColor GetDeliveryColor() const;
	const FSlateBrush* GetDeliveryBackground() const;
	FOptionalSize GetHeight() const;

	FReply OnOpenPlayerCaseClicked() const;

	FOnOpenPlayerCase OnOpenPlayerCase;
	TWeakObjectPtr<UPlayerCaseEntity> PlayerCaseEntity;

};
