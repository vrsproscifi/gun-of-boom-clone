// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "SShopItem.h"

class UBasicPlayerItem;
class UCaseProductEntity;
class UCaseEntity;

struct FCaseProperty;
struct FCaseItemProperty;

/**
 * 
 */
class SHOOTERGAME_API SShopCaseItem : public SShopItem
{
public:
	SLATE_BEGIN_ARGS(SShopCaseItem)
	{
		
	}
	SLATE_ATTRIBUTE(float, ActiveCoefficient)
	SLATE_END_ARGS()

	void Construct(const FArguments& InArgs, TWeakObjectPtr<UBasicItemEntity> InInstance);
	
	
	UCaseEntity* GetCaseEntity() const;
	const FCaseProperty* GetCaseEntityProperty() const;
	const TArray<FCaseItemProperty>* GetDropedItems() const;
	const UCaseProductEntity* GetCaseProduct() const;

protected:

	FReply OnClickedInfo();
};
