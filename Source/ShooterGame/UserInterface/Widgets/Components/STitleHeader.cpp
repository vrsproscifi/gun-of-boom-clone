// Fill out your copyright notice in the Description page of Project Settings.

#include "ShooterGame.h"
#include "STitleHeader.h"
#include "SlateOptMacros.h"
#include "SCurrencyBlock.h"

BEGIN_SLATE_FUNCTION_BUILD_OPTIMIZATION
void STitleHeader::Construct(const FArguments& InArgs)
{
	OnClickedBack = InArgs._OnClickedBack;
	Attr_HeaderText = InArgs._Header;
	Attr_ShowCurrency = InArgs._ShowCurrency;
	Attr_ShowBack = InArgs._ShowBack;

	ChildSlot
	[
		SNew(SOverlay)
		+ SOverlay::Slot()
		[
			SNew(SHorizontalBox)
			+ SHorizontalBox::Slot().AutoWidth()
			[
				SNew(SButton)
				.ButtonStyle(&FShooterStyle::Get().GetWidgetStyle<FButtonStyle>("Default_BackButton"))
				.ContentPadding(80)
				.HAlign(HAlign_Center)
				.VAlign(VAlign_Center)
				.OnClicked_Lambda([&]()
				{
					OnClickedBack.ExecuteIfBound();
					return FReply::Handled();
				})
				.Visibility_Lambda([&]()
				{
					return Attr_ShowBack.Get(true) ? EVisibility::Visible : EVisibility::Collapsed;
				})
			]
			+ SHorizontalBox::Slot()
			+ SHorizontalBox::Slot().AutoWidth()
			[
				SNew(SBox).Visibility_Lambda([&]()
				{
					return Attr_ShowCurrency.Get(false) ? EVisibility::Visible : EVisibility::Collapsed;
				})
				[
					SCurrencyBlock::Get()->Generate(true)
				]
			]
		]
		+ SOverlay::Slot().VAlign(VAlign_Top).HAlign(HAlign_Center)
		[
			SNew(SBox).HeightOverride(100).VAlign(VAlign_Center)
			[
				SNew(STextBlock)
				.Text_Lambda([&]() { return Attr_HeaderText.Get(FText::GetEmpty()); })
				.TextStyle(&FShooterStyle::Get().GetWidgetStyle<FTextBlockStyle>("Default_HeaderText"))
			]
		]
	];
}
END_SLATE_FUNCTION_BUILD_OPTIMIZATION
