// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "Widgets/SCompoundWidget.h"
#include "Styles/PlayerNameAndProgressWidgetStyle.h"
#include "IsoCountry.h"

class UIdentityComponent;

struct FPlayerNameAndProgressData
{
	FString Name;
	uint32 CurrentExp;
	uint32 NeedExp;
	uint32 ELO;
	uint8 Level;
	EIsoCountry Country;
	bool HasPremium;

	FPlayerNameAndProgressData();
	FPlayerNameAndProgressData(const FString& InName, const uint8& InLevel = 0, const float& InCurrentExp = 0, const float& InNeedExp = 0, const bool& InHasPremium = false, const uint32& InELO = 0);

	bool IsValid() const;

	bool operator==(const FPlayerNameAndProgressData& InOther) const;
	bool operator!=(const FPlayerNameAndProgressData& InOther) const;
};

class SHOOTERGAME_API SPlayerNameAndProgress 
	: public SCompoundWidget
{
public:
	SLATE_BEGIN_ARGS(SPlayerNameAndProgress)
		: _Style(&FShooterStyle::Get().GetWidgetStyle<FPlayerNameAndProgressStyle>("SPlayerNameAndProgressStyle"))
	{}
	SLATE_STYLE_ARGUMENT(FPlayerNameAndProgressStyle, Style)
	SLATE_ARGUMENT(TWeakObjectPtr<UIdentityComponent>, IdentityComponent)
	SLATE_END_ARGS()

	~SPlayerNameAndProgress();

	void Construct(const FArguments& InArgs);
	void SetOverrideData(const FPlayerNameAndProgressData& InData);
	
	void RefreshCountryData();

protected:

	const FPlayerNameAndProgressStyle* Style;

	UMaterialInstanceDynamic* AvatarMID;
	FSlateBrush AvatarBrush;
	double LastRefreshTime;

	TWeakObjectPtr<UIdentityComponent> IdentityComponent;
	FPlayerNameAndProgressData OverrideData;

	FSlateFontInfo GetNameFont() const;
	FSlateColor GetNameColor() const;
	FText GetNameText() const;

	int64 GetLevelNumber() const;
	int64 GetEloNumber() const;
	FText GetProgressText() const;
	TOptional<float> GetProgressPercent() const;
	EIsoCountry GetCountry() const;

	void Tick(const FGeometry& AllottedGeometry, const double InCurrentTime, const float InDeltaTime) override;
};
