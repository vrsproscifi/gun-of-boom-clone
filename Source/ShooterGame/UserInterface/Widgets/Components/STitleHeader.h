// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "Widgets/SCompoundWidget.h"

/**
 * 
 */
class SHOOTERGAME_API STitleHeader : public SCompoundWidget
{
public:
	SLATE_BEGIN_ARGS(STitleHeader)
		: _ShowBack(true)
	{}
	SLATE_EVENT(FOnClickedOutside, OnClickedBack)
	SLATE_ATTRIBUTE(FText, Header)
	SLATE_ATTRIBUTE(bool, ShowCurrency)
	SLATE_ATTRIBUTE(bool, ShowBack)
	SLATE_END_ARGS()

	void Construct(const FArguments& InArgs);

protected:

	FOnClickedOutside OnClickedBack;

	TAttribute<FText> Attr_HeaderText;
	TAttribute<bool> Attr_ShowCurrency;
	TAttribute<bool> Attr_ShowBack;
};
