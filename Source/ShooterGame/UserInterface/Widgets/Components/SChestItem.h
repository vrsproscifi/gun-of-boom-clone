// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "Widgets/SCompoundWidget.h"
#include "ChestWidgetsWidgetStyle.h"

class UPlayerCaseEntity;

DECLARE_DELEGATE_OneParam(FOnClickedChest, const TWeakObjectPtr<UPlayerCaseEntity>&);

class SHOOTERGAME_API SChestItem : public SCompoundWidget
{
public:
	SLATE_BEGIN_ARGS(SChestItem)
		: _Style(&FShooterStyle::Get().GetWidgetStyle<FChestWidgetsStyle>("SChestWidgetsStyle"))
	{}
	SLATE_STYLE_ARGUMENT(FChestWidgetsStyle, Style)
	SLATE_EVENT(FOnClickedChest, OnClickedChest)
	SLATE_END_ARGS()

	void Construct(const FArguments& InArgs, const TWeakObjectPtr<UPlayerCaseEntity>& InInstance);

protected:

	const FChestWidgetsStyle* Style;

	static FSlateBrush EmptyBrush;
	FOnClickedChest OnClickedChest;

	TWeakObjectPtr<UPlayerCaseEntity> Instance;

	FReply OnClicked();

	FText GetHeaderText() const;
	FText GetContentText() const;
	const FSlateBrush* GetHeaderBackground() const;
	const FSlateBrush* GetContentBackground() const;
	const FSlateBrush* GetItemImage() const;
	EVisibility GetVisibilityChest() const;
};
