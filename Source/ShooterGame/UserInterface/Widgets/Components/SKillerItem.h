#pragma once

#include "SShopItem.h"

class UBasicPlayerItem;


class SHOOTERGAME_API SKillerItem : public SShopItem
{
public:
	SLATE_BEGIN_ARGS(SKillerItem)
	{}
		SLATE_ARGUMENT(int32, InItemLevel)
		SLATE_STYLE_ARGUMENT(FShopItemStyle, Style)
		SLATE_ATTRIBUTE(float, ActiveCoefficient)

	SLATE_END_ARGS()

	void Construct(const FArguments& InArgs, TWeakObjectPtr<UBasicItemEntity> InInstance);

	FLinearColor DamageColor;

protected:

	
	int32 ItemLevel;
	float Damage;
	virtual FText GetItemName() const override;
	FText GetDamageText() const;
};