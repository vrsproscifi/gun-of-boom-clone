// Fill out your copyright notice in the Description page of Project Settings.

#include "ShooterGame.h"
#include "SShopItem.h"
#include "SlateOptMacros.h"
#include "SScaleBox.h"
#include "BasicItemEntity.h"
#include "Decorators/LokaTextDecorator.h"
#include "Decorators/LokaResourceDecorator.h"
#include "UObjectExtensions.h"
#include "SInvalidationPanel.h"
#include "SRetainerWidget.h"
#include "ProductItemEntity.h"

BEGIN_SLATE_FUNCTION_BUILD_OPTIMIZATION
void SShopItem::Construct(const FArguments& InArgs, TWeakObjectPtr<UBasicItemEntity> InInstance)
{
	Style = InArgs._Style;
	ActiveCoefficient = InArgs._ActiveCoefficient;
	Instance = InInstance;

	ChildSlot
	[
		SNew(SInvalidationPanel)
		[
			SAssignNew(Container, SOverlay)
			+ SOverlay::Slot().Padding(TAttribute<FMargin>::Create(TAttribute<FMargin>::FGetter::CreateSP(this, &SShopItem::GetImagePadding)))
			[
				SNew(SScaleBox)
				.Stretch(EStretch::ScaleToFit)
				.Visibility(EVisibility::HitTestInvisible)
				[
					SNew(SImage)
					.Image(this, &SShopItem::GetItemImage)
					.ColorAndOpacity(this, &SShopItem::GetImageColor)
				]
			]
			+ SOverlay::Slot().VAlign(VAlign_Top).Padding(TAttribute<FMargin>::Create(TAttribute<FMargin>::FGetter::CreateSP(this, &SShopItem::GetTitlePadding)))
			[
				SNew(SDPIScaler)
				.DPIScale(this, &SShopItem::GetFontScale)
				[
					SNew(SRichTextBlock)
					.Justification(ETextJustify::Center)
					.Text(this, &SShopItem::GetItemName)
					.TextStyle(&Style->Title)
					.AutoWrapText(true)
					+ SRichTextBlock::Decorator(FLokaTextDecorator::Create(Style->Title))
					+ SRichTextBlock::Decorator(FLokaResourceDecorator::Create())
				]
			]
		]
	];
}
END_SLATE_FUNCTION_BUILD_OPTIMIZATION

TWeakObjectPtr<UBasicItemEntity> SShopItem::GetInstanceBasic() const
{
	return GetValidObject(Instance);
}

const FSlateBrush* SShopItem::GetItemImage() const
{
	if (IsValidObject(Instance) && Instance->IsValidLowLevel())
	{
		//if (FMath::IsNearlyEqual(ActiveCoefficient.Get(), 0.0f, .1f))
		//{
		//	return &Style->PreviewBrush;
		//}

		return &Instance->GetItemProperty().Image;
	}

	return FStyleDefaults::GetNoBrush();
}

FText SShopItem::GetItemName() const
{
	if (IsValidObject(Instance) && Instance->IsValidLowLevel())
	{
		//if (auto ProductInstance = GetValidObjectAs<UProductItemEntity>(Instance.Get()))
		//{
		//	if (ProductInstance->IsDiscountedProduct())
		//	{
		//		return FText::Format(NSLOCTEXT("SShopItem", "SShopItem.GetItemName", "{0} / {2} \nDiscount {1}%"), 
		//			Instance->GetItemProperty().Title,
		//			FText::AsNumber(ProductInstance->GetActiveDiscountPercent()),
		//			FText::AsTimespan(ProductInstance->GetAvalibleDiscountTime())
		//		);
		//	}
		//}
		return Instance->GetItemProperty().Title;
	}

	return FText::GetEmpty();
}

FMargin SShopItem::GetImagePadding() const
{
	return FMargin(
		FMath::Lerp(Style->ImagePaddingInactive.Left, Style->ImagePaddingActive.Left, ActiveCoefficient.Get()),
		FMath::Lerp(Style->ImagePaddingInactive.Top, Style->ImagePaddingActive.Top, ActiveCoefficient.Get()),
		FMath::Lerp(Style->ImagePaddingInactive.Right, Style->ImagePaddingActive.Right, ActiveCoefficient.Get()),
		FMath::Lerp(Style->ImagePaddingInactive.Bottom, Style->ImagePaddingActive.Bottom, ActiveCoefficient.Get()));
}

FMargin SShopItem::GetTitlePadding() const
{
	return Style->TitlePadding;
}

FSlateColor SShopItem::GetImageColor() const
{
	return FLinearColor(1, 1, 1, FMath::Lerp(Style->ImageTranslucently.X, Style->ImageTranslucently.Y, ActiveCoefficient.Get()));
}

float SShopItem::GetFontScale() const
{
	return FMath::Lerp(Style->FontScaleInactive, Style->FontScaleActive, ActiveCoefficient.Get());
}
