#include "ShooterGame.h"
#include "SKillerItem.h"
#include "WeaponItemEntity.h"
#include "Decorators/LokaTextDecorator.h"
#include "GameUtilities.h"

BEGIN_SLATE_FUNCTION_BUILD_OPTIMIZATION

void SKillerItem::Construct(const FArguments& InArgs, TWeakObjectPtr<UBasicItemEntity> InInstance)
{
	ItemLevel = InArgs._InItemLevel;
	SShopItem::Construct(SShopItem::FArguments().ActiveCoefficient(InArgs._ActiveCoefficient).Style(InArgs._Style), InInstance);

	auto MyWeaponItem = GetInstanceBasic<UWeaponItemEntity>();
	if (IsValidObject(MyWeaponItem))
	{
		Damage = FGameUtilities::GetWeaponCoefficient(MyWeaponItem, ItemLevel);

		Container->AddSlot().HAlign(HAlign_Left).VAlign(VAlign_Bottom)
		[
			SNew(SBorder).BorderImage(new FSlateColorBrush(FColor::Black.WithAlpha(128))).Padding(FMargin(5, 1, 10, 1))
			[
				SNew(SRichTextBlock)
				.Text(this, &SKillerItem::GetDamageText)
				.AutoWrapText(false)
				.TextStyle(&Style->Title)
				.Justification(ETextJustify::Left)
				+ SRichTextBlock::Decorator(FLokaTextDecorator::Create(Style->Title))
			]
		];
	}

	DamageColor = FLinearColor::White;
}
END_SLATE_FUNCTION_BUILD_OPTIMIZATION

FText SKillerItem::GetItemName() const
{
	FText ItemName = SShopItem::GetItemName();
	if (ItemLevel && GetInstanceBasic().IsValid())
	{
		ItemName = FText::Format(FText::FromString("{0} <text font=\"Oswald.Medium\">{1}|{2}</>"), ItemName, FText::AsNumber(ItemLevel, &FNumberFormattingOptions::DefaultNoGrouping()), FText::AsNumber(GetInstanceBasic()->GetItemProperty().Modifications.Num() - 1, &FNumberFormattingOptions::DefaultNoGrouping()));
	}	
	return ItemName;
}

FText SKillerItem::GetDamageText() const
{
	static FNumberFormattingOptions NumberFormating = FNumberFormattingOptions().SetMaximumFractionalDigits(1);
	return FText::Format(NSLOCTEXT("SKillerItem", "SKillerItem.Damage", "Damage rate: <text color=\"{1}\">{0}</>"), FText::AsNumber(Damage, &NumberFormating), FText::FromString(DamageColor.ToString()));
}

