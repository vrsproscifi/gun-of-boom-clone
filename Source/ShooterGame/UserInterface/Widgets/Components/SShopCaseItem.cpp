// Fill out your copyright notice in the Description page of Project Settings.

#include "ShooterGame.h"
#include "SShopCaseItem.h"
#include "SlateOptMacros.h"
#include "SScaleBox.h"

#include "BasicPlayerItem.h"
#include "BasicItemEntity.h"
#include "CaseProductEntity.h"
#include "Notify/SMessageBox.h"
#include "Localization/TableBaseStrings.h"
#include "GameSingletonExtensions.h"
#include "CaseItemProperty.h"
#include "SDropedShopItem.h"
#include "Lobby/Cases/SAvalibleCaseItemsContainer.h"



BEGIN_SLATE_FUNCTION_BUILD_OPTIMIZATION

void SShopCaseItem::Construct(const FArguments& InArgs, TWeakObjectPtr<UBasicItemEntity> InInstance)
{
	SShopItem::Construct(SShopItem::FArguments().ActiveCoefficient(InArgs._ActiveCoefficient), InInstance);

	Container->AddSlot().HAlign(HAlign_Left).VAlign(VAlign_Top).Padding(40)
	[
		SNew(SBox).WidthOverride(50).HeightOverride(50)
		[
			SNew(SButton)
			.ButtonStyle(&FShooterStyle::Get().GetWidgetStyle<FButtonStyle>("Default_InfoButton"))
			.OnClicked(this, &SShopCaseItem::OnClickedInfo)
		]
	];
}

const UCaseProductEntity* SShopCaseItem::GetCaseProduct() const
{
	return GetInstanceBasic<UCaseProductEntity>();
}

FReply SShopCaseItem::OnClickedInfo()
{
	if (auto MyCaseItem = GetInstanceBasic<UCaseProductEntity>())
	{
		if (const auto MyDropedItems = MyCaseItem->GetDropedItems())
		{
			auto ItemsContainer = SNew(SAvalibleCaseItemsContainer, *MyDropedItems);

			QueueBegin(SMessageBox, "DropBoxAllowItems")
				SMessageBox::Get()->SetHeaderText(GetItemName());
				SMessageBox::Get()->SetContent(ItemsContainer);
				SMessageBox::Get()->SetButtonsText(FTableBaseStrings::GetBaseText(EBaseStrings::Close));
				SMessageBox::Get()->OnMessageBoxButton.AddLambda([&](EMessageBoxButton InButton)
				{
					SMessageBox::Get()->ToggleWidget(false);
				});
				SMessageBox::Get()->ToggleWidget(true);
			QueueEnd
		}
	}

	return FReply::Handled();
}

UCaseEntity* SShopCaseItem::GetCaseEntity() const
{
	if(const auto product = GetCaseProduct())
	{
		return product->GetCaseEntity();
	}
	return nullptr;
}

const FCaseProperty* SShopCaseItem::GetCaseEntityProperty() const
{
	if (const auto product = GetCaseProduct())
	{
		return product->GetCaseEntityProperty();
	}
	return nullptr;
}

const TArray<FCaseItemProperty>* SShopCaseItem::GetDropedItems() const
{
	if (const auto product = GetCaseProduct())
	{
		return product->GetDropedItems();
	}
	return nullptr;
}


END_SLATE_FUNCTION_BUILD_OPTIMIZATION
