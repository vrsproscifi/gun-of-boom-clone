// VRSPRO

#include "ShooterGame.h"
#include "SResourceTextBox.h"
#include "SlateOptMacros.h"

#include "SResourceTextBoxType.h"
#include "SRetainerWidget.h"
#include "DateTimeExtensions.h"

FNumberFormattingOptions SResourceTextBox::CurrencyFormattingOptions = FNumberFormattingOptions()
	.SetMinimumIntegralDigits(1)
	.SetMaximumIntegralDigits(10000)
	.SetMinimumFractionalDigits(2)
	.SetMaximumFractionalDigits(2);

BEGIN_SLATE_FUNCTION_BUILD_OPTIMIZATION
void SResourceTextBox::Construct(const FArguments& InArgs)
{
	Style = new FResourceTextBoxStyle(*InArgs._Style);
	OnClicked = InArgs._OnClicked;

	SetCustomSize(InArgs._CustomSize);

	ChildSlot
	[
		SNew(SOverlay)
		+ SOverlay::Slot()
		[
			SNew(SHorizontalBox)
			+ SHorizontalBox::Slot()
			.AutoWidth()
			.HAlign(InArgs._ImageHAlign)
			.VAlign(InArgs._ImageVAlign)
			[
				SAssignNew(Image, SImage)
				.Image(this, &SResourceTextBox::GetCurrentImage)
				.OnMouseButtonDown(this, &SResourceTextBox::OnClickedOnImage)
			]
			+ SHorizontalBox::Slot()
			.FillWidth(1)
			.VAlign(InArgs._TextVAlign)
			.HAlign(InArgs._TextHAlign)
			[
				SAssignNew(TextBlock, STextBlock)
				.MinDesiredWidth(InArgs._MinDesiredWidth)
				.Font(this, &SResourceTextBox::GetCurrentFont)
				.ColorAndOpacity(this, &SResourceTextBox::GetCurrentFontColor)
				.Text(this, &SResourceTextBox::GetCurrentValue)
				.Margin(FMargin(10, 0, 0, 0))
				.Visibility(this, &SResourceTextBox::GetCurrentVisibilityText)
			]
		]
		+ SOverlay::Slot()
		[
			SNew(SImage).Image(this, &SResourceTextBox::GetOverlayImage).Visibility(EVisibility::HitTestInvisible)
		]
	];
	
	SetTypeAndValue(InArgs._TypeAndValue);
	SetDisplayMethod(InArgs._DisplayMethod);
	SetOverlayMethod(InArgs._OverlayMethod);
	SetType(InArgs._Type);
	SetValue(InArgs._Value);	
}
END_SLATE_FUNCTION_BUILD_OPTIMIZATION

FReply SResourceTextBox::OnClickedOnImage(const FGeometry& InGeometry, const FPointerEvent& InEvent)
{
	if (OnClicked.IsBound())
	{
		return OnClicked.Execute();
	}

	return FReply::Handled();
}

const int64 SResourceTextBox::GetValue() const
{
	if (Attr_TypeAndValue.IsSet())
	{
		return Attr_TypeAndValue.Get().Value;
	}

	return Attr_ValueAmount.Get();
}

const EResourceTextBoxType::Type SResourceTextBox::GetValueType() const
{
	if (Attr_TypeAndValue.IsSet())
	{
		return Attr_TypeAndValue.Get().Type;
	}

	return Attr_ValueType.Get();
}

const EResourceTextBoxDisplayMethod SResourceTextBox::GetDisplayMethod() const
{
	if (Attr_TypeAndValue.IsSet())
	{
		return Attr_TypeAndValue.Get().DisplayMethod;
	}

	return Attr_DisplayMethod.Get();
}


void SResourceTextBox::SetDisplayMethod(const TAttribute<EResourceTextBoxDisplayMethod>& InDisplayMethod)
{
	Attr_DisplayMethod = InDisplayMethod;
}

void SResourceTextBox::SetOverlayMethod(const TAttribute<EResourceTextBoxOverlayMethod>& InOverlayMethod)
{
	Attr_OverlayMethod = InOverlayMethod;
}

void SResourceTextBox::SetTypeAndValue(const TAttribute<FResourceTextBoxValue>& InTypeAndValueAttr)
{
	Attr_TypeAndValue = InTypeAndValueAttr;
}

void SResourceTextBox::SetType(const TAttribute<EResourceTextBoxType::Type>& InTypeAttr)
{
	Attr_ValueType = InTypeAttr;
}

void SResourceTextBox::SetValue(const TAttribute<int64>& InValueAttr)
{
	Attr_ValueAmount = InValueAttr;
}

void SResourceTextBox::SetValue(const FText &InCustomText)
{
	TextBlock->SetText(InCustomText);
}

void SResourceTextBox::SetCustomSize(const FVector& Size)
{
	if (Size == FVector::ZeroVector)
	{
		delete Style;
		Style = new FResourceTextBoxStyle(FShooterStyle::Get().GetWidgetStyle<FResourceTextBoxStyle>("SResourceTextBoxStyle"));
	}
	else
	{
		for(auto &s : Style->DefaultStyles)
		{
			if (Size.X > 0 && Size.Y > 0)	s.Value.Image.ImageSize = FVector2D(Size.X, Size.Y);
			if (Size.Z > 0) s.Value.TextBlock.Font.Size = Size.Z;
		}
	}
}

FText SResourceTextBox::GetCurrentValue() const
{
	const auto method = GetDisplayMethod();
	if (method == EResourceTextBoxDisplayMethod::Time || GetValueType() == EResourceTextBoxType::Time || GetValueType() == EResourceTextBoxType::PremiumTime)
	{
		return GetCurrentValueAsTime();
	}	
	else if (method == EResourceTextBoxDisplayMethod::Currency)
	{
		return FText::AsNumber(GetValue() / 100.0f, &SResourceTextBox::CurrencyFormattingOptions);
	}
	else if (method == EResourceTextBoxDisplayMethod::None || method == EResourceTextBoxDisplayMethod::Numeric)
	{
		return FText::AsNumber(GetValue(), &FNumberFormattingOptions::DefaultNoGrouping());
	}

	return FText::AsNumber(GetValue(), &FNumberFormattingOptions::DefaultNoGrouping());
}

FText SResourceTextBox::GetCurrentValueAsTime() const
{
	auto AvalibleTime = FTimespan::FromSeconds(GetValue());

	return FDateTimeExtensions::AsFormatedTimespan(AvalibleTime);

	//if (Time.GetHour())
	//{
	//	return FText::FromString(Time.ToString(TEXT("%H:%M:%S")));
	//}
	//else if (Time.GetMinute())
	//{
	//	return FText::FromString(Time.ToString(TEXT("%M:%S")));
	//}
	//else
	//{
	//	return FText::FromString(Time.ToString(TEXT("%S")));
	//}
}

const FSlateBrush* SResourceTextBox::GetCurrentImage() const
{
	return &GetResourceStyle()->Image;
}

const FSlateBrush* SResourceTextBox::GetOverlayImage() const
{
	static FSlateBrush EmptyBrush = FSlateNoResource();
	if (Attr_OverlayMethod.IsSet() && Style->OverlayLines.Contains(Attr_OverlayMethod.Get()))
	{
		return &Style->OverlayLines.FindChecked(Attr_OverlayMethod.Get());
	}

	return &EmptyBrush;
}

FSlateFontInfo SResourceTextBox::GetCurrentFont() const
{
	return GetResourceStyle()->TextBlock.Font;
}

FSlateColor SResourceTextBox::GetCurrentFontColor() const
{
	return GetResourceStyle()->TextBlock.ColorAndOpacity;
}

EVisibility SResourceTextBox::GetCurrentVisibilityText() const
{
	return GetCurrentFont().Size == 0 ? EVisibility::Collapsed : EVisibility::Visible;
}

const FRTBStyles* SResourceTextBox::GetResourceStyle() const
{
	static FRTBStyles* EmptyStyle = new FRTBStyles();

	//===========================================
	const auto localType = GetValueType();

	//===========================================
	if (Style->DefaultStyles.Contains(localType))
	{
		return &Style->DefaultStyles.FindChecked(localType);
	}

	//===========================================
	ensureMsgf
	(
		false, 
		TEXT("SResourceTextBox::GetResourceStyle(Type: %d):: return nullptr!"), 
		static_cast<int32>(localType)
	);

	//===========================================
	return EmptyStyle;
}
