// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "Widgets/SCompoundWidget.h"
#include "Styles/SphereProgressWidgetStyle.h"

/**
 * 
 */
class SHOOTERGAME_API SSphereProgress 
	: public SCompoundWidget
	, public FGCObject
{
public:
	SLATE_BEGIN_ARGS(SSphereProgress)
		: _Style(&FShooterStyle::Get().GetWidgetStyle<FSphereProgressStyle>("SSphereProgressStyle"))
		, _Progress(.5f)
		, _Rotation(.25f)
	{}
	SLATE_STYLE_ARGUMENT(FSphereProgressStyle, Style)
	SLATE_ATTRIBUTE(FText, Title)
	SLATE_ATTRIBUTE(float, Progress)
	SLATE_ATTRIBUTE(FText, ProgressText)
	SLATE_ATTRIBUTE(float, Rotation)
	SLATE_END_ARGS()

	void Construct(const FArguments& InArgs);

	virtual void AddReferencedObjects(FReferenceCollector& Collector) override;

protected:

	void Tick(const FGeometry& AllottedGeometry, const double InCurrentTime, const float InDeltaTime) override;

	const FSphereProgressStyle* Style;

	TAttribute<float> Attr_Progress, Attr_Rotation;
	TAttribute<FText> Attr_TitleText, Attr_ProgressText;

	UMaterialInstanceDynamic* ProgressBackground;
	UMaterialInstanceDynamic* ProgressForeground;

	FSlateBrush ProgressBackgroundBrush;
	FSlateBrush ProgressForegroundBrush;
};
