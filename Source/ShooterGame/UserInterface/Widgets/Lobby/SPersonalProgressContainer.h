// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "Utilities/SUsableCompoundWidget.h"

class SAnimatedBackground;
class UIdentityComponent;

struct FPersonalProgressData
{
	uint32 OldExperience;
	uint32 NewExperience;
	uint32 MaxExperience;

	uint32 OldELO;
	uint32 NewELO;
};

/**
 * 
 */
class SHOOTERGAME_API SPersonalProgressContainer : public SUsableCompoundWidget
{
public:
	SLATE_BEGIN_ARGS(SPersonalProgressContainer)
	{
		_Visibility = EVisibility::SelfHitTestInvisible;
	}
	SLATE_ARGUMENT(TWeakObjectPtr<UIdentityComponent>, IdentityComponent)
	SLATE_END_ARGS()

	void Construct(const FArguments& InArgs);

	virtual void ToggleWidget(const bool) override;
	virtual void HideWidget() override;
	virtual bool IsInInteractiveMode() const override;

protected:

	TWeakObjectPtr<UIdentityComponent> IdentityComponent;

	TSharedPtr<SAnimatedBackground> Back_Animation;

	float GetExpProgressValue() const;
	float GetExpProgressInvertValue() const;
	TOptional<float> GetExpProgressBarValue() const;
	FText GetExpAnimatedText() const;

	FPersonalProgressData PersonalProgressData;

	FCurveSequence AnimSequence;
	FCurveHandle ProgressHandle;
};
