// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "Utilities/SUsableCompoundWidget.h"
#include "Styles/SearchFightScreenWidgetStyle.h"

class SAnimatedBackground;

class SHOOTERGAME_API SSearchFightScreen 
	: public SUsableCompoundWidget
	, public FGCObject
{
public:
	SLATE_BEGIN_ARGS(SSearchFightScreen)
		: _Style(&FShooterStyle::Get().GetWidgetStyle<FSearchFightScreenStyle>("SSearchFightScreenStyle"))
	{
		_Visibility = EVisibility::SelfHitTestInvisible;
	}
	SLATE_STYLE_ARGUMENT(FSearchFightScreenStyle, Style)
	SLATE_EVENT(FOnClickedOutside, OnCancel)
	SLATE_END_ARGS()

	void Construct(const FArguments& InArgs);

	virtual void ToggleWidget(const bool InToggle) override;
	void SetFound(bool IsFound, int32 InIndex = INDEX_NONE);

	virtual void AddReferencedObjects(FReferenceCollector& Collector) override;

protected:

	struct FMapNameAndImage
	{
		int32 Index;
		FText DisplayName;
		FText Description;
		FSlateBrush Brush;
	};

	const FSearchFightScreenStyle* Style;

	TSharedPtr<SAnimatedBackground> Anim_Background;
	TSharedPtr<SAnimatedBackground> Anim_Image[2];
	TSharedPtr<SAnimatedBackground> Anim_UserWidget[2];
	TSharedPtr<SImage>				Widget_Images[2];

	FOnClickedOutside OnCancel;

	TArray<UUserWidget*>	UserWidgets;

	TArray<FMapNameAndImage> MapImages;
	int32 FoundIndex;
	uint8 CurrentImage;
	float CurrentInterval, CurrentInterval2;
	bool bIsFound;
	bool bIsFirstImage;
	bool bIsFirstUserWidget;

	const FSlateBrush* GetBackground() const;
	FText GetCurrentMapName() const;
	FText GetCurrentMapDesc() const;

	void Tick(const FGeometry& AllottedGeometry, const double InCurrentTime, const float InDeltaTime) override;

	void SwitchImage();
	void SwitchUserWidget();
};
