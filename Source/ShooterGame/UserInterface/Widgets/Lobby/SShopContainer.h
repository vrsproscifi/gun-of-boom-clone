// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "Utilities/SUsableCompoundWidget.h"
#include "Components/SResourceTextBoxValue.h"
#include "ProductItemCategory.h"

class UBasicItemEntity;
class UProductItemEntity;
class STabbedContainer;
class SAdaptiveCarousel;
class SAnimatedBackground;
class UBaseProductItemEntity;

class SShopItem;

DECLARE_DELEGATE_OneParam(FOnItemEntityAction, const UBaseProductItemEntity*);

class SHOOTERGAME_API SShopContainer : public SUsableCompoundWidget
{
public:
	SLATE_BEGIN_ARGS(SShopContainer)
	{
		_Visibility = EVisibility::SelfHitTestInvisible;
	}
	SLATE_EVENT(FOnItemEntityAction, OnRequestBuy)
	SLATE_EVENT(FOnClickedOutside, OnClickedBack)
	SLATE_END_ARGS()

	void Construct(const FArguments& InArgs);

	virtual void ToggleWidget(const bool InToggle) override;
	void ToggleWidget(const bool InToggle, EGameCurrency InTarget);
	virtual bool IsInInteractiveMode() const override;

	void OnFill(const TArray<UBaseProductItemEntity*>& InItems);
	void ScrollTo(UBasicItemEntity* InBaseTargetItem);

protected:

	EVisibility GetDiscountBlockVisibilityState() const;


	TSharedRef<SShopItem> FactoryShopItemWidget(UBaseProductItemEntity* InEntity, const float& fB);


	FOnClickedOutside OnClickedBack;
	FOnItemEntityAction OnRequestBuy;

	TMap<EProductItemCategory, uint8>	TabbedMap;

	TSharedPtr<SAnimatedBackground>		Anim_Back;
	TSharedPtr<STabbedContainer>		Widget_TabbedContainer;
	TSharedPtr<SAdaptiveCarousel>		Widget_ItemsContainer[static_cast<uint8>(EProductItemCategory::End)];

	TWeakObjectPtr<UBaseProductItemEntity> GetActiveEntityItem() const;
	
	template<typename T = UBaseProductItemEntity>
	TWeakObjectPtr<T> GetActiveEntityItem() const
	{
		return Cast<T>(GetActiveEntityItem().Get());
	}

	bool IsItemDiscounted() const;
	EVisibility GetOriginalItemCostVisibility() const;

	FResourceTextBoxValue GetOriginalItemCost() const;
	FResourceTextBoxValue GetActualItemCost() const;

	FReply OnClickedBuyButton();
	void OnDoubleClickedItem();
};
