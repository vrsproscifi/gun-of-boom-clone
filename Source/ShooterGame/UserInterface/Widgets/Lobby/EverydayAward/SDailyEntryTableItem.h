// Fill out your copyright notice in the Description page of Project Settings.

#pragma once
#include "Styling/SlateColor.h"
#include "Math/ColorList.h"
#include "Math/Color.h"
#include "SScaleBox.h"

#include "Utilities/SUsableCompoundWidget.h"
#include "Styles/DailyEntryTableWidgetStyle.h"

class UEverydayAwardEntity;
class UMaterialInstanceDynamic;

class SHOOTERGAME_API SDailyEntryTableItem 
	: public SCompoundWidget
	, public FGCObject
{
public:

	SLATE_BEGIN_ARGS(SDailyEntryTableItem)
		: _Style(&FShooterStyle::Get().GetWidgetStyle<FDailyEntryTableStyle>("SDailyEntryTableStyle"))
	{
		_Visibility = EVisibility::SelfHitTestInvisible;
	}
	SLATE_STYLE_ARGUMENT(FDailyEntryTableStyle, Style)
	SLATE_ATTRIBUTE(int32, CurrentSelectedDay)
	SLATE_END_ARGS()

	void Construct(const FArguments& InArgs, TWeakObjectPtr<UEverydayAwardEntity> InDayEntity);
	void PlayFX();
	void AddReferencedObjects(FReferenceCollector& Collector);

protected:

	const FDailyEntryTableStyle* Style;

	bool bPlayFX;
	float FXTime;
	FSlateBrush FXBrush;
	UMaterialInstanceDynamic* FXMaterial;
	int32 CurrentWidgetDay;
	TAttribute<int32> CurrentSelectedDay;

	enum EDayType
	{
		PrevDay,
		Yasterday,
		NextDay,
		End
	};

	EDayType GetDayType() const;
	
	const FSlateBrush* GetItemImage() const;
	const FSlateBrush* GetHeaderBackground() const;
	const FSlateBrush* GetItemBackground() const;
	FText GetItemName() const;

	virtual void Tick(const FGeometry& AllottedGeometry, const double InCurrentTime, const float InDeltaTime) override;
	virtual int32 OnPaint(const FPaintArgs& Args, const FGeometry& AllottedGeometry, const FSlateRect& MyCullingRect, FSlateWindowElementList& OutDrawElements, int32 LayerId, const FWidgetStyle& InWidgetStyle, bool bParentEnabled) const override;
};
