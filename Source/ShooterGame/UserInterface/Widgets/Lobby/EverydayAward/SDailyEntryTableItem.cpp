// Fill out your copyright notice in the Description page of Project Settings.

#include "ShooterGame.h"
#include "SDailyEntryTableItem.h"
#include "Award/EverydayAward/EverydayAwardEntity.h"

#include "Decorators/LokaTextDecorator.h"
#include "Decorators/LokaResourceDecorator.h"
#include "SlateMaterialBrush.h"

BEGIN_SLATE_FUNCTION_BUILD_OPTIMIZATION
void SDailyEntryTableItem::Construct(const FArguments& InArgs, TWeakObjectPtr<UEverydayAwardEntity> InDayEntity)
{
	CurrentSelectedDay = InArgs._CurrentSelectedDay;
	CurrentWidgetDay = InDayEntity->GetBasicProperty().ModelId;
	Style = InArgs._Style;

	ChildSlot
	[
		SNew(SVerticalBox)
		+ SVerticalBox::Slot()
		.AutoHeight()
		[
			SNew(SBorder).Padding(0).BorderImage(this, &SDailyEntryTableItem::GetHeaderBackground)
			[
				SNew(SRichTextBlock)
				.Margin(FMargin(0, 5))
				.Justification(ETextJustify::Center)
				.Text(InDayEntity->GetBasicProperty().Title)
				.TextStyle(&Style->ItemTitle)
				+ SRichTextBlock::Decorator(FLokaTextDecorator::Create(Style->ItemTitle))
				+ SRichTextBlock::Decorator(FLokaResourceDecorator::Create())
			]
		]
		+ SVerticalBox::Slot()
		[
			SNew(SBorder).Padding(0).BorderImage(this, &SDailyEntryTableItem::GetItemBackground)
			[
				SNew(SOverlay)
				+ SOverlay::Slot()
				[
					SNew(SScaleBox)
					.Stretch(EStretch::ScaleToFit)
					.Visibility(EVisibility::HitTestInvisible)
					[
						SNew(SImage)
						.Image(&InDayEntity->GetEverydayAwardProperty().Image)
					]
				]
				+ SOverlay::Slot()				
				[
					SNew(SRichTextBlock)
					.Justification(ETextJustify::Center)
					.Text(InDayEntity->GetBasicProperty().Description)
					.TextStyle(&Style->ItemDescription)
					+ SRichTextBlock::Decorator(FLokaTextDecorator::Create(Style->ItemDescription))
					+ SRichTextBlock::Decorator(FLokaResourceDecorator::Create())
				]
			]
		]
	];

	bPlayFX = false;
	FXBrush = FSlateNoResource();
}
END_SLATE_FUNCTION_BUILD_OPTIMIZATION

void SDailyEntryTableItem::PlayFX()
{
	FXMaterial = UMaterialInstanceDynamic::Create(Style->FX_Material, GetTransientPackage());
	FXBrush = FSlateMaterialBrush(*FXMaterial, FVector2D(64, 64));
	FXTime = .0f;
	bPlayFX = true;
}

void SDailyEntryTableItem::AddReferencedObjects(FReferenceCollector& Collector)
{
	Collector.AddReferencedObject(FXMaterial);	
}

SDailyEntryTableItem::EDayType SDailyEntryTableItem::GetDayType() const
{
	if (CurrentWidgetDay == CurrentSelectedDay.Get())
	{
		return EDayType::Yasterday;
	}
	else if (CurrentWidgetDay > CurrentSelectedDay.Get())
	{
		return EDayType::NextDay;
	}

	return EDayType::PrevDay;
}

const FSlateBrush* SDailyEntryTableItem::GetHeaderBackground() const
{
	switch (GetDayType()) 
	{
		case PrevDay: return &Style->ItemPrev.Header;
		case NextDay: return &Style->ItemNext.Header;
	}

	return &Style->ItemActive.Header;
}

const FSlateBrush* SDailyEntryTableItem::GetItemBackground() const
{
	switch (GetDayType())
	{
		case PrevDay: return &Style->ItemPrev.Content;
		case NextDay: return &Style->ItemNext.Content;
	}

	return &Style->ItemActive.Content;
}

void SDailyEntryTableItem::Tick(const FGeometry& AllottedGeometry, const double InCurrentTime, const float InDeltaTime)
{
	SCompoundWidget::Tick(AllottedGeometry, InCurrentTime, InDeltaTime);

	if (bPlayFX && IsValidObject(FXMaterial))
	{
		FXTime += InDeltaTime;
		FXMaterial->SetScalarParameterValue(Style->FX_Parameter, FMath::Clamp(FXTime / Style->FX_Duration, 0.0f, 1.0f));
	}
}

int32 SDailyEntryTableItem::OnPaint(const FPaintArgs& Args,
	const FGeometry& AllottedGeometry,
	const FSlateRect& MyCullingRect,
	FSlateWindowElementList& OutDrawElements,
	int32 LayerId,
	const FWidgetStyle& InWidgetStyle,
	bool bParentEnabled) const
{
	int32 LayerIdOut = SCompoundWidget::OnPaint(Args, AllottedGeometry, MyCullingRect, OutDrawElements, LayerId, InWidgetStyle, bParentEnabled);

	if (bPlayFX)
	{
		FSlateDrawElement::MakeBox(OutDrawElements, ++LayerIdOut, AllottedGeometry.ToPaintGeometry(), &FXBrush);
	}

	return LayerIdOut;
}
