// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "ScrollBox.h"
#include "Utilities/SUsableCompoundWidget.h"
#include "Styles/DailyEntryTableWidgetStyle.h"

DECLARE_DELEGATE_OneParam(FOnGetDailyRewardClicked, const int32&/*InCurrentDay*/);

class SHOOTERGAME_API SDailyEntryTable : public SCompoundWidget
{
public:
	SLATE_BEGIN_ARGS(SDailyEntryTable)
		: _Style(&FShooterStyle::Get().GetWidgetStyle<FDailyEntryTableStyle>("SDailyEntryTableStyle"))
	{
		_Visibility = EVisibility::SelfHitTestInvisible;
	}
	SLATE_STYLE_ARGUMENT(FDailyEntryTableStyle, Style)
	SLATE_EVENT(FOnGetDailyRewardClicked, OnGetDailyRewardClicked)
	SLATE_ATTRIBUTE(int32, CurrentSelectedDay)
	SLATE_END_ARGS()

	void Construct(const FArguments& InArgs);
	void SetCurrentDay(const int32& InCurrentDay);
	void RefreshRewards();

protected:

	const FDailyEntryTableStyle* Style;

	FReply OnGetRewardButtonClicked();
	FReply OnGetBonusRewardButtonClicked();
	
	TSharedPtr<class SButton> GetRewardButton;
	TSharedPtr<class SVideoRewardButton> GetVideoRewardButton;
	FOnGetDailyRewardClicked OnGetDailyRewardClicked;

	TArray<class UEverydayAwardEntity*> DayRewards;
	TAttribute<int32> CurrentSelectedDay;
	TSharedPtr<class SScrollBox> DaysContainer;
	int32 CurrentDay;
	TSharedPtr<class SDailyEntryTableItem> CurrentDayItemWidget;
};
