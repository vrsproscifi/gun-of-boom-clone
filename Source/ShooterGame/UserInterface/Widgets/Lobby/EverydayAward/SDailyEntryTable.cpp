// Fill out your copyright notice in the Description page of Project Settings.

#include "ShooterGame.h"
#include "SDailyEntryTable.h"
#include "SDailyEntryTableItem.h"
#include "EverydayAwardEntity.h"
#include "Extensions/GameSingletonExtensions.h"

#include "Decorators/LokaTextDecorator.h"
#include "Decorators/LokaResourceDecorator.h"
#include "Award/EverydayAward/EverydayAwardEntity.h"
#include "MessageBoxWidgetStyle.h"

#include "Input/SVideoRewardButton.h"
#include "Notify/SMessageBox.h"


BEGIN_SLATE_FUNCTION_BUILD_OPTIMIZATION
void SDailyEntryTable::Construct(const FArguments& InArgs)
{
	Style = InArgs._Style;

	DayRewards = FGameSingletonExtensions::GetEverydayAwards();

	CurrentSelectedDay = InArgs._CurrentSelectedDay;
	OnGetDailyRewardClicked = InArgs._OnGetDailyRewardClicked;
	const auto MessageBoxStyle = &FShooterStyle::Get().GetWidgetStyle<FMessageBoxStyle>("SMessageBoxStyle");

	ChildSlot
	[
		SNew(SVerticalBox)
		+ SVerticalBox::Slot().AutoHeight()
		.Padding(0, 20)
		[
			SNew(SRichTextBlock)
			.Justification(ETextJustify::Center)
			.Text(NSLOCTEXT("DailyReward", "UArsenalComponent.OnPlayerEntryReward.Description", "Come into the game every day and get interesting and nice bonuses. Do not miss your chance to get money, boosters or cool weapons!"))
			.TextStyle(&MessageBoxStyle->Content.Text)
			.AutoWrapText(true)
			+ SRichTextBlock::Decorator(FLokaTextDecorator::Create(MessageBoxStyle->Content.Text))
			+ SRichTextBlock::Decorator(FLokaResourceDecorator::Create())
		]
		+ SVerticalBox::Slot()
		.Padding(0)
		[
			SAssignNew(DaysContainer, SScrollBox)
			.Orientation(Orient_Horizontal)
			.ScrollBarVisibility(EVisibility::Collapsed)
		]
		+ SVerticalBox::Slot()
		.Padding(400, 30)
		.AutoHeight()
		[
			SAssignNew(GetRewardButton, SButton)
			.OnClicked(this, &SDailyEntryTable::OnGetRewardButtonClicked)
			.ButtonStyle(&FShooterStyle::Get().GetWidgetStyle<FMessageBoxStyle>("SMessageBoxStyle").Buttons.Button)
			.ContentPadding(FMargin(20, 12))
			[
				SNew(STextBlock)
				.TextStyle(&FShooterStyle::Get().GetWidgetStyle<FMessageBoxStyle>("SMessageBoxStyle").Buttons.Text)
				.Text(NSLOCTEXT("DailyReward", "UArsenalComponent.OnPlayerEntryReward.Button", "Get reward now!"))
				.Justification(ETextJustify::Center)
			]
		]
		+ SVerticalBox::Slot()
		.Padding(400, 30)
		.AutoHeight()
		[
			SAssignNew(GetVideoRewardButton, SVideoRewardButton)
			.OnClicked(this, &SDailyEntryTable::OnGetBonusRewardButtonClicked)
			.Visibility(EVisibility::Collapsed)
		]
	];

	RefreshRewards();
}
END_SLATE_FUNCTION_BUILD_OPTIMIZATION

FReply SDailyEntryTable::OnGetBonusRewardButtonClicked()
{
	OnGetDailyRewardClicked.ExecuteIfBound(CurrentSelectedDay.Get());
	SMessageBox::Get()->ToggleWidget(false);
	return FReply::Handled();
}

FReply SDailyEntryTable::OnGetRewardButtonClicked()
{
	//===================================
	if(GetRewardButton.IsValid())
	{
		GetRewardButton.Get()->SetVisibility(EVisibility::Collapsed);
	}

	//===================================
	if (GetVideoRewardButton.IsValid())
	{
		GetVideoRewardButton.Get()->SetVisibility(EVisibility::All);
	}

	if (Style->RewardSound.GetResourceObject())
	{
		FSlateApplication::Get().PlaySound(Style->RewardSound);
	}

	if (CurrentDayItemWidget.IsValid())
	{
		CurrentDayItemWidget->PlayFX();
	}

	//===================================
	//auto Result = DayRewards.FindByPredicate([&, SelectedDay = CurrentSelectedDay.Get()](const UEverydayAwardEntity* InEverydayAwardEntity)
	//{
	//	return IsValidObject(InEverydayAwardEntity) && InEverydayAwardEntity->GetModelId() == SelectedDay;
	//});
	//
	////===================================
	//if(Result && *Result && Result->)
	//{
	//	
	//}
	return FReply::Handled();
}

void SDailyEntryTable::SetCurrentDay(const int32& InCurrentDay)
{
	CurrentSelectedDay = InCurrentDay;
}

void SDailyEntryTable::RefreshRewards()
{
	DaysContainer->ClearChildren();

	TSharedPtr<SBox> CurrentDayWidget;

	for (auto Day : DayRewards)
	{
		TSharedPtr<SBox> Widget;
		DaysContainer->AddSlot().Padding(2, 0)
		[
			SAssignNew(Widget, SBox).WidthOverride(250).HeightOverride(300)
			[
				SNew(SDailyEntryTableItem, Day).CurrentSelectedDay(CurrentSelectedDay)
			]
		];

		if (Day && Day->GetModelId() == CurrentSelectedDay.Get())
		{
			CurrentDayWidget = Widget;
			CurrentDayItemWidget = StaticCastSharedRef<SDailyEntryTableItem>(CurrentDayWidget->GetChildren()->GetChildAt(0));
		}
	}

	if (CurrentDayWidget.IsValid())
	{
		DaysContainer->ScrollDescendantIntoView(CurrentDayWidget, false, EDescendantScrollDestination::Center);
	}
}
