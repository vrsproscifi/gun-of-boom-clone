// Fill out your copyright notice in the Description page of Project Settings.

#include "ShooterGame.h"
#include "SPersonalProgressContainer.h"
#include "SlateOptMacros.h"
#include "Utilities/SAnimatedBackground.h"
#include "Utilities/SlateUtilityTypes.h"
#include "Localization/TableBaseStrings.h"

BEGIN_SLATE_FUNCTION_BUILD_OPTIMIZATION
void SPersonalProgressContainer::Construct(const FArguments& InArgs)
{
	IdentityComponent = InArgs._IdentityComponent;

	ChildSlot
	[
		SAssignNew(Back_Animation, SAnimatedBackground)
		.Visibility(EVisibility::Visible)
		.IsEnabledBlur(true)
		.Duration(.2f)
		.ShowAnimation(EAnimBackAnimation::UpFade)
		.HideAnimation(EAnimBackAnimation::DownFade)
		.InitAsHide(true)
		[
			SNew(SOverlay)
			+ SOverlay::Slot()
			[
				SNew(SHorizontalBox)
				+ SHorizontalBox::Slot()
				+ SHorizontalBox::Slot().VAlign(VAlign_Bottom).Padding(0, 20)
				[
					SNew(SButton)
					.ContentPadding(FMargin(20))
					.TextStyle(&FShooterStyle::Get().GetWidgetStyle<FTextBlockStyle>("Default_ActionText"))
					.ButtonStyle(&FShooterStyle::Get().GetWidgetStyle<FButtonStyle>("Default_ActionButton"))
					.HAlign(HAlign_Center)
					.VAlign(VAlign_Center)
					.Text(FTableBaseStrings::GetBaseText(EBaseStrings::Continue))
					.OnClicked_Lambda([&]()
					{
						HideWidget();
						return FReply::Handled();
					})
				]
				+ SHorizontalBox::Slot()
			]
			+ SOverlay::Slot().HAlign(HAlign_Center).VAlign(VAlign_Center)
			[
				SNew(SOverlay)
				+ SOverlay::Slot()
				[
					SNew(SBox).WidthOverride(400)
					[
						SNew(SProgressBar).Percent(this, &SPersonalProgressContainer::GetExpProgressBarValue)
					]
				]
				+ SOverlay::Slot()
				[
					SNew(SSplitter)
					.Clipping(EWidgetClipping::Inherit)
					+ SSplitter::Slot().Value(TAttribute<float>(this, &SPersonalProgressContainer::GetExpProgressValue))
					+ SSplitter::Slot().Value(TAttribute<float>(this, &SPersonalProgressContainer::GetExpProgressInvertValue))
					[
						SNew(STextBlock).Text(this, &SPersonalProgressContainer::GetExpAnimatedText).Clipping(EWidgetClipping::Inherit)
					]
				]
			]
		]
	];

	AnimSequence = FCurveSequence();

	ProgressHandle = AnimSequence.AddCurve(0, 3, ECurveEaseFunction::QuadInOut);

	PersonalProgressData.OldExperience = 500;
	PersonalProgressData.NewExperience = 1200;
	PersonalProgressData.MaxExperience = 1500;
}
END_SLATE_FUNCTION_BUILD_OPTIMIZATION

void SPersonalProgressContainer::ToggleWidget(const bool InToggle)
{
	if (InToggle)
	{
		if (FWidgetOrderedSupportQueue::GetCurrentQueueId() == TEXT("SPersonalFightResult"))
		{
			SUsableCompoundWidget::ToggleWidget(InToggle);
			Back_Animation->ToggleWidget(InToggle);
			AnimSequence.Play(AsShared());
		}

		FWidgetOrderedSupportQueue::AddQueue(TEXT("SPersonalFightResult"), FOnClickedOutside::CreateLambda([&, t = InToggle]() {
			SUsableCompoundWidget::ToggleWidget(t);
			Back_Animation->ToggleWidget(t);
			AnimSequence.Play(AsShared());
		}), ORDERED_QUEUE_PERSONALPROGRESS, true);
	}
	else
	{
		SUsableCompoundWidget::ToggleWidget(InToggle);
		Back_Animation->ToggleWidget(InToggle);
	}
}

void SPersonalProgressContainer::HideWidget()
{
	SUsableCompoundWidget::HideWidget();
	FWidgetOrderedSupportQueue::RemoveQueue(TEXT("SPersonalFightResult"));
}

bool SPersonalProgressContainer::IsInInteractiveMode() const
{
	return Back_Animation->IsAnimAtEnd();
}

float SPersonalProgressContainer::GetExpProgressValue() const
{
	const float CurrentMin = float(PersonalProgressData.OldExperience) / float(PersonalProgressData.MaxExperience);
	const float CurrentMax = float(PersonalProgressData.NewExperience) / float(PersonalProgressData.MaxExperience);

	return FMath::Lerp(CurrentMin, CurrentMax, ProgressHandle.GetLerp());
}

float SPersonalProgressContainer::GetExpProgressInvertValue() const
{
	return 1.0f - GetExpProgressValue();
}

TOptional<float> SPersonalProgressContainer::GetExpProgressBarValue() const
{
	return GetExpProgressValue();
}

FText SPersonalProgressContainer::GetExpAnimatedText() const
{
	return FText::Format(FText::FromString("| {0}"), FText::AsNumber(FMath::Lerp(PersonalProgressData.OldExperience, PersonalProgressData.NewExperience, ProgressHandle.GetLerp()), &FNumberFormattingOptions::DefaultNoGrouping()));
}
