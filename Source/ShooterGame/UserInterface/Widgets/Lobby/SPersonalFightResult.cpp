// Fill out your copyright notice in the Description page of Project Settings.

#include "ShooterGame.h"
#include "SPersonalFightResult.h"
#include "SlateOptMacros.h"
#include "Localization/TableBaseStrings.h"
#include "Components/SResourceTextBox.h"
#include "Utilities/SAnimatedBackground.h"
#include "ProgressComponent/GameMatchInformation.h"
#include "ShooterGameAchievements.h"
#include "GameSingletonExtensions.h"
#include "BasicGameAchievement.h"
#include "Components/SShopItem.h"
#include "Item/BasicItemEntity.h"
#include "SScaleBox.h"
#include "ShooterGameAnalytics.h"
#include "Utilities/SlateUtilityTypes.h"
#include "Decorators/LokaTextDecorator.h"
#include "Decorators/LokaResourceDecorator.h"
#include "Input/SVideoRewardButton.h"

BEGIN_SLATE_FUNCTION_BUILD_OPTIMIZATION

void SPersonalFightResult::Construct(const FArguments& InArgs)
{
	Style = InArgs._Style;
	OnRequestApplyMatchPremiumAccountBonus = InArgs._OnRequestApplyMatchPremiumAccountBonus;
	OnRequestApplyMatchAdsBonus = InArgs._OnRequestApplyMatchAdsBonus;
	OnPersonalFightResultClosed = InArgs._OnPersonalFightResultClosed;

	FText PremiumName = FText::GetEmpty();
	const FSlateBrush* PremiumImage = new FSlateNoResource();

	PremiumItemEntity = FGameSingletonExtensions::FindItemById(Style->PremiumModels[FMath::RandRange(0, Style->PremiumModels.Num() - 1)]);
	if (IsValidObject(PremiumItemEntity) == false)
	{
		PremiumItemEntity = FGameSingletonExtensions::FindItemById(Style->PremiumModels[FMath::RandRange(0, Style->PremiumModels.Num() - 1)]);
	}

	if (PremiumItemEntity)
	{
		PremiumImage = &PremiumItemEntity->GetItemProperty().Image;
		PremiumName = PremiumItemEntity->GetItemProperty().Title;
	}


	ChildSlot
	[
		SAssignNew(Back_Animation, SAnimatedBackground)
		.Visibility(EVisibility::Visible)
		.IsEnabledBlur(true)
		.Duration(.2f)
		.ShowAnimation(EAnimBackAnimation::UpFade)
		.HideAnimation(EAnimBackAnimation::DownFade)
		.InitAsHide(true)
		[
			SNew(SVerticalBox)
			+ SVerticalBox::Slot().AutoHeight().VAlign(VAlign_Center).HAlign(HAlign_Center)
			[
				SAssignNew(Widget_WinStatus, STextBlock)
				.Margin(10)
				.TextStyle(&Style->StatusText)
			]
			+ SVerticalBox::Slot()
			[
				SNew(SHorizontalBox)
				+ SHorizontalBox::Slot().VAlign(VAlign_Top).HAlign(HAlign_Center)
				[
					SNew(SFxWidget)
					.ColorAndOpacity(this, &SPersonalFightResult::GetSidesColor)
					.VisualOffset(this, &SPersonalFightResult::GetLeftSideOffset)
					[
						SNew(SWidgetSwitcher)
						.WidgetIndex(this, &SPersonalFightResult::GetActiveRewardIndex)
						+ SWidgetSwitcher::Slot()
						[
							SNew(SVerticalBox)
							+ SVerticalBox::Slot().VAlign(VAlign_Center).HAlign(HAlign_Center).AutoHeight()
							[
								SNew(SBox).HeightOverride(100).VAlign(VAlign_Center)
								[
									SNew(STextBlock)
									.Text(NSLOCTEXT("SPersonalFightResult", "SPersonalFightResult.GetBonus", "Get Bonus"))
									.TextStyle(&FShooterStyle::Get().GetWidgetStyle<FTextBlockStyle>("Default_HeaderText"))
								]
							]
							+ SVerticalBox::Slot()
							[
								SNew(SOverlay)
								+ SOverlay::Slot().HAlign(HAlign_Left)
								[
									SNew(SScaleBox)
									.Stretch(EStretch::ScaleToFit)
									[
										SNew(SImage)
										.Image(PremiumImage)
										.ColorAndOpacity(FColor::White.WithAlpha(100))
									]
								]
								+ SOverlay::Slot().VAlign(VAlign_Top).HAlign(HAlign_Center)
								[
									SNew(SBorder)
									.BorderImage(&Style->RewardBG)
									.Padding(4)
									[
										SAssignNew(Widget_RewardX2Premium, STextBlock)
										.Text(this, &SPersonalFightResult::GetPremeiumFightResultMultiplerText)
										.TextStyle(&Style->RewardText)
									]
								]
								+ SOverlay::Slot().VAlign(VAlign_Bottom).HAlign(HAlign_Center)
								[
									SNew(SBorder)
									.BorderImage(&Style->Days7BG)
									.Padding(4)
									[
										SNew(SRichTextBlock)
										.Justification(ETextJustify::Center)
										.Text(PremiumName)
										.TextStyle(&Style->Days7Text)
										.AutoWrapText(true)
										+ SRichTextBlock::Decorator(FLokaTextDecorator::Create(Style->Days7Text))
										+ SRichTextBlock::Decorator(FLokaResourceDecorator::Create())
									]
								]
								+ SOverlay::Slot().VAlign(VAlign_Center).HAlign(HAlign_Center)
								[
									SNew(SVerticalBox)
									+ SVerticalBox::Slot().AutoHeight().Padding(4)
									[
										SAssignNew(Widget_RewardMoneyBonus, SResourceTextBox)
										.CustomSize(FVector(64, 64, 32))
										.Type(EResourceTextBoxType::Money)
									]
									+ SVerticalBox::Slot().AutoHeight().Padding(4)
									[
										SAssignNew(Widget_RewardExpBonus, SResourceTextBox)
										.CustomSize(FVector(64, 64, 32))
										.Type(EResourceTextBoxType::Exp)
									]
								]
							]
							+ SVerticalBox::Slot().AutoHeight().Padding(20, 0, 20, 20)
							[
								SNew(SButton)
								.ButtonStyle(&Style->ButtonStyle)
								.OnClicked(this, &SPersonalFightResult::OnRequestApplyMatchPremiumAccountBonusClicked)
								.HAlign(HAlign_Center)
								.VAlign(VAlign_Center)
								.ContentPadding(4)
								[
									SNew(SBox).HeightOverride(80).HAlign(HAlign_Center).VAlign(VAlign_Center)
									[
										SNew(SResourceTextBox)
										.CustomSize(FVector(64, 64, 28))
										.TypeAndValue_Lambda([&]()
										{
											if (PremiumItemEntity && PremiumItemEntity->IsValidLowLevel())
											{
												const auto cost = PremiumItemEntity->GetItemCost();
												return FResourceTextBoxValue::Factory(cost);
											}

											return FResourceTextBoxValue();
										})
									]
								]
							]
						]
						+ SWidgetSwitcher::Slot()
						[
							SNew(SVerticalBox)
							+ SVerticalBox::Slot().VAlign(VAlign_Center).HAlign(HAlign_Center).AutoHeight()
							[
								SNew(SBox).HeightOverride(100).VAlign(VAlign_Center)
								[
									SNew(STextBlock)
									.Text(NSLOCTEXT("SPersonalFightResult", "SPersonalFightResult.Reward", "Reward"))
									.TextStyle(&FShooterStyle::Get().GetWidgetStyle<FTextBlockStyle>("Default_HeaderText"))
								]
							]
							+ SVerticalBox::Slot()
							.Padding(10)
							[
								SAssignNew(Widget_RewardList, SListView<TSharedPtr<FFightResultRewardInfo>>)
								.ListItemsSource(&RewardInfoList)
								.SelectionMode(ESelectionMode::None)
								.OnGenerateRow_Lambda([&](TSharedPtr<FFightResultRewardInfo> InItem, const TSharedRef<STableViewBase>& InOwnerTable)
								{
									if (InItem->Experience && InItem->Money && InItem->Title.IsEmptyOrWhitespace() == false)
									{
										return SNew(STableRow<TSharedPtr<FFightResultRewardInfo>>, InOwnerTable).Style(&Style->TableRow).Padding(FMargin(4, 6))
										[
											SNew(SHorizontalBox)
											+ SHorizontalBox::Slot().VAlign(VAlign_Center).Padding(5).FillWidth(4.5f)
											[
												SNew(STextBlock).Text(InItem->Title).TextStyle(&Style->ColumnName).MinDesiredWidth(200)
											]
											+ SHorizontalBox::Slot().HAlign(HAlign_Right).VAlign(VAlign_Center).Padding(5).AutoWidth()
											[
												SNew(SResourceTextBox)
												.MinDesiredWidth(64)
												.Type(EResourceTextBoxType::Money)
												.Value(InItem->Money)
											]
											+ SHorizontalBox::Slot().HAlign(HAlign_Right).VAlign(VAlign_Center).Padding(5).AutoWidth()
											[
												SNew(SResourceTextBox)
												.MinDesiredWidth(64)
												.Type(EResourceTextBoxType::Exp)
												.Value(InItem->Experience)
											]
										];
									}
									return SNew(STableRow<TSharedPtr<FFightResultRewardInfo>>, InOwnerTable).Style(&Style->TableRow).Padding(FMargin(4, 3));
								})
							]
						]
					]
				]
				+ SHorizontalBox::Slot()
				[
					SNew(SVerticalBox)
					+ SVerticalBox::Slot().VAlign(VAlign_Center).HAlign(HAlign_Center).AutoHeight()
					[
						SNew(SBox).HeightOverride(100).VAlign(VAlign_Center)
						[
							SNew(STextBlock)
							.Text(NSLOCTEXT("SPersonalFightResult", "SPersonalFightResult.Reward", "Reward"))
							.TextStyle(&FShooterStyle::Get().GetWidgetStyle<FTextBlockStyle>("Default_HeaderText"))
						]
					]
					+ SVerticalBox::Slot()
					[
						SNew(SOverlay)
						+ SOverlay::Slot().VAlign(VAlign_Top).HAlign(HAlign_Center)
						[
							SNew(SDPIScaler)
							.DPIScale(this, &SPersonalFightResult::GetMultiplerScale)
							[
								SNew(SBorder)
								.Visibility(this, &SPersonalFightResult::GetHasPremiumVis)
								.BorderImage(&Style->RewardBG)
								.Padding(4)
								[
									SAssignNew(Widget_RewardX2, STextBlock)			
									.Text(this, &SPersonalFightResult::GetFightResultMultiplerText)
									.TextStyle(&Style->RewardText)
								]
							]
						]
						+ SOverlay::Slot().VAlign(VAlign_Center).HAlign(HAlign_Center)
						[
							SNew(SDPIScaler)
							.DPIScale(this, &SPersonalFightResult::GetNumbersScale)
							[
								SNew(SVerticalBox)
								+ SVerticalBox::Slot().AutoHeight().Padding(4)
								[
									SNew(SResourceTextBox)
									.CustomSize(FVector(64, 64, 32))
									.Type(EResourceTextBoxType::Money)
									.Value(this, &SPersonalFightResult::GetMoneyReward)
								]
								+ SVerticalBox::Slot().AutoHeight().Padding(4)
								[
									SNew(SResourceTextBox)
									.CustomSize(FVector(64, 64, 32))
									.Type(EResourceTextBoxType::Exp)
									.Value(this, &SPersonalFightResult::GetExpReward)
								]
								+ SVerticalBox::Slot().AutoHeight().Padding(4)
								[
									SNew(SSeparator)
								]
								+ SVerticalBox::Slot().AutoHeight().Padding(4)
								[
									SAssignNew(Widget_RewardElo, SResourceTextBox)
									.CustomSize(FVector(64, 64, 32))
									.Type(EResourceTextBoxType::Elo)
								]
							]
						]
					]
					+ SVerticalBox::Slot().AutoHeight().Padding(20, 0, 20, 20)
					[
						SNew(SButton)
						.ContentPadding(FMargin(20))
						.TextStyle(&Style->ButtonText)
						.ButtonStyle(&FShooterStyle::Get().GetWidgetStyle<FButtonStyle>("Default_ActionButton"))
						.HAlign(HAlign_Center)
						.VAlign(VAlign_Center)
						.Text(FTableBaseStrings::GetBaseText(EBaseStrings::Continue))
						.OnClicked_Lambda([&]()
						{
							HideWidget();
							OnPersonalFightResultClosed.ExecuteIfBound();
							return FReply::Handled();
						})
					]					
				]
				+ SHorizontalBox::Slot().Padding(0, 0, 40, 0)
				[
					SNew(SFxWidget)
					.ColorAndOpacity(this, &SPersonalFightResult::GetSidesColor)
					.VisualOffset(this, &SPersonalFightResult::GetRightSideOffset)
					[
						SNew(SVerticalBox)
						+ SVerticalBox::Slot().VAlign(VAlign_Center).HAlign(HAlign_Center).AutoHeight()
						[
							SNew(SBox).HeightOverride(100).VAlign(VAlign_Center)
							[
								SNew(STextBlock)
								.Text(NSLOCTEXT("SPersonalFightResult", "SPersonalFightResult.Achiv", "Achievements"))
								.TextStyle(&FShooterStyle::Get().GetWidgetStyle<FTextBlockStyle>("Default_HeaderText"))
							]
						]
						+ SVerticalBox::Slot()
						[
							SAssignNew(Widget_AchievementsList, SListView<TSharedPtr<FPlayerGameAchievement>>)
							.ListItemsSource(&AchievementsList)
							.SelectionMode(ESelectionMode::None)
							.OnGenerateRow_Lambda([&](TSharedPtr<FPlayerGameAchievement> InItem, const TSharedRef<STableViewBase>& InOwnerTable)
							{
								if (auto AchivItem = FGameSingletonExtensions::FindAchievementById(InItem->Key))
								{
									return SNew(STableRow<TSharedPtr<FPlayerGameAchievement>>, InOwnerTable).Style(&Style->TableRow).Padding(FMargin(4, 6))
									[
										SNew(SHorizontalBox)
										+ SHorizontalBox::Slot().FillWidth(.2f).VAlign(VAlign_Center)
										[
											SNew(STextBlock).Text(FText::Format(FText::FromString("x{0}"), FText::AsNumber(InItem->Value, &FNumberFormattingOptions::DefaultNoGrouping()))).TextStyle(&Style->ColumnAmount)
										]
										+ SHorizontalBox::Slot().FillWidth(1.0f).VAlign(VAlign_Center)
										[
											SNew(STextBlock).Text(AchivItem->GetAchievementProperty().Title).TextStyle(&Style->ColumnName).AutoWrapText(true)
										]
										+ SHorizontalBox::Slot().FillWidth(.6f).HAlign(HAlign_Right).VAlign(VAlign_Center)
										[
											SNew(SResourceTextBox)
											.Type(EResourceTextBoxType::ScorePoints)
											.Value(AchivItem->GetAchievementProperty().ScorePoints * InItem->Value)
										]
									];
								}

								return SNew(STableRow<TSharedPtr<FPlayerGameAchievement>>, InOwnerTable).Style(&Style->TableRow);
							})
						]
						+ SVerticalBox::Slot().AutoHeight().Padding(20, 0, 20, 20)
						[
							SNew(SVideoRewardButton)
							.Visibility(this, &SPersonalFightResult::GetExtraRewardVis)
							.OnClicked(this, &SPersonalFightResult::OnRequestApplyMatchAdsBonusClicked)
						]
					]
				]
			]
		]
	];

	AnimSequence = FCurveSequence();
	HandleNumbers = AnimSequence.AddCurve(.0f, Style->NumbersDuration, ECurveEaseFunction::Linear);
	HandleMultipler = AnimSequence.AddCurve(.0f, Style->MultiplerDuration, ECurveEaseFunction::Linear);
	HandleOther = AnimSequence.AddCurveRelative(-.1f, Style->OtherDuration, ECurveEaseFunction::Linear);
}
END_SLATE_FUNCTION_BUILD_OPTIMIZATION



FReply SPersonalFightResult::OnRequestApplyMatchAdsBonusClicked()
{
	//=========================================
	if (OnRequestApplyMatchAdsBonus.ExecuteIfBound(PlayerMatchMember.MemberId) == false)
	{
		FShooterGameAnalytics::RecordErrorEvent(EErrorSeverity::warning, "OnRequestApplyMatchAdsBonus | Event not bound");
	}

	return FReply::Handled();
}

FReply SPersonalFightResult::OnRequestApplyMatchPremiumAccountBonusClicked()
{
	//=========================================
	int32 TargetPremiumAccount = 0;

	//=========================================
	if (IsValidObject(PremiumItemEntity) == false)
	{
		FShooterGameAnalytics::RecordErrorEvent(EErrorSeverity::warning, "OnRequestBuyPremium::PremiumItemEntity::Null");
		PremiumItemEntity = FGameSingletonExtensions::FindItemById(Style->PremiumModels[FMath::RandRange(0, Style->PremiumModels.Num() - 1)]);
	}

	//=========================================
	if (IsValidObject(PremiumItemEntity))
	{
		TargetPremiumAccount = PremiumItemEntity->GetModelId();
	}
	else
	{
		FShooterGameAnalytics::RecordErrorEvent(EErrorSeverity::error, "OnRequestBuyPremium::PremiumItemEntity::Null2");
	}

	//=========================================
	const bool bHasUsedPremiumAccount = HasUsedPremiumAccount();

	//=========================================
	if (OnRequestApplyMatchPremiumAccountBonus.ExecuteIfBound(PlayerMatchMember.MemberId, bHasUsedPremiumAccount, TargetPremiumAccount) == false)
	{
		FShooterGameAnalytics::RecordErrorEvent(EErrorSeverity::warning, "OnRequestApplyMatchPremiumAccountBonus | Event not bound");
	}

	return FReply::Handled();
}

EVisibility SPersonalFightResult::GetHasPremiumVis() const
{
	return (GetFightResultMultipler() > 1.0) ? EVisibility::Visible : EVisibility::Collapsed;
}

EVisibility SPersonalFightResult::GetExtraRewardVis() const
{
	return HasUsedAdsBooster() ? EVisibility::Collapsed : EVisibility::Visible;
}

int64 SPersonalFightResult::GetMoneyReward() const
{
	if (IsValidObject(Style->NumbersLerpCurve))
	{
		return FMath::Lerp<int64>(0, CachedMoney, Style->NumbersLerpCurve->GetFloatValue(HandleNumbers.GetLerp()));
	}

	return FMath::Lerp<int64>(0, CachedMoney, HandleNumbers.GetLerp());
}

int64 SPersonalFightResult::GetExpReward() const
{
	if (IsValidObject(Style->NumbersLerpCurve))
	{
		return FMath::Lerp<int64>(0, CachedExp, Style->NumbersLerpCurve->GetFloatValue(HandleNumbers.GetLerp()));
	}

	return FMath::Lerp<int64>(0, CachedExp, HandleNumbers.GetLerp());
}

float SPersonalFightResult::GetNumbersScale() const
{
	if (IsValidObject(Style->NumbersScaleCurve))
	{
		return Style->NumbersScaleCurve->GetFloatValue(HandleNumbers.GetLerp());
	}

	return FMath::Lerp<float>(.0f, 1.0f, HandleNumbers.GetLerp());
}

float SPersonalFightResult::GetMultiplerScale() const
{
	if (IsValidObject(Style->MultiplerScaleCurve))
	{
		return Style->MultiplerScaleCurve->GetFloatValue(HandleMultipler.GetLerp());
	}

	return FMath::Lerp<float>(.0f, 1.0f, HandleMultipler.GetLerp());
}

FLinearColor SPersonalFightResult::GetSidesColor() const
{
	if (IsValidObject(Style->OtherCurve))
	{
		return FLinearColor(1.0f, 1.0f, 1.0f, Style->OtherCurve->GetVectorValue(HandleOther.GetLerp()).Z);
	}

	return FLinearColor(1.0f, 1.0f, 1.0f, FMath::Lerp<float>(.0f, 1.0f, HandleOther.GetLerp()));
}

FVector2D SPersonalFightResult::GetLeftSideOffset() const
{
	if (IsValidObject(Style->OtherCurve))
	{
		return FVector2D(Style->OtherCurve->GetVectorValue(HandleOther.GetLerp()).X, .0f);
	}

	return FVector2D(FMath::Lerp<float>(.0f, 1.0f, HandleOther.GetLerp()), .0f);
}

FVector2D SPersonalFightResult::GetRightSideOffset() const
{
	if (IsValidObject(Style->OtherCurve))
	{
		return FVector2D(-Style->OtherCurve->GetVectorValue(HandleOther.GetLerp()).X, .0f);
	}

	return FVector2D(FMath::Lerp<float>(.0f, 1.0f, HandleOther.GetLerp()), .0f);
}

void SPersonalFightResult::ToggleWidget(const bool InToggle)
{
	if (InToggle)
	{
#if WITH_EDITOR
		SUsableCompoundWidget::ToggleWidget(InToggle);
		Back_Animation->ToggleWidget(InToggle);
		AnimSequence.Play(AsShared());
#else
		if (FWidgetOrderedSupportQueue::GetCurrentQueueId() == TEXT("SPersonalFightResult"))
		{
			SUsableCompoundWidget::ToggleWidget(InToggle);
			Back_Animation->ToggleWidget(InToggle);
			AnimSequence.Play(AsShared());
		}

		FWidgetOrderedSupportQueue::AddQueue(TEXT("SPersonalFightResult"), FOnClickedOutside::CreateLambda([&, t = InToggle]() {
			SUsableCompoundWidget::ToggleWidget(t);
			Back_Animation->ToggleWidget(t);
			AnimSequence.Play(AsShared());
		}), ORDERED_QUEUE_PERSONALFIGHTRESULT);
#endif
	}
	else
	{
		SUsableCompoundWidget::ToggleWidget(InToggle);
		Back_Animation->ToggleWidget(InToggle);
	}	
}

bool SPersonalFightResult::IsInInteractiveMode() const
{
	return Back_Animation->IsAnimAtEnd();
}

void SPersonalFightResult::HideWidget()
{
	SUsableCompoundWidget::HideWidget();
	FWidgetOrderedSupportQueue::RemoveQueue(TEXT("SPersonalFightResult"));
}

int32 SPersonalFightResult::GetActiveRewardIndex() const
{
	if(HasUsedPremiumAccount())
	{
		return 1;
	}
	return 0;
}

bool SPersonalFightResult::HasUsedPremiumAccount() const
{
	return PlayerMatchMember.HasUsedPremiumAccount();
}

bool SPersonalFightResult::HasUsedEveryDayBooster() const
{
	return PlayerMatchMember.HasUsedEveryDayBooster();
}

bool SPersonalFightResult::HasUsedAdsBooster() const
{
	return PlayerMatchMember.HasUsedAdsBooster();
}

bool SPersonalFightResult::HasUsedPrimeTimeBooster() const
{
	return PlayerMatchMember.HasUsedPrimeTimeBooster();
}

void SPersonalFightResult::OnApplyMatchPremiumAccountBonus()
{
	PlayerMatchMember.SetUsedPremiumAccount();
	UpdateDisplayReward();
}

void SPersonalFightResult::OnApplyMatchAdsBonus()
{
	PlayerMatchMember.SetUsedAdsBonus();
	UpdateDisplayReward();
}

double SPersonalFightResult::GetPremeiumFightResultMultipler() const
{
	return GetFightResultMultipler() + 1.0f;
}


double SPersonalFightResult::GetFightResultMultipler() const
{
	//================================
	double multipler = 1.0f;

	//================================
	if (HasUsedPremiumAccount())
	{
		multipler += 1.0f;
	}

	//================================
	if (HasUsedEveryDayBooster())
	{
		multipler += 1.0f;
	}

	//================================
	if (HasUsedPrimeTimeBooster())
	{
		multipler += 0.5f;
	}

	//================================
	if (HasUsedAdsBooster())
	{
		multipler += 1.0f;
	}

	//================================
	return multipler;
}

FText SPersonalFightResult::GetFightResultMultiplerText() const
{
	const auto ResultMultipler = GetFightResultMultipler();
	return FText::Format(NSLOCTEXT("Bonus", "Bonus.RewardX", "Reward x{0}"), FText::AsNumber(ResultMultipler, &FNumberFormattingOptions::DefaultNoGrouping()));
}

FText SPersonalFightResult::GetPremeiumFightResultMultiplerText() const
{
	const auto ResultMultiplerPremium = GetPremeiumFightResultMultipler();
	return FText::Format(NSLOCTEXT("Bonus", "Bonus.RewardX", "Reward x{0}"), FText::AsNumber(ResultMultiplerPremium, &FNumberFormattingOptions::DefaultNoGrouping()));
}

void SPersonalFightResult::UpdateDisplayReward()
{		
	//================================
	const auto ResultMultipler = GetFightResultMultipler();
	const auto ResultMultiplerPremium = GetPremeiumFightResultMultipler();

	//================================
	int64 Money = 0;
	int64 Experience = 0;

	//================================
	for (const auto& Achiv : PlayerMatchMember.Achievements)
	{
		if (Achiv.Key == EShooterGameAchievements::Money)
		{
			Money = Achiv.Value;

			CachedMoney = Achiv.Value * ResultMultipler;										//	Умножаем на ResultMultipler, т.к могут быть бонусы в виде ежедневного входа или других			
			Widget_RewardMoneyBonus->SetValue(Achiv.Value * ResultMultiplerPremium);			//	Умножаем на 2, т.к премиум аккаунт увеличивает награду в двое
		}
		else if (Achiv.Key == EShooterGameAchievements::Experience)
		{
			Experience = Achiv.Value;
			
			CachedExp = Achiv.Value * ResultMultipler;											//	Умножаем на ResultMultipler, т.к могут быть бонусы в виде ежедневного входа или других						
			Widget_RewardExpBonus->SetValue(Achiv.Value * ResultMultiplerPremium);				//	Умножаем на 2, т.к премиум аккаунт увеличивает награду в двое
		}
	}

	//================================
	//	Clean RewardInfoList
	RewardInfoList.Empty(8);
	RewardInfoList.Add(MakeShareable(new FFightResultRewardInfo(NSLOCTEXT("SPersonalFightResult", "SPersonalFightResult.BattleReward", "+ Battle reward"), Money, Experience)));

	//================================
	if (HasUsedPremiumAccount())
	{
		RewardInfoList.Add(MakeShareable(new FFightResultRewardInfo(NSLOCTEXT("SPersonalFightResult", "SPersonalFightResult.PremiumAccount", "+ Premium Account"), Money, Experience)));
	}

	//================================
	if (HasUsedEveryDayBooster())
	{
		RewardInfoList.Add(MakeShareable(new FFightResultRewardInfo(NSLOCTEXT("SPersonalFightResult", "SPersonalFightResult.EveryDayBooster", "+ Every day bonus"), Money, Experience)));
	}

	//================================
	if (HasUsedPrimeTimeBooster())
	{
		RewardInfoList.Add(MakeShareable(new FFightResultRewardInfo(NSLOCTEXT("SPersonalFightResult", "SPersonalFightResult.PrimeTimeBooster", "+ Prime Time Booster"), Money * 0.5, Experience * 0.5)));
	}

	//================================
	if (HasUsedAdsBooster())
	{
		RewardInfoList.Add(MakeShareable(new FFightResultRewardInfo(NSLOCTEXT("SPersonalFightResult", "SPersonalFightResult.AdsBooster", "+ Ads"), Money, Experience)));
	}

	//================================
	RewardInfoList.Add(MakeShareable(new FFightResultRewardInfo(FText::GetEmpty(), 0, 0)));
	RewardInfoList.Add(MakeShareable(new FFightResultRewardInfo(NSLOCTEXT("SPersonalFightResult", "SPersonalFightResult.TotalReward", "= Total reward"), Money * ResultMultipler, Experience * ResultMultipler)));
	
	//================================
	Widget_RewardList->RequestListRefresh();
}


void SPersonalFightResult::SetInformation(const FGameMatchInformation& InInformation, const FGuid& InLocalPlayerId)
{
	AchievementsList.Empty();
	//=================================================================
	{
		FShooterGameAnalytics::RecordDesignEvent("Match:MatchesTotal");
		FShooterGameAnalytics::AddResourceEvent(EResourceFlowType::Source, "MatchesTotal", 1, "Gameplay", "TDM");
		Widget_WinStatus->SetText(NSLOCTEXT("SFightResults", "SFightResults.Draw", "The Draw"));
	}

	//=================================================================
	for (const auto &Member : InInformation.Members)
	{
		if (Member.PlayerId == InLocalPlayerId)
		{
			//================================
			PlayerMatchMember = Member;

			//================================
			Widget_RewardElo->SetValue(PlayerMatchMember.EloRatingScore - PlayerMatchMember.EloRatingScorePreview);

			//================================
			UpdateDisplayReward();

			//================================
			for (const auto& Achiv : Member.Achievements)
			{
				if (auto Entity = FGameSingletonExtensions::FindAchievementById(Achiv.Key))
				{
					if (Entity->GetAchievementProperty().bDisplayInPersonalResult)
					{
						AchievementsList.Add(MakeShareable(new FPlayerGameAchievement(Achiv)));
					}
				}

				//=================================================================
				if (Achiv.Key == EShooterGameAchievements::Money)
				{
					FShooterGameAnalytics::RecordDesignEvent("Match:TDM:Money", Achiv.Value);
					FShooterGameAnalytics::AddResourceEvent(EResourceFlowType::Source, "Money", Achiv.Value, "Gameplay", "TDM");
					FShooterGameAnalytics::RecordDesignEventRange("Match:TDM:MoneyMore[Key]", Achiv.Value, { 10000, 7000, 5000, 3000, 2500, 2000, 1500, 1000, 700, 500, 300, 200, 100, 50, 25, 0 });
				}
				else if (Achiv.Key == EShooterGameAchievements::Experience)
				{
					FShooterGameAnalytics::RecordDesignEvent("Match:TDM:Experience", Achiv.Value);
					FShooterGameAnalytics::AddResourceEvent(EResourceFlowType::Source, "Experience", Achiv.Value, "Gameplay", "TDM");
					FShooterGameAnalytics::RecordDesignEventRange("Match:TDM:ExperienceMore[Key]", Achiv.Value, {10000, 7000, 5000, 3000, 2500, 2000, 1500, 1000, 700, 500, 300, 200, 100, 50, 25, 0});
				}
				else if (Achiv.Key == EShooterGameAchievements::Kills)
				{
					FShooterGameAnalytics::RecordDesignEvent("Match:TDM:Kills", Achiv.Value);
					FShooterGameAnalytics::AddResourceEvent(EResourceFlowType::Source, "Kills", Achiv.Value, "Gameplay", "TDM");
					FShooterGameAnalytics::RecordDesignEventRange("Match:TDM:KillsMore[Key]", Achiv.Value);
				}
				else if (Achiv.Key == EShooterGameAchievements::Deads)
				{
					FShooterGameAnalytics::RecordDesignEvent("Match:TDM:Deads", Achiv.Value);
					FShooterGameAnalytics::AddResourceEvent(EResourceFlowType::Source, "Deads", Achiv.Value, "Gameplay", "TDM");
					FShooterGameAnalytics::RecordDesignEventRange("Match:TDM:DeadsMore[Key]", Achiv.Value);
				}
				else if (Achiv.Key == EShooterGameAchievements::Assist)
				{
					FShooterGameAnalytics::RecordDesignEvent("Match:TDM:Assist", Achiv.Value);
					FShooterGameAnalytics::AddResourceEvent(EResourceFlowType::Source, "Assist", Achiv.Value, "Gameplay", "TDM");
					FShooterGameAnalytics::RecordDesignEventRange("Match:TDM:AssistMore[Key]", Achiv.Value);
				}
				else if (Achiv.Key == EShooterGameAchievements::CriticalAssist)
				{
					FShooterGameAnalytics::RecordDesignEvent("Match:TDM:CriticalAssist", Achiv.Value);
					FShooterGameAnalytics::AddResourceEvent(EResourceFlowType::Source, "CriticalAssist", Achiv.Value, "Gameplay", "TDM");
					FShooterGameAnalytics::RecordDesignEventRange("Match:TDM:CriticalAssistMore[Key]", Achiv.Value);
				}
				else if (Achiv.Key == EShooterGameAchievements::KillsByGrenade)
				{
					FShooterGameAnalytics::RecordDesignEventRange("Match:TDM:KillsByGrenadeMore[Key]", Achiv.Value, { 15, 12, 10, 7, 5, 3, 1, 0 });
				}
				else if (Achiv.Key == EShooterGameAchievements::KillsByPistol)
				{
					FShooterGameAnalytics::RecordDesignEventRange("Match:TDM:KillsByPistolMore[Key]", Achiv.Value, { 15, 12, 10, 7, 5, 3, 1, 0 });
				}
				else if (Achiv.Key == EShooterGameAchievements::BrutalKill)
				{
					FShooterGameAnalytics::RecordDesignEventRange("Match:TDM:BrutalKillMore[Key]", Achiv.Value, {15, 12, 10, 7, 5, 3, 1});
				}
				else if (Achiv.Key == EShooterGameAchievements::DoubleKill)
				{
					FShooterGameAnalytics::RecordDesignEventRange("Match:TDM:DoubleKillMore[Key]", Achiv.Value, { 15, 12, 10, 7, 5, 3, 1 });
				}
				else if (Achiv.Key == EShooterGameAchievements::TripleKill)
				{
					FShooterGameAnalytics::RecordDesignEventRange("Match:TDM:TripleKillMore[Key]", Achiv.Value, { 15, 12, 10, 7, 5, 3, 1 });
				}
				else if (Achiv.Key == EShooterGameAchievements::Quadrakill)
				{
					FShooterGameAnalytics::RecordDesignEventRange("Match:TDM:QuadrakillMore[Key]", Achiv.Value, { 15, 12, 10, 7, 5, 3, 1 });
				}
				else if (Achiv.Key == EShooterGameAchievements::Pentakill)
				{
					FShooterGameAnalytics::RecordDesignEventRange("Match:TDM:PentakillMore[Key]", Achiv.Value, { 15, 12, 10, 7, 5, 3, 1 });
				}
				else if (Achiv.Key == EShooterGameAchievements::MatchesWins)
				{
					FShooterGameAnalytics::RecordDesignEvent("Match:MatchesWins");
					Widget_WinStatus->SetText(NSLOCTEXT("SFightResults", "SFightResults.Win", "The Victory"));
				}
				else if (Achiv.Key == EShooterGameAchievements::MatchesLoses)
				{
					FShooterGameAnalytics::RecordDesignEvent("Match:MatchesLoses");
					Widget_WinStatus->SetText(NSLOCTEXT("SFightResults", "SFightResults.Lose", "The Defeat"));
				}
			}	
			
			break;
		}
	}

	Widget_AchievementsList->RequestListRefresh();
}
