#pragma once

//=======================================
#include "Extensions/UObjectExtensions.h"

//=======================================
#include "Styles/LobbyContainerWidgetStyle.h"

//=======================================
#include "Utilities/SUsableCompoundWidget.h"
#include "Components/SPlayerDiscount.h"

class UPlayerDiscountEntity;
class UPlayerCaseEntity;
//=======================================
class UArsenalComponent;
class UIdentityComponent;
class USquadComponent;
class UAdsComponent;

//=======================================
enum class EGameCurrency : uint8;

//=======================================
class SAnimatedBackground;

//=======================================
class SHOOTERGAME_API SLobbyContainer : public SUsableCompoundWidget
{
public:

	SLATE_BEGIN_ARGS(SLobbyContainer)
		: _Style(&FShooterStyle::Get().GetWidgetStyle<FLobbyContainerStyle>("SLobbyContainerStyle"))
	{
		_Visibility = EVisibility::SelfHitTestInvisible;
	}
	SLATE_STYLE_ARGUMENT(FLobbyContainerStyle, Style)
	SLATE_ARGUMENT(TWeakObjectPtr<UIdentityComponent>, IdentityComponent)
	SLATE_ARGUMENT(TWeakObjectPtr<USquadComponent>, SquadComponent)
	SLATE_ARGUMENT(TWeakObjectPtr<UArsenalComponent>, ArsenalComponent)
	SLATE_ARGUMENT(TWeakObjectPtr<UAdsComponent>, AdsComponent)

	SLATE_EVENT(FOnClickedOutside, OnClickedArsenal)
	SLATE_EVENT(FOnClickedOutside, OnClickedSettings)
	SLATE_EVENT(FOnClickedOutside, OnClickedProfile)
	SLATE_EVENT(FOnClickedOutside, OnClickedRaitings)
	SLATE_EVENT(FOnClickedOutside, OnClickedChests)
	SLATE_EVENT(FOnProductAction, OnClickedDiscount)
	SLATE_END_ARGS()
		
	void Construct(const FArguments& InArgs);

	virtual void ToggleWidget(const bool) override;
	virtual void HideWidget() override;
	virtual bool IsInInteractiveMode() const override;

protected:
	void OnOpenPlayerCaseShowAds(const UPlayerCaseEntity* InPlayerCase) const;
	void OnOpenPlayerCaseClicked(const UPlayerCaseEntity* InPlayerCase) const;
	void OnOpenPlayerCaseClicked(const FGuid& InPlayerCaseId) const;

	const FLobbyContainerStyle* Style;

	FOnClickedOutside OnClickedArsenal;
	FOnClickedOutside OnClickedSettings;
	FOnClickedOutside OnClickedChests;
	FOnProductAction OnClickedDiscount;

	TSharedPtr<SAnimatedBackground> Widget_Animation;
	TSharedPtr<SWrapBox>			Widget_CasesContainer;

	TWeakObjectPtr<UAdsComponent> AdsComponent;
	TWeakObjectPtr<UArsenalComponent> ArsenalComponent;
	TWeakObjectPtr<UIdentityComponent> IdentityComponent;
	TWeakObjectPtr<USquadComponent> SquadComponent;

	UIdentityComponent* GetIdentityComponent() const;
	USquadComponent* GetSquadComponent() const;
	UArsenalComponent* GetArsenalComponent() const;

	int64 GetPlayerCash(const EGameCurrency InCurrency) const;
	int64 GetPlayerLevel() const;
	FText GetPlayerName() const;
	FSlateColor GetPlayerNameColor() const;

	FText GetBonusDescription() const;
	FText GetBonusNumber() const;
	EVisibility GetBonusNumberVisibility() const;

	FText GetPlayerProgress() const;
	TOptional<float> GetPlayerProgressPercent() const;

	TSharedRef<SWidget> GenerateCaseWidget(TWeakObjectPtr<UPlayerCaseEntity> InCaseInstance);
	TSharedRef<SWidget> GenerateDiscountWidget(TWeakObjectPtr<UPlayerDiscountEntity> InDiscountInstance);

	virtual void Tick(const FGeometry& AllottedGeometry, const double InCurrentTime, const float InDeltaTime) override;

	int32 HashCases;
	int32 HashDiscounts;

public:
	void RefreshCases();

};
