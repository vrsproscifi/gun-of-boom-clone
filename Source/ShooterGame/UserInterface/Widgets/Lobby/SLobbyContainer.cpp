
#include "ShooterGame.h"
#include "SLobbyContainer.h"
#include "SlateOptMacros.h"
#include "StringTableRegistry.h"
#include "Localization/TableLobbyStrings.h"
#include "Containers/SAdaptiveCarousel.h"
#include "Utilities/SAnimatedBackground.h"
#include "Item/GameItemCost.h"
#include "Shared/SessionMatchOptions.h"
#include "SquadComponent.h"
#include "IdentityComponent.h"
#include "ArsenalComponent.h"
#include "AdsComponent.h"

#include "Components/SCurrencyBlock.h"
#include "SScaleBox.h"
#include "Entity/PlayerCaseEntity.h"
#include "CaseProperty.h"
#include "Components/SPlayerCase.h"
#include "Components/SPlayerNameAndProgress.h"
#include "Input/STouchButton.h"
#include "ShooterGameAnalytics.h"
#include "Input/STextBlockButton.h"
#include "Utilities/SlateUtilityTypes.h"
#include "Components/SPlayerDiscount.h"
#include "Entity/PlayerDiscountEntity.h"
#include "DateTimeExtensions.h"
//#include "CrashlyticsBlueprintLibrary.h"

BEGIN_SLATE_FUNCTION_BUILD_OPTIMIZATION
void SLobbyContainer::Construct(const FArguments& InArgs)
{
	HashCases = -1;
	HashDiscounts = -1;

	Style = InArgs._Style;
	
	AdsComponent = InArgs._AdsComponent;
	IdentityComponent = InArgs._IdentityComponent;
	SquadComponent = InArgs._SquadComponent;
	ArsenalComponent = InArgs._ArsenalComponent;
	OnClickedArsenal = InArgs._OnClickedArsenal;
	OnClickedSettings = InArgs._OnClickedSettings;
	OnClickedDiscount = InArgs._OnClickedDiscount;
	OnClickedChests = InArgs._OnClickedChests;

	const TCHAR sSettings[] = { static_cast<TCHAR>(0xf013), 0 }; // fa-solid
	const TCHAR sRaitings[] = { static_cast<TCHAR>(0xf091), 0 }; // fa-solid
	const TCHAR sArsenal[] = { static_cast<TCHAR>(0xf07a), 0 }; // fa-solid

	ChildSlot
	[
		SAssignNew(Widget_Animation, SAnimatedBackground)
		.ShowAnimation(EAnimBackAnimation::ZoomOut)
		.HideAnimation(EAnimBackAnimation::ZoomIn)
		.InitAsHide(true)
		.IsEnabledBlur(false)
		.Duration(.5f)
		.Visibility(EVisibility::SelfHitTestInvisible)
		[
			SNew(SOverlay)
			+ SOverlay::Slot().HAlign(HAlign_Left).VAlign(VAlign_Top).Padding(20)
			[
				SNew(STouchButton)
				.Padding(FMargin(10, 0))
				.Style(&FShooterStyle::Get().GetWidgetStyle<FTocuhButtonStyle>("Default_TouchPlayerName"))
				.OnClicked_Lambda([e = InArgs._OnClickedProfile]()
				{
					FShooterGameAnalytics::RecordDesignEvent("UI:Lobby:Profile");
					if (e.ExecuteIfBound() == false)
					{
						FShooterGameAnalytics::RecordErrorEvent(EErrorSeverity::debug, "OnClickedProfile | Event not bound");
					}

					return FReply::Handled();
				})
				[
					SNew(SPlayerNameAndProgress).IdentityComponent(IdentityComponent)
				]
			]
			+ SOverlay::Slot().HAlign(HAlign_Left).VAlign(VAlign_Bottom).Padding(20)
			[				
				SNew(SButton).Tag(TEXT("SLobbyContainer.Button.Arsenal"))
				.HAlign(HAlign_Center)
				.VAlign(VAlign_Center)
				.ContentPadding(FMargin(20, 21))
				.ButtonStyle(&FShooterStyle::Get().GetWidgetStyle<FButtonStyle>("Default_NormalButton"))
				.TextStyle(&FShooterStyle::Get().GetWidgetStyle<FTextBlockStyle>("Default_ActionText"))
				.Text(FTableLobbyStrings::IntoArsenal)
				.OnClicked_Lambda([&]()
				{
					FShooterGameAnalytics::RecordDesignEvent("UI:Lobby:Arsenal");
					if(OnClickedArsenal.ExecuteIfBound() == false)
					{
						FShooterGameAnalytics::RecordErrorEvent(EErrorSeverity::debug, "OnClickedArsenal | Event not bound");
					}
					return FReply::Handled();
				})
				[
					SNew(SHorizontalBox)
					+ SHorizontalBox::Slot().AutoWidth().VAlign(VAlign_Center)
					[
						SNew(STextBlock)
						.TextStyle(&Style->FontAwesome2)
						.Text(FText::FromString(sArsenal))
						.Margin(FMargin(0, 0, 16, 0))
					]
					+ SHorizontalBox::Slot().AutoWidth()
					[
						SNew(STextBlock)
						.TextStyle(&FShooterStyle::Get().GetWidgetStyle<FTextBlockStyle>("Default_ActionText"))
						.Text(FTableLobbyStrings::IntoArsenal)
					]
				]
			]
			+ SOverlay::Slot().HAlign(HAlign_Right).VAlign(VAlign_Bottom).Padding(20)
			[
				SNew(SHorizontalBox)
				+ SHorizontalBox::Slot().AutoWidth()
				[					
					SNew(SBorder).BorderImage(&Style->BonusBackground).Padding(2)
					[
						SNew(SHorizontalBox)
						+ SHorizontalBox::Slot().AutoWidth() // Desc
						[
							SNew(SBox).MaxDesiredWidth(500).HAlign(HAlign_Center).VAlign(VAlign_Center).Padding(FMargin(8, 4))
							[
								SNew(STextBlock)
								.TextStyle(&Style->BonusDesc)
								.AutoWrapText(true)
								.Text(this, &SLobbyContainer::GetBonusDescription)
							]
						]
						+ SHorizontalBox::Slot().AutoWidth() // Big x<N>
						[
							SNew(SBox).HAlign(HAlign_Center).VAlign(VAlign_Center).Padding(4).Visibility(this, &SLobbyContainer::GetBonusNumberVisibility)
							[
								SNew(STextBlock)
								.TextStyle(&Style->BonusBigNum)
								.Text(this, &SLobbyContainer::GetBonusNumber)
							]
						]
					]
				]
				+ SHorizontalBox::Slot().AutoWidth()
				[
					SNew(SButton).Tag(TEXT("SLobbyContainer.Button.IntoFight"))
					.HAlign(HAlign_Center)
					.VAlign(VAlign_Center)
					.ContentPadding(FMargin(40, 21))
					.ButtonStyle(&FShooterStyle::Get().GetWidgetStyle<FButtonStyle>("Default_ActionButton"))
					.TextStyle(&FShooterStyle::Get().GetWidgetStyle<FTextBlockStyle>("Default_ActionText"))
					.Text(FTableLobbyStrings::IntoFight)
					.OnClicked_Lambda([&]() 
					{
						FShooterGameAnalytics::RecordDesignEvent("UI:Lobby:IntoFight:StartMatchMaking");
						if (auto squad = GetSquadComponent())
						{
							const FSessionMatchOptions options;
							squad->SendRequestToggleSearch(options, true);
						}
						else
						{
							FShooterGameAnalytics::RecordErrorEvent(EErrorSeverity::warning, "OnClickedIntoFight | SquadComponent was nullptr");
						}
						return FReply::Handled();
					})
				]
			]
			+ SOverlay::Slot()
			[
				SCurrencyBlock::Get()->Generate()
			]
			+ SOverlay::Slot().HAlign(HAlign_Left).VAlign(VAlign_Center).Padding(20)
			[
				SNew(SVerticalBox)
				+ SVerticalBox::Slot().AutoHeight().Padding(4)
				[
					SNew(SButton).Tag(TEXT("SLobbyContainer.Button.Raitings"))
					.HAlign(HAlign_Center)
					.VAlign(VAlign_Center)
					.ContentPadding(FMargin(22, 10))
					.ButtonStyle(&FShooterStyle::Get().GetWidgetStyle<FButtonStyle>("Default_NormalButton"))
					.Text(FTableLobbyStrings::IntoFight)
					.OnClicked_Lambda([&, e = InArgs._OnClickedRaitings]()
					{
						FShooterGameAnalytics::RecordDesignEvent("UI:Lobby:Raitings");
						if (e.ExecuteIfBound() == false)
						{
							FShooterGameAnalytics::RecordErrorEvent(EErrorSeverity::debug, "OnClickedRaitings | Event not bound");
						}
						return FReply::Handled();
					})
					[
						SNew(STextBlock)
						.TextStyle(&Style->FontAwesome)
						.Text(FText::FromString(sRaitings))
						.Margin(FMargin(0, 0, 6, 0))
					]
				]
				+ SVerticalBox::Slot().AutoHeight().Padding(4)
				[
					SNew(SButton).Tag(TEXT("SLobbyContainer.Button.Settings"))
					.HAlign(HAlign_Center)
					.VAlign(VAlign_Center)
					.ContentPadding(FMargin(22, 10))
					.ButtonStyle(&FShooterStyle::Get().GetWidgetStyle<FButtonStyle>("Default_NormalButton"))
					.Text(FTableLobbyStrings::IntoFight)
					.OnClicked_Lambda([&]()
					{
						FShooterGameAnalytics::RecordDesignEvent("UI:Lobby:Settings");
						if (OnClickedSettings.ExecuteIfBound() == false)
						{
							FShooterGameAnalytics::RecordErrorEvent(EErrorSeverity::debug, "OnClickedSettings | Event not bound");
						}
						return FReply::Handled();
					})
					[
						SNew(STextBlock)
						.TextStyle(&Style->FontAwesome)
						.Text(FText::FromString(sSettings))
						.Margin(FMargin(0, 0, 6, 0))
					]
				]				
			]
			+ SOverlay::Slot().HAlign(HAlign_Right).VAlign(VAlign_Top).Padding(FMargin(20, 100))
			[
				SAssignNew(Widget_CasesContainer, SWrapBox).Tag(TEXT("SLobbyContainer.Container.Cases"))
				.UseAllottedWidth(true)
				.PreferredWidth(180)
				.InnerSlotPadding(FVector2D(10, 10))
			]
		]
	];

}
END_SLATE_FUNCTION_BUILD_OPTIMIZATION


UArsenalComponent* SLobbyContainer::GetArsenalComponent() const
{
	return GetValidObject(ArsenalComponent);
}

UIdentityComponent* SLobbyContainer::GetIdentityComponent() const
{
	return GetValidObject(IdentityComponent);
}

USquadComponent* SLobbyContainer::GetSquadComponent() const
{
	return GetValidObject(SquadComponent);
}

void SLobbyContainer::ToggleWidget(const bool InToggle)
{
	if (InToggle)
	{
		if (FWidgetOrderedSupportQueue::GetCurrentQueueId() == TEXT("SLobbyContainer"))
		{
			Widget_Animation->ToggleWidget(InToggle);
		}

		FWidgetOrderedSupportQueue::AddQueue(TEXT("SLobbyContainer"), FOnClickedOutside::CreateLambda([&, t = InToggle]() {
			Widget_Animation->ToggleWidget(t);
		}), 5, true);
	}
	else
	{
		FWidgetOrderedSupportQueue::RemoveQueue(TEXT("SLobbyContainer"));
		Widget_Animation->ToggleWidget(InToggle);
	}
}

void SLobbyContainer::HideWidget()
{
	
}

bool SLobbyContainer::IsInInteractiveMode() const
{
	return Widget_Animation->IsAnimAtEnd();
}

int64 SLobbyContainer::GetPlayerCash(const EGameCurrency InCurrency) const
{
	if (IsValidObject(IdentityComponent))
	{
		return IdentityComponent->GetPlayerInfo().TakeCash(InCurrency);
	}


	return INDEX_NONE;
}

int64 SLobbyContainer::GetPlayerLevel() const
{
	if (IsValidObject(IdentityComponent))
	{
		return IdentityComponent->GetPlayerInfo().Experience.Level;
	}

	return INDEX_NONE;
}

FSlateColor SLobbyContainer::GetPlayerNameColor() const
{
	if (IsValidObject(IdentityComponent))
	{
		if (IdentityComponent->HasPremiumAccount())
		{
			return FSlateColor(FLinearColor::Yellow);
		}
	}

	return FSlateColor(FLinearColor::White);
}

FText SLobbyContainer::GetBonusDescription() const
{
	if (IsValidObject(IdentityComponent))
	{
		const auto AvalibleMatches = IdentityComponent->GetPlayerInfo().EveryDayPrimeTime.AvalibleMatches;
		if (AvalibleMatches > 0)
		{
			return NSLOCTEXT("SLobbyContainer", "SLobbyContainer.MatchBonus.Available.Desc", "Take double reward from battle!");
		}

		const auto TimeLeft = (FDateTime::FromUnixTimestamp(IdentityComponent->GetPlayerInfo().EveryDayPrimeTime.NextAvalibleDate) - FDateTime::UtcNow()).GetTotalSeconds();

		return FText::Format(
			NSLOCTEXT("SLobbyContainer", "SLobbyContainer.MatchBonus.Disabled.Desc", "Double reward has ended, time left for use again: {0}."),
			FDateTimeExtensions::AsFormatedTimespan(FTimespan::FromSeconds(TimeLeft))
		);
	}

	return FText::GetEmpty();
}

FText SLobbyContainer::GetBonusNumber() const
{
	if (IsValidObject(IdentityComponent))
	{
		const auto AvalibleMatches = IdentityComponent->GetPlayerInfo().EveryDayPrimeTime.AvalibleMatches;
		return FText::FromString(FString::Printf(TEXT("x%d"), AvalibleMatches));
	}

	return FText::GetEmpty();
}

EVisibility SLobbyContainer::GetBonusNumberVisibility() const
{
	if (IsValidObject(IdentityComponent) && IdentityComponent->GetPlayerInfo().EveryDayPrimeTime.AvalibleMatches > 0)
	{
		return EVisibility::Visible;
	}

	return EVisibility::Collapsed;
}

FText SLobbyContainer::GetPlayerName() const
{
	if (IsValidObject(IdentityComponent))
	{
		return FText::FromString(IdentityComponent->GetPlayerInfo().Login);
	}

	return FText::GetEmpty();
}

FText SLobbyContainer::GetPlayerProgress() const
{
	if (IsValidObject(IdentityComponent))
	{
		return FText::Format(FText::FromString("{0}|{1}"), 
			FText::AsNumber(IdentityComponent->GetPlayerInfo().Experience.Experience, &FNumberFormattingOptions::DefaultNoGrouping()),
			FText::AsNumber(IdentityComponent->GetPlayerInfo().Experience.NextExperience, &FNumberFormattingOptions::DefaultNoGrouping())
		);
	}

	return FText::GetEmpty();
}

TOptional<float> SLobbyContainer::GetPlayerProgressPercent() const
{
	if (IsValidObject(IdentityComponent))
	{
		return 
			float(IdentityComponent->GetPlayerInfo().Experience.Experience) /
			float(IdentityComponent->GetPlayerInfo().Experience.NextExperience);
	}

	return .0f;
}

TSharedRef<SWidget> SLobbyContainer::GenerateCaseWidget(TWeakObjectPtr<UPlayerCaseEntity> InCaseInstance)
{
	return SNew(SPlayerCase, InCaseInstance).Tag(*FString::Printf(TEXT("SLobbyContainer.Case_%d"), InCaseInstance->GetCaseEntityProperty()->ModelId))
		.OnOpenPlayerCase(this, &SLobbyContainer::OnOpenPlayerCaseClicked);
}

TSharedRef<SWidget> SLobbyContainer::GenerateDiscountWidget(TWeakObjectPtr<UPlayerDiscountEntity> InDiscountInstance)
{
	return SNew(SPlayerDiscount, InDiscountInstance).OnClicked(OnClickedDiscount);
}

void SLobbyContainer::OnOpenPlayerCaseShowAds(const UPlayerCaseEntity* InPlayerCase) const
{
	
}

void SLobbyContainer::OnOpenPlayerCaseClicked(const UPlayerCaseEntity* InPlayerCase) const
{
	//==============================================
	if(IsValidObject(InPlayerCase) == false)
	{
		return;
	}

	//==============================================
	const auto EntityId = InPlayerCase->GetEntityId();

	//==============================================
	//UCrashlyticsBlueprintLibrary::LogFbCustomEventKeyValue("Lobby_Clicked_OpenChest", "ChestId", FString::FromInt(InPlayerCase->GetModelId()));
	
	//==============================================
	const auto CaseEntityProperty = InPlayerCase->GetCaseEntityProperty();
	if(CaseEntityProperty)
	{
		if(CaseEntityProperty->CaseType == EGameCaseType::EveryDayAds && InPlayerCase->IsElapsedLastActive())
		{
			if (IsValidObject(AdsComponent))
			{
				AdsComponent->PlayRewardVideo(FPlayRewardedVideoRequest("GetFreeDayliCase", EntityId.ToString()));
			}
		}
		else
		{
			ArsenalComponent->SendRequestOpenPlayerCase(EntityId);
		}
	}
	else
	{
		ArsenalComponent->SendRequestOpenPlayerCase(EntityId);
	}
}

void SLobbyContainer::OnOpenPlayerCaseClicked(const FGuid& InPlayerCaseId) const
{
	//==============================================
	FShooterGameAnalytics::RecordDesignEvent("UI:Lobby:Cases:CaseClicked");

	//==============================================
	if (IsValidObject(ArsenalComponent))
	{
		auto PlayerCaseEntity = ArsenalComponent->GetCaseById(InPlayerCaseId);
		OnOpenPlayerCaseClicked(PlayerCaseEntity);
	}
	else
	{
		FShooterGameAnalytics::RecordErrorEvent(EErrorSeverity::warning, "OnOpenPlayerCaseClicked | ArsenalComponent was nullptr");
	}
}

void SLobbyContainer::RefreshCases()
{
	Widget_CasesContainer->ClearChildren();

	Widget_CasesContainer->AddSlot().FillEmptySpace(true).HAlign(HAlign_Right)
	[
		SNew(SBox).WidthOverride(150).HeightOverride(150)
		[
			SNew(SButton)
			.ButtonStyle(&Style->Chest.Button)
			.OnClicked_Lambda([&]()
			{
				OnClickedChests.ExecuteIfBound();
				return FReply::Handled();
			})
			[
				SNew(SOverlay)
				+ SOverlay::Slot()
				[
					SNew(SScaleBox)
					.Stretch(EStretch::ScaleToFit)
					[
						SNew(SImage).Image(&Style->Chest.Brush)
					]
				]
				+ SOverlay::Slot().HAlign(HAlign_Center).VAlign(VAlign_Bottom)
				[
					SNew(STextBlock).Text(NSLOCTEXT("SChestsContainer", "SChestsContainer.Title", "Chests"))
					.TextStyle(&Style->Chest.Text)
				]
			]
		]
	];

	const auto& DiscountArray = ArsenalComponent->GetDiscountCoupones();
	for (const auto& InDiscountCoupone : DiscountArray)
	{
		if (const auto PlayerDiscount = GetValidObject(InDiscountCoupone))
		{
			if (PlayerDiscount->IsAvalible())
			{
				Widget_CasesContainer->AddSlot().FillEmptySpace(true).HAlign(HAlign_Right)
				[
					GenerateDiscountWidget(PlayerDiscount)
				];
			}
		}
	}
}

void SLobbyContainer::Tick(const FGeometry& AllottedGeometry, const double InCurrentTime, const float InDeltaTime)
{
	SUsableCompoundWidget::Tick(AllottedGeometry, InCurrentTime, InDeltaTime);


	if (IsValidObject(ArsenalComponent))
	{
		//================================
		const auto& Cases = ArsenalComponent->GetCases();
		const auto& Coupones = ArsenalComponent->GetDiscountCoupones();

		//================================
		const bool bCaseRefreshRequired = HashCases != Cases.Num();
		const bool bCouponesRefreshRequired = Coupones.Num() && Coupones[0]->GetModelId() != HashDiscounts;
		const bool bAnyRefreshRequired = bCaseRefreshRequired || bCouponesRefreshRequired;

		//================================
		if (bCaseRefreshRequired)
		{
			HashCases = Cases.Num();
		}

		if (bCouponesRefreshRequired)
		{
			HashDiscounts = Coupones[0]->GetModelId();
		}

		//================================
		if (bAnyRefreshRequired)
		{
			RefreshCases();
		}
	}
}
