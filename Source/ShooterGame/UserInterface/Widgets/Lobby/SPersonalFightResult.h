// Fill out your copyright notice in the Description page of Project Settings.

#pragma once
//=========================================
#include "ProgressComponent/GameMatchMemberInformation.h"

#include "Utilities/SUsableCompoundWidget.h"
#include "Styles/PersonalFightResultWidgetStyle.h"

struct FPlayerGameAchievement;
class SResourceTextBox;
struct FGameMatchInformation;
class SAnimatedBackground;
class UBasicItemEntity;

DECLARE_DELEGATE_ThreeParams(FOnRequestApplyMatchPremiumAccountBonus, const FGuid& /*InMatchMemberId*/, const bool& /*HasPremiumAccount*/, const int32& /*InTargetPremiumAccount*/);
DECLARE_DELEGATE_OneParam(FOnRequestApplyMatchAdsBonus, const FGuid& /*InMatchMemberId*/);
DECLARE_DELEGATE(FOnPersonalFightResultClosed);


struct FFightResultRewardInfo
{
	FText Title;
	int64 Money;
	int64 Experience;

	FFightResultRewardInfo()
		: Money(0)
		, Experience(0)
	{
		
	}

	FFightResultRewardInfo(FText txt, const int64& InMoney, const int64& InExperience)
		: Title(txt)
		, Money(InMoney)
		, Experience(InExperience)
	{

	}
};


class SHOOTERGAME_API SPersonalFightResult : public SUsableCompoundWidget
{
public:
	SLATE_BEGIN_ARGS(SPersonalFightResult)
		: _Style(&FShooterStyle::Get().GetWidgetStyle<FPersonalFightResultStyle>("SPersonalFightResultStyle"))
	{
		_Visibility = EVisibility::SelfHitTestInvisible;
	}
	SLATE_STYLE_ARGUMENT(FPersonalFightResultStyle, Style)
	SLATE_EVENT(FOnRequestApplyMatchPremiumAccountBonus, OnRequestApplyMatchPremiumAccountBonus)
	SLATE_EVENT(FOnRequestApplyMatchAdsBonus, OnRequestApplyMatchAdsBonus)
	SLATE_EVENT(FOnPersonalFightResultClosed, OnPersonalFightResultClosed)



	SLATE_END_ARGS()
	void Construct(const FArguments& InArgs);

	virtual void ToggleWidget(const bool InToggle) override;
	bool IsInInteractiveMode() const override;
	virtual void HideWidget() override;

	void SetInformation(const FGameMatchInformation& InInformation, const FGuid& InLocalPlayerId);
	void OnApplyMatchPremiumAccountBonus();
	void OnApplyMatchAdsBonus();

protected:
	UBasicItemEntity* PremiumItemEntity;

	FOnRequestApplyMatchPremiumAccountBonus OnRequestApplyMatchPremiumAccountBonus;
	FOnRequestApplyMatchAdsBonus OnRequestApplyMatchAdsBonus;
	FOnPersonalFightResultClosed OnPersonalFightResultClosed;

	/*	The player has a premium account flag during the battle*/
	bool HasUsedPremiumAccount() const;

	/*	The player has a every day booster flag during the battle (5 bonus matches)*/
	bool HasUsedEveryDayBooster() const;

	/*	The player has a prime time booster flag during the battle (Bonus for entry at a certain time)*/
	bool HasUsedPrimeTimeBooster() const;
	
	bool HasUsedAdsBooster() const;

	int32 GetActiveRewardIndex() const;

	double GetFightResultMultipler() const;
	double GetPremeiumFightResultMultipler() const;

	FText GetFightResultMultiplerText() const;
	FText GetPremeiumFightResultMultiplerText() const;

	void UpdateDisplayReward();

	FReply OnRequestApplyMatchPremiumAccountBonusClicked();
	FReply OnRequestApplyMatchAdsBonusClicked();

	FGameMatchMemberInformation PlayerMatchMember;

	EVisibility GetHasPremiumVis() const;
	EVisibility GetExtraRewardVis() const;

	const FPersonalFightResultStyle* Style;

	TSharedPtr<SAnimatedBackground> Back_Animation;

	
	TSharedPtr<STextBlock> Widget_RewardX2;
	TSharedPtr<STextBlock> Widget_RewardX2Premium;

	TSharedPtr<STextBlock> Widget_WinStatus;
	TSharedPtr<SResourceTextBox> Widget_RewardElo;

	TSharedPtr<SResourceTextBox> Widget_RewardMoneyBonus;
	TSharedPtr<SResourceTextBox> Widget_RewardExpBonus;

	TSharedPtr<SListView<TSharedPtr<FFightResultRewardInfo>>> Widget_RewardList;
	TSharedPtr<SListView<TSharedPtr<FPlayerGameAchievement>>> Widget_AchievementsList;

	TArray<TSharedPtr<FPlayerGameAchievement>> AchievementsList;
	TArray<TSharedPtr<FFightResultRewardInfo>> RewardInfoList;

	//----------------------------------------------------------------[ FX

	FCurveSequence AnimSequence;
	FCurveHandle HandleNumbers;
	FCurveHandle HandleMultipler;
	FCurveHandle HandleOther;

	int64 CachedMoney;
	int64 CachedExp;

	int64 GetMoneyReward() const;
	int64 GetExpReward() const;

	float GetNumbersScale() const;
	float GetMultiplerScale() const;

	FLinearColor GetSidesColor() const;
	FVector2D GetLeftSideOffset() const;
	FVector2D GetRightSideOffset() const;
};
