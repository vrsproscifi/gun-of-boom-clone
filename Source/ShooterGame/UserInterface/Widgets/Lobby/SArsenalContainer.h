// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "Utilities/SUsableCompoundWidget.h"
#include "Components/SResourceTextBoxValue.h"
#include "GameItemType.h"
#include "Styles/ArsenalContainerWidgetStyle.h"

class SADefendContainer;
class SASupportContainer;
class SAWeaponsContainer;
class UArsenalComponent;
class UIdentityComponent;
class STabbedContainer;
class UBasicItemEntity;
class UBasicPlayerItem;
class SAdaptiveCarousel;
class SAnimatedBackground;
class SBasicArsenalContainer;

DECLARE_DELEGATE_OneParam(FOnItemAction, const UBasicPlayerItem*);


class SHOOTERGAME_API SArsenalContainer : public SUsableCompoundWidget
{
public:
	SLATE_BEGIN_ARGS(SArsenalContainer)
		: _Style(&FShooterStyle::Get().GetWidgetStyle<FArsenalContainerStyle>("SArsenalContainerStyle"))
	{
		_Visibility = EVisibility::SelfHitTestInvisible;
	}
	SLATE_STYLE_ARGUMENT(FArsenalContainerStyle, Style)
	SLATE_EVENT(FOnItemAction, OnRequestBuy)
	SLATE_EVENT(FOnItemAction, OnRequestSelect)
	SLATE_EVENT(FOnItemAction, OnRequestUpgrade)
	SLATE_EVENT(FOnItemAction, OnActiveItem)
	SLATE_ARGUMENT(TWeakObjectPtr<UIdentityComponent>, IdentityComponent)
	SLATE_ARGUMENT(TWeakObjectPtr<UArsenalComponent>, ArsenalComponent)
	SLATE_END_ARGS()

	void Construct(const FArguments& InArgs);

	virtual void ToggleWidget(const bool InToggle) override;
	virtual bool IsInInteractiveMode() const override;

	void OnFill(const TArray<UBasicPlayerItem*>& InItems);
	void ScrollTo(UBasicItemEntity* InBaseTargetItem);

	TSharedPtr<SBasicArsenalContainer> GetTargetContainerByItemType(const EGameItemType& InGameItemType, int32& OutTabbedContainerSlot) const;

protected:

	const FArsenalContainerStyle* Style;

	TWeakObjectPtr<UIdentityComponent> IdentityComponent;
	TWeakObjectPtr<UArsenalComponent> ArsenalComponent;

	FOnItemAction OnRequestBuy;
	FOnItemAction OnRequestSelect;
	FOnItemAction OnRequestUpgrade;
	FOnItemAction OnActiveItem;
	
	TSharedPtr<SAnimatedBackground>		Anim_Back;
	TSharedPtr<STabbedContainer>		Widget_TabbedContainer;

	TSharedPtr<SBasicArsenalContainer>		Widget_Weapons;
	TSharedPtr<SBasicArsenalContainer>		Widget_Support;
	TSharedPtr<SBasicArsenalContainer>		Widget_Defend;
		
};
