// Fill out your copyright notice in the Description page of Project Settings.

#include "ShooterGame.h"
#include "SAvalibleCaseItemsContainer.h"

#include "Decorators/LokaTextDecorator.h"
#include "Decorators/LokaResourceDecorator.h"
#include "Extensions/GameSingletonExtensions.h"

#include "Components/SDropedShopItem.h"
#include "BasicItemEntity.h"
#include "CaseItemProperty.h"


#include "Case/CaseEntity.h"
#include "Entity/PlayerCaseEntity.h"
#include "CaseProperty.h"

BEGIN_SLATE_FUNCTION_BUILD_OPTIMIZATION
void SAvalibleCaseItemsContainer::Construct(const FArguments& InArgs, const TArray<FCaseItemProperty>& InAvalibleDropeItems)
{
	//====================================
	Style = &FShooterStyle::Get().GetWidgetStyle<FShopItemStyle>("SShopItemStyle");


	//====================================
	ChildSlot
	[
		SNew(SVerticalBox)
		+ SVerticalBox::Slot().AutoHeight()
		.Padding(0, 10)
		//.AutoHeight(true)
		[
			SNew(SRichTextBlock)
			.Justification(ETextJustify::Center)
			.Text(NSLOCTEXT("Cases", "AvalibleCaseItemsContainer.Description", "The following items may drop out of this case."))
			.TextStyle(&Style->Title)
			.AutoWrapText(true)
			+ SRichTextBlock::Decorator(FLokaTextDecorator::Create(Style->Title))
			+ SRichTextBlock::Decorator(FLokaResourceDecorator::Create())
		]
		+ SVerticalBox::Slot().AutoHeight()
		.Padding(0)
		[
			SAssignNew(ItemsContainer, SScrollBox)
			.Orientation(Orient_Horizontal)
			.ScrollBarVisibility(EVisibility::Collapsed)
		]
	];

	//====================================
	for (auto MyItem : InAvalibleDropeItems)
	{
		if (const auto item = FGameSingletonExtensions::FindItemById(MyItem.ModelId))
		{
			ItemsContainer->AddSlot()
			[
				SNew(SBox).WidthOverride(400).HeightOverride(400)
				[
					SNew(SDropedShopItem, item).Range(FIntPoint(MyItem.RandomMin, MyItem.RandomMax))
				]
			];
		}
	}

}

void SAvalibleCaseContainer::Construct(const FArguments& InArgs, const TWeakObjectPtr<UPlayerCaseEntity>& InPlayerCaseEntity)
{
	PlayerCaseEntity = InPlayerCaseEntity;
	SDropedShopItem::Construct(SDropedShopItem::FArguments().Num(PlayerCaseEntity->GetAmount()), nullptr);
}

END_SLATE_FUNCTION_BUILD_OPTIMIZATION

const FSlateBrush* SAvalibleCaseContainer::GetItemImage() const
{
	if (IsValidObject(PlayerCaseEntity) && PlayerCaseEntity->GetCaseEntity())
	{		
		return &PlayerCaseEntity->GetCaseEntityProperty()->Image;
	}
	return nullptr;
}

FText SAvalibleCaseContainer::GetItemName() const
{
	if (IsValidObject(PlayerCaseEntity) && PlayerCaseEntity->GetCaseEntity())
	{
		return PlayerCaseEntity->GetCaseEntityProperty()->Title;
	}
	return FText::GetEmpty();
}