// Fill out your copyright notice in the Description page of Project Settings.

#include "ShooterGame.h"
#include "SAvalibleCasesContainer.h"
#include "SAvalibleCaseItemsContainer.h"
#include "CaseEntity.h"


#include "Decorators/LokaTextDecorator.h"
#include "Decorators/LokaResourceDecorator.h"
#include "Extensions/GameSingletonExtensions.h"

BEGIN_SLATE_FUNCTION_BUILD_OPTIMIZATION
void SAvalibleCasesContainer::Construct(const FArguments& InArgs, const TArray<UPlayerCaseEntity*>& InPlayerCases)
{
	//====================================
	PlayerCases = InPlayerCases;
	Style = &FShooterStyle::Get().GetWidgetStyle<FShopItemStyle>("SShopItemStyle");

	//====================================
	ChildSlot
	[
		SNew(SBox).HeightOverride(400)
		[
			SAssignNew(CasesContainer, SAdaptiveCarousel).DisplayedCells(3).Clipping(EWidgetClipping::ClipToBoundsAlways)
		]
	];

	//====================================
	for(auto c : InPlayerCases)
	{ 
		CasesContainer->AddSlot()
			.Padding(150, 0)
			.HAlign(EHorizontalAlignment::HAlign_Center)
			.VAlign(EVerticalAlignment::VAlign_Center)
		[
			SNew(SBox).WidthOverride(400).HeightOverride(400)
			[
				SNew(SAvalibleCaseContainer, c)
			]
		];
	}
}
END_SLATE_FUNCTION_BUILD_OPTIMIZATION
//
const UPlayerCaseEntity* SAvalibleCasesContainer::GetSelectedPlayerCase() const
{
	if (CasesContainer.IsValid())
	{
		//============================================================
		auto ActiveRawWidget = CasesContainer->GetActiveSlotWidget();
		if (ActiveRawWidget != SNullWidget::NullWidget)
		{
			auto ActiveBoxWidget = StaticCastSharedRef<SBox>(ActiveRawWidget);
			if (ActiveBoxWidget != SNullWidget::NullWidget && ActiveBoxWidget->GetChildren() != nullptr)
			{
				auto ActiveBoxChildren = ActiveBoxWidget->GetChildren();
				auto ActiveBoxChildWidget = ActiveBoxChildren->GetChildAt(0);
				if (ActiveBoxChildWidget != SNullWidget::NullWidget)
				{
					auto ActiveCastedWidget = StaticCastSharedRef<SAvalibleCaseContainer>(ActiveBoxChildWidget);
					if (ActiveCastedWidget != SNullWidget::NullWidget)
					{
						auto CaseEntity = ActiveCastedWidget->GetPlayerCaseEntity();
						auto ValidCaseEntity = GetValidObject(CaseEntity);
						return ValidCaseEntity;
					}
				}
			}
		}

		//============================================================
		auto ActiveSlotWidget = CasesContainer->GetActiveSlotIndex();
		if (PlayerCases.IsValidIndex(ActiveSlotWidget))
		{
			auto CaseEntity = PlayerCases[ActiveSlotWidget];
			auto ValidCaseEntity = GetValidObject(CaseEntity);
			return ValidCaseEntity;
		}
	}
	return nullptr;
}

