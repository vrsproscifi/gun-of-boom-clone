// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "ScrollBox.h"
#include "Containers/SAdaptiveCarousel.h"

#include "Styles/ShopItemWidgetStyle.h"
#include "Utilities/SUsableCompoundWidget.h"

class UPlayerCaseEntity;

class SHOOTERGAME_API SAvalibleCasesContainer : public SCompoundWidget
{
public:
	SLATE_BEGIN_ARGS(SAvalibleCasesContainer)
	{
		_Visibility = EVisibility::SelfHitTestInvisible;
	}

	SLATE_END_ARGS()

	void Construct(const FArguments& InArgs, const TArray<UPlayerCaseEntity*>& InPlayerCases);

	const UPlayerCaseEntity* GetSelectedPlayerCase() const;

private:
	const FShopItemStyle* Style;
	TArray<UPlayerCaseEntity*> PlayerCases;
	TSharedPtr<SAdaptiveCarousel> CasesContainer;

};
