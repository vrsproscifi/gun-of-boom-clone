// Fill out your copyright notice in the Description page of Project Settings.

#pragma once


#include "ScrollBox.h"
#include "Components/SDropedShopItem.h"
#include "Styles/ShopItemWidgetStyle.h"
#include "Utilities/SUsableCompoundWidget.h"

struct FCaseItemProperty;

class SHOOTERGAME_API SAvalibleCaseItemsContainer : public SCompoundWidget
{
public:
	SLATE_BEGIN_ARGS(SAvalibleCaseItemsContainer)
	{
		_Visibility = EVisibility::SelfHitTestInvisible;
	}

	SLATE_ATTRIBUTE(int32, CurrentSelectedDay)
	SLATE_END_ARGS()

	void Construct(const FArguments& InArgs, const TArray<FCaseItemProperty>& InAvalibleDropeItems);

private:
	const FShopItemStyle* Style;
	TSharedPtr<SScrollBox> ItemsContainer;
};

class UPlayerCaseEntity;

class SHOOTERGAME_API SAvalibleCaseContainer : public SDropedShopItem
{
public:
	SLATE_BEGIN_ARGS(SAvalibleCaseContainer)
	{
		_Visibility = EVisibility::SelfHitTestInvisible;
	}
	SLATE_END_ARGS()

	void Construct(const FArguments& InArgs, const TWeakObjectPtr<UPlayerCaseEntity>& InPlayerCaseEntity);
	const TWeakObjectPtr<UPlayerCaseEntity>& GetPlayerCaseEntity() const
	{
		return PlayerCaseEntity;
	}

protected:
	TWeakObjectPtr<UPlayerCaseEntity> PlayerCaseEntity;
	virtual const FSlateBrush* GetItemImage() const override;
	virtual FText GetItemName() const override;
};


