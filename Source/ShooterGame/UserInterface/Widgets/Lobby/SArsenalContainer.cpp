// Fill out your copyright notice in the Description page of Project Settings.

#include "ShooterGame.h"
#include "SArsenalContainer.h"
#include "SlateOptMacros.h"
#include "Utilities/SAnimatedBackground.h"
#include "Containers/SAdaptiveCarousel.h"
#include "Localization/TableLobbyStrings.h"
#include "BasicPlayerItem.h"
#include "Components/SArsenalItem.h"
#include "Components/SResourceTextBox.h"
#include "BasicItemEntity.h"
#include "Localization/TableBaseStrings.h"
#include "Components/SCurrencyBlock.h"
#include "Containers/STabbedContainer.h"
#include "Localization/TableWeaponStrings.h"
#include "IdentityComponent.h"
#include "ArsenalComponent.h"
#include "WeaponItemEntity.h"
#include "Containers/Arsenal/SAWeaponsContainer.h"
#include "Containers/Arsenal/SADefendContainer.h"
#include "Containers/Arsenal/SASupportContainer.h"
#include "SScaleBox.h"
#include "ShooterGameAnalytics.h"
#include "Components/STitleHeader.h"
//#include "CrashlyticsBlueprintLibrary.h"

BEGIN_SLATE_FUNCTION_BUILD_OPTIMIZATION
void SArsenalContainer::Construct(const FArguments& InArgs)
{
	Style = InArgs._Style;
	IdentityComponent = InArgs._IdentityComponent;
	ArsenalComponent = InArgs._ArsenalComponent;
	OnRequestBuy = InArgs._OnRequestBuy;
	OnRequestSelect = InArgs._OnRequestSelect;
	OnRequestUpgrade = InArgs._OnRequestUpgrade;
	OnActiveItem = InArgs._OnActiveItem;

	ChildSlot
	[
		SAssignNew(Anim_Back, SAnimatedBackground)
		.Visibility(EVisibility::Visible)
		.IsEnabledBlur(true)
		.Duration(.5f)
		.ShowAnimation(EAnimBackAnimation::UpFade)
		.HideAnimation(EAnimBackAnimation::DownFade)
		.InitAsHide(true)
		[
			SNew(SOverlay)
			+ SOverlay::Slot().VAlign(VAlign_Top)
			[
				SNew(STitleHeader)
				.ShowCurrency(true)
				.OnClickedBack_Lambda([&]() { FShooterGameAnalytics::RecordDesignEvent("UI:Lobby:Arsenal:ClickedBack"); ToggleWidget(false); })
			]
			+ SOverlay::Slot()
			[
				SAssignNew(Widget_TabbedContainer, STabbedContainer).IsCenteredControlls(true)
				+ STabbedContainer::Slot().Name(FText::FromString("Weapons"))
				.OnActivated_Lambda([&]()
				{
					FShooterGameAnalytics::RecordContentSearch("UI:Lobby:Arsenal:Weapons:WeaponsPage");
				})
				.Header()
				[
					SNew(SBox).HeightOverride(100).WidthOverride(180).Tag(TEXT("SArsenalContainer.Tabbed.Weapons"))
					[
						SNew(SScaleBox)
						.Stretch(EStretch::ScaleToFit)
						[
							SNew(SImage).Image(&Style->CatWeapons)
						]
					]
				]
				.Content()
				[
					SAssignNew(Widget_Weapons, SAWeaponsContainer)
					.IdentityComponent(IdentityComponent)
					.OnRequestUpgrade(OnRequestUpgrade)
					.OnRequestBuy(OnRequestBuy)
					.OnRequestSelect(OnRequestSelect)
					.OnActiveItem(OnActiveItem)
				]
				+ STabbedContainer::Slot().Name(FText::FromString("Defend"))
				.OnActivated_Lambda([&]()
				{
					FShooterGameAnalytics::RecordContentSearch("UI:Lobby:Arsenal:Defend:DefendPage");
				})
				.Header()
				[
					SNew(SBox).HeightOverride(100).WidthOverride(180).Tag(TEXT("SArsenalContainer.Tabbed.Defend"))
					[
						SNew(SScaleBox)
						.Stretch(EStretch::ScaleToFit)
						[
							SNew(SImage).Image(&Style->CatDefend)
						]
					]
				]
				.Content()
				[
					SAssignNew(Widget_Defend, SADefendContainer)
					.IdentityComponent(IdentityComponent)
					.OnRequestUpgrade(OnRequestUpgrade)
					.OnRequestBuy(OnRequestBuy)
					.OnRequestSelect(OnRequestSelect)
				]
				+ STabbedContainer::Slot().Name(FText::FromString("Support"))
				.OnActivated_Lambda([&]()
				{
					FShooterGameAnalytics::RecordContentSearch("UI:Lobby:Arsenal:Support:SupportPage");
				})
				.Header()
				[
					SNew(SBox).HeightOverride(100).WidthOverride(180).Tag(TEXT("SArsenalContainer.Tabbed.Support"))
					[
						SNew(SScaleBox)
						.Stretch(EStretch::ScaleToFit)
						[
							SNew(SImage).Image(&Style->CatSupport)
						]
					]
				]
				.Content()
				[
					SAssignNew(Widget_Support, SASupportContainer)
					.IdentityComponent(IdentityComponent)
					.OnRequestUpgrade(OnRequestUpgrade)
					.OnRequestBuy(OnRequestBuy)
					.OnRequestSelect(OnRequestSelect)
				]
			]
		]
	];
}
END_SLATE_FUNCTION_BUILD_OPTIMIZATION

void SArsenalContainer::ToggleWidget(const bool InToggle)
{
	SUsableCompoundWidget::ToggleWidget(InToggle);
	Anim_Back->ToggleWidget(InToggle);

	const auto MyProfile = ArsenalComponent->GetProfileDataPtr(0);
	if (InToggle && IsValidObject(ArsenalComponent) && MyProfile)
	{
		for (auto pItem : MyProfile->Items)
		{
			if (pItem.EntityId == MyProfile->DefaultItemId)
			{
				Widget_Weapons->ScrollToActive(ArsenalComponent->FindItem(pItem.ItemId));
				break;
			}
		}		
	}

	if (InToggle == false)
	{
		FWidgetOrderedSupportQueue::RemoveQueue(TEXT("SArsenalContainer"));
	}

	//UCrashlyticsBlueprintLibrary::LogFbCustomEvent(InToggle ? TEXT("Open_Store_Arsenal") : TEXT("Close_Store_Arsenal"));
}

TSharedPtr<SBasicArsenalContainer> SArsenalContainer::GetTargetContainerByItemType(const EGameItemType& InGameItemType, int32& OutTabbedContainerSlot) const
{
	switch (InGameItemType)
	{
		//================================
		case EGameItemType::Rifle:
		case EGameItemType::Shotgun:
		case EGameItemType::SniperRifle:
		case EGameItemType::MachineGun:
		case EGameItemType::Pistol:
		case EGameItemType::Knife:
			OutTabbedContainerSlot = 0;
			return Widget_Weapons;

		//================================
		case EGameItemType::Head:
		case EGameItemType::Torso:
		case EGameItemType::Legs:
			OutTabbedContainerSlot = 1;
			return Widget_Defend;

		//================================
		default:
			OutTabbedContainerSlot = 2;
			return Widget_Support;
	}
}

void SArsenalContainer::ScrollTo(UBasicItemEntity* InBaseTargetItem)
{
	int32 TabbedContainerSlot = 0;

	//==========================================
	const auto TargetType = InBaseTargetItem->GetItemType();
	const auto TargetContainer = GetTargetContainerByItemType(TargetType, TabbedContainerSlot);

	Widget_TabbedContainer->SetActiveSlot(TabbedContainerSlot);

	const auto ActiveTab = TargetContainer->GetIndexOfTabWithItemType(TargetType);

	if (ActiveTab >= 0)
	{
		TargetContainer->SetActiveSlot(ActiveTab);
		auto TargetItemsContainer = TargetContainer->GetItemsContainerByItemType(TargetType);

		//==========================================
		if (TargetItemsContainer.IsValid())
		{
			const auto Childs = TargetItemsContainer->GetChildren();

			for (SIZE_T i = 0; i < Childs->Num(); ++i)
			{
				TSharedRef<SShopItem> MyWidget = StaticCastSharedRef<SShopItem>(Childs->GetChildAt(i));
				if (MyWidget != SNullWidget::NullWidget)
				{
					if (MyWidget->GetInstanceBasic().IsValid() && MyWidget->GetInstanceBasic().Get() == InBaseTargetItem)
					{
						TargetItemsContainer->ScrollToWidget(MyWidget);
						break;
					}
				}
			}
		}
	}
}

bool SArsenalContainer::IsInInteractiveMode() const
{
	return Anim_Back->IsAnimAtEnd();
}

void SArsenalContainer::OnFill(const TArray<UBasicPlayerItem*>& InItems)
{
	auto FilteredItems = InItems.FilterByPredicate([](UBasicPlayerItem* Source)
	{
		return GetValidObject(Source) && Source->GetItemProperty()->bIsHiddenInShop == false;
	});

	Widget_Weapons->OnFill(FilteredItems);
	Widget_Defend->OnFill(FilteredItems);
	Widget_Support->OnFill(FilteredItems);
}