// Fill out your copyright notice in the Description page of Project Settings.

#include "ShooterGame.h"
#include "SRaitingsContainer.h"
#include "SlateOptMacros.h"
#include "Utilities/SAnimatedBackground.h"
#include "Components/STitleHeader.h"
#include "ProgressComponent/RatingTopContainer.h"
#include "GameSingletonExtensions.h"
#include "Containers/STabbedContainer.h"
#include "Components/SRadioButtonsBox.h"
#include "Components/SResourceTextBox.h"
#include "WorldFlagsData.h"
#include "SlateMaterialBrush.h"
#include "ScaleBox.h"

FName SRaitingsContainer::Column_Num = TEXT("Number");
FName SRaitingsContainer::Column_Name = TEXT("Name");
FName SRaitingsContainer::Column_Score = TEXT("Score");

class SHOOTERGAME_API SRaitingsContainer_Item : public SMultiColumnTableRow<TSharedPtr<FPlayerRatingTopContainer>>
{
public:
	SLATE_BEGIN_ARGS(SRaitingsContainer_Item)
		: _HasELO(false)
		, _Style(&FShooterStyle::Get().GetWidgetStyle<FRaitingsContainerStyle>("SRaitingsContainerStyle"))
	{}
	SLATE_ARGUMENT(const TArray<TSharedPtr<FPlayerRatingTopContainer>>*, ListItemsSource)
	SLATE_ARGUMENT(bool, HasELO)
	SLATE_STYLE_ARGUMENT(FRaitingsContainerStyle, Style)
	SLATE_END_ARGS()

	BEGIN_SLATE_FUNCTION_BUILD_OPTIMIZATION
	void Construct(const FArguments& InArgs, const TSharedRef<STableViewBase>& OwnerTableView, const TSharedRef<FPlayerRatingTopContainer>& InItem)
	{
		SCOPED_SUSPEND_RENDERING_THREAD(false);

		Style = InArgs._Style;
		bELO = InArgs._HasELO;
		Instance = InItem;

		auto FlagsData = FGameSingletonExtension::GetDataAssets<UWorldFlagsData>();
		if (FlagsData.Num() && FlagsData.IsValidIndex(0) && IsValidObject(FlagsData[0]))
		{
			FlagBrush = FSlateMaterialBrush(*FlagsData[0]->GetFlagMaterialByCode(Instance->Country), FVector2D(128, 128));
		}
		else
		{
			FlagBrush = FSlateNoResource();
		}

		const bool IsLocal = FGameSingletonExtension::GetLastLocalPlayerId() == Instance->Id;

		auto SuperArgs = FSuperRowType::FArguments().Padding(FMargin(2, 6)).Style(IsLocal ? &Style->TableRowStyleLocal : &Style->TableRowStyle);
		FSuperRowType::Construct(SuperArgs, OwnerTableView);
	}
	END_SLATE_FUNCTION_BUILD_OPTIMIZATION

protected:

	const FRaitingsContainerStyle* Style;

	TSharedRef<SWidget> GenerateWidgetForColumn(const FName& InColumnName) override
	{
		if (Instance->Name.IsEmpty())
		{
			return SNew(SBox).VAlign(VAlign_Center)[SNew(STextBlock).Text(FText::FromString("...")).TextStyle(&Style->RowTextStyle)];
		}
		else
		{
			if (InColumnName.IsEqual(SRaitingsContainer::Column_Num))
			{
				return SNew(SBox).VAlign(VAlign_Center).Padding(FMargin(4, 0, 0, 0))[SNew(STextBlock).Text(FText::AsNumber(Instance->Position, &FNumberFormattingOptions::DefaultNoGrouping())).TextStyle(&Style->RowNumberTextStyle)];
			}
			else if (InColumnName.IsEqual(SRaitingsContainer::Column_Name))
			{
				return SNew(SBox).VAlign(VAlign_Center)
				[
					SNew(SHorizontalBox)
					+ SHorizontalBox::Slot().AutoWidth()
					[
						SNew(SBox).WidthOverride(100).HeightOverride(80)
						[
							SNew(SScaleBox).Stretch(EStretch::ScaleToFit)
							[
								SNew(SImage).Image(&FlagBrush)
							]
						]
					]
					+ SHorizontalBox::Slot().VAlign(VAlign_Center).Padding(5, 0, 0, 0)
					[
						SNew(STextBlock).Text(FText::FromString(Instance->Name)).TextStyle(&Style->RowTextStyle)
					]
				];
			}
			else if (InColumnName.IsEqual(SRaitingsContainer::Column_Score))
			{
				return SNew(SBox).VAlign(VAlign_Center).Padding(FMargin(0, 0, 4, 0))
				[
					SNew(SVerticalBox)
					+ SVerticalBox::Slot()
					[
						SNew(SResourceTextBox)
						.Type(EResourceTextBoxType::Elo)
						.Value(bELO ? Instance->EloRatingScore : Instance->Score)
					]
					+ SVerticalBox::Slot()
					[
						SNew(SResourceTextBox)
						.Type(EResourceTextBoxType::Level)
						.Value(Instance->Level)
					]
				];
			}
		}

		return SNullWidget::NullWidget;
	}

	TSharedPtr<FPlayerRatingTopContainer> Instance;
	bool bELO;
	FSlateBrush FlagBrush;
};

BEGIN_SLATE_FUNCTION_BUILD_OPTIMIZATION
void SRaitingsContainer::Construct(const FArguments& InArgs)
{
	Style = InArgs._Style;
	OnPlayerSelected = InArgs._OnPlayerSelected;

	ChildSlot
	[
		SAssignNew(Anim_Back, SAnimatedBackground)
		.Visibility(EVisibility::SelfHitTestInvisible)
		.IsEnabledBlur(false)
		.Duration(.5f)
		.ShowAnimation(EAnimBackAnimation::UpFade)
		.HideAnimation(EAnimBackAnimation::DownFade)
		.InitAsHide(true)		
		[
			SNew(SVerticalBox)
			+ SVerticalBox::Slot().AutoHeight()
			[
				SNew(STitleHeader).Header(NSLOCTEXT("SRaitingsContainer", "SRaitingsContainer.Title", "Rankings"))
				.OnClickedBack_Lambda([&, e = InArgs._OnClickedBack]()
				{
					e.ExecuteIfBound();
					ToggleWidget(false);
				})
			]
			+ SVerticalBox::Slot()
			[
				SNew(SHorizontalBox)
				+ SHorizontalBox::Slot().VAlign(VAlign_Top).HAlign(HAlign_Left)
				[
					SAssignNew(Widget_Controlls, SRadioButtonsBox)
					.ActiveIndex(0)
					.IsAutoSize(true)
					.ButtonsPadding(FMargin(20, 5, 0, 5))
					.Orientation(Orient_Vertical)
					.ButtonsText({ NSLOCTEXT("SRaitingsContainer", "SRaitingsContainer.Raitings.ELO", "ELO"), NSLOCTEXT("SRaitingsContainer", "SRaitingsContainer.Raitings.Classic", "Classic") })
				]
				+ SHorizontalBox::Slot().FillWidth(1.2f)
				[
					SNew(SAnimatedBackground)
					.Visibility(EVisibility::Visible)
					.IsEnabledBlur(true)
					.InitAsHide(false)
					.Padding(FMargin(10))
					[
						SNew(SWidgetSwitcher)
						.WidgetIndex(this, &SRaitingsContainer::GetActiveRaitingIndex)
						+ SWidgetSwitcher::Slot()
						[
							SAssignNew(RaitingsELO_Table, SListView<TSharedPtr<FPlayerRatingTopContainer>>)
							.ScrollbarVisibility(EVisibility::Collapsed)
							.AllowOverscroll(EAllowOverscroll::Yes)
							.SelectionMode(ESelectionMode::Single)
							.ListItemsSource(&RaitingsELO_List)
							.HeaderRow
							(
								SNew(SHeaderRow).Style(&Style->HeaderRowStyle).Visibility(EVisibility::Collapsed)
								+ SHeaderRow::Column(Column_Num).FillWidth(.14f)[SNew(STextBlock).Text(NSLOCTEXT("SRaitingsContainer", "SRaitingsContainer.Num", "#")).TextStyle(&Style->HeaderTextStyle)]
								+ SHeaderRow::Column(Column_Name).FillWidth(1.0f)[SNew(STextBlock).Text(NSLOCTEXT("SRaitingsContainer", "SRaitingsContainer.Name", "Name")).TextStyle(&Style->HeaderTextStyle)]
								+ SHeaderRow::Column(Column_Score).FillWidth(.3f)[SNew(STextBlock).Text(NSLOCTEXT("SRaitingsContainer", "SRaitingsContainer.Score", "Score")).TextStyle(&Style->HeaderTextStyle)]
							)
							.OnGenerateRow_Lambda([&](TSharedPtr<FPlayerRatingTopContainer> InItem, const TSharedRef<STableViewBase>& OwnerTableView) -> TSharedRef<ITableRow>
							{
								return SNew(SRaitingsContainer_Item, OwnerTableView, InItem.ToSharedRef()).ListItemsSource(&RaitingsELO_List).HasELO(true);
							})
							.OnSelectionChanged_Lambda([&](TSharedPtr<FPlayerRatingTopContainer> InItem, ESelectInfo::Type InInfo)
							{
								if (InItem.IsValid())
								{
									OnPlayerSelected.ExecuteIfBound(InItem->Id);
									Raitings_Table->SetItemSelection(InItem, false);
								}
							})
						]
						+ SWidgetSwitcher::Slot()
						[
							SAssignNew(Raitings_Table, SListView<TSharedPtr<FPlayerRatingTopContainer>>)
							.ScrollbarVisibility(EVisibility::Collapsed)
							.AllowOverscroll(EAllowOverscroll::Yes)
							.SelectionMode(ESelectionMode::Single)
							.ListItemsSource(&Raitings_List)
							.HeaderRow
							(
								SNew(SHeaderRow).Style(&Style->HeaderRowStyle).Visibility(EVisibility::Collapsed)
								+ SHeaderRow::Column(Column_Num).FillWidth(.14f)[SNew(STextBlock).Text(NSLOCTEXT("SRaitingsContainer", "SRaitingsContainer.Num", "#")).TextStyle(&Style->HeaderTextStyle)]
								+ SHeaderRow::Column(Column_Name).FillWidth(1.0f)[SNew(STextBlock).Text(NSLOCTEXT("SRaitingsContainer", "SRaitingsContainer.Name", "Name")).TextStyle(&Style->HeaderTextStyle)]
								+ SHeaderRow::Column(Column_Score).FillWidth(.3f)[SNew(STextBlock).Text(NSLOCTEXT("SRaitingsContainer", "SRaitingsContainer.Score", "Score")).TextStyle(&Style->HeaderTextStyle)]
							)
							.OnGenerateRow_Lambda([&](TSharedPtr<FPlayerRatingTopContainer> InItem, const TSharedRef<STableViewBase>& OwnerTableView) -> TSharedRef<ITableRow>
							{
								return SNew(SRaitingsContainer_Item, OwnerTableView, InItem.ToSharedRef()).ListItemsSource(&Raitings_List);
							})
							.OnSelectionChanged_Lambda([&](TSharedPtr<FPlayerRatingTopContainer> InItem, ESelectInfo::Type InInfo)
							{
								if (InItem.IsValid())
								{
									OnPlayerSelected.ExecuteIfBound(InItem->Id);
									Raitings_Table->SetItemSelection(InItem, false);
								}
							})
						]
					]
				]
			]
		]
	];
}
END_SLATE_FUNCTION_BUILD_OPTIMIZATION

void SRaitingsContainer::ToggleWidget(const bool InToggle)
{
	SUsableCompoundWidget::ToggleWidget(InToggle);
	Anim_Back->ToggleWidget(InToggle);
}

bool SRaitingsContainer::IsInInteractiveMode() const
{
	return Anim_Back->IsAnimAtEnd();
}

void SRaitingsContainer::SetInformation(const FRatingTopContainer& InTopData)
{
	static uint8 WordsNum = 16;
	static uint16 MaxRows = 30;

	TSharedPtr<FPlayerRatingTopContainer> LocalPlayerPtr;

	auto NameLambda = [&](const FString& InStr) -> FString
	{
		if (InStr.Len() > WordsNum)
		{
			return InStr.Left(WordsNum) + TEXT("...");
		}

		return InStr;
	};

	auto ListLambda = [&](TArray<TSharedPtr<FPlayerRatingTopContainer>>& InFillArray, const TArray<FPlayerRatingTopContainer>& InRawList, const uint16& InFrom, const uint16& InTo) -> void
	{
		for (SIZE_T i = InFrom; i < InTo; ++i)
		{
			if (InRawList.IsValidIndex(i))
			{
				auto RawValue = InRawList[i];

				RawValue.Name = NameLambda(RawValue.Name);
				InFillArray.Add(MakeShareable(new FPlayerRatingTopContainer(RawValue)));

				if (RawValue.Id == FGameSingletonExtension::GetLastLocalPlayerId())
				{
					LocalPlayerPtr = InFillArray.Last().ToSharedRef();
				}
			}
		}
	};

	Raitings_List.Empty();
	RaitingsELO_List.Empty();
	
	ListLambda(Raitings_List, InTopData.PlayerRating, 0, MaxRows);
	ListLambda(RaitingsELO_List, InTopData.PlayerEloRating, 0, MaxRows);

	if (InTopData.PlayerRating.Num() > MaxRows)
	{
		Raitings_List.Add(MakeShareable(new FPlayerRatingTopContainer()));
		ListLambda(Raitings_List, InTopData.PlayerRating, MaxRows, InTopData.PlayerRating.Num());
	}

	Raitings_Table->RequestListRefresh();
	Raitings_Table->RequestScrollIntoView(LocalPlayerPtr);


	//============================================================== ELO

	if (InTopData.PlayerEloRating.Num() > MaxRows)
	{
		RaitingsELO_List.Add(MakeShareable(new FPlayerRatingTopContainer()));
		ListLambda(RaitingsELO_List, InTopData.PlayerEloRating, MaxRows, InTopData.PlayerEloRating.Num());
	}

	RaitingsELO_Table->RequestListRefresh();
	RaitingsELO_Table->RequestScrollIntoView(LocalPlayerPtr);
}

int32 SRaitingsContainer::GetActiveRaitingIndex() const
{
	return Widget_Controlls->GetActiveIndex();
}
