// Fill out your copyright notice in the Description page of Project Settings.

#include "ShooterGame.h"
#include "SSearchFightScreen.h"
#include "SlateOptMacros.h"
#include "Localization/TableLobbyStrings.h"
#include "Localization/TableBaseStrings.h"
#include "Utilities/SAnimatedBackground.h"
#include "SScaleBox.h"
#include "GameMapData.h"
#include "GameSingletonExtensions.h"
#include "ShooterGameAnalytics.h"
#include "UserWidget.h"

BEGIN_SLATE_FUNCTION_BUILD_OPTIMIZATION
void SSearchFightScreen::Construct(const FArguments& InArgs)
{
	Style = InArgs._Style;
	OnCancel = InArgs._OnCancel;

	auto MapAssets = FGameSingletonExtensions::GetDataAssets<UGameMapData>();
	for (auto MapAsset : MapAssets)
	{
		for (auto Brush : MapAsset->GetImages())
		{
			FMapNameAndImage MapNameAndImage;
			MapNameAndImage.DisplayName = MapAsset->GetDisplayName();
			MapNameAndImage.Description = MapAsset->GetDescription();
			MapNameAndImage.Brush = Brush;
			MapNameAndImage.Index = MapAsset->GetValue();

			MapImages.Add(MapNameAndImage);
		}		
	}

	// Random images
	MapImages.Sort([](const FMapNameAndImage& A, const FMapNameAndImage& B) { return FMath::RandBool(); });

	for (auto TargetUserWidgetCalss : Style->UserWidgetsOverlay)
	{
		auto TargetUserWidget = NewObject<UUserWidget>(GetTransientPackage(), TargetUserWidgetCalss);
		UserWidgets.Add(TargetUserWidget);
	}

	ChildSlot
	[
		SAssignNew(Anim_Background, SAnimatedBackground)
		.Visibility(EVisibility::Visible)
		.InitAsHide(true)
		.IsEnabledBlur(false)
		.Duration(.1f)
		.Ease(ECurveEaseFunction::QuadIn)
		.ShowAnimation(EAnimBackAnimation::Color)
		.HideAnimation(EAnimBackAnimation::Color)
		[
			SNew(SOverlay)
			+ SOverlay::Slot() // Background 
			[
				SNew(SBorder).BorderImage(new FSlateColorBrush(FColor::Black)).Padding(0)
				[
					SNew(SOverlay)
					+ SOverlay::Slot()
					[
						SAssignNew(Anim_Image[0], SAnimatedBackground)
						.Visibility(EVisibility::Visible)
						.InitAsHide(false)
						.IsEnabledBlur(false)
						.Duration(0.5f)
						.Ease(ECurveEaseFunction::Linear)
						.ShowAnimation(EAnimBackAnimation::RightFade)
						.HideAnimation(EAnimBackAnimation::LeftFade)
						[
							SNew(SScaleBox)
							.Stretch(EStretch::ScaleToFit)
							[
								SAssignNew(Widget_Images[0], SImage)
							]
						]
					]
					+ SOverlay::Slot()
					[
						SAssignNew(Anim_Image[1], SAnimatedBackground)
						.Visibility(EVisibility::Visible)
						.InitAsHide(false)
						.IsEnabledBlur(false)
						.Duration(0.5f)
						.Ease(ECurveEaseFunction::Linear)
						.ShowAnimation(EAnimBackAnimation::RightFade)
						.HideAnimation(EAnimBackAnimation::LeftFade)
						[
							SNew(SScaleBox)
							.Stretch(EStretch::ScaleToFit)
							[
								SAssignNew(Widget_Images[1], SImage)
							]
						]
					]
					+ SOverlay::Slot()
					[
						SAssignNew(Anim_UserWidget[0], SAnimatedBackground)
						.Visibility(EVisibility::Visible)
						.InitAsHide(true)
						.IsEnabledBlur(false)
						.Duration(0.5f)
						.Ease(ECurveEaseFunction::Linear)
						.ShowAnimation(EAnimBackAnimation::Color)
						.HideAnimation(EAnimBackAnimation::Color)
					]
					+ SOverlay::Slot()
					[
						SAssignNew(Anim_UserWidget[1], SAnimatedBackground)
						.Visibility(EVisibility::Visible)
						.InitAsHide(true)
						.IsEnabledBlur(false)
						.Duration(0.5f)
						.Ease(ECurveEaseFunction::Linear)
						.ShowAnimation(EAnimBackAnimation::Color)
						.HideAnimation(EAnimBackAnimation::Color)
					]
				]
			]
			+ SOverlay::Slot() // Content
			[
				SNew(SVerticalBox)
				+ SVerticalBox::Slot().AutoHeight() // Name of map
				[
					SNew(SBorder)
					.Padding(10)
					.BorderImage(&Style->BackgoundMapName)
					[
						SNew(STextBlock)
						.TextStyle(&Style->MapNameText)
						.Justification(ETextJustify::Center)
						.Text(this, &SSearchFightScreen::GetCurrentMapName)
					]
				]
				+ SVerticalBox::Slot() // Fill empty space
				+ SVerticalBox::Slot().AutoHeight() // Description
				[
					SNew(SBorder)
					.Padding(10)
					.BorderImage(&Style->BackgoundMapDesc)
					[
						SNew(STextBlock)
						.TextStyle(&Style->MapDescText)
						.Justification(ETextJustify::Center)
						.Text(this, &SSearchFightScreen::GetCurrentMapDesc)
					]
				]
				+ SVerticalBox::Slot().AutoHeight() // Status & Cancel
				[
					SNew(SOverlay)
					+ SOverlay::Slot().VAlign(VAlign_Center).Padding(20) // Desc
					[
						SNew(STextBlock)
						.TextStyle(&Style->StatusText)
						.Justification(ETextJustify::Center)
						.Text_Lambda([&]() { return bIsFound ? FTableLobbyStrings::FightFound : FTableLobbyStrings::SearchOfFight; })
					]
					+ SOverlay::Slot().HAlign(HAlign_Right).Padding(20) // Cancel
					[
						SNew(SButton)
						.Visibility_Lambda([&]() { return bIsFound ? EVisibility::Collapsed : EVisibility::Visible; })
						.TextStyle(&Style->CancelText)
						.ButtonStyle(&FShooterStyle::Get().GetWidgetStyle<FButtonStyle>("Default_ActionButton"))
						.HAlign(HAlign_Center)
						.VAlign(VAlign_Center)
						.ContentPadding(FMargin(80, 21))
						.Text(FTableBaseStrings::GetBaseText(EBaseStrings::Cancel))
						.OnClicked_Lambda([&]()
						{		
							FShooterGameAnalytics::RecordDesignEvent("UI:Lobby:IntoFight:CancelMatchMaking");

							if (OnCancel.ExecuteIfBound() == false)
							{
								FShooterGameAnalytics::RecordErrorEvent(EErrorSeverity::warning, "OnMatchMaking_Cancel | Event not bound");
							}

							
							return FReply::Handled();
						})
					]
				]
			]
		]
	];

	CurrentInterval = .0f;
	CurrentInterval2 = .0f;
	bIsFound = false;
	FoundIndex = INDEX_NONE;
}
END_SLATE_FUNCTION_BUILD_OPTIMIZATION

void SSearchFightScreen::ToggleWidget(const bool InToggle)
{
	Anim_Background->ToggleWidget(InToggle);
	SUsableCompoundWidget::ToggleWidget(InToggle);

	if (InToggle)
	{
		SwitchImage();
	}
}

void SSearchFightScreen::SetFound(bool IsFound, int32 InIndex)
{
	FoundIndex = InIndex;

	if (bIsFound != IsFound)
	{
		bIsFound = IsFound;

		if (bIsFound)
		{
			SwitchImage();
		}
	}
}

void SSearchFightScreen::AddReferencedObjects(FReferenceCollector& Collector)
{
	Collector.AddReferencedObjects<UUserWidget>(UserWidgets);
}

const FSlateBrush* SSearchFightScreen::GetBackground() const
{
	if (MapImages.Num() >= 2)
	{
		const auto ArrayIndex = (bIsFound && FoundIndex != INDEX_NONE) ? MapImages.FindLastByPredicate([&](const FMapNameAndImage& InItem) { return InItem.Index == FoundIndex; }) : CurrentImage;

		if (MapImages.IsValidIndex(ArrayIndex))
		{
			return &MapImages[ArrayIndex].Brush;
		}

		return &MapImages.Last().Brush;
	}

	return &Style->Backgound;
}

FText SSearchFightScreen::GetCurrentMapName() const
{
	if (MapImages.Num() >= 2)
	{
		const auto ArrayIndex = (bIsFound && FoundIndex != INDEX_NONE) ? MapImages.FindLastByPredicate([&](const FMapNameAndImage& InItem) { return InItem.Index == FoundIndex; }) : CurrentImage;

		if (MapImages.IsValidIndex(ArrayIndex))
		{
			return MapImages[ArrayIndex].DisplayName;
		}

		return MapImages.Last().DisplayName;
	}

	return FText::GetEmpty();
}

FText SSearchFightScreen::GetCurrentMapDesc() const
{
	if (MapImages.Num() >= 2)
	{
		const auto ArrayIndex = (bIsFound && FoundIndex != INDEX_NONE) ? MapImages.FindLastByPredicate([&](const FMapNameAndImage& InItem) { return InItem.Index == FoundIndex; }) : CurrentImage;

		if (MapImages.IsValidIndex(ArrayIndex))
		{
			return MapImages[ArrayIndex].Description;
		}

		return MapImages.Last().Description;
	}

	return FText::GetEmpty();
}

void SSearchFightScreen::Tick(const FGeometry& AllottedGeometry, const double InCurrentTime, const float InDeltaTime)
{
	SUsableCompoundWidget::Tick(AllottedGeometry, InCurrentTime, InDeltaTime);

	CurrentInterval += InDeltaTime;
	CurrentInterval2 += InDeltaTime;

	if (CurrentInterval > 3.5f && bIsFound == false)
	{		
		CurrentInterval = .0f;
		SwitchImage();
	}

	if (CurrentInterval2 > 9.0f)
	{
		CurrentInterval2 = .0f;
		SwitchUserWidget();
	}
}

void SSearchFightScreen::SwitchImage()
{
	CurrentImage++;

	if (CurrentImage > MapImages.Num())
	{
		CurrentImage = 0;
	}

	if (bIsFirstImage)
	{
		Anim_Image[0]->ToggleWidget(false);
		Anim_Image[1]->ToggleWidget(true);

		Widget_Images[1]->SetImage(GetBackground());
	}
	else
	{
		Anim_Image[1]->ToggleWidget(false);
		Anim_Image[0]->ToggleWidget(true);

		Widget_Images[0]->SetImage(GetBackground());
	}

	bIsFirstImage = !bIsFirstImage;
}

void SSearchFightScreen::SwitchUserWidget()
{
	if (UserWidgets.Num() > 1)
	{
		auto RandomUserWidget = UserWidgets[FMath::RandRange(0, UserWidgets.Num() - 1)];
		auto TakedWidget = RandomUserWidget->TakeWidget();

		if (bIsFirstImage)
		{
			Anim_UserWidget[0]->ToggleWidget(false);
			Anim_UserWidget[1]->ToggleWidget(true);

			Anim_UserWidget[1]->SetContent(TakedWidget);
		}
		else
		{
			Anim_UserWidget[1]->ToggleWidget(false);
			Anim_UserWidget[0]->ToggleWidget(true);

			Anim_UserWidget[0]->SetContent(TakedWidget);
		}

		bIsFirstUserWidget = !bIsFirstUserWidget;

		TakedWidget->SetCanTick(true);
	}
}
