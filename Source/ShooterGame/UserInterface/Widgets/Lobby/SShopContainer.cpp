// Fill out your copyright notice in the Description page of Project Settings.

#include "ShooterGame.h"
#include "SShopContainer.h"
#include "SlateOptMacros.h"
#include "Components/SCurrencyBlock.h"
#include "Utilities/SAnimatedBackground.h"
#include "Containers/SAdaptiveCarousel.h"
#include "Components/SResourceTextBox.h"
#include "SScaleBox.h"

#include "Item/Product/ProductItemEntity.h"
#include "Containers/STabbedContainer.h"
#include "Components/SShopItem.h"
#include "Components/SShopCaseItem.h"
#include "Localization/TableWeaponStrings.h"
#include "ShooterGameAnalytics.h"
#include "UObjectExtensions.h"
#include "Components/SDiscountBlock.h"
//#include "CrashlyticsBlueprintLibrary.h"

BEGIN_SLATE_FUNCTION_BUILD_OPTIMIZATION
void SShopContainer::Construct(const FArguments& InArgs)
{
	OnClickedBack = InArgs._OnClickedBack;
	OnRequestBuy = InArgs._OnRequestBuy;

	ChildSlot
	[
		SAssignNew(Anim_Back, SAnimatedBackground)
		.Visibility(EVisibility::Visible)
		.IsEnabledBlur(true)
		.Duration(.5f)
		.ShowAnimation(EAnimBackAnimation::UpFade)
		.HideAnimation(EAnimBackAnimation::DownFade)
		.InitAsHide(true)
		[
			SNew(SOverlay)
			+ SOverlay::Slot()
			[
				SNew(SVerticalBox)
				+ SVerticalBox::Slot().AutoHeight() // Back btn
				[
					SNew(SOverlay)
					+ SOverlay::Slot()
					[
						SNew(SHorizontalBox)
						+ SHorizontalBox::Slot().AutoWidth()
						[
							SNew(SButton)
							.ButtonStyle(&FShooterStyle::Get().GetWidgetStyle<FButtonStyle>("Default_BackButton"))
							.ContentPadding(80)
							.HAlign(HAlign_Center)
							.VAlign(VAlign_Center)
							.OnClicked_Lambda([&]()
							{		
								FShooterGameAnalytics::RecordDesignEvent("UI:Lobby:Market:Back");

								if (OnClickedBack.ExecuteIfBound() == false)
								{
									FShooterGameAnalytics::RecordErrorEvent(EErrorSeverity::warning, "OnMarket_ClickedBack | Event not bound");
								}

								ToggleWidget(false);
								return FReply::Handled();
							})
						]
						+ SHorizontalBox::Slot()
						+ SHorizontalBox::Slot().AutoWidth()
						[
							SCurrencyBlock::Get()->Generate(true)
						]
					]
					+ SOverlay::Slot().VAlign(VAlign_Top).HAlign(HAlign_Center)
					[
						SNew(SBox).HeightOverride(100).VAlign(VAlign_Center)
						[
							SNew(STextBlock)
							.Text(NSLOCTEXT("SShopContainer", "SShopContainer.Title", "Shop"))
							.TextStyle(&FShooterStyle::Get().GetWidgetStyle<FTextBlockStyle>("Default_HeaderText"))
						]
					]
				]
				+ SVerticalBox::Slot() // Carusel
				[
					SAssignNew(Widget_TabbedContainer, STabbedContainer).IsCenteredControlls(false)
					//+ STabbedContainer::Slot().Name(FTableItemStrings::GetProductText(EProductItemCategory::TrophyCases, true))
					//	.OnActivated_Lambda([&]() {FShooterGameAnalytics::RecordContentSearch("UI:Lobby:Market:TrophyCases"); })
					//[
					//	SAssignNew(Widget_ItemsContainer[static_cast<uint8>(EProductItemCategory::TrophyCases)], SAdaptiveCarousel)
					//]
					+ STabbedContainer::Slot().Name(FTableItemStrings::GetProductText(EProductItemCategory::Cases, true))
					.OnActivated_Lambda([&]() {FShooterGameAnalytics::RecordContentSearch("UI:Lobby:Market:Cases"); })
					[
						SAssignNew(Widget_ItemsContainer[static_cast<uint8>(EProductItemCategory::Cases)], SAdaptiveCarousel).OnDoubleTouch(this, &SShopContainer::OnDoubleClickedItem)
					]
					+ STabbedContainer::Slot().Name(FTableItemStrings::GetProductText(EProductItemCategory::Money, false))
					.OnActivated_Lambda([&]() {FShooterGameAnalytics::RecordContentSearch("UI:Lobby:Market:Money"); })
					[
						SAssignNew(Widget_ItemsContainer[static_cast<uint8>(EProductItemCategory::Money)], SAdaptiveCarousel).OnDoubleTouch(this, &SShopContainer::OnDoubleClickedItem)
					]
					+ STabbedContainer::Slot().Name(FTableItemStrings::GetProductText(EProductItemCategory::Donate, false))
					.OnActivated_Lambda([&]() {FShooterGameAnalytics::RecordContentSearch("UI:Lobby:Market:Donate"); })
					[
						SAssignNew(Widget_ItemsContainer[static_cast<uint8>(EProductItemCategory::Donate)], SAdaptiveCarousel).OnDoubleTouch(this, &SShopContainer::OnDoubleClickedItem)
					]
					+ STabbedContainer::Slot().Name(FTableItemStrings::GetProductText(EProductItemCategory::Booster, false))
					.OnActivated_Lambda([&]() {FShooterGameAnalytics::RecordContentSearch("UI:Lobby:Market:Booster"); })
					[
						SAssignNew(Widget_ItemsContainer[static_cast<uint8>(EProductItemCategory::Booster)], SAdaptiveCarousel).OnDoubleTouch(this, &SShopContainer::OnDoubleClickedItem)
					]
					//+ STabbedContainer::Slot().Name(FTableItemStrings::GetProductText(EProductItemCategory::BattleCoins, true))
					//	.OnActivated_Lambda([&]() {FShooterGameAnalytics::RecordContentSearch("UI:Lobby:Market:BattleCoins"); })
					//[
					//	SAssignNew(Widget_ItemsContainer[static_cast<uint8>(EProductItemCategory::BattleCoins)], SAdaptiveCarousel)
					//]
				]
				+ SVerticalBox::Slot().AutoHeight()
				[
					SNew(SHorizontalBox)
					+ SHorizontalBox::Slot()
					+ SHorizontalBox::Slot()
					[
						SNew(SButton).Tag(TEXT("SShopContainer.Button.Action"))
						.ButtonStyle(&FShooterStyle::Get().GetWidgetStyle<FButtonStyle>("Default_ActionButton"))
						.OnClicked(this, &SShopContainer::OnClickedBuyButton)
						.HAlign(HAlign_Center)
						.VAlign(VAlign_Center)
						.ContentPadding(0)
						[
							SNew(SBox).HeightOverride(80).HAlign(HAlign_Center).VAlign(VAlign_Center)
							[
								SNew(SHorizontalBox)
								+ SHorizontalBox::Slot().VAlign(VAlign_Top).AutoWidth()
								[
									SNew(SResourceTextBox)
									.IsEnabled(false)
									.CustomSize(FVector(32, 32, 14))
									.TypeAndValue(this, &SShopContainer::GetOriginalItemCost)
									.Visibility(this, &SShopContainer::GetOriginalItemCostVisibility)
									.OverlayMethod(EResourceTextBoxOverlayMethod::StrikeOut)
								]
								+ SHorizontalBox::Slot().AutoWidth()
								[
									SNew(SResourceTextBox)
									.CustomSize(FVector(64, 64, 28))								
									.TypeAndValue(this, &SShopContainer::GetActualItemCost)
								]
							]
						]
					]
					+ SHorizontalBox::Slot()
				]
			]
			+ SOverlay::Slot()
			[
				SNew(SHorizontalBox)
				.Visibility(this, &SShopContainer::GetDiscountBlockVisibilityState)
				+ SHorizontalBox::Slot()
				+ SHorizontalBox::Slot().VAlign(VAlign_Center).HAlign(HAlign_Left)
				[
					SNew(SDiscountBlock).Tag(TEXT("SShopContainer.Discount"))
					.Product_Lambda([&]() -> UBaseProductItemEntity*
					{
						return GetActiveEntityItem().Get();
					})
				]
				+ SHorizontalBox::Slot()
			]
		]
	];

	//TabbedMap.Add(EProductItemCategory::TrophyCases, 0);
	//TabbedMap.Add(EProductItemCategory::Cases, 1);
	//TabbedMap.Add(EProductItemCategory::Money, 2);
	//TabbedMap.Add(EProductItemCategory::Donate, 3);
	//TabbedMap.Add(EProductItemCategory::Booster, 4);
	//TabbedMap.Add(EProductItemCategory::BattleCoins, 5);

	//TabbedMap.Add(EProductItemCategory::TrophyCases, 0);
	TabbedMap.Add(EProductItemCategory::Cases, 0);
	TabbedMap.Add(EProductItemCategory::Money, 1);
	TabbedMap.Add(EProductItemCategory::Donate, 2);
	TabbedMap.Add(EProductItemCategory::Booster, 3);
	//TabbedMap.Add(EProductItemCategory::BattleCoins, 5);
}
END_SLATE_FUNCTION_BUILD_OPTIMIZATION

EVisibility SShopContainer::GetDiscountBlockVisibilityState() const
{
	if (auto ProductTarget = GetActiveEntityItem().Get())
	{
		if (ProductTarget->IsDiscountedProduct())
		{
			return EVisibility::HitTestInvisible;
		}
	}

	return EVisibility::Collapsed;
}

void SShopContainer::ToggleWidget(const bool InToggle)
{
	if (InToggle)
	{
		FShooterGameAnalytics::RecordDesignEvent("UI:Lobby:Market:ToggleWidget");
	}

	SUsableCompoundWidget::ToggleWidget(InToggle);
	Anim_Back->ToggleWidget(InToggle);

	//UCrashlyticsBlueprintLibrary::LogFbCustomEvent(InToggle ? TEXT("Open_Store_Shop") : TEXT("Close_Store_Shop"));
}

void SShopContainer::ToggleWidget(const bool InToggle, EGameCurrency InTarget)
{
	if (InTarget == EGameCurrency::Money)
	{
		FShooterGameAnalytics::RecordContentSearch("UI:Lobby:Market:Money");
		Widget_TabbedContainer->SetActiveSlot(TabbedMap[EProductItemCategory::Money]);
	}
	else if (InTarget == EGameCurrency::Donate)
	{
		FShooterGameAnalytics::RecordContentSearch("UI:Lobby:Market:Donate");
		Widget_TabbedContainer->SetActiveSlot(TabbedMap[EProductItemCategory::Donate]);
	}
	else if (InTarget == EGameCurrency::BattleCoins)
	{
		FShooterGameAnalytics::RecordContentSearch("UI:Lobby:Market:Premium");
		Widget_TabbedContainer->SetActiveSlot(TabbedMap[EProductItemCategory::Booster]);
	}

	ToggleWidget(InToggle);
}

bool SShopContainer::IsInInteractiveMode() const
{
	return Anim_Back->IsAnimAtEnd();
}

void SShopContainer::OnFill(const TArray<UBaseProductItemEntity*>& InItems)
{
	for (uint8 i = 0; i < static_cast<uint8>(EProductItemCategory::End); ++i)
	{
		if (Widget_ItemsContainer[i].IsValid())
		{
			Widget_ItemsContainer[i]->ClearChildren();
		}
	}

	auto FilteredItems = InItems.FilterByPredicate([](UBaseProductItemEntity* Source)
	{
		return GetValidObject(Source) && Source->IsHiddenInShop() == false;
	});

	uint16 _count[static_cast<uint8>(EProductItemCategory::End)] = { 0 };
	for (auto Item : FilteredItems)
	{
		if (Item && Item->IsValidLowLevel())
		{
			const auto Type = Item->GetProductCategoryId();
			if (Widget_ItemsContainer[Type].IsValid())
			{
				Widget_ItemsContainer[Type]->AddSlot()
				[
					FactoryShopItemWidget(Item, float(_count[Type]))
				];
				++_count[Type];
			}
		}
	}
}

void SShopContainer::ScrollTo(UBasicItemEntity* InBaseTargetItem)
{
	auto InTarget = GetValidObjectAs<UBaseProductItemEntity>(InBaseTargetItem);
	if (InTarget && InTarget->IsValidLowLevel())
	{
		const auto TargetType = InTarget->GetProductCategory();
		if (auto ActiveTab = TabbedMap.Find(TargetType))
		{
			Widget_TabbedContainer->SetActiveSlot(*ActiveTab);

			auto RawTargetType = static_cast<uint8>(TargetType);
			if (Widget_ItemsContainer[RawTargetType].IsValid())
			{
				const auto Childs = Widget_ItemsContainer[RawTargetType]->GetChildren();

				for (SIZE_T i = 0; i < Childs->Num(); ++i)
				{
					TSharedRef<SShopItem> MyWidget = StaticCastSharedRef<SShopItem>(Childs->GetChildAt(i));
					if (MyWidget != SNullWidget::NullWidget)
					{
						if (MyWidget->GetInstanceBasic().IsValid() && MyWidget->GetInstanceBasic().Get() == InTarget)
						{
							Widget_ItemsContainer[RawTargetType]->ScrollToWidget(MyWidget);
							break;
						}
					}
				}
			}
		}
	}
}

TSharedRef<SShopItem> SShopContainer::FactoryShopItemWidget(UBaseProductItemEntity* InEntity, const float& fB)
{
	const auto ProductCategory = InEntity->GetProductCategory();
	const auto ProductCategoryId = InEntity->GetProductCategoryId();

	if (ProductCategory == EProductItemCategory::Cases || ProductCategory == EProductItemCategory::TrophyCases)
	{
		return SNew(SShopCaseItem, InEntity).Tag(*FString::Printf(TEXT("SShopContainer.ShopItem_%d"), InEntity->GetBaseProductProperty().ModelId))
		.ActiveCoefficient_Lambda([&, t = ProductCategoryId, fB]() -> float
		{
			const float fA = Widget_ItemsContainer[t]->GetCurrentSlot();
			return FMath::Clamp(FMath::Abs<float>(fA - fB), .0f, 1.0f);
		});
	}

	return SNew(SShopItem, InEntity).Tag(*FString::Printf(TEXT("SShopContainer.ShopItem_%d"), InEntity->GetBaseProductProperty().ModelId))
	.ActiveCoefficient_Lambda([&, t = ProductCategoryId, fB]() -> float
	{
		const float fA = Widget_ItemsContainer[t]->GetCurrentSlot();
		return FMath::Clamp(FMath::Abs<float>(fA - fB), .0f, 1.0f);
	});
}


TWeakObjectPtr<UBaseProductItemEntity> SShopContainer::GetActiveEntityItem() const
{
	if (auto ActiveType = TabbedMap.FindKey(Widget_TabbedContainer->GetActiveSlot()))
	{
		auto MyType = static_cast<uint8>(*ActiveType);
		if (Widget_ItemsContainer[MyType].IsValid())
		{
			TSharedRef<SShopItem> MyWidget = StaticCastSharedRef<SShopItem>(Widget_ItemsContainer[MyType]->GetActiveSlotWidget());

			return GetValidObjectAs<UBaseProductItemEntity>(MyWidget->GetInstanceBasic().Get());
		}
	}

	return nullptr;
}

EVisibility SShopContainer::GetOriginalItemCostVisibility() const
{
	if (IsItemDiscounted())
	{
		return EVisibility::Visible;
	}
	return EVisibility::Collapsed;
}

bool SShopContainer::IsItemDiscounted() const
{
	auto MyItem = GetActiveEntityItem();
	if(auto Product = GetValidObject(MyItem))
	{
		return Product->IsDiscountedProduct();
	}
	return false;
}

FResourceTextBoxValue SShopContainer::GetActualItemCost() const
{
	auto MyItem = GetActiveEntityItem();
	if (auto Product = GetValidObject(MyItem))
	{
		if (Product->IsDiscountedProduct())
		{
			auto Cost = Product->GetDiscountedCost();
			if (Cost)
			{
				return FResourceTextBoxValue::Factory(*Cost);
			}
		}

		return GetOriginalItemCost();
	}

	return FResourceTextBoxValue::Money(0);
}

FResourceTextBoxValue SShopContainer::GetOriginalItemCost() const
{
	auto MyItem = GetActiveEntityItem();
	if (MyItem.IsValid())
	{
		const auto cost = MyItem->GetItemCost();
		return FResourceTextBoxValue::Factory(cost);
	}

	return FResourceTextBoxValue::Money(0);
}

FReply SShopContainer::OnClickedBuyButton()
{
	auto TargetItem = GetActiveEntityItem();
	if (IsValidObject(TargetItem))
	{
		FShooterGameAnalytics::RecordDesignEvent("UI:Lobby:Market:Buy");
		if (OnRequestBuy.ExecuteIfBound(TargetItem.Get()) == false)
		{
			FShooterGameAnalytics::RecordErrorEvent(EErrorSeverity::warning, "OnMarket_Clicked_Buy | Event not bound");
		}
	}

	return FReply::Handled();
}

void SShopContainer::OnDoubleClickedItem()
{
	OnClickedBuyButton();
}
