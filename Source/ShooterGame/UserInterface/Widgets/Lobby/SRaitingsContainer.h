// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "Utilities/SlateUtilityTypes.h"
#include "Utilities/SUsableCompoundWidget.h"
#include "Styles/RaitingsContainerWidgetStyle.h"

struct FRatingTopContainer;
struct FPlayerRatingTopContainer;
class SAnimatedBackground;
/**
 * 
 */
class SHOOTERGAME_API SRaitingsContainer : public SUsableCompoundWidget
{
public:
	SLATE_BEGIN_ARGS(SRaitingsContainer)
		: _Style(&FShooterStyle::Get().GetWidgetStyle<FRaitingsContainerStyle>("SRaitingsContainerStyle"))
	{
		_Visibility = EVisibility::SelfHitTestInvisible;
	}
	SLATE_STYLE_ARGUMENT(FRaitingsContainerStyle, Style)
	SLATE_EVENT(FOnClickedOutside, OnClickedBack)
	SLATE_EVENT(FOnGuid, OnPlayerSelected)
	SLATE_END_ARGS()

	void Construct(const FArguments& InArgs);

	virtual void ToggleWidget(const bool InToggle) override;
	virtual bool IsInInteractiveMode() const override;

	void SetInformation(const FRatingTopContainer& InTopData);

	static FName Column_Num;
	static FName Column_Name;
	static FName Column_Score;

protected:

	const FRaitingsContainerStyle* Style;

	FOnGuid OnPlayerSelected;

	TSharedPtr<SAnimatedBackground>	Anim_Back;
	TSharedPtr<class SRadioButtonsBox> Widget_Controlls;

	TSharedPtr<SListView<TSharedPtr<FPlayerRatingTopContainer>>>	Raitings_Table, RaitingsELO_Table;
	TArray<TSharedPtr<FPlayerRatingTopContainer>>					Raitings_List, RaitingsELO_List;

	uint32 CurrentNumRefresh;

	int32 GetActiveRaitingIndex() const;
};
