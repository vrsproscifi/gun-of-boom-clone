// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "Utilities/SUsableCompoundWidget.h"

class UIdentityComponent;
class SAnimatedBackground;
class SAdaptiveCarousel;
class STabbedContainer;

class SShopItem;
class UBasicItemEntity;

class SResourceTextBox;

DECLARE_DELEGATE_OneParam(FOnClickedIntoArsenal, UBasicItemEntity*);
/**
 * 
 */
class SHOOTERGAME_API SNewLevelContainer : public SUsableCompoundWidget
{
public:
	SLATE_BEGIN_ARGS(SNewLevelContainer)
	{
		_Visibility = EVisibility::SelfHitTestInvisible;
	}
	SLATE_EVENT(FOnClickedOutside, OnClickedBack)
	SLATE_EVENT(FOnClickedIntoArsenal, OnClickedArsenal)
	SLATE_ARGUMENT(TWeakObjectPtr<UIdentityComponent>, IdentityComponent)
	SLATE_END_ARGS()

	void Construct(const FArguments& InArgs);

	virtual void ToggleWidget(const bool InToggle) override;
	virtual bool IsInInteractiveMode() const override;
	virtual void HideWidget() override;

	void UpdateNewAvalibleItems(const TArray<UBasicItemEntity*>& InNewAvalibleItems);
	void UpdateRewardItems(const TArray<struct FLevelRewardItem>& InRewardItems);

protected:
	TSharedRef<SShopItem> FactoryItemWidget(const TSharedPtr<SAdaptiveCarousel>& InWidgetContext, UBasicItemEntity* InEntity, const float& fB);


	void UpdateInformation();

	TSharedPtr<SResourceTextBox>		Widget_MoneyReward;
	TSharedPtr<SResourceTextBox>		Widget_DonateReward;

	TSharedPtr<SWidgetSwitcher>			Widget_TabbedContainer;
	TSharedPtr<SAdaptiveCarousel>		Widget_NewAvalibleItemCarousel;
	TSharedPtr<SAdaptiveCarousel>		Widget_RewardItemsCarousel;

	TSharedPtr<SAnimatedBackground>		Anim_Back;

	FOnClickedOutside OnClickedBack;
	TWeakObjectPtr<UIdentityComponent>	IdentityComponent;

	FText GetAvailableItemNum() const;

};
