// Fill out your copyright notice in the Description page of Project Settings.

#include "ShooterGame.h"
#include "SNewLevelContainer.h"
#include "SlateOptMacros.h"
#include "Components/STitleHeader.h"
#include "Utilities/SAnimatedBackground.h"
#include "Components/SPlayerNameAndProgress.h"
#include "Localization/TableBaseStrings.h"
#include "Components/SResourceTextBox.h"
#include "GameSingletonExtensions.h"
#include "BasicItemEntity.h"
#include "IdentityComponent.h"
#include "SScaleBox.h"
#include "Styles/ArsenalContainerWidgetStyle.h"
#include "Utilities/SlateUtilityTypes.h"

#include "Containers/SAdaptiveCarousel.h"
#include "Award/LevelAward/LevelAwardEntity.h"
#include "Containers/STabbedContainer.h"
#include "Components/SShopItem.h"
#include "Styles/LobbyContainerWidgetStyle.h"
#include "ShooterGameAnalytics.h"

BEGIN_SLATE_FUNCTION_BUILD_OPTIMIZATION
void SNewLevelContainer::Construct(const FArguments& InArgs)
{
	OnClickedBack = InArgs._OnClickedBack;
	IdentityComponent = InArgs._IdentityComponent;

	auto ArsenalStyle = &FShooterStyle::Get().GetWidgetStyle<FArsenalContainerStyle>("SArsenalContainerStyle");
	auto LobbyStyle = &FShooterStyle::Get().GetWidgetStyle<FLobbyContainerStyle>("SLobbyContainerStyle");

	const TCHAR sArsenal[] = { static_cast<TCHAR>(0xf07a), 0 }; // fa-solid

	ChildSlot
	[
		SAssignNew(Anim_Back, SAnimatedBackground)
		.Visibility(EVisibility::SelfHitTestInvisible)
		.IsEnabledBlur(false)
		.Duration(.5f)
		.ShowAnimation(EAnimBackAnimation::UpFade)
		.HideAnimation(EAnimBackAnimation::DownFade)
		.InitAsHide(true)
		[
			SNew(SVerticalBox)
			+ SVerticalBox::Slot().AutoHeight()
			[
				SNew(STitleHeader)
				.ShowBack(false)
				.Header(NSLOCTEXT("SNewLevelContainer", "SNewLevelContainer.Title", "New Level"))
				.OnClickedBack_Lambda([&]()
				{
					HideWidget();
				})
			]
			+ SVerticalBox::Slot()
			[
				SNew(SHorizontalBox)
				+ SHorizontalBox::Slot()
				+ SHorizontalBox::Slot()
				[
					SNew(SAnimatedBackground)
					.Visibility(EVisibility::Visible)
					.IsEnabledBlur(true)
					.InitAsHide(false)
					[
						SAssignNew(Widget_TabbedContainer, SWidgetSwitcher)
						
						+ SWidgetSwitcher::Slot()
						[
							SNew(SVerticalBox)
							+ SVerticalBox::Slot().AutoHeight().HAlign(HAlign_Left).Padding(10, 5)
							[
								SNew(SPlayerNameAndProgress).IdentityComponent(IdentityComponent)
							]
							+ SVerticalBox::Slot().AutoHeight().Padding(5)
							[
								SNew(STextBlock)
								.AutoWrapText(true)
								.Text(NSLOCTEXT("SNewLevelContainer", "SNewLevelContainer.LevelReward", "Your reward for reaching the new level!"))
								.TextStyle(&FShooterStyle::Get().GetWidgetStyle<FTextBlockStyle>("Default_ActionText"))
							]
							+ SVerticalBox::Slot().Padding(5).AutoHeight()
							[
								SNew(SHorizontalBox)
								+ SHorizontalBox::Slot().Padding(10) // Money
								[
									SAssignNew(Widget_MoneyReward, SResourceTextBox)
									.CustomSize(FVector(64, 64, 32))
									.Type(EResourceTextBoxType::Money)
									.Value(500)
								]
								+ SHorizontalBox::Slot().Padding(10) // Donate
								[
									SAssignNew(Widget_DonateReward, SResourceTextBox)
									.CustomSize(FVector(64, 64, 32))
									.Type(EResourceTextBoxType::Donate)
									.Value(10)
								]
							]
							+ SVerticalBox::Slot().Padding(5)
							[
								SAssignNew(Widget_RewardItemsCarousel, SAdaptiveCarousel).Clipping(EWidgetClipping::ClipToBoundsAlways)
							]
							+ SVerticalBox::Slot().VAlign(VAlign_Bottom).HAlign(HAlign_Right).AutoHeight().Padding(20)
							[
								SNew(SButton)
								.ContentPadding(FMargin(20))
								.TextStyle(&FShooterStyle::Get().GetWidgetStyle<FTextBlockStyle>("Default_ActionText"))
								.ButtonStyle(&FShooterStyle::Get().GetWidgetStyle<FButtonStyle>("Default_ActionButton"))
								.HAlign(HAlign_Center)
								.VAlign(VAlign_Center)
								.Text(FTableBaseStrings::GetBaseText(EBaseStrings::Accept))
								.OnClicked_Lambda([&]()
								{									
									Widget_TabbedContainer->SetActiveWidgetIndex(1);
									return FReply::Handled();
								})
							]
						]
						+ SWidgetSwitcher::Slot()
						[
							SNew(SVerticalBox)
							+ SVerticalBox::Slot().AutoHeight().HAlign(HAlign_Left).Padding(10)
							[
								SNew(SPlayerNameAndProgress).IdentityComponent(IdentityComponent)
							]
							+ SVerticalBox::Slot().AutoHeight().Padding(10)
							[
								SNew(STextBlock)
								.AutoWrapText(true)
								.Text(this, &SNewLevelContainer::GetAvailableItemNum)
	
								.TextStyle(&FShooterStyle::Get().GetWidgetStyle<FTextBlockStyle>("Default_ActionText"))
							]
							+ SVerticalBox::Slot().Padding(10)
							[
								SAssignNew(Widget_NewAvalibleItemCarousel, SAdaptiveCarousel).Clipping(EWidgetClipping::ClipToBoundsAlways)
							]
							+ SVerticalBox::Slot().VAlign(VAlign_Bottom).AutoHeight().Padding(20)
							[
								SNew(SHorizontalBox)
								+ SHorizontalBox::Slot().Padding(20, 0).HAlign(HAlign_Right)
								[
									SNew(SButton)
									.HAlign(HAlign_Center)
									.VAlign(VAlign_Center)
									.ContentPadding(FMargin(80, 16))
									.ButtonStyle(&FShooterStyle::Get().GetWidgetStyle<FButtonStyle>("Default_ActionButton"))
									.TextStyle(&FShooterStyle::Get().GetWidgetStyle<FTextBlockStyle>("Default_ActionText"))
									.Text(NSLOCTEXT("SNewLevelContainer", "SNewLevelContainer.IntoArsenal", "Into Arsenal"))
									.OnClicked_Lambda([&, e = InArgs._OnClickedArsenal]()
									{
										FWidgetOrderedSupportQueue::AddQueue(TEXT("SArsenalContainer"), FOnClickedOutside::CreateLambda([&, se = e]()
										{
											TSharedRef<SShopItem> ShopWidget = StaticCastSharedRef<SShopItem>(Widget_NewAvalibleItemCarousel->GetActiveSlotWidget());
											if (ShopWidget != SNullWidget::NullWidget)
											{
												se.ExecuteIfBound(ShopWidget->GetInstanceBasic().Get());												
											}
											else
											{
												se.ExecuteIfBound(nullptr);
											}
										}), ORDERED_QUEUE_NEWLEVEL + 1);

										HideWidget();

										return FReply::Handled();
									})
									//[
									//	SNew(STextBlock)
									//	.TextStyle(&LobbyStyle->FontAwesome)
									//	.Text(FText::FromString(sArsenal))
									//	.Margin(FMargin(0, 0, 6, 0))
									//]
								]
								/*+ SHorizontalBox::Slot().Padding(20, 0)
								[
									SNew(SButton)
									.ContentPadding(FMargin(20))
									.TextStyle(&FShooterStyle::Get().GetWidgetStyle<FTextBlockStyle>("Default_ActionText"))
									.ButtonStyle(&FShooterStyle::Get().GetWidgetStyle<FButtonStyle>("Default_ActionButton"))
									.HAlign(HAlign_Center)
									.VAlign(VAlign_Center)
									.Text(FTableBaseStrings::GetBaseText(EBaseStrings::Continue))
									.OnClicked_Lambda([&]()
									{
										HideWidget();
										return FReply::Handled();
									})
								]*/
							]
						]
					]
				]
			]
		]
	];
}
END_SLATE_FUNCTION_BUILD_OPTIMIZATION

FText SNewLevelContainer::GetAvailableItemNum() const
{
	if (Widget_NewAvalibleItemCarousel.IsValid())
	{
		return FText::Format(NSLOCTEXT("SNewLevelContainer", "SNewLevelContainer.AvailableItems", "New Available Items: {0}"), FText::AsNumber(Widget_NewAvalibleItemCarousel->GetNumChildrens(), &FNumberFormattingOptions::DefaultNoGrouping()));
	}
	return FText::Format(NSLOCTEXT("SNewLevelContainer", "SNewLevelContainer.AvailableItems", "New Available Items: {0}"), FText::AsNumber(0, &FNumberFormattingOptions::DefaultNoGrouping()));
}


void SNewLevelContainer::ToggleWidget(const bool InToggle)
{
	if (InToggle)
	{
		if (FWidgetOrderedSupportQueue::GetCurrentQueueId() == TEXT("OnPlayerLevelUp"))
		{
			SUsableCompoundWidget::ToggleWidget(InToggle);
			Anim_Back->ToggleWidget(InToggle);
			UpdateInformation();
		}

		FWidgetOrderedSupportQueue::AddQueue(TEXT("OnPlayerLevelUp"), FOnClickedOutside::CreateLambda([&, t = InToggle]() {
			SUsableCompoundWidget::ToggleWidget(t);
			Anim_Back->ToggleWidget(t);
			UpdateInformation();
		}), ORDERED_QUEUE_NEWLEVEL, true);
	}
	else
	{
		SUsableCompoundWidget::ToggleWidget(false);
		Anim_Back->ToggleWidget(false);
		OnClickedBack.ExecuteIfBound();
	}
}

bool SNewLevelContainer::IsInInteractiveMode() const
{
	return Anim_Back->IsAnimAtEnd();
}

void SNewLevelContainer::HideWidget()
{
	SUsableCompoundWidget::HideWidget();
	FWidgetOrderedSupportQueue::RemoveQueue(TEXT("OnPlayerLevelUp"));
}

TSharedRef<SShopItem> SNewLevelContainer::FactoryItemWidget(const TSharedPtr<SAdaptiveCarousel>& InWidgetContext, UBasicItemEntity* InEntity, const float& fB)
{
	return SNew(SShopItem, InEntity).ActiveCoefficient_Lambda([&, InWidgetContext, fB]() -> float
	{
		const float fA = InWidgetContext->GetCurrentSlot();
		return FMath::Clamp(FMath::Abs<float>(fA - fB), .0f, 1.0f);
	});
}

void SNewLevelContainer::UpdateNewAvalibleItems(const TArray<UBasicItemEntity*>& InNewAvalibleItems)
{
	//----------------------------------------------------------------
	Widget_NewAvalibleItemCarousel->ClearChildren();

	//----------------------------------------------------------------
	int32 Count = 0;

	//----------------------------------------------------------------
	for (auto Item : InNewAvalibleItems)
	{
		Widget_NewAvalibleItemCarousel->AddSlot()
		[
			FactoryItemWidget(Widget_NewAvalibleItemCarousel, Item, float(Count))
		];
		Count++;
	}

	//----------------------------------------------------------------
	//	Делаем активным второй предмет (первый слот) что бы предметы шли без пустого места слева
	Widget_NewAvalibleItemCarousel->ScrollToSlot(1);
}

void SNewLevelContainer::UpdateRewardItems(const TArray<FLevelRewardItem>& InRewardItems)
{
	//----------------------------------------------------------------
	Widget_RewardItemsCarousel->ClearChildren();

	//----------------------------------------------------------------
	int32 Count = 0;

	//----------------------------------------------------------------
	for (auto Item : InRewardItems)
	{
		Widget_RewardItemsCarousel->AddSlot()
		[
			FactoryItemWidget(Widget_RewardItemsCarousel, Item.GetInstance(), float(Count))
		];
		Count++;
	}

	//----------------------------------------------------------------
	//	Делаем активным второй предмет (первый слот) что бы предметы шли без пустого места слева
	Widget_RewardItemsCarousel->ScrollToSlot(1);
}

void SNewLevelContainer::UpdateInformation()
{
	//======================================
	int32 PlayerLevel = 1;

	//======================================
	if (IsValidObject(IdentityComponent))
	{
		PlayerLevel = IdentityComponent->GetPlayerLevel();
	}

	//======================================
	auto LevelAward = FGameSingletonExtensions::FindLevelAwardById(PlayerLevel);
	if (LevelAward)
	{
		//----------------------------------------------------------------
		const auto  &RewardItems = LevelAward->GetRewardItems();
		const auto  NewAvalibleItems = LevelAward->GetNewAvalibleItems();

		//----------------------------------------------------------------
		UpdateRewardItems(RewardItems);
		UpdateNewAvalibleItems(NewAvalibleItems);

		//----------------------------------------------------------------
		Widget_MoneyReward->SetValue(LevelAward->GetLevelAwardProperty().RewardMoney);
		Widget_DonateReward->SetValue(LevelAward->GetLevelAwardProperty().RewardDonate);

		//----------------------------------------------------------------
	}
	else
	{
		//----------------------------------------------------------------
		const auto NewAvalibleItems = FGameSingletonExtensions::GetItems([PlayerLevel](const UBasicItemEntity* InItemEntity) { return InItemEntity && InItemEntity->GetItemRequiredLevel() == PlayerLevel; });

		//----------------------------------------------------------------
		UpdateNewAvalibleItems(NewAvalibleItems);

		//----------------------------------------------------------------
		//	Открываем страницу с доступными предметами, если не удалось найти награду за уровень
		Widget_TabbedContainer->SetActiveWidgetIndex(1);
	}
}
