// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "Widgets/SCompoundWidget.h"
#include "Styles/NameAndHealthWidgetStyle.h"

class UTeamColorsData;
class AShooterCharacter;
/**
 * 
 */
class SHOOTERGAME_API SNameAndHealth : public SCompoundWidget
{
public:
	SLATE_BEGIN_ARGS(SNameAndHealth)
		: _Style(&FShooterStyle::Get().GetWidgetStyle<FNameAndHealthStyle>("SNameAndHealthStyle"))
	{
		_Visibility = EVisibility::HitTestInvisible;
	}
	SLATE_STYLE_ARGUMENT(FNameAndHealthStyle, Style)
	SLATE_END_ARGS()

	void Construct(const FArguments& InArgs, const TWeakObjectPtr<AShooterCharacter>& InCharacter);

protected:

	const FNameAndHealthStyle* Style;

	TWeakObjectPtr<AShooterCharacter>	MyCharacter;
	TWeakObjectPtr<UTeamColorsData>		TeamColorsPtr;

	FText GetPlayerNameText() const;
	TOptional<float> GetPlayerHealthPercent() const;
	TOptional<float> GetPlayerArmourPercent() const;
	FSlateColor GetPlayerNameColor() const;
	EVisibility GetPlayerNameVisibility() const;
	float GetScaleByDistance() const;
};
