// Fill out your copyright notice in the Description page of Project Settings.

#include "ShooterGame.h"
#include "SGameTimeAndScores.h"
#include "SlateOptMacros.h"
#include "GameState/ShooterGameState.h"
#include "GameState/ShooterPlayerState.h"
#include "ShooterGameSingleton.h"
#include "TeamColorsData.h"
#include "SRetainerWidget.h"

BEGIN_SLATE_FUNCTION_BUILD_OPTIMIZATION
void SGameTimeAndScores::Construct(const FArguments& InArgs)
{
	Style = InArgs._Style;
	GameStatePtr = InArgs._GameState;
	PlayerStatePtr = InArgs._PlayerState;

	ChildSlot.HAlign(HAlign_Fill).VAlign(VAlign_Top).Padding(10)
	[
		SNew(SRetainerWidget)
		.Phase(0)
		.PhaseCount(10)
		.RenderOnPhase(true)
		.RenderOnInvalidation(false)
		.Visibility(EVisibility::SelfHitTestInvisible)
		[
			SNew(SHorizontalBox)
			+ SHorizontalBox::Slot().HAlign(HAlign_Right).Padding(8, 0) // Friendly Score Points
			[
				SNew(STextBlock)
				.Text(this, &SGameTimeAndScores::GetScoreText, true)
				.TextStyle(&Style->FriendlyProgressText)
			]
			+ SHorizontalBox::Slot().AutoWidth() // Score bars & time
			[
				SNew(SVerticalBox)
				+ SVerticalBox::Slot().AutoHeight() // Score bars
				[
					SNew(SBox).WidthOverride(320)
					[
						SNew(SHorizontalBox)
						+ SHorizontalBox::Slot().VAlign(VAlign_Top)
						[
							SNew(SProgressBar)
							.FillColorAndOpacity(this, &SGameTimeAndScores::GetProgressColor, true)
							.BarFillType(EProgressBarFillType::LeftToRight)
							.Percent(this, &SGameTimeAndScores::GetScorePercent, true)
							.Style(&Style->FriendlyProgressBar)
						]
						+ SHorizontalBox::Slot().AutoWidth().VAlign(VAlign_Top)
						[
							SNew(SSeparator)
							.SeparatorImage(new FSlateColorBrush(FColor::White))
							.Thickness(2)
							.Orientation(Orient_Vertical)
						]
						+ SHorizontalBox::Slot().VAlign(VAlign_Top)
						[
							SNew(SProgressBar)
							.FillColorAndOpacity(this, &SGameTimeAndScores::GetProgressColor, false)
							.BarFillType(EProgressBarFillType::RightToLeft)
							.Percent(this, &SGameTimeAndScores::GetScorePercent, false)
							.Style(&Style->EnemyProgressBar)
						]
					]
				]
				+ SVerticalBox::Slot().AutoHeight().HAlign(HAlign_Center) // Time
				[
					SNew(STextBlock)
					.Text(this, &SGameTimeAndScores::GetTimeText)
					.TextStyle(&Style->TimeProgressText)
				]
			]
			+ SHorizontalBox::Slot().HAlign(HAlign_Left).Padding(8, 0) // Enemy Score Points
			[
				SNew(STextBlock)
				.Text(this, &SGameTimeAndScores::GetScoreText, false)
				.TextStyle(&Style->EnemyProgressText)
			]
		]
	];

	auto Assets = UShooterGameSingleton::Get()->GetDataAssets<UTeamColorsData>();
	if (Assets.Num())
	{
		TeamColorsPtr = Assets[0];
	}
}
END_SLATE_FUNCTION_BUILD_OPTIMIZATION

void SGameTimeAndScores::SetGameState(TWeakObjectPtr<AShooterGameState> InGameState)
{
	if (InGameState.IsValid() && InGameState->IsValidLowLevel())
	{
		GameStatePtr = InGameState;
	}
}

TOptional<float> SGameTimeAndScores::GetTimePercent() const
{
	if (IsValidObject(GameStatePtr))
	{
		return float(GameStatePtr->GetRemainingTime()) / float(GameStatePtr->GetTimeLimit());
	}

	return .0f;
}

FText SGameTimeAndScores::GetTimeText() const
{
	if (IsValidObject(GameStatePtr))
	{
		const FTimespan Timespan = FTimespan::FromSeconds(GameStatePtr->GetRemainingTime());
		const FString FormatedTime = FString::Printf(TEXT("%02d%s%02d"), Timespan.GetMinutes(), Timespan.GetSeconds() % 2 == 0 ? TEXT(":") : TEXT(" "), Timespan.GetSeconds());
		return FText::FromString(FormatedTime);
	}

	return FText::FromString(TEXT("00:00"));
}

TOptional<float> SGameTimeAndScores::GetScorePercent(bool IsFriendly) const
{
	if (IsValidObject(GameStatePtr) && PlayerStatePtr.IsValid() && PlayerStatePtr->IsValidLowLevel())
	{
		const float TargetTeamScore = GameStatePtr->GetTeamScore(IsFriendly ? PlayerStatePtr->GetTeamNum() : !PlayerStatePtr->GetTeamNum());
		return TargetTeamScore / GameStatePtr->GetGoalScore();
	}

	return .0f;
}

FText SGameTimeAndScores::GetScoreText(bool IsFriendly) const
{
	if (IsValidObject(GameStatePtr) && PlayerStatePtr.IsValid() && PlayerStatePtr->IsValidLowLevel())
	{
		const float TargetTeamScore = GameStatePtr->GetTeamScore(IsFriendly ? PlayerStatePtr->GetTeamNum() : !PlayerStatePtr->GetTeamNum());
		return FText::AsNumber(TargetTeamScore, &FNumberFormattingOptions::DefaultNoGrouping());
	}

	return FText::FromString(TEXT("0"));
}

FSlateColor SGameTimeAndScores::GetProgressColor(bool IsFriendly) const
{	
	if (TeamColorsPtr.IsValid())
	{
		return IsFriendly ? TeamColorsPtr->GetFriendlyColor() : TeamColorsPtr->GetEnemyColor();
	}

	return FColor::White.ReinterpretAsLinear();
}
