// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "Utilities/SUsableCompoundWidget.h"
#include "Styles/BadConnectionWidgetStyle.h"

class SAnimatedBackground;
/**
 * 
 */
class SHOOTERGAME_API SBadConnection : public SUsableCompoundWidget
{
public:
	SLATE_BEGIN_ARGS(SBadConnection)
		: _Style(&FShooterStyle::Get().GetWidgetStyle<FBadConnectionStyle>("SBadConnectionStyle"))
	{
		_Visibility = EVisibility::HitTestInvisible;
	}
	SLATE_STYLE_ARGUMENT(FBadConnectionStyle, Style)
	SLATE_END_ARGS()

	void Construct(const FArguments& InArgs);

	void ToggleWidget(const bool InToggle) override;
	bool IsInInteractiveMode() const override;
	bool IsUniqueWidget() const override;

protected:

	const FBadConnectionStyle* Style;

	TSharedPtr<SAnimatedBackground> Anim_Back;

};
