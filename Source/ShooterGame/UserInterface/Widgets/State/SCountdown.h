// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "Widgets/SCompoundWidget.h"
#include "Styles/CountdownWidgetStyle.h"

/**
 * 
 */
class SHOOTERGAME_API SCountdown : public SCompoundWidget
{
public:
	SLATE_BEGIN_ARGS(SCountdown)
		: _Style(&FShooterStyle::Get().GetWidgetStyle<FCountdownStyle>("SCountdownStyle"))
		, _CountdownAt(5)
	{}
	SLATE_STYLE_ARGUMENT(FCountdownStyle, Style)
	SLATE_ATTRIBUTE(int32, RemainingTime)
	SLATE_ATTRIBUTE(int32, CountdownAt)
	SLATE_END_ARGS()

	void Construct(const FArguments& InArgs);

protected:

	const FCountdownStyle* Style;

	float CurrentNumberTime;
	int32 CurrentRemainingTime;

	TAttribute<int32> Attr_RemainingTime;
	TAttribute<int32> Attr_CountdownAt;

	FText GetCurrentNumberText() const;
	FSlateColor GetCurrentNumberColor() const;
	float GetRenderScale() const;

	void Tick(const FGeometry& AllottedGeometry, const double InCurrentTime, const float InDeltaTime) override;
};
