// Fill out your copyright notice in the Description page of Project Settings.

#include "ShooterGame.h"
#include "SWeaponBar.h"
#include "SlateOptMacros.h"
#include "ShooterCharacter.h"
#include "Inventory/ShooterWeapon.h"
#include "WeaponItemProperty.h"
#include "SRetainerWidget.h"

BEGIN_SLATE_FUNCTION_BUILD_OPTIMIZATION
void SWeaponBar::Construct(const FArguments& InArgs)
{
	Style = InArgs._Style;

	const auto StyleTocuh = &FShooterStyle::Get().GetWidgetStyle<FTouchInputButtonStyle>("STouchInputButton_Reload");

	STouchInputButton::Construct(STouchInputButton::FArguments()
		.Style(StyleTocuh)
		.Action("Reload")
		.ActivationMethod(EButtonActivationMethod::Click)
	);

	ChildSlot
	[
		SNew(SRetainerWidget)
		.Phase(0)
		.PhaseCount(10)
		.RenderOnPhase(true)
		.RenderOnInvalidation(false)
		[
			SNew(SHorizontalBox)
			+ SHorizontalBox::Slot()
			[
				SNew(SVerticalBox)
				+ SVerticalBox::Slot()
				[
					SNew(STextBlock)
					.TextStyle(&Style->HealthFont)
					.Text(this, &SWeaponBar::GetClipText)
					.Margin(FMargin(0, 0, 10, 0))
				]
				+ SVerticalBox::Slot().AutoHeight()
				[
					SNew(SProgressBar)
					.Style(&Style->HealthBar)
					.BarFillType(EProgressBarFillType::LeftToRight)
					.Percent(this, &SWeaponBar::GetClipPercent)
					.FillColorAndOpacity(this, &SWeaponBar::GetFillColor)
				]
			]
			+ SHorizontalBox::Slot().AutoWidth()
			[
				SNew(SImage)
				.Image(&StyleTocuh->IconNormal)
			]
		]
	];
}
END_SLATE_FUNCTION_BUILD_OPTIMIZATION

void SWeaponBar::InitCharacter(AShooterCharacter* InCharacter)
{
	if (auto Character = GetValidObject(InCharacter))
	{
		CharacterPtr = Character;
	}
}

TOptional<float> SWeaponBar::GetClipPercent() const
{
	if (const auto TargetWeapon = GetWeapon())
	{
		if (TargetWeapon->GetCurrentState() == EWeaponState::Reloading)
		{
			return GetReloadProgress();
		}

		return float(TargetWeapon->GetCurrentAmmoInClip()) / float(TargetWeapon->GetAmmoPerClip());
	}

	return .0f;
}

AShooterWeapon* SWeaponBar::GetWeapon() const
{
	if (auto Character = GetCharacter())
	{
		return Character->GetWeapon();
	}
	return nullptr;
}

AShooterCharacter* SWeaponBar::GetCharacter() const
{
	if (CharacterPtr.IsValid())
	{
		return GetValidObject(CharacterPtr.Get());
	}
	return nullptr;
}


FText SWeaponBar::GetClipText() const
{
	if (const auto TargetWeapon = GetWeapon())
	{
		//=============================================
		const auto Ammo = TargetWeapon->GetAmmo();
		const auto AmmoPerClip = TargetWeapon->GetAmmoPerClip();
		const auto CurrentAmmoInClip = TargetWeapon->GetCurrentAmmoInClip();

		//=============================================
		return FText::Format(FText::FromString(TEXT("{0}/{1}"))
			, FText::AsNumber(TargetWeapon->GetCurrentAmmoInClip(), &FNumberFormattingOptions::DefaultNoGrouping())
			, Ammo > AmmoPerClip ? FText::AsNumber(AmmoPerClip, &FNumberFormattingOptions::DefaultNoGrouping()) : FText::AsNumber(CurrentAmmoInClip, &FNumberFormattingOptions::DefaultNoGrouping())
		);
	}
	return FText::GetEmpty();
}

FSlateColor SWeaponBar::GetFillColor() const
{
	if (Style->RealoadCurve)
	{
		if (const auto TargetWeapon = GetWeapon())
		{
			if (TargetWeapon->GetCurrentState() == EWeaponState::Reloading)
			{
				return Style->RealoadCurve->GetLinearColorValue(GetReloadProgress());
			}
		}
	}

	return Style->HealthBar.FillImage.TintColor;
}

float SWeaponBar::GetReloadProgress() const
{
	if (const auto TargetWeapon = GetWeapon())
	{
		if (TargetWeapon->GetCurrentState() == EWeaponState::Reloading)
		{
			const auto TimeCurrent = FSlateApplication::Get().GetCurrentTime();
			const auto TimeStarted = TargetWeapon->StartedRealoadTime;
			const auto TimeDelay = TargetWeapon->GetWeaponConfiguration()->ReloadDuration;

			return (TimeCurrent - TimeStarted) / TimeDelay;
		}
	}

	return .0f;
}

int32 SWeaponBar::OnPaint(const FPaintArgs& Args,
	const FGeometry& AllottedGeometry,
	const FSlateRect& MyCullingRect,
	FSlateWindowElementList& OutDrawElements,
	int32 LayerId,
	const FWidgetStyle& InWidgetStyle,
	bool bParentEnabled) const
{
	return SCompoundWidget::OnPaint(Args, AllottedGeometry, MyCullingRect, OutDrawElements, LayerId, InWidgetStyle, bParentEnabled);
}
