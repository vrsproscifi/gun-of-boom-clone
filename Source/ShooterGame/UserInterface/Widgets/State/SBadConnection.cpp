// Fill out your copyright notice in the Description page of Project Settings.

#include "ShooterGame.h"
#include "SBadConnection.h"
#include "SlateOptMacros.h"
#include "Utilities/SAnimatedBackground.h"
#include "ScaleBox.h"

BEGIN_SLATE_FUNCTION_BUILD_OPTIMIZATION
void SBadConnection::Construct(const FArguments& InArgs)
{
	Style = InArgs._Style;

	ChildSlot.VAlign(VAlign_Top).HAlign(HAlign_Center).Padding(0, 50)
	[
		SAssignNew(Anim_Back, SAnimatedBackground)
		.InitAsHide(true)
		.IsEnabledBlur(false)
		.IsRelative(true)
		.ShowAnimation(EAnimBackAnimation::Color)
		.HideAnimation(EAnimBackAnimation::ZoomIn)
		.Duration(.2f)
		[
			SNew(SVerticalBox)
			+ SVerticalBox::Slot().AutoHeight().HAlign(HAlign_Center)
			[
				SNew(SBox)
				.WidthOverride(64)
				.HeightOverride(64)
				[
					SNew(SScaleBox)
					.Stretch(EStretch::ScaleToFit)
					[
						SNew(SImage).Image(&Style->Image)
					]
				]

				//SNew(STextBlock)
				//.Justification(ETextJustify::Center)
				//.Text(NSLOCTEXT("SBadConnection", "SBadConnection.Problem", "Bad Connection"))
				//.TextStyle(&Style->BigText)
			]
			+ SVerticalBox::Slot().AutoHeight().Padding(6)
			[
				SNew(STextBlock)
				.Justification(ETextJustify::Center)
				.Text(NSLOCTEXT("SBadConnection", "SBadConnection.Desc", "The connection is bad, please improve you internet!"))
				.TextStyle(&Style->NormalText)
			]
		]
	];
}
END_SLATE_FUNCTION_BUILD_OPTIMIZATION

void SBadConnection::ToggleWidget(const bool InToggle)
{
	SUsableCompoundWidget::ToggleWidget(InToggle);
	Anim_Back->ToggleWidget(InToggle);
}

bool SBadConnection::IsInInteractiveMode() const
{
	return Anim_Back->IsAnimAtEnd();
}

bool SBadConnection::IsUniqueWidget() const
{
	return false;
}
