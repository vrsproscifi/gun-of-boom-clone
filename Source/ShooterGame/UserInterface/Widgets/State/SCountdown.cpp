// Fill out your copyright notice in the Description page of Project Settings.

#include "ShooterGame.h"
#include "SCountdown.h"
#include "SlateOptMacros.h"

BEGIN_SLATE_FUNCTION_BUILD_OPTIMIZATION
void SCountdown::Construct(const FArguments& InArgs)
{
	Style = InArgs._Style;
	Attr_RemainingTime = InArgs._RemainingTime;
	Attr_CountdownAt = InArgs._CountdownAt;

	ChildSlot.HAlign(HAlign_Center).VAlign(VAlign_Center)
	[
		SNew(SFxWidget)
		.RenderScaleOrigin(FVector2D(.5f, .5f))
		.RenderScale(this, &SCountdown::GetRenderScale)
		[
			SNew(STextBlock)
			.TextStyle(&Style->NumberFont)
			.ColorAndOpacity(this, &SCountdown::GetCurrentNumberColor)
			.Text(this, &SCountdown::GetCurrentNumberText)
		]
	];
}
END_SLATE_FUNCTION_BUILD_OPTIMIZATION

FText SCountdown::GetCurrentNumberText() const
{
	return FText::AsNumber(CurrentRemainingTime, &FNumberFormattingOptions::DefaultNoGrouping());
}

FSlateColor SCountdown::GetCurrentNumberColor() const
{
	if (Style->CurveColor)
	{
		return Style->CurveColor->GetLinearColorValue(CurrentNumberTime);
	}

	return FColor::White.ReinterpretAsLinear();
}

float SCountdown::GetRenderScale() const
{
	if (Style->CurveScale)
	{
		return Style->CurveScale->GetFloatValue(CurrentNumberTime);
	}

	return 1.0f - CurrentNumberTime;
}

void SCountdown::Tick(const FGeometry& AllottedGeometry, const double InCurrentTime, const float InDeltaTime)
{
	SCompoundWidget::Tick(AllottedGeometry, InCurrentTime, InDeltaTime);

	auto FromAttr_RemainingTime = Attr_RemainingTime.Get(0);
	auto FromAttr_CountdownAt = Attr_CountdownAt.Get(0);

	if (FromAttr_RemainingTime <= FromAttr_CountdownAt && FromAttr_RemainingTime != CurrentRemainingTime)
	{
		CurrentRemainingTime = FromAttr_RemainingTime;
		CurrentNumberTime = .0f;
	}

	CurrentNumberTime += InDeltaTime;
}
