// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "Styles/FightResultsWidgetStyle.h"
#include "Utilities/SUsableCompoundWidget.h"

class AShooterGameState;
class SAnimatedBackground;
class AShooterPlayerState;
/**
 * 
 */
class SHOOTERGAME_API SPlayersTable : public SUsableCompoundWidget
{
public:
	SLATE_BEGIN_ARGS(SPlayersTable)
		: _Style(&FShooterStyle::Get().GetWidgetStyle<FFightResultsStyle>("SFightResultsStyle"))
	{
		_Visibility = EVisibility::SelfHitTestInvisible;
	}
	SLATE_STYLE_ARGUMENT(FFightResultsStyle, Style)
	SLATE_END_ARGS()

	void Construct(const FArguments& InArgs);

	void ToggleWidget(const bool InToggle) override;
	void SetGameState(AShooterGameState* InGameState, AShooterPlayerState* InLocalState);

protected:

	const FFightResultsStyle* Style;

	const AShooterPlayerState* GetLocalPlayerState() const;
	const AShooterGameState* GetGameState() const;

	TWeakObjectPtr<AShooterPlayerState> LocalPlayerState;
	TWeakObjectPtr<AShooterGameState> GameState;
	TArray<TWeakObjectPtr<AShooterPlayerState>> PlayersList_Friendly, PlayersList_Enemy;
	TSharedPtr<SListView<TWeakObjectPtr<AShooterPlayerState>>> PlayersTable_Friendly, PlayersTable_Enemy;
	TSharedPtr<STextBlock> Widget_WinStatus;
	TSharedPtr<SAnimatedBackground> Back_Animation;
};
