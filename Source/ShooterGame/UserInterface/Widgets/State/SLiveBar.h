// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "Widgets/SCompoundWidget.h"
#include "Styles/LiveBarWidgetStyle.h"

class AShooterCharacter;

class SHOOTERGAME_API SLiveBar : public SCompoundWidget
{
public:
	SLATE_BEGIN_ARGS(SLiveBar)
		: _Style(&FShooterStyle::Get().GetWidgetStyle<FLiveBarStyle>("SLiveBarStyle"))
	{}
	SLATE_STYLE_ARGUMENT(FLiveBarStyle, Style)
	SLATE_END_ARGS()

	void Construct(const FArguments& InArgs);

	void InitCharacter(AShooterCharacter* InCharacter);

	void OverrideHealth(const FVector2D& InHealth);
	void OverrideArmour(const FVector2D& InArmour);

protected:

	const FLiveBarStyle* Style;

	bool bOverrideHealth;
	bool bOverrideArmour;

	FVector2D vOverrideHealth;
	FVector2D vOverrideArmour;

	TWeakObjectPtr<AShooterCharacter> CharacterPtr;
	TOptional<float> GetHealthPercent() const;
	TOptional<float> GetArmorPercent() const;
	FText GetHealthText() const;
	FText GetArmorText() const;
};
