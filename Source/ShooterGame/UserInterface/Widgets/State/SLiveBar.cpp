// Fill out your copyright notice in the Description page of Project Settings.

#include "ShooterGame.h"
#include "SLiveBar.h"
#include "SlateOptMacros.h"
#include "ShooterCharacter.h"
#include "Inventory/ShooterArmour.h"
#include "SRetainerWidget.h"

BEGIN_SLATE_FUNCTION_BUILD_OPTIMIZATION
void SLiveBar::Construct(const FArguments& InArgs)
{
	Style = InArgs._Style;

	ChildSlot
	[
		SNew(SRetainerWidget)
		.Phase(0)
		.PhaseCount(10)
		.RenderOnPhase(true)
		.RenderOnInvalidation(false)
		[
			SNew(SVerticalBox)
			+ SVerticalBox::Slot()
			[
				SNew(SHorizontalBox)
				+ SHorizontalBox::Slot().HAlign(HAlign_Left)
				[
					SNew(STextBlock)
					.TextStyle(&Style->HealthFont)
					.Text(this, &SLiveBar::GetHealthText)
					.Margin(FMargin(0, 0, 10, 0))
				]
				+ SHorizontalBox::Slot().HAlign(HAlign_Right)
				[
					SNew(STextBlock)
					.TextStyle(&Style->ArmorFont)
					.Text(this, &SLiveBar::GetArmorText)
					.Margin(FMargin(0, 0, 10, 0))
				]
			]
			+ SVerticalBox::Slot().AutoHeight()
			[
				SNew(SProgressBar)
				.Style(&Style->HealthBar)
				.BarFillType(EProgressBarFillType::LeftToRight)
				.Percent(this, &SLiveBar::GetHealthPercent)
			]
			+ SVerticalBox::Slot().AutoHeight().Padding(0, 4, 0, 0)
			[
				SNew(SProgressBar)
				.Style(&Style->ArmorBar)
				.BarFillType(EProgressBarFillType::LeftToRight)
				.Percent(this, &SLiveBar::GetArmorPercent)
			]
		]
	];

	bOverrideHealth = false;
	bOverrideArmour = false;
}
END_SLATE_FUNCTION_BUILD_OPTIMIZATION

void SLiveBar::InitCharacter(AShooterCharacter* InCharacter)
{
	if (InCharacter && InCharacter->IsValidLowLevel())
	{
		CharacterPtr = InCharacter;
	}
}

void SLiveBar::OverrideHealth(const FVector2D& InHealth)
{
	if (InHealth == FVector2D::ZeroVector)
	{
		bOverrideHealth = false;
	}
	else
	{
		vOverrideHealth = InHealth;
		bOverrideHealth = true;
	}
}

void SLiveBar::OverrideArmour(const FVector2D& InArmour)
{
	if (InArmour == FVector2D::ZeroVector)
	{
		bOverrideArmour = false;
	}
	else
	{
		vOverrideArmour = InArmour;
		bOverrideArmour = true;
	}
}

TOptional<float> SLiveBar::GetHealthPercent() const
{
	if (IsValidObject(CharacterPtr))
	{
		return CharacterPtr->Health.X / CharacterPtr->Health.Y;
	}
	else if (bOverrideHealth)
	{
		return vOverrideHealth.X / vOverrideHealth.Y;
	}

	return .0f;
}

TOptional<float> SLiveBar::GetArmorPercent() const
{
	if (IsValidObject(CharacterPtr))
	{
		return CharacterPtr->GetCurrentArmour() / CharacterPtr->GetTotalArmour();
	}
	else if (bOverrideArmour)
	{
		return vOverrideArmour.X / vOverrideArmour.Y;
	}

	return .0f;
}

FText SLiveBar::GetHealthText() const
{
	if (IsValidObject(CharacterPtr))
	{
		return FText::AsNumber(CharacterPtr->Health.X, &FNumberFormattingOptions::DefaultNoGrouping());
	}
	else if (bOverrideHealth)
	{
		return FText::AsNumber(vOverrideHealth.X, &FNumberFormattingOptions::DefaultNoGrouping());
	}

	return FText::GetEmpty();
}

FText SLiveBar::GetArmorText() const
{
	if (IsValidObject(CharacterPtr))
	{
		return FText::AsNumber(CharacterPtr->GetCurrentArmour(), &FNumberFormattingOptions::DefaultNoGrouping());
	}
	else if (bOverrideArmour)
	{
		return FText::AsNumber(vOverrideArmour.X, &FNumberFormattingOptions::DefaultNoGrouping());
	}

	return FText::GetEmpty();
}
