// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "Input/STouchInputButton.h"
#include "Styles/LiveBarWidgetStyle.h"

class AShooterWeapon;
class AShooterCharacter;
/**
 * 
 */
class SHOOTERGAME_API SWeaponBar : public STouchInputButton
{
public:
	SLATE_BEGIN_ARGS(SWeaponBar)
		: _Style(&FShooterStyle::Get().GetWidgetStyle<FLiveBarStyle>("SWeaponBarStyle"))
	{}
	SLATE_STYLE_ARGUMENT(FLiveBarStyle, Style)
	SLATE_END_ARGS()

	void Construct(const FArguments& InArgs);
	void InitCharacter(AShooterCharacter* InCharacter);

protected:

	int32 OnPaint(const FPaintArgs& Args, const FGeometry& AllottedGeometry, const FSlateRect& MyCullingRect, FSlateWindowElementList& OutDrawElements, int32 LayerId, const FWidgetStyle& InWidgetStyle, bool bParentEnabled) const override;

	const FLiveBarStyle* Style;

	TWeakObjectPtr<AShooterCharacter> CharacterPtr;
	TOptional<float> GetClipPercent() const;
	FText GetClipText() const;
	FSlateColor GetFillColor() const;

	AShooterWeapon* GetWeapon() const;
	AShooterCharacter* GetCharacter() const;

	float GetReloadProgress() const;
};
