// Fill out your copyright notice in the Description page of Project Settings.

#include "ShooterGame.h"
#include "SNameAndHealth.h"
#include "SlateOptMacros.h"
#include "ShooterCharacter.h"
#include "TeamColorsData.h"
#include "ShooterGameSingleton.h"
#include "GameState/ShooterGameState.h"
#include "ShooterPlayerController.h"

BEGIN_SLATE_FUNCTION_BUILD_OPTIMIZATION
void SNameAndHealth::Construct(const FArguments& InArgs, const TWeakObjectPtr<AShooterCharacter>& InCharacter)
{
	MyCharacter = InCharacter;
	Style = InArgs._Style;

	ChildSlot
	[
		SNew(SDPIScaler)
		.DPIScale(this, &SNameAndHealth::GetScaleByDistance)
		[
			SNew(SBox).MinDesiredWidth(160)
			.Visibility(this, &SNameAndHealth::GetPlayerNameVisibility)
			[
				SNew(SVerticalBox)
				+ SVerticalBox::Slot().AutoHeight()
				[
					SNew(STextBlock)
					.ColorAndOpacity(this, &SNameAndHealth::GetPlayerNameColor)
					.Text(this, &SNameAndHealth::GetPlayerNameText)
					.Justification(ETextJustify::Center)
					.TextStyle(&Style->Name)
				]
				+ SVerticalBox::Slot().AutoHeight().Padding(0, 2).HAlign(HAlign_Center)
				[
					SNew(SBox).WidthOverride(120)
					[
						SNew(SProgressBar)
						.BarFillType(EProgressBarFillType::LeftToRight)
						.Style(&Style->Health)
						.Percent(this, &SNameAndHealth::GetPlayerHealthPercent)
						.FillColorAndOpacity(this, &SNameAndHealth::GetPlayerNameColor)
					]
				]
				+ SVerticalBox::Slot().AutoHeight().Padding(0, 2).HAlign(HAlign_Center)
				[
					SNew(SBox).WidthOverride(120)
					[
						SNew(SProgressBar)
						.BarFillType(EProgressBarFillType::LeftToRight)
						.Style(&Style->Armour)
						.Percent(this, &SNameAndHealth::GetPlayerArmourPercent)
					]
				]
			]
		]
	];

	auto Assets = UShooterGameSingleton::Get()->GetDataAssets<UTeamColorsData>();
	if (Assets.Num())
	{
		TeamColorsPtr = Assets[0];
	}
}
END_SLATE_FUNCTION_BUILD_OPTIMIZATION

FText SNameAndHealth::GetPlayerNameText() const
{
	if (IsValidObject(MyCharacter) && MyCharacter->GetPlayerState() && MyCharacter->GetPlayerState()->IsValidLowLevel())
	{
		return FText::FromString(MyCharacter->GetPlayerState()->GetPlayerName());
	}

	return FText::FromString("nullptr;");
}

TOptional<float> SNameAndHealth::GetPlayerHealthPercent() const
{
	
	if (IsValidObject(MyCharacter))
	{
		return MyCharacter->Health.X / MyCharacter->Health.Y;
	}

	return .0f;
}

TOptional<float> SNameAndHealth::GetPlayerArmourPercent() const
{
	if (IsValidObject(MyCharacter))
	{
		return MyCharacter->GetCurrentArmour() / MyCharacter->GetTotalArmour();
	}

	return .0f;
}

FSlateColor SNameAndHealth::GetPlayerNameColor() const
{
	if (TeamColorsPtr.IsValid() && IsValidObject(MyCharacter))
	{
		const APlayerController* LocalCtrl = MyCharacter->GetWorld() ? MyCharacter->GetWorld()->GetFirstPlayerController() : nullptr;
		const AShooterGameState* LocalGameState = MyCharacter->GetWorld() ? MyCharacter->GetWorld()->GetGameState<AShooterGameState>() : nullptr;

		if (LocalGameState && LocalGameState->IsValidLowLevel())
		{
			return LocalGameState->HasSameTeam(LocalCtrl, MyCharacter.Get()) ? TeamColorsPtr->GetFriendlyColor() : TeamColorsPtr->GetEnemyColor();
		}
	}

	return FColor::White.ReinterpretAsLinear();
}

EVisibility SNameAndHealth::GetPlayerNameVisibility() const
{
	if (TeamColorsPtr.IsValid() && IsValidObject(MyCharacter))
	{
		const APlayerController* LocalCtrl = MyCharacter->GetWorld() ? MyCharacter->GetWorld()->GetFirstPlayerController() : nullptr;
		const AShooterGameState* LocalGameState = MyCharacter->GetWorld() ? MyCharacter->GetWorld()->GetGameState<AShooterGameState>() : nullptr;

		if (LocalGameState && LocalGameState->IsValidLowLevel())
		{
			const AShooterCharacter* LocalCharacter = LocalCtrl ? GetValidObjectAs<AShooterCharacter>(LocalCtrl->GetCharacter()) : nullptr;
			if (LocalCharacter && LocalCharacter->IsValidLowLevel())
			{
				const bool bHasSomeTeam = LocalGameState->HasSameTeam(LocalCtrl, MyCharacter.Get());
				const bool bHasLastHit = LocalCharacter->GetGiveHitInfo().PawnDamaged == MyCharacter && LocalCharacter->GetLastGiveHitInfoTime() + 3.0f >= FSlateApplication::Get().GetCurrentTime() && MyCharacter->IsAlive();

				return (bHasSomeTeam || bHasLastHit) ? EVisibility::Visible : EVisibility::Collapsed;
			}
			else if (auto MyLocalCtrl = GetValidObjectAs<AShooterPlayerController>(LocalCtrl))
			{
				if (MyLocalCtrl->MyLastKiller == MyCharacter.Get())
				{
					return EVisibility::Visible;
				}
			}
		}
	}

	return EVisibility::Collapsed;
}

float SNameAndHealth::GetScaleByDistance() const
{
	if (IsValidObject(MyCharacter) && Style->CurveDistance && Style->CurveDistance->IsValidLowLevel())
	{
		const APlayerController* LocalCtrl = MyCharacter->GetWorld() ? MyCharacter->GetWorld()->GetFirstPlayerController() : nullptr;
		const AShooterGameState* LocalGameState = MyCharacter->GetWorld() ? MyCharacter->GetWorld()->GetGameState<AShooterGameState>() : nullptr;

		if (LocalGameState && LocalGameState->IsValidLowLevel())
		{
			const AShooterCharacter* LocalCharacter = LocalCtrl ? GetValidObjectAs<AShooterCharacter>(LocalCtrl->GetCharacter()) : nullptr;
			if (LocalCharacter && LocalCharacter->IsValidLowLevel())
			{
				// 	float GetFloatValue(float InTime) const;
				return Style->CurveDistance->GetFloatValue(FVector::Distance(MyCharacter->GetActorLocation(), LocalCharacter->GetActorLocation()) / 100.0f);
			}
		}
	}

	return .0f;
}
