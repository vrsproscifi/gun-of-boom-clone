// Fill out your copyright notice in the Description page of Project Settings.

#include "ShooterGame.h"
#include "SPlayersTable.h"
#include "SlateOptMacros.h"
#include "Utilities/SAnimatedBackground.h"
#include "Notify/SFightResults.h"
#include "Localization/TableBaseStrings.h"
#include "GameState/ShooterPlayerState.h"
#include "GameState/ShooterGameState.h"
#include "SRetainerWidget.h"
#include "GameSingletonExtensions.h"
#include "IdentityComponent.h"

class SHOOTERGAME_API SPlayersTable_Item : public SMultiColumnTableRow<TWeakObjectPtr<AShooterPlayerState>>
{
public:
	SLATE_BEGIN_ARGS(SPlayersTable_Item)
		: _Style(&FShooterStyle::Get().GetWidgetStyle<FFightResultsStyle>("SFightResultsStyle"))
	{}
	SLATE_STYLE_ARGUMENT(FFightResultsStyle, Style)
	SLATE_END_ARGS()

	BEGIN_SLATE_FUNCTION_BUILD_OPTIMIZATION
	void Construct(const FArguments& InArgs, const TSharedRef<STableViewBase>& OwnerTableView, const TWeakObjectPtr<AShooterPlayerState>& InItem)
	{
		Style = InArgs._Style;
		Instance = InItem;
		bool IsLocal = false;
		
		if (IsValidObject(Instance) && Instance->GetIdentityComponent())
		{
			IsLocal = FGameSingletonExtension::GetLastLocalPlayerId() == Instance->GetIdentityComponent()->GetPlayerInfo().PlayerId;
		}

		auto SuperArgs = FSuperRowType::FArguments().Style(IsLocal ? &Style->RowTableLocal : &Style->RowTable).Padding(FMargin(2, 6));

		FSuperRowType::Construct(SuperArgs, OwnerTableView);
	}
	END_SLATE_FUNCTION_BUILD_OPTIMIZATION

protected:

	const FFightResultsStyle* Style;

	#define CreateWidgetForColumn(Instance, DisplayMethod, TargetMethod, DefaultValue, RowStyle ) \
	SNew(STextBlock).Text_Lambda([&, Instance]() \
	{ \
		if(IsValidObject(Instance)) \
		{ \
			return FText::DisplayMethod(Instance->TargetMethod()); \
		} \
		else \
		{ \
			return FText::DisplayMethod(DefaultValue); \
		} \
	}).TextStyle(RowStyle);

	TSharedRef<SWidget> GenerateWidgetForColumn(const FName& InColumnName) override
	{
		auto LocalInstance = GetInstance();
		if (LocalInstance)
		{
			if (SFightResults::Column_Name.IsEqual(InColumnName))
			{
				return CreateWidgetForColumn(LocalInstance, FromString, GetPlayerName, " - ", &Style->RowName);
			}
			else if (SFightResults::Column_Kills.IsEqual(InColumnName))
			{
				return CreateWidgetForColumn(LocalInstance, AsNumber, GetKills, -1, &Style->RowNumber);
			}
			else if (SFightResults::Column_Deaths.IsEqual(InColumnName))
			{
				return CreateWidgetForColumn(LocalInstance, AsNumber, GetDeaths, -1, &Style->RowNumber);
			}
			else if (SFightResults::Column_Score.IsEqual(InColumnName))
			{
				return CreateWidgetForColumn(LocalInstance, AsNumber, GetScore, -1, &Style->RowNumber);
			}
		}

		return SNullWidget::NullWidget;
	}

	const AShooterPlayerState* GetInstance() const
	{
		return GetValidObject(Instance);
	}

	TWeakObjectPtr<AShooterPlayerState> Instance;
};

BEGIN_SLATE_FUNCTION_BUILD_OPTIMIZATION
void SPlayersTable::Construct(const FArguments& InArgs)
{
	Style = InArgs._Style;

	ChildSlot
	[
		SAssignNew(Back_Animation, SAnimatedBackground)
		.Visibility(EVisibility::Visible)
		.IsEnabledBlur(true)
		.Duration(.2f)
		.ShowAnimation(EAnimBackAnimation::UpFade)
		.HideAnimation(EAnimBackAnimation::DownFade)
		.InitAsHide(true)
		[
			SNew(SRetainerWidget)
			.Phase(0)
			.PhaseCount(1)
			.RenderOnPhase(true)
			.RenderOnInvalidation(false)
			[
				SNew(SVerticalBox)
				+ SVerticalBox::Slot().AutoHeight().VAlign(VAlign_Center).HAlign(HAlign_Center)
				[
					SNew(SBox).HeightOverride(100).VAlign(VAlign_Center)
					[
						SNew(STextBlock)
						.Text(NSLOCTEXT("SPlayersTable", "SPlayersTable.Title", "Statistic"))
						.TextStyle(&FShooterStyle::Get().GetWidgetStyle<FTextBlockStyle>("Default_HeaderText"))
					]
				]
				+ SVerticalBox::Slot()
				[
					SNew(SHorizontalBox)
					+ SHorizontalBox::Slot().Padding(80, 20, 20, 80)
					[
						SAssignNew(PlayersTable_Friendly, SListView<TWeakObjectPtr<AShooterPlayerState>>)
						.ListItemsSource(&PlayersList_Friendly)
						.ScrollbarVisibility(EVisibility::Collapsed)
						.AllowOverscroll(EAllowOverscroll::Yes)
						.SelectionMode(ESelectionMode::None)
						.HeaderRow
						(
							SNew(SHeaderRow).Style(&Style->RowHeader)
							+ SHeaderRow::Column(SFightResults::Column_Name).FillWidth(SFightResults::FillColumn_Name)[SNew(STextBlock).TextStyle(&Style->RowHeaderText).Text(FText::FromString("Name"))]
							+ SHeaderRow::Column(SFightResults::Column_Kills).FillWidth(SFightResults::FillColumn_Kills)[SNew(STextBlock).TextStyle(&Style->RowHeaderText).Text(FText::FromString("K"))]
							+ SHeaderRow::Column(SFightResults::Column_Deaths).FillWidth(SFightResults::FillColumn_Deaths)[SNew(STextBlock).TextStyle(&Style->RowHeaderText).Text(FText::FromString("D"))]
							+ SHeaderRow::Column(SFightResults::Column_Score).FillWidth(SFightResults::FillColumn_Score)[SNew(STextBlock).TextStyle(&Style->RowHeaderText).Text(FText::FromString("S"))]
						)
						.OnGenerateRow_Lambda([&](TWeakObjectPtr<AShooterPlayerState> InItem, const TSharedRef<STableViewBase>& OwnerTableView) -> TSharedRef<ITableRow>
						{
							return SNew(SPlayersTable_Item, OwnerTableView, InItem);
						})
					]
					+ SHorizontalBox::Slot().Padding(20, 20, 80, 80)
					[
						SAssignNew(PlayersTable_Enemy, SListView<TWeakObjectPtr<AShooterPlayerState>>)
						.ListItemsSource(&PlayersList_Enemy)
						.ScrollbarVisibility(EVisibility::Collapsed)
						.AllowOverscroll(EAllowOverscroll::Yes)
						.SelectionMode(ESelectionMode::None)
						.HeaderRow
						(
							SNew(SHeaderRow).Style(&Style->RowHeader)
							+ SHeaderRow::Column(SFightResults::Column_Name).FillWidth(SFightResults::FillColumn_Name)[SNew(STextBlock).TextStyle(&Style->RowHeaderText).Text(FText::FromString("Name"))]
							+ SHeaderRow::Column(SFightResults::Column_Kills).FillWidth(SFightResults::FillColumn_Kills)[SNew(STextBlock).TextStyle(&Style->RowHeaderText).Text(FText::FromString("K"))]
							+ SHeaderRow::Column(SFightResults::Column_Deaths).FillWidth(SFightResults::FillColumn_Deaths)[SNew(STextBlock).TextStyle(&Style->RowHeaderText).Text(FText::FromString("D"))]
							+ SHeaderRow::Column(SFightResults::Column_Score).FillWidth(SFightResults::FillColumn_Score)[SNew(STextBlock).TextStyle(&Style->RowHeaderText).Text(FText::FromString("S"))]
						)
						.OnGenerateRow_Lambda([&](TWeakObjectPtr<AShooterPlayerState> InItem, const TSharedRef<STableViewBase>& OwnerTableView) -> TSharedRef<ITableRow>
						{
							return SNew(SPlayersTable_Item, OwnerTableView, InItem);
						})
					]
				]
				+ SVerticalBox::Slot().AutoHeight().Padding(20, 0, 20, 20).HAlign(HAlign_Center).VAlign(VAlign_Center)
				[
					SNew(SButton)
					.ContentPadding(FMargin(20))
					.TextStyle(&Style->TextContinue)
					.ButtonStyle(&FShooterStyle::Get().GetWidgetStyle<FButtonStyle>("Default_ActionButton"))
					.HAlign(HAlign_Center)
					.VAlign(VAlign_Center)
					.Text(FTableBaseStrings::GetBaseText(EBaseStrings::Continue))
					.OnClicked_Lambda([&]()
					{
						ToggleWidget(false);
						return FReply::Handled();
					})
				]
			]
		]
	];

	RegisterActiveTimer(1.0f, FWidgetActiveTimerDelegate::CreateLambda([&] (double InCurrentTime, float InDeltaTime) -> EActiveTimerReturnType
	{
		auto LocalGS = GetGameState();
		auto LocalPS = GetLocalPlayerState();

		if (LocalGS && LocalPS)
		{
			PlayersList_Friendly.Empty();
			PlayersList_Enemy.Empty();

			for (auto TargetPS : LocalGS->PlayerArray)
			{
				auto CastedPS = GetValidObjectAs<AShooterPlayerState>(TargetPS);
				if (CastedPS)
				{
					if (GameState->HasSameTeam(CastedPS, LocalPS))
					{
						PlayersList_Friendly.Add(CastedPS);
					}
					else
					{
						PlayersList_Enemy.Add(CastedPS);
					}
				}
			}

			auto SortLambda = [](const TWeakObjectPtr<AShooterPlayerState>& A, const TWeakObjectPtr<AShooterPlayerState>& B)
			{
				return IsValidObject(A) && IsValidObject(B) && A->GetScore() > B->GetScore();
			};

			PlayersList_Friendly.Sort(SortLambda);
			PlayersList_Enemy.Sort(SortLambda);

			PlayersTable_Friendly->RequestListRefresh();
			PlayersTable_Enemy->RequestListRefresh();
		}

		return EActiveTimerReturnType::Continue;
	}));
}
END_SLATE_FUNCTION_BUILD_OPTIMIZATION

const AShooterPlayerState* SPlayersTable::GetLocalPlayerState() const
{
	return GetValidObject(LocalPlayerState);
}

const AShooterGameState* SPlayersTable::GetGameState() const
{
	return GetValidObject(GameState);
}

void SPlayersTable::ToggleWidget(const bool InToggle)
{
	SUsableCompoundWidget::ToggleWidget(InToggle);
	Back_Animation->ToggleWidget(InToggle);
}

void SPlayersTable::SetGameState(AShooterGameState* InGameState, AShooterPlayerState* InLocalState)
{
	GameState = InGameState;
	LocalPlayerState = InLocalState;
}
