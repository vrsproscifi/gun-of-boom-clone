// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "Widgets/SCompoundWidget.h"
#include "Styles/GameTimeAndScoresWidgetStyle.h"

class UTeamColorsData;
class AShooterPlayerState;
class AShooterGameState;
/**
 * 
 */
class SHOOTERGAME_API SGameTimeAndScores : public SCompoundWidget
{
public:
	SLATE_BEGIN_ARGS(SGameTimeAndScores)
		: _Style(&FShooterStyle::Get().GetWidgetStyle<FGameTimeAndScoresStyle>("SGameTimeAndScoresStyle"))
	{
		_Visibility = EVisibility::SelfHitTestInvisible;
	}
	SLATE_STYLE_ARGUMENT(FGameTimeAndScoresStyle, Style)
	SLATE_ARGUMENT(TWeakObjectPtr<AShooterGameState>, GameState)
	SLATE_ARGUMENT(TWeakObjectPtr<AShooterPlayerState>, PlayerState)
	SLATE_END_ARGS()

	void Construct(const FArguments& InArgs);

	void SetGameState(TWeakObjectPtr<AShooterGameState> InGameState);

protected:

	const FGameTimeAndScoresStyle* Style;

	TWeakObjectPtr<AShooterGameState> GameStatePtr;
	TWeakObjectPtr<AShooterPlayerState> PlayerStatePtr;
	TWeakObjectPtr<UTeamColorsData> TeamColorsPtr;

	TOptional<float> GetTimePercent() const;
	FText GetTimeText() const;

	TOptional<float> GetScorePercent(bool IsFriendly) const;
	FText GetScoreText(bool IsFriendly) const;
	FSlateColor GetProgressColor(bool IsFriendly) const;

};
