// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "GameFramework/HUD.h"
#include "BaseHUD.generated.h"

class UHUDFontsData;
/**
 * 
 */
UCLASS()
class SHOOTERGAME_API ABaseHUD : public AHUD
{
	GENERATED_BODY()
	
public:

	ABaseHUD();

	virtual void DrawHUD() override;
	virtual void DrawDebugInformation();

	bool CorrectProject(const FVector& Location, FVector2D& OutScreen);

protected:	
	
	UPROPERTY(EditDefaultsOnly, Category = Fonts)
	UHUDFontsData* FontsData;
};
