#include "ShooterGame.h"
#include "ShooterAid.h"
#include "ShooterCharacter.h"

#include "Item/Aid/AidItemEntity.h"
#include "Player/AidPlayerItem.h"

UShooterAid::UShooterAid(const FObjectInitializer& ObjectInitializer)
	: Super(ObjectInitializer)
{
	ProcessPSC = ObjectInitializer.CreateDefaultSubobject<UParticleSystemComponent>(this, TEXT("ProcessPSC"));
	ProcessPSC->SetupAttachment(GetAttachmentRoot());
	ProcessPSC->bAutoActivate = false;
	ProcessPSC->bAutoDestroy = false;

	PrimaryComponentTick.bCanEverTick = true;
	SetComponentTickEnabled(true);
}

void UShooterAid::StopTimer()
{
	Super::StopTimer();
}

void UShooterAid::OnRep_Activated()
{
	Super::OnRep_Activated();

	if (ProcessPSC && ProcessPSC->IsValidLowLevel())
	{
		GEngine->AddOnScreenDebugMessage(32, 20.0f, FColor::White, FString::Printf(TEXT("UShooterAid::OnRep_Activated >> IsActivated: %d"), IsActivated()));
		ProcessPSC->SetActive(IsActivated(), true);
	}
}


void UShooterAid::OnProcess()
{

}


bool UShooterAid::IsAllowExecuteProcess() const
{
	return IsActivated() == false && GetAmount() > 0;
}

bool UShooterAid::IsActivated() const
{
	return IsValidTimerHandle() || Super::IsActivated();
}

bool UShooterAid::IsAllowStartProcess() const
{
	return GetInstance() && AnyAmount() && IsActivated() == false;
}

bool UShooterAid::StopProcess()
{
	SetProcessState(false);
	StopTimer();
	return true;
}

float UShooterAid::GetExecuteInterval() const
{
	if (auto MyInstance = GetInstance())
	{
		return MyInstance->GetItemProperty<FAidItemProperty>()->ActivationTime;
	}

	return Super::GetExecuteInterval();
}

bool UShooterAid::StartProcess()
{
	if (IsAllowStartProcess())
	{
		if (const auto MyProperty = GetAidProperty())
		{
			if (FMath::IsNearlyZero(MyProperty->ActivationDelay))
			{
				CurrentTime = .0f;
				UseItem();
				SetProcessState(true);
				return true;
			}
			else if (Handle_ActivationDelay.IsValid() == false)
			{
				GetWorld()->GetTimerManager().SetTimer(Handle_ActivationDelay, FTimerDelegate::CreateLambda([&]()
				{
					CurrentTime = .0f;
					UseItem();
					SetProcessState(true);
				}), MyProperty->ActivationDelay, false);
				return true;
			}
		}
	}
	return false;
}

void UShooterAid::OnRep_Instance()
{
	Super::OnRep_Instance();
	if (const auto MyProperty = GetAidProperty())
	{
		ProcessPSC->SetTemplate(MyProperty->ActiveFX);
	}
}

void UShooterAid::TickComponent(float DeltaTime, ELevelTick TickType, FActorComponentTickFunction* ThisTickFunction)
{
	Super::TickComponent(DeltaTime, TickType, ThisTickFunction);

	if (IsActivated() && GetOwnerRole() == ROLE_Authority)
	{
		if (const auto MyProperty = GetAidProperty())
		{
			CurrentTime += DeltaTime;
			if (CurrentTime >= MyProperty->ActivationTime)
			{
				StopProcess();
			}

			if (auto MyPawn = GetCharacter<AShooterCharacter>())
			{
				const float GiveAmount = MyProperty->Amount * (DeltaTime / MyProperty->ActivationTime);
				MyPawn->GiveHealth(GiveAmount);
			}
		}
	}
}

void UShooterAid::InitializeInstance(UBasicPlayerItem* InInstance)
{
	//==================================================
	if (InInstance)
	{
		UE_LOG(LogInit, Error, TEXT("> [UShooterAid::InitializeInstance][%s][%d / amount: %d]"), *GetPlayerName(), InInstance->GetModelId(), InInstance->GetAmount());
	}

	//==================================================
	Super::InitializeInstance(InInstance);
	if (InInstance)
	{	
#if WITH_EDITOR
		SetAmount(10);
#else
		SetAmount(FMath::Clamp(InInstance->GetAmount(), 0, 5));
#endif
	}
}


const FAidItemProperty* UShooterAid::GetAidProperty() const
{
	return GetItemProperty<FAidItemProperty>();
}
