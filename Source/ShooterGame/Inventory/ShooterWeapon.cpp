// Copyright 1998-2015 Epic Games, Inc. All Rights Reserved.
//==========================================
#include "ShooterGame.h"
#include "ShooterWeapon.h"

//==========================================
#include "Particles/ParticleSystemComponent.h"
#include "Particles/ParticleSystem.h"
#include "Animation/AnimMontage.h"
#include "Animation/AnimInstance.h"
#include "Engine/ActorChannel.h"

//==========================================
#include "ShooterImpactEffect.h"

//==========================================
#include "Bots/ShooterBot.h"
#include "Item/GameItemType.h"

#include "Player/ShooterCharacter.h"
#include "Player/ShooterPlayerController.h"

#include "GameState/ShooterGameState.h"
#include "GameState/ShooterPlayerState.h"

//==========================================
#include "Item/Weapon/WeaponItemEntity.h"
#include "Player/BasicPlayerItem.h"
#include "WeaponPlayerItem.h"
#include "Bots/ShooterAIController.h"
#include "ArsenalComponent.h"
#include "Components/SSniperScope.h"

//==========================================
#include "Components/SkinnedMeshComponent.h"


AShooterWeapon::AShooterWeapon(const FObjectInitializer& ObjectInitializer) 
	: Super(ObjectInitializer)
{
	//PrimaryActorTick.bRunOnAnyThread = true;
	Mesh1P = ObjectInitializer.CreateDefaultSubobject<USkeletalMeshComponent>(this, TEXT("WeaponMesh1P"));
	Mesh1P->SetGenerateOverlapEvents(false);
	Mesh1P->VisibilityBasedAnimTickOption = EVisibilityBasedAnimTickOption::OnlyTickPoseWhenRendered;
	

	Mesh1P->bReceivesDecals = false;
	Mesh1P->SetCollisionObjectType(ECC_WorldDynamic);
	Mesh1P->SetCollisionEnabled(ECollisionEnabled::NoCollision);
	Mesh1P->SetCollisionResponseToAllChannels(ECR_Ignore);
	Mesh1P->bEnableUpdateRateOptimizations = true;
	Mesh1P->bUseAttachParentBound = true;
	//Mesh1P->bDeferMovementFromSceneQueries = true;
	Mesh1P->bOnlyOwnerSee = true;
	Mesh1P->bOverrideMinLod = true;

	Mesh1P->bDisableClothSimulation = true;
	Mesh1P->CastShadow = false;
	Mesh1P->bAffectDynamicIndirectLighting = false;
	Mesh1P->bAffectDistanceFieldLighting = false;
	Mesh1P->bCastStaticShadow = false;
	Mesh1P->bCastDynamicShadow = false;
	Mesh1P->bCastVolumetricTranslucentShadow = false;
	Mesh1P->bSelfShadowOnly = true;
	Mesh1P->bReceiveMobileCSMShadows = false;
	Mesh1P->bDisableMorphTarget = true;
	Mesh1P->bPerBoneMotionBlur = false;

	RootComponent = Mesh1P;

	Mesh3P = ObjectInitializer.CreateDefaultSubobject<USkeletalMeshComponent>(this, TEXT("WeaponMesh3P"));
	Mesh3P->SetGenerateOverlapEvents(false);
	Mesh3P->VisibilityBasedAnimTickOption = EVisibilityBasedAnimTickOption::OnlyTickPoseWhenRendered;
	Mesh3P->bReceivesDecals = false;
	Mesh3P->SetCollisionObjectType(ECC_WorldDynamic);
	Mesh3P->SetCollisionEnabled(ECollisionEnabled::NoCollision);
	Mesh3P->SetCollisionResponseToAllChannels(ECR_Ignore);
	Mesh3P->SetCollisionResponseToChannel(COLLISION_WEAPON, ECR_Block);
	Mesh3P->SetCollisionResponseToChannel(ECC_Visibility, ECR_Block);
	Mesh3P->bEnableUpdateRateOptimizations = true;
	Mesh3P->bUseAttachParentBound = true;
	//Mesh3P->bDeferMovementFromSceneQueries = true;
	Mesh3P->bOwnerNoSee = true;
	Mesh3P->SetupAttachment(Mesh1P);
	Mesh3P->bOverrideMinLod = true;

	Mesh3P->bDisableClothSimulation = true;
	Mesh3P->CastShadow = false;
	Mesh3P->bAffectDynamicIndirectLighting = false;
	Mesh3P->bAffectDistanceFieldLighting = false;
	Mesh3P->bCastStaticShadow = false;
	Mesh3P->bCastDynamicShadow = false;
	Mesh3P->bCastVolumetricTranslucentShadow = false;
	Mesh3P->bSelfShadowOnly = true;
	Mesh3P->bReceiveMobileCSMShadows = false;
	Mesh3P->bDisableMorphTarget = true;
	Mesh3P->bPerBoneMotionBlur = false;

	bPlayingFireAnim = false;
	bIsEquipped = false;
	bWantsToFire = false;
	bPendingReload = false;
	bPendingEquip = false;
	CurrentState = EWeaponState::Idle;
	bIsCancelReload = false;

	CurrentAmmoInClip = 0;
	BurstCounter = 0;
	LastFireTime = 0.0f;

	PrimaryActorTick.bCanEverTick = true;
	PrimaryActorTick.TickGroup = TG_PrePhysics;
	SetRemoteRoleForBackwardsCompat(ROLE_SimulatedProxy);
	SetReplicates(true);
	bNetLoadOnClient = false;
	bNetUseOwnerRelevancy = true;
	NetUpdateFrequency = 20.0f;

	bAlwaysRelevant = true;
}

bool AShooterWeapon::IsAllowFireOnDistance(const float& InTargetDistance) const
{
	if(auto prop = GetWeaponConfiguration())
	{
		return prop->Range * 100.0f >= InTargetDistance;
	}
	return false;
}


void AShooterWeapon::OnRep_Instance()
{
	auto EntityInstance = GetInstanceEntity();
	if (EntityInstance)
	{
		float Damage = 0.0f;
		int32 AmmoPerClip = 0;
		if (auto WeaponConfiguration = GetWeaponConfiguration())
		{
			Damage = WeaponConfiguration->Damage;
			AmmoPerClip = WeaponConfiguration->AmmoPerClip;
		}

		if (auto instance = GetInstance())
		{
			auto WeaponProperty = GetWeaponProperty();
			//==========================================================
			auto modifications = GetWeaponProperty()->Modifications;
			auto modification = FBasicItemProperty::GetSafeModification(modifications, instance->GetLevel());
			
			//==========================================================
			if (modification)
			{
				SetOverridedDamage(modification->Damage);
			}
			else
			{
				SetOverridedDamage(Damage);
			}
		}
		else
		{
			SetOverridedDamage(Damage);
		}

		//==========================================================
		CurrentAmmoInClip = AmmoPerClip;
		TotalCountAmmo = AmmoPerClip * 10;

		Mesh1P->SetSkeletalMesh(EntityInstance->GetFPPMeshData().Mesh);
		Mesh1P->SetAnimInstanceClass(EntityInstance->GetFPPMeshData().Anim);
		Mesh1P->SetMinLOD(EntityInstance->GetFPPMeshData().MinLOD);
		//Mesh1P->SetMaterial(0, EntityInstance->GetMaterialByModel(Instance->GetInstalledMaterialModelId()));
		
		Mesh3P->SetSkeletalMesh(EntityInstance->GetTPPMeshData().Mesh);
		Mesh3P->SetAnimInstanceClass(EntityInstance->GetTPPMeshData().Anim);
		Mesh3P->SetMinLOD(EntityInstance->GetTPPMeshData().MinLOD);
		//Mesh3P->SetMaterial(0, EntityInstance->GetMaterialByModel(Instance->GetInstalledMaterialModelId()));

		if (GetLocalRole() == ROLE_Authority) MakeFeedback();
	}
}

UWeaponPlayerItem* AShooterWeapon::GetInstance() const
{
	return GetValidObject(Instance);
}

void AShooterWeapon::InitializeInstance(UWeaponPlayerItem* InInstance)
{
	Instance = InInstance;
	OnRep_Instance();
}

void AShooterWeapon::PostInitializeComponents()
{
	Super::PostInitializeComponents();

	if (IsValidObject(Mesh1P))
	{
		Mesh1P->SetComponentTickEnabledAsync(true);
	}

	if (IsValidObject(Mesh3P))
	{
		Mesh3P->SetComponentTickEnabledAsync(true);
	}

	DetachMeshFromPawn();
}


void AShooterWeapon::BeginDestroy()
{
	//----------------------------------------
	if (auto TM = GetTimerManager())
	{
		TM->ClearAllTimersForObject(this);
	}

	//----------------------------------------
	Super::BeginDestroy();
}

void AShooterWeapon::Destroyed()
{
	//===============================================
	auto PawnOwner = GetPawnOwner();

	if (IsLocallyControlled())
	{
		ToggleAimingParams(false);
	}

	//===============================================
	if (auto TM = GetTimerManager())
	{
		TM->ClearAllTimersForObject(this);
	}

	//===============================================
	StopSimulatingWeaponFire();

	//===============================================
	Super::Destroyed();
}


UWorld* AShooterWeapon::GetValidWorld() const
{
	return GetValidObject(GetWorld());
}

UWorld* AShooterWeapon::GetValidWorld()
{
	return GetValidObject(GetWorld());
}

FTimerManager* AShooterWeapon::GetTimerManager() const
{
	if (auto w = GetValidWorld())
	{
		return &w->GetTimerManager();
	}
	return nullptr;
}

FTimerManager* AShooterWeapon::GetTimerManager()
{
	if (auto w = GetValidWorld())
	{
		return &w->GetTimerManager();
	}
	return nullptr;
}

//////////////////////////////////////////////////////////////////////////
// Inventory

void AShooterWeapon::OnEquip()
{
	if (IsValidObject(this) == false || GetValidWorld() == nullptr)
	{
		return;
	}

	SetReplicates(true);
	SetActorTickEnabled(true);

	AttachMeshToPawn();

	bPendingEquip = true;
	DetermineWeaponState();
	
	if (IsValidObject(Instance))
	{
		float Duration = PlayWeaponAnimation(GetWeaponProperty()->EquipAnim);
		if (Duration <= 0.0f)
		{
			// failsafe
			Duration = 0.5f;
		}
		EquipStartedTime = GetWorld()->GetTimeSeconds();
		EquipDuration = Duration;

		GetWorldTimerManager().SetTimer(TimerHandle_OnEquipFinished, this, &AShooterWeapon::OnEquipFinished, Duration, false);
	}
	else
	{
		OnEquipFinished();
	}

	auto PawnOwner = GetPawnOwner();
	if (IsLocallyControlled() && GetInstance())
	{
		PlayWeaponSound(GetWeaponProperty()->EquipSound);
	}
}

void AShooterWeapon::OnEquipFinished()
{
	if (IsValidObject(this) == false || GetValidWorld() == nullptr)
	{
		return;
	}

	AttachMeshToPawn();

	bIsEquipped = true;
	bPendingEquip = false;

	// Determine the state so that the can reload checks will work
	DetermineWeaponState(); 
	

	// try to reload empty clip
	if (HasAuthority() && CurrentAmmoInClip <= 0 && CanReload())
	{
		StartReload();
	}	
}

bool AShooterWeapon::IsLocallyControlled() const
{
	return GetPawnOwner() && GetPawnOwner()->IsLocallyControlled();
}

void AShooterWeapon::OnUnEquip()
{
	if (IsValidObject(this) == false || GetValidWorld() == nullptr)
	{
		return;
	}

	auto PawnOwner = GetPawnOwner();
	UE_LOG(LogShooterWeapon, Log, TEXT("OnUnEquip >> HasAuthority = %s, MyPawn = %s"), HasAuthority() ? TEXT("True") : TEXT("False"), PawnOwner ? *PawnOwner->GetName() : TEXT("nullptr"));

	DetachMeshFromPawn();
	bIsEquipped = false;
	StopFire();

	auto WeaponProperty = GetWeaponProperty();

	if (bPendingReload)
	{
		if (WeaponProperty)
		{
			StopWeaponAnimation(WeaponProperty->ReloadAnim);
		}

		bPendingReload = false;

		GetWorldTimerManager().ClearTimer(TimerHandle_StopReload);
		GetWorldTimerManager().ClearTimer(TimerHandle_ReloadWeapon);
	}

	if (bPendingEquip)
	{
		if (WeaponProperty)
		{
			StopWeaponAnimation(WeaponProperty->EquipAnim);
		}

		bPendingEquip = false;

		GetWorldTimerManager().ClearTimer(TimerHandle_OnEquipFinished);
	}

	DetermineWeaponState();

	if (IsLocallyControlled())
	{
		ToggleAimingParams(false);
	}

	SetActorTickEnabled(false);
	bReplicates = false;
}

void AShooterWeapon::OnEnterInventory(AShooterCharacter* NewOwner)
{
	SetOwningPawn(NewOwner);
}

void AShooterWeapon::OnLeaveInventory()
{
	auto PawnOwner = GetPawnOwner();
	UE_LOG(LogShooterWeapon, Log, TEXT("OnLeaveInventory >> HasAuthority = %s, MyPawn = %s"), HasAuthority() ? TEXT("True") : TEXT("False"), PawnOwner ? *PawnOwner->GetName() : TEXT("nullptr"));

	if (GetLocalRole() == ROLE_Authority)
	{
		SetOwningPawn(nullptr);
		SetLifeSpan(3.0f); // For unquip anim
	}

	if (IsAttachedToPawn())
	{
		OnUnEquip();
	}
}

void AShooterWeapon::AttachMeshToPawn()
{
	auto PawnOwner = GetPawnOwner();

	if (PawnOwner)
	{
		FAttachmentTransformRules Rules(EAttachmentRule::SnapToTarget, false);

		// Remove and hide both first and third person meshes
		DetachMeshFromPawn();

		// For locally controller players we attach both weapons and let the bOnlyOwnerSee, bOwnerNoSee flags deal with visibility.
		if(PawnOwner->IsLocallyControlled() == true )
		{
			USkeletalMeshComponent* PawnMesh1p = PawnOwner->GetSpecifcPawnMesh(true);
			USkeletalMeshComponent* PawnMesh3p = PawnOwner->GetSpecifcPawnMesh(false);
			Mesh1P->SetHiddenInGame(false);
			Mesh3P->SetHiddenInGame(false);
			Mesh1P->AttachToComponent(PawnMesh1p, Rules, PawnOwner->GetFPPWeaponAttachPoint());
			Mesh3P->AttachToComponent(PawnMesh3p, Rules, PawnOwner->GetTPPWeaponAttachPoint());
		}
		else
		{
			auto UseWeaponMesh = GetWeaponMesh();
			auto UsePawnMesh = PawnOwner->GetPawnMesh();
			UseWeaponMesh->AttachToComponent(UsePawnMesh, Rules, PawnOwner->GetTPPWeaponAttachPoint());
			UseWeaponMesh->SetHiddenInGame( false );
		}
	}
}

void AShooterWeapon::DetachMeshFromPawn()
{
	FDetachmentTransformRules Rules(EDetachmentRule::KeepRelative, false);

	if (IsValidObject(Mesh1P))
	{
		Mesh1P->DetachFromComponent(Rules);
		Mesh1P->SetHiddenInGame(true);
	}

	if (IsValidObject(Mesh3P))
	{
		Mesh3P->DetachFromComponent(Rules);
		Mesh3P->SetHiddenInGame(true);
	}
}


//////////////////////////////////////////////////////////////////////////
// Input

void AShooterWeapon::StartFire()
{
	if (CurrentState == EWeaponState::Reloading)
	{
		bIsCancelReload = true;
	}

	if (GetLocalRole() < ROLE_Authority)
	{
		ServerStartFire();
	}

	if (!bWantsToFire)
	{
		bWantsToFire = true;
		DetermineWeaponState();
	}
}

void AShooterWeapon::StopFire()
{
	if (GetLocalRole() < ROLE_Authority)
	{
		ServerStopFire();
	}

	if (bWantsToFire)
	{
		bWantsToFire = false;
		DetermineWeaponState();
	}
}

void AShooterWeapon::SwitchWeaponToPreview()
{
	auto PawnOwner = GetPawnOwner();
	if (PawnOwner)
	{
		PawnOwner->SwitchWeaponToPreview();
	}
}


bool AShooterWeapon::SwitchWeaponToPistol()
{
	auto PawnOwner = GetPawnOwner();

	if (PawnOwner)
	{
		bHasSwitctedToPistol = PawnOwner->SwitchWeaponToPistol();
		return bHasSwitctedToPistol;
	}
	return false;
}

bool AShooterWeapon::IsRequiredReload() const
{
	return CurrentAmmoInClip <= 0 && HasInfiniteClip() == false;
}

void AShooterWeapon::StartReload(bool bFromReplication)
{
	if (IsRunningDedicatedServer() == false)
	{
		StartedRealoadTime = FSlateApplication::Get().GetCurrentTime();
	}

	//====================================
	if (GetLocalRole() == ROLE_Authority && bIsEquipped && IsRequiredReload())
	{
		if (GetItemType() == EGameItemType::Pistol)
		{
			SwitchWeaponToPreview();
		}
		else if(!bHasSwitctedToPistol)
		{
			if (SwitchWeaponToPistol())
			{
				return;
			}
		}
	}

	//====================================
	if (!bFromReplication && GetLocalRole() < ROLE_Authority)
	{
		auto PawnOwner = GetPawnOwner();
		if (PawnOwner)
		{
			PawnOwner->SetTargeting(false);
			ServerStartReload();
		}
	}

	if (GetInstance() && bIsEquipped)
	{
		auto PawnOwner = GetPawnOwner();

		if (PawnOwner)
		{
			if (GetItemType() == EGameItemType::Pistol)
			{
				if (PawnOwner->GetPreviewWeapon() != nullptr)
				{
					return;
				}
			}
			else
			{
				if (!bHasSwitctedToPistol && PawnOwner->FindWeaponBySlot(EGameItemType::Pistol) && IsRequiredReload())
				{
					return;
				}
			}
		}

		if (bFromReplication || (CanReload() && HasAuthority()))
		{
			bPendingReload = true;
			DetermineWeaponState();

			float AnimDuration = 2.0f;
			bool IsPartlyReload = false;
			int32 AmmoPerClip = 0;

			if (auto WeaponConfiguration = GetWeaponConfiguration())
			{
				AnimDuration = WeaponConfiguration->ReloadDuration;
				IsPartlyReload = WeaponConfiguration->IsPartlyReload;
				AmmoPerClip = WeaponConfiguration->AmmoPerClip;
			}

			if (auto WeaponProperty = GetWeaponProperty())
			{
				PlayMontageWithDuration(Mesh1P, WeaponProperty->WeaponMontageReload, AnimDuration);

				if (PawnOwner)
				{
					if (IsFirstPerson())
					{
						PlayMontageWithDuration(PawnOwner->GetMesh1P(), WeaponProperty->GetPawnFPPMontageReload(), AnimDuration);
					}
					else
					{
						PlayMontageWithDuration(PawnOwner->GetMesh(), WeaponProperty->GetPawnTPPMontageReload(), AnimDuration);
					}
				}
			}

			GetWorldTimerManager().SetTimer(TimerHandle_StopReload, this, &AShooterWeapon::StopReload, AnimDuration, false);
			if (GetLocalRole() == ROLE_Authority)
			{
				GetWorldTimerManager().SetTimer(TimerHandle_ReloadWeapon, this, &AShooterWeapon::ReloadWeapon, FMath::Max(0.1f, AnimDuration - 0.1f), false);
				MakeFeedback();

				//if (GetInstance())
				//{
				//	UUTGameplayStatics::NotifyBotsSound(this, PawnOwner, GetWeaponProperty()->ReloadSound);
				//}
			}



			if ((GetNetMode() == NM_Client || GetNetMode() == NM_Standalone) && IsPartlyReload)
			{
				if (GetCurrentAmmoInClip() < AmmoPerClip && bIsCancelReload == false)
				{
					GetWorldTimerManager().SetTimer(TimerHandle_ReloadWeapon, FTimerDelegate::CreateUObject(this, &AShooterWeapon::StartReload, false), FMath::Max(0.1f, AnimDuration + 0.1f), false);
				}

				if (bIsCancelReload)
				{
					bIsCancelReload = false;
				}
			}

			if (IsLocallyControlled())
			{
				PlayWeaponSound(GetWeaponProperty()->ReloadSound);
			}
		}
	}
}

void AShooterWeapon::StopReload()
{
	if (CurrentState == EWeaponState::Reloading)
	{
		bPendingReload = false;
		DetermineWeaponState();
		auto WeaponProperty = GetWeaponProperty();
		if (WeaponProperty)
		{
			StopWeaponAnimation(WeaponProperty->ReloadAnim);
		}
	}
}

bool AShooterWeapon::ServerStartFire_Validate()
{
	return true;
}

void AShooterWeapon::ServerStartFire_Implementation()
{
	StartFire();
}

bool AShooterWeapon::ServerStopFire_Validate()
{
	return true;
}

void AShooterWeapon::ServerStopFire_Implementation()
{
	StopFire();
}

bool AShooterWeapon::ServerStartReload_Validate()
{
	return true;
}

void AShooterWeapon::ServerStartReload_Implementation()
{
	StartReload();
}

bool AShooterWeapon::ServerStopReload_Validate()
{
	return true;
}

void AShooterWeapon::ServerStopReload_Implementation()
{
	StopReload();
}

void AShooterWeapon::ClientStartReload_Implementation()
{
	StartReload();
}

//////////////////////////////////////////////////////////////////////////
// Control

bool AShooterWeapon::CanFire() const
{
	auto PawnOwner = GetPawnOwner();
	bool bCanFire = PawnOwner && PawnOwner->CanFire();
	bool bStateOKToFire = ( ( CurrentState ==  EWeaponState::Idle ) || ( CurrentState == EWeaponState::Firing) );	
	return (( bCanFire == true ) && ( bStateOKToFire == true ) && ( bPendingReload == false ) );
}

bool AShooterWeapon::CanReload() const
{
	if (GetInstance())
	{
		int32 AmmoPerClip = 0;
		auto PawnOwner = GetPawnOwner();
		bool bCanReload = (!PawnOwner || PawnOwner->CanReload());

		if (auto WeaponConfiguration = GetWeaponConfiguration())
		{
			AmmoPerClip = WeaponConfiguration->AmmoPerClip;
		}

		bool bGotAmmo =(CurrentAmmoInClip < AmmoPerClip) && (TotalCountAmmo - CurrentAmmoInClip > 0 || HasInfiniteClip());
		bool bStateOKToReload = ((CurrentState == EWeaponState::Idle) || (CurrentState == EWeaponState::Firing));
		return ((bCanReload == true) && (bGotAmmo == true) && (bStateOKToReload == true));
	}

	return false;
}


//////////////////////////////////////////////////////////////////////////
// Weapon usage

void AShooterWeapon::GiveAmmo(int AddAmount)
{
	TotalCountAmmo += AddAmount;
	
	// start reload if clip was empty
	if (GetCurrentAmmoInClip() <= 0 && CanReload() && GetPawnOwner() && GetPawnOwner()->GetWeapon() == this)
	{
		ClientStartReload();
	}
}

void AShooterWeapon::SetAmmo(int AddAmount)
{
	CurrentAmmoInClip = 0;
	TotalCountAmmo = AddAmount;

	if (TotalCountAmmo >= GetAmmoPerClip())
	{
		TotalCountAmmo -= GetAmmoPerClip();
		CurrentAmmoInClip = GetAmmoPerClip();
	}
}

int32 AShooterWeapon::GetAmmo() const
{
	if (GetInstance())
	{
		return TotalCountAmmo;
	}

	return 1;
}

void AShooterWeapon::UseAmmo()
{
	if (!HasInfiniteAmmo())
	{
		CurrentAmmoInClip--;
	}

	if (!HasInfiniteAmmo() && !HasInfiniteClip())
	{
		TotalCountAmmo--;
		//OnRep_TotalCountAmmo();
	}

	FRandomStream rnd(FeedbackData);

	float FeedBack = 0.5;
	if (auto WeaponConfiguration = GetWeaponConfiguration())
	{
		FeedBack = WeaponConfiguration->Feedback;
	}

	TargetRecoilY = -FeedBack;
	TargetRecoilX = rnd.FRandRange(FeedBack, FeedBack * -1.0f);

	auto PawnOwner = GetPawnOwner();

	if (PawnOwner)
	{
		if (PawnOwner->bIsCrouched)
		{
			const auto ModiferCrouched = rnd.FRandRange(.5f, .8f);
			TargetRecoilX *= ModiferCrouched;
			TargetRecoilY *= ModiferCrouched;
		}

		if (PawnOwner->IsTargeting())
		{
			const auto ModiferTargeting = rnd.FRandRange(.4f, .6f);
			TargetRecoilX *= ModiferTargeting;
			TargetRecoilY *= ModiferTargeting;
		}
	}

}

void AShooterWeapon::HandleFiring()
{
	if ((GetCurrentAmmoInClip() > 0 || HasInfiniteClip() || HasInfiniteAmmo()) && CanFire())
	{
		if (GetNetMode() != NM_DedicatedServer)
		{
			SimulateWeaponFire();
		}

		if (IsLocallyControlled())
		{
			FireWeapon();

			UseAmmo();
			
			// update firing FX on remote clients if function was called on server
			BurstCounter++;
		}
	}
	else if (IsRequiredReload() && CanReload())
	{
		StartReload();
	}
	else if (IsLocallyControlled())
	{
		if (GetCurrentAmmo() == 0 && !bRefiring)
		{
			PlayWeaponSound(GetWeaponProperty()->OutOfAmmoSound);
		}
		
		// stop weapon fire FX, but stay in Firing state
		if (BurstCounter > 0)
		{
			OnBurstFinished();
		}
	}

	if (IsLocallyControlled())
	{
		// local client will notify server
		if (GetLocalRole() < ROLE_Authority)
		{
			ServerHandleFiring();
		}

		// reload after firing last round
		if (IsRequiredReload() && CanReload())
		{
			StartReload();
		}

		float TimeBetweenShots = 0.050f;
		if (auto WeaponConfiguration = GetWeaponConfiguration())
		{
			TimeBetweenShots = WeaponConfiguration->TimeBetweenShots();
		}

		// setup refire timer
		bRefiring = (CurrentState == EWeaponState::Firing && GetInstance() && TimeBetweenShots > 0.0f);
		if (bRefiring)
		{
			GetWorldTimerManager().SetTimer(TimerHandle_HandleFiring, this, &AShooterWeapon::HandleFiring, TimeBetweenShots, false);
		}
	}	

	LastFireTime = GetWorld()->GetTimeSeconds();

	if (GetWeaponConfiguration() && GetWeaponConfiguration()->IsAutomaticFire == false)
	{
		StopFire();
	}
}

bool AShooterWeapon::ServerHandleFiring_Validate()
{
	return true;
}

void AShooterWeapon::ServerHandleFiring_Implementation()
{
	const bool bShouldUpdateAmmo = (CurrentAmmoInClip > 0 && CanFire());

	HandleFiring();

	if (bShouldUpdateAmmo)
	{
		// update ammo
		UseAmmo();

		// update firing FX on remote clients
		BurstCounter++;				
	}
}

void AShooterWeapon::ForceReloadWeapon()
{
	if (auto WeaponConfiguration = GetWeaponConfiguration())
	{
		CurrentAmmoInClip = WeaponConfiguration->AmmoPerClip;
	}
	else
	{
		CurrentAmmoInClip = 0;
	}
	bPendingReload = false;
}

void AShooterWeapon::ReloadWeapon()
{
	int32 AmmoPerClip = 0;
	bool IsPartlyReload = false;

	if (auto WeaponConfiguration = GetWeaponConfiguration())
	{
		IsPartlyReload = WeaponConfiguration->IsPartlyReload;
		AmmoPerClip = WeaponConfiguration->AmmoPerClip;
	}

	int32 ClipDelta = IsPartlyReload ? 1 : FMath::Min<int32>(AmmoPerClip - CurrentAmmoInClip, TotalCountAmmo - CurrentAmmoInClip);

	bHasSwitctedToPistol = false;

	if (HasInfiniteClip())
	{
		ClipDelta = AmmoPerClip - CurrentAmmoInClip;
	}

	if (ClipDelta > 0)
	{
		CurrentAmmoInClip += ClipDelta;
	}

	if (HasInfiniteClip())
	{
		TotalCountAmmo = FMath::Max(CurrentAmmoInClip, TotalCountAmmo);
		//OnRep_TotalCountAmmo();
	}

}

void AShooterWeapon::SetWeaponState(EWeaponState NewState)
{
	const EWeaponState PrevState = CurrentState;

	if (PrevState == EWeaponState::Firing && NewState != EWeaponState::Firing)
	{
		OnBurstFinished();
	}

	CurrentState = NewState;

	if (PrevState != EWeaponState::Firing && NewState == EWeaponState::Firing)
	{
		OnBurstStarted();
	}
}

void AShooterWeapon::DetermineWeaponState()
{
	EWeaponState NewState = EWeaponState::Idle;

	if (bIsEquipped)
	{
		if( bPendingReload  )
		{
			if( CanReload() == false )
			{
				NewState = CurrentState;
			}
			else
			{
				NewState = EWeaponState::Reloading;
			}
		}		
		else if ( (bPendingReload == false ) && ( bWantsToFire == true ) && ( CanFire() == true ))
		{
			NewState = EWeaponState::Firing;
		}
	}
	else if (bPendingEquip)
	{
		NewState = EWeaponState::Equipping;
	}

	SetWeaponState(NewState);
}

void AShooterWeapon::OnBurstStarted()
{
	if (IsValidObject(GetWorld()))
	{
		if (GetWeaponConfiguration())
		{
			// start firing, can be delayed to satisfy TimeBetweenShots()
			const float GameTime = GetWorld()->GetTimeSeconds();
			if (GetInstance() && LastFireTime > 0 && GetWeaponConfiguration()->TimeBetweenShots() > 0.0f &&
				LastFireTime + GetWeaponConfiguration()->TimeBetweenShots() > GameTime)
			{
				GetWorldTimerManager().SetTimer(TimerHandle_HandleFiring, this, &AShooterWeapon::HandleFiring, LastFireTime + GetWeaponConfiguration()->TimeBetweenShots() - GameTime, false);
			}
			else
			{
				HandleFiring();
			}
		}
	}
}

void AShooterWeapon::OnBurstFinished()
{
	// stop firing FX on remote clients
	BurstCounter = 0;
	CurrentFiringSpread = .0f;

	// stop firing FX locally, unless it's a dedicated server
	if (GetNetMode() != NM_DedicatedServer)
	{
		StopSimulatingWeaponFire();
	}
	
	GetWorldTimerManager().ClearTimer(TimerHandle_HandleFiring);
	bRefiring = false;
}

//
//////////////////////////////////////////////////////////////////////////
// Weapon usage helpers

UAudioComponent* AShooterWeapon::PlayWeaponSound(const TSoftObjectPtr<USoundCue>& InSound)
{
	return PlayWeaponSound(InSound.LoadSynchronous());
}

UAudioComponent* AShooterWeapon::PlayWeaponSound(USoundCue* Sound)
{
	UAudioComponent* AC = nullptr;
	auto PawnOwner = GetPawnOwner();

	if (IsValidObject(Sound) && PawnOwner)
	{
		AC = UGameplayStatics::SpawnSoundAttached(Sound, PawnOwner->GetRootComponent());
	}

	return AC;
}

float AShooterWeapon::PlayWeaponAnimation(FTwoAnimMontage& Animation)
{
	float Duration = 0.0f;
	auto PawnOwner = GetPawnOwner();

	if (PawnOwner)
	{
		Duration = PawnOwner->PlayTwoAnimMontage(Animation);
	}

	return Duration;
}

void AShooterWeapon::StopWeaponAnimation(FTwoAnimMontage& Animation)
{
	auto PawnOwner = GetPawnOwner();

	if (PawnOwner)
	{
		auto UseAnim = PawnOwner->IsFirstPerson() ? Animation.GetPawn1P() : Animation.GetPawn3P();
		if (IsValidObject(UseAnim))
		{
			PawnOwner->StopAnimMontage(UseAnim);
		}
	}
}

FVector AShooterWeapon::GetCameraAim() const
{


	AShooterPlayerController* const PlayerController = IsValidObject(GetInstigator()) ? GetValidObjectAs<AShooterPlayerController>(GetInstigator()->Controller) : nullptr;
	FVector FinalAim = FVector::ZeroVector;

	if (PlayerController)
	{
		FVector CamLoc;
		FRotator CamRot;
		PlayerController->GetPlayerViewPoint(CamLoc, CamRot);
		FinalAim = CamRot.Vector();
	}
	else if (IsValidObject(GetInstigator()))
	{
		FinalAim = GetInstigator()->GetBaseAimRotation().Vector();		
	}

	return FinalAim;
}

FVector AShooterWeapon::GetAdjustedAim() const
{
	AShooterPlayerController* const PlayerController = IsValidObject(GetInstigator()) ? GetValidObjectAs<AShooterPlayerController>(GetInstigator()->Controller) : nullptr;
	FVector FinalAim = FVector::ZeroVector;
	// If we have a player controller use it for the aim
	if (PlayerController)
	{
		FVector CamLoc;
		FRotator CamRot;
		PlayerController->GetPlayerViewPoint(CamLoc, CamRot);
		FinalAim = CamRot.Vector();
	}
	else if (IsValidObject(GetInstigator()))
	{
		// Now see if we have an AI controller - we will want to get the aim from there if we do
		AShooterAIController* AIController = GetPawnAIController();
		if(AIController != nullptr )
		{
			FinalAim = AIController->GetControlRotation().Vector();
		}
		else
		{			
			FinalAim = GetInstigator()->GetBaseAimRotation().Vector();
		}
	}

	return FinalAim;
}

AShooterAIController* AShooterWeapon::GetPawnAIController() const
{
	auto PawnOwner = GetPawnOwner();
	if (PawnOwner)
	{
		return GetValidObjectAs<AShooterAIController>(PawnOwner->Controller);
	}
	return nullptr;
}

AShooterPlayerController* AShooterWeapon::GetPawnPlayerController() const
{
	auto PawnOwner = GetPawnOwner();
	if (PawnOwner)
	{
		return GetValidObjectAs<AShooterPlayerController>(PawnOwner->Controller);
	}
	return nullptr;
}

FVector AShooterWeapon::GetCameraDamageStartLocation(const FVector& AimDir) const
{
	AShooterPlayerController* PC = GetPawnPlayerController();
	FVector OutStartTrace = FVector::ZeroVector;

	if (PC)
	{
		// use player's camera
		FRotator UnusedRot;
		PC->GetPlayerViewPoint(OutStartTrace, UnusedRot);

		// Adjust trace so there is nothing blocking the ray between the camera and the pawn, and calculate distance from adjusted start
		OutStartTrace = OutStartTrace + AimDir * ((GetInstigator()->GetActorLocation() - OutStartTrace) | AimDir);
	}
	else
	{
		AShooterAIController* AIPC = GetPawnAIController();
		if (AIPC)
		{
			OutStartTrace = GetMuzzleLocation();
		}
	}
	return OutStartTrace;
}

FVector AShooterWeapon::GetMuzzleLocation() const
{
	USceneComponent* UseMesh = GetMuzzleSocketComponent(IsFirstPerson());

	if (GetMuzzleSocketName() != NAME_None)
	{	
		return UseMesh->GetSocketLocation(GetMuzzleSocketName());
	}

	return UseMesh->GetComponentLocation() + UseMesh->GetForwardVector() * 100.0f;
}

FVector AShooterWeapon::GetMuzzleDirection() const
{
	USceneComponent* UseMesh = GetMuzzleSocketComponent(IsFirstPerson());

	if (GetMuzzleSocketName() != NAME_None)
	{		
		return UseMesh->GetSocketRotation(GetMuzzleSocketName()).Vector();
	}

	return UseMesh->GetForwardVector();
}

FName AShooterWeapon::GetMuzzleSocketName() const
{
	if (GetInstance())
	{
		return GetWeaponProperty()->Name_InstantMuzzle;
	}

	return NAME_None;
}

USceneComponent* AShooterWeapon::GetMuzzleSocketComponent(const bool IsFirstView) const
{
	return GetValidObject(IsFirstView ? Mesh1P : Mesh3P);
}

FHitResult AShooterWeapon::WeaponTrace(const FVector& StartTrace, const FVector& EndTrace) const
{
	static FName WeaponFireTag = FName(TEXT("WeaponTrace"));

	// Perform trace to retrieve hit info
	FCollisionQueryParams TraceParams(WeaponFireTag, true, GetInstigator());
	TraceParams.bReturnPhysicalMaterial = true;
	TraceParams.MobilityType = EQueryMobilityType::Any;
	TraceParams.bFindInitialOverlaps = true;
	
	FHitResult Hit(ForceInit);
	GetWorld()->LineTraceSingleByChannel(Hit, StartTrace, EndTrace, COLLISION_WEAPON, TraceParams);

	return Hit;
}

void AShooterWeapon::SetOwningPawn(AShooterCharacter* NewOwner)
{
	if (MyPawn != NewOwner)
	{
		SetInstigator(NewOwner);
		MyPawn = NewOwner;
		// net owner for RPC calls
		SetOwner(NewOwner);
	}	
}

//////////////////////////////////////////////////////////////////////////
// Replication & effects

void AShooterWeapon::OnRep_MyPawn()
{
	auto PawnOwner = GetPawnOwner();
	if (PawnOwner)
	{
		UE_LOG(LogShooterWeapon, Log, TEXT("OnRep_MyPawn >> MyPawn = %s"), *PawnOwner->GetName());
		OnEnterInventory(PawnOwner);
	}
	else
	{
		UE_LOG(LogShooterWeapon, Log, TEXT("OnRep_MyPawn >> MyPawn = nullptr"));
		OnLeaveInventory();
	}
}

void AShooterWeapon::OnRep_BurstCounter()
{
	if (BurstCounter > 0)
	{
		SimulateWeaponFire();
	}
	else
	{
		StopSimulatingWeaponFire();
	}
}

void AShooterWeapon::OnRep_Reload()
{
	if (bPendingReload)
	{
		StartReload(true);
	}
	else
	{
		StopReload();
	}
}

void AShooterWeapon::SimulateWeaponFire()
{
	if (GetLocalRole() == ROLE_Authority && CurrentState != EWeaponState::Firing)
	{
		return;
	}

	auto PawnOwner = GetPawnOwner();

	if (GetInstanceEntity())
	{
		bool bIsScopeTargeting = false;

		auto MyPS = GetPawnPlayerState();

		if (MyPS && MyPS->Slate_SniperScope.IsValid())
		{
			if (MyPS->Slate_SniperScope->GetVisibility() == EVisibility::HitTestInvisible)
			{
				bIsScopeTargeting = true;
			}
		}

		int32 TargetMeshIdx = INDEX_NONE;
		//UItemModuleTrunkEntity* ModuleTrunk = Cast<UItemModuleTrunkEntity>(FindInstalledModuleEntity(UItemModuleTrunkEntity::StaticClass(), TargetMeshIdx));

		if (GetWeaponProperty()->GetMuzzleFX())
		{
			if (!GetWeaponProperty()->bLoopedMuzzleFX || MuzzlePSC == nullptr)
			{
				// Split screen requires we create 2 effects. One that we see and one that the other player sees.
				if (PawnOwner && PawnOwner->IsLocallyControlled())
				{
					AController* PlayerCon = PawnOwner->GetController();
					if (PlayerCon != nullptr)
					{
						if (bIsScopeTargeting == false)
						{


							MuzzlePSC = UGameplayStatics::SpawnEmitterAttached(GetWeaponProperty()->GetMuzzleFX(), GetMuzzleSocketComponent(true), GetMuzzleSocketName());
							MuzzlePSC->bOwnerNoSee = false;
							MuzzlePSC->bOnlyOwnerSee = true;

							//if (ModuleTrunk)
							//{
							//	MuzzlePSC->SetFloatParameter("Intensity", ModuleTrunk->GetFlameIntensity());
							//	MuzzlePSC->SetVectorParameter("Intensity", FVector(ModuleTrunk->GetFlameIntensity()));
							//}
						}

						MuzzlePSCSecondary = UGameplayStatics::SpawnEmitterAttached(GetWeaponProperty()->GetMuzzleFX(), GetMuzzleSocketComponent(false), GetMuzzleSocketName());
						MuzzlePSCSecondary->bOwnerNoSee = true;
						MuzzlePSCSecondary->bOnlyOwnerSee = false;

						//if (ModuleTrunk)
						//{
						//	MuzzlePSCSecondary->SetFloatParameter("Intensity", ModuleTrunk->GetFlameIntensity());
						//	MuzzlePSCSecondary->SetVectorParameter("Intensity", FVector(ModuleTrunk->GetFlameIntensity()));
						//}
					}
				}
				else
				{
					MuzzlePSC = UGameplayStatics::SpawnEmitterAttached(GetWeaponProperty()->GetMuzzleFX(), GetMuzzleSocketComponent(false), GetMuzzleSocketName());

					//if (ModuleTrunk)
					//{
					//	MuzzlePSC->SetFloatParameter("Intensity", ModuleTrunk->GetFlameIntensity());
					//	MuzzlePSC->SetVectorParameter("Intensity", FVector(ModuleTrunk->GetFlameIntensity()));
					//}
				}
			}
		}

		if (!GetWeaponProperty()->bLoopedFireAnim || !bPlayingFireAnim)
		{
			PlayWeaponAnimation(GetWeaponProperty()->FireAnim);
			bPlayingFireAnim = true;
		}

		float TimeBetweenShots = 0.050f;
		if (auto WeaponConfiguration = GetWeaponConfiguration())
		{
			TimeBetweenShots = GetWeaponConfiguration()->TimeBetweenShots();
		}

		PlayMontageWithDuration(Mesh1P, GetWeaponProperty()->GetWeaponMontageFire(), TimeBetweenShots);

		if (PawnOwner)
		{
			if (PawnOwner->IsTargeting())
			{
				if (PawnOwner->IsFirstPerson())
				{
					PlayMontageWithDuration(PawnOwner->GetMesh1P(), GetWeaponProperty()->FireInTargeting.Pawn1P, TimeBetweenShots);
				}
				else
				{
					PlayMontageWithDuration(PawnOwner->GetMesh(), GetWeaponProperty()->FireInTargeting.Pawn3P, TimeBetweenShots);
				}
			}
			else
			{
				if (PawnOwner->IsFirstPerson())
				{
					PlayMontageWithDuration(PawnOwner->GetMesh1P(), GetWeaponProperty()->PawnFPPMontageFire, TimeBetweenShots);
				}
				else
				{
					PlayMontageWithDuration(PawnOwner->GetMesh(), GetWeaponProperty()->PawnTPPMontageFire, TimeBetweenShots);
				}
			}
		}


		if (GetWeaponProperty()->bLoopedFireSound)
		{
			if (FireAC == nullptr)
			{
				FireAC = PlayWeaponSound(GetWeaponProperty()->FireLoopSound);
			}
		}
		else
		{
			PlayWeaponSound(GetWeaponProperty()->FireSound);
		}

		AShooterPlayerController* PC = (PawnOwner != nullptr) ? GetValidObjectAs<AShooterPlayerController>(PawnOwner->Controller) : NULL;
		if (PC != nullptr && PC->IsLocalController())
		{
			if (GetWeaponProperty()->FireCameraShake != nullptr)
			{
				PC->ClientPlayCameraShake(GetWeaponProperty()->FireCameraShake);
			}

			if (GetWeaponProperty()->FireForceFeedback != nullptr)
			{
				FForceFeedbackParameters FeedbackParameters;
				FeedbackParameters.Tag = TEXT("Weapon");
				FeedbackParameters.bLooping = false;
				FeedbackParameters.bIgnoreTimeDilation = false;
				
				PC->ClientPlayForceFeedback(GetWeaponProperty()->FireForceFeedback, FeedbackParameters);
			}
		}
	}
}

void AShooterWeapon::PlayMontageWithDuration(USkeletalMeshComponent* PlayTarget, const TSoftObjectPtr<UAnimMontage>& Montage, float Duration)
{
	PlayMontageWithDuration(PlayTarget, Montage.LoadSynchronous(), Duration);
}

void AShooterWeapon::PlayMontageWithDuration(USkeletalMeshComponent* PlayTarget, UAnimMontage* Montage, float Duration)
{
	if (IsValidObject(PlayTarget) && IsValidObject(PlayTarget->AnimScriptInstance) && IsValidObject(Montage))
	{
		float playRate = FMath::Max(0.2f, Montage->SequenceLength / Duration);
		PlayTarget->AnimScriptInstance->Montage_Play(Montage, playRate);
	}
}

void AShooterWeapon::StopSimulatingWeaponFire()
{
	auto PawnOwner = GetPawnOwner();

	if (IsValidObject(FireAC))
	{
		FireAC->Stop();
	}

	auto WeaponProperty = GetWeaponProperty();
	if (WeaponProperty)
	{
		if (WeaponProperty->bLoopedMuzzleFX)
		{
			if (MuzzlePSC != nullptr)
			{
				MuzzlePSC->DeactivateSystem();
				MuzzlePSC = NULL;
			}
			if (MuzzlePSCSecondary != nullptr)
			{
				MuzzlePSCSecondary->DeactivateSystem();
				MuzzlePSCSecondary = NULL;
			}
		}

		if (WeaponProperty->bLoopedFireAnim && bPlayingFireAnim)
		{
			StopWeaponAnimation(WeaponProperty->FireAnim);
			
			if (IsValidObject(Mesh1P) && IsValidObject(Mesh1P->AnimScriptInstance) && WeaponProperty->GetWeaponMontageFire())
			{
				Mesh1P->AnimScriptInstance->Montage_Stop(.1f, WeaponProperty->GetWeaponMontageFire());
			}

			bPlayingFireAnim = false;
		}

		if (PawnOwner)
		{
			if (IsValidObject(PawnOwner->GetMesh1P()) && IsValidObject(PawnOwner->GetMesh1P()->AnimScriptInstance) && WeaponProperty->GetPawnFPPMontageFire())
			{
				PawnOwner->GetMesh1P()->AnimScriptInstance->Montage_Stop(.1f, WeaponProperty->GetPawnFPPMontageFire());
			}

			if (IsValidObject(PawnOwner->GetMesh()) && IsValidObject(PawnOwner->GetMesh()->AnimScriptInstance) && WeaponProperty->GetPawnTPPMontageFire())
			{
				PawnOwner->GetMesh()->AnimScriptInstance->Montage_Stop(.1f, WeaponProperty->GetPawnTPPMontageFire());
			}
		}
	}
}

void AShooterWeapon::GetLifetimeReplicatedProps( TArray< FLifetimeProperty > & OutLifetimeProps ) const
{
	Super::GetLifetimeReplicatedProps( OutLifetimeProps );

	DOREPLIFETIME(AShooterWeapon, Instance);
	DOREPLIFETIME(AShooterWeapon, MyPawn);

	DOREPLIFETIME_CONDITION(AShooterWeapon, bHasSwitctedToPistol,	COND_OwnerOnly);

	DOREPLIFETIME_CONDITION(AShooterWeapon, CurrentAmmoInClip,	COND_OwnerOnly);
	DOREPLIFETIME_CONDITION(AShooterWeapon, FeedbackData,		COND_OwnerOnly);

	DOREPLIFETIME_CONDITION(AShooterWeapon, BurstCounter,		COND_SkipOwner);
	DOREPLIFETIME(AShooterWeapon, bPendingReload);	

	DOREPLIFETIME_CONDITION(AShooterWeapon, ShootNotifyInstant, COND_SkipOwner);
}

USkeletalMeshComponent* AShooterWeapon::GetWeaponMesh() const
{
	auto PawnOwner = GetPawnOwner();
	return (PawnOwner != nullptr && PawnOwner->IsFirstPerson()) ? Mesh1P : Mesh3P;
}

class AShooterCharacter* AShooterWeapon::GetPawnOwner() const
{
	return GetValidObject(MyPawn);
}

bool AShooterWeapon::IsEquipped() const
{
	return bIsEquipped;
}

bool AShooterWeapon::IsAttachedToPawn() const
{
	return bIsEquipped || bPendingEquip;
}

EWeaponState AShooterWeapon::GetCurrentState() const
{
	return CurrentState;
}

int32 AShooterWeapon::GetCurrentAmmo() const
{
	if (IsValidObject(Instance))
	{
		return TotalCountAmmo;
	}

	return 0;
}

int32 AShooterWeapon::GetCurrentAmmoInClip() const
{
	return CurrentAmmoInClip;
}

int32 AShooterWeapon::GetAmmoPerClip() const
{
	if (IsValidObject(Instance))
	{
		if (auto WeaponConfiguration = GetWeaponConfiguration())
		{
			return WeaponConfiguration->AmmoPerClip;
		}
	}

	return 1;
}

int32 AShooterWeapon::GetMaxAmmo() const
{
	return 600;// GetWeaponConfiguration()->MaxAmmo;
}

bool AShooterWeapon::HasInfiniteAmmo() const
{
	//const AShooterPlayerController* MyPC = (PawnOwner != nullptr) ? Cast<const AShooterPlayerController>(PawnOwner->Controller) : NULL;
	return false;// (MyPC && MyPC->HasInfiniteAmmo());
}

bool AShooterWeapon::HasInfiniteClip() const
{
	//const AShooterPlayerController* MyPC = (PawnOwner != nullptr) ? Cast<const AShooterPlayerController>(PawnOwner->Controller) : NULL;
	return false;// (MyPC && MyPC->HasInfiniteClip());
}

float AShooterWeapon::GetEquipStartedTime() const
{
	return EquipStartedTime;
}

float AShooterWeapon::GetEquipDuration() const
{
	return EquipDuration;
}

void AShooterWeapon::ToggleAimingParams(const bool IsAiming)
{
	if (GetPawnOwner() && GetWeaponConfiguration() && GetWeaponConfiguration()->HasScope)
	{
		ToggleSniperScope(IsAiming);
	}
}

AShooterPlayerState* AShooterWeapon::GetPawnPlayerState() const
{
	auto PawnOwner = GetPawnOwner();
	if (PawnOwner)
	{
		return GetValidObjectAs<AShooterPlayerState>(PawnOwner->GetPlayerState());
	}
	return nullptr;
}

void AShooterWeapon::ToggleSniperScope(const bool toggle)
{
	bool bEnableSniperScope = false;
	const FSlateBrush* ScopeImage = new FSlateNoResource();
	FSlateColor ScopeBackground;
	const FWeaponConfiguration* WeaponConfig = nullptr;

	if (GetValidObject(Instance) && GetValidObject(Instance->GetWeaponEntity()))
	{
		WeaponConfig = &Instance->GetWeaponEntity()->GetWeaponConfiguration();
	}

	if (WeaponConfig && WeaponConfig->HasScope)
	{
		bEnableSniperScope = true;
	
		ScopeImage = &WeaponConfig->ScopeBrush;
		ScopeBackground = WeaponConfig->ScopeColor;
	}
	
	auto PawnOwner = GetPawnOwner();
	if (bEnableSniperScope && PawnOwner)
	{
		if (auto MyPS = GetPawnPlayerState())
		{
			if (MyPS->Slate_SniperScope.IsValid())
			{
				if (toggle)
				{
					MyPS->Slate_SniperScope->SetImage(ScopeImage);
					MyPS->Slate_SniperScope->SetColor(ScopeBackground);
					MyPS->Slate_SniperScope->SetVisibility(EVisibility::HitTestInvisible);
					Mesh1P->SetVisibility(false, true);
					PawnOwner->GetMesh1P()->SetVisibility(false, true);
				}
				else
				{
					MyPS->Slate_SniperScope->SetVisibility(EVisibility::Hidden);
					Mesh1P->SetVisibility(true, true);
					PawnOwner->GetMesh1P()->SetVisibility(true, true);
				}
			}
		}
	}
}

void AShooterWeapon::ApplyOffsetIfAI(FVector &Origin)
{
	//if (auto AIPC = GetValidObjectAs<AShooterBot>(MyPawn->Controller))
	//{
	//	Origin.X += FMath::FRandRange(-AIPC->AimRateOffset, AIPC->AimRateOffset);
	//	Origin.Y += FMath::FRandRange(-AIPC->AimRateOffset, AIPC->AimRateOffset);
	//	Origin.Z += FMath::FRandRange(-(AIPC->AimRateOffset / 2), AIPC->AimRateOffset / 2);

	//	//const int32 RandomSeed = FMath::Rand();
	//	//FRandomStream WeaponRandomStream(RandomSeed);		
	//	//Origin = WeaponRandomStream.VRandCone(Origin, FMath::DegreesToRadians(AIPC->AimRateOffset), FMath::DegreesToRadians(AIPC->AimRateOffset / 2));
	//}
}

USkeletalMeshComponent* AShooterWeapon::GetMesh1P() const { return GetValidObject(Mesh1P); }
USkeletalMeshComponent* AShooterWeapon::GetMesh3P() const { return GetValidObject(Mesh3P); }

float AShooterWeapon::GetWeaponMass() const
{
	if (IsValidObject(Instance))
	{
		return Instance->GetMass();
	}
	
	return .0f;
}

float AShooterWeapon::GetWeaponCoefficient() const
{	
	if (GetInstance())
	{
		if (auto WeaponConfiguration = GetWeaponConfiguration())
		{
			const float FireRate = 60000.0f / FTimespan::FromSeconds(WeaponConfiguration->TimeBetweenShots()).GetTotalMilliseconds();
			return (GetWeaponDamage() * FireRate) / 2660;
		}
	}

	return .0f;
}

void AShooterWeapon::TickActor(float DeltaTime, enum ELevelTick TickType, FActorTickFunction& ThisTickFunction)
{
	Super::TickActor(DeltaTime, TickType, ThisTickFunction);

	ProccesFeedback(DeltaTime);
}

void AShooterWeapon::ProccesFeedback(float DeltaTime)
{
	auto PawnOwner = GetPawnOwner();
	if (PawnOwner)
	{
		TargetRecoilX = FMath::FInterpTo(TargetRecoilX, .0f, DeltaTime, 10.0f);
		TargetRecoilY = FMath::FInterpTo(TargetRecoilY, .0f, DeltaTime, 10.0f);

		PawnOwner->AddControllerPitchInput(TargetRecoilY);
		PawnOwner->AddControllerYawInput(TargetRecoilX);
	}
}

void AShooterWeapon::MakeFeedback()
{
	FeedbackData = FMath::Rand();
}

FTransform AShooterWeapon::GetAimOffsetTransform() const
{
	FTransform ReturnValue(FTransform::Identity);

	if (GetInstanceEntity())
	{
		auto PawnOwner = GetPawnOwner();
		const auto Config = GetInstanceEntity()->GetWeaponProperty();
		if (PawnOwner && PawnOwner->IsTargeting())
		{
			//if (ScopeModule)
			//{
			//	ReturnValue = Config.AnimOffsetTransform;
			//	ReturnValue.SetTranslation(ReturnValue.GetTranslation() + ScopeModule->GetAimOffset());
			//}
			//else
			{
				ReturnValue = Config.AnimOffsetTransform;
				ReturnValue.SetTranslation(ReturnValue.GetTranslation() + Config.DefaultOffsetTransform.GetTranslation());
			}
		}
		else
		{
			ReturnValue = Config.IdleOffsetTransform;
		}		
	}

	return ReturnValue;
}










void AShooterWeapon::FireWeapon()
{
	if (GetInstance())
	{
		FireWeapon_Shotgun();

		//FireWeapon_Instant();
		//switch (GetCurrentAmmoInstance()->GetAmmoType())
		//{
		//	case EItemAmmoType::Instant: FireWeapon_Instant(); break;
		//	case EItemAmmoType::Scintilla: FireWeapon_Shotgun(); break;
		//	case EItemAmmoType::Projectile: FireWeapon_Projectile(); break;
		//}
	}
}

void AShooterWeapon::FireWeapon_Instant()
{
	//const auto Config = GetCurrentAmmoInstance()->GetPropertyInstant();

	const int32 RandomSeed = FMath::Rand();
	FRandomStream WeaponRandomStream(RandomSeed);
	const float CurrentSpread = GetCurrentSpread();
	const float ConeHalfAngle = FMath::DegreesToRadians(CurrentSpread * 0.5f);

	FVector AimDir = GetAdjustedAim();

	ApplyOffsetIfAI(AimDir);

	const FVector StartTrace = GetCameraDamageStartLocation(AimDir);
	const FVector ShootDir = WeaponRandomStream.VRandCone(AimDir, ConeHalfAngle, ConeHalfAngle);
	const FVector EndTrace = StartTrace + ShootDir * (/*Config.Distance*/ 50 * 100.0f);

	const FHitResult Impact = WeaponTrace(StartTrace, EndTrace);

	TArray<FInstantShootData> ShootData;
	ShootData.Add(FInstantShootData(Impact, ShootDir, FMath::Lerp<FVector>(Impact.ImpactPoint, EndTrace, .0001f)));

	ProcessInstantHit(ShootData);

	auto WeaponConfiguration = GetWeaponConfiguration();
	if (WeaponConfiguration)
	{
		CurrentFiringSpread = FMath::Min(WeaponConfiguration->Spread, CurrentFiringSpread + (WeaponConfiguration->Spread / 10.0f));
	}
}

void AShooterWeapon::FireWeapon_Shotgun()
{
	const auto Config = GetWeaponConfiguration();
	
	const int32 RandomSeed = FMath::Rand();
	FRandomStream WeaponRandomStream(RandomSeed);
	const float CurrentSpread = GetCurrentSpread();
	const float ConeHalfAngle = FMath::DegreesToRadians((Config->BulletsGrouping + CurrentSpread) * 0.5f);
	
	FVector AimDir = GetAdjustedAim();
	
	ApplyOffsetIfAI(AimDir);
	
	const int32 CountShots = FMath::RandRange(Config->BulletsMin, Config->BulletsMax);
	const FVector StartTrace = GetCameraDamageStartLocation(AimDir);
	
	TArray<FInstantShootData> ShotgunShootData;
	
	for (SSIZE_T i = 0; i < CountShots; ++i)
	{
		const FVector ShootDir = WeaponRandomStream.VRandCone(AimDir, ConeHalfAngle, ConeHalfAngle);
		const FVector EndTrace = StartTrace + ShootDir * (Config->Range * 100.0f);
	
		const FHitResult Impact = WeaponTrace(StartTrace, EndTrace);
	
		ShotgunShootData.Add(FInstantShootData(Impact, ShootDir, FMath::Lerp<FVector>(Impact.ImpactPoint, EndTrace, .0001f)));
	}
	
	ProcessInstantHit(ShotgunShootData);
	CurrentFiringSpread = FMath::Min(Config->Spread, CurrentFiringSpread + (Config->Spread / 10.0f));
}

void AShooterWeapon::FireWeapon_Projectile()
{
	FVector ShootDir = GetAdjustedAim();
	FVector Origin = GetMuzzleLocation();

	ApplyOffsetIfAI(ShootDir);

	// trace from camera to check what's under crosshair
	const float ProjectileAdjustRange = 10000.0f;
	const FVector StartTrace = GetCameraDamageStartLocation(ShootDir);
	FVector EndTrace = StartTrace + (ShootDir * ProjectileAdjustRange);

	FHitResult Impact = WeaponTrace(StartTrace, EndTrace);

	// and adjust directions to hit that actor
	if (Impact.bBlockingHit)
	{
		const FVector AdjustedDir = (Impact.ImpactPoint - Origin).GetSafeNormal();
		bool bWeaponPenetration = false;

		const float DirectionDot = FVector::DotProduct(AdjustedDir, ShootDir);
		if (DirectionDot < 0.0f)
		{
			// shooting backwards = weapon is penetrating
			bWeaponPenetration = true;
		}
		else if (DirectionDot < 0.5f)
		{
			// check for weapon penetration if angle difference is big enough
			// raycast along weapon mesh to check if there's blocking hit

			FVector MuzzleStartTrace = Origin - GetMuzzleDirection() * 150.0f;
			FVector MuzzleEndTrace = Origin;
			FHitResult MuzzleImpact = WeaponTrace(MuzzleStartTrace, MuzzleEndTrace);

			if (MuzzleImpact.bBlockingHit)
			{
				bWeaponPenetration = true;
			}
		}

		if (bWeaponPenetration)
		{
			// spawn at crosshair position
			Origin = Impact.ImpactPoint - ShootDir * 10.0f;
		}
		else
		{
			// adjust direction to hit
			ShootDir = AdjustedDir;
		}
	}

	//DrawDebugLine(GetWorld(), Origin, Impact.Location, FColor::Cyan, false, 10.0f);

	ServerFireProjectile(Origin, ShootDir);
}

bool AShooterWeapon::ServerNotifyInstantShoot_Validate(const TArray<FInstantShootData>& InShootData)
{
	return true;
}

void AShooterWeapon::ServerNotifyInstantShoot_Implementation(const TArray<FInstantShootData>& InShootData)
{
	const float WeaponAngleDot = FMath::Abs(FMath::Sin(GetCurrentSpread() * PI / 180.f));

	if (IsValidObject(GetInstigator()) && InShootData.Num())
	{
		TArray<FInstantShootData> ShootDataConfirmed;
		const FVector Origin = GetMuzzleLocation();

		for (auto ShootData : InShootData)
		{			
			if (ShootData.Impact.GetActor())
			{				
				const FVector ViewDir = (ShootData.TraceEnd - Origin).GetSafeNormal();
				const float ViewDotHitDir = FVector::DotProduct(GetInstigator()->GetViewRotation().Vector(), ViewDir);
				bool bEqualViewDot = (ViewDotHitDir > .8f - WeaponAngleDot);
				if (bEqualViewDot == false)
				{
					const FVector OriginCamera = GetCameraDamageStartLocation(ViewDir);
					const FVector ViewDirCamera = (ShootData.TraceEnd - OriginCamera).GetSafeNormal();
					const float ViewDotHitDirCamera = FVector::DotProduct(GetInstigator()->GetViewRotation().Vector(), ViewDirCamera);
					bEqualViewDot = (ViewDotHitDirCamera > .8f - WeaponAngleDot);
				}

				if (bEqualViewDot)
				{
					if (CurrentState != EWeaponState::Idle || GetWeaponConfiguration()->IsAutomaticFire == false)
					{
						if (ShootData.Impact.GetActor()->IsRootComponentStatic() || ShootData.Impact.GetActor()->IsRootComponentStationary())
						{
							ShootDataConfirmed.Add(ShootData);
						}
						else
						{
							// Get the component bounding box
							const FBox HitBox = ShootData.Impact.GetActor()->GetComponentsBoundingBox();

							// calculate the box extent, and increase by a leeway
							FVector BoxExtent = 0.5 * (HitBox.Max - HitBox.Min);
							BoxExtent *= 200.0f;

							// avoid precision errors with really thin objects
							BoxExtent.X = FMath::Max(20.0f, BoxExtent.X);
							BoxExtent.Y = FMath::Max(20.0f, BoxExtent.Y);
							BoxExtent.Z = FMath::Max(20.0f, BoxExtent.Z);

							// Get the box center
							const FVector BoxCenter = (HitBox.Min + HitBox.Max) * 0.5;

							// if we are within client tolerance
							if (FMath::Abs(ShootData.Impact.Location.Z - BoxCenter.Z) < BoxExtent.Z &&
								FMath::Abs(ShootData.Impact.Location.X - BoxCenter.X) < BoxExtent.X &&
								FMath::Abs(ShootData.Impact.Location.Y - BoxCenter.Y) < BoxExtent.Y)
							{
								ShootDataConfirmed.Add(ShootData);
							}
							else
							{
								UE_LOG(LogShooterWeapon, Error, TEXT("%s Rejected client side hit of %s (outside bounding box tolerance)"), *GetNameSafe(this), *GetNameSafe(ShootData.Impact.GetActor()));
							}
						}
					}
				}
				else
				{
					UE_LOG(LogShooterWeapon, Error, TEXT("%s Rejected client side hit of %s (outside view cone tolerance)"), *GetNameSafe(this), *GetNameSafe(ShootData.Impact.GetActor()));
				}
			}
			else
			{
				ShootDataConfirmed.Add(ShootData);
			}
		}

		ProcessInstantHit_Confirmed(ShootDataConfirmed);
	}
}

void AShooterWeapon::ProcessInstantHit(const TArray<FInstantShootData>& InShootData)
{
	if (IsLocallyControlled() && GetNetMode() == NM_Client)
	{
		ServerNotifyInstantShoot(InShootData);
	}

	ProcessInstantHit_Confirmed(InShootData);
}

void AShooterWeapon::ProcessInstantHit_Confirmed(const TArray<FInstantShootData>& InShootData)
{
	if (GetLocalRole() == ROLE_Authority)
	{
		DealDamage(InShootData);
		ShootNotifyInstant = InShootData;

		//// TODO: Требуется рефакторинг
		//if (GetInstance() && MyPawn)
		//{
		//	for (auto ShootData : ShootNotifyInstant)
		//	{
		//		if (ShootData.Impact.PhysMaterial.IsValid() && ShootData.Impact.PhysMaterial.Get())
		//		{
		//			UPhysicalMaterial* HitPhysMat = ShootData.Impact.PhysMaterial.Get();
		//			AActor* HitActor = ShootData.Impact.GetActor();
		//			EPhysicalSurface HitSurfaceType = UPhysicalMaterial::DetermineSurfaceType(HitPhysMat);

		//			if (auto MyState = GetValidObjectAs<AShooterPlayerState>(MyPawn->GetPlayerState()))
		//			{
		//				//FPlayerShootData PlayerShootData;
		//				
		//				//PlayerShootData.Weapon = FWeaponHelper(GetInstanceEntity());
		//				//PlayerShootData.Surface = HitSurfaceType;
		//				//
		//				////if (auto HitPawn = GetValidObjectAs<AShooterCharacter>(HitActor))
		//				////{
		//				////	auto MyGameState = GetWorld()->GetGameState<AUTGameState>();
		//				////	auto HitState = GetValidObjectAs<AShooterPlayerState>(HitPawn->GetPlayerState());
		//				////
		//				////	if (MyGameState && HitState)
		//				////	{
		//				////		PlayerShootData.IsPawn_Friendly = (MyGameState && FFlagsHelper::HasAnyFlags(MyGameState->GetGameModeType().Value, EGameMode::LostDeadMatch.ToFlag() | EGameMode::DuelMatch.ToFlag())) ? false : MyGameState->OnSameTeam(HitState, MyState);
		//				////		PlayerShootData.IsPawn_Enemy = (MyGameState && FFlagsHelper::HasAnyFlags(MyGameState->GetGameModeType().Value, EGameMode::LostDeadMatch.ToFlag() | EGameMode::DuelMatch.ToFlag())) ? true : MyGameState->OnSameTeam(HitState, MyState) == false;
		//				////	}
		//				////}
		//				//
		//				//MyState->AddShootData(PlayerShootData);
		//			}
		//		}
		//	}
		//}
	}

	// play FX locally
	if (GetNetMode() != NM_DedicatedServer)
	{
		for (auto ShootData : InShootData)
		{
			FHitResult Impact = WeaponTrace(GetMuzzleLocation(), ShootData.TraceEnd);
			if (Impact.bBlockingHit)
			{
				SpawnImpactEffects(Impact);
				SpawnTrailEffect(Impact.ImpactPoint);
			}
			else
			{
				SpawnTrailEffect(ShootData.TraceEnd);
			}
		}
	}
}

bool AShooterWeapon::ShouldDealDamage(AActor* TestActor) const
{
	if (IsValidObject(TestActor))
	{
		return (GetNetMode() != NM_Client || TestActor->GetLocalRole() == ROLE_Authority || TestActor->GetTearOff());
	}

	return false;
}

float AShooterWeapon::GetWeaponDamage() const
{
	if (OverridedDamage < 1)
	{
		if (auto WeaponConfiguration = GetWeaponConfiguration())
		{
			return WeaponConfiguration->Damage;
		}
	}

	return OverridedDamage;
}

void AShooterWeapon::DealDamage(const TArray<FInstantShootData>& InShootData)
{
	TMap<AActor*, float> DamageMap;
	TMap<AActor*, FHitResult> ImpactMap;

	for (auto ShootData : InShootData)
	{
		if (ShootData.Impact.GetActor())
		{
			float& Value = DamageMap.FindOrAdd(ShootData.Impact.GetActor());
			Value += GetWeaponDamage();

			ImpactMap.Add(ShootData.Impact.GetActor(), ShootData.Impact);
		}
	}

	auto PawnOwner = GetPawnOwner();
	{
		for (auto actor : DamageMap)
		{
			FPointDamageEvent PointDmg;
			PointDmg.DamageTypeClass = GetWeaponConfiguration()->DamageType;
			PointDmg.HitInfo = ImpactMap[actor.Key];
			PointDmg.ShotDirection = InShootData[0].ShootDir;
			PointDmg.Damage = actor.Value;

			if (actor.Key)
			{
				actor.Key->TakeDamage(PointDmg.Damage, PointDmg, PawnOwner->Controller, this);
			}
		}
	}
}

void AShooterWeapon::OnRep_ShootNotifyInstant()
{
	for (auto ShootData : ShootNotifyInstant)
	{
		FHitResult Impact = WeaponTrace(GetMuzzleLocation(), ShootData.TraceEnd);
		if (Impact.bBlockingHit)
		{
			SpawnImpactEffects(Impact);
			SpawnTrailEffect(Impact.ImpactPoint);
		}
		else
		{
			SpawnTrailEffect(ShootData.TraceEnd);
		}
	}
}

void AShooterWeapon::SpawnImpactEffects(const FHitResult& Impact)
{
	TSubclassOf<AShooterImpactEffect> ImpactEffect;

	if (GetWeaponProperty() && GetWeaponProperty()->ImpactEffect)
	{
		ImpactEffect = GetWeaponProperty()->ImpactEffect;
	}

	if (ImpactEffect && Impact.bBlockingHit)
	{
		FHitResult UseImpact = Impact;

		// trace again to find component lost during replication
		if (!Impact.Component.IsValid())
		{
			const FVector StartTrace = Impact.ImpactPoint + Impact.ImpactNormal * 10.0f;
			const FVector EndTrace = Impact.ImpactPoint - Impact.ImpactNormal * 10.0f;
			FHitResult Hit = WeaponTrace(StartTrace, EndTrace);
			UseImpact = Hit;
		}

		FTransform const SpawnTransform(Impact.ImpactNormal.Rotation(), Impact.ImpactPoint);
		AShooterImpactEffect* EffectActor = GetWorld()->SpawnActorDeferred<AShooterImpactEffect>(ImpactEffect, SpawnTransform);

		if (EffectActor)
		{
			EffectActor->SurfaceHit = UseImpact;
			UGameplayStatics::FinishSpawningActor(EffectActor, SpawnTransform);
		}
	}
}

bool AShooterWeapon::IsFirstPerson() const
{
	return GetPawnOwner() && GetPawnOwner()->IsFirstPerson();
}

bool AShooterWeapon::IsValidPawnOwner() const
{
	return GetPawnOwner() != nullptr;
}

bool AShooterWeapon::IsValidPawnPlayerState() const
{
	return GetPawnPlayerState() != nullptr;
}

void AShooterWeapon::SpawnTrailEffect(const FVector& EndPoint)
{
	UParticleSystem* TraceFX = nullptr;
	FVector TraceFXScale = FVector::OneVector;

	if (GetWeaponProperty() && GetWeaponProperty()->TraceFX)
	{
		TraceFX = GetWeaponProperty()->GetTraceFX();
	}

	if (IsValidObject(TraceFX) && IsValidPawnOwner())
	{
		auto Trans = GetMuzzleSocketComponent(IsFirstPerson())->GetSocketTransform(GetMuzzleSocketName());

		UParticleSystemComponent* TrailPSC = UGameplayStatics::SpawnEmitterAtLocation(GetWorld(), TraceFX, Trans);
		if (TrailPSC)
		{
			TrailPSC->SetRelativeScale3D(TraceFXScale);
			TrailPSC->SetVectorParameter(TEXT("HitLocation"), EndPoint);
		}
	}
}

float AShooterWeapon::GetCurrentSpread() const
{
	auto PawnOwner = GetPawnOwner();
	float FinalSpread = GetWeaponConfiguration()->Spread + CurrentFiringSpread;

	if (PawnOwner && PawnOwner->bIsCrouched)
	{
		FinalSpread *= .8f;
	}

	if (PawnOwner && PawnOwner->IsTargeting())
	{
		FinalSpread *= .5f;
	}

	return FinalSpread;
}

bool AShooterWeapon::ServerFireProjectile_Validate(FVector Origin, FVector_NetQuantizeNormal ShootDir)
{
	return true;
}

void AShooterWeapon::ServerFireProjectile_Implementation(FVector Origin, FVector_NetQuantizeNormal ShootDir)
{
	//if (GetInstance() && GetCurrentAmmoInstance())
	//{
	//	auto ProjTemplate = GetCurrentAmmoInstance()->GetPropertyProjectile().Projectile;
	//	FTransform ProjTransform(FRotationMatrix::MakeFromX(ShootDir).Rotator(), Origin);
	//
	//	AUTProjectile* Projectile = GetWorld()->SpawnActorDeferred<AUTProjectile>(ProjTemplate, ProjTransform, this, Instigator, ESpawnActorCollisionHandlingMethod::AlwaysSpawn);
	//
	//	auto localEntityInstance = GetInstanceEntity();
	//	auto localWeaponInstance = GetInstance();
	//	auto localAmmoInstance = GetCurrentAmmoInstance();
	//
	//	if (Projectile && localEntityInstance && localWeaponInstance && localAmmoInstance)
	//	{
	//		Projectile->SetIgnoreActor(this);
	//		Projectile->SetIgnoreActor(GetOwner());
	//		Projectile->ShooterLocation = Origin;
	//
	//		Projectile->DamageParams.BaseDamage = localWeaponInstance->GetProperty().Damage;
	//		Projectile->DamageParams.OuterRadius = localAmmoInstance->GetPropertyProjectile().Radius * 100.0f;
	//		Projectile->DamageParams.InnerRadius = Projectile->DamageParams.OuterRadius / 3.5f;
	//
	//		Projectile->OwnerGategory = localEntityInstance->GetDescription().CategoryType;
	//		Projectile->OwnerModel = localEntityInstance->GetModelId();
	//
	//		Projectile->FinishSpawning(ProjTransform);
	//	}
	//}
}

UWeaponItemEntity* AShooterWeapon::GetInstanceEntity() const
{
	if (auto instance = GetInstance())
	{
		return instance->GetEntity<UWeaponItemEntity>();
	}
	return nullptr;
}

const FWeaponConfiguration* AShooterWeapon::GetWeaponConfiguration() const
{
	if (auto instance = GetInstanceEntity())
	{
		return &instance->GetWeaponConfiguration();
	}
	return nullptr;
}

const FWeaponItemProperty* AShooterWeapon::GetWeaponProperty() const
{
	if (auto instance = GetInstanceEntity())
	{
		return &instance->GetWeaponProperty();
	}
	return nullptr;
}

FWeaponItemProperty* AShooterWeapon::GetWeaponProperty()
{
	if (auto instance = GetInstanceEntity())
	{
		return &instance->GetWeaponProperty();
	}
	return nullptr;
}

int32 AShooterWeapon::GetModelId() const
{
	if(const auto property = GetWeaponProperty())
	{
		return  property->ModelId;
	}
	return -1;
}

EGameItemType AShooterWeapon::GetItemType() const
{
	if (const auto enity = GetInstanceEntity())
	{
		return enity->GetItemType();
	}
	return EGameItemType::None;
}

int32 AShooterWeapon::GetLevel() const
{
	if (const auto instance = GetInstance())
	{
		return instance->GetLevel();
	}
	return 0;
}
