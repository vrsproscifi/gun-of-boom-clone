#include "ShooterGame.h"
#include "ShooterArmour.h"
#include "ArmourPlayerItem.h"
#include "ArmourItemEntity.h"

UShooterArmour::UShooterArmour(const FObjectInitializer& ObjectInitializer)
	: Super(ObjectInitializer)
{

}

void UShooterArmour::GetLifetimeReplicatedProps(TArray< FLifetimeProperty > & OutLifetimeProps) const
{
	Super::GetLifetimeReplicatedProps(OutLifetimeProps);

	DOREPLIFETIME(UShooterArmour, CurrentArmour);
}

void UShooterArmour::OnRep_Instance()
{
	Super::OnRep_Instance();
	if (auto property = GetArmourProperty())
	{
		MaximumArmour = property->Armour;
		CurrentArmour = property->Armour;
	}
}

float UShooterArmour::TakeDamage(const float& InDamage, float& OutDamageToThis)
{
	//===============================
	const float damage = InDamage * 0.8f;

	//===============================
	if (CurrentArmour >= damage)
	{
		OutDamageToThis += damage;
		CurrentArmour -= damage;		
		return 0;
	}

	//===============================
	const float result = FMath::Max(0.0f, InDamage - CurrentArmour);

	//===============================
	CurrentArmour = 0;

	//===============================
	return result;
}

const FArmourItemProperty* UShooterArmour::GetArmourProperty() const
{
	return GetItemProperty<FArmourItemProperty>();
}
