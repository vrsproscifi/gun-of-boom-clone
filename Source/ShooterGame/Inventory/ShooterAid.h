#pragma once

//==========================================
#include "ExecutableGamePlayerItem.h"


//==========================================
#include "ShooterAid.generated.h"

//==========================================
struct FAidItemProperty;

//==========================================
UCLASS(Blueprintable)
class UShooterAid : public UExecutableGamePlayerItem
{
	GENERATED_UCLASS_BODY()

protected:
	//----------------------------------------
	UPROPERTY(Transient, VisibleInstanceOnly)
	UParticleSystemComponent* ProcessPSC;

	FTimerHandle Handle_ActivationDelay;
	float CurrentTime;
	//----------------------------------------
	virtual void OnProcess() override;
	virtual void StopTimer() override;
	virtual bool IsAllowExecuteProcess() const override;
	virtual void OnRep_Activated() override;
	virtual	void OnRep_Instance() override;

	void TickComponent(float DeltaTime, ELevelTick TickType, FActorComponentTickFunction* ThisTickFunction) override;
public:
	virtual bool IsActivated() const override;
	virtual	void InitializeInstance(UBasicPlayerItem* InInstance) override;
	virtual bool IsAllowStartProcess() const override;
	virtual bool StartProcess() override;
	virtual bool StopProcess() override;
	virtual float GetExecuteInterval() const override;


	const FAidItemProperty* GetAidProperty() const;
};