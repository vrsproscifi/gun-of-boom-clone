//==============================
#include "ShooterGame.h"

//==============================
#include "GrenadeProjectile.h"

//==============================
#include "Particles/ParticleSystemComponent.h"

//==============================
#include "GrenadeItemEntity.h"

//==============================
#include "GameSingletonExtensions.h"


// Sets default values
AGrenadeProjectile::AGrenadeProjectile()
	: Super()
	, bExploded(false)
{
 	// Set this actor to call Tick() every frame.  You can turn this off to improve performance if you don't need it.
	PrimaryActorTick.bCanEverTick = true;

	CapsuleComp = CreateDefaultSubobject<UCapsuleComponent>(TEXT("CapsuleComp"));
	CapsuleComp->bTraceComplexOnMove = true;
	CapsuleComp->SetCollisionEnabled(ECollisionEnabled::QueryAndPhysics);
	CapsuleComp->SetCollisionObjectType(COLLISION_WEAPON);
	CapsuleComp->SetCollisionResponseToAllChannels(ECR_Block);

	SetRootComponent(CapsuleComp);

	MeshComp = CreateDefaultSubobject<USkeletalMeshComponent>(TEXT("MeshComp"));
	MeshComp->SetCollisionEnabled(ECollisionEnabled::NoCollision);
	MeshComp->SetupAttachment(GetRootComponent());
	MeshComp->bAllowAnyoneToDestroyMe = false;
	MeshComp->bDisableClothSimulation = true;
	MeshComp->CastShadow = false;
	MeshComp->bAffectDynamicIndirectLighting = false;
	MeshComp->bAffectDistanceFieldLighting = false;
	MeshComp->bCastStaticShadow = false;
	MeshComp->bCastDynamicShadow = false;
	MeshComp->bCastVolumetricTranslucentShadow = false;
	MeshComp->bSelfShadowOnly = true;
	MeshComp->bReceiveMobileCSMShadows = false;
	MeshComp->bDisableMorphTarget = true;
	MeshComp->bPerBoneMotionBlur = false;
	MeshComp->bRenderStatic = true;
	MeshComp->bPauseAnims = true;

	MovementComp = CreateDefaultSubobject<UProjectileMovementComponent>(TEXT("MovementComp"));
	MovementComp->UpdatedComponent = GetRootComponent();
	MovementComp->InitialSpeed = 2000.0f;
	MovementComp->MaxSpeed = 10000.0f;
	MovementComp->bRotationFollowsVelocity = false;
	MovementComp->bInitialVelocityInLocalSpace = true;
	MovementComp->ProjectileGravityScale = 0.5f;
	MovementComp->bShouldBounce = true;
	MovementComp->bBounceAngleAffectsFriction = true;
	MovementComp->Bounciness = .2f;
	MovementComp->Friction = 1.0f;
	MovementComp->BounceVelocityStopSimulatingThreshold = .1f;
	MovementComp->Velocity = FVector(1000.0f, -0.4f, 0.0f);
	MovementComp->bAllowAnyoneToDestroyMe = false;
	MovementComp->bAutoActivate = true;
	MovementComp->OnProjectileBounce.AddDynamic(this, &AGrenadeProjectile::OnBounce);
	
	bReplicates = true;
	SetReplicatingMovement(true);
}

void AGrenadeProjectile::PostInitializeComponents()
{
	Super::PostInitializeComponents();

	CapsuleComp->IgnoreActorWhenMoving(GetInstigator(), true);
}

// Called when the game starts or when spawned
void AGrenadeProjectile::BeginPlay()
{
	Super::BeginPlay();	
}

// Called every frame
void AGrenadeProjectile::Tick( float DeltaTime )
{
	Super::Tick( DeltaTime );

}

void AGrenadeProjectile::GetLifetimeReplicatedProps(TArray< FLifetimeProperty > & OutLifetimeProps) const
{
	Super::GetLifetimeReplicatedProps(OutLifetimeProps);

	DOREPLIFETIME(AGrenadeProjectile, ModelId);
	DOREPLIFETIME(AGrenadeProjectile, bExploded);
}

void AGrenadeProjectile::SetInstance(UGrenadeItemEntity* InInstance)
{
	Instance = InInstance;
	if(Instance)
	{
		ModelId = Instance->GetItemModelId();
	}
	OnRep_Instance();
}

UGrenadeItemEntity* AGrenadeProjectile::GetInstanceEntity() const
{
	return Instance;
}

UPrimitiveComponent* AGrenadeProjectile::GetMovableComponent() const
{
	return CapsuleComp;
}

void AGrenadeProjectile::OnRep_Instance()
{
	Instance = FGameSingletonExtensions::FindItemById<UGrenadeItemEntity>(ModelId);
	if (Instance)
	{
		MeshComp->SetSkeletalMesh(Instance->GetSkeletalMesh());

		if (GetLocalRole() == ROLE_Authority)
		{
			if (Instance->GetGrenadeProperty().DetonationDelay > .0f)
			{
				GetWorldTimerManager().SetTimer(timer_Detonation, FTimerDelegate::CreateUObject(this, &AGrenadeProjectile::Explode), Instance->GetGrenadeProperty().DetonationDelay, false);
			}
		}
	}
}

void AGrenadeProjectile::OnBounce(const FHitResult& ImpactResult, const FVector& ImpactVelocity)
{
	if (GetLocalRole() == ROLE_Authority)
	{
		if (!timer_Phisycs.IsValid())
		{
			GetWorldTimerManager().SetTimer(timer_Phisycs, FTimerDelegate::CreateLambda([&]() {
				CapsuleComp->SetSimulatePhysics(true);
				CapsuleComp->SetAngularDamping(.01f);
				CapsuleComp->SetEnableGravity(true);
			}), .1f, false);
		}

		if (Instance->GetGrenadeProperty().DetonationDelay <= 0.1f && bExploded == false)
		{
			Explode();
		}
	}
}

void AGrenadeProjectile::Explode()
{
	if (GetLocalRole() == ROLE_Authority)
	{
		if (Instance)
		{
			bExploded = true;

			const auto Property = Instance->GetGrenadeProperty();

			TArray<AActor*> IgnoreActors;
			UGameplayStatics::ApplyRadialDamage(GetWorld(), Property.Damage, GetActorLocation(), Property.DamageRadius, Property.DamageType, IgnoreActors, this, GetInstigator()->GetController(), false, /*COLLISION_PROJECTILE*/ COLLISION_WEAPON);
		}

		SetLifeSpan(2.0f);
	}
}

void AGrenadeProjectile::OnRep_Explode()
{
	if (Instance)
	{
		auto Property = Instance->GetGrenadeProperty();

		if (Property.ExplosionFX)
		{
			UGameplayStatics::SpawnEmitterAtLocation(this, Property.ExplosionFX, MeshComp->GetComponentLocation(), MeshComp->GetComponentRotation());
		}

		if (Property.GetExplosionSound())
		{
			UGameplayStatics::PlaySoundAtLocation(this, Property.GetExplosionSound(), MeshComp->GetComponentLocation());
		}

		MeshComp->SetVisibility(false);
		CapsuleComp->SetCollisionEnabled(ECollisionEnabled::NoCollision);
	}
}