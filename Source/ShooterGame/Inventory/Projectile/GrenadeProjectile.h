// VRSPRO

#pragma once
#include "Components/CapsuleComponent.h"

#include "GameFramework/Actor.h"
#include "GameFramework/ProjectileMovementComponent.h"
#include "GrenadeProjectile.generated.h"

class UGrenadeItemEntity;

UCLASS()
class AGrenadeProjectile : public AActor
{
	GENERATED_BODY()
	
public:	
	// Sets default values for this actor's properties
	AGrenadeProjectile();

	/** initial setup */
	virtual void PostInitializeComponents() override;

	// Called when the game starts or when spawned
	virtual void BeginPlay() override;
	
	// Called every frame
	virtual void Tick( float DeltaSeconds ) override;

	void Explode();

	UFUNCTION()
	void OnBounce(const FHitResult& ImpactResult, const FVector& ImpactVelocity);

	UFUNCTION()
	void SetInstance(UGrenadeItemEntity* InInstance);

	UFUNCTION(BlueprintCallable, BlueprintPure, Category = Instance)
	UGrenadeItemEntity* GetInstanceEntity() const;

	UFUNCTION(BlueprintCallable, BlueprintPure, Category = Projectile)
	UPrimitiveComponent* GetMovableComponent() const;

protected:

	UPROPERTY(VisibleDefaultsOnly, Category = Projectile)
	USkeletalMeshComponent* MeshComp;

	UPROPERTY(VisibleDefaultsOnly, Category = Projectile)
	UProjectileMovementComponent* MovementComp;

	UPROPERTY(VisibleDefaultsOnly, Category = Projectile)
	UCapsuleComponent* CapsuleComp;

	UPROPERTY(Transient)
	UGrenadeItemEntity* Instance;

	UPROPERTY(Transient, ReplicatedUsing = OnRep_Instance)
	uint32 ModelId;

	UFUNCTION()
	void OnRep_Instance();

	FTimerHandle timer_Detonation;
	FTimerHandle timer_Phisycs;

	UPROPERTY(Transient, ReplicatedUsing = OnRep_Explode)
	uint32 bExploded : 1;

	UFUNCTION()
	void OnRep_Explode();

};
