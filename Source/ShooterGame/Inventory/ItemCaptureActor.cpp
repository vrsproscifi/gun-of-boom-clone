// Fill out your copyright notice in the Description page of Project Settings.

#include "ShooterGame.h"
#include "ItemCaptureActor.h"
#include "Decorators/DecoratorHelpers.h"

AItemCaptureActor::AItemCaptureActor()
{
	SceneComponent = CreateDefaultSubobject<USceneComponent>(TEXT("SceneComponent"));
	SetRootComponent(SceneComponent);

	RotateComponent = CreateDefaultSubobject<USceneComponent>(TEXT("RotateComponent"));
	RotateComponent->SetupAttachment(GetRootComponent());

	PreviewMeshComponent = CreateDefaultSubobject<USkeletalMeshComponent>(TEXT("PreviewMeshComponent"));
	PreviewMeshComponent->SetupAttachment(RotateComponent);	

	CaptureComponent = CreateDefaultSubobject<USceneCaptureComponent2D>(TEXT("CaptureComponent"));
	CaptureComponent->SetupAttachment(GetRootComponent());
	CaptureComponent->ShowOnlyComponent(PreviewMeshComponent);

	RotationMovement = CreateDefaultSubobject<URotatingMovementComponent>(TEXT("RotationMovement"));
	RotationMovement->bRotationInLocalSpace = true;

	PrimaryActorTick.bCanEverTick = true;
}

void AItemCaptureActor::SetPreviewMesh(USkeletalMesh* InMesh, const FPreviewMeshSettings& InSettings)
{
	PreviewMeshComponent->SetSkeletalMesh(InMesh);
	PreviewMeshComponent->SetRelativeTransform(InSettings.Mesh);
	RotateComponent->SetRelativeTransform(InSettings.Pivot);
	CaptureComponent->SetRelativeTransform(InSettings.Camera);
	PreviewMeshComponent->SetForcedLOD(1);
	PreviewMeshComponent->bForceMipStreaming = true;
	PreviewMeshComponent->SetTextureForceResidentFlag(true);

	ToggleRotationMovement(true);
}

void AItemCaptureActor::ToggleRotationMovement(bool InToggle)
{
	RotationMovement->SetActive(InToggle, true);

	if (InToggle)
	{
		RotationMovement->SetUpdatedComponent(RotateComponent);
	}
	else
	{
		RotationMovement->SetUpdatedComponent(nullptr);
	}
}
