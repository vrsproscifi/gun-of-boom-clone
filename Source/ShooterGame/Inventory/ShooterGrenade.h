#pragma once

//==========================================
#include "ExecutableGamePlayerItem.h"

//==========================================
#include "ShooterGrenade.generated.h"

//==========================================
struct FGrenadeItemProperty;

	
//==========================================
UCLASS(Blueprintable)
class UShooterGrenade : public UExecutableGamePlayerItem
{
	GENERATED_UCLASS_BODY()

protected:
	//----------------------------------------
	virtual void OnRep_Activated() override;
	virtual void OnProcess() override;
	virtual bool IsAllowExecuteProcess() const override;
	virtual void BeginDestroy() override;

	UFUNCTION() void OnAnimNotify(FName InNotifyName, USkeletalMeshComponent* InMesh, UAnimSequenceBase* InAnimation);

	float GetMontageEventTime(UAnimMontage* InTargetMontage, const FName& InEventName) const;

public:
	virtual bool StartProcess() override;
	virtual bool StopProcess() override;
	virtual	void InitializeInstance(UBasicPlayerItem* InInstance) override;

	virtual float GetExecuteInterval() const override;

	const FGrenadeItemProperty* GetGrenadeProperty() const;
	FGrenadeItemProperty* GetGrenadeProperty();
};