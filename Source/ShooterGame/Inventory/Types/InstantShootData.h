#pragma once
//==========================================
#include "Engine/EngineTypes.h"

//==========================================
#include "InstantShootData.generated.h"

//==========================================

USTRUCT()
struct FInstantShootData
{
	GENERATED_USTRUCT_BODY();

	UPROPERTY()		FHitResult						Impact;
	UPROPERTY()		FVector_NetQuantizeNormal		ShootDir;
	UPROPERTY()		FVector							TraceEnd;

	FInstantShootData() : Impact(), ShootDir(FVector::ZeroVector), TraceEnd(FVector::ZeroVector) {}
	FInstantShootData(const FHitResult& InImpact, const FVector& InShootDir, const FVector& InTraceEnd) : Impact(InImpact), ShootDir(InShootDir), TraceEnd(InTraceEnd) {}
};
