#pragma once

//==========================================
#include "FeedbackData.generated.h"

//==========================================

USTRUCT()
struct FFeedbackData
{
	GENERATED_USTRUCT_BODY();

	UPROPERTY()
		FVector2D Feedback;

	UPROPERTY()
		float ModiferCrouched;

	UPROPERTY()
		float ModiferTargeting;
};