#pragma once

//==========================================
#include "BasicGamePlayerItem.h"

//==========================================
#include "Item/BasicItemInstance.h"

//==========================================
#include "ShooterArmour.generated.h"

//==========================================
class UArmourPlayerItem;
class UArmourItemEntity;
struct FArmourItemProperty;

//==========================================
UCLASS(Blueprintable)
class UShooterArmour : public UBasicGamePlayerItem
{
	GENERATED_UCLASS_BODY()

private:
	UPROPERTY(VisibleInstanceOnly)
		float MaximumArmour;

	UPROPERTY(Replicated, VisibleInstanceOnly)
		float CurrentArmour;

	virtual void OnRep_Instance() override final;

public:

	UFUNCTION(BlueprintCallable, Category = Inventory)
	const float& GetMaximumArmour() const
	{
		return MaximumArmour;
	}

	UFUNCTION(BlueprintCallable, Category = Inventory)
	const float& GetCurrentArmour() const
	{
		return CurrentArmour;
	}

	const FArmourItemProperty* GetArmourProperty() const;

	float TakeDamage(const float& InDamage, float& OutDamageToThis);

};