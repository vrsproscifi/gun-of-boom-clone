#pragma once

//==========================================
#include "Item/BasicItemInstance.h"

//==========================================
#include "Engine/Animation/TwoAnimSequence.h"

//==========================================
#include "Inventory/Types/FeedbackData.h"
#include "Inventory/Types/InstantShootData.h"
#include "Inventory/Types/WeaponState.h"

//=============================================
#include <TimerManager.h>

//==========================================
#include "ShooterWeapon.generated.h"

//==========================================
class UWeaponPlayerItem;
class UWeaponItemEntity;

//==========================================
struct FWeaponConfiguration;
struct FWeaponItemProperty;

//==========================================
UCLASS(Blueprintable)
class AShooterWeapon : public AActor
{
	GENERATED_UCLASS_BODY()

	/** perform initial setup */
	virtual void PostInitializeComponents() override;
	virtual void BeginDestroy() override;

	virtual void Destroyed() override;

	//////////////////////////////////////////////////////////////////////////
	// Ammo
	
	/** [server] add ammo */
	void GiveAmmo(int AddAmount);

	/** [server] set ammo */
	void SetAmmo(int AddAmount);

	/** [server] get ammo */
	int32 GetAmmo() const;

	int32 GetLevel() const;

	/** consume a bullet */
	void UseAmmo();

	//////////////////////////////////////////////////////////////////////////
	// Inventory

	/** weapon is being equipped by owner pawn */
	virtual void OnEquip();

	/** weapon is now equipped by owner pawn */
	virtual void OnEquipFinished();

	/** weapon is holstered by owner pawn */
	virtual void OnUnEquip();

	/** [server] weapon was added to pawn's inventory */
	virtual void OnEnterInventory(AShooterCharacter* NewOwner);

	/** [server] weapon was removed from pawn's inventory */
	virtual void OnLeaveInventory();

	/** check if it's currently equipped */
	bool IsEquipped() const;

	/** check if mesh is already attached */
	bool IsAttachedToPawn() const;

	bool IsAllowFireOnDistance(const float& InTargetDistance) const;

	//////////////////////////////////////////////////////////////////////////
	// Input

	/** [local + server] start weapon fire */
	virtual void StartFire();

	/** [local + server] stop weapon fire */
	virtual void StopFire();

	/** [all] start weapon reload */
	virtual void StartReload(bool bFromReplication = false);

	/** [local + server] interrupt weapon reload */
	virtual void StopReload();

	/** [server] performs actual reload */
	virtual void ReloadWeapon();

	virtual void ForceReloadWeapon();


	/** trigger reload from server */
	UFUNCTION(reliable, client)
	void ClientStartReload();


	//////////////////////////////////////////////////////////////////////////
	// Control

	/** check if weapon can fire */
	bool CanFire() const;

	/** check if weapon can be reloaded */
	bool CanReload() const;


	//////////////////////////////////////////////////////////////////////////
	// Reading data

	/** get current weapon state */
	UFUNCTION(BlueprintCallable, BlueprintPure, Category = Gameplay)
	EWeaponState GetCurrentState() const;

	/** get current ammo amount (total) */
	int32 GetCurrentAmmo() const;

	/** get current ammo amount (clip) */
	int32 GetCurrentAmmoInClip() const;

	/** get clip size */
	int32 GetAmmoPerClip() const;

	/** get max ammo amount */
	int32 GetMaxAmmo() const;

	/** get weapon mesh (needs pawn owner to determine variant) */
	USkeletalMeshComponent* GetWeaponMesh() const;

	/** get pawn owner */
	UFUNCTION(BlueprintCallable, Category="Game|Weapon")
	class AShooterCharacter* GetPawnOwner() const;
	
	UFUNCTION(BlueprintCallable, Category = "Game|Weapon")
	AShooterPlayerState* GetPawnPlayerState() const;

	UFUNCTION(BlueprintCallable, Category = "Game|Weapon")
	AShooterAIController* GetPawnAIController() const;

	UFUNCTION(BlueprintCallable, Category = "Game|Weapon")
	AShooterPlayerController* GetPawnPlayerController() const;


	UFUNCTION(BlueprintCallable, Category = "Game|Weapon")
	bool IsValidPawnOwner() const;

	UFUNCTION(BlueprintCallable, Category = "Game|Weapon")
	bool IsValidPawnPlayerState() const;

	UFUNCTION(BlueprintCallable, Category = "Game|Weapon")
	bool IsLocallyControlled() const;

	UFUNCTION(BlueprintCallable, Category = "Game|Weapon")
	bool IsFirstPerson() const;

	/** check if weapon has infinite ammo (include owner's cheats) */
	bool HasInfiniteAmmo() const;

	/** check if weapon has infinite clip (include owner's cheats) */
	bool HasInfiniteClip() const;

	/** set the weapon's owning pawn */
	void SetOwningPawn(AShooterCharacter* AShooterCharacter);

	/** gets last time when this weapon was switched to */
	float GetEquipStartedTime() const;

	/** gets the duration of equipping weapon*/
	float GetEquipDuration() const;

public:

	UFUNCTION()
	virtual void OnRep_Instance();

	UFUNCTION(BlueprintCallable, Category = Inventory)
	void InitializeInstance(UWeaponPlayerItem* InInstance);

	UPROPERTY(ReplicatedUsing=OnRep_Instance)
	UWeaponPlayerItem* Instance;
	
	UFUNCTION(BlueprintCallable, BlueprintPure, Category = GamePlay)
	UWeaponPlayerItem* GetInstance() const;

	UFUNCTION(BlueprintCallable, BlueprintPure, Category = GamePlay)
	UWeaponItemEntity* GetInstanceEntity() const;

	const FWeaponConfiguration* GetWeaponConfiguration() const;

	const FWeaponItemProperty* GetWeaponProperty() const;
	FWeaponItemProperty* GetWeaponProperty();

	int32 GetModelId() const;

	EGameItemType GetItemType()  const;

	bool IsRequiredReload() const;

	/** pawn owner */
	UPROPERTY(Transient, ReplicatedUsing=OnRep_MyPawn)
	class AShooterCharacter* MyPawn;


	protected:
		UWorld* GetValidWorld() const;
		UWorld* GetValidWorld();

		//========================================================
#pragma region TimerManager
		FTimerManager* GetTimerManager() const;
		FTimerManager* GetTimerManager();
#pragma endregion

private:
	/** weapon mesh: 1st person view */
	UPROPERTY(VisibleDefaultsOnly, Category=Mesh)
	USkeletalMeshComponent* Mesh1P;

	/** weapon mesh: 3rd person view */
	UPROPERTY(VisibleDefaultsOnly, Category=Mesh)
		USkeletalMeshComponent* Mesh3P;
protected:

	/** firing audio (bLoopedFireSound set) */
	UPROPERTY(VisibleInstanceOnly, Transient)
	UAudioComponent* FireAC;

	/** spawned component for muzzle FX */
	UPROPERTY(Transient)
	UParticleSystemComponent* MuzzlePSC;

	/** spawned component for second muzzle FX (Needed for split screen) */
	UPROPERTY(Transient)
	UParticleSystemComponent* MuzzlePSCSecondary;



	/** is fire animation playing? */
	uint32 bPlayingFireAnim : 1;

	/** is weapon currently equipped? */
	uint32 bIsEquipped : 1;

	/** is weapon fire active? */
	uint32 bWantsToFire : 1;

	/** is reload animation playing? */
	UPROPERTY(Transient, ReplicatedUsing=OnRep_Reload)
	uint32 bPendingReload : 1;

	/** is equip animation playing? */
	uint32 bPendingEquip : 1;

	/** weapon is refiring */
	uint32 bRefiring : 1;

	/** current weapon state */
	EWeaponState CurrentState;

	/** time of last successful weapon fire */
	float LastFireTime;

	/** last time when this weapon was switched to */
	float EquipStartedTime;

	/** how much time weapon needs to be equipped */
	float EquipDuration;

	/** current ammo - inside clip */
	UPROPERTY(Transient, Replicated/*, ReplicatedUsing = OnRep_CountAmmo*/)
	uint16 CurrentAmmoInClip;

	UPROPERTY(Transient)
	uint16 TotalCountAmmo;

	/** burst counter, used for replicating fire events to remote clients */
	UPROPERTY(Transient, ReplicatedUsing=OnRep_BurstCounter)
	uint16 BurstCounter;

	/** Handle for efficient management of OnEquipFinished timer */
	FTimerHandle TimerHandle_OnEquipFinished;

	/** Handle for efficient management of StopReload timer */
	FTimerHandle TimerHandle_StopReload;

	/** Handle for efficient management of ReloadWeapon timer */
	FTimerHandle TimerHandle_ReloadWeapon;

	/** Handle for efficient management of HandleFiring timer */
	FTimerHandle TimerHandle_HandleFiring;

	//////////////////////////////////////////////////////////////////////////
	// Input - server side

	UPROPERTY(Replicated)
	bool bHasSwitctedToPistol;

	UFUNCTION()
	bool SwitchWeaponToPistol();

	
	UFUNCTION()
	void SwitchWeaponToPreview();


	UFUNCTION(reliable, server, WithValidation)
	void ServerStartFire();

	UFUNCTION(reliable, server, WithValidation)
	void ServerStopFire();

	UFUNCTION(reliable, server, WithValidation)
	void ServerStartReload();

	UFUNCTION(reliable, server, WithValidation)
	void ServerStopReload();


	//////////////////////////////////////////////////////////////////////////
	// Replication & effects

	UFUNCTION()
	void OnRep_MyPawn();

	UFUNCTION()
	void OnRep_BurstCounter();

	UFUNCTION()
	void OnRep_Reload();

	/** Called in network play to do the cosmetic fx for firing */
	virtual void SimulateWeaponFire();

	/** Called in network play to stop cosmetic fx (e.g. for a looping shot). */
	virtual void StopSimulatingWeaponFire();


	//////////////////////////////////////////////////////////////////////////
	// Weapon usage

	/** [local] weapon specific fire implementation */
	virtual void FireWeapon();

	/** [server] fire & update ammo */
	UFUNCTION(reliable, server, WithValidation)
	void ServerHandleFiring();

	/** [local + server] handle weapon fire */
	void HandleFiring();

	/** [local + server] firing started */
	virtual void OnBurstStarted();

	/** [local + server] firing finished */
	virtual void OnBurstFinished();

	/** update weapon state */
	void SetWeaponState(EWeaponState NewState);

	/** determine current weapon state */
	void DetermineWeaponState();


	//////////////////////////////////////////////////////////////////////////
	// Inventory

	/** attaches weapon mesh to pawn's mesh */
	void AttachMeshToPawn();

	/** detaches weapon mesh from pawn */
	void DetachMeshFromPawn();


	//////////////////////////////////////////////////////////////////////////
	// Weapon usage helpers
public:

	/** play weapon sounds */
	UAudioComponent* PlayWeaponSound(const TSoftObjectPtr<USoundCue>& InSound);
	UAudioComponent* PlayWeaponSound(USoundCue* Sound);

	/** play weapon animations */
	float PlayWeaponAnimation(FTwoAnimMontage& Animation);

	/** stop playing weapon animations */
	void StopWeaponAnimation(FTwoAnimMontage& Animation);

	/** Get the aim of the weapon, allowing for adjustments to be made by the weapon */
	virtual FVector GetAdjustedAim() const;

	/** Get the aim of the camera */
	FVector GetCameraAim() const;

	/** get the originating location for camera damage */
	FVector GetCameraDamageStartLocation(const FVector& AimDir) const;

	/** get the muzzle location of the weapon */
	FVector GetMuzzleLocation() const;

	/** get direction of weapon's muzzle */
	FVector GetMuzzleDirection() const;

	/** find hit */
	FHitResult WeaponTrace(const FVector& TraceFrom, const FVector& TraceTo) const;

	void ToggleAimingParams(const bool IsAiming);

	void ToggleSniperScope(const bool);

public:
	/** Returns Mesh1P subobject **/
	UFUNCTION(BlueprintCallable, BlueprintPure, Category = Mesh)
	USkeletalMeshComponent* GetMesh1P() const;

	/** Returns Mesh3P subobject **/
	UFUNCTION(BlueprintCallable, BlueprintPure, Category = Mesh)
		USkeletalMeshComponent* GetMesh3P() const;

	UFUNCTION(BlueprintCallable, BlueprintPure, Category = Mesh)
	float GetWeaponMass() const;

	virtual void ApplyOffsetIfAI(FVector &Origin);
	virtual float GetWeaponCoefficient() const;
	void PlayMontageWithDuration(USkeletalMeshComponent* PlayTarget, UAnimMontage* Montage, float Duration);
	void PlayMontageWithDuration(USkeletalMeshComponent* PlayTarget, const TSoftObjectPtr<UAnimMontage>& Montage, float Duration);

private:
	UPROPERTY(Transient, VisibleInstanceOnly)
	float OverridedDamage;

	void SetOverridedDamage(const float& InDamage)
	{
		OverridedDamage = InDamage;
	}

public:
	float GetWeaponDamage() const;


protected:

	virtual void TickActor(float DeltaTime, enum ELevelTick TickType, FActorTickFunction& ThisTickFunction) override;

	UPROPERTY()
	float TargetRecoilX;

	UPROPERTY()
	float TargetRecoilY;

	UPROPERTY(Replicated)
	uint32 FeedbackData;

	void ProccesFeedback(float DeltaTime);
	void MakeFeedback();

	UPROPERTY()
	bool bIsCancelReload;

public:

	UFUNCTION(BlueprintCallable, BlueprintPure, Category = Animation)
	FTransform GetAimOffsetTransform() const;

	//====================================================] Multi-types in-one

protected:
	void FireWeapon_Instant();
	void FireWeapon_Shotgun();
	void FireWeapon_Projectile();

	//====================================================] Shotgun & Instant
	UFUNCTION(reliable, server, WithValidation)
	void ServerNotifyInstantShoot(const TArray<FInstantShootData>& InShootData);

	void ProcessInstantHit(const TArray<FInstantShootData>& InShootData);
	void ProcessInstantHit_Confirmed(const TArray<FInstantShootData>& InShootData);

public:

	bool ShouldDealDamage(AActor* TestActor) const;

protected:

	void DealDamage(const TArray<FInstantShootData>& InShootData);

	UFUNCTION()
	void OnRep_ShootNotifyInstant();

	UPROPERTY(Transient, ReplicatedUsing = OnRep_ShootNotifyInstant)
	TArray<FInstantShootData> ShootNotifyInstant;

	float CurrentFiringSpread;

public:

	float GetCurrentSpread() const;

protected:

	void SimulateInstantHit(const FVector& Origin, int32 RandomSeed, float ReticleSpread);
	void SpawnImpactEffects(const FHitResult& Impact);
	void SpawnTrailEffect(const FVector& EndPoint);

	UFUNCTION(reliable, server, WithValidation)
	void ServerFireProjectile(FVector Origin, FVector_NetQuantizeNormal ShootDir);

public:

	UFUNCTION(BlueprintCallable, BlueprintPure, Category = Gameplay)
	FName GetMuzzleSocketName() const;

	UFUNCTION(BlueprintCallable, BlueprintPure, Category = Gameplay)
	USceneComponent* GetMuzzleSocketComponent(const bool IsFirstView = true) const;

	UPROPERTY() double StartedRealoadTime;
};

