//----------------------------------------
#include "ShooterGame.h"

//----------------------------------------
#include "ShooterGrenade.h"
#include "ShooterCharacter.h"

//----------------------------------------
#include "Item/Grenade/GrenadeItemEntity.h"

//----------------------------------------
#include "Inventory/Projectile/GrenadeProjectile.h"
#include "Inventory/ShooterWeapon.h"
#include "GameSingletonExtensions.h"
#include "Animation/AnimNotify_NativeEvent.h"

UShooterGrenade::UShooterGrenade(const FObjectInitializer& ObjectInitializer)
	: Super(ObjectInitializer)
{

}

//----------------------------------------
bool UShooterGrenade::IsAllowExecuteProcess() const
{
	return true;
}

void UShooterGrenade::BeginDestroy()
{
	if (UAnimNotify_NativeEvent::OnAnimNativeEvent.IsBoundToObject(this))
	{
		UAnimNotify_NativeEvent::OnAnimNativeEvent.RemoveAll(this);
	}

	Super::BeginDestroy();
}

void UShooterGrenade::OnRep_Activated()
{
	Super::OnRep_Activated();

	if (bActivated)
	{
		if (auto MyProperty = GetGrenadeProperty())
		{
			if (auto MyCharacter = GetCharacter<AShooterCharacter>())
			{
				MyCharacter->PlayTwoAnimMontage(MyProperty->FireAnim);
			}
		}
	}
}


void UShooterGrenade::OnProcess()
{
	if (const auto character = GetCharacter<AShooterCharacter>())
	{
		if (const auto MyProperty = GetGrenadeProperty())
		{
			FVector  CameraLoc;
			FRotator CameraRot;
			character->GetActorEyesViewPoint(CameraLoc, CameraRot);
			CameraRot = (CameraRot.Vector() + character->GetActorUpVector() * 0.5f).Rotation();

			auto Transform = FTransform(CameraRot, CameraLoc);// character->GetActorRightVector() * -30.0f);
			const FVector TransformedVector = Transform.TransformVector(MyProperty->SpawnGrenadeOffset);
			Transform.SetLocation(CameraLoc + TransformedVector);
			const auto ProjectileClass = FGameSingletonExtensions::FindSubClassByName("GrenadeProjectileClass", AGrenadeProjectile::StaticClass());

			AGrenadeProjectile* Projectile = GetWorld()->SpawnActorDeferred<AGrenadeProjectile>(ProjectileClass, Transform, character, character, ESpawnActorCollisionHandlingMethod::AlwaysSpawn);
			Projectile->SetInstance(GetEntity<UGrenadeItemEntity>());
			Projectile->GetMovableComponent()->IgnoreActorWhenMoving(character, true);
			Projectile->GetMovableComponent()->IgnoreActorWhenMoving(character->GetWeapon(), true);
			Projectile->FinishSpawning(Transform);

			StopTimer();
		}
	}
	else UE_LOG(LogAnimation, Error, TEXT("UShooterGrenade::OnProcess >> character was nullptr"));
}

bool UShooterGrenade::StopProcess()
{
	bActivated = false;
	return true;
}

bool UShooterGrenade::StartProcess()
{
	if (IsAllowStartProcess())
	{
		if (auto MyProperty = GetGrenadeProperty())
		{
			if (auto MyCharacter = GetCharacter<AShooterCharacter>())
			{
				auto UseMesh = MyCharacter->GetMesh1P();
				auto UseMontage = MyProperty->FireAnim.GetPawn1P();

				//if (UAnimNotify_NativeEvent::OnAnimNativeEvent.IsBoundToObject(this))
				//{
				//	UE_LOG(LogAnimation, Error, TEXT("UShooterGrenade::StartProcess >> OnAnimNativeEvent was binded"));
				//}
				//else
				//{
				//	UE_LOG(LogAnimation, Error, TEXT("UShooterGrenade::StartProcess >> OnAnimNativeEvent was binding"));
				//	UAnimNotify_NativeEvent::OnAnimNativeEvent.AddUObject(this, &UShooterGrenade::OnAnimNotify);
				//}

				if (UseMontage && UseMesh && UseMesh->AnimScriptInstance)
				{
					const float AnimDuration = UseMesh->AnimScriptInstance->Montage_Play(UseMontage, 1.0f);
					const float EventTime = GetMontageEventTime(UseMontage, TEXT("GrenadeFire"));

					if (EventTime > .0f)
					{
						if (StartGameTimer(EventTime, false, EventTime))
						{
							UE_LOG(LogAnimation, Error, TEXT("UShooterGrenade::StartProcess >> AnimDuration: %.2f, AcivationDelay: %.2f #1"), AnimDuration, MyProperty->AcivationDelay);
							bActivated = true;
							UseItem();
							return true;
						}
					}
					else
					{
						if (StartGameTimer(FMath::Max(0.01f, AnimDuration), false, FMath::Max(0.01f, MyProperty->AcivationDelay)))
						{
							UE_LOG(LogAnimation, Error, TEXT("UShooterGrenade::StartProcess >> AnimDuration: %.2f, AcivationDelay: %.2f #2"), AnimDuration, MyProperty->AcivationDelay);
							bActivated = true;
							UseItem();
							return true;
						}
					}
				}				
			}
			else UE_LOG(LogAnimation, Error, TEXT("UShooterGrenade::StartProcess >> MyCharacter was nullptr"));
		}
		else UE_LOG(LogAnimation, Error, TEXT("UShooterGrenade::StartProcess >> MyProperty was nullptr"));
	}
	else UE_LOG(LogAnimation, Error, TEXT("UShooterGrenade::StartProcess >> IsAllowStartProcess was false"));
	return false;
}

void UShooterGrenade::OnAnimNotify(FName InNotifyName, USkeletalMeshComponent* InMesh, UAnimSequenceBase* InAnimation)
{
	//==================================================
	UE_LOG(LogAnimation, Error, TEXT("UShooterGrenade::OnAnimNotify >> InNotifyName: %s / eq: %d"), *InNotifyName.ToString(), InNotifyName == TEXT("GrenadeFire"));

	//==================================================
	if (InNotifyName == TEXT("GrenadeFire"))
	{
		OnProcess();
	}
}

float UShooterGrenade::GetMontageEventTime(UAnimMontage* InTargetMontage, const FName& InEventName) const
{
	if (auto UseMontage = GetValidObject(InTargetMontage))
	{
		for (auto Notify : UseMontage->Notifies)
		{
			if (auto NotifyNativeEvent = GetValidObjectAs<UAnimNotify_NativeEvent>(Notify.Notify))
			{
				if (NotifyNativeEvent->NotifyName == InEventName)
				{
					return Notify.GetDuration();
				}
			}
		}
	}

	return .0f;
}

void UShooterGrenade::InitializeInstance(UBasicPlayerItem* InInstance)
{
	//==================================================
	if (InInstance)
	{
		UE_LOG(LogInit, Error, TEXT("> [UShooterGrenade::InitializeInstance][%s][%d / amount: %d]"), *GetPlayerName(), InInstance->GetModelId(), InInstance->GetAmount());
	}

	//==================================================
	Super::InitializeInstance(InInstance);
	if (const auto property = GetGrenadeProperty())
	{
#if WITH_EDITOR
		SetAmount(5);
#else
		SetAmount(FMath::Min<int32>(InInstance->GetAmount(), property->GetDefaultAmount()));
#endif
	}
}

float UShooterGrenade::GetExecuteInterval() const
{
	if (const auto MyProperty = GetGrenadeProperty())
	{
		return MyProperty->AcivationDelay + MyProperty->DetonationDelay;
	}

	return Super::GetExecuteInterval();
}

const FGrenadeItemProperty* UShooterGrenade::GetGrenadeProperty() const
{
	return GetItemProperty<FGrenadeItemProperty>();
}

FGrenadeItemProperty* UShooterGrenade::GetGrenadeProperty()
{
	return GetItemProperty<FGrenadeItemProperty>();
}

