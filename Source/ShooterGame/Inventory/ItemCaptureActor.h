// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "GameFramework/Actor.h"
#include "BasicItemEntity.h"
#include "ItemCaptureActor.generated.h"

class USceneComponent;
class USceneCaptureComponent2D;
class USkeletalMeshComponent;
class URotatingMovementComponent;

UCLASS()
class SHOOTERGAME_API AItemCaptureActor : public AActor
{
	GENERATED_BODY()
	
public:	

	AItemCaptureActor();

	UFUNCTION(BlueprintCallable, Category = Capture)
	void SetPreviewMesh(USkeletalMesh* InMesh, const FPreviewMeshSettings& InSettings);

protected:

	UPROPERTY(VisibleAnywhere, BlueprintReadOnly, Category = Capture)
	USceneComponent* SceneComponent;

	UPROPERTY(VisibleAnywhere, BlueprintReadOnly, Category = Capture)
	USceneComponent* RotateComponent;

	UPROPERTY(VisibleAnywhere, BlueprintReadOnly, Category = Capture)
	USceneCaptureComponent2D* CaptureComponent;

	UPROPERTY(VisibleAnywhere, BlueprintReadOnly, Category = Capture)
	USkeletalMeshComponent* PreviewMeshComponent;

	UPROPERTY(VisibleAnywhere, BlueprintReadOnly, Category = Capture)
	URotatingMovementComponent* RotationMovement;

	UFUNCTION(BlueprintCallable, Category = Capture)
	void ToggleRotationMovement(bool InToggle);
};
