// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "GameFramework/Actor.h"
#include "Knife.generated.h"

class UKnifePlayerItem;
class UKnifeItemEntity;
struct FKnifeItemProperty;

UCLASS()
class SHOOTERGAME_API AKnife : public AActor
{
	GENERATED_BODY()
	
public:	

	AKnife();

	void InitializeInstance(UKnifePlayerItem* InItem);
	void Activate();
	FKnifeItemProperty* GetKnifeProperty() const;
	UKnifeItemEntity* GetInstanceEntity() const;
	bool CanActivate(bool InIsTargetCheck = false) const;
	bool IsActivated() const { return bIsActivated; }
	void SetTargetCollision(ECollisionChannel InTargetCollision);
	virtual void BeginDestroy() override;
protected:

	virtual void Tick(float DeltaTime) override;
	void OnDeactivate();

	bool KnifeTrace() const;
	bool IsPointInCone(const FVector& InPoint, float& OutPercentToCenter);

	void AttachMeshToPawn();

	UFUNCTION()
	void OnKnifeBeginOverlap(UPrimitiveComponent* OverlappedComponent, AActor* OtherActor, UPrimitiveComponent* OtherComp, int32 OtherBodyIndex, bool bFromSweep, const FHitResult& SweepResult);

	UFUNCTION()
	void OnRep_IsActivated();

	UFUNCTION()
	void OnRep_Instance();

	UFUNCTION()	void OnMesh1PMontageComplete() const;
	UFUNCTION()	void OnMesh3PMontageComplete() const;

	UFUNCTION(Reliable, Server, WithValidation)
	void ServerCheckHit(const FHitResult& InHitResult);

	UPROPERTY()
	FTimerHandle UnUsedHandle;

	UPROPERTY(ReplicatedUsing = OnRep_Instance)
	UKnifePlayerItem* Instance;

	UPROPERTY(ReplicatedUsing = OnRep_IsActivated)
	bool bIsActivated;

	UPROPERTY(BlueprintReadOnly, VisibleDefaultsOnly, Category = Mesh)
	USkeletalMeshComponent* Mesh1P;

	UPROPERTY(BlueprintReadOnly, VisibleDefaultsOnly, Category = Mesh)
	USkeletalMeshComponent* Mesh3P;

	UPROPERTY(Replicated, BlueprintReadOnly, EditDefaultsOnly, Category = Collision)
	TEnumAsByte<ECollisionChannel> TargetCollision;

	UPROPERTY()
	TArray<AActor*> OverlapedActors;

	FTimerHandle Timer_Deactivate;
};
