// Copyright 1998-2018 Epic Games, Inc. All Rights Reserved.

#pragma once

#include "Sound/SoundCue.h"
#include "Components/CapsuleComponent.h"
#include "ShooterPickup.generated.h"

UENUM(BlueprintType)
enum class EShooterPickupFlags : uint8
{
	PickedUpOnEntry,
	PickedUpManual,
	OnlyOnceUsed,
	Respawnable,
	LimitedTime
};

UCLASS(abstract)
class AShooterPickup : public AActor
{
	GENERATED_BODY()

public:

	DECLARE_LOG_CATEGORY_CLASS(LogShooterPickup, Log, All);

	AShooterPickup();

	/** pickup on touch */
	virtual void NotifyActorBeginOverlap(class AActor* Other) override;
	virtual void NotifyActorEndOverlap(class AActor* Other) override;

	/** check if pawn can use this pickup */
	virtual bool CanBePickedUp(class AShooterCharacter* TestPawn) const;

	/** handle touches */
	void PickupOnTouch(class AShooterCharacter* Pawn);

	UFUNCTION(BlueprintCallable, BlueprintPure, Category = Pickup)
	EShooterPickupFlags GetShooterPickupFlags() const;

protected:
	/** initial setup */
	virtual void BeginPlay() override;

	UPROPERTY(VisibleDefaultsOnly, BlueprintReadOnly)
	UCapsuleComponent* CollisionComp;

	/** FX of active pickup */
	UPROPERTY(EditDefaultsOnly, Category=Effects)
	UParticleSystem* ActiveFX;

	/** FX of pickup on respawn timer */
	UPROPERTY(EditDefaultsOnly, Category=Effects)
	UParticleSystem* RespawningFX;

	/** sound played when player picks it up */
	UPROPERTY(EditDefaultsOnly, Category=Effects)
	USoundCue* PickupSound;

	/** sound played on respawn */
	UPROPERTY(EditDefaultsOnly, Category=Effects)
	USoundCue* RespawnSound;

	/** how long it takes to respawn? */
	UPROPERTY(EditDefaultsOnly, Category=Pickup)
	float RespawnTime;

	UPROPERTY(EditDefaultsOnly, Category = Pickup)
	float LimitTime;

	UPROPERTY(EditDefaultsOnly, Category = Pickup, Meta = (Bitmask, BitmaskEnum = "EShooterPickupFlags"))
	int32 ShooterPickupFlags;

	/** is it ready for interactions? */
	UPROPERTY(Transient, ReplicatedUsing=OnRep_IsActive)
	bool bIsActive;

	/** Handle for efficient management of RespawnPickup timer */
	FTimerHandle TimerHandle_RespawnPickup;

	UFUNCTION()
	void OnRep_IsActive();

	/** give pickup */
	virtual void GivePickupTo(class AShooterCharacter* Pawn);

	/** show and enable pickup */
	virtual void RespawnPickup();

	/** show effects when pickup disappears */
	virtual void OnPickedUp();

	/** show effects when pickup appears */
	virtual void OnRespawned();

	/** blueprint event: pickup disappears */
	UFUNCTION(BlueprintImplementableEvent)
	void OnPickedUpEvent();

	/** blueprint event: pickup appears */
	UFUNCTION(BlueprintImplementableEvent)
	void OnRespawnEvent();
};
