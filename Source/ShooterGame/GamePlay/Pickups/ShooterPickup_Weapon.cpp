// Fill out your copyright notice in the Description page of Project Settings.

#include "ShooterGame.h"
#include "ShooterPickup_Weapon.h"
#include "ShooterCharacter.h"
#include "GameSingletonExtensions.h"
#include "Inventory/ShooterWeapon.h"
#include "WeaponPlayerItem.h"
#include "WeaponItemEntity.h"
#include "FlagsHelperExtensions.h"

const FName AShooterPickup_Weapon::PICKUP_OBJECT_CLASS_NAME = "WeaponPickup";

AShooterPickup_Weapon::AShooterPickup_Weapon() : Super()
{
	WeaponPreview = CreateDefaultSubobject<USkeletalMeshComponent>(TEXT("WeaponPreview"));
	WeaponPreview->bEnableUpdateRateOptimizations = true;
	WeaponPreview->bUseAttachParentBound = true;
	//WeaponPreview->bDeferMovementFromSceneQueries = true;
	WeaponPreview->bReceivesDecals = false;
	WeaponPreview->CastShadow = false;
	WeaponPreview->SetupAttachment(RootComponent);
	WeaponPreview->bOverrideMinLod = true;
	WeaponPreview->MinLodModel = 2;

	WeaponPreview->bDisableClothSimulation = true;
	WeaponPreview->CastShadow = false;
	WeaponPreview->bAffectDynamicIndirectLighting = false;
	WeaponPreview->bAffectDistanceFieldLighting = false;
	WeaponPreview->bCastStaticShadow = false;
	WeaponPreview->bCastDynamicShadow = false;
	WeaponPreview->bCastVolumetricTranslucentShadow = false;
	WeaponPreview->bSelfShadowOnly = true;
	WeaponPreview->bReceiveMobileCSMShadows = false;
	WeaponPreview->bDisableMorphTarget = true;
	WeaponPreview->bPerBoneMotionBlur = false;
	WeaponPreview->bRenderStatic = true;
	WeaponPreview->bPauseAnims = true;

	ShooterPickupFlags = 
		FFlagsHelper::ToFlag(static_cast<int32>(EShooterPickupFlags::PickedUpManual)) | 
		FFlagsHelper::ToFlag(static_cast<int32>(EShooterPickupFlags::OnlyOnceUsed)) | 
		FFlagsHelper::ToFlag(static_cast<int32>(EShooterPickupFlags::LimitedTime));
}

UWeaponItemEntity* AShooterPickup_Weapon::GetInstanceEntity() const
{
	return GetValidObject(ModelEntity);
}

USkeletalMeshComponent* AShooterPickup_Weapon::GetWeaponPreview() const
{
	return GetValidObject(WeaponPreview);
}

void AShooterPickup_Weapon::SetModelId(const int32& InModelId)
{
	ModelId = InModelId;
	OnRep_Instance();
}

void AShooterPickup_Weapon::GivePickupTo(AShooterCharacter* Pawn)
{
	//==========================================
	if (Pawn == nullptr)
	{
		return;
	}

	//==========================================
	if (const auto instance = FGameSingletonExtensions::FindItemById<UWeaponItemEntity>(ModelId))
	{
		//---------------------------------------------------
		auto PlayerItem = NewObject<UWeaponPlayerItem>(Pawn, instance->GetInventoryClass());
		if (PlayerItem)
		{
			PlayerItem->SetLevel(Level);
			PlayerItem->SetEntityId(FGuid::NewGuid());
			PlayerItem->InitializeInstanceDynamic(instance);
			PlayerItem->SetState(EPlayerItemState::Temporary);

			AShooterWeapon* NewWeapon = GetWorld()->SpawnActorDeferred<AShooterWeapon>(AShooterWeapon::StaticClass(), FTransform(), Pawn, Pawn, ESpawnActorCollisionHandlingMethod::AlwaysSpawn);
			if (NewWeapon)
			{
				//-----------------------------------------
				NewWeapon->InitializeInstance(PlayerItem);
				NewWeapon->FinishSpawning(FTransform());
				
				//-----------------------------------------
				auto FoundType = Pawn->FindWeaponBySlot(instance->GetWeaponProperty().Type);

				Pawn->AddWeapon(NewWeapon);
				Pawn->EquipWeapon(NewWeapon);
				Pawn->RemoveWeapon(FoundType);
			}
			else
			{
				UE_LOG(LogShooterPickup, Error, TEXT("GivePickupTo >> NewWeapon [%d] was nullptr | GetInventoryClass: %d / %s"), ModelId, instance->GetInventoryClass() != nullptr, *instance->GetInventoryClass()->GetName());
			}
		}
		else
		{
			UE_LOG(LogShooterPickup, Error, TEXT("GivePickupTo >> PlayerItem [%d] was nullptr | GetInventoryClass: %d / %s"), ModelId, instance->GetInventoryClass() != nullptr, *instance->GetInventoryClass()->GetName());
		}
	}
}

void AShooterPickup_Weapon::OnRep_Instance()
{
	//==========================================
	if (ModelId >= 0)
	{
		ModelEntity = FGameSingletonExtensions::FindItemById<UWeaponItemEntity>(ModelId);
	}

	//==========================================
	if (ModelId < 0 || ModelEntity == nullptr)
	{
		UE_LOG(LogInit, Error, TEXT("> [AShooterWeaponPickup :: OnRep_Instance] : ModelEntity was nullptr | ModelId: %d"), ModelId);
	}

	//==========================================
	if (const auto entity = GetInstanceEntity())
	{
		if (auto mesh = GetWeaponPreview())
		{
			auto MeshData = entity->GetPreviewMeshData();
			mesh->SetSkeletalMesh(MeshData.Mesh);
		}
		else
		{
			UE_LOG(LogInit, Error, TEXT("> [AShooterWeaponPickup :: OnRep_Instance] : WeaponPreview was nullptr | ModelId: %d"), ModelId);
		}
	}
	else
	{
		Destroy(true);
	}
}

void AShooterPickup_Weapon::GetLifetimeReplicatedProps(TArray< FLifetimeProperty > & OutLifetimeProps) const
{
	Super::GetLifetimeReplicatedProps(OutLifetimeProps);

	DOREPLIFETIME(AShooterPickup_Weapon, ModelId);
	DOREPLIFETIME(AShooterPickup_Weapon, Level);
}