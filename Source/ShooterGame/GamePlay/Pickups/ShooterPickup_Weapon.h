// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "GamePlay/Pickups/ShooterPickup.h"
#include "ShooterPickup_Weapon.generated.h"

class UWeaponItemEntity;
/**
 * 
 */
UCLASS()
class SHOOTERGAME_API AShooterPickup_Weapon : public AShooterPickup
{
	GENERATED_BODY()

public:

	AShooterPickup_Weapon();

	static const FName PICKUP_OBJECT_CLASS_NAME;

	UFUNCTION(BlueprintCallable, BlueprintPure, Category = "Pickups|Weapon")
	UWeaponItemEntity* GetInstanceEntity() const;

	UFUNCTION(BlueprintCallable, BlueprintPure, Category = "Pickups|Weapon")
	USkeletalMeshComponent* GetWeaponPreview() const;

	UFUNCTION(BlueprintCallable, Category = "Pickups|Weapon")
	void SetModelId(const int32& InModelId);

	UFUNCTION(BlueprintCallable, Category = "Pickups|Weapon")
	void SetCountAmmo(const int32& InCountAmmo)	{ CountAmmo = InCountAmmo; }

	UFUNCTION(BlueprintCallable, Category = "Pickups|Weapon")
	void SetLevel(const int32& InLevel) { Level = InLevel; }

	UFUNCTION(BlueprintCallable, BlueprintPure, Category = "Pickups|Weapon")
	int32 GetInstanceLevel() const { return Level; }

	UFUNCTION(BlueprintCallable, BlueprintPure, Category = "Pickups|Weapon")
	int32 GetInstanceCountAmmo() const { return CountAmmo; }

	virtual void GivePickupTo(AShooterCharacter* Pawn) override;

protected:
	
	UPROPERTY(VisibleDefaultsOnly, BlueprintReadOnly)
	USkeletalMeshComponent* WeaponPreview;

	UPROPERTY(VisibleInstanceOnly, Transient, BlueprintReadOnly)
	int32 CountAmmo;

	UPROPERTY(VisibleInstanceOnly, Transient, Replicated, BlueprintReadOnly)
	int32 Level;

	UPROPERTY(VisibleInstanceOnly, Transient, ReplicatedUsing = OnRep_Instance, BlueprintReadOnly)
	int32 ModelId;

	UPROPERTY(VisibleInstanceOnly, Transient)
	UWeaponItemEntity* ModelEntity;

	UFUNCTION()
	virtual void OnRep_Instance();
};
