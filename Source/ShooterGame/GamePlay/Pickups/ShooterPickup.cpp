// Copyright 1998-2018 Epic Games, Inc. All Rights Reserved.

#include "ShooterGame.h"
#include "ShooterPickup.h"
#include "Particles/ParticleSystemComponent.h"
#include "GameMode/ShooterGameMode.h"
#include "Player/ShooterCharacter.h"
#include "FlagsHelperExtensions.h"

DEFINE_LOG_CATEGORY_CLASS(AShooterPickup, LogShooterPickup);

AShooterPickup::AShooterPickup() : Super()
{
	CollisionComp = CreateDefaultSubobject<UCapsuleComponent>(TEXT("CollisionComp"));
	CollisionComp->InitCapsuleSize(40.0f, 50.0f);
	CollisionComp->SetCollisionObjectType(COLLISION_PICKUP);
	CollisionComp->SetCollisionEnabled(ECollisionEnabled::QueryOnly);
	CollisionComp->SetCollisionResponseToAllChannels(ECR_Ignore);
	CollisionComp->SetGenerateOverlapEvents(true);

	CollisionComp->SetCollisionResponseToChannel(COLLISION_FRIENDLY, ECR_Overlap);
	CollisionComp->SetCollisionResponseToChannel(COLLISION_ENEMY, ECR_Overlap);

	SetRootComponent(CollisionComp);

	RespawnTime = 10.0f;
	LimitTime = 10.0f;
	bIsActive = false;

	SetRemoteRoleForBackwardsCompat(ROLE_SimulatedProxy);
	SetReplicates(true);
}

void AShooterPickup::BeginPlay()
{
	Super::BeginPlay();

	RespawnPickup();

	// register on pickup list (server only), don't care about unregistering (in FinishDestroy) - no streaming
	AShooterGameMode* GameMode = GetWorld()->GetAuthGameMode<AShooterGameMode>();
	if (GameMode)
	{
		GameMode->LevelPickups.Add(this);
	}

	if (FFlagsHelper::HasAnyFlags(ShooterPickupFlags, FFlagsHelper::ToFlag(static_cast<int32>(EShooterPickupFlags::LimitedTime))))
	{
		SetLifeSpan(LimitTime);
	}
}

void AShooterPickup::NotifyActorBeginOverlap(class AActor* Other)
{
	Super::NotifyActorBeginOverlap(Other);

	if (FFlagsHelper::HasAnyFlags(ShooterPickupFlags, FFlagsHelper::ToFlag(static_cast<int32>(EShooterPickupFlags::PickedUpOnEntry))))
	{
		PickupOnTouch(GetValidObjectAs<AShooterCharacter>(Other));
	}
	else if (FFlagsHelper::HasAnyFlags(ShooterPickupFlags, FFlagsHelper::ToFlag(static_cast<int32>(EShooterPickupFlags::PickedUpManual))))
	{
		if (auto EntryCharacter = GetValidObjectAs<AShooterCharacter>(Other))
		{
			EntryCharacter->AddOverlapedPickup(this);
		}
	}
}

void AShooterPickup::NotifyActorEndOverlap(AActor* Other)
{
	Super::NotifyActorEndOverlap(Other);

	if (FFlagsHelper::HasAnyFlags(ShooterPickupFlags, FFlagsHelper::ToFlag(static_cast<int32>(EShooterPickupFlags::PickedUpManual))))
	{
		if (auto EntryCharacter = GetValidObjectAs<AShooterCharacter>(Other))
		{
			EntryCharacter->RemoveOverlapedPickup(this);
		}
	}
}

bool AShooterPickup::CanBePickedUp(AShooterCharacter* TestPawn) const
{
	return TestPawn && TestPawn->IsAlive();
}

void AShooterPickup::GivePickupTo(AShooterCharacter* Pawn)
{
}

void AShooterPickup::PickupOnTouch(AShooterCharacter* Pawn)
{
	if (bIsActive && Pawn && Pawn->IsAlive() && !IsPendingKill())
	{
		if (CanBePickedUp(Pawn))
		{
			GivePickupTo(Pawn);

			if (FFlagsHelper::HasAnyFlags(ShooterPickupFlags, FFlagsHelper::ToFlag(static_cast<int32>(EShooterPickupFlags::OnlyOnceUsed))))
			{
				Destroy(true);
			}
			else if (FFlagsHelper::HasAnyFlags(ShooterPickupFlags, FFlagsHelper::ToFlag(static_cast<int32>(EShooterPickupFlags::Respawnable))))
			{
				if (!IsPendingKill())
				{
					bIsActive = false;
					OnPickedUp();

					if (RespawnTime > 0.0f)
					{
						GetWorldTimerManager().SetTimer(TimerHandle_RespawnPickup, this, &AShooterPickup::RespawnPickup, RespawnTime, false);
					}
				}
			}
		}
	}
}

EShooterPickupFlags AShooterPickup::GetShooterPickupFlags() const
{
	return static_cast<EShooterPickupFlags>(ShooterPickupFlags);
}

void AShooterPickup::RespawnPickup()
{
	bIsActive = true;
	OnRespawned();

	if (FFlagsHelper::HasAnyFlags(ShooterPickupFlags, FFlagsHelper::ToFlag(static_cast<int32>(EShooterPickupFlags::PickedUpOnEntry))))
	{
		TSet<AActor*> OverlappingPawns;
		GetOverlappingActors(OverlappingPawns, AShooterCharacter::StaticClass());

		for (AActor* OverlappingPawn : OverlappingPawns)
		{
			PickupOnTouch(CastChecked<AShooterCharacter>(OverlappingPawn));
		}
	}
}

void AShooterPickup::OnPickedUp()
{
	if (PickupSound)
	{
		UGameplayStatics::SpawnSoundAttached(PickupSound, GetRootComponent());
	}

	if (ActiveFX)
	{
		UGameplayStatics::SpawnEmitterAttached(ActiveFX, GetRootComponent());
	}

	OnPickedUpEvent();
}

void AShooterPickup::OnRespawned()
{
	const bool bJustSpawned = CreationTime <= (GetWorld()->GetTimeSeconds() + 5.0f);
	if (RespawnSound && !bJustSpawned)
	{
		UGameplayStatics::PlaySoundAtLocation(this, RespawnSound, GetActorLocation());
	}

	if (RespawningFX && !bJustSpawned)
	{
		UGameplayStatics::SpawnEmitterAttached(RespawningFX, GetRootComponent());
	}

	OnRespawnEvent();
}

void AShooterPickup::OnRep_IsActive()
{
	if (bIsActive)
	{
		OnRespawned();
	}
	else
	{
		OnPickedUp();
	}
}

void AShooterPickup::GetLifetimeReplicatedProps( TArray< FLifetimeProperty > & OutLifetimeProps ) const
{
	Super::GetLifetimeReplicatedProps( OutLifetimeProps );

	DOREPLIFETIME( AShooterPickup, bIsActive );
}