// Fill out your copyright notice in the Description page of Project Settings.

#include "ShooterGame.h"
#include "Knife.h"
#include "UObjectExtensions.h"
#include "KnifeItemEntity.h"
#include "BasicPlayerItem.h"
#include "KnifePlayerItem.h"
#include "ShooterCharacter.h"

AKnife::AKnife()
{
	Mesh1P = CreateDefaultSubobject<USkeletalMeshComponent>(TEXT("Mesh1P"));
	Mesh1P->VisibilityBasedAnimTickOption = EVisibilityBasedAnimTickOption::OnlyTickPoseWhenRendered;
	Mesh1P->bReceivesDecals = false;
	Mesh1P->SetCollisionObjectType(ECC_WorldDynamic);
	Mesh1P->SetCollisionEnabled(ECollisionEnabled::QueryOnly);
	Mesh1P->SetCollisionResponseToAllChannels(ECR_Ignore);
	Mesh1P->bEnableUpdateRateOptimizations = true;
	Mesh1P->bUseAttachParentBound = true;
	Mesh1P->bOnlyOwnerSee = true;
	Mesh1P->bOverrideMinLod = true;
	Mesh1P->MinLodModel = 1;
	Mesh1P->SetGenerateOverlapEvents(true);
	Mesh1P->OnComponentBeginOverlap.AddDynamic(this, &AKnife::OnKnifeBeginOverlap);
	Mesh1P->bDisableClothSimulation = true;
	Mesh1P->CastShadow = false;
	Mesh1P->bAffectDynamicIndirectLighting = false;
	Mesh1P->bAffectDistanceFieldLighting = false;
	Mesh1P->bCastStaticShadow = false;
	Mesh1P->bCastDynamicShadow = false;
	Mesh1P->bCastVolumetricTranslucentShadow = false;
	Mesh1P->bSelfShadowOnly = true;
	Mesh1P->bReceiveMobileCSMShadows = false;
	Mesh1P->bDisableMorphTarget = true;
	Mesh1P->bPerBoneMotionBlur = false;
	Mesh1P->bRenderStatic = true;
	Mesh1P->bPauseAnims = true;


	RootComponent = Mesh1P;



	Mesh3P = CreateDefaultSubobject<USkeletalMeshComponent>(TEXT("Mesh3P"));
	Mesh3P->SetGenerateOverlapEvents(false);
	Mesh3P->VisibilityBasedAnimTickOption = EVisibilityBasedAnimTickOption::OnlyTickPoseWhenRendered;
	Mesh3P->bReceivesDecals = false;
	Mesh3P->SetCollisionObjectType(ECC_WorldDynamic);
	Mesh3P->SetCollisionEnabled(ECollisionEnabled::NoCollision);
	Mesh3P->SetCollisionResponseToAllChannels(ECR_Ignore);
	Mesh3P->SetCollisionResponseToChannel(COLLISION_WEAPON, ECR_Block);
	Mesh3P->SetCollisionResponseToChannel(ECC_Visibility, ECR_Block);
	Mesh3P->bEnableUpdateRateOptimizations = true;
	Mesh3P->bUseAttachParentBound = true;
	Mesh3P->bOwnerNoSee = true;
	Mesh3P->SetupAttachment(Mesh1P);
	Mesh3P->MinLodModel = 2;
	Mesh3P->bOverrideMinLod = true;

	Mesh3P->bDisableClothSimulation = true;
	Mesh3P->CastShadow = false;
	Mesh3P->bAffectDynamicIndirectLighting = false;
	Mesh3P->bAffectDistanceFieldLighting = false;
	Mesh3P->bCastStaticShadow = false;
	Mesh3P->bCastDynamicShadow = false;
	Mesh3P->bCastVolumetricTranslucentShadow = false;
	Mesh3P->bSelfShadowOnly = true;
	Mesh3P->bReceiveMobileCSMShadows = false;
	Mesh3P->bDisableMorphTarget = true;
	Mesh3P->bPerBoneMotionBlur = false;
	Mesh3P->bRenderStatic = true;
	Mesh3P->bPauseAnims = true;


	PrimaryActorTick.bCanEverTick = true;
	SetReplicates(true);
}

void AKnife::BeginDestroy()
{
	if (auto world = GetValidObject(GetWorld()))
	{
		world->GetTimerManager().ClearAllTimersForObject(this);
	}

	Super::BeginDestroy();
}


void AKnife::InitializeInstance(UKnifePlayerItem* InItem)
{
	Instance = InItem;
	OnRep_Instance();
}

void AKnife::Activate()
{
	if (HasAuthority() && CanActivate())
	{
		if (KnifeTrace())
		{
			if (auto MyProperty = GetKnifeProperty())
			{
				bIsActivated = true;
				OnRep_IsActivated();

				GetWorldTimerManager().SetTimer(Timer_Deactivate, this, &AKnife::OnDeactivate, MyProperty->ActivationInterval);					
			}
		}
	}
}

FKnifeItemProperty* AKnife::GetKnifeProperty() const
{
	if (auto ValidEntity = GetInstanceEntity())
	{
		return &ValidEntity->GetKnifeProperty();
	}

	return nullptr;
}

UKnifeItemEntity* AKnife::GetInstanceEntity() const
{
	if (auto ValidInstance = GetValidObject(Instance))
	{
		if (auto ValidEntity = GetValidObject(ValidInstance->GetKnifeEntity()))
		{
			return ValidEntity;
		}
	}

	return nullptr;
}

bool AKnife::CanActivate(bool InIsTargetCheck) const
{
	if (InIsTargetCheck)
	{
		return !bIsActivated && KnifeTrace();
	}

	return !bIsActivated;
}

void AKnife::SetTargetCollision(ECollisionChannel InTargetCollision)
{
	Mesh1P->SetCollisionResponseToChannel(TargetCollision, ECR_Ignore);
	TargetCollision = InTargetCollision;
	Mesh1P->SetCollisionResponseToChannel(TargetCollision, ECR_Overlap);
}

void AKnife::Tick(float DeltaTime)
{
	Super::Tick(DeltaTime);

}

void AKnife::OnDeactivate()
{
	bIsActivated = false;	
	OnRep_IsActivated();
}

bool AKnife::KnifeTrace() const
{
	static FName KnifeTraceTag = FName(TEXT("KnifeTrace"));

	if (auto MyCharacter = GetValidObjectAs<ACharacter>(GetOwner()))
	{
		FVector ViewLocation;
		FRotator ViewRotation;

		MyCharacter->GetActorEyesViewPoint(ViewLocation, ViewRotation);

		// Perform trace to retrieve hit info
		FCollisionQueryParams TraceParams(KnifeTraceTag, true, MyCharacter);
		TraceParams.bReturnPhysicalMaterial = true;
		TraceParams.MobilityType = EQueryMobilityType::Any;
		TraceParams.bFindInitialOverlaps = true;

		FCollisionResponseParams ResponseParams(ECR_Ignore);
		ResponseParams.CollisionResponse.SetResponse(TargetCollision, ECR_Block);

		TArray<FOverlapResult> Overlaps;
		GetWorld()->OverlapMultiByChannel(Overlaps, ViewLocation + MyCharacter->GetActorForwardVector() * 50.0f, FQuat::Identity, TargetCollision, FCollisionShape::MakeSphere(200.0f), TraceParams, ResponseParams);

		for (auto Overlap : Overlaps)
		{
			if (Cast<ACharacter>(Overlap.GetActor()) != nullptr)
			{
				//OutHitResult = Overlap;
				return true;
			}
		}
	}

	return false;
}

void AKnife::OnKnifeBeginOverlap(UPrimitiveComponent* OverlappedComponent,	AActor* OtherActor,	UPrimitiveComponent* OtherComp,	int32 OtherBodyIndex, bool bFromSweep, const FHitResult& SweepResult)
{
	if (bIsActivated)
	{
		if (OverlapedActors.Contains(OtherActor) == false)
		{
			OverlapedActors.Add(OtherActor);
			ServerCheckHit(SweepResult);
		}
	}
}

bool AKnife::ServerCheckHit_Validate(const FHitResult& InHitResult) { return true; }
void AKnife::ServerCheckHit_Implementation(const FHitResult& InHitResult)
{
	if (auto MyCharacter = GetValidObjectAs<ACharacter>(GetInstigator()))
	{
		FVector ViewLocation;
		FRotator ViewRotation;
		bool bIsConfirmed = false;

		MyCharacter->GetActorEyesViewPoint(ViewLocation, ViewRotation);

		if (InHitResult.GetActor())
		{
			float PercentToCenter = .0f;
			const bool IsInCone = IsPointInCone(InHitResult.GetActor()->GetActorLocation(), PercentToCenter);

			GEngine->AddOnScreenDebugMessage(110, 20.0f, FColorList::OrangeRed, FString::Printf(TEXT("IsInCone: %d, PercentToCenter: %.2f"), IsInCone, PercentToCenter));

			if (PercentToCenter > .1f)
			{
				if (InHitResult.GetActor()->IsRootComponentStatic() || InHitResult.GetActor()->IsRootComponentStationary())
				{
					bIsConfirmed = true;
				}
				else
				{
					// Get the component bounding box
					const FBox HitBox = InHitResult.GetActor()->GetComponentsBoundingBox();

					// calculate the box extent, and increase by a leeway
					FVector BoxExtent = 0.5 * (HitBox.Max - HitBox.Min);
					BoxExtent *= 200.0f;

					// avoid precision errors with really thin objects
					BoxExtent.X = FMath::Max(20.0f, BoxExtent.X);
					BoxExtent.Y = FMath::Max(20.0f, BoxExtent.Y);
					BoxExtent.Z = FMath::Max(20.0f, BoxExtent.Z);

					// Get the box center
					const FVector BoxCenter = (HitBox.Min + HitBox.Max) * 0.5;

					// if we are within client tolerance
					if (FMath::Abs(InHitResult.Location.Z - BoxCenter.Z) < BoxExtent.Z &&
						FMath::Abs(InHitResult.Location.X - BoxCenter.X) < BoxExtent.X &&
						FMath::Abs(InHitResult.Location.Y - BoxCenter.Y) < BoxExtent.Y)
					{
						bIsConfirmed = true;
					}
					else
					{
						UE_LOG(LogShooterWeapon, Error, TEXT("%s Rejected client side hit of %s (outside bounding box tolerance)"), *GetNameSafe(this), *GetNameSafe(InHitResult.GetActor()));
					}
				}
			}
			else
			{
				UE_LOG(LogShooterWeapon, Error, TEXT("%s Rejected client side hit of %s (outside view cone tolerance)"), *GetNameSafe(this), *GetNameSafe(InHitResult.GetActor()));
			}
		}

		auto MyProperty = GetKnifeProperty();
		if (bIsConfirmed && MyProperty)
		{
			AController* MyController = MyCharacter->GetController();

			FPointDamageEvent PointDamageEvent;
			PointDamageEvent.Damage = GetInstanceEntity()->GetDynamicValueForLevel(Instance->GetLevel());
			PointDamageEvent.DamageTypeClass = MyProperty->DamageType;

			if (InHitResult.GetActor())
			{
				PointDamageEvent.HitInfo = InHitResult;
				InHitResult.GetActor()->TakeDamage(PointDamageEvent.Damage, PointDamageEvent, MyController, this);
			}
		}
	}
}

bool AKnife::IsPointInCone(const FVector& InPoint, float& OutPercentToCenter)
{
	if (auto MyCharacter = GetValidObjectAs<ACharacter>(GetInstigator()))
	{
		const FVector localOwnerRotation = MyCharacter->GetActorForwardVector();
		const FVector localOwnerLocation = MyCharacter->GetActorLocation();

		//MyCharacter->GetActorEyesViewPoint(localOwnerLocation, localOwnerRotation);

		GEngine->AddOnScreenDebugMessage(111, 20.0f, FColorList::OrangeRed, FString::Printf(TEXT("localOwnerLocation: %s, localOwnerRotation: %s"), *localOwnerLocation.ToString(), *localOwnerRotation.ToString()));

		const float RadiusAtStart = 100.0f * 2.0f;
		const float RadiusAtEnd = 100.0f * 15.0f;

		const FVector LineEnd = localOwnerRotation * 1000.0f;

		::DrawDebugPoint(GetWorld(), LineEnd, 10, FColorList::LimeGreen, true, 20, SDPG_Foreground);

		return FMath::GetDistanceWithinConeSegment(InPoint, localOwnerLocation, LineEnd, RadiusAtStart, RadiusAtEnd, OutPercentToCenter);
	}

	return false;
}

void AKnife::OnMesh1PMontageComplete() const
{
	if (IsValidObject(this) && IsValidObject(Mesh1P))
	{
		Mesh1P->SetHiddenInGame(true);
	}
}

void AKnife::OnMesh3PMontageComplete() const
{
	if (IsValidObject(this) && IsValidObject(Mesh3P))
	{
		Mesh3P->SetHiddenInGame(true);
	}
}

void AKnife::OnRep_IsActivated()
{
	if (bIsActivated)
	{
		OverlapedActors.Empty();

		if (auto KnifeProperty = GetKnifeProperty())
		{
			AttachMeshToPawn();

			UGameplayStatics::PlaySoundAtLocation(GetWorld(), KnifeProperty->GetFireSound(), GetActorLocation(), GetActorRotation());

			if (auto MyCharacter = GetValidObjectAs<AShooterCharacter>(GetOwner()))
			{
				if (MyCharacter->IsFirstPerson() || HasAuthority())
				{
					Mesh1P->SetHiddenInGame(false);

					if (auto CharacterMesh = MyCharacter->GetMesh1P())
					{
						if (auto CharacterMeshAnimScriptInstance = CharacterMesh->AnimScriptInstance)
						{
							const float AnimDuration = CharacterMeshAnimScriptInstance->Montage_Play(KnifeProperty->FireAnimation.GetPawn1P());
							GetWorldTimerManager().SetTimer(UnUsedHandle, FTimerDelegate::CreateUObject(this, &AKnife::OnMesh1PMontageComplete), AnimDuration, false, AnimDuration);
						}
					}
				}
				else
				{
					Mesh3P->SetHiddenInGame(false);

					if (auto CharacterMesh = MyCharacter->GetMesh())
					{
						if (auto CharacterMeshAnimScriptInstance = CharacterMesh->AnimScriptInstance)
						{
							const float AnimDuration = CharacterMeshAnimScriptInstance->Montage_Play(KnifeProperty->FireAnimation.GetPawn3P());
							GetWorldTimerManager().SetTimer(UnUsedHandle, FTimerDelegate::CreateUObject(this, &AKnife::OnMesh3PMontageComplete), AnimDuration, false, AnimDuration);
						}
					}
				}
			}
		}
	}
	else
	{
		Mesh3P->SetHiddenInGame(true);
		Mesh1P->SetHiddenInGame(true);
	}
}

void AKnife::OnRep_Instance()
{
	if (auto MyEntity = GetInstanceEntity())
	{
		Mesh3P->SetSkeletalMesh(MyEntity->GetTPPMeshData().Mesh);
		Mesh3P->SetAnimInstanceClass(MyEntity->GetTPPMeshData().Anim);
		Mesh3P->SetHiddenInGame(true);

		Mesh1P->SetSkeletalMesh(MyEntity->GetFPPMeshData().Mesh);
		Mesh1P->SetAnimInstanceClass(MyEntity->GetFPPMeshData().Anim);
		Mesh1P->SetHiddenInGame(true);

		AttachMeshToPawn();
	}
}

void AKnife::AttachMeshToPawn()
{
	if (auto MyCharacter = GetValidObjectAs<AShooterCharacter>(GetOwner()))
	{
		FAttachmentTransformRules Rules(EAttachmentRule::SnapToTarget, false);

		// For locally controller players we attach both weapons and let the bOnlyOwnerSee, bOwnerNoSee flags deal with visibility.
		if (MyCharacter->IsLocallyControlled() == true || HasAuthority())
		{
			USkeletalMeshComponent* PawnMesh1p = MyCharacter->GetSpecifcPawnMesh(true);
			USkeletalMeshComponent* PawnMesh3p = MyCharacter->GetSpecifcPawnMesh(false);
			//Mesh1P->SetHiddenInGame(false);
			//Mesh3P->SetHiddenInGame(false);
			Mesh1P->AttachToComponent(PawnMesh1p, Rules, TEXT("KnifeMount"));
			Mesh3P->AttachToComponent(PawnMesh3p, Rules, TEXT("KnifeMount"));
		}
		else
		{
			auto UseKnifeMesh = MyCharacter->IsFirstPerson() ? Mesh1P : Mesh3P;
			auto UsePawnMesh = MyCharacter->GetPawnMesh();
			UseKnifeMesh->AttachToComponent(UsePawnMesh, Rules, TEXT("KnifeMount"));
			//UseKnifeMesh->SetHiddenInGame(false);
		}
	}
}

void AKnife::GetLifetimeReplicatedProps(TArray<FLifetimeProperty, FDefaultAllocator>& OutLifetimeProps) const
{
	Super::GetLifetimeReplicatedProps(OutLifetimeProps);

	DOREPLIFETIME(AKnife, Instance);
	DOREPLIFETIME(AKnife, bIsActivated);

	DOREPLIFETIME_CONDITION(AKnife, TargetCollision, COND_OwnerOnly);
	
}