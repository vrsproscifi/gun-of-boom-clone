// Fill out your copyright notice in the Description page of Project Settings.

#include "ShooterGame.h"
#include "TeamInterface.h"

uint8 ITeamInterface::NullTeam = 255;

uint8 ITeamInterface::GetTeamNum() const
{
	return K2_GetTeamNum();
}

ATeamInfo* ITeamInterface::GetTeam() const
{
	return K2_GetTeam();
}