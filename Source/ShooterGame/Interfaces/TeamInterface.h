// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "UObject/Interface.h"
#include "TeamInterface.generated.h"

class ATeamInfo;

// This class does not need to be modified.
UINTERFACE(MinimalAPI)
class UTeamInterface : public UInterface
{
	GENERATED_BODY()
};

/**
 * 
 */
class SHOOTERGAME_API ITeamInterface
{
	GENERATED_BODY()

	// Add interface functions to this class. This is the class that will be inherited to implement this interface.
public:
	
	UFUNCTION()
	virtual uint8 GetTeamNum() const;

	UFUNCTION()
	virtual ATeamInfo* GetTeam() const;

	//UFUNCTION()
	//virtual bool HasTeam() const
	//{
	//	return !!GetTeam();
	//}

	UFUNCTION(BlueprintImplementableEvent)
	uint8 K2_GetTeamNum() const;

	UFUNCTION(BlueprintImplementableEvent)
	ATeamInfo* K2_GetTeam() const;

	static uint8 NullTeam;
};
