﻿// Copyright 1998-2018 Epic Games, Inc. All Rights Reserved.

#include "ShooterGame.h"
#include "ShooterGameAchievements.h"
#include "ShooterPlayerState.h"
#include "Player/ShooterCharacter.h"
#include "ShooterGameState.h"
#include "Player/ShooterPlayerController.h"
#include "GameInstance/ShooterGameViewportClient.h"
#include "Containers/STouchLayout.h"
#include "Input/STouchInputLook.h"
#include "Input/STouchInputStick.h"
#include "Input/STouchInputButton.h"
#include "ProfileTestData.h"
#include "GameSingletonExtensions.h"
#include "CharacterItemEntity.h"
#include "ArsenalComponent.h"
#include "IdentityComponent.h"
#include "State/SLiveBar.h"
#include "TeamInfo.h"
#include "State/SWeaponBar.h"
#include "State/SGameTimeAndScores.h"
#include "GameInstance/ShooterGameUserSettings.h"
#include "Notify/SKillList.h"

#include "Game/BasicGameAchievement.h"
#include "Extensions/GameSingletonExtensions.h"
#include "Extensions/UObjectExtensions.h"

#include "Notify/SFightAction.h"
#include "Inventory/ShooterAid.h"


#include "Input/STouchInputSwitch.h"
#include "Components/SSniperScope.h"
#include "Input/STouchInputPickup.h"
#include "WeaponItemEntity.h"
#include "Inventory/ShooterGrenade.h"
#include "Inventory/ShooterAid.h"
#include "State/SPlayersTable.h"
#include "Components/SVerticalMenuBuilder.h"
#include "Settings/SSettingsManager.h"
#include "State/SCountdown.h"
#include "State/SBadConnection.h"
#include "ShooterGameAnalytics.h"

#include "Classes/Sound/SoundConcurrency.h"
#include "Containers/SKilledContainer.h"

#include "SRetainerWidget.h"
#include "UObjectExtensions.h"
#include "GamePlay/Knife.h"
#include "GamePlay/Pickups/ShooterPickup_Weapon.h"
#include "GameUtilities.h"
#include "Inventory/ShooterWeapon.h"
#include "WeaponPlayerItem.h"

const int32 AShooterPlayerState::DefaultProfileIndex = 0;

AShooterPlayerState::AShooterPlayerState(const FObjectInitializer& ObjectInitializer) 
	: Super(ObjectInitializer)
	, PlayerWinRate(100)
	, PlayerTargetWeaponLevel(10)
	, PlayerTargetWeaponUpgrade(0)
	, PlayerTargetArmourLevel(0)
	, Team(nullptr)
	, PlayerCoefficient(1.0f)
{
	NotifyAchievementSoundConcurrency = ObjectInitializer.CreateDefaultSubobject<USoundConcurrency>(this, TEXT("NotifyAchievementSoundConcurrency"));
	NotifyAchievementSoundConcurrency->Concurrency.ResolutionRule = EMaxConcurrentResolutionRule::PreventNew;
	NotifyAchievementSoundConcurrency->Concurrency.bLimitToOwner = true;
	NotifyAchievementSoundConcurrency->Concurrency.MaxCount = 1;

	NumBulletsFired = 0;
	NumRocketsFired = 0;
	bQuitter = false;

	PrimaryActorTick.bCanEverTick = true;
	SetActorTickEnabled(true);
}

bool AShooterPlayerState::IsInventorySlotEquiped(const EGameItemType& InInventorySlot) const
{
	return GetInventoryComponent() && GetInventoryComponent()->IsExistAllSlotsInProfile(AShooterPlayerState::DefaultProfileIndex, { InInventorySlot });
}

bool AShooterPlayerState::BotIsArmoured() const
{
	return GetInventoryComponent() && GetInventoryComponent()->IsExistAllSlotsInProfile(AShooterPlayerState::DefaultProfileIndex, { EGameItemType::Head, EGameItemType::Torso, EGameItemType::Legs });
}

bool AShooterPlayerState::BotMaximumLevelReached() const
{
	return GetInventoryComponent() && GetInventoryComponent()->IsMaximumLevelReached(AShooterPlayerState::DefaultProfileIndex);
}

bool AShooterPlayerState::BotIsTargetWeaponLevel(const int32& InTargetWeaponLevel) const
{
	return GetInventoryComponent() && GetInventoryComponent()->IsTargetWeaponLevel(AShooterPlayerState::DefaultProfileIndex, InTargetWeaponLevel);
}


void AShooterPlayerState::Reset()
{
	Super::Reset();
	
	//PlayerStates persist across seamless travel.  Keep the same teams as previous match.
	//SetTeamNum(0);
	NumBulletsFired = 0;
	NumRocketsFired = 0;
	bQuitter = false;
}

void AShooterPlayerState::OnCreateUserInterface()
{
	if (auto MyViewport = UShooterGameViewportClient::Get())
	{
		SAssignNew(Slate_TouchLayout, STouchLayout)
		.Visibility_UObject(this, &AShooterPlayerState::IsOnProgressMatch_Visible)		
		+ STouchLayout::Slot(TEXT("TouchBackground")).HAlign(HAlign_Fill).VAlign(VAlign_Fill)
		[
			SNew(STouchInputLook)
			.AxisY("LookUp")
			.AxisX("Turn")
		]
		+ STouchLayout::Slot(TEXT("TouchStick")).HAlign(HAlign_Left).VAlign(VAlign_Fill).SetSize(FVector2D(600, 680))
		[
			SNew(STouchInputStick)
			.AxisY("MoveForward")
			.AxisX("MoveRight")
			.ActionTargetAxis("MoveForward")
			.ActionOnFullAxis("Run")
			.Visibility_UObject(this, &AShooterPlayerState::IsAliveVisible)
			.IsDynamic_Lambda([&]()
			{
				if (auto MyShooterSettings = GetValidObject(UShooterGameUserSettings::Get()))
				{
					return MyShooterSettings->IsDynamicStick();
				}

				return false;
			})
		]
		+ STouchLayout::Slot(TEXT("TouchTargeting")).HAlign(HAlign_Right).VAlign(VAlign_Center).SetSize(FVector2D(120, 120)).SetPosition(FVector2D(20, 50))
		[
			SNew(SRetainerWidget)
			.Phase(0)
			.PhaseCount(10)
			.RenderOnPhase(true)
			.RenderOnInvalidation(false)
			.Visibility_UObject(this, &AShooterPlayerState::IsAliveVisible)
			[
				SNew(STouchInputButton)
				.Style(&FShooterStyle::Get().GetWidgetStyle<FTouchInputButtonStyle>("STouchInputButton_Targeting"))
				.Action("Targeting")
				.ActivationMethod(EButtonActivationMethod::Toggle)
				.IsActivated_UObject(this, &AShooterPlayerState::TargetingIsActivated)				
			]
		]
		+ STouchLayout::Slot(TEXT("TouchFireing")).HAlign(HAlign_Right).VAlign(VAlign_Center).SetSize(FVector2D(120, 120)).SetPosition(FVector2D(100, 180))
		[
			SNew(SRetainerWidget)
			.Phase(0)
			.PhaseCount(10)
			.RenderOnPhase(true)
			.RenderOnInvalidation(false)
			.Visibility_UObject(this, &AShooterPlayerState::GetFireVisibility)
			[
				SNew(STouchInputButton)
				.Style(&FShooterStyle::Get().GetWidgetStyle<FTouchInputButtonStyle>("STouchInputButton_Fire"))
				.Action("Fire")
				.ActivationMethod(EButtonActivationMethod::Click)				
			]
		]
		+ STouchLayout::Slot(TEXT("TouchEscape")).HAlign(HAlign_Left).VAlign(VAlign_Top).SetSize(FVector2D(96, 96)).SetPosition(FVector2D(4, 4))
		[
			SNew(SRetainerWidget)
			.Phase(0)
			.PhaseCount(10)
			.RenderOnPhase(true)
			.RenderOnInvalidation(false)
			[
				SAssignNew(Touch_EscapeMenu, STouchInputButton)
				.Style(&FShooterStyle::Get().GetWidgetStyle<FTouchInputButtonStyle>("STouchInputButton_Escape"))
				.ActivationMethod(EButtonActivationMethod::Click)
				.OnRawClicked_UObject(this, &AShooterPlayerState::OnEscapeMenuClicked)
			]
		]
		+ STouchLayout::Slot(TEXT("TouchWeaponPickup")).HAlign(HAlign_Center).VAlign(VAlign_Bottom).SetSize(FVector2D(400, 100)).SetPosition(FVector2D(0, 200))
		[
			SNew(SRetainerWidget)
			.Phase(0)
			.PhaseCount(10)
			.RenderOnPhase(true)
			.RenderOnInvalidation(false)
			.Visibility_UObject(this, &AShooterPlayerState::GetWeaponPickupVisibility)
			[
				SNew(STouchInputPickup)
				.Action("WeaponPickup")
				.ActivationMethod(EButtonActivationMethod::Click)				
				.PickupIcon_UObject(this, &AShooterPlayerState::GetWeaponPickupIcon)
				.Better_UObject(this, &AShooterPlayerState::GetWeaponPickupBetter)
			]
		]
		+ STouchLayout::Slot(TEXT("TouchAidActivate")).HAlign(HAlign_Right).VAlign(VAlign_Top).SetSize(FVector2D(120, 120)).SetPosition(FVector2D(180, 200))
		[
			//SNew(SRetainerWidget)
			//.Phase(0)
			//.PhaseCount(10)
			//.RenderOnPhase(true)
			//.RenderOnInvalidation(false)
			//.Visibility_UObject(this, &AShooterPlayerState::GetAidVisibility)
			//[
				SNew(STouchInputButton)
				.Style(&FShooterStyle::Get().GetWidgetStyle<FTouchInputButtonStyle>("STouchInputButton_Aid"))
				.Action("AidActivate")
				.ActivationMethod(EButtonActivationMethod::Click)
				.Amount_UObject(this, &AShooterPlayerState::GetAidAmount)	
				.Progress_UObject(this, &AShooterPlayerState::GetAidPercent)
				.Visibility_UObject(this, &AShooterPlayerState::GetAidVisibility)
			//]
		]
		+ STouchLayout::Slot(TEXT("TouchGrenadeActivate")).HAlign(HAlign_Right).VAlign(VAlign_Top).SetSize(FVector2D(120, 120)).SetPosition(FVector2D(20, 200))
		[
			//SNew(SRetainerWidget)
			//.Phase(0)
			//.PhaseCount(10)
			//.RenderOnPhase(true)
			//.RenderOnInvalidation(false)
			//.Visibility_UObject(this, &AShooterPlayerState::GetGrenadeVisibility)
			//[
				SNew(STouchInputButton)
				.Style(&FShooterStyle::Get().GetWidgetStyle<FTouchInputButtonStyle>("STouchInputButton_Grenade"))
				.Action("GrenadeActivate")
				.ActivationMethod(EButtonActivationMethod::Click)
				.Amount_UObject(this, &AShooterPlayerState::GetGrenadeAmount)
				.Progress_UObject(this, &AShooterPlayerState::GetGrenadePercent)
				.Visibility_UObject(this, &AShooterPlayerState::GetGrenadeVisibility)
			//]
		]
		+ STouchLayout::Slot(TEXT("TouchKnifeActivate")).HAlign(HAlign_Right).VAlign(VAlign_Center).SetSize(FVector2D(120, 120)).SetPosition(FVector2D(220, 80))
		[
			SNew(SRetainerWidget)
			.Phase(0)
			.PhaseCount(10)
			.RenderOnPhase(true)
			.RenderOnInvalidation(false)
			.Visibility_UObject(this, &AShooterPlayerState::GetKnifeVisibility)
			[
				SNew(STouchInputButton)
				.Style(&FShooterStyle::Get().GetWidgetStyle<FTouchInputButtonStyle>("STouchInputButton_Knife"))
				.Action("KnifeActivate")
				.ActivationMethod(EButtonActivationMethod::Click)				
			]
		]
		+ STouchLayout::Slot(TEXT("TouchLiveBar")).HAlign(HAlign_Center).VAlign(VAlign_Bottom).SetSize(FVector2D(200, 64)).SetPosition(FVector2D(-120, 20))
		[
			SAssignNew(Slate_LiveBar, SLiveBar)	
			.Visibility_UObject(this, &AShooterPlayerState::IsAliveHitTestInvisible)
		]
		+ STouchLayout::Slot(TEXT("TouchWeaponBar")).HAlign(HAlign_Center).VAlign(VAlign_Bottom).SetSize(FVector2D(264, 64)).SetPosition(FVector2D(120, 20))
		[
			SAssignNew(Slate_WeaponBar, SWeaponBar)
			.Visibility_UObject(this, &AShooterPlayerState::IsAliveVisible)
		]
		+ STouchLayout::Slot(TEXT("TouchTimeAndScore")).HAlign(HAlign_Fill).VAlign(VAlign_Top).SetSize(FVector2D(0, 100))
		[
			SAssignNew(Slate_GameTimeAndScores, SGameTimeAndScores)
			.GameState(GetWorld()->GetGameState<AShooterGameState>())
			.PlayerState(this)
		]
		+ STouchLayout::Slot(TEXT("TouchWeaponSwitch")).HAlign(HAlign_Right).VAlign(VAlign_Bottom).SetSize(FVector2D(120, 120)).SetPosition(FVector2D(20, 20))
		[
			SNew(STouchInputSwitch)
			.Visibility_UObject(this, &AShooterPlayerState::IsAliveVisible)
			.OnGetWeapons_UObject(this, &AShooterPlayerState::GetAvalibleWeapons)
			.OnSelectWeapon_UObject(this, &AShooterPlayerState::OnSelectWeapon)
			.CurrentWeapon_UObject(this, &AShooterPlayerState::GetCurrentWeapon)
		];		

		SAssignNew(Slate_PlayersTable, SPlayersTable);
		MyViewport->AddViewportWidgetContent_AlwaysVisible(Slate_PlayersTable.ToSharedRef(), 20);

		MyViewport->AddViewportWidgetContent_AlwaysVisible(Slate_TouchLayout.ToSharedRef(), 10);

		SAssignNew(Slate_KillList, SKillList).LocalState(this).Visibility_UObject(this, &AShooterPlayerState::IsOnProgressMatch_HitTestInvisible);
		MyViewport->AddViewportWidgetContent_AlwaysVisible(Slate_KillList.ToSharedRef(), 2);

		SAssignNew(Slate_FightAction, SFightAction).Visibility_UObject(this, &AShooterPlayerState::IsOnProgressMatch_HitTestInvisible);
		MyViewport->AddViewportWidgetContent_AlwaysVisible(Slate_FightAction.ToSharedRef(), 3);

		SAssignNew(Slate_SniperScope, SSniperScope);
		MyViewport->AddViewportWidgetContent_AlwaysVisible(Slate_SniperScope.ToSharedRef(), 1);
		
		Slate_GameTimeAndScores->SetOnMouseButtonDown(FPointerEventHandler::CreateUObject(this, &AShooterPlayerState::OnPlayersTableClicked));

		TArray<FText> EscapeButtons;
		EscapeButtons.Add(NSLOCTEXT("Menu","Menu.Settings","Settings"));
		EscapeButtons.Add(NSLOCTEXT("Menu","Menu.Leave","Leave Fight"));
		EscapeButtons.Add(NSLOCTEXT("Menu","Menu.Continue","Continue"));

		SAssignNew(Slate_EscapeMenu, SVerticalMenuBuilder)
		.IsDoubleClickProtection(true)
		.ButtonsText(EscapeButtons)
		.OnClickAnyButton_UObject(this, &AShooterPlayerState::OnEscapeMenuClickAnyButton);

		MyViewport->AddViewportWidgetContent_AlwaysVisible(Slate_EscapeMenu.ToSharedRef(), 50);
		
		auto Widget_Countdown = SNew(SCountdown)
			.Visibility_UObject(this, &AShooterPlayerState::IsOnProgressMatch_HitTestInvisible)
			.CountdownAt(5)
			.RemainingTime_UObject(this, &AShooterPlayerState::GetGameRemainingTime);

		MyViewport->AddViewportWidgetContent_AlwaysVisible(Widget_Countdown, 5);

		SAssignNew(Slate_BadConnection, SBadConnection);
		MyViewport->AddViewportWidgetContent_AlwaysVisible(Slate_BadConnection.ToSharedRef(), 50);

		SAssignNew(Slate_KilledContainer, SKilledContainer)
		.OnClickedRespawn_UObject(this, &AShooterPlayerState::RequestPlayerRespawn);
		MyViewport->AddUsableViewportWidgetContent_AlwaysVisible(Slate_KilledContainer.ToSharedRef(), 12);
	}

	Super::OnCreateUserInterface();
}

void AShooterPlayerState::Tick(float DeltaSeconds)
{
	Super::Tick(DeltaSeconds);

	if (Slate_BadConnection.IsValid())
	{
		if (GetBaseController() && GetBaseController()->GetNetConnection())
		{
			auto NetConn = GetBaseController()->GetNetConnection();	
			bool bPacketsLost = (NetConn->InPacketsLost > 5 || NetConn->OutPacketsLost > 5);
			bool bHighPing = ExactPing >= 350;

			GEngine->AddOnScreenDebugMessage(68, 1.0f, FColorList::BlueViolet, *FString::Printf(TEXT("AvgLag: %.2f, InPacketsLost: %d, OutPacketsLost: %d"), NetConn->AvgLag, NetConn->InPacketsLost, NetConn->OutPacketsLost));

			Slate_BadConnection->ToggleWidget(bHighPing || bPacketsLost);
		}		
	}
}

void AShooterPlayerState::OnRepGameStateEvent()
{
	if (Slate_GameTimeAndScores.IsValid())
	{
		Slate_GameTimeAndScores->SetGameState(GetWorld()->GetGameState<AShooterGameState>());
	}

	if (Slate_PlayersTable.IsValid())
	{
		Slate_PlayersTable->SetGameState(GetWorld()->GetGameState<AShooterGameState>(), this);
	}
}

void AShooterPlayerState::RequestPlayerRespawn()
{
	if (auto MyCtrl = GetBaseController<AShooterPlayerController>())
	{
		MyCtrl->ServerRestartPlayer();
	}
}

void AShooterPlayerState::OnPlayerTravelToLobby()
{
	GetWorldTimerManager().SetTimer(TimerHandle_PlayerTravelToLobby, this, &AShooterPlayerState::OnClientPlayerTravelToLobby, 5.0f, false, 5.0f);
	OnServerPlayerTravelToLobby();
}

void AShooterPlayerState::OnClientPlayerTravelToLobby_Implementation()
{
	GetWorldTimerManager().ClearTimer(TimerHandle_PlayerTravelToLobby);
	if (const auto PC = GetValidObjectAs<AShooterPlayerController>(GetOwner()))
	{
		PC->PlayerTravelToLobby();
	}
}

bool AShooterPlayerState::OnServerPlayerTravelToLobby_Validate()
{
	return true;
}

void AShooterPlayerState::OnServerPlayerTravelToLobby_Implementation()
{
	UE_LOG(LogInit, Display, TEXT("AShooterPlayerState::nServerPlayerTravelToLobby >> MemberId: %s"), *MemberId.ToString());

	if(const auto gs = GetGameState())
	{
		gs->SendLeaveMemberRequest(MemberId);
	}
	OnClientPlayerTravelToLobby();
}

AShooterGameState* AShooterPlayerState::GetGameState() const
{
	if(const auto world = GetValidWorld())
	{
		return GetValidObjectAs<AShooterGameState>(world->GetGameState());
	}
	return nullptr;
}

AShooterGameState* AShooterPlayerState::GetGameState()
{
	if (const auto world = GetValidWorld())
	{
		return GetValidObjectAs<AShooterGameState>(world->GetGameState());
	}
	return nullptr;
}

AShooterPickup_Weapon* AShooterPlayerState::GetCurrentWeaponPickup() const
{
	if (const auto MyCharacter = GetBasePlayerCharacter<AShooterCharacter>())
	{
		return GetValidObjectAs<AShooterPickup_Weapon>(MyCharacter->GetNearlyOverlapedPickup());
	}
	return nullptr;
}

UShooterAid* AShooterPlayerState::GetAid() const
{
	if (const auto MyCharacter = GetBasePlayerCharacter<AShooterCharacter>())
	{
		return MyCharacter->GetAid();
	}
	return nullptr;
}

bool AShooterPlayerState::IsAllowUseAid() const
{
	if (const auto MyCharacter = GetBasePlayerCharacter<AShooterCharacter>())
	{
		return MyCharacter->IsAllowUseAid();
	}
	return false;
}


UShooterGrenade* AShooterPlayerState::GetGrenade() const
{
	if (const auto MyCharacter = GetBasePlayerCharacter<AShooterCharacter>())
	{
		return MyCharacter->GetGrenade();
	}
	return nullptr;
}

bool AShooterPlayerState::IsAllowUseGrenade() const
{
	if (const auto MyCharacter = GetBasePlayerCharacter<AShooterCharacter>())
	{
		return MyCharacter->IsAllowUseGrenade();
	}
	return false;
}

void AShooterPlayerState::UnregisterPlayerWithSession()
{
	if (!bFromPreviousLevel)
	{
		Super::UnregisterPlayerWithSession();
	}
}

void AShooterPlayerState::ClientInitialize(AController* InController)
{
	Super::ClientInitialize(InController);

	UpdateTeamColors();
}

void AShooterPlayerState::OnRep_TeamColor()
{
	UpdateTeamColors();
}

void AShooterPlayerState::AddBulletsFired(int32 NumBullets)
{
	NumBulletsFired += NumBullets;
}

void AShooterPlayerState::AddRocketsFired(int32 NumRockets)
{
	NumRocketsFired += NumRockets;
}

void AShooterPlayerState::SetQuitter(bool bInQuitter)
{
	bQuitter = bInQuitter;
}

void AShooterPlayerState::CopyProperties(APlayerState* PlayerState)
{	
	Super::CopyProperties(PlayerState);

	AShooterPlayerState* ShooterPlayer = GetValidObjectAs<AShooterPlayerState>(PlayerState);
	if (ShooterPlayer)
	{
		ShooterPlayer->Team = Team;
	}	
}

int32 AShooterPlayerState::GetGrenadeAmount() const
{
	if (IsValidObject(this))
	{
		if (const auto MyItem = GetGrenade())
		{
			return MyItem->GetAmount();
		}
	}

	return 0;
}

float AShooterPlayerState::GetGrenadePercent() const
{
	if (IsValidObject(this))
	{
		if (const auto MyItem = GetGrenade())
		{
			return MyItem->GetExecutePercent();
		}
	}

	return .0f;
}

int32 AShooterPlayerState::GetAidAmount() const
{
	if (IsValidObject(this))
	{
		if (const auto MyItem = GetAid())
		{
			return MyItem->GetAmount();
		}
	}
	return 0;
}

float AShooterPlayerState::GetAidPercent() const
{
	if (IsValidObject(this))
	{
		if (const auto MyItem = GetAid())
		{
			return MyItem->GetExecutePercent();
		}
	}
	return .0f;
}

FReply AShooterPlayerState::OnPlayersTableClicked(const FGeometry& InGeon, const FPointerEvent& InPoint) const
{
	FShooterGameAnalytics::RecordDesignEvent("UI:Game:TableClicked");
	Slate_PlayersTable->ToggleWidget(true);
	return FReply::Handled();
}


void AShooterPlayerState::OnSelectWeapon(AShooterWeapon* InTargetWeapon)
{
	auto MyCharacter = GetBaseCharacter<AShooterCharacter>();
	if (MyCharacter && MyCharacter->IsValidLowLevel())
	{
		MyCharacter->EquipWeapon(InTargetWeapon, true);
	}
}


AShooterWeapon* AShooterPlayerState::GetCurrentWeapon() const
{
	auto MyCharacter = GetBaseCharacter<AShooterCharacter>();
	if (MyCharacter && MyCharacter->IsValidLowLevel())
	{
		return MyCharacter->GetWeapon();
	}

	return nullptr;
}


TArray<AShooterWeapon*> AShooterPlayerState::GetAvalibleWeapons() const
{
	auto MyCharacter = GetBaseCharacter<AShooterCharacter>();
	if (MyCharacter && MyCharacter->IsValidLowLevel())
	{
		return MyCharacter->GetAvalibleWeapons();
	}

	return TArray<AShooterWeapon*>();
}


const FSlateBrush* AShooterPlayerState::GetWeaponPickupIcon() const
{
	static FSlateBrush Empty = FSlateNoResource();
	auto pickup = GetCurrentWeaponPickup();
	if (pickup && pickup->GetInstanceEntity())
	{
		return &pickup->GetInstanceEntity()->GetWeaponProperty().Icon;
	}

	return &Empty;
}

EWeaponPickupBetter::Type AShooterPlayerState::GetWeaponPickupBetter() const
{
	auto MyCharacter = GetBaseCharacter<AShooterCharacter>();
	auto MyPickup = GetCurrentWeaponPickup();

	if (MyCharacter && MyPickup && MyPickup->GetInstanceEntity())
	{
		const auto PickupInstance = MyPickup->GetInstanceEntity();
		if (auto FoundWeapon = MyCharacter->FindWeaponBySlot(PickupInstance->GetItemType()))
		{
			if (FoundWeapon->GetInstanceEntity())
			{
				const float MyWeaponDamage = FGameUtilities::GetWeaponCoefficient(FoundWeapon->GetInstanceEntity(), FoundWeapon->GetInstance()->GetLevel());
				const float PickupWeaponDamage = FGameUtilities::GetWeaponCoefficient(PickupInstance, MyPickup->GetInstanceLevel());

				if (PickupWeaponDamage > MyWeaponDamage)
				{
					return EWeaponPickupBetter::Better;
				}
				else if (MyWeaponDamage > PickupWeaponDamage)
				{
					return EWeaponPickupBetter::Worse;
				}
			}
		}
		else
		{
			return EWeaponPickupBetter::Better;
		}
	}

	return EWeaponPickupBetter::None;
}

int32 AShooterPlayerState::GetGameRemainingTime() const
{
	auto MyGameState = GetWorld() ? GetWorld()->GetGameState<AShooterGameState>() : nullptr;
	if (MyGameState) return MyGameState->GetRemainingTime();
	return 0;
}

void AShooterPlayerState::OnEscapeMenuClickAnyButton(const uint8& InButtonIndex) 
{
	if (InButtonIndex == 0 && Slate_SettingsManager.IsValid())
	{
		FShooterGameAnalytics::RecordDesignEvent("UI:Game:GameMainMenu:SettingsManager");
		Slate_SettingsManager->ToggleWidget(true);
	}
	else if (InButtonIndex == 1)
	{
		FShooterGameAnalytics::RecordDesignEvent("UI:Game:GameMainMenu:TravelToLobby");
		OnPlayerTravelToLobby();
	}

	Slate_EscapeMenu->ToggleWidget(false);
}


void AShooterPlayerState::OnEscapeMenuClicked() const
{
	if (Slate_EscapeMenu.IsValid())
	{
		Slate_EscapeMenu->ToggleWidget(true);
		Touch_EscapeMenu->ForceClearTouch();
	}
}


bool AShooterPlayerState::TargetingIsActivated() const
{
	auto Ctrl = GetValidObjectAs<APlayerController>(GetOwner());
	if (Ctrl && Ctrl->IsLocalController())
	{
		if (auto ShooterCharacter = GetValidObjectAs<AShooterCharacter>(Ctrl->GetPawnOrSpectator()))
		{
			return ShooterCharacter->IsTargeting();
		}
	}

	return false;
}


EVisibility AShooterPlayerState::GetGrenadeVisibility() const
{
	if (GetGrenade() && (GetGrenade()->AnyAmount() || FMath::IsNearlyZero(GetGrenade()->GetExecutePercent()) == false))
	{
		return EVisibility::Visible;
	}
	return  EVisibility::Collapsed;
}

EVisibility AShooterPlayerState::GetKnifeVisibility() const
{
	if (auto ValidChar = GetBaseCharacter<AShooterCharacter>())
	{
		if (auto ValidKnife = ValidChar->GetKnife())
		{
			return ValidKnife->CanActivate(true) ? EVisibility::Visible : EVisibility::Collapsed;
		}
	}

	return EVisibility::Collapsed;
}

EVisibility AShooterPlayerState::GetAidVisibility() const
{
	if (GetAid() && (GetAid()->AnyAmount() || FMath::IsNearlyZero(GetAid()->GetExecutePercent()) == false))
	{
		return EVisibility::Visible;
	}
	return  EVisibility::Collapsed;
}

EVisibility AShooterPlayerState::GetFireVisibility() const
{
	auto MySettings = UShooterGameUserSettings::Get();
	auto MyCharacter = GetBaseCharacter<AShooterCharacter>();
	if (MySettings && MyCharacter && MyCharacter->IsValidLowLevel() && MyCharacter->IsAlive())
	{
		return MySettings->IsEnabledAutoFire() ? EVisibility::Collapsed : EVisibility::Visible;
	}

	return EVisibility::Collapsed;
}


EVisibility AShooterPlayerState::GetWeaponPickupVisibility() const
{
	if (auto pickup = GetCurrentWeaponPickup())
	{
		if (pickup->CanBePickedUp(GetBaseCharacter<AShooterCharacter>()))
		{
			return EVisibility::Visible;
		}
	}
	return  EVisibility::Collapsed;
}

EVisibility AShooterPlayerState::IsAliveVisible() const
{
	auto MyCharacter = GetBaseCharacter<AShooterCharacter>();
	if (MyCharacter && MyCharacter->IsValidLowLevel() && MyCharacter->IsAlive())
	{
		return EVisibility::Visible;
	}

	return EVisibility::Collapsed;
}

EVisibility AShooterPlayerState::IsAliveHitTestInvisible() const
{
	auto MyCharacter = GetBaseCharacter<AShooterCharacter>();
	if (MyCharacter && MyCharacter->IsValidLowLevel() && MyCharacter->IsAlive())
	{
		return EVisibility::HitTestInvisible;
	}

	return EVisibility::Collapsed;
}


EVisibility AShooterPlayerState::IsOnProgressMatch_Visible() const
{
	auto MyGameState = GetWorld() ? GetWorld()->GetGameState<AShooterGameState>() : nullptr;
	if (MyGameState && MyGameState->IsMatchInProgress() && MyGameState->GetRemainingTime() > 0) return EVisibility::Visible;
	return EVisibility::Collapsed;
}


EVisibility AShooterPlayerState::IsOnProgressMatch_HitTestInvisible() const
{
	auto MyGameState = GetWorld() ? GetWorld()->GetGameState<AShooterGameState>() : nullptr;
	if (MyGameState && MyGameState->IsMatchInProgress() && MyGameState->GetRemainingTime() > 0) return EVisibility::HitTestInvisible;
	return EVisibility::Collapsed;
}

void AShooterPlayerState::UpdateTeamColors()
{
	AController* OwnerController = GetValidObjectAs<AController>(GetOwner());
	if (OwnerController != NULL)
	{
		AShooterCharacter* ShooterCharacter = GetValidObjectAs<AShooterCharacter>(OwnerController->GetCharacter());
		if (ShooterCharacter != NULL)
		{
			ShooterCharacter->UpdateTeamColor();
		}
	}
}

uint8 AShooterPlayerState::GetTeamNum() const
{
	if (Team && Team->IsValidLowLevel())
	{
		return Team->GetTeamNum();
	}

	return ATeamInfo::NullTeam;
}

void AShooterPlayerState::SetTeam(ATeamInfo* InTeam)
{
	Team = GetValidObject(InTeam);
	if(Team)
	{
		SafeTeamId = InTeam->GetTeamId();
	}
}

ATeamInfo* AShooterPlayerState::GetTeam() const
{
	return GetValidObject(Team);
}

FGuid AShooterPlayerState::GetTeamId() const
{
	if(auto team = GetTeam())
	{
		if(team->GetTeamId().IsValid())
		{
			return team->GetTeamId();
		}
	}

	return SafeTeamId;
}


int32 AShooterPlayerState::GetNumBulletsFired() const
{
	return NumBulletsFired;
}

int32 AShooterPlayerState::GetNumRocketsFired() const
{
	return NumRocketsFired;
}

bool AShooterPlayerState::IsQuitter() const
{
	return bQuitter;
}

void AShooterPlayerState::ScoreAssist(const int32& Points)
{
	GivePlayerAchievement(EShooterGameAchievements::Assist, 1, Points);
}

void AShooterPlayerState::ScoreKill(AShooterPlayerState* InVictim, const int32& Points)
{
	//====================================================
	if(auto Victim = GetValidObject(InVictim))
	{
		//------------------------------------------
		auto& Value = KillsDataLast.FindOrAdd(Victim->GetDamageDataLast().Weapon);

		//------------------------------------------
		const bool IsFreindly = GetTeamNum() == Victim->GetTeamNum();

		//------------------------------------------
		if(IsFreindly == false)
		{
			GivePlayerAchievement(EShooterGameAchievements::Kills);
			Value++;
		}
	}
	else
	{
		GivePlayerAchievement(EShooterGameAchievements::Kills);
	}

	//====================================================
	ScorePoints(Points);
}

void AShooterPlayerState::ScoreDeath(AShooterPlayerState* InKilledBy, const int32& Points)
{
	//====================================================
	DamageData.Empty();
	DamageDataLast = FPlayerDamageData();

	//====================================================
	for (auto& i : KillsDataLast)
	{
		KillsData.Add(i.Key, i.Value);
	}

	//====================================================
	ResetDominatioKills();
	KillsDataLast.Empty();

	//====================================================
	GivePlayerAchievement(EShooterGameAchievements::Deads);

	//====================================================
	ScorePoints(Points);
}

void AShooterPlayerState::ScorePoints(const int32& Points)
{
	GivePlayerAchievement(EShooterGameAchievements::Score, Points);
}

void AShooterPlayerState::BroadcastDeath_Implementation(class AShooterPlayerState* KillerPlayerState, const UDamageType* KillerDamageType, class AShooterPlayerState* KilledPlayerState)
{	
	for (FConstPlayerControllerIterator It = GetWorld()->GetPlayerControllerIterator(); It; ++It)
	{
		// all local players get death messages so they can update their huds.
		AShooterPlayerController* TestPC = GetValidObjectAs<AShooterPlayerController>(*It);
		if (TestPC && TestPC->IsLocalController())
		{
			TestPC->OnDeathMessage(KillerPlayerState, this, KillerDamageType);				
		}
	}
	

}

void AShooterPlayerState::OnDeathMessage_Implementation(AShooterPlayerState* InKilled, AShooterPlayerState* InKiller, const FKillerInfo& InKillerInfo)
{
	if (Slate_KillList.IsValid())
	{
		Slate_KillList->AddRow(InKilled, InKiller, InKillerInfo.Weapon.ModelId);
	}

	if (InKilled == this && Slate_KilledContainer.IsValid())
	{
		FKillerInfo LocalInfo;

		auto FindModelIdLambda = [&, LocalInventoryComponent = GetInventoryComponent()](const EGameItemType& InFindType) -> FKillerItemInfo
		{
			if (LocalInventoryComponent)
			{
				if (auto FoundItem = LocalInventoryComponent->GetPlayerItemInProfileBySlot(0, InFindType))
				{
					return FKillerItemInfo(FoundItem->GetModelId(), FoundItem->GetLevel());
				}
			}
			return FKillerItemInfo();
		};

		UBasicPlayerItem* MyDefaultItem = nullptr;
		FPlayerProfileItemModel ItemModel;

		if (GetInventoryComponent()->GetDefaultItem(0, ItemModel, MyDefaultItem) && IsValidObject(MyDefaultItem))
		{
			LocalInfo.Weapon = FKillerItemInfo(MyDefaultItem->GetModelId(), MyDefaultItem->GetLevel());
		}

		LocalInfo.Head = FindModelIdLambda(EGameItemType::Head);
		LocalInfo.Torso = FindModelIdLambda(EGameItemType::Torso);
		LocalInfo.Legs = FindModelIdLambda(EGameItemType::Legs);

		Slate_KilledContainer->SetInformation(InKiller, InKillerInfo, InKilled, LocalInfo);
		Slate_KilledContainer->ToggleWidget(true);
	}
}

void AShooterPlayerState::GetLifetimeReplicatedProps( TArray< FLifetimeProperty > & OutLifetimeProps ) const
{
	Super::GetLifetimeReplicatedProps( OutLifetimeProps );

	DOREPLIFETIME(AShooterPlayerState, PlayerCoefficient);
	DOREPLIFETIME(AShooterPlayerState, Team);
}

FString AShooterPlayerState::GetShortPlayerName() const
{
	if( GetPlayerName().Len() > MAX_PLAYER_NAME_LENGTH )
	{
		return GetPlayerName().Left(MAX_PLAYER_NAME_LENGTH) + "...";
	}
	return GetPlayerName();
}

void AShooterPlayerState::InitTestData(UProfileTestData* InTestAsset)
{
	if (InTestAsset && InTestAsset->IsValidLowLevel())
	{
		GetIdentityComponent()->OnAuthorizeAsSimulation();

		FPlayerProfileModel HackedProfile;
		TArray<FPlayerInventoryItemModel> HackedInventory;

		HackedProfile.ProfileId = FGuid::NewGuid();

		for (auto modelid : InTestAsset->GetItemsModelId())
		{
			auto FoundItem = FGameSingletonExtensions::FindItemById<UBasicItemEntity>(modelid);
			if (FoundItem && FoundItem->IsValidLowLevel())
			{
				FPlayerProfileItemModel ItemProf;
				FPlayerInventoryItemModel ItemInventory;
				ItemInventory.EntityId = FGuid::NewGuid();
				ItemInventory.ModelId = modelid;

				ItemProf.EntityId = FGuid::NewGuid();
				ItemProf.ItemId = ItemInventory.EntityId;
				ItemProf.SetSlotId = static_cast<int32>(FoundItem->GetItemProperty().Type);

				HackedProfile.Items.Add(ItemProf);
				HackedInventory.Add(ItemInventory);
			}
		}

		TArray<FPlayerProfileModel> HackedProfiles;
		HackedProfiles.Add(HackedProfile);

		GetInventoryComponent()->OnLoadPlayerInventorySuccessfully(HackedInventory);
		GetInventoryComponent()->OnProfiles(HackedProfiles);
	}
}

void AShooterPlayerState::OnInitializeCharacter()
{
	Super::OnInitializeCharacter();

	AController* OwnerController = Cast<AController>(GetOwner());
	if (OwnerController && Slate_LiveBar.IsValid() && Slate_WeaponBar.IsValid())
	{
		AShooterCharacter* ShooterCharacter = GetValidObjectAs<AShooterCharacter>(OwnerController->GetCharacter());
		if (ShooterCharacter)
		{
			Slate_LiveBar->InitCharacter(ShooterCharacter);
			Slate_WeaponBar->InitCharacter(ShooterCharacter);
		}

		if (Slate_KilledContainer.IsValid())
		{
			Slate_KilledContainer->ToggleWidget(false);
		}

		OnRepGameStateEvent();
	}
}

int32 AShooterPlayerState::GetKills() const
{
	return GetAchievementValueById(EShooterGameAchievements::Kills);
}

int32 AShooterPlayerState::GetDeaths() const
{
	return GetAchievementValueById(EShooterGameAchievements::Deads);
}

int32 AShooterPlayerState::GetScore() const
{
	return GetAchievementValueById(EShooterGameAchievements::Score);
}

void AShooterPlayerState::NotifyPlayerAchievement_Implementation(const int32& InAchievementId, const int32& InTotalAmount, const int32& InAmount, const int32& InScore)
{
	//=====================================
	UBasicGameAchievement* achievement = FGameSingletonExtensions::FindAchievementById(InAchievementId);

	//=====================================
	if(achievement == nullptr)
	{
		UE_LOG(LogInit, Error, TEXT("[AShooterPlayerState::NotifyPlayerAchievement][InAchievementId: %d / InAmount: %d][achievement was nullptr]"), InAchievementId, InAmount);
		return;
	}

	//=====================================
	auto AchievementProperty = achievement->GetAchievementProperty();

	//=====================================
	const FString& AchievementName = AchievementProperty.AchievementName;

	//=====================================
	if (AchievementName.IsEmpty() == false && InTotalAmount > 0)
	{
		auto AchievementEventName = FString::Printf(TEXT("Match:Achievements:%s:More%d"), *AchievementName, InTotalAmount);
		FShooterGameAnalytics::RecordDesignEvent(AchievementEventName);
	}

	//=====================================
	if (AchievementProperty.bPlayNotify && HasLocalClientState())
	{
		if (auto notify = GetValidObject(AchievementProperty.GetNotifySound()))
		{
			UGameplayStatics::PlaySound2D(this, notify, 1.0f, 1.0f, 0.0f, NotifyAchievementSoundConcurrency, this);
		}
	}

	//=====================================
	//	TODO:: Отобразить достижение, например HeadShot, Series Kill
	if (AchievementProperty.bShowNotify && Slate_FightAction.IsValid())
	{
		Slate_FightAction->AddAchievement(FNotifyPlayerAchievement(achievement, InAmount));
	}
}


bool AShooterPlayerState::AddDamageData(const int32& Index, const FPlayerDamageData& Data)
{
	auto &Value = DamageData.FindOrAdd(Index);
	Value += Data;
	DamageDataLast = Data;

	return true;
}

int32 AShooterPlayerState::GivePlayerAchievement(const int32& InAchievementId, const int32& InAmount, const int32& InScore)
{
	//==============================
	const auto ScorePoints =  Super::GivePlayerAchievement(InAchievementId, InAmount, InScore);

	//==============================
	if(ScorePoints)
	{
		Super::GivePlayerAchievement(EShooterGameAchievements::Score, ScorePoints);
	}

	//==============================
	return ScorePoints;
}