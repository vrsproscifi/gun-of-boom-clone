// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "GameFramework/Info.h"
#include "TeamInfo.generated.h"

class AShooterPlayerState;
/**
 * 
 */
UCLASS()
class SHOOTERGAME_API ATeamInfo : public AInfo
{
	GENERATED_BODY()
	
public:

	ATeamInfo();

	// * Add player to this team and remove from other
	UFUNCTION(BlueprintCallable, Category = "Gameplay|Team")
	void AddPlayer(AShooterPlayerState* InPlayer);


	// * Remove player from this team
	UFUNCTION(BlueprintCallable, Category = "Gameplay|Team")
	bool RemovePlayer(AShooterPlayerState* InPlayer);

	UFUNCTION(BlueprintCallable, BlueprintPure, Category = "Gameplay|Team")
		bool IsEqualId(const FGuid& InTeamId) const;

	UFUNCTION(BlueprintCallable, BlueprintPure, Category = "Gameplay|Team")
		bool IsEqualNum(const uint8& InTeamNum) const;

	// * Get this team number
	UFUNCTION(BlueprintCallable, BlueprintPure, Category = "Gameplay|Team")
	uint8 GetTeamNum() const;

	// * Get total score points from this team
	UFUNCTION(BlueprintCallable, BlueprintPure, Category = "Gameplay|Team")
		float GetWinRate() const;

	UFUNCTION(BlueprintCallable, BlueprintPure, Category = "Gameplay|Team")
		int32 GetTargetWeaponLevel() const;

	UFUNCTION(BlueprintCallable, BlueprintPure, Category = "Gameplay|Team")
		int32 GetTargetWeaponUpgrade() const;


	UFUNCTION(BlueprintCallable, BlueprintPure, Category = "Gameplay|Team")
		int32 GetTargetArmourLevel() const;

	

	// * Give score points to this team and get total score from team
	UFUNCTION(BlueprintCallable, Category = "Gameplay|Team")
	float GiveScore(const float& InScorePoints);


	// * Set score points on this team
	UFUNCTION(BlueprintCallable, Category = "Gameplay|Team")
	void SetScore(const float& InScorePoints);


	// * Get total score points from this team
	UFUNCTION(BlueprintCallable, BlueprintPure, Category = "Gameplay|Team")
	float GetScore() const;

	// * Get total score points from this team
	UFUNCTION(BlueprintCallable, BlueprintPure, Category = "Gameplay|Team")
	int32 GetIntScore() const;

	UFUNCTION(BlueprintCallable, BlueprintPure, Category = "Gameplay|Team")
	FString GetStrScore() const;

	// * Set team identifier from guid
	// * Note: team identifier can't set if fully spawned team
	UFUNCTION(BlueprintCallable, Category = "Gameplay|Team")
	bool SetTeamId(const FLokaGuid& InTeamId);


	// * Get team identifier as guid
	UFUNCTION(BlueprintCallable, BlueprintPure, Category = "Gameplay|Team")
	const FLokaGuid& GetTeamId() const;

	// * Get team identifier as string
	UFUNCTION(BlueprintCallable, BlueprintPure, Category = "Gameplay|Team")
	FString GetTeamStrId() const;

	// * Set team identifier from array index
	// * Note: team identifier can't set if fully spawned team
	UFUNCTION(BlueprintCallable, Category = "Gameplay|Team")
	bool SetTeamNum(const uint8& InTeamNum);


	/** returns current number of players on the team; server only */
	UFUNCTION(BlueprintCallable, BlueprintPure, Category = "Gameplay|Team")
	int32 GetSize() const;

	/** returns number of human (not bot) players on the team; server only */
	UFUNCTION(BlueprintCallable, BlueprintPure, Category = "Gameplay|Team")
	int32 GetNumHumans() const;

	UFUNCTION(BlueprintCallable, BlueprintPure, Category = "Gameplay|Team")
	const TArray<AShooterPlayerState*>& GetTeamMembers() const
	{
		return Players;
	}

	UFUNCTION(BlueprintCallable, BlueprintPure, Category = "Gameplay|Team")
	const TArray<AShooterPlayerState*> GetTeamBots() const;

	UFUNCTION(BlueprintCallable, BlueprintPure, Category = "Gameplay|Team")
	const TArray<AShooterPlayerState*> GetTeamHumans() const;


	static uint8 NullTeam;

protected:

	UPROPERTY(VisibleAnywhere, Replicated)	uint8								TeamNum;
	UPROPERTY(VisibleAnywhere, Replicated)	float								TeamScore;
	UPROPERTY(VisibleAnywhere)				FLokaGuid							TeamId;
	UPROPERTY(VisibleAnywhere, Replicated)	TArray<AShooterPlayerState*>		Players;

	//	Для отладки в редакторе
	UPROPERTY(VisibleAnywhere)				mutable float						WinRateCache;
	UPROPERTY(VisibleAnywhere)				mutable float						TargetWeaponUpgradeCache;
	UPROPERTY(VisibleAnywhere)				mutable float						TargetWeaponLevelCache;
	UPROPERTY(VisibleAnywhere)				mutable float						TargetArmourLevelCache;

	
};
