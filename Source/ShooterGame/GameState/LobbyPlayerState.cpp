﻿// Fill out your copyright notice in the Description page of Project Settings.
//====================================
#include "ShooterGame.h"
#include "LobbyPlayerState.h"
#include "LobbyCharacter.h"

#include "GameInstance/ShooterGameViewportClient.h"
#include "Lobby/SLobbyContainer.h"
#include "Containers/STouchLayout.h"
#include "Input/STouchInputLook.h"
#include "Lobby/SSearchFightScreen.h"
//====================================
#include "BasicPlayerItem.h"
#include "Item/Product/BaseProductItemEntity.h"
#include "Item/GameItemType.h"

//====================================
#include "IdentityComponent.h"
#include "ArsenalComponent.h"
#include "SquadComponent.h"
#include "AdsComponent.h"



#include "Lobby/SPersonalFightResult.h"
#include "ProgressComponent.h"
#include "Notify/SFightResults.h"
#include "Lobby/SArsenalContainer.h"
#include "Components/SCurrencyBlock.h"
#include "Lobby/SShopContainer.h"
#include "GameSingletonExtensions.h"
#include "Containers/Arsenal/SImprovementContainer.h"
#include "ShooterGameAchievements.h"
#include "Settings/SSettingsManager.h"
#include "Containers/SProfileContainer.h"
#include "GameMapData.h"
#include "ShooterGameAnalytics.h"
#include "Lobby/SNewLevelContainer.h"
#include "ShooterGameAnalytics.h"
#include "Lobby/SRaitingsContainer.h"
#include "WeaponItemEntity.h"
#include "NotifySoundsData.h"
#include "BasicPlayerController.h"
#include "Notify/SNotifyContainer.h"
#include "Lobby/SPersonalProgressContainer.h"

#include "Lobby/EverydayAward/SDailyEntryTable.h"
#include "Notify/SMessageBox.h"
#include "TutorialData.h"
#include "TutorialPlayerComponent.h"
#include "Lobby/Cases/SAvalibleCasesContainer.h"
#include "Entity/PlayerCaseEntity.h"

#include "Containers/SChestsContainer.h"
#include "CaseProperty.h"
//#include "CrashlyticsBlueprintLibrary.h"
#include "Inventory/ItemCaptureActor.h"

ALobbyPlayerState::ALobbyPlayerState()
	: Super()
{
	GetSquadComponent()->EventOnStartSearch.AddDynamic(this, &ALobbyPlayerState::OnStartSearchFight);
	GetSquadComponent()->EventOnStopSearch.AddDynamic(this, &ALobbyPlayerState::OnStopSearchFight);
	GetSquadComponent()->EventOnSearchFound.AddDynamic(this, &ALobbyPlayerState::OnFoundSearchFight);	

	GetProgressComponent()->OnMatchResultsUpdate.AddDynamic(this, &ALobbyPlayerState::OnMatchResults);
	GetProgressComponent()->OnTopPlayers.AddDynamic(this, &ALobbyPlayerState::OnTopPlayers);


	GetIdentityComponent()->OnPlayerEntryReward.AddDynamic(this, &ALobbyPlayerState::OnPlayerEntryReward);
	GetIdentityComponent()->OnStatisticUpdate.AddDynamic(this, &ALobbyPlayerState::OnPlayerStat);
	GetIdentityComponent()->OnSchedulePlayerLevelUp.AddDynamic(this, &ALobbyPlayerState::OnSchedulePlayerLevelUp);

	AdsComponent = CreateDefaultSubobject<UAdsComponent>(TEXT("AdsComponent"));
	
	//========================
	AdsComponent->OnPlayRewardVideoSuccessDelegate.AddDynamic(this, &ALobbyPlayerState::OnPlayRewardVideoSuccess);
	AdsComponent->OnPlayRewardVideoSuccessDelegate.AddDynamic(GetInventoryComponent(), &UArsenalComponent::OnPlayRewardVideoSuccess);
	AdsComponent->OnPlayRewardVideoSuccessDelegate.AddDynamic(GetProgressComponent(), &UProgressComponent::OnPlayRewardVideoSuccess);
}

int32 ALobbyPlayerState::GetPlayerLevel() const
{
	auto identity = GetIdentityComponent();
	if (identity)
	{
		return identity->GetPlayerInfo().Experience.Level;
	}

	return -1;
}





void ALobbyPlayerState::CreateLobbyContainer(UShooterGameViewportClient* TargetViewport)
{
	//UCrashlyticsBlueprintLibrary::WriteLog("ALobbyPlayerState::CreateLobbyContainer");

	//================================================================
	SAssignNew(Slate_LobbyContainer, SLobbyContainer)
		.IdentityComponent(GetIdentityComponent())
		.SquadComponent(GetSquadComponent())
		.ArsenalComponent(GetInventoryComponent())
		.AdsComponent(GetAdsComponent())
		.OnClickedArsenal_UObject(this, &ALobbyPlayerState::OnClickedArsenalEmpty)
		.OnClickedSettings_UObject(this, &ALobbyPlayerState::OnClickedSettings)
		.OnClickedRaitings_UObject(this, &ALobbyPlayerState::OnClickedRaitings)
		.OnClickedProfile_UObject(this, &ALobbyPlayerState::OnClickedProfile)
		.OnClickedDiscount_UObject(this, &ALobbyPlayerState::OnClickedDiscount)
		.OnClickedChests_UObject(this, &ALobbyPlayerState::OnClickedChests);

	TargetViewport->AddUsableViewportWidgetContent(Slate_LobbyContainer.ToSharedRef(), 10);
}

void ALobbyPlayerState::CreateArsenalContainer(UShooterGameViewportClient* TargetViewport)
{	
	//================================================================
	//UCrashlyticsBlueprintLibrary::WriteLog("ALobbyPlayerState::CreateArsenalContainer");

	SAssignNew(Slate_ArsenalContainer, SArsenalContainer)
		.IdentityComponent(GetIdentityComponent())
		.ArsenalComponent(GetInventoryComponent())
		.OnRequestBuy_UObject(this, &ALobbyPlayerState::OnRequestBuy)
		.OnRequestSelect_UObject(this, &ALobbyPlayerState::OnRequestSelect)
		.OnRequestUpgrade_UObject(this, &ALobbyPlayerState::OnSelectItemToUpgrade)
		.OnActiveItem_UObject(this, &ALobbyPlayerState::OnActiveItem);

	TargetViewport->AddUsableViewportWidgetContent(Slate_ArsenalContainer.ToSharedRef(), 10);
}


void ALobbyPlayerState::CreateImprovementContainer(UShooterGameViewportClient* TargetViewport)
{	
	//================================================================
	//UCrashlyticsBlueprintLibrary::WriteLog("ALobbyPlayerState::CreateImprovementContainer");

	SAssignNew(Slate_ImprovementContainer, SImprovementContainer)
		.OnRequestBuy_UObject(this, &ALobbyPlayerState::OnRequestUpgrade);

	TargetViewport->AddUsableViewportWidgetContent(Slate_ImprovementContainer.ToSharedRef(), 11);
}


bool ALobbyPlayerState::IsAccountServiceAttached(const TEnumAsByte<EAccountService::Type>& InService) const
{
	
	const auto identity = GetIdentityComponent();
	if (identity)
	{
		const auto& Accounts = identity->GetPlayerInfo().Accounts;
		return Accounts.Contains(InService);;
	}
	return false;
}

void ALobbyPlayerState::OnAccountServiceAttach(const TEnumAsByte<EAccountService::Type>& InService, const bool& InAttach)
{
	const auto identity = GetIdentityComponent();
	if (identity)
	{
		if (IsAccountServiceAttached(InService))
		{
			SNotifyContainer::ShowNotify("OnAccountServiceAttach", NSLOCTEXT("Authentication", "Authentication.:OnAccountServiceAttach.Attached", "You have already successfully linked account"));
		}
		else
		{
			identity->OnAttachGoogleSeviceStart();
		}
	}
}

void ALobbyPlayerState::CreateProfileContainer(UShooterGameViewportClient* TargetViewport)
{
	//================================================================
	//UCrashlyticsBlueprintLibrary::WriteLog("ALobbyPlayerState::CreateProfileContainer");

	SAssignNew(Slate_ProfileContainer, SProfileContainer)
		.IsAccountServiceAttached_UObject(this, &ALobbyPlayerState::IsAccountServiceAttached)
		.OnClickedSwitchServiceStatus_UObject(this, &ALobbyPlayerState::OnAccountServiceAttach)
		.IdentityComponent(GetIdentityComponent())
		.ArsenalComponent(GetInventoryComponent())
		.OnClickedChangeName_Lambda([&](const FText& InName)
		{
			if (IsValidObject(this))
			{
				if (auto identity = GetIdentityComponent())
				{
					identity->SendRequestChangeName(InName);
				}
			}
		})
		.OnClosed_Lambda([&]()
		{
			OnInitializeCharacter();
		});

	Slate_ProfileContainer->SetSupportAlternativeCamera(true);
	TargetViewport->AddUsableViewportWidgetContent(Slate_ProfileContainer.ToSharedRef(), 12);

}


void ALobbyPlayerState::CreateShopContainer(UShooterGameViewportClient* TargetViewport)
{
	//================================================================
	//UCrashlyticsBlueprintLibrary::WriteLog("ALobbyPlayerState::CreateShopContainer");

	SAssignNew(Slate_ShopContainer, SShopContainer).OnRequestBuy_UObject(this, &ALobbyPlayerState::OnRequestProductBuy);

	TargetViewport->AddUsableViewportWidgetContent(Slate_ShopContainer.ToSharedRef(), 50);
	
	Slate_ShopContainer->OnFill(FGameSingletonExtensions::GetItems<UBaseProductItemEntity>());

}



void ALobbyPlayerState::CreateSearchFightScreen(UShooterGameViewportClient* TargetViewport)
{
	//================================================================
	//UCrashlyticsBlueprintLibrary::WriteLog("ALobbyPlayerState::CreateSearchFightScreen");

	SAssignNew(Slate_SearchFightScreen, SSearchFightScreen)
		.OnCancel_Lambda([&]()
	{
		if (auto MyMatchmaking = GetSquadComponent())
		{
			FSessionMatchOptions Empty;
			MyMatchmaking->SendRequestToggleSearch(Empty, false);
		}
		else
		{
			UE_LOG(LogInit, Error, TEXT("[SSearchFightScreen::OnCancel_Lambda][GetSquadComponent was nullptr]"));
			FShooterGameAnalytics::RecordErrorEvent(EErrorSeverity::warning, "SSearchFightScreen::OnCancel_Lambda | GetSquadComponent was nullptr");
		}
	});
	TargetViewport->AddViewportWidgetContent(Slate_SearchFightScreen.ToSharedRef(), 100);

}


void ALobbyPlayerState::CreatePersonalFightResult(UShooterGameViewportClient* TargetViewport)
{
	//================================================================
	//UCrashlyticsBlueprintLibrary::WriteLog("ALobbyPlayerState::CreatePersonalFightResult");

	SAssignNew(Slate_PersonalFightResult, SPersonalFightResult)
	.OnRequestApplyMatchPremiumAccountBonus_Lambda([&, bHasPremiumAccount = HasPremiumAccount()](const FGuid& InPlayerMatchMemberId, const bool& InHasPremiumAccount, const int32& InTargetPremiumAccount)
	{
		if (auto Progress = GetProgressComponent())
		{
			const bool bHasPremiumAccountFinal = bHasPremiumAccount || InHasPremiumAccount;
			Progress->SendRequestAdditionalBonus(InPlayerMatchMemberId, bHasPremiumAccountFinal, InTargetPremiumAccount);
		}
		else
		{
			FShooterGameAnalytics::RecordErrorEvent(EErrorSeverity::warning, "OnRequestBuyPremium_Lambda | GetProgressComponent was nullptr");
		}
	})
	.OnRequestApplyMatchAdsBonus_Lambda([&](const FGuid& InPlayerMatchMemberId)
	{
		PlayRewardVideo(FPlayRewardedVideoRequest("ApplyMatchAdsBonus", InPlayerMatchMemberId.ToString()));
	});

	if (auto Progress = GetProgressComponent())
	{
		Progress->OnApplyMatchPremiumAccountBonus.BindLambda([&]()
		{
			if (Slate_PersonalFightResult.IsValid() && Slate_PersonalFightResult.Get())
			{
				Slate_PersonalFightResult->OnApplyMatchPremiumAccountBonus();
			}
			else
			{
				FShooterGameAnalytics::RecordErrorEvent(EErrorSeverity::warning, "OnApplyMatchPremiumAccountBonus | Slate_PersonalFightResult was nullptr");
			}
		});

		Progress->OnApplyMatchAdsBonus.BindLambda([&]()
		{
			if (Slate_PersonalFightResult.IsValid() && Slate_PersonalFightResult.Get())
			{
				Slate_PersonalFightResult->OnApplyMatchAdsBonus();
			}
			else
			{
				FShooterGameAnalytics::RecordErrorEvent(EErrorSeverity::warning, "OnApplyMatchAdsBonus | Slate_PersonalFightResult was nullptr");
			}
		});
	}
	else
	{
		FShooterGameAnalytics::RecordErrorEvent(EErrorSeverity::warning, "OnMatchUsedPremiumAccount | GetProgressComponent was nullptr");
	}

	TargetViewport->AddUsableViewportWidgetContent(Slate_PersonalFightResult.ToSharedRef(), 110);
}


void ALobbyPlayerState::CreateFightResults(UShooterGameViewportClient* TargetViewport)
{
	//================================================================
	//UCrashlyticsBlueprintLibrary::WriteLog("ALobbyPlayerState::CreateFightResults");

	//================================================================
	SAssignNew(Slate_FightResults, SFightResults)
		.OnPlayerSelected_UObject(this, &ALobbyPlayerState::OnRequestPlayerStat);
	TargetViewport->AddUsableViewportWidgetContent(Slate_FightResults.ToSharedRef(), 120);

}


void ALobbyPlayerState::CreateNewLevelContainer(UShooterGameViewportClient* TargetViewport)
{
	//================================================================
	//UCrashlyticsBlueprintLibrary::WriteLog("ALobbyPlayerState::CreateNewLevelContainer");

	SAssignNew(Slate_NewLevelContainer, SNewLevelContainer)
		.OnClickedArsenal_UObject(this, &ALobbyPlayerState::OnClickedArsenal)
		.IdentityComponent(GetIdentityComponent());
	Slate_NewLevelContainer->SetSupportAlternativeCamera(true);
	TargetViewport->AddUsableViewportWidgetContent(Slate_NewLevelContainer.ToSharedRef(), 150);

}



void ALobbyPlayerState::CreatePersonalProgressContainer(UShooterGameViewportClient* TargetViewport)
{
	//================================================================
	//UCrashlyticsBlueprintLibrary::WriteLog("ALobbyPlayerState::CreatePersonalProgressContainer");

	SAssignNew(Slate_PersonalProgressContainer, SPersonalProgressContainer);
	TargetViewport->AddUsableViewportWidgetContent(Slate_PersonalProgressContainer.ToSharedRef(), 150);

}


void ALobbyPlayerState::CreateRaitingsContainer(UShooterGameViewportClient* TargetViewport)
{
	//================================================================
	//UCrashlyticsBlueprintLibrary::WriteLog("ALobbyPlayerState::CreateRaitingsContainer");

	SAssignNew(Slate_RaitingsContainer, SRaitingsContainer)
		.OnPlayerSelected_UObject(this, &ALobbyPlayerState::OnRequestPlayerStat);
	Slate_RaitingsContainer->SetSupportAlternativeCamera(true);
	TargetViewport->AddUsableViewportWidgetContent(Slate_RaitingsContainer.ToSharedRef(), 10);

}



void ALobbyPlayerState::BindIdentityComponent()
{
	//================================================================
	//UCrashlyticsBlueprintLibrary::WriteLog("ALobbyPlayerState::BindIdentityComponent | 0");

	if (auto arsenal = GetInventoryComponent())
	{
		arsenal->OnImproveItem.AddDynamic(this, &ALobbyPlayerState::OnImproveItem);

		arsenal->OnPlayerNotEnoughMoney.AddLambda([&](const FGameItemCost& InNotEnoughMoney)
		{
			OnShowShop(InNotEnoughMoney.Currency);
		});
	}
}

void ALobbyPlayerState::CreateCurrencyBlock(UShooterGameViewportClient* TargetViewport)
{
	//================================================================
	auto OpenArsenalLambda = [&](EGameItemType InType)
	{
#if WITH_EDITOR
		FGameMatchInformation FakeInfo;
		FGameMatchMemberInformation FakeMember;

		FakeMember.PlayerId = GetIdentityComponent()->GetPlayerInfo().PlayerId;
		FakeMember.Achievements.Add(FPlayerGameAchievement(EShooterGameAchievements::Money, 5000));
		FakeMember.Achievements.Add(FPlayerGameAchievement(EShooterGameAchievements::Experience, 8000));
		FakeMember.State = EMatchMemberHistoryState::HasUsedEveryDayBooster | EMatchMemberHistoryState::HasUsedPrimeTimeBooster;		

		FakeInfo.Members.Add(FakeMember);

		Slate_PersonalFightResult->SetInformation(FakeInfo, FakeMember.PlayerId);
		Slate_PersonalFightResult->ToggleWidget(true);
#else
		if (Slate_ArsenalContainer.IsValid())
		{
			const auto FoundItems = FGameSingletonExtension::FindItemsByType(InType);
			if (FoundItems.Num())
			{
				Slate_ArsenalContainer->ToggleWidget(true);
				Slate_ArsenalContainer->ScrollTo(FoundItems[0]);
			}
		}
#endif
	};
	//================================================================
	SCurrencyBlock::Get();
	SCurrencyBlock::Get()->SetIdentityComponent(GetIdentityComponent());
	SCurrencyBlock::Get()->SetArsenalComponent(GetInventoryComponent());
	SCurrencyBlock::Get()->OnClickedCash.BindUObject(this, &ALobbyPlayerState::OnShowShop);
	SCurrencyBlock::Get()->OnClickedHealth.BindLambda(OpenArsenalLambda, EGameItemType::Stimulant);
	SCurrencyBlock::Get()->OnClickedArmour.BindLambda(OpenArsenalLambda, EGameItemType::Torso);
	SCurrencyBlock::Get()->OnClickedGrenade.BindLambda(OpenArsenalLambda, EGameItemType::Grenade);
}

void ALobbyPlayerState::CreateTouchLayout(UShooterGameViewportClient* TargetViewport)
{
	//================================================================
	SAssignNew(Slate_TouchLayout, STouchLayout)
	+ STouchLayout::Slot(TEXT("TouchBackground")).HAlign(HAlign_Fill).VAlign(VAlign_Fill)
	[
		SNew(STouchInputLook)
		.AxisY("LookUp")
		.AxisX("Turn")
	];

	TargetViewport->AddViewportWidgetContent_AlwaysVisible(Slate_TouchLayout.ToSharedRef(), 2);
}


void ALobbyPlayerState::OnCreateUserInterface()
{
	if (UShooterGameViewportClient* TargetViewport = UShooterGameViewportClient::Get())
	{
		CreateCurrencyBlock(TargetViewport);
		CreateTouchLayout(TargetViewport);

		SAssignNew(Slate_ChestsContainer, SChestsContainer)
		.OnClickedChest_UObject(this, &ALobbyPlayerState::OnClickedChest);
		TargetViewport->AddUsableViewportWidgetContent(Slate_ChestsContainer.ToSharedRef(), 10);

		CreateLobbyContainer(TargetViewport);
		CreateArsenalContainer(TargetViewport);
		CreateImprovementContainer(TargetViewport);
		CreateProfileContainer(TargetViewport);
		CreateShopContainer(TargetViewport);

		CreateSearchFightScreen(TargetViewport);
		CreatePersonalFightResult(TargetViewport);
		CreateFightResults(TargetViewport);
		CreateNewLevelContainer(TargetViewport);

		GetInventoryComponent()->OnChestsUpdate.AddDynamic(this, &ALobbyPlayerState::OnPlayerChestsUpdate);

		CreatePersonalProgressContainer(TargetViewport);
		CreateRaitingsContainer(TargetViewport);
		BindIdentityComponent();

#if WITH_EDITOR && false // Test tutorial

		auto Tutorials = FGameSingletonExtension::GetDataAssets<UTutorialData>();
		if (Tutorials.Num())
		{
			auto RandomTutorial = Tutorials[FMath::RandRange(0, Tutorials.Num() - 1)];
			GetTutorialComponent()->StartTutorial(RandomTutorial);
		}
#endif
	}

	//=============================================================


	//=============================================================
	Super::OnCreateUserInterface();
}


bool ALobbyPlayerState::PlayRewardVideo(const FPlayRewardedVideoRequest& InVideoRequest) const
{
	//=============================================================
	auto Ads = GetAdsComponent();

	//=============================================================
	if(Ads)
	{
		return Ads->PlayRewardVideo(InVideoRequest);
	}

	//=============================================================
	return false;
}


UAdsComponent* ALobbyPlayerState::GetAdsComponent() const
{
	if (IsValidObject(this))
	{
		return GetValidObject(AdsComponent);
	}
	return nullptr;
}



void ALobbyPlayerState::OnClickedDiscount(UBasicItemEntity* InTargetBasicItem)
{
	if (InTargetBasicItem)
	{
		//UCrashlyticsBlueprintLibrary::LogFbCustomEvent("Open_Special_Offer");
		
		if (IsValidObjectAs<UBaseProductItemEntity>(InTargetBasicItem))
		{
			if (Slate_ShopContainer.IsValid())
			{
				RequestUpdateCurrencyRegion();
				Slate_ShopContainer->ToggleWidget(true);
				Slate_ShopContainer->ScrollTo(InTargetBasicItem);
			}
		}
		else if (Slate_ArsenalContainer.IsValid())
		{
			Slate_ArsenalContainer->ToggleWidget(true);
			Slate_ArsenalContainer->ScrollTo(InTargetBasicItem);
		}
	}
}

void ALobbyPlayerState::OnFullyLoadedData()
{
	//=============================================================
	RequestUpdateCurrencyRegion();

	//=============================================================
	if (Slate_LobbyContainer.IsValid() && Slate_ArsenalContainer.IsValid() && Slate_ChestsContainer.IsValid())
	{
		if (!Slate_ArsenalContainer->IsInInteractiveMode() && !Slate_ChestsContainer->IsInInteractiveMode())
		{
			Slate_LobbyContainer->ToggleWidget(true);
		}

/*		if (auto TargetViewport = UShooterGameViewportClient::Get())
		{
			TargetViewport->ToggleSlateControll(true);
		}*/	
	}

	Super::OnFullyLoadedData();
}

void ALobbyPlayerState::OnRequestProductBuy(const UBaseProductItemEntity* InEntity)
{
#if UE_BUILD_DEVELOPMENT
	GEngine->AddOnScreenDebugMessage(-1, 5.f, FColor::Red, TEXT("[OnRequestProductBuy]"));
#endif
	if (auto arsenal = GetInventoryComponent())
	{
		arsenal->OnMarketRequestBuy(InEntity);
	}
	else
	{
		FShooterGameAnalytics::RecordErrorEvent(EErrorSeverity::warning, "Slate_ShopContainer:GetInventoryComponent was nullptr #2");
		UE_LOG(LogInit, Error, TEXT("[SShopContainer::OnRequestBuy_Lambda][GetInventoryComponent was nullptr]"));
	}
}


void ALobbyPlayerState::OnRequestUpgrade(const UBasicPlayerItem* InItem, const int32& InLevel)
{
	if (auto MyArsenal = GetInventoryComponent())
	{
		MyArsenal->OnMarketRequestUpgrade(InItem, InLevel);
	}
	else
	{
		FShooterGameAnalytics::RecordErrorEvent(EErrorSeverity::warning, "Slate_ImprovementContainer:GetInventoryComponent was nullptr #1");
	}
}

void ALobbyPlayerState::OnSelectItemToUpgrade(const UBasicPlayerItem* InItem)
{
	if (Slate_ImprovementContainer.IsValid())
	{
		if (InItem && InItem->IsValidLowLevel())
		{
			//=====================================================
			if (auto entity = InItem->GetEntityBase())
			{
				FShooterGameAnalytics::RecordContentView(entity->GetModelName(), entity->GetItemProperty().Title.ToString(), entity->GetItemTypeName());
			}
			else
			{
				FShooterGameAnalytics::RecordErrorEvent(EErrorSeverity::warning, "Slate_ImprovementContainer:Entity was nullptr #1");
			}

			//=====================================================
			Slate_ImprovementContainer->SetInstance(const_cast<UBasicPlayerItem*>(InItem));
			Slate_ImprovementContainer->ToggleWidget(true);
		}
		else
		{
			FShooterGameAnalytics::RecordErrorEvent(EErrorSeverity::warning, "Slate_ImprovementContainer:InItem was nullptr #1");
		}
	}
	else
	{
		FShooterGameAnalytics::RecordErrorEvent(EErrorSeverity::warning, "Slate_ImprovementContainer was nullptr #1");
	}
}


void ALobbyPlayerState::OnRequestSelect(const UBasicPlayerItem* InItem)
{
	if (auto arsenal = GetInventoryComponent())
	{
		arsenal->OnArsenalRequestEquip(InItem);
	}
	else
	{
		UE_LOG(LogInit, Error, TEXT("[SArsenalContainer::OnRequestSelect_Lambda][GetInventoryComponent was nullptr]"));
		FShooterGameAnalytics::RecordErrorEvent(EErrorSeverity::warning, "OnRequestSelect_Lambda | GetInventoryComponentwas nullptr #1");
	}
}

void ALobbyPlayerState::OnRequestBuy(const UBasicPlayerItem* InItem)
{
	if (auto arsenal = GetInventoryComponent())
	{
		arsenal->OnArsenalRequestBuy(InItem);
	}
	else
	{
		UE_LOG(LogInit, Error, TEXT("[SArsenalContainer::OnRequestBuy_Lambda][GetInventoryComponent was nullptr]"));
		FShooterGameAnalytics::RecordErrorEvent(EErrorSeverity::warning, "OnRequestBuy_Lambda | GetInventoryComponentwas nullptr #1");
	}
}

void ALobbyPlayerState::OnActiveItem(const UBasicPlayerItem* InItem)
{
	if (IsValidObject(ItemCaptureActor) && IsValidObject(InItem) && IsValidObject(InItem->GetEntityBase()))
	{
		ItemCaptureActor->SetPreviewMesh(InItem->GetEntityBase()->GetPreviewMeshData().Mesh, InItem->GetEntityBase()->GetPreviewSettings());
	}
}

void ALobbyPlayerState::OnClickedArsenalEmpty()
{
	OnClickedArsenal(nullptr);
}

void ALobbyPlayerState::OnClickedArsenal(UBasicItemEntity* InItem)
{
	if (Slate_ArsenalContainer.IsValid())
	{
		Slate_ArsenalContainer->ToggleWidget(true);

		if (IsValidObject(InItem))
		{
			Slate_ArsenalContainer->ScrollTo(InItem);
		}
	}
	else
	{
		FShooterGameAnalytics::RecordErrorEvent(EErrorSeverity::warning, "Slate_ArsenalContainer was nullptr #1");
	}
}

void ALobbyPlayerState::OnClickedSettings()
{
	if (Slate_SettingsManager.IsValid())
	{
		Slate_SettingsManager->ToggleWidget(true);
	}
	else
	{
		FShooterGameAnalytics::RecordErrorEvent(EErrorSeverity::warning, "Slate_SettingsManager was nullptr #1");
	}
}

void ALobbyPlayerState::OnClickedRaitings()
{
	if (auto ProgressComp = GetProgressComponent())
	{
		ProgressComp->StartRequestTopPlayers();
	}
}
void ALobbyPlayerState::OnClickedProfile()
{
	OnRequestPlayerStat(FGuid());
}

void ALobbyPlayerState::OnClickedChests()
{
	if (Slate_ChestsContainer.IsValid())
	{
		Slate_ChestsContainer->ToggleWidget(true);
	}
}

void ALobbyPlayerState::OnClickedChest(const TWeakObjectPtr<UPlayerCaseEntity>& InChest)
{
	//==============================================
	if (IsValidObject(InChest) == false)
	{
		return;
	}

	//==============================================
	const auto EntityId = InChest->GetEntityId();

	//==============================================
	const auto CaseEntityProperty = InChest->GetCaseEntityProperty();
	if (CaseEntityProperty)
	{
		if (CaseEntityProperty->CaseType == EGameCaseType::EveryDayAds && InChest->IsElapsedLastActive())
		{
			if (IsValidObject(AdsComponent))
			{
				GetAdsComponent()->PlayRewardVideo(FPlayRewardedVideoRequest("GetFreeDayliCase", EntityId.ToString()));
			}
		}
		else
		{
			GetInventoryComponent()->SendRequestOpenPlayerCase(EntityId);
		}
	}
	else
	{
		GetInventoryComponent()->SendRequestOpenPlayerCase(EntityId);
	}
}

void ALobbyPlayerState::OnShowShop(EGameCurrency InCurrency)
{
//#if WITH_EDITOR
//	if (Slate_PersonalFightResult.IsValid())
//	{
//		Slate_PersonalFightResult->ToggleWidget(true);
//	}
//#else
	if (Slate_ShopContainer.IsValid())
	{
		RequestUpdateCurrencyRegion();

		Slate_ShopContainer->ToggleWidget(true, InCurrency);
	}
	else
	{
		FShooterGameAnalytics::RecordErrorEvent(EErrorSeverity::warning, "Slate_ShopContainer was nullptr #1");

		QueueBegin(SNotifyContainer, "Slate_ShopContainer")
			SNotifyContainer::Get()->SetContent(FText::FromString("Slate_ShopContainer: Invalid"));
			SNotifyContainer::Get()->SetDisplayTime(1.0f);
			SNotifyContainer::Get()->ToggleWidget(true);
		QueueEnd
	}
//#endif
}

void ALobbyPlayerState::OnInventoryUpdated(const TArray<UBasicPlayerItem*> InData, const bool IsInternal)
{
	Super::OnInventoryUpdated(InData, IsInternal);

	if (Slate_ArsenalContainer.IsValid())
	{
		Slate_ArsenalContainer->OnFill(InData);
	}
}

void ALobbyPlayerState::OnImproveItem()
{
	if (Slate_ImprovementContainer.IsValid() && Slate_ImprovementContainer->IsInInteractiveMode())
	{
		Slate_ImprovementContainer->RequestScroll();
	}
}

void ALobbyPlayerState::OnRequestPlayerStat(const FGuid& InPlayerId)
{
	if (GetIdentityComponent())
	{
		GetIdentityComponent()->SendRequestPlayerStatistic(InPlayerId);
	}
}

void ALobbyPlayerState::OnPlayerStat()
{
	if (Slate_ProfileContainer.IsValid())
	{
		Slate_ProfileContainer->SetInformation(GetIdentityComponent()->Statistic);
		Slate_ProfileContainer->ToggleWidget(true);

		if (auto LobbyChar = GetBaseCharacter<ALobbyCharacter>())
		{
			if (auto FoundItem = FGameSingletonExtension::FindItemById<UWeaponItemEntity>(GetIdentityComponent()->Statistic.DefaulItemModelId))
			{
				LobbyChar->GetWeaponComponent()->SetSkeletalMesh(FoundItem->GetFPPMeshData().Mesh);
			}
		}
	}
	else
	{
		FShooterGameAnalytics::RecordErrorEvent(EErrorSeverity::warning, "Slate_ProfileContainer was nullptr #1");
	}
}

void ALobbyPlayerState::OnStartSearchFight()
{
	if (Slate_SearchFightScreen.IsValid())
	{
		Slate_SearchFightScreen->SetFound(false);
		Slate_SearchFightScreen->ToggleWidget(true);

		auto MyCtrl = GetBaseController();
		auto NotifySounds = FGameSingletonExtension::GetDataAssets<UNotifySoundsData>();
		if (NotifySounds.IsValidIndex(0) && NotifySounds[0] && NotifySounds[0]->IsValidLowLevel() && MyCtrl && MyCtrl->IsValidLowLevel())
		{
			FSlateApplication::Get().PlaySound(NotifySounds[0]->Notify_FightSearch);
			//MyCtrl->ClientPlayForceFeedback(NotifySounds[0]->Feedback_FightSearch, false, false, TEXT("Feedback_FightSearch"));
		}
	}
	else
	{
		FShooterGameAnalytics::RecordErrorEvent(EErrorSeverity::warning, "OnStartSearchFight | Slate_SearchFightScreen was nullptr");
	}
}

void ALobbyPlayerState::OnStopSearchFight()
{
	if (Slate_SearchFightScreen.IsValid())
	{
		Slate_SearchFightScreen->ToggleWidget(false);
	}
	else
	{
		FShooterGameAnalytics::RecordErrorEvent(EErrorSeverity::warning, "OnStopSearchFight | Slate_SearchFightScreen was nullptr");
	}
}

void ALobbyPlayerState::OnFoundSearchFight()
{
	if (Slate_SearchFightScreen.IsValid())
	{
		auto MapIndex = GetSquadComponent()->GetSqaudInformation().Options.GameMap;
		auto MapAssets = FGameSingletonExtensions::GetDataAssets<UGameMapData>();
		for (auto MapAsset : MapAssets)
		{
			if (MapIndex == MapAsset->ToFlag())
			{
				MapIndex = MapAsset->GetValue();
				break;
			}
		}

		
		Slate_SearchFightScreen->SetFound(true, MapIndex);

		auto MyCtrl = GetBaseController();
		auto NotifySounds = FGameSingletonExtension::GetDataAssets<UNotifySoundsData>();
		if (NotifySounds.IsValidIndex(0) && NotifySounds[0] && NotifySounds[0]->IsValidLowLevel() && MyCtrl && MyCtrl->IsValidLowLevel())
		{
			FSlateApplication::Get().PlaySound(NotifySounds[0]->Notify_FightFound);
			//MyCtrl->ClientPlayForceFeedback(NotifySounds[0]->Feedback_FightFound, false, false, TEXT("Feedback_FightFound"));
		}
	}
	else
	{
		FShooterGameAnalytics::RecordErrorEvent(EErrorSeverity::warning, "OnFoundSearchFight | Slate_SearchFightScreen was nullptr");
	}
}

void ALobbyPlayerState::OnMatchResults(const TArray<FGameMatchInformation>& InData)
{
	if(Slate_PersonalFightResult.IsValid() == false)
	{
		FShooterGameAnalytics::RecordErrorEvent(EErrorSeverity::warning, "OnMatchResults | Slate_PersonalFightResult was nullptr");
	}

	if (Slate_FightResults.IsValid() == false)
	{
		FShooterGameAnalytics::RecordErrorEvent(EErrorSeverity::warning, "OnMatchResults | Slate_FightResults was nullptr");
	}

	if (InData.Num() && Slate_PersonalFightResult.IsValid() && Slate_FightResults.IsValid())
	{
		Slate_FightResults->SetInformation(InData.Last(), GetIdentityComponent()->GetPlayerInfo().PlayerId);
		Slate_FightResults->ToggleWidget(true);

		Slate_PersonalFightResult->SetInformation(InData.Last(), GetIdentityComponent()->GetPlayerInfo().PlayerId);

		Slate_PersonalFightResult->ToggleWidget(true);
	}
}


void ALobbyPlayerState::OnPlayerEntryReward(const int32& InRewardDay)
{
	//	Обновляем информацию о доступных кейсах
	//	Кейс может быть выдан после показа формы
	auto Inventory = GetInventoryComponent();
	if (Inventory)
	{
		Inventory->SendRequestPlayerCases();
	}

	QueueBegin(SMessageBox, "OnPlayerEntryReward", InRewardDay)

		SMessageBox::Get()->SetHeaderText(NSLOCTEXT("DailyReward", "UArsenalComponent.OnPlayerEntryReward.Title", "Daily reward"));
		SMessageBox::Get()->SetEnableClose(true);
		SMessageBox::Get()->SetContent
		(
			SNew(SDailyEntryTable)
			.CurrentSelectedDay(InRewardDay)
			.OnGetDailyRewardClicked_Lambda([&](const int32& InCurrentDay)
			{
				//UCrashlyticsBlueprintLibrary::LogFbCustomEventKeyValue("Lobby_Clicked_OpenDailyReward", "Day", FString::FromInt(InCurrentDay));

				auto lInventory = GetInventoryComponent();
				if (lInventory)
				{
					lInventory->OnOpenDayliRewardClicked(InRewardDay);
				}
				else
				{
					SNotifyContainer::ShowNotify("OnPlayerEntryReward", NSLOCTEXT("DailyReward", "UArsenalComponent.OnPlayerEntryReward.InventoryWasNullptr", "Inventory was nullptr"));
				}
			})
		);
		SMessageBox::Get()->SetButtonsText();
		SMessageBox::Get()->ToggleWidget(true);
	QueueEnd
	
}


void ALobbyPlayerState::OnPlayerChestsUpdate(const TArray<UPlayerCaseEntity*>& InChests)
{
	if (Slate_ChestsContainer.IsValid())
	{
		Slate_ChestsContainer->OnFill(InChests);
	}
}

void ALobbyPlayerState::OnSchedulePlayerLevelUpExecute()
{
	if (IsValidObject(this) && Slate_NewLevelContainer.IsValid())
	{
		Slate_NewLevelContainer->ToggleWidget(false);
		Slate_NewLevelContainer->ToggleWidget(true);
	}

	if (auto tm = GetTimerManager())
	{
		tm->ClearTimer(ScheduleShowLevelUpTimerHandler);
	}
}

void ALobbyPlayerState::OnSchedulePlayerLevelUp()
{	
	if (auto tm = GetTimerManager())
	{
		tm->SetTimer(ScheduleShowLevelUpTimerHandler, FTimerDelegate::CreateUObject(this, &ALobbyPlayerState::OnSchedulePlayerLevelUpExecute), 1.0f, true);
	}
}

void ALobbyPlayerState::OnTopPlayers(const FRatingTopContainer& InRatingContainer)
{
	if (Slate_RaitingsContainer.IsValid())
	{
		FRatingTopContainer RatingTopContainer = InRatingContainer;

		if (InRatingContainer.PlayerRating.Num() == 0)
		{			
			for (SIZE_T i = 0; i < 3; ++i)
			{
				FPlayerRatingTopContainer Tmp;
				Tmp.Name = FString::Printf(TEXT("Test_%d"), i);
				Tmp.Score = FMath::RandRange(10, 6535555);
				RatingTopContainer.PlayerRating.Add(Tmp);
			}			
		}

		RatingTopContainer.PlayerRating.Sort([](const FPlayerRatingTopContainer& A, const FPlayerRatingTopContainer& B)
		{
			return A.Score > B.Score;
		});

		Slate_RaitingsContainer->SetInformation(RatingTopContainer);
		Slate_RaitingsContainer->ToggleWidget(true);

		if (auto MyCharacter = GetBaseCharacter<ALobbyCharacter>())
		{
			MyCharacter->SetArmMode(true);
		}
	}
}

