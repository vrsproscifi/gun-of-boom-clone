//====================================
#include "ShooterGame.h"
#include "DominationSeriesKill.h"

void FDominationSeriesKill::ResetKills()
{
	Kills.Empty(16);
}

void FDominationSeriesKill::ResetKills(const uint32& InPlayerId)
{
	if (Kills.Contains(InPlayerId))
	{
		Kills[InPlayerId] = 0;
	}
	else
	{
		Kills.Add(InPlayerId) = 0;
	}
}

uint32 FDominationSeriesKill::GetKills(const uint32& InPlayerId) const
{
	if (Kills.Contains(InPlayerId))
	{
		return Kills[InPlayerId];
	}
	else
	{
		return 0;
	}
}

uint32 FDominationSeriesKill::AddKills(const uint32& InPlayerId)
{
	if (Kills.Contains(InPlayerId))
	{
		return Kills[InPlayerId]++;
	}
	else
	{
		return Kills.Add(InPlayerId) = 1;
	}
}
