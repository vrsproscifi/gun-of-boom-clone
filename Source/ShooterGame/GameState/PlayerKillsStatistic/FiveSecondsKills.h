#pragma once

//==========================================
#include "FiveSecondsKills.generated.h"

//==========================================

USTRUCT()
struct FFiveSecondsKills
{
	GENERATED_USTRUCT_BODY()

		FFiveSecondsKills()
		: Kills(0)
	{

	}

	UPROPERTY(VisibleInstanceOnly)
		FDateTime StartDate;

	UPROPERTY(VisibleInstanceOnly)
		uint32 Kills;

	uint32 AddKills();

	void RestartSeries();
};