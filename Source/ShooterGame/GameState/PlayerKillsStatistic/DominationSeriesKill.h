#pragma once

//==========================================
#include "DominationSeriesKill.generated.h"

//==========================================

USTRUCT()
struct FDominationSeriesKill
{
	GENERATED_USTRUCT_BODY()

		UPROPERTY(VisibleInstanceOnly)
		TMap<uint32, uint32> Kills;

	void ResetKills();

	void ResetKills(const uint32& InPlayerId);

	uint32 GetKills(const uint32& InPlayerId) const;

	uint32 AddKills(const uint32& InPlayerId);
};