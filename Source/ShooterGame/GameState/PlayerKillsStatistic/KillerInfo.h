#pragma once

//==========================================
#include "KillerInfo.generated.h"

USTRUCT()
struct FKillerItemInfo
{
	GENERATED_USTRUCT_BODY()

	FKillerItemInfo()
		: ModelId(INDEX_NONE)
		, Level(0)
	{

	}

	FKillerItemInfo(const int32& InModelId, const uint8& InLevel)
		: ModelId(InModelId)
		, Level(InLevel)
	{

	}

	UPROPERTY()		int32		ModelId;
	UPROPERTY()		uint8		Level;
};

//==========================================
USTRUCT()
struct FKillerInfo
{
	GENERATED_USTRUCT_BODY()

	UPROPERTY()		FKillerItemInfo		Weapon;
	UPROPERTY()		FKillerItemInfo		Head;
	UPROPERTY()		FKillerItemInfo		Torso;
	UPROPERTY()		FKillerItemInfo		Legs;
	UPROPERTY()		FVector2D	Health;
	UPROPERTY()		FVector2D	Armour;

	FKillerInfo()
		: Weapon()
		, Head()
		, Torso()
		, Legs()
		, Health()
		, Armour()
	{}
};