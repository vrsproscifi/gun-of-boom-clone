//====================================
#include "ShooterGame.h"
#include "FiveSecondsKills.h"

uint32 FFiveSecondsKills::AddKills()
{
	//--------------------------------
	if (FDateTime::Now() - StartDate > FTimespan::FromSeconds(5))
	{
		RestartSeries();
	}

	//--------------------------------
	Kills++;

	//--------------------------------
	return Kills;
}

void FFiveSecondsKills::RestartSeries()
{
	StartDate = FDateTime::Now();
	Kills = 0;
}
