// Copyright 1998-2018 Epic Games, Inc. All Rights Reserved.

#pragma once

#include "Game/OnlineGameState.h"
#include "ShooterGameState.generated.h"

class ATeamInfo;
class AShooterPlayerState;

UCLASS()
class AShooterGameState : public AOnlineGameState
{
	friend class AShooterGameMode;

	GENERATED_UCLASS_BODY()

public:
	void SendLeaveMemberRequest(const FGuid& member) const;


	UFUNCTION(BlueprintCallable, BlueprintPure, Category = "Gameplay|Team")
	float GetTeamScore(const uint8& InTeamNum) const;

	UFUNCTION(BlueprintCallable, BlueprintPure, Category = "Gameplay|Team")
	float GetTeamWinRate(const uint8& InTeamNum) const;

	UFUNCTION(BlueprintCallable, BlueprintPure, Category = "Gameplay|Team")
	ATeamInfo* GetTeam(const uint8& InTeamNum) const;

	UFUNCTION(BlueprintCallable, BlueprintPure, Category = "Gameplay|Team")
	const TArray<ATeamInfo*>& GetTeams() const;

	UFUNCTION(BlueprintCallable, Category = "Gameplay|Team")
	float GiveTeamScore(const uint8& InTeamNum, const float& InScorePoints);

	UFUNCTION(BlueprintCallable, BlueprintPure, Category = "Gameplay|Team")
	bool HasSameTeam(const AActor* InA, const AActor* InB) const;

	UFUNCTION(BlueprintCallable, BlueprintPure, Category = "Gameplay|State")
	float GetGoalScore() const;

	UFUNCTION(BlueprintCallable, BlueprintPure, Category = "Gameplay|State")
	int32 GetRemainingTime() const;

	UFUNCTION(BlueprintCallable, BlueprintPure, Category = "Gameplay|State")
	int32 GetTimeLimit() const;

	UFUNCTION(BlueprintCallable, Category = "Gameplay|State")
	void SetTimeLimit(const int32& InTimeLimit);

	UFUNCTION(BlueprintCallable, BlueprintPure, Category = "Gameplay|State")
	const float& GetSpawnProtectionTime() const;

	virtual float GetPlayerRespawnDelay(AController* Controller) const override;

	virtual void BeginPlay() override;

protected:

	virtual void InitGameState(const FNodeSessionMatch& information) override;
	virtual void InitTeams(const FNodeSessionMatch& information);

	UFUNCTION()
	void OnRep_RemainingTime();

	UPROPERTY(Replicated, VisibleInstanceOnly)
	TArray<ATeamInfo*> Teams;

	UPROPERTY(Replicated, VisibleInstanceOnly)
	float GoalScore;

	UPROPERTY(ReplicatedUsing = OnRep_RemainingTime, VisibleInstanceOnly)
	int32 RemainingTime;

	UPROPERTY(Replicated, VisibleInstanceOnly)
	int32 CurrentLimitTime;

	/** amount of time after a player spawns where they are immune to damage from enemies */
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Replicated, Category = GameState)
	float SpawnProtectionTime;
};
