// Copyright 1998-2018 Epic Games, Inc. All Rights Reserved.

//----------------------------------------
#pragma once


//----------------------------------------
#include "Player/Statistic/PlayerDamageData.h"
#include "Player/Statistic/PlayerShootData.h"
#include "UserInterface/Widgets/Input/WeaponPickupBetter.h"

//----------------------------------------
#include "PlayerKillsStatistic/DominationSeriesKill.h"
#include "PlayerKillsStatistic/FiveSecondsKills.h"
#include "PlayerKillsStatistic/KillerInfo.h"


//----------------------------------------
#include "Player/OnlinePlayerState.h"
#include "Interfaces/TeamInterface.h"
#include "ShooterPlayerState.generated.h"

class SKilledContainer;
class STouchInputButton;
class SVerticalMenuBuilder;
class SPlayersTable;
class SSniperScope;
class SFightAction;
class SKillList;
class SGameTimeAndScores;
class SWeaponBar;
class ATeamInfo;
class UProfileTestData;
class STouchLayout;
class SLiveBar;
class SBadConnection;

//
class AShooterWeapon;
class AShooterPickup_Weapon;
class UShooterGrenade;
class UShooterAid;

class AShooterGameState;




//=============================================
UCLASS()
class AShooterPlayerState 
	: public AOnlinePlayerState
	, public ITeamInterface
{
	friend class ATeamInfo;

	GENERATED_UCLASS_BODY()

private:
	UPROPERTY(VisibleInstanceOnly)
	FFiveSecondsKills FiveSecondsKills;

	UPROPERTY(VisibleInstanceOnly)
	FDominationSeriesKill DominationSeriesKill;

	UPROPERTY(VisibleInstanceOnly)
	FDominationSeriesKill RevengeSeriesKill;


public:
	static const int32 DefaultProfileIndex;

	UFUNCTION(BlueprintCallable, BlueprintPure)
		bool BotIsArmoured() const;

	UFUNCTION(BlueprintCallable, BlueprintPure)
		bool IsInventorySlotEquiped(const EGameItemType& InInventorySlot) const;

	UFUNCTION(BlueprintCallable, BlueprintPure)
		bool BotMaximumLevelReached() const;

	UFUNCTION(BlueprintCallable, BlueprintPure)
		bool BotIsTargetWeaponLevel(const int32& InTargetWeaponLevel) const;

	UFUNCTION(BlueprintCallable, BlueprintPure)
		int32 GetGrenadeAmount() const;

	UFUNCTION(BlueprintCallable, BlueprintPure)
		float GetGrenadePercent() const;


	UFUNCTION(BlueprintCallable, BlueprintPure)
		int32 GetAidAmount() const;

	UFUNCTION(BlueprintCallable, BlueprintPure)
		float GetAidPercent() const;

	UFUNCTION(BlueprintCallable, BlueprintPure)
		int32 GetGameRemainingTime() const;

	UFUNCTION(BlueprintCallable, BlueprintPure)
	bool TargetingIsActivated() const;


	UFUNCTION()
	void OnEscapeMenuClicked() const;

	UFUNCTION()
	void OnEscapeMenuClickAnyButton(const uint8& InButtonIndex);

	const FSlateBrush* GetWeaponPickupIcon() const;

	EWeaponPickupBetter::Type GetWeaponPickupBetter() const;

	UFUNCTION(BlueprintCallable)
	void OnSelectWeapon(AShooterWeapon* InTargetWeapon);

	UFUNCTION(BlueprintCallable, BlueprintPure)
	TArray<AShooterWeapon*> GetAvalibleWeapons() const;

	UFUNCTION(BlueprintCallable, BlueprintPure)
	AShooterWeapon* GetCurrentWeapon() const;

	
	FReply OnPlayersTableClicked(const FGeometry& InGeon, const FPointerEvent& InPoint) const;

	//===================================
	//	Visibility
	EVisibility GetAidVisibility() const;
	EVisibility GetGrenadeVisibility() const;
	EVisibility GetKnifeVisibility() const;

	EVisibility GetFireVisibility() const;
	EVisibility GetWeaponPickupVisibility() const;

	EVisibility IsAliveVisible() const;
	EVisibility IsAliveHitTestInvisible() const;

	EVisibility IsOnProgressMatch_Visible() const;
	EVisibility IsOnProgressMatch_HitTestInvisible() const;



	uint32 AddFiveSecondsKills()
	{
		return FiveSecondsKills.AddKills();
	}

	uint32 AddDominationKills(const uint32& InPlayerId)
	{
		return DominationSeriesKill.AddKills(InPlayerId);
	}

		void ResetDominatioKills()
	{
		DominationSeriesKill.ResetKills();
	}

	uint32 AddRevengeKills(const uint32& InPlayerId)
	{
		return RevengeSeriesKill.AddKills(InPlayerId);
	}

	uint32 GetRevengeKills(const uint32& InPlayerId)
	{
		return RevengeSeriesKill.GetKills(InPlayerId);
	}

	void ResetRevengeKills(const uint32& InPlayerId)
	{
		RevengeSeriesKill.ResetKills(InPlayerId);
	}






	//----------------------------------------
	UPROPERTY(VisibleInstanceOnly)
	USoundConcurrency* NotifyAchievementSoundConcurrency;

	//----------------------------------------
	AShooterPickup_Weapon* GetCurrentWeaponPickup() const;

	//----------------------------------------
	UFUNCTION(BlueprintCallable, Category = "Game|Character")
	UShooterAid* GetAid() const;

	UFUNCTION(BlueprintCallable, Category = "Game|Character")
	bool IsAllowUseAid() const;

	//----------------------------------------
	UFUNCTION(BlueprintCallable, Category = "Game|Character")
		UShooterGrenade* GetGrenade() const;

	UFUNCTION(BlueprintCallable, Category = "Game|Character")
		bool IsAllowUseGrenade() const;

	//----------------------------------------
	UPROPERTY()
	FTimerHandle TimerHandle_PlayerTravelToLobby;

	void OnPlayerTravelToLobby();

	UFUNCTION(Reliable, Client)
	void OnClientPlayerTravelToLobby();

	UFUNCTION(reliable, server, WithValidation)
	void OnServerPlayerTravelToLobby();

	UFUNCTION(BlueprintCallable, BlueprintPure)
	AShooterGameState* GetGameState() const;
	AShooterGameState* GetGameState();

	//----------------------------------------

	// Begin APlayerState interface
	/** clear scores */
	virtual void Reset() override;

	/**
	 * Set the team 
	 *
	 * @param	InController	The controller to initialize state with
	 */
	virtual void ClientInitialize(class AController* InController) override;

	virtual void OnCreateUserInterface() override;

	virtual void UnregisterPlayerWithSession() override;

	// End APlayerState interface

	virtual void NotifyPlayerAchievement_Implementation(const int32& InAchievementId, const int32& InTotalAmount, const int32& InAmount, const int32& InScore) override;

	/** player assist kill someone */
	void ScoreAssist(const int32& Points);

	/** player killed someone */
	void ScoreKill(AShooterPlayerState* InVictim, const int32& Points);

	/** player died */
	void ScoreDeath(AShooterPlayerState* InKilledBy, const int32& Points);

	/** get current team */
	virtual uint8 GetTeamNum() const override;
	virtual ATeamInfo* GetTeam() const override;

	/** get number of kills */
	virtual int32 GetKills() const override;

	/** get number of deaths */
	virtual int32 GetDeaths() const override;

	/** get number of points */
	virtual int32 GetScore() const override;

	/** get number of bullets fired this match */
	int32 GetNumBulletsFired() const;

	/** get number of rockets fired this match */
	int32 GetNumRocketsFired() const;

	UPROPERTY(VisibleAnywhere)
	int32 PlayerWinRate;

	UPROPERTY(VisibleAnywhere)
	int32 PlayerTargetWeaponLevel;

	UPROPERTY(VisibleAnywhere)
	int32 PlayerTargetWeaponUpgrade;

	UPROPERTY(VisibleAnywhere)
		int32 PlayerTargetArmourLevel;

	UFUNCTION(BlueprintCallable, BlueprintPure)
	const int32& GetPlayerWinRate() const
	{
		return PlayerWinRate;
	}

	UFUNCTION(BlueprintCallable, BlueprintPure)
	const int32& GetTargetWeaponLevel() const
	{
		return PlayerTargetWeaponLevel;
	}

	UFUNCTION(BlueprintCallable, BlueprintPure)
		const int32& GetTargetWeaponUpgrade() const
	{
		return PlayerTargetWeaponUpgrade;
	}

	UFUNCTION(BlueprintCallable, BlueprintPure)
	const int32& GetTargetArmourLevel() const
	{
		return PlayerTargetArmourLevel;
	}

	/** get whether the player quit the match */
	UFUNCTION(BlueprintCallable, BlueprintPure)
	bool IsQuitter() const;

	/** gets truncated player name to fit in death log and scoreboards */
	FString GetShortPlayerName() const;

	UFUNCTION(Reliable, Client)
	void OnDeathMessage(AShooterPlayerState* InKilled, AShooterPlayerState* InKiller, const FKillerInfo& InKillerInfo);

	/** broadcast death to local clients */
	UFUNCTION(Reliable, NetMulticast)
	void BroadcastDeath(class AShooterPlayerState* KillerPlayerState, const UDamageType* KillerDamageType, class AShooterPlayerState* KilledPlayerState);

	/** replicate team colors. Updated the players mesh colors appropriately */
	UFUNCTION()
	void OnRep_TeamColor();

	//We don't need stats about amount of ammo fired to be server authenticated, so just increment these with local functions
	void AddBulletsFired(int32 NumBullets);
	void AddRocketsFired(int32 NumRockets);

	/** Set whether the player is a quitter */
	void SetQuitter(bool bInQuitter);

	virtual void CopyProperties(class APlayerState* PlayerState) override;

	FORCEINLINE void SetCoefficient(const float& c)
	{
		PlayerCoefficient = c;
	}

	UFUNCTION(BlueprintCallable, Category = Statistics)
	FORCEINLINE float GetCoefficient() const
	{
		return PlayerCoefficient;
	}


protected:

	/** Set the mesh colors based on the current teamnum variable */
	void UpdateTeamColors();

	/** number of bullets fired this match */
	UPROPERTY()
	int32 NumBulletsFired;

	/** number of rockets fired this match */
	UPROPERTY()
	int32 NumRocketsFired;

	/** whether the user quit the match */
	UPROPERTY()
	uint8 bQuitter : 1;

	UPROPERTY(Transient, ReplicatedUsing = OnRep_TeamColor)
	ATeamInfo* Team;

	UPROPERTY(Transient, VisibleInstanceOnly)
	FGuid SafeTeamId;

public:
	void SetTeam(ATeamInfo* InTeam);

	UFUNCTION(BlueprintCallable, BlueprintPure)
	FGuid GetTeamId() const;

	UPROPERTY(VisibleInstanceOnly, Transient, Replicated)	
	float PlayerCoefficient;

	/** helper for scoring points */
	void ScorePoints(const int32& Points);

	TSharedPtr<STouchLayout>			Slate_TouchLayout;
	TSharedPtr<SLiveBar>				Slate_LiveBar;
	TSharedPtr<SWeaponBar>				Slate_WeaponBar;
	TSharedPtr<SGameTimeAndScores>		Slate_GameTimeAndScores;
	TSharedPtr<SKillList>				Slate_KillList;
	TSharedPtr<SFightAction>			Slate_FightAction;
	TSharedPtr<SSniperScope>			Slate_SniperScope;
	TSharedPtr<SPlayersTable>			Slate_PlayersTable;
	TSharedPtr<SVerticalMenuBuilder>	Slate_EscapeMenu;
	TSharedPtr<STouchInputButton>		Touch_EscapeMenu;
	TSharedPtr<SBadConnection>			Slate_BadConnection;
	TSharedPtr<SKilledContainer>		Slate_KilledContainer;

private:
	// Damage data before die (used to detect all killers)
	UPROPERTY(Transient)
	TMap<int32, FPlayerDamageData> DamageData;

	// Last damage data (used to detect what kill me and by weapon type)
	UPROPERTY(Transient)
	FPlayerDamageData DamageDataLast;

	// Total shoots on session
	UPROPERTY(Transient)
	TArray<FPlayerShootData> ShootData;

	// Total series kills on session (only server, TMultiMap not supported as UPROPERTY)
	TMultiMap<int32, int32> KillsData;

	// Series kills before die (only server, FWeaponHelper not supported as TMap key on UPROPERTY)
	UPROPERTY(Transient)
	TMap<int32, int32> KillsDataLast;

public:
	/** Server only */
	UFUNCTION(BlueprintPure, BlueprintCallable, Category = Statistics)
	int32 GetKillsOneLife() const
	{
		int32 _value = 0;
		for (auto& i : KillsDataLast)
		{
			_value += i.Value;
		}

		return _value;
	}

	UFUNCTION(BlueprintCallable, Category = Statistics)
	bool AddDamageData(const int32& Index, const FPlayerDamageData& Data);

	const TMap<int32, FPlayerDamageData>& GetDamageData() const
	{
		return DamageData;
	}

	TMap<int32, FPlayerDamageData>& GetDamageData()
	{
		return DamageData;
	}

	UFUNCTION(BlueprintPure, BlueprintCallable, Category = Statistics)
	FPlayerDamageData& GetDamageDataLast()
	{
		return DamageDataLast;
	}


public:
	virtual int32 GivePlayerAchievement(const int32& InAchievementId, const int32& InAmount = 1, const int32& InScore = 0) override;


	void InitTestData(UProfileTestData* InTestAsset);
	virtual void OnInitializeCharacter() override;

	void Tick(float DeltaSeconds) override;

	void OnRepGameStateEvent();

	UFUNCTION()
	void RequestPlayerRespawn();
};
