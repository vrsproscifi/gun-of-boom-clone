// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"

#include "Shared/AttachAccountService.h"


#include "Player/OnlinePlayerState.h"
#include "ProgressComponent/GameMatchInformation.h"
#include "ProgressComponent/RatingTopContainer.h"
#include "LobbyPlayerState.generated.h"

class UShooterGameViewportClient;
class UPlayerCaseEntity;
class SChestsContainer;
enum class EGameCurrency : unsigned char;
class SRaitingsContainer;
class SNewLevelContainer;
class SProfileContainer;
class SImprovementContainer;
class SShopContainer;
class SPersonalProgressContainer;
class SArsenalContainer;
class SFightResults;
class SPersonalFightResult;
class SSearchFightScreen;
class STouchLayout;
class SLobbyContainer;
class UBasicPlayerItem;
class UBaseProductItemEntity;
class UBasicItemEntity;

class UAdsComponent;


UCLASS()
class SHOOTERGAME_API ALobbyPlayerState : public AOnlinePlayerState
{
	GENERATED_BODY()
	
	UPROPERTY(Transient)	UAdsComponent*	AdsComponent;

public:
	UFUNCTION(BlueprintCallable, BlueprintPure, Category = Component)
	UAdsComponent* GetAdsComponent() const;

public:

	ALobbyPlayerState();

	UFUNCTION() bool IsAccountServiceAttached(const TEnumAsByte<EAccountService::Type>& InService) const;
	UFUNCTION() void OnAccountServiceAttach(const TEnumAsByte<EAccountService::Type>& InService, const bool& InAttach);

	UFUNCTION() void CreateCurrencyBlock(UShooterGameViewportClient* TargetViewport);
	UFUNCTION() void CreateTouchLayout(UShooterGameViewportClient* TargetViewport);


	UFUNCTION() void CreateLobbyContainer(UShooterGameViewportClient* TargetViewport);
	UFUNCTION() void CreateArsenalContainer(UShooterGameViewportClient* TargetViewport);
	UFUNCTION() void CreateImprovementContainer(UShooterGameViewportClient* TargetViewport);
	UFUNCTION() void CreateProfileContainer(UShooterGameViewportClient* TargetViewport);
	UFUNCTION() void CreateShopContainer(UShooterGameViewportClient* TargetViewport);

	UFUNCTION() void CreateSearchFightScreen(UShooterGameViewportClient* TargetViewport);
	UFUNCTION() void CreatePersonalFightResult(UShooterGameViewportClient* TargetViewport);
	UFUNCTION() void CreateFightResults(UShooterGameViewportClient* TargetViewport);
	UFUNCTION() void CreateNewLevelContainer(UShooterGameViewportClient* TargetViewport);

	UFUNCTION() void CreatePersonalProgressContainer(UShooterGameViewportClient* TargetViewport);
	UFUNCTION() void CreateRaitingsContainer(UShooterGameViewportClient* TargetViewport);

	UFUNCTION() void BindIdentityComponent();


	virtual void OnCreateUserInterface() override;
	virtual void OnFullyLoadedData() override;
	virtual void OnInventoryUpdated(const TArray<UBasicPlayerItem*> InData, const bool IsInternal) override;
	virtual int32 GetPlayerLevel() const override;

public:
	virtual bool PlayRewardVideo(const FPlayRewardedVideoRequest& InVideoRequest) const override;

private:
	UPROPERTY(Transient, VisibleInstanceOnly) FTimerHandle ScheduleShowLevelUpTimerHandler;

protected:

	TSharedPtr<SLobbyContainer>				Slate_LobbyContainer;
	TSharedPtr<SSearchFightScreen>			Slate_SearchFightScreen;
	TSharedPtr<STouchLayout>				Slate_TouchLayout;
	TSharedPtr<SPersonalFightResult>		Slate_PersonalFightResult;
	TSharedPtr<SFightResults>				Slate_FightResults;
	TSharedPtr<SArsenalContainer>			Slate_ArsenalContainer;
	TSharedPtr<SShopContainer>				Slate_ShopContainer;
	TSharedPtr<SImprovementContainer>		Slate_ImprovementContainer;
	TSharedPtr<SProfileContainer>			Slate_ProfileContainer;
	TSharedPtr<SNewLevelContainer>			Slate_NewLevelContainer;
	TSharedPtr<SRaitingsContainer>			Slate_RaitingsContainer;
	TSharedPtr<SPersonalProgressContainer>	Slate_PersonalProgressContainer;
	TSharedPtr<SChestsContainer>			Slate_ChestsContainer;

	UFUNCTION() void OnImproveItem();
	UFUNCTION() void OnRequestPlayerStat(const FGuid& InPlayerId);
	UFUNCTION() void OnPlayerStat();
	UFUNCTION() void OnStartSearchFight();
	UFUNCTION() void OnStopSearchFight();
	UFUNCTION() void OnFoundSearchFight();
	UFUNCTION() void OnMatchResults(const TArray<FGameMatchInformation>& InData);
	UFUNCTION() void OnSchedulePlayerLevelUp();
	UFUNCTION() void OnSchedulePlayerLevelUpExecute();

	UFUNCTION() void OnTopPlayers(const FRatingTopContainer& InRatingContainer);
	UFUNCTION() void OnPlayerEntryReward(const int32& InRewardDay);
	UFUNCTION() void OnPlayerChestsUpdate(const TArray<UPlayerCaseEntity*>& InChests);

	UFUNCTION() void OnClickedArsenalEmpty();
	UFUNCTION() void OnClickedArsenal(UBasicItemEntity* InItem);
	UFUNCTION() void OnClickedSettings();
	UFUNCTION() void OnClickedRaitings();
	UFUNCTION() void OnClickedProfile();
	UFUNCTION() void OnClickedChests();
	void OnClickedChest(const TWeakObjectPtr<UPlayerCaseEntity>& InChest);
	UFUNCTION() void OnShowShop(EGameCurrency InCurrency);

	void OnRequestUpgrade(const UBasicPlayerItem* InItem, const int32& InLevel);
	void OnSelectItemToUpgrade(const UBasicPlayerItem* InItem);
	void OnRequestSelect(const UBasicPlayerItem* InItem);
	void OnRequestProductBuy(const UBaseProductItemEntity* InEntity);
	void OnRequestBuy(const UBasicPlayerItem* InItem);
	void OnActiveItem(const UBasicPlayerItem* InItem);
	void OnClickedDiscount(UBasicItemEntity* InTargetBasicItem);

public:

	UPROPERTY(BlueprintReadWrite, Category = Expiremental)
	class AItemCaptureActor* ItemCaptureActor;
};
