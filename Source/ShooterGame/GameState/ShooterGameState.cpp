// Copyright 1998-2018 Epic Games, Inc. All Rights Reserved.

#include "ShooterGame.h"
#include "ShooterGameState.h"
#include "ShooterPlayerState.h"
#include "GameMode/ShooterGameMode.h"

#include "Player/ShooterPlayerController.h"
#include "TeamInfo.h"
#include "Game/Components/NodeComponent/NodeSessionMatch.h"
#include "Game/Components/NodeComponent.h"
#include "AIController.h"
#include "GameSingletonExtensions.h"
#include "AnnoncerData.h"

AShooterGameState::AShooterGameState(const FObjectInitializer& ObjectInitializer) 
	: Super(ObjectInitializer)
	, GoalScore(0)
	, RemainingTime(0)
	, CurrentLimitTime(0)
	, SpawnProtectionTime(5)
{
}

const float& AShooterGameState::GetSpawnProtectionTime() const
{
	return SpawnProtectionTime;
}

float AShooterGameState::GetPlayerRespawnDelay(AController* Controller) const
{
	if (auto MyGameMode = GetDefaultGameMode<AShooterGameMode>())
	{
		const auto RespawnDelay = MyGameMode->MinRespawnDelay;
		return RespawnDelay;
	}

	return Super::GetPlayerRespawnDelay(Controller);
}

void AShooterGameState::BeginPlay()
{
	Super::BeginPlay();

	if (!IsRunningDedicatedServer())
	{
		if (auto FirstCtrl = GetWorld()->GetFirstPlayerController())
		{	
			if (auto LocalPS = GetValidObjectAs<AShooterPlayerState>(FirstCtrl->PlayerState))
			{
				LocalPS->OnRepGameStateEvent();
			}
		}
	}
}

void AShooterGameState::SendLeaveMemberRequest(const FGuid& member) const
{
	if(const auto node = GetNodeComponent())
	{
		node->SendLeaveMemberRequest(member);
	}
}

void AShooterGameState::InitGameState(const FNodeSessionMatch& information)
{
	Super::InitGameState(information);

	InitTeams(information);
	GoalScore = information.Options.GetScoreLimit();

	if (auto MyGameMode = GetBasicGameMode<AShooterGameMode>())
	{
		MyGameMode->OnInitializeMatchInformation();
	}	
}

void AShooterGameState::InitTeams(const FNodeSessionMatch& information)
{
	UE_LOG(LogInit, Display, TEXT("AShooterGameState::InitTeams >> Count: %d"), information.TeamList.Num());

	for (const auto TeamId : information.TeamList)
	{
		ATeamInfo* SpawnedTeam = GetWorld()->SpawnActorDeferred<ATeamInfo>(ATeamInfo::StaticClass(), FTransform::Identity, this);
		if (SpawnedTeam && SpawnedTeam->IsValidLowLevel() && SpawnedTeam->SetTeamId(TeamId) && SpawnedTeam->SetTeamNum(Teams.Num()))
		{
			Teams.Add(SpawnedTeam);
			SpawnedTeam->FinishSpawning(FTransform::Identity);

			UE_LOG(LogInit, Display, TEXT("AShooterGameState::InitTeams >> Team: %d[%s]"), SpawnedTeam->GetTeamNum(), *SpawnedTeam->GetTeamId().ToString());
		}
	}
}

float AShooterGameState::GetTeamWinRate(const uint8& InTeamNum) const
{
	if (Teams.IsValidIndex(InTeamNum) && Teams[InTeamNum] && Teams[InTeamNum]->IsValidLowLevel())
	{
		return Teams[InTeamNum]->GetWinRate();
	}

	return .0f;
}

float AShooterGameState::GetTeamScore(const uint8& InTeamNum) const
{
	if (Teams.IsValidIndex(InTeamNum) && Teams[InTeamNum] && Teams[InTeamNum]->IsValidLowLevel())
	{
		return Teams[InTeamNum]->GetScore();
	}

	return .0f;
}

ATeamInfo* AShooterGameState::GetTeam(const uint8& InTeamNum) const
{
	if (Teams.IsValidIndex(InTeamNum) && Teams[InTeamNum] && Teams[InTeamNum]->IsValidLowLevel())
	{
		return Teams[InTeamNum];
	}

	return nullptr;
}

const TArray<ATeamInfo*>& AShooterGameState::GetTeams() const
{
	return Teams;
}

float AShooterGameState::GiveTeamScore(const uint8& InTeamNum, const float& InScorePoints)
{
	if (Teams.IsValidIndex(InTeamNum) && Teams[InTeamNum] && Teams[InTeamNum]->IsValidLowLevel())
	{
		return Teams[InTeamNum]->GiveScore(InScorePoints);
	}

	return .0f;
}

bool AShooterGameState::HasSameTeam(const AActor* InA, const AActor* InB) const
{	
	const ITeamInterface* AInterface = Cast<ITeamInterface>(InA);
	const ITeamInterface* BInterface = Cast<ITeamInterface>(InB);

	if (AInterface && BInterface)
	{
		if (AInterface && BInterface)
		{
			if (AInterface->GetTeamNum() == ATeamInfo::NullTeam || BInterface->GetTeamNum() == ATeamInfo::NullTeam)
			{
				return InA == InB;
			}

			return AInterface->GetTeamNum() == BInterface->GetTeamNum();
		}
	}

	return false;
}

float AShooterGameState::GetGoalScore() const
{
	return GoalScore;
}

int32 AShooterGameState::GetRemainingTime() const
{
	return RemainingTime;
}

int32 AShooterGameState::GetTimeLimit() const
{
	return CurrentLimitTime;
}

void AShooterGameState::SetTimeLimit(const int32& InTimeLimit)
{
	CurrentLimitTime = InTimeLimit;
	RemainingTime = CurrentLimitTime;
}

void AShooterGameState::OnRep_RemainingTime()
{
	static int32 OldRemainingTime;
	if (RemainingTime != OldRemainingTime)
	{
		OldRemainingTime = RemainingTime;

		if (GetMatchState() == MatchState::InProgress)
		{
			const auto RemainingTimeAnnoncers = FGameSingletonExtension::GetDataAssets<UAnnoncerData>();
			if (RemainingTimeAnnoncers.Num() && RemainingTimeAnnoncers[0] && RemainingTimeAnnoncers[0]->IsValidLowLevel())
			{
				if (RemainingTimeAnnoncers[0]->IsExistSoundForTime(RemainingTime))
				{
					UGameplayStatics::PlaySound2D(this, RemainingTimeAnnoncers[0]->GetSoundForTime(RemainingTime));
				}
			}
		}
	}
}

void AShooterGameState::GetLifetimeReplicatedProps(TArray< FLifetimeProperty > & OutLifetimeProps) const
{
	Super::GetLifetimeReplicatedProps(OutLifetimeProps);

	DOREPLIFETIME(AShooterGameState, Teams);
	DOREPLIFETIME(AShooterGameState, RemainingTime);
	DOREPLIFETIME(AShooterGameState, CurrentLimitTime);
	DOREPLIFETIME(AShooterGameState, GoalScore);
}
