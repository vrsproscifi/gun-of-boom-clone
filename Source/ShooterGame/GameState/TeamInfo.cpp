// Fill out your copyright notice in the Description page of Project Settings.

#include "ShooterGame.h"
#include "TeamInfo.h"
#include "ShooterPlayerState.h"

uint8 ATeamInfo::NullTeam = 255;

ATeamInfo::ATeamInfo()
	: TeamNum(NullTeam)
	, TeamScore(0.0f)
	, TargetWeaponUpgradeCache(0)
	, TargetWeaponLevelCache(0)
	, TargetArmourLevelCache(0)
{
	SetReplicates(true);
	bAlwaysRelevant = true;
	NetUpdateFrequency = 5.0f;

	TeamId.Invalidate();
	Players.Empty();
}

bool ATeamInfo::IsEqualId(const FGuid& InTeamId) const
{
	return InTeamId == TeamId;
}

bool ATeamInfo::IsEqualNum(const uint8& InTeamNum) const
{
	return InTeamNum == TeamNum;
}

void ATeamInfo::AddPlayer(AShooterPlayerState* InPlayer)
{
	if (InPlayer && InPlayer->IsValidLowLevel())
	{
		if (InPlayer->GetTeam() && InPlayer->GetTeam() != this)
		{
			InPlayer->GetTeam()->RemovePlayer(InPlayer);
		}

		//==============================
		const auto to = FString::Printf(TEXT("to %d[%s] | Players: %d"), static_cast<int32>(TeamNum), *GetTeamId().ToString(), Players.Num());

		//==============================
		const auto last = InPlayer->GetTeam();
		FString lastId = "nullptr";
		int32 lastNum = -1;

		//==============================
		if(last)
		{
			lastId = last->GetTeamId().ToString();
			lastNum = static_cast<int32>(last->GetTeamNum());
		}

		//==============================
		const auto from = FString::Printf(TEXT("from %d[%s]"), lastNum, *lastId);

		UE_LOG(LogGameMode, Error, TEXT("ATeamInfo::AddPlayer[%s] ] %s -> %s"), *InPlayer->GetPlayerName(), *from, *to);

		//==============================
		Players.AddUnique(InPlayer);
		InPlayer->SetTeam(this);
	}
}

bool ATeamInfo::RemovePlayer(AShooterPlayerState* InPlayer)
{
	if (InPlayer && InPlayer->IsValidLowLevel())
	{
		//==============================
		auto last = InPlayer->GetTeam();
		FString lastId = "nullptr";
		int32 lastNum = -1;

		//==============================
		if (last)
		{
			lastId = last->GetTeamId().ToString();
			lastNum = last->GetTeamNum();
		}

		UE_LOG(LogGameMode, Error, TEXT("ATeamInfo::RemovePlayer[%s] from %d[%s] -> %d[%s] | Players: %s"), *InPlayer->GetPlayerName(), lastNum, *lastId, GetTeamNum(), *GetTeamId().ToString(), Players.Num());

		if (InPlayer->GetTeam() == this && Players.Contains(InPlayer))
		{
			Players.Remove(InPlayer);
			InPlayer->SetTeam(nullptr);
			return true;
		}
	}

	return false;
}

const TArray<AShooterPlayerState*> ATeamInfo::GetTeamBots() const
{
	return GetTeamMembers().FilterByPredicate([](AShooterPlayerState* InPlayerState)
	{
		return InPlayerState && InPlayerState->IsValidLowLevel() && InPlayerState->bIsABot;
	});
}

const TArray<AShooterPlayerState*> ATeamInfo::GetTeamHumans() const
{
	return GetTeamMembers().FilterByPredicate([](AShooterPlayerState* InPlayerState)
	{
		return InPlayerState && InPlayerState->IsValidLowLevel() && InPlayerState->bIsABot == false;
	});
}


uint8 ATeamInfo::GetTeamNum() const
{
	return TeamNum;
}

float ATeamInfo::GetWinRate() const
{
	//=============================
	int32 WinRateSum = 0;

	//=============================
	auto Humans = GetTeamHumans();

	//=============================
	for(auto Player : Humans)
	{
		WinRateSum += Player->GetPlayerWinRate();
	}

	//=============================
	WinRateCache = float(WinRateSum) / float(FMath::Max(1, Humans.Num()));

	//=============================
	return FMath::RoundToInt(WinRateCache);
}

int32 ATeamInfo::GetTargetWeaponUpgrade() const
{
	//=============================
	int32 AmmunationLevelSum = 0;

	//=============================
	auto Humans = GetTeamHumans();

	//=============================
	for (auto Player : Humans)
	{
		AmmunationLevelSum += Player->GetTargetWeaponUpgrade();
	}

	//=============================
	TargetWeaponUpgradeCache = float(AmmunationLevelSum) / float(FMath::Max(1, Humans.Num()));

	//=============================
	return FMath::RoundToInt(TargetWeaponUpgradeCache);
}

int32 ATeamInfo::GetTargetWeaponLevel() const
{
	//=============================
	int32 AmmunationLevelSum = 0;

	//=============================
	auto Humans = GetTeamHumans();

	//=============================
	for (auto Player : Humans)
	{
		AmmunationLevelSum += Player->GetTargetWeaponLevel();
	}

	//=============================
	TargetWeaponLevelCache = float(AmmunationLevelSum) / float(FMath::Max(1, Humans.Num()));

	//=============================
	return FMath::RoundToInt(TargetWeaponLevelCache);
}

int32 ATeamInfo::GetTargetArmourLevel() const
{
	//=============================
	int32 AmmunationLevelSum = 0;

	//=============================
	auto Humans = GetTeamHumans();

	//=============================
	for (auto Player : Humans)
	{
		//------------------------------------
		auto TargetArmourLevel = Player->GetTargetArmourLevel();

		//------------------------------------
		AmmunationLevelSum += TargetArmourLevel;

		//------------------------------------
		UE_LOG(LogInit, Warning, TEXT("[ATeamInfo::GetTargetArmourLevel][%s][%d]"), *Player->GetPlayerName(), TargetArmourLevel);
	}

	//=============================
	TargetArmourLevelCache = float(AmmunationLevelSum) / float(FMath::Max(1, Humans.Num()));

	//=============================
	return FMath::RoundToInt(TargetArmourLevelCache);
}



float ATeamInfo::GiveScore(const float& InScorePoints)
{
	TeamScore += InScorePoints;
	return TeamScore;
}

void ATeamInfo::SetScore(const float& InScorePoints)
{
	TeamScore = InScorePoints;
}

float ATeamInfo::GetScore() const
{
	return TeamScore;
}

int32 ATeamInfo::GetIntScore() const
{
	return FMath::FloorToInt(GetScore());
}

FString ATeamInfo::GetStrScore() const
{
	return FString::FormatAsNumber(GetIntScore());
}


bool ATeamInfo::SetTeamId(const FLokaGuid& InTeamId)
{
	if (IsActorInitialized() == false)
	{
		TeamId = InTeamId;
		return true;
	}

	return false;
}

const FLokaGuid& ATeamInfo::GetTeamId() const
{
	return TeamId;
}

FString ATeamInfo::GetTeamStrId() const
{
	return GetTeamId().ToString();
}


bool ATeamInfo::SetTeamNum(const uint8& InTeamNum)
{
	if (IsActorInitialized() == false)
	{
		TeamNum = InTeamNum;
		return true;
	}

	return false;
}

int32 ATeamInfo::GetSize() const
{
	checkSlow(Role == ROLE_Authority);
	return Players.Num();
}

int32 ATeamInfo::GetNumHumans() const
{
	int32 Count = 0;
	checkSlow(Role == ROLE_Authority);
	for (const auto Member : Players)
	{
		if (GetValidObject(Member))
		{
			Count++;
		}
	}
	return Count;
}

void ATeamInfo::GetLifetimeReplicatedProps(TArray<FLifetimeProperty>& OutLifetimeProps) const
{
	Super::GetLifetimeReplicatedProps(OutLifetimeProps);

	DOREPLIFETIME(ATeamInfo, TeamNum);
	DOREPLIFETIME(ATeamInfo, TeamScore);
	DOREPLIFETIME(ATeamInfo, Players);
}
