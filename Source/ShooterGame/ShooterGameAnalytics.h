#pragma once

//class IAnalyticsProvider;
class UOperationRequest;

enum class EErrorSeverity : uint8
{
	undefined = 0,
	debug = 1,
	info = 2,
	warning = 3,
	error = 4,
	critical = 5
};

enum class EResourceFlowType : uint8
{
	Source = 1,
	Sink = 2
};

enum EProgressionStatus
{
	Start = 1,
	Complete = 2,
	Fail = 3
};


UENUM(BlueprintType)
enum class EBusinessPurchaseState : uint8
{
	Unknown = 0 UMETA(DisplayName = "Unknown"),
	Success UMETA(DisplayName = "Success"),
	Failed UMETA(DisplayName = "Failed"),
	Cancelled UMETA(DisplayName = "Cancelled"),
	Invalid UMETA(DisplayName = "Invalid"),
	NotAllowed UMETA(DisplayName = "NotAllowed"),
	Restored UMETA(DisplayName = "Restored"),
	AlreadyOwned UMETA(DisplayName = "AlreadyOwned"),
};

struct FShooterGameAnalytics
{
	static FGuid UserId;
	static int32 MatchesPerSession;
	static int32 BadNetworkConnections;
	static bool bHasSessionStarted;
	

	static void RecordMatchPerSession();

	static void AddProgressionEvent(EProgressionStatus progressionStatus, const FString& progression01, const FString& progression02, const int32& InScore = 0);

	static void RecordContentSearch(const FString& InQuery);

	static void RecordContentView(const FString& InContentId, const FString& InContentName, const FString& InContentType);
	static void RecordDesignEvent(const FString& InEventName);
	static void RecordDesignEvent(const FString& InEventName, const float& InAmount);
	static void RecordDesignEventRange(const FString& InEventName, const int32& InAmount, const TArray<int32>& InRangeArray = { 50, 40, 30, 20, 10, 7, 5, 3, 1, 0 });
	static void RecordErrorEvent(const EErrorSeverity& InErrorType, const FString& InErrorMessage);

	static void AddResourceEvent(const EResourceFlowType& InFlowType, const FString& currency, const float& amount, const FString& itemType, const FString& itemId);
	static void AddBusinessEvent(const FString&currency, const int& InCostInCents, const FString& itemType, const FString&itemId, const FString& cartType, const FString& receipt, const FString& signature, const EBusinessPurchaseState& InPurchaseState);
	static void AddBusinessEvent(const FString&InItemId, const FString&InItemName, const FString&InItemType, const FString&currency, const int& InCostInCents);

	static void StartSession(const FString& InGameKey, const FString& InGameSecret);
	static void StopSession();

	static void SetUserId(const FGuid& InUserId);
};

