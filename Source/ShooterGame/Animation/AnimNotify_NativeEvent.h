// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "Animation/AnimNotifies/AnimNotify.h"
#include "AnimNotify_NativeEvent.generated.h"


DECLARE_MULTICAST_DELEGATE_ThreeParams(FOnAnimNativeEvent, FName, USkeletalMeshComponent*, UAnimSequenceBase*);

UCLASS()
class SHOOTERGAME_API UAnimNotify_NativeEvent : public UAnimNotify
{
	GENERATED_BODY()

public:

	UAnimNotify_NativeEvent();

	virtual void Notify(USkeletalMeshComponent* MeshComp, UAnimSequenceBase* Animation) override;

	static FOnAnimNativeEvent OnAnimNativeEvent;

	UPROPERTY(EditAnywhere, BlueprintReadOnly, Category = "Notify")
	FName NotifyName;
};
