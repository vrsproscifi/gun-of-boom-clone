// Fill out your copyright notice in the Description page of Project Settings.

#include "ShooterGame.h"
#include "AnimNotify_NativeEvent.h"

FOnAnimNativeEvent UAnimNotify_NativeEvent::OnAnimNativeEvent;

UAnimNotify_NativeEvent::UAnimNotify_NativeEvent()
	: Super()
{
	
}

void UAnimNotify_NativeEvent::Notify(USkeletalMeshComponent* MeshComp, UAnimSequenceBase* Animation)
{
	OnAnimNativeEvent.Broadcast(NotifyName, MeshComp, Animation);
}
