// Copyright 1998-2018 Epic Games, Inc. All Rights Reserved.

#pragma once

#include "Player/ShooterCharacter.h"
#include "ShooterBot.generated.h"


UCLASS()
class AShooterBot : public AShooterCharacter
{
GENERATED_UCLASS_BODY()

	UPROPERTY(EditAnywhere, Category=Behavior)
	class UBehaviorTree* BotBehavior;

	UPROPERTY(EditAnywhere)
	bool bIsAllowUseAid;

	UPROPERTY(EditAnywhere)
	uint32 AidHealthCriticalAmount;

	virtual bool IsFirstPerson() const override;

	virtual void FaceRotation(FRotator NewRotation, float DeltaTime = 0.f) override;

	virtual float TakeDamage(float Damage, struct FDamageEvent const& DamageEvent, class AController* InEventInstigator, class AActor* InDamageCauser) override;
};