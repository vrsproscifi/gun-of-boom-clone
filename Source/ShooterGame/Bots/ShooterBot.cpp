// Copyright 1998-2018 Epic Games, Inc. All Rights Reserved.

#include "ShooterGame.h"
#include "Bots/ShooterBot.h"
#include "Bots/ShooterAIController.h"
#include "LookOnComponent.h"
#include "Inventory/ShooterAid.h"

AShooterBot::AShooterBot(const FObjectInitializer& ObjectInitializer) 
	: Super(ObjectInitializer)
	, bIsAllowUseAid(true)
	, AidHealthCriticalAmount(400)
{
	AIControllerClass = AShooterAIController::StaticClass();

	UpdatePawnMeshes();

	bUseControllerRotationYaw = true;

	AutoTargeting->SetTargetingAutoFinding(.0f);
}

float AShooterBot::TakeDamage(float Damage, struct FDamageEvent const& DamageEvent, class AController* InEventInstigator, class AActor* InDamageCauser)
{
	auto actual = Super::TakeDamage(Damage, DamageEvent, InEventInstigator, InDamageCauser);

	if (bIsAllowUseAid && Health.X > 0 && Health.X < AidHealthCriticalAmount)
	{
		if (auto aid = GetAid())
		{
			aid->StartProcess();
		}
	}

	return actual;
}

bool AShooterBot::IsFirstPerson() const
{
	return false;
}

void AShooterBot::FaceRotation(FRotator NewRotation, float DeltaTime)
{
	FRotator CurrentRotation = FMath::RInterpTo(GetActorRotation(), NewRotation, DeltaTime, 8.0f);

	Super::FaceRotation(CurrentRotation, DeltaTime);
}
