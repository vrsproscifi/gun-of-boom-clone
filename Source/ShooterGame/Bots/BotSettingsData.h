// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "ProfileTestData.h"
#include "BotSettingsData.generated.h"


/**
 * 
 */
UCLASS()
class SHOOTERGAME_API UBotSettingsData : public UDataAsset
{
	GENERATED_BODY()

public:

	UPROPERTY(EditDefaultsOnly, Category = Bot)
	UProfileTestData* BotData;
	
	UPROPERTY(EditDefaultsOnly, Category = Bot)
	TArray<FString> BotNames;
};
