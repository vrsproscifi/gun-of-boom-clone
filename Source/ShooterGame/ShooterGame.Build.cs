// Copyright 1998-2018 Epic Games, Inc. All Rights Reserved.

using UnrealBuildTool;
using System.IO;

public class ShooterGame : ModuleRules
{
	public ShooterGame(ReadOnlyTargetRules Target) : base(Target)
	{
		OptimizeCode = CodeOptimization.InShippingBuildsOnly;

        bUseRTTI = false;
        bEnableExceptions = false;
        PrivatePCHHeaderFile = "ShooterGame.h";
        //===============================================================================
        if (Target.Platform == UnrealTargetPlatform.Android)
	    {
            PrivateDependencyModuleNames.AddRange(
	            new string[]
	            {
	                "Launch",
	            }
	        );
			
			
			string PluginPath = Utils.MakePathRelativeTo(ModuleDirectory, Target.RelativeEnginePath);
			AdditionalPropertiesForReceipt.Add("AndroidPlugin", Path.Combine(PluginPath, "LOKA_UPL_Android.xml"));
	    }


        //===============================================================================
        //  GameAnalytics
        {
            PrivateDependencyModuleNames.AddRange(new string[] {"GameAnalytics"});
	        PrivateIncludePathModuleNames.AddRange(new string[] {"GameAnalytics"});
            
            //PrivateDependencyModuleNames.AddRange(new string[] { "Crashlytics" });
            //PrivateIncludePathModuleNames.AddRange(new string[] { "Crashlytics" });

            PrivateDependencyModuleNames.AddRange(new string[] { "AdCollection" });
            PrivateIncludePathModuleNames.AddRange(new string[] { "AdCollection" });
        }

		//===============================================================================
		//  AndroidPermission
		{
            PrivateDependencyModuleNames.AddRange(new string[] { "OnlineSubsystemGoogle" });
            PrivateDependencyModuleNames.AddRange(new string[] { "AndroidPermission" });
			PrivateIncludePathModuleNames.AddRange(new string[] { "AndroidPermission" });
		}

		//===============================================================================
		PrivateIncludePaths.AddRange(
			new string[] { 
				"ShooterGame/UserInterface",
				"ShooterGame/UserInterface/Widgets",
			}
		);

	    //===============================================================================
        PublicIncludePaths.AddRange(
			new string[] { 
				"ShooterGame/Player/Components",
				"ShooterGame/Player/Shared",
				"ShooterGame/Player/",				
				"ShooterGame/Shared/",
				"ShooterGame/",
				"ShooterGame/Interfaces",
                "ShooterGame/UserInterface",
                "ShooterGame/UserInterface/Widgets",
                "ShooterGame/UserInterface/Widgets/Styles",


        });

	    //===============================================================================
        PublicDependencyModuleNames.AddRange(
			new string[] {
				"Core",
				"CoreUObject",
				"Engine",
				"OnlineSubsystem",
				"OnlineSubsystemUtils",
				"AssetRegistry",
                "AIModule",
				"GameplayTasks",

                "Json",
                "JsonUtilities",
                "HTTP",

                "ShooterGameFrameWork",
                "Slate",
                "SlateCore",
                "ApplicationCore",
                "ReplicationGraph"
            }
        );

	    //===============================================================================
        PrivateDependencyModuleNames.AddRange(
			new string[] {
				"InputCore",
                "UMG",
                "Slate",
				"SlateCore",
				"ShooterGameLoadingScreen",
				"Json",
				"ApplicationCore",
				"NavigationSystem"
			}
		);

	    //===============================================================================
        //DynamicallyLoadedModuleNames.AddRange(
		//	new string[] {
		//		"OnlineSubsystemNull",
        //        "NetworkReplayStreaming",
		//		"NullNetworkReplayStreaming",
		//		"HttpNetworkReplayStreaming",
		//		"LocalFileNetworkReplayStreaming"
		//	}
		//);
        //
	    ////===============================================================================
        //PrivateIncludePathModuleNames.AddRange(
		//	new string[] {
		//		"NetworkReplayStreaming"
		//	}
		//);

	    //===============================================================================
        if (Target.Platform == UnrealTargetPlatform.Android)
        {
            DynamicallyLoadedModuleNames.Add("OnlineSubsystemGooglePlay");
            DynamicallyLoadedModuleNames.Add("AndroidAdvertising");
        }

        //===============================================================================
        //  Facebook
	    if (Target.Type != TargetRules.TargetType.Server)
        {
            //Facebook API
            PublicDependencyModuleNames.AddRange(new string[] {"Facebook"});

	        //Facebook Subsystem
	        PrivateDependencyModuleNames.Add("OnlineSubsystemFacebook");

	        //Include
	        //PublicIncludePaths.Add("Runtime/Online/OnlineSubsystemFacebook/Public");
	        //PublicIncludePaths.Add("Runtime/Online/OnlineSubsystemFacebook/Private/Windows");
	    }

	    //===============================================================================
        if (Target.Type != TargetRules.TargetType.Server)
		{
			PublicDependencyModuleNames.AddRange(new string[] { "AppFramework", "RHI", "SlateRHIRenderer", "RenderCore", "MoviePlayer", "UMG" });
		}
		

		
	    //===============================================================================
        if (Target.Type == TargetRules.TargetType.Editor)
		{
			PublicDependencyModuleNames.AddRange(new string[] { "UnrealEd", "PropertyEditor" });

			MinFilesUsingPrecompiledHeaderOverride = 1;
			//bFasterWithoutUnity = true;
		}
	}
}
