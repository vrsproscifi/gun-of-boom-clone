// Copyright 1998-2018 Epic Games, Inc. All Rights Reserved.

#pragma once
//=========================================
#include "Player/BasicPlayerController.h"

//=========================================
#include "OnlinePlayerController.generated.h"

//=========================================
UCLASS()
class AOnlinePlayerController
	: public ABasicPlayerController
{
	GENERATED_UCLASS_BODY()

	UPROPERTY(VisibleAnywhere, BlueprintReadOnly)
	TArray<int32> Fps5SecAVG;

	UPROPERTY(VisibleAnywhere, BlueprintReadOnly)
	TArray<int32> Fps10SecAVG;

	//=================================================
	virtual void PostInitializeComponents() override;
	virtual void BeginPlay() override;

	//=================================================
	UPROPERTY()
	FTimerHandle TimerHandle_CollectPerfomanceStatistic;

	FTimerDelegate TimerDelegate_CollectPerfomanceStatistic;

	//=================================================
	UFUNCTION()	virtual void CollectPerfomanceStatistic();
	UFUNCTION() virtual FString GetGameMapName() const;
};

