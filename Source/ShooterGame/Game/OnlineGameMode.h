#pragma once

//=========================================
#include "Game/BasicGameMode.h"
#include "Game/Components/NodeComponent/MatchEndResult.h"

//=========================================
#include "OnlineGameMode.generated.h"

//=========================================
struct FSessionMatchOptions;
struct FNodeSessionMatch;
struct FNodeSessionMember;

//=========================================
class UNodeComponent;

//=========================================
USTRUCT()
struct FWinnerTeamInfo
{
	GENERATED_USTRUCT_BODY()

	FWinnerTeamInfo()
		: Num(-1)
	{
		
	}

	UPROPERTY(VisibleInstanceOnly)
	FLokaGuid Id;

	UPROPERTY(VisibleInstanceOnly)
	int32 Num;
};


//=========================================
UCLASS(Config = Game, Abstract)
class AOnlineGameMode : public ABasicGameMode
{
	GENERATED_UCLASS_BODY()

	//=========================================
public:
	UPROPERTY(VisibleInstanceOnly)
	mutable FWinnerTeamInfo WinnerTeamInfo;

	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = GameMode)
	bool bIsMultiPlayerGameMode;

	virtual void EndMatch() override;
	virtual void InitGame(const FString& MapName, const FString& Options, FString& ErrorMessage) override;
	virtual void PreInitializeComponents() override;
	virtual void InitGameState() override;

	virtual void StartPlay() override;
	virtual void EndPlay(const EEndPlayReason::Type EndPlayReason) override;

	virtual void PreLogin(const FString& Options, const FString& Address, const FUniqueNetIdRepl& UniqueId, FString& ErrorMessage) override;
	virtual void PostLogin(APlayerController* NewPlayer) override;

	virtual void OnMatchStateSet() override;

	/** check if PlayerState is a winner */
	virtual bool IsWinner(class AOnlinePlayerState* PlayerState) const
	{
		return false;
	}

	UNodeComponent* GetNodeComponent();
	UNodeComponent* GetNodeComponent() const;

	const FSessionMatchOptions& GetMatchOptions() const;
	const FNodeSessionMatch& GetMatchInformation() const;
	const TArray<FNodeSessionMember>& GetMatchMebers() const;
	const FNodeSessionMember* FindMatchMember(const FString& InMemberId) const;



	UFUNCTION(BlueprintCallable, BlueprintPure, Category = "Gameplay|State")
		virtual bool IsAllowJoinInProgress() const;

	UFUNCTION(BlueprintCallable, BlueprintPure, Category = "Gameplay|State")
		virtual int32 GetRemainingTime() const;

	UFUNCTION(BlueprintCallable, BlueprintPure, Category = "Gameplay|State")
				int32 GetHalfTimeLimit() const;

	UFUNCTION(BlueprintCallable, BlueprintPure, Category = "Gameplay|State")
		virtual int32 GetTimeLimit() const;

	//	Отправляет результат матча на мастер сервер, если вызов был сделан с сервера
	virtual void SendEndMatchRequest(const FMatchEndResult& result);

	virtual FMatchEndResult GetMatchEndResult() const;

	UFUNCTION(BlueprintCallable, BlueprintPure, Category = "Gameplay|State")
	virtual FGuid GetMatchWinnerTeamId() const;

	UFUNCTION() 
	virtual void OnMatchEnded() {}
};