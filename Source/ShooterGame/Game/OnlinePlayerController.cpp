// Copyright 1998-2018 Epic Games, Inc. All Rights Reserved.

#include "ShooterGame.h"
#include "Game/OnlinePlayerController.h"

#include "BasicPlayerState.h"
#include "ShooterGameAnalytics.h"

AOnlinePlayerController::AOnlinePlayerController(const FObjectInitializer& ObjectInitializer) : Super(ObjectInitializer)
{

}

FString AOnlinePlayerController::GetGameMapName() const
{
	auto world = GetValidWorld();
	if(world)
	{
		FString LevelName = world->GetMapName();
		LevelName.RemoveFromStart(GetWorld()->StreamingLevelsPrefix);	
		return LevelName;
	}
	return "None";
}

void AOnlinePlayerController::PostInitializeComponents()
{
	Super::PostInitializeComponents();
	TimerDelegate_CollectPerfomanceStatistic.BindUObject(this, &AOnlinePlayerController::CollectPerfomanceStatistic);
}

void AOnlinePlayerController::BeginPlay()
{
	Super::BeginPlay();

	if (FPlatformProperties::IsServerOnly() == false)
	{
		if (auto tm = GetTimerManager())
		{
			tm->SetTimer(TimerHandle_CollectPerfomanceStatistic, TimerDelegate_CollectPerfomanceStatistic, 1.0f, true, 5.0f);
		}
	}
}


static int32 IntArrayAvg(const TArray<int32>& InArray)
{
	//=========================
	int32 Total = 0;

	//=========================
	for (auto i : InArray)
	{
		Total += i;
	}

	//=========================
	float Avg = Total / InArray.Num();

	//=========================
	return FMath::FloorToInt(Avg);
}

void AOnlinePlayerController::CollectPerfomanceStatistic()
{
	//===============================================
	auto ps = GetBasicPlayerState();
	if (ps)
	{
		//===============================================
		const float PingValue = ps->ExactPing;

		//===============================================
		FShooterGameAnalytics::RecordDesignEventRange("Perfomance:Ping:PingMore[Key]", PingValue, { 500, 400, 350, 320, 300, 270, 250, 220, 200, 180, 150, 130, 120, 100, 90, 80, 70, 60, 50 });
	}

	//===============================================
	const auto GameMapName = GetGameMapName();

	//===============================================
	const float FPSValue = 1.0f / FSlateApplication::Get().GetAverageDeltaTime();

	//===============================================
	//	Общая статистика производительности
	FShooterGameAnalytics::RecordDesignEvent("Perfomance:FPS:Global:Fps", FPSValue);
	FShooterGameAnalytics::RecordDesignEventRange("Perfomance:FPS:Global:FpsMore[Key]", FPSValue, { 60, 55, 50, 40, 30, 28, 25, 22, 20, 15, 10, 5, 0 });

	//===============================================
	const auto EventName1 = FString::Printf(TEXT("Perfomance:FPS:%s:Fps"), *GameMapName);
	const auto EventName2 = FString::Printf(TEXT("Perfomance:FPS:%s:FpsMore[Key]"), *GameMapName);

	//	Статистика производительности для определенной карты
	FShooterGameAnalytics::RecordDesignEvent(EventName1, FPSValue);
	FShooterGameAnalytics::RecordDesignEventRange(EventName2, FPSValue, { 60, 55, 50, 40, 30, 28, 25, 22, 20, 15, 10, 5, 0 });

	//===============================================
	//	FPS меньше 20
	if (FPSValue < 20)
	{
		FShooterGameAnalytics::RecordDesignEvent(FString::Printf(TEXT("Perfomance:FPSLess20:"), "Global"));
		FShooterGameAnalytics::RecordDesignEvent(FString::Printf(TEXT("Perfomance:FPSLess20:"), *GameMapName));
	}

	//	FPS меньше 10
	if (FPSValue <= 10)
	{
		FShooterGameAnalytics::RecordDesignEvent(FString::Printf(TEXT("Perfomance:FPSLess10:"), "Global"));
		FShooterGameAnalytics::RecordDesignEvent(FString::Printf(TEXT("Perfomance:FPSLess10:"), *GameMapName));
	}

	//===============================================
	//	Средний FPS
	if (Fps5SecAVG.Num() == 5)
	{
		auto FPSAVG = IntArrayAvg(Fps5SecAVG);
		FShooterGameAnalytics::RecordDesignEventRange("Perfomance:FPS5SecAVG:Global:FpsMore[Key]", FPSAVG, { 60, 55, 50, 40, 30, 28, 25, 22, 20, 15, 10, 5, 0 });
		
		const auto EventName3 = FString::Printf(TEXT("Perfomance:FPS5SecAVG:%s:FpsMore[Key]"), *GameMapName);
		FShooterGameAnalytics::RecordDesignEventRange(EventName3, FPSAVG, { 60, 55, 50, 40, 30, 28, 25, 22, 20, 15, 10, 5, 0 });

		Fps5SecAVG.Empty(5);
	}
	else
	{
		Fps5SecAVG.Add(FMath::FloorToInt(FPSValue));
	}

	if (Fps10SecAVG.Num() == 10)
	{
		auto FPSAVG = IntArrayAvg(Fps10SecAVG);
		FShooterGameAnalytics::RecordDesignEventRange("Perfomance:Fps10SecAVG:Global:FpsMore[Key]", FPSAVG, { 60, 55, 50, 40, 30, 28, 25, 22, 20, 15, 10, 5, 0 });

		const auto EventName3 = FString::Printf(TEXT("Perfomance:Fps10SecAVG:%s:FpsMore[Key]"), *GameMapName);
		FShooterGameAnalytics::RecordDesignEventRange(EventName3, FPSAVG, { 60, 55, 50, 40, 30, 28, 25, 22, 20, 15, 10, 5, 0 });

		Fps5SecAVG.Empty(10);
	}
	else
	{
		Fps10SecAVG.Add(FMath::FloorToInt(FPSValue));
	}
}
