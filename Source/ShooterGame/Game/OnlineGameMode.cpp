//=============================================
#include "ShooterGame.h"

//=============================================
#include "Game/OnlineGameMode.h"
#include "Game/OnlineGameState.h"

#include "Game/Components/NodeComponent.h"


//=============================================
#include "Player/OnlinePlayerState.h"
#include "Player/Components/IdentityComponent.h"
#include "Components/NodeComponent/MatchEndResult.h"

//=============================================
#include "ShooterGameAchievements.h"
#include "GameSingletonExtensions.h"
#include "BasicGameAchievement.h"

//=============================================
AOnlineGameMode::AOnlineGameMode(const FObjectInitializer& ObjectInitializer)
	: bIsMultiPlayerGameMode(true)
{
	GameStateClass = AOnlineGameState::StaticClass();
	PlayerStateClass = AOnlinePlayerState::StaticClass();
	InactivePlayerStateLifeSpan = 30 * 60;
	MaxInactivePlayers = 128;
}



//=============================================
void AOnlineGameMode::EndMatch()
{
	//=============================================
	const auto result = GetMatchEndResult();

	//=============================================
	Super::EndMatch();

	//=============================================
	for (FConstPawnIterator Iterator = GetWorld()->GetPawnIterator(); Iterator; ++Iterator)
	{
		// If a pawn is marked pending kill, *Iterator will be NULL
		APawn* Pawn = Iterator->Get();
		if (Pawn && !GetValidObjectAs<ASpectatorPawn>(Pawn))
		{
			Pawn->TurnOff();
		}
	}

	//=============================================
	SendEndMatchRequest(result);
}


FGuid AOnlineGameMode::GetMatchWinnerTeamId() const
{
	return FGuid();
}

int32 AOnlineGameMode::GetHalfTimeLimit() const
{
	return FMath::CeilToInt(float(GetTimeLimit()) * 0.5f);
}

int32 AOnlineGameMode::GetRemainingTime() const
{
	return 0;
}

bool AOnlineGameMode::IsAllowJoinInProgress() const
{
	return false;
}

int32 AOnlineGameMode::GetTimeLimit() const
{
	return 0;
}

FMatchEndResult AOnlineGameMode::GetMatchEndResult() const
{
	//=============================================
	FMatchEndResult result;

	//=============================================
	result.WinnerTeamId = GetMatchWinnerTeamId();

	//=============================================
	const auto ElapsedTimeInGame = GetTimeLimit() - GetRemainingTime();

	//=============================================
	auto PlayerStates = GetPlayerStates<AOnlinePlayerState>(true);

	//=============================================
	PlayerStates.Sort([&](const AOnlinePlayerState& A, const AOnlinePlayerState& B)
	{
		return A.GetScore() > B.GetScore();
	});

	//=============================================
	for (const auto& It : PlayerStates)
	{
		//---------------------------------------
		if (It && It->bIsABot)
		{
			continue;
		}

		//---------------------------------------
		auto member = result.AddOrGetMember(It->MemberId);
		if (member == nullptr)
		{
			UE_LOG(LogInit, Error, TEXT("> AOnlineGameMode :: GetMatchEndResult [member: %s] was nullptr"), *It->MemberId.ToString());
			continue;
		}

		//---------------------------------------
		//	Добавляем матч в статистику
		It->GivePlayerAchievement(EShooterGameAchievements::MatchesTotal);

		//---------------------------------------
		//	Обновляем статистику побед и поражений
		//	так же даем бонус игроку за победу
		if(IsWinner(It))
		{
			It->GivePlayerAchievement(EShooterGameAchievements::MatchesWins);
		}
		else
		{
			It->GivePlayerAchievement(EShooterGameAchievements::MatchesLoses);
		}

		//---------------------------------------
		const auto ItIndex = PlayerStates.IndexOfByKey(It);
		if (ItIndex == 0)
		{
			It->GivePlayerAchievement(EShooterGameAchievements::FirstInMatch, 1);
		}
		else if (ItIndex == 1)
		{
			It->GivePlayerAchievement(EShooterGameAchievements::SecondInMatch, 1);
		}
		else if (ItIndex == 2)
		{
			It->GivePlayerAchievement(EShooterGameAchievements::ThirdInMatch, 1);
		}

		//---------------------------------------
		const auto score = FMath::Max(0, It->GetScore());

		//---------------------------------------
		const auto experience = score * 2;
		const auto money = score;
		
		//---------------------------------------
		It->GivePlayerAchievement(EShooterGameAchievements::Money, money);
		It->GivePlayerAchievement(EShooterGameAchievements::Experience, experience);

		//---------------------------------------
		It->GivePlayerAchievement(EShooterGameAchievements::TimeInGame, ElapsedTimeInGame);

		//---------------------------------------
		int64 scorepoint = 0;
		auto achievements = It->GetAchievements();
		UE_LOG(LogInit, Display, TEXT("=================[%s | achievements: %d]==================="), *It->GetPlayerName(), achievements.Num());

		for(const auto &a: achievements)
		{
			if (auto inst = FGameSingletonExtensions::FindAchievementById(a.Key))
			{
				auto props = inst->GetAchievementProperty();
				UE_LOG(LogInit, Display, TEXT("> %d - %d [name: %s][score point: %d]"), a.Key, a.Value, *props.Title.ToString(), props.ScorePoints);
				scorepoint += inst->GetAchievementProperty().ScorePoints;
			}
			else
			{
				UE_LOG(LogInit, Display, TEXT("> %d - %d"), a.Key, a.Value);
			
			}
		}
		//---------------------------------------
		UE_LOG(LogInit, Display, TEXT("=================[%s | score: %d vs %d]==================="), *It->GetPlayerName(), scorepoint, score);

		//---------------------------------------
		member->HasUsedPremiumAccount = It->HasPremiumAccount();
		member->Achievements = It->GetAchievements();
	}

	//=============================================
	return result;
}



void AOnlineGameMode::SendEndMatchRequest(const FMatchEndResult& result)
{
	//=============================================================
	if (const auto node = GetNodeComponent())
	{
		node->OnMatchEnded.AddDynamic(this, &AOnlineGameMode::OnMatchEnded);
		node->SendCompleteMatchRequest(result);
	}
	else
	{
		UE_LOG(LogInit, Fatal, TEXT("> SendGetAvalibleMembersRequest :: NodeComponent was nullptr"));
	}
}

void AOnlineGameMode::InitGame(const FString& MapName, const FString& Options, FString& ErrorMessage)
{
	Super::InitGame(MapName, Options, ErrorMessage);

	//=============================================================
	UE_LOG(LogInit, Display, TEXT("==============="));
	UE_LOG(LogInit, Display, TEXT("  Init Game Option: %s | Map: %s | ErrorMessage: %s"), *Options, *MapName, *ErrorMessage);

	//=============================================================
}

void AOnlineGameMode::PreInitializeComponents()
{
	Super::PreInitializeComponents();
}

void AOnlineGameMode::InitGameState()
{
	Super::InitGameState();

	if (!HasAnyFlags(RF_ClassDefaultObject | RF_ArchetypeObject))
	{
		//=============================================================
		if (auto gs = GetBaseGameState<AGameStateBase>())
		{
			UE_LOG(LogInit, Display, TEXT("  [GameState] Name: %s | FullName: %s"), *gs->GetName(), *gs->GetFullName());
		}
		else
		{
			UE_LOG(LogInit, Error, TEXT("> AOnlineGameMode::InitGameState :: GameState was nullptr"));
		}

		//=============================================================
		if (auto gs = GetBaseGameState<AOnlineGameState>())
		{
			//UE_LOG(LogInit, Display, TEXT("  [OnlineGameState::Token]: %s"), *(gs->GetSessionToken().Get().ToString()));
		}
		else
		{
			UE_LOG(LogInit, Error, TEXT("> AOnlineGameMode::InitGameState :: OnlineGameState was nullptr"));
		}


		//=============================================================
		if (auto node = GetNodeComponent())
		{
			node->BeginGetAvalibleMembersRequestPooling();
			node->BeginPoolingMatchInformation();
		}
		else
		{
			UE_LOG(LogInit, Fatal, TEXT("> AOnlineGameMode::InitGameState :: NodeComponent was nullptr"));
		}
	}
}

void AOnlineGameMode::EndPlay(const EEndPlayReason::Type EndPlayReason)
{
	Super::EndPlay(EndPlayReason);
}

void AOnlineGameMode::StartPlay()
{
	Super::StartPlay();
}

void AOnlineGameMode::PreLogin(const FString& Options, const FString& Address, const FUniqueNetIdRepl& UniqueId, FString& ErrorMessage)
{
	FString token;
	if (!UniqueId.IsValid())
	{
		UE_LOG(LogNet, Error, TEXT("> AOnlineGameMode::PreLogin | Address: %s | Id: Null / Invalid | Options: %s | ErrorMessage: %s"), *Address, *Options, *ErrorMessage);
	}


	if (UniqueId.IsValid())
	{
		token = UniqueId->ToString();
	}
	else
	{
		token = UGameplayStatics::ParseOption(Options, "UserToken");
	}

	UE_LOG(LogNet, Error, TEXT("> AOnlineGameMode::PreLogin | Address: %s | Id: %s | Options: %s | ErrorMessage: %s"), *Address, *token, *Options, *ErrorMessage);

#if UE_SERVER && !WITH_EDITOR
	const auto TargetMember = FindMatchMember(token);

	if (TargetMember)
	{
		Super::PreLogin(Options, Address, UniqueId, ErrorMessage);
	}
	else
	{
		ErrorMessage = FString::Printf(TEXT("Member {%s} not found on this match!"), *token);
	}
#else
	Super::PreLogin(Options, Address, UniqueId, ErrorMessage);
#endif
}

void AOnlineGameMode::PostLogin(APlayerController* NewPlayer)
{
	auto LocalPlayer = GetValidObjectAs<UNetConnection>(NewPlayer->Player);
	if (LocalPlayer && NewPlayer->PlayerState)
	{
		UE_LOG(LogGameMode, Warning, TEXT("AOnlineGameMode::PostLogin >> Controller: %s, Token: %s, PlayerState->UniqleId: %s"), *NewPlayer->GetName(), *LocalPlayer->PlayerId.ToString(), *NewPlayer->PlayerState->UniqueId.ToString());
	}

#if UE_SERVER && !WITH_EDITOR
	if (LocalPlayer)
	{
		const auto TargetMember = FindMatchMember(LocalPlayer->PlayerId->ToString());

		if (TargetMember)
		{
			NewPlayer->PlayerState->SetPlayerName(TargetMember->PlayerName);
			Super::PostLogin(NewPlayer);
		}

		auto TargetState = GetValidObjectAs<AOnlinePlayerState>(NewPlayer->PlayerState);

		if (TargetMember && TargetState)
		{
			TargetState->SetIdentityToken(TargetMember->TokenId);
			TargetState->MemberId = TargetMember->MemberId;

			UE_LOG(LogGameMode, Warning, TEXT("AOnlineGameMode::PostLogin[%s][%s / %s] [TargetMember: %s]"),
				*TargetMember->PlayerName, *TargetMember->TokenId.ToString(), *TargetState->GetIdentityToken().ToString(),
				*LocalPlayer->PlayerId->ToString());

			TargetState->GetIdentityComponent()->SendRequestExist();
		}
	}
#else
	if (GetNetMode() != NM_DedicatedServer)
	{
		auto TargetState = GetValidObjectAs<AOnlinePlayerState>(NewPlayer->PlayerState);
		if (TargetState)
		{
			if (auto Identity = TargetState->GetIdentityComponent())
			{
				Identity->SendRequestExist();
			}
		}
	}

	Super::PostLogin(NewPlayer);
#endif
}

void AOnlineGameMode::OnMatchStateSet()
{
	Super::OnMatchStateSet();

	UE_LOG(LogGameMode, Display, TEXT("OnMatchStateSet >> New Match State: %s"), *GetMatchState().ToString());	
}

UNodeComponent* AOnlineGameMode::GetNodeComponent()
{
	if (auto gs = GetBaseGameState<AOnlineGameState>())
	{
		return GetValidObject(gs->GetNodeComponent());
	}
	return nullptr;
}

UNodeComponent* AOnlineGameMode::GetNodeComponent() const
{
	if (auto gs = GetBaseGameState<AOnlineGameState>())
	{
		return GetValidObject(gs->GetNodeComponent());
	}
	return nullptr;
}

const FSessionMatchOptions& AOnlineGameMode::GetMatchOptions() const
{
	return GetMatchInformation().Options;
}

const FNodeSessionMatch& AOnlineGameMode::GetMatchInformation() const
{
	return GetNodeComponent()->GetMatchInformation();
}

const TArray<FNodeSessionMember>& AOnlineGameMode::GetMatchMebers() const
{
	return GetNodeComponent()->GetMatchMebers();
}

const FNodeSessionMember* AOnlineGameMode::FindMatchMember(const FString& InMemberId) const
{
	FGuid TargetId = FLokaGuid::FromString(InMemberId);
	return GetMatchMebers().FindByPredicate([&](const FNodeSessionMember& A) {
		return (A.MemberId == TargetId);
	});
}