#pragma once

//==============================================
#include "BasicGameComponent.h"
#include "NodeComponent/NodeSessionMatch.h"
#include "NodeComponent/MatchHeartBeatView.h"
#include "NodeComponent.generated.h"

struct FConfirmMemberContainer;
//==============================================
class UOperationRequest;


//==============================================
DECLARE_MULTICAST_DELEGATE_OneParam(FOnGetMatchInformationSuccessfully, const FNodeSessionMatch&);
DECLARE_DYNAMIC_MULTICAST_DELEGATE(FOnMatchEnded);

//==============================================
UCLASS(Transient)
class UNodeComponent : public UBasicGameComponent
{
	//friend class AShooterGame_CapturePoint;
	//friend class AShooterGame_FreeForAll;
	//friend class AShooterGame_Duel;

	GENERATED_BODY()

protected:
	UPROPERTY(Replicated) FNodeSessionMatch MatchInformation;

public:
	UNodeComponent();
	TAttribute<FGuid> GetIdentityToken() const override;

	static bool IsAllowMasterServerQueries();

	FORCEINLINE_DEBUGGABLE const FNodeSessionMatch& GetMatchInformation() const
	{
		return MatchInformation;
	}

	FORCEINLINE_DEBUGGABLE const TArray<FNodeSessionMember>& GetMatchMebers() const
	{
		return MatchInformation.Members;
	}

private:
	FORCEINLINE_DEBUGGABLE TArray<FNodeSessionMember>& GetInternalMatchMebers()
	{
		return MatchInformation.Members;
	}

	FNodeSessionMember* GetMatchMember(const FGuid& InMemberId);

	//======================================================================================
	//	GetMatchInformationRequest
private:
	void SendGetMatchInformationRequest() const;
	UPROPERTY(Transient, VisibleInstanceOnly) UOperationRequest* GetMatchInformationRequest;

	//	Срабатывает, когда удалось успешно получить информацию о матче
	void OnGetMatchInformation_Successfully(const FNodeSessionMatch& information);

	//	Срабатывает, когда не удалось получить информацию о матче
	void OnGetMatchInformation_Failed(const FRequestExecuteError& error) const;

	//	Срабатывает, когда закончились попытки получения информации о матче
	void OnGetMatchInformation_Complete(const FRequestExecuteError& error) const;

public:
	//	Запускает таймер на получение информации о матче
	void BeginPoolingMatchInformation();
	void OnSimulatedInformation(const FNodeSessionMatch& information)
	{
		OnGetMatchInformation_Successfully(information);
	}

	//======================================================================================
	//	StartMatchRequest
private :
	UPROPERTY(Transient, VisibleInstanceOnly) UOperationRequest* StartMatchRequest;
	void SendStartMatchRequest() const;

	void OnSendStartMatch_Successfully();
	void OnSendStartMatch_Failed(const FRequestExecuteError& error) const;
	void OnSendStartMatch_Complete(const FRequestExecuteError& error) const;


public:
	FOnGetMatchInformationSuccessfully OnGetMatchInformationSuccessfullyDelegate;
	void BeginPoolingStartMatchRequest();

	//======================================================================================
	//	CompleteMatchRequest
private:
	UPROPERTY(Transient, VisibleInstanceOnly) UOperationRequest* CompleteMatchRequest;

public:
	void SendCompleteMatchRequest(const struct FMatchEndResult& result) const;


	//======================================================================================
	//	GameNodeHeartBeatRequest
private:
	void SendGameNodeHeartBeatRequest() const;
	UPROPERTY(Transient, VisibleInstanceOnly) UOperationRequest* GameNodeHeartBeatRequest;

	//	Срабатывает, когда удалось успешно получить информацию
	void OnSendGameNodeHeartBeatRequest_Successfully() const;

	//	Срабатывает, когда не удалось получить информацию 
	void OnSendGameNodeHeartBeatRequest_Failed(const FRequestExecuteError& error) const;

	//	Срабатывает, когда закончились попытки 
	void OnSendGameNodeHeartBeatRequest_Complete(const FRequestExecuteError& error) const;

	//======================================================================================
	//	GetAvalibleMembersRequest
public:
	void BeginGetAvalibleMembersRequestPooling();

	bool IsAllowJoinInProgress() const;
	int64 GetRemainingTime() const;

private:
	UPROPERTY(Transient, VisibleInstanceOnly) UOperationRequest* GetAvalibleMembersRequest;

	//	Proxy функция для отправки запроса
	void SendGetAvalibleMembersRequest();

	//	Реальная функция для отправки запроса
	void SendGetAvalibleMembersRequest(const FMatchHeartBeatView& view) const;

	//	Срабатывает, когда удалось успешно получить информацию
	void OnGetAvalibleMembersRequest(const TArray<FNodeSessionMember>& InAvalibleMembers);

	//	Срабатывает, когда не удалось получить информацию 
	void OnGetAvalibleMembersRequest_Failed(const FRequestExecuteError& error) const;

	//	Срабатывает, когда закончились попытки 
	void OnGetAvalibleMembersRequest_Complete(const FRequestExecuteError& error) const;

	//======================================================================================
	//	ConfirmJoinNewMembersRequest
public:
	void SendConfirmJoinNewMembersRequest(const TArray<FConfirmMemberContainer>& members) const;

	private:
	UPROPERTY(Transient, VisibleInstanceOnly) UOperationRequest* ConfirmJoinNewMembersRequest;

	private:
	void OnConfirmJoinNewMembersRequest(const TArray<FConfirmMemberContainer>& members);
	void OnConfirmJoinNewMembersRequest_Failed(const FRequestExecuteError& error);

	//======================================================================================
	//	ConfirmJoinNewMembersRequest
public:
	void SendLeaveMemberRequest(const FGuid& member) const;
	UPROPERTY(Transient, VisibleInstanceOnly) UOperationRequest* LeaveMemberRequest;

	UPROPERTY(BlueprintAssignable, Category = Session)
	FOnMatchEnded OnMatchEnded;
};