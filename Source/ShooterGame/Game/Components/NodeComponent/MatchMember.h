#pragma once
#include "Engine/Types/LokaGuid.h"
#include "Player/PlayerGameAchievement.h"
#include "MatchMember.generated.h"


USTRUCT()
struct FMatchMember
{
	GENERATED_USTRUCT_BODY()

	UPROPERTY()		FLokaGuid MemberId;


	UPROPERTY()		TArray<FPlayerGameAchievement> Achievements;

	UPROPERTY()		bool HasUsedPremiumAccount;

	FMatchMember()
		: HasUsedPremiumAccount(false)
	{

	}

	FMatchMember(const FLokaGuid& id) 
		: MemberId(id)
		, HasUsedPremiumAccount(false)
	{

	}

};


USTRUCT()
struct FMatchMemberBotResultModel
{
	GENERATED_USTRUCT_BODY()

	UPROPERTY()		FLokaGuid TeamId;

	UPROPERTY()		FString PlayerName;

	UPROPERTY()		int32 Kills;
	UPROPERTY()		int32 Deads;
	UPROPERTY()		int32 Score;


};