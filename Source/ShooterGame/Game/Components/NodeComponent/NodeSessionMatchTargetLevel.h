#pragma once
#include "NodeSessionMatchTargetLevel.generated.h"

UENUM(BlueprintType, Category = Runtime)
namespace ENodeSessionMatchTargetLevel
{
	enum Type
	{
		None,
		ZeroLevel,
		FirstLevel,
		HighLevel,
		End,
	};
}