#pragma once

#include "MatchMember.h"
#include "Engine/Types/LokaGuid.h"
#include "MatchEndResult.generated.h"

USTRUCT()
struct FMatchEndResult
{
	GENERATED_USTRUCT_BODY()

	UPROPERTY()
	FLokaGuid WinnerTeamId;

	UPROPERTY()
	TArray<FMatchMember> Members;

	UPROPERTY()
	TArray<FMatchMemberBotResultModel> Bots;

	UPROPERTY()
	TMap<FLokaGuid, int32> Scores;

	const FMatchMember* GetMemberById(const FGuid& id) const
	{
		return Members.FindByPredicate([&, id](const FMatchMember& m)
		{
			return m.MemberId == id;
		});
	}

	FMatchMember* GetMemberById(const FGuid& id)
	{
		return Members.FindByPredicate([&, id](const FMatchMember& m)
		{
			return m.MemberId == id;
		});
	}

	FMatchMember* AddOrGetMember(const FGuid& id)
	{
		if (auto member = GetMemberById(id))
		{
			return member;
		}
		else
		{
			Members.Add(FMatchMember(id));
			return GetMemberById(id);
		}
	}
};