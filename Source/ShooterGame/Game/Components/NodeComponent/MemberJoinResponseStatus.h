#pragma once

#include "MemberJoinResponseStatus.generated.h"

UENUM()
enum class EMemberJoinResponseStatus : uint8
{
	None = 0,
	Allow = 1,
	DisAllow = 2,
	Confirmed = 4,
};

ENUM_CLASS_FLAGS(EMemberJoinResponseStatus);