#pragma once
#include "NodeSessionMember.h"
#include "Extensions/UEnumExtensions.h"
#include "Shared/SessionMatchOptions.h"
#include "NodeSessionMatchTargetLevel.h"
#include "NodeSessionMatch.generated.h"

USTRUCT(BlueprintType)
struct FNodeSessionMatch
{
	GENERATED_USTRUCT_BODY()

	static													FGuid TokenId;

	UPROPERTY()												FLokaGuid NodeId;

	UPROPERTY()												FLokaGuid MatchId;

	UPROPERTY(EditAnywhere, BlueprintReadWrite)				FSessionMatchOptions Options;

	UPROPERTY(EditAnywhere, BlueprintReadWrite)				TArray<FLokaGuid> TeamList;

	UPROPERTY()												TEnumAsByte<ENodeSessionMatchTargetLevel::Type> TargetLevel;

	UPROPERTY()												TArray<FNodeSessionMember> Members;

	FString MembersToString() const
	{
		FString members = "================= Members ========== \r\n";

		for (const auto m : Members)
		{
			members.Append(m.ToString());
		}

		return members;
	}

	FString ToString() const
	{
		TMap<FString, FStringFormatArg> args;
		args.Add("TokenId", FStringFormatArg(NodeId.ToString()));
		args.Add("MatchId", FStringFormatArg(MatchId.ToString()));
		args.Add("TargetLevel", FStringFormatArg(GetEnumValueAsString("ENodeSessionMatchTargetLevel", TargetLevel)));

		args.Add("Options", FStringFormatArg(Options.ToString()));
		args.Add("Members", FStringFormatArg(MembersToString()));

		args.Add("Header", FStringFormatArg("================= Match Information ========== \r\n"));

		return FString::Format(TEXT("{Header} TokenId: {TokenId} | MatchId: {MatchId} | TargetLevel: {TargetLevel} {Options} {Members} \r\n"), args);
	}
};
