#pragma once

//=========================================
#include "Engine/Types/IsoCountry.h"

#include "MemberJoinResponseStatus.h"
#include "NodeSessionMember.generated.h"


USTRUCT()
struct FNodeSessionMember
{
	GENERATED_USTRUCT_BODY()

	UPROPERTY()		FLokaGuid					PlayerId;
	UPROPERTY()		FLokaGuid					MemberId;
	UPROPERTY()		FLokaGuid					TokenId;
	UPROPERTY()		FLokaGuid					TeamId;
	UPROPERTY()		FLokaGuid					SquadId;
	UPROPERTY()		FString						PlayerName;
	UPROPERTY()		EMemberJoinResponseStatus	Status;
	UPROPERTY()		EIsoCountry					Country;

	UPROPERTY()		int32 TargetWeaponUpgrade;
	UPROPERTY()		int32 TargetWeaponLevel;
	UPROPERTY()		int32 TargetArmourLevel;
	UPROPERTY()		int32 WinRate;
	UPROPERTY()		uint64 EloRatingScore;

	

	bool IsAllowJoin() const
	{
		return EnumHasAnyFlags(Status, EMemberJoinResponseStatus::Allow);
	}

	bool IsConfirmed() const
	{
		return EnumHasAnyFlags(Status, EMemberJoinResponseStatus::Confirmed);
	}

	FNodeSessionMember()
		: Status(EMemberJoinResponseStatus::None)
		, Country(EIsoCountry::None)
		, TargetWeaponUpgrade(0)
		, TargetWeaponLevel(0)
		, TargetArmourLevel(0)
		, WinRate(0)
		, EloRatingScore(0)
	{
		
	}
	

	FString ToString() const
	{
		TMap<FString, FStringFormatArg> args;
		args.Add("PlayerId", FStringFormatArg(PlayerId.ToString()));
		args.Add("MemberId", FStringFormatArg(MemberId.ToString()));
		args.Add("TokenId", FStringFormatArg(TokenId.ToString()));
		args.Add("TeamId", FStringFormatArg(TeamId.ToString()));
		args.Add("SquadId", FStringFormatArg(SquadId.ToString()));
		args.Add("PlayerName", FStringFormatArg(PlayerName));

		args.Add("WeaponUpgrade", FStringFormatArg(TargetWeaponUpgrade));
		args.Add("WeaponLevel", FStringFormatArg(TargetWeaponLevel));
		args.Add("ArmourLevel", FStringFormatArg(TargetArmourLevel));
		args.Add("EloRatingScore", FStringFormatArg(EloRatingScore));

		

		args.Add("WinRate", FStringFormatArg(WinRate));


		return FString::Format(TEXT("> {PlayerName} [MemberId: {MemberId} | TeamId: {TeamId}] | [PlayerId: {PlayerId} | TokenId: {TokenId}] | SquadId: {SquadId} | WeaponUpgrade: {WeaponUpgrade} | WeaponLevel: {WeaponLevel} | ArmourLevel: {ArmourLevel} | WinRate: {WinRate} | EloRatingScore: {EloRatingScore} \r\n"), args);
	}

};