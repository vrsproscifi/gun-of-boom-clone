#pragma once

#include "MemberJoinResponseStatus.h"
#include "ConfirmMemberContainer.generated.h"

USTRUCT()
struct FConfirmMemberContainer
{
	GENERATED_USTRUCT_BODY()

	UPROPERTY()
		FLokaGuid MemberId;

	UPROPERTY()
		EMemberJoinResponseStatus Status;

	bool IsAllowJoin() const
	{
		return EnumHasAnyFlags(Status, EMemberJoinResponseStatus::Allow);
	}

	FConfirmMemberContainer()
		: Status(EMemberJoinResponseStatus::None)
	{
		
	}

	FConfirmMemberContainer(const FLokaGuid& InMemberId, const bool IsAllowJoin)
		: MemberId(InMemberId)
		, Status(IsAllowJoin ? EMemberJoinResponseStatus::Allow : EMemberJoinResponseStatus::DisAllow)
	{
		
	}

	FConfirmMemberContainer(const FLokaGuid& InMemberId, const EMemberJoinResponseStatus& InStatus)
		: MemberId(InMemberId)
		, Status(InStatus)
	{

	}

};


USTRUCT()
struct FConfirmMemberRequest
{
	GENERATED_USTRUCT_BODY()

	UPROPERTY()
	TArray<FConfirmMemberContainer> Members;

	FConfirmMemberRequest() : Members() {}
	FConfirmMemberRequest(const TArray<FConfirmMemberContainer>& In) : Members(In) {}
};
