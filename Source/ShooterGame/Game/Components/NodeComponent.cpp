#include "ShooterGame.h"
#include "Online.h"
#include "NodeComponent.h"
#include "Engine/Http/RequestManager.h"
#include "NodeComponent/MatchEndResult.h"
#include "NodeComponent/ConfirmMemberContainer.h"
#include "Game/OnlineGameMode.h"

struct FConfirmMemberRequest;

UNodeComponent::UNodeComponent()
{	
	//=========================================================
	//					[ GetMatchInformationRequest ]
	GetMatchInformationRequest = CreateRequest(FOperationRequestServerHost::MasterServer, "Node/CheckAvalibleMatch", 2.0f, EOperationRequestTimerMethod::CallViaIntervalOrResponse, FTimerDelegate::CreateUObject(this, &UNodeComponent::SendGetMatchInformationRequest), 10, true);
	GetMatchInformationRequest->BindObject<FNodeSessionMatch>(200).AddUObject(this, &UNodeComponent::OnGetMatchInformation_Successfully);
	GetMatchInformationRequest->OnFailedResponse.BindUObject(this, &UNodeComponent::OnGetMatchInformation_Failed);
	GetMatchInformationRequest->OnLoseAttemps.BindUObject(this, &UNodeComponent::OnGetMatchInformation_Complete);

	//=========================================================
	//					[ StartMatchRequest ]
	StartMatchRequest = CreateRequest(FOperationRequestServerHost::MasterServer, "Node/StartMatch", 2.0f, EOperationRequestTimerMethod::CallViaIntervalOrResponse, FTimerDelegate::CreateUObject(this, &UNodeComponent::SendStartMatchRequest), 10, true);
	StartMatchRequest->Bind(200).AddUObject(this, &UNodeComponent::OnSendStartMatch_Successfully);
	StartMatchRequest->OnFailedResponse.BindUObject(this, &UNodeComponent::OnSendStartMatch_Failed);
	StartMatchRequest->OnLoseAttemps.BindUObject(this, &UNodeComponent::OnSendStartMatch_Complete);

	//=========================================================
	//					[ CompleteMatchRequest ]
	CompleteMatchRequest = CreateRequest(FOperationRequestServerHost::MasterServer, "Node/CompleteMatch");
	CompleteMatchRequest->Bind(200).AddLambda([&]()
	{
		OnMatchEnded.Broadcast();
	});
	
	//=========================================================
	//					[ GetAvalibleMembersRequest ]
	GetAvalibleMembersRequest = CreateRequest(FOperationRequestServerHost::MasterServer, "Node/CheckAvalibleMember", 4.0f, EOperationRequestTimerMethod::CallViaInterval, FTimerDelegate::CreateUObject(this, &UNodeComponent::SendGetAvalibleMembersRequest), 10, true);
	GetAvalibleMembersRequest->BindArray<TArray<FNodeSessionMember>>(200).AddUObject(this, &UNodeComponent::OnGetAvalibleMembersRequest);
	GetAvalibleMembersRequest->OnFailedResponse.BindUObject(this, &UNodeComponent::OnGetAvalibleMembersRequest_Failed);
	GetAvalibleMembersRequest->OnLoseAttemps.BindUObject(this, &UNodeComponent::OnGetAvalibleMembersRequest_Complete);
	

	ConfirmJoinNewMembersRequest = CreateRequest(FOperationRequestServerHost::MasterServer, "Node/ConfirmMemberJoin");
	ConfirmJoinNewMembersRequest->BindArray<TArray<FConfirmMemberContainer>>(200).AddUObject(this, &UNodeComponent::OnConfirmJoinNewMembersRequest);
	ConfirmJoinNewMembersRequest->OnFailedResponse.BindUObject(this, &UNodeComponent::OnGetAvalibleMembersRequest_Failed);

	LeaveMemberRequest = CreateRequest(FOperationRequestServerHost::MasterServer, "Node/LeaveSession");

	//=========================================================
	//					[ GameNodeHeartBeatRequest ]
	GameNodeHeartBeatRequest = CreateRequest(FOperationRequestServerHost::MasterServer, "Node/GameNodeHeartBeat", 2.0f, EOperationRequestTimerMethod::CallViaInterval, FTimerDelegate::CreateUObject(this, &UNodeComponent::SendGameNodeHeartBeatRequest), 10, true);
	GameNodeHeartBeatRequest->Bind(200).AddUObject(this, &UNodeComponent::OnSendGameNodeHeartBeatRequest_Successfully);
	GameNodeHeartBeatRequest->OnFailedResponse.BindUObject(this, &UNodeComponent::OnSendGameNodeHeartBeatRequest_Failed);
	GameNodeHeartBeatRequest->OnLoseAttemps.BindUObject(this, &UNodeComponent::OnSendGameNodeHeartBeatRequest_Complete);
}

TAttribute<FGuid> UNodeComponent::GetIdentityToken() const
{
	return TAttribute<FGuid>::Create([this]
	{
		return GetMatchInformation().TokenId;
	});
}

bool UNodeComponent::IsAllowMasterServerQueries()
{
	return FPlatformProperties::IsServerOnly();
}

void UNodeComponent::BeginGetAvalibleMembersRequestPooling()
{
	UE_LOG(LogOnline, Display, TEXT("> BeginGetAvalibleMembersRequestPooling"));
	if (UNodeComponent::IsAllowMasterServerQueries())
	{
		StartTimer(GetAvalibleMembersRequest, 10.0f);
	}
	else
	{
		UE_LOG(LogOnline, Warning, TEXT("> BeginGetAvalibleMembersRequestPooling not avalible in non server mode!"));
	}
}

void UNodeComponent::OnConfirmJoinNewMembersRequest_Failed(const FRequestExecuteError& error)
{
	//=========================================
	auto& LocalMatchMembers = GetInternalMatchMebers();

	//=========================================
	TArray<FConfirmMemberContainer> ConfirmedMembers;

	//=========================================
	for (auto m : LocalMatchMembers)
	{
		if (m.IsConfirmed() == false)
		{
			ConfirmedMembers.Add(FConfirmMemberContainer(m.MemberId, m.Status));
		}
	}

	//=========================================
	if (ConfirmedMembers.Num())
	{
		SendConfirmJoinNewMembersRequest(ConfirmedMembers);
	}
}

void UNodeComponent::OnGetAvalibleMembersRequest(const TArray<FNodeSessionMember>& InAvalibleMembers)
{
//#if UE_SERVER && !WITH_EDITOR

	//=========================================
	auto& LocalMatchMembers = GetInternalMatchMebers();

	//=========================================
	TArray<FConfirmMemberContainer> ConfirmedMembers;

	//=========================================
	UE_LOG(LogInit, Error, TEXT("> OnGetAvalibleMembers | InAvalibleMembers: %d"), InAvalibleMembers.Num());

	//=========================================
	for (auto m : LocalMatchMembers)
	{
		if(EnumHasAnyFlags(m.Status, EMemberJoinResponseStatus::Confirmed) == false)
		{
			ConfirmedMembers.Add(FConfirmMemberContainer(m.MemberId, m.Status));
		}
	}

	//=========================================
	for (auto m : InAvalibleMembers)
	{
		FConfirmMemberContainer ConfirmedMember(m.MemberId, IsAllowJoinInProgress());

		if (ConfirmedMember.IsAllowJoin())
		{
			UE_LOG(LogInit, Error, TEXT("> OnGetAvalibleMembers :: member %s[%s] - Allow"), *m.MemberId.ToString(), *m.PlayerName);
			LocalMatchMembers.Add(m);
		}
		else
		{
			UE_LOG(LogInit, Error, TEXT("> OnGetAvalibleMembers :: member %s[%s] - DisAllow"), *m.MemberId.ToString(), *m.PlayerName);
		}
		ConfirmedMembers.Add(ConfirmedMember);
	}

	if (ConfirmedMembers.Num())
	{
		SendConfirmJoinNewMembersRequest(ConfirmedMembers);
	}
//#endif
}

FNodeSessionMember* UNodeComponent::GetMatchMember(const FGuid& InMemberId)
{
	return GetInternalMatchMebers().FindByPredicate([InMemberId](const FNodeSessionMember& InMember)
	{
		return InMember.MemberId == InMemberId;
	});
}

void UNodeComponent::OnConfirmJoinNewMembersRequest(const TArray<FConfirmMemberContainer>& members)
{
	//=========================================
	UE_LOG(LogInit, Error, TEXT("> OnConfirmJoinNewMembersRequest | members: %d"), members.Num());

	//=========================================
	auto& LocalMatchMembers = GetInternalMatchMebers();

	//=========================================
	for (auto m : members)
	{
		auto LocalMatchMember = GetMatchMember(m.MemberId);
		if(LocalMatchMember)
		{
			LocalMatchMember->Status |= EMemberJoinResponseStatus::Confirmed;

			if (m.IsAllowJoin())
			{
				UE_LOG(LogInit, Error, TEXT("> OnConfirmJoinNewMembersRequest :: member %s[%s] - Allow"), *LocalMatchMember->MemberId.ToString(), *LocalMatchMember->PlayerName);
			}
			else
			{
				UE_LOG(LogInit, Error, TEXT("> OnConfirmJoinNewMembersRequest :: member %s[%s] - DisAllow"), *LocalMatchMember->MemberId.ToString(), *LocalMatchMember->PlayerName);
			}
		}
	}

	//=========================================
}

bool UNodeComponent::IsAllowJoinInProgress() const
{
	if(auto gm = GetGameMode<AOnlineGameMode>())
	{
		return gm->IsAllowJoinInProgress();
	}
	return false;
}

int64 UNodeComponent::GetRemainingTime() const
{
	if (auto gm = GetGameMode<AOnlineGameMode>())
	{
		return gm->GetRemainingTime();
	}
	return 0;
}

void UNodeComponent::SendGetAvalibleMembersRequest()
{
	//===========================================
	FMatchHeartBeatView r;

	//===========================================
	r.TimeLeftInSeconds = GetRemainingTime();
	r.IsAllowJoinNewPlayers = IsAllowJoinInProgress();

	//===========================================
	SendGetAvalibleMembersRequest(r);
}

void UNodeComponent::OnGetAvalibleMembersRequest_Failed(const FRequestExecuteError& error) const
{
	UE_LOG(LogOnline, Fatal, TEXT("> OnGetAvalibleMembersRequest_Failed: %s"), *error.ToString());
}

void UNodeComponent::OnGetAvalibleMembersRequest_Complete(const FRequestExecuteError& error) const
{
	UE_LOG(LogOnline, Fatal, TEXT("> OnGetAvalibleMembersRequest_Complete: %s"), *error.ToString());
}


void UNodeComponent::SendGetAvalibleMembersRequest(const FMatchHeartBeatView& view) const
{
	UE_LOG(LogOnline, Display, TEXT("> SendGetAvalibleMembersRequest| TimeLeftInSeconds %d | IsAllowJoinNewPlayers: %d"), view.TimeLeftInSeconds, view.IsAllowJoinNewPlayers);
	GetAvalibleMembersRequest->SendRequestObject(view);
}

void UNodeComponent::SendConfirmJoinNewMembersRequest(const TArray<FConfirmMemberContainer>& members) const
{
	FConfirmMemberRequest container(members);
	UE_LOG(LogOnline, Display, TEXT("> SendConfirmJoinNewMembersRequest | Count: %d"), members.Num());
	ConfirmJoinNewMembersRequest->SendRequestObject(container);
}

void UNodeComponent::SendLeaveMemberRequest(const FGuid& member) const
{
	LeaveMemberRequest->SendRequestObject(member);
}

//===========================================================================================================================
//	StartMatchRequest

void UNodeComponent::SendStartMatchRequest() const
{
	UE_LOG(LogOnline, Display, TEXT("> SendStartMatchRequest"));

	if (auto world = GetValidWorld())
	{
		if (UNetDriver* NetDriver = GetValidObject(world->GetNetDriver()))
		{
			//====================================================
			//	Получаем ИП и Порт сервера
			FString TestStr(NetDriver->LowLevelGetNetworkNumber());

			//====================================================
			//	Получаем только порт сервера
			TestStr.RemoveAt(0, TestStr.Find(":") + 1);
			
			//====================================================
			//	Проверяем корректность получения порта сервера
			const int32 port = FCString::Atoi(*TestStr);
			if(port >= 2000 && port < 65000)
			{
				//	Отправляем порт сервера
				StartMatchRequest->SendRequestObject(TestStr);
				UE_LOG(LogOnline, Display, TEXT("> SendStartMatchRequest :: SendRequestObject(%s)"), *TestStr);
			}
			else
			{
				UE_LOG(LogOnline, Fatal, TEXT("> SendStartMatchRequest :: Invalid port: %d [%s]"), port, *TestStr);
			}
		}
		else
		{
			UE_LOG(LogOnline, Fatal, TEXT("> SendStartMatchRequest :: NetDriver was nullptr"));
		}
	}
	else
	{
		UE_LOG(LogOnline, Fatal, TEXT("> SendStartMatchRequest :: World was nullptr"));
	}
}

void UNodeComponent::OnSendStartMatch_Successfully()
{
  UE_LOG(LogOnline, Display, TEXT("> OnSendStartMatch_Successfully:: %s"), *MatchInformation.MatchId.ToString());

  //===================================
  StopTimer(StartMatchRequest);
}

void UNodeComponent::OnSendStartMatch_Failed(const FRequestExecuteError& error) const
{
  UE_LOG(LogOnline, Error, TEXT("> OnSendStartMatch_Failed: %s | %s"), *error.ToString(), *StartMatchRequest->GetItterationString());
}

void UNodeComponent::OnSendStartMatch_Complete(const FRequestExecuteError& error) const
{
  UE_LOG(LogOnline, Fatal, TEXT("> OnSendStartMatch_Complete: %s"), *error.ToString());
}

void UNodeComponent::BeginPoolingStartMatchRequest()
{
	UE_LOG(LogOnline, Display, TEXT("> BeginPoolingStartMatchRequest"));
	if (UNodeComponent::IsAllowMasterServerQueries())
	{
		//	Пытаемся отправить запрос на старт боя
		StartTimer(StartMatchRequest);
	}
	else
	{
		UE_LOG(LogOnline, Warning, TEXT("> BeginPoolingStartMatchRequest not avalible in non server mode!"));
	}
}


//===========================================================================================================================
//	CompleteMatchRequest

void UNodeComponent::SendCompleteMatchRequest(const FMatchEndResult& result) const
{
	UE_LOG(LogOnline, Display, TEXT("> SendCompleteMatchRequest"));
	if (UNodeComponent::IsAllowMasterServerQueries())
	{
		//	Временное решение
		CompleteMatchRequest->SendRequestObject(result);
	}
	else
	{
		UE_LOG(LogOnline, Warning, TEXT("> SendCompleteMatchRequest not avalible in non server mode!"));
	}
}

//===========================================================================================================================
//	GameNodeHeartBeatRequest


void UNodeComponent::SendGameNodeHeartBeatRequest() const
{
	UE_LOG(LogOnline, Display, TEXT("> SendGameNodeHeartBeatRequest"));
	GameNodeHeartBeatRequest->GetRequest();
}

//	Срабатывает, когда удалось успешно получить информацию
void UNodeComponent::OnSendGameNodeHeartBeatRequest_Successfully() const
{
	GameNodeHeartBeatRequest->ResetItterations();
}

//	Срабатывает, когда не удалось получить информацию 
void UNodeComponent::OnSendGameNodeHeartBeatRequest_Failed(const FRequestExecuteError& error) const
{
	UE_LOG(LogOnline, Display, TEXT("> OnSendGameNodeHeartBeatRequest_Failed: [%s] | Error: %s"), *GameNodeHeartBeatRequest->GetItterationString(), *error.ToString());
	
	//=======================================================
	GameNodeHeartBeatRequest->ItterationInc();
	if (GameNodeHeartBeatRequest->GetItterationNum() >= 5)
	{
		OnSendGameNodeHeartBeatRequest_Complete(error);
	}

	//=======================================================
}

//	Срабатывает, когда закончились попытки 
void UNodeComponent::OnSendGameNodeHeartBeatRequest_Complete(const FRequestExecuteError& error) const
{
	UE_LOG(LogOnline, Fatal, TEXT("> OnSendGameNodeHeartBeatRequest_Complete: %s"), *error.ToString());
}


//===========================================================================================================================
//	GetMatchInformationRequest


void UNodeComponent::SendGetMatchInformationRequest() const
{
	UE_LOG(LogOnline, Display, TEXT("> SendGetMatchInformationRequest"));
	if (UNodeComponent::IsAllowMasterServerQueries())
	{
		GetMatchInformationRequest->GetRequest();
	}
	else
	{
		UE_LOG(LogOnline, Warning, TEXT("> SendGetMatchInformationRequest not avalible in non server mode!"));
	}
}

void UNodeComponent::BeginPoolingMatchInformation()
{
	UE_LOG(LogOnline, Display, TEXT("> BeginPoolingMatchInformation"));
	if (UNodeComponent::IsAllowMasterServerQueries())
	{
		StartTimer(GetMatchInformationRequest);
	}
	else
	{
		UE_LOG(LogOnline, Warning, TEXT("> BeginPoolingMatchInformation not avalible in non server mode!"));
	}
}

void UNodeComponent::OnGetMatchInformation_Successfully(const FNodeSessionMatch& information)
{
	UE_LOG(LogOnline, Display, TEXT("OnGetMatchInformation_Successfully \n %s"), *information.ToString());

	//===================================
	MatchInformation = information;

	//===================================
	for(auto& member : MatchInformation.Members)
	{
		member.Status |= EMemberJoinResponseStatus::Allow;
		member.Status |= EMemberJoinResponseStatus::Confirmed;
	}

	//===================================
	//	Останавливаем попытку загрузить информацию о матче
	StopTimer(GetMatchInformationRequest);

	if (UNodeComponent::IsAllowMasterServerQueries())
	{
		//	Запускаем проверку на новых игроков
		StartTimer(GetAvalibleMembersRequest);

		//	Запускаем уведомление мастер-сервера о работоспособности игрового сервера
		StartTimer(GameNodeHeartBeatRequest);
	}

	//	Вызываем другие события при успешном получении информации о матче
	if (OnGetMatchInformationSuccessfullyDelegate.IsBound())
	{
		OnGetMatchInformationSuccessfullyDelegate.Broadcast(information);
	}
}

void UNodeComponent::OnGetMatchInformation_Failed(const FRequestExecuteError& error) const
{
	UE_LOG(LogOnline, Error, TEXT("OnGetMatchInformation_Failed [%s] \n %s"), *GetMatchInformationRequest->GetItterationString(), *error.ToString());
}

void UNodeComponent::OnGetMatchInformation_Complete(const FRequestExecuteError& error) const
{
	UE_LOG(LogOnline, Fatal, TEXT("OnGetMatchInformation_Complete \n %s"), *error.ToString());
}


void UNodeComponent::GetLifetimeReplicatedProps(TArray<FLifetimeProperty>& OutLifetimeProps) const
{
	Super::GetLifetimeReplicatedProps(OutLifetimeProps);

	DOREPLIFETIME(UNodeComponent, MatchInformation);
}
