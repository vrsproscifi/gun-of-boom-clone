#pragma once
//=============================================
#include "Game/BasicGameState.h"

//=============================================
#include "OnlineGameState.generated.h"

//=============================================
struct FNodeSessionMatch;

//=============================================
class UNodeComponent;

//=============================================
UCLASS(Config = Game)
class AOnlineGameState : public ABasicGameState
{
	GENERATED_UCLASS_BODY()
public:
	AOnlineGameState();
	void HandleBeginPlay() override;


	//====================================================
	//	NodeComponent
protected:
	UPROPERTY(Transient) UNodeComponent* NodeComponent;
	virtual void OnGetMatchInformation_Successfully(const FNodeSessionMatch& information);
	virtual void InitGameState(const FNodeSessionMatch& information);

public:
	UFUNCTION(BlueprintCallable, BlueprintPure, Category = Online)
	UNodeComponent* GetNodeComponent() const;
	UNodeComponent* GetNodeComponent();


	const FNodeSessionMatch* GetMatchInformation() const;
};