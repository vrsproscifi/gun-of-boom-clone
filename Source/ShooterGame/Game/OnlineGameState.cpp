//=============================================
#include "ShooterGame.h"

//=============================================
#include "Game/OnlineGameState.h"
#include "Game/Components/NodeComponent.h"

//=============================================
AOnlineGameState::AOnlineGameState(const FObjectInitializer& ObjectInitializer)
	: Super(ObjectInitializer)
{
	//==========================================================================
	NodeComponent = CreateDefaultSubobject<UNodeComponent>(TEXT("NodeComponent"));


	//==========================================================================
	//	Добавляем виртуальное событие при успешной загрузке информации о матче
	//	Что бы перегрузить его в режиме строительства
	NodeComponent->OnGetMatchInformationSuccessfullyDelegate.AddUObject(this, &AOnlineGameState::OnGetMatchInformation_Successfully);
}

void AOnlineGameState::HandleBeginPlay()
{
	Super::HandleBeginPlay();
	GetNodeComponent()->BeginPoolingMatchInformation();
}

void AOnlineGameState::OnGetMatchInformation_Successfully(const FNodeSessionMatch& information)
{
	UE_LOG(LogInit, Display, TEXT("AOnlineGameState::OnGetMatchInformation_Successfully"));
	GetNodeComponent()->BeginPoolingStartMatchRequest();
	InitGameState(information);
}

void AOnlineGameState::InitGameState(const FNodeSessionMatch& information)
{
	UE_LOG(LogInit, Display, TEXT(">> AOnlineGameState::InitGameState"));
}

const FNodeSessionMatch* AOnlineGameState::GetMatchInformation() const
{
	if (auto node = GetNodeComponent())
	{
		return &node->GetMatchInformation();
	}

	return nullptr;
}

UNodeComponent* AOnlineGameState::GetNodeComponent() const
{
	return GetValidObject(NodeComponent);
}


UNodeComponent* AOnlineGameState::GetNodeComponent()
{
	return GetValidObject(NodeComponent);
}

