// Copyright 1998-2014 Epic Games, Inc. All Rights Reserved.

#include "ShooterGameEditor.h"
#include "PropertyEditorModule.h"
#include "LevelEditor.h"
#include "Editor.h"
#include "Developer/MessageLog/Public/MessageLogModule.h"
#include "IAssetRegistry.h"
#include "Commands.h"
#include "ShooterGameSingleton.h"
#include "ShooterCaptureImages.h"
#include "ShooterMergeMeshses.h"

DEFINE_LOG_CATEGORY(LogShooterGameEditor);

TSharedPtr<FCaptureImagesWorker> FShooterGameEditorModule::CaptureImagesWorker;
TSharedPtr<FMergeMeshesWorker> FShooterGameEditorModule::MergeMeshesWorker;


class SHOOTERGAMEEDITOR_API FShooterExtenderCommands : public TCommands<FShooterExtenderCommands>
{
public:

	FShooterExtenderCommands() : TCommands<FShooterExtenderCommands>(
		"FLokaExtenderCommands", // Context name for fast lookup
		NSLOCTEXT("FLokaExtenderCommands", "FLokaExtenderCommands", "Loka Editor Extender Commands"), // Localized context name for displaying
		NAME_None, "EditorStyle")
	{}

	enum ECommand
	{
		ReloadSingleton,
		CaptureImages,
		MergeMeshses,
		End
	};

	virtual void RegisterCommands() override
	{
		FUICommandInfo::MakeCommandInfo(
			this->AsShared(),
			Command[ECommand::ReloadSingleton],
			TEXT("ReloadSingleton"),
			FText::FromString("Reload Singleton"),
			FText::FromString("Reload entity items"),
			FSlateIcon("EditorStyle", "Cascade.RegenerateLowestLOD", "Cascade.RegenerateLowestLOD.Small"),
			EUserInterfaceActionType::Button,
			FInputChord()
		);

		FUICommandInfo::MakeCommandInfo(
			this->AsShared(),
			Command[ECommand::CaptureImages],
			TEXT("CaptureImages"),
			FText::FromString("Capture Images"),
			FText::FromString("Capture images from all entity items."),
			FSlateIcon("EditorStyle", "StaticMeshEditor.SaveThumbnail", "StaticMeshEditor.SaveThumbnail.Small"),
			EUserInterfaceActionType::Button,
			FInputChord()
		);

		FUICommandInfo::MakeCommandInfo(
			this->AsShared(),
			Command[ECommand::MergeMeshses],
			TEXT("MergeMeshses"),
			FText::FromString("Merge Meshses"),
			FText::FromString("Merge Meshses from all entity items."),
			FSlateIcon("EditorStyle", "StaticMeshEditor.SetShowWireframe", "StaticMeshEditor.SetShowWireframe.Small"),
			EUserInterfaceActionType::Button,
			FInputChord()
		);

		UICommandList = MakeShareable(new FUICommandList());

		UICommandList->MapAction(Command[ECommand::ReloadSingleton], FExecuteAction::CreateLambda([]() {
			if (auto MySingleton = UShooterGameSingleton::Get())
			{
				MySingleton->ReloadGameData();
			}
		}));

		UICommandList->MapAction(Command[ECommand::CaptureImages], FExecuteAction::CreateLambda([]() {
			if (FShooterGameEditorModule::CaptureImagesWorker.IsValid())
			{
				FShooterGameEditorModule::CaptureImagesWorker->AddItems(UShooterGameSingleton::Get()->GetItems());
				FShooterGameEditorModule::CaptureImagesWorker->StartCapture();				
			}
		}));

		UICommandList->MapAction(Command[ECommand::MergeMeshses], FExecuteAction::CreateLambda([]() {
			if (FShooterGameEditorModule::MergeMeshesWorker.IsValid())
			{
				FShooterGameEditorModule::MergeMeshesWorker->AddItems(UShooterGameSingleton::Get()->GetItems());
				FShooterGameEditorModule::MergeMeshesWorker->StartMerge();
			}
		}));
	}

	FORCENOINLINE static FShooterExtenderCommands& GetAlt()
	{
		if (Instance.IsValid() == false)
		{
			FShooterExtenderCommands::Register();
		}

		return *(Instance.Pin());
	}

	FORCEINLINE TSharedRef<FUICommandList> GetCommandList() const { return UICommandList.ToSharedRef(); }
	FORCEINLINE TSharedRef<FUICommandInfo> GetCommand(const ECommand& InCommandTarget) const { return Command[InCommandTarget].ToSharedRef(); }

	static void GenerateMenu(FToolBarBuilder& InBuilder)
	{
		InBuilder.AddToolBarButton(GetAlt().GetCommand(ECommand::ReloadSingleton));
		InBuilder.AddComboButton(
			FUIAction(),
			FOnGetContent::CreateStatic(&FShooterExtenderCommands::GenerateSubList)
		);

		//InBuilder.AddToolBarButton(GetAlt().GetCommand(ECommand::CaptureImages));
		//InBuilder.AddToolBarButton(GetAlt().GetCommand(ECommand::MergeMeshses));
	}

protected:

	TSharedPtr<FUICommandInfo> Command[ECommand::End];
	TSharedPtr<FUICommandList> UICommandList;

	static TSharedRef<SWidget> GenerateSubList()
	{
		FMenuBuilder MenuBuilder(true, GetAlt().UICommandList);

		MenuBuilder.BeginSection(TEXT("SubList"), FText::FromString("Optional actions"));
		{
			MenuBuilder.AddMenuEntry(GetAlt().GetCommand(ECommand::CaptureImages));
			MenuBuilder.AddMenuEntry(GetAlt().GetCommand(ECommand::MergeMeshses));
		}

		return MenuBuilder.MakeWidget();
	}
};

typedef FShooterExtenderCommands::ECommand EShooterExtenderCommands;

void FShooterGameEditorModule::StartupModule()
{
	CaptureImagesWorker = MakeShareable(new FCaptureImagesWorker());
	MergeMeshesWorker = MakeShareable(new FMergeMeshesWorker());

	FShooterExtenderCommands::Register();

	//FPropertyEditorModule& PropertyModule = FModuleManager::LoadModuleChecked<FPropertyEditorModule>("PropertyEditor");
	//PropertyModule.RegisterCustomPropertyTypeLayout("TypeCost", FOnGetPropertyTypeCustomizationInstance::CreateStatic(&FTypeCostDetails::MakeInstance));
	//PropertyModule.RegisterCustomPropertyTypeLayout("ProfileHack", FOnGetPropertyTypeCustomizationInstance::CreateStatic(&FProfileHackDetails::MakeInstance));
	//PropertyModule.RegisterCustomClassLayout("ItemBaseEntity", FOnGetDetailCustomizationInstance::CreateStatic(&FItemEntitySaveLoadDetails::MakeInstance));

	FLevelEditorModule& LevelEditorModule = FModuleManager::LoadModuleChecked<FLevelEditorModule>("LevelEditor");

	TSharedPtr<FExtender> MyExtender = MakeShareable(new FExtender);
	MyExtender->AddToolBarExtension("Content", EExtensionHook::First, FShooterExtenderCommands::Get().GetCommandList(), FToolBarExtensionDelegate::CreateStatic(&FShooterExtenderCommands::GenerateMenu));
	LevelEditorModule.GetToolBarExtensibilityManager()->AddExtender(MyExtender);

	FMessageLogModule& MessageLogModule = FModuleManager::LoadModuleChecked<FMessageLogModule>("MessageLog");
	{
		FMessageLogInitializationOptions InitOptions;
		InitOptions.bDiscardDuplicates = true;
		MessageLogModule.RegisterLogListing("LokaErrors", NSLOCTEXT("LokaEditorErrors", "LokaEditorErrors.General", "LOKA Errors"), InitOptions);
	}
}

void FShooterGameEditorModule::ShutdownModule()
{
	CaptureImagesWorker.Reset();
}

IMPLEMENT_MODULE(FShooterGameEditorModule, ShooterGameEditor);