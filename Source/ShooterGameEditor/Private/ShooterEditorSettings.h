// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "Engine/DeveloperSettings.h"
#include "Engine/EngineTypes.h"
#include "ShooterEditorSettings.generated.h"


/**
 * 
 */
UCLASS(config = Editor, defaultconfig, meta = (DisplayName = "Shooter Editor Settings", ToolTip = "Shooter Editor settings for extra actions, capture entity, etc..."))
class UShooterEditorSettings : public UDeveloperSettings
{
	GENERATED_BODY()
	
public:

	UPROPERTY(globalconfig, EditAnywhere, Category = "Entity Capture Images", meta = (AllowedClasses = "MaterialInterface", DisplayName = "Capture Image Material"))
	FSoftObjectPath Asset_SourceMaterial;

	UPROPERTY(globalconfig, EditAnywhere, Category = "Entity Capture Images", meta = (AllowedClasses = "MaterialInterface", DisplayName = "Capture Icon Material"))
	FSoftObjectPath Asset_IconMaterial;

	UPROPERTY(globalconfig, EditAnywhere, Category = "Entity Capture Images", meta = (AllowedClasses = "TextureRenderTarget2D", DisplayName = "Capture Image Texture"))
	FSoftObjectPath Asset_SourceTexture;

	UPROPERTY(globalconfig, EditAnywhere, Category = "Entity Capture Images", meta = (AllowedClasses = "TextureCube", DisplayName = "Capture Sky Cubemap"))
	FSoftObjectPath Asset_SkyCubemap;

	UPROPERTY(globalconfig, EditAnywhere, Category = "Entity Capture Images", meta = (DisplayName = "Captured Texture Path", LongPackageName))
	FDirectoryPath Path_CapturedStaticTexture;

	UPROPERTY(globalconfig, EditAnywhere, Category = "Entity Capture Images", meta = (DisplayName = "Capture Sky Cubemap Angle"))
	float Conf_SkyCubemapAngle;

	UPROPERTY(globalconfig, EditAnywhere, Category = "Entity Capture Images", meta = (DisplayName = "Capture Sky Intensity"))
	float Conf_SkyIntensity;

	UPROPERTY(globalconfig, EditAnywhere, Category = "Entity Capture Images", meta = (DisplayName = "Capture Distance Multipler"))
	float Conf_DistanceMultipler;

	UPROPERTY(globalconfig, EditAnywhere, Category = "Entity Capture Images", meta = (DisplayName = "Capture Allow Auto-Save"))
	bool Conf_IfSaveCaptures;

	UPROPERTY(globalconfig, EditAnywhere, Category = "Entity Capture Images", meta = (DisplayName = "Capture Light Rotation"))
	FRotator Conf_LightRotation;

	UPROPERTY(globalconfig, EditAnywhere, Category = "Entity Capture Images", meta = (DisplayName = "Capture Model(Mesh) Rotation"))
	FRotator Conf_ModelRotation;

	UPROPERTY(globalconfig, EditAnywhere, Category = "Entity Merge Meshes", meta = (DisplayName = "Merge Meshes Path", LongPackageName))
	FDirectoryPath Path_MergeMeshes;

	UPROPERTY(globalconfig, EditAnywhere, Category = "Entity Merge Meshes", meta = (DisplayName = "Merge Meshes Allow Auto-Save"))
	bool Conf_IfSaveMergeMeshes;
};
