
#include "ShooterGameEditor.h"
#include "ShooterMergeMeshses.h"
#include "ShooterEditorSettings.h"
#include "AssetToolsModule.h"
#include "AssetRegistryModule.h"
#include "SlateMaterialBrush.h"
#include "KismetEditorUtilities.h"
#include "BasicItemEntity.h"
#include "Animation/SkeletalMeshActor.h"
#include "KismetEditorUtilities.h"
#include "FileHelpers.h"
#include "CharacterItemEntity.h"
#include "Factories/FbxSkeletalMeshImportData.h"
#include "SkeletalMeshLODImporterData.h"

FMergeMeshesWorker::FMergeMeshesWorker()
	: CurrentStage(EMergeMeshStage::None)
{
	CurrentPeriod = .0f;
}

void FMergeMeshesWorker::AddItems(const TArray<UBasicItemEntity*>& InItems)
{
	for (auto item : InItems)
	{
		if (CheckIsExistPackage(item) == false && Cast<UCharacterItemEntity>(item))
		{
			MergeQueue.Enqueue(item);
		}
	}
}

bool FMergeMeshesWorker::StartMerge()
{
	if (MergeQueue.IsEmpty())
	{
		return false;
	}

	CurrentPeriod = .0f;
	CurrentStage = EMergeMeshStage::Ready;

	FSlateNotificationManager::Get().AddNotification(FNotificationInfo(FText::FromString("Merge meshes entity was started.")));
	return true;
}

bool FMergeMeshesWorker::StopMerge()
{
	MergeQueue.Empty();
	CurrentPeriod = .0f;
	CurrentStage = EMergeMeshStage::None;

	FSlateNotificationManager::Get().AddNotification(FNotificationInfo(FText::FromString("Merge meshes entity was stopped, check save folder.")));
	return true;
}

bool FMergeMeshesWorker::ClearMerge()
{
	return false;
}

void FMergeMeshesWorker::Tick(float DeltaTime)
{
	if (MergeQueue.IsEmpty() == false && CurrentStage != EMergeMeshStage::None)
	{
		CurrentPeriod += DeltaTime;
		
		if (CurrentPeriod > .01f)
		{
			CurrentPeriod = .0f;

			auto MySettings = GetDefault<UShooterEditorSettings>();
			UBasicItemEntity* Target = nullptr;
			MergeQueue.Dequeue(Target);

			if (auto CharacterItem = Cast<UCharacterItemEntity>(Target))
			{
				FString PackageName, AssetName;
				FAssetToolsModule& AssetToolsModule = FModuleManager::Get().LoadModuleChecked<FAssetToolsModule>("AssetTools");

				//if (CharacterItem->TPPMeshData.ModularMesh.MeshesToMerge.Num() >= 2)
				//{
				//	AssetToolsModule.Get().CreateUniqueAssetName(MySettings->Path_MergeMeshes.Path + CharacterItem->GetName(), "_TPPMesh", PackageName, AssetName);
				//	auto SkeletalMeshMerged = NewObject<USkeletalMesh>(CreatePackage(nullptr, *PackageName), *AssetName, CharacterItem->TPPMeshData.ModularMesh.MeshesToMerge[0]->GetMaskedFlags());
				//
				//	SkeletalMeshMerged->PreEditChange(nullptr);
				//	SkeletalMeshMerged = UMeshMergeFunctionLibrary::MergeMeshes(CharacterItem->TPPMeshData.ModularMesh, SkeletalMeshMerged);
				//
				//	PackagesToSave.Add(SkeletalMeshMerged->GetOutermost());
				//
				//	if (MySettings->Conf_IfSaveMergeMeshes)
				//	{
				//		CharacterItem->TPPMeshData.Mesh = SkeletalMeshMerged;
				//		PackagesToSave.AddUnique(CharacterItem->GetOutermost());
				//	}
				//	//int32 SkeletalDepth = 0;
				//
				//	//FSkeletalMeshImportData TempData;
				//	//// Fill with data from buffer - contains the full .FBX file. 	
				//	//FSkeletalMeshImportData* SkelMeshImportDataPtr = &TempData;
				//
				//	//ProcessImportMeshSkeleton(SkeletalMeshMerged->Skeleton, SkeletalMeshMerged->RefSkeleton, SkeletalDepth, *SkelMeshImportDataPtr);
				//
				//	////Store the original fbx import data
				//	////LODModel.RawSkeletalMeshBulkData.SaveRawMesh(*SkelMeshImportDataPtr);
				//
				//	//// process materials from import data
				//	////ProcessImportMeshMaterials(SkeletalMesh->Materials, *SkelMeshImportDataPtr);
				//
				//
				//	//// process bone influences from import data
				//	//ProcessImportMeshInfluences(*SkelMeshImportDataPtr);
				//
				//
				//	SkeletalMeshMerged->CalculateInvRefMatrices();
				//	SkeletalMeshMerged->PostEditChange();
				//	SkeletalMeshMerged->MarkPackageDirty();
				//	FAssetRegistryModule::AssetCreated(SkeletalMeshMerged);
				//
				//	FSlateNotificationManager::Get().AddNotification(FNotificationInfo(FText::FromString(
				//		FString::Printf(TEXT("Mesh merged: %s"), *SkeletalMeshMerged->GetName())
				//	)));
				//
				//	UE_LOG(LogCore, Warning, TEXT("Mesh merged: %s"), *SkeletalMeshMerged->GetFullName());
				//}

				//if (CharacterItem->FPPMeshData.ModularMesh.MeshesToMerge.Num() >= 2)
				//{
				//	AssetToolsModule.Get().CreateUniqueAssetName(MySettings->Path_MergeMeshes.Path + CharacterItem->GetName(), "_FPPMesh", PackageName, AssetName);
				//	auto SkeletalMeshMerged = NewObject<USkeletalMesh>(CreatePackage(nullptr, *PackageName), *AssetName, CharacterItem->FPPMeshData.ModularMesh.MeshesToMerge[0]->GetMaskedFlags());

				//	SkeletalMeshMerged->PreEditChange(nullptr);
				//	SkeletalMeshMerged = UMeshMergeFunctionLibrary::MergeMeshes(CharacterItem->FPPMeshData.ModularMesh, SkeletalMeshMerged);
				//	PackagesToSave.Add(SkeletalMeshMerged->GetOutermost());

				//	if (MySettings->Conf_IfSaveMergeMeshes)
				//	{
				//		CharacterItem->FPPMeshData.Mesh = SkeletalMeshMerged;
				//		PackagesToSave.AddUnique(CharacterItem->GetOutermost());
				//	}

				//	SkeletalMeshMerged->CalculateInvRefMatrices();
				//	SkeletalMeshMerged->PostEditChange();
				//	SkeletalMeshMerged->MarkPackageDirty();
				//	FAssetRegistryModule::AssetCreated(SkeletalMeshMerged);

				//	FSlateNotificationManager::Get().AddNotification(FNotificationInfo(FText::FromString(
				//		FString::Printf(TEXT("Mesh merged: %s"), *SkeletalMeshMerged->GetName())
				//	)));

				//	UE_LOG(LogCore, Warning, TEXT("Mesh merged: %s"), *SkeletalMeshMerged->GetFullName());
				//}

				//if (CharacterItem->LobbyMeshData.ModularMesh.MeshesToMerge.Num() >= 2)
				//{
				//	AssetToolsModule.Get().CreateUniqueAssetName(MySettings->Path_MergeMeshes.Path + CharacterItem->GetName(), "_LobbyMesh", PackageName, AssetName);
				//	auto SkeletalMeshMerged = NewObject<USkeletalMesh>(CreatePackage(nullptr, *PackageName), *AssetName, CharacterItem->LobbyMeshData.ModularMesh.MeshesToMerge[0]->GetMaskedFlags());

				//	SkeletalMeshMerged->PreEditChange(nullptr);
				//	SkeletalMeshMerged = UMeshMergeFunctionLibrary::MergeMeshes(CharacterItem->LobbyMeshData.ModularMesh, SkeletalMeshMerged);
				//	PackagesToSave.Add(SkeletalMeshMerged->GetOutermost());

				//	if (MySettings->Conf_IfSaveMergeMeshes)
				//	{
				//		CharacterItem->LobbyMeshData.Mesh = SkeletalMeshMerged;
				//		PackagesToSave.AddUnique(CharacterItem->GetOutermost());
				//	}

				//	SkeletalMeshMerged->CalculateInvRefMatrices();
				//	SkeletalMeshMerged->PostEditChange();
				//	SkeletalMeshMerged->MarkPackageDirty();
				//	FAssetRegistryModule::AssetCreated(SkeletalMeshMerged);

				//	FSlateNotificationManager::Get().AddNotification(FNotificationInfo(FText::FromString(
				//		FString::Printf(TEXT("Mesh merged: %s"), *SkeletalMeshMerged->GetName())
				//	)));

				//	UE_LOG(LogCore, Warning, TEXT("Mesh merged: %s"), *SkeletalMeshMerged->GetFullName());
				//}
			}

			if (MergeQueue.IsEmpty())
			{
				if (MySettings->Conf_IfSaveMergeMeshes)
				{
					FEditorFileUtils::PromptForCheckoutAndSave(PackagesToSave, /*bCheckDirty*/false, /*bPromptToSave*/false);
				}

				FSlateNotificationManager::Get().AddNotification(FNotificationInfo(FText::FromString(
					FString::Printf(TEXT("Mesh merge was end, num of packages: %d"), PackagesToSave.Num())
				)));

				PackagesToSave.Empty();
				StopMerge();
			}
		}
	}
}

ETickableTickType FMergeMeshesWorker::GetTickableTickType() const
{
	return ETickableTickType::Always;
}

bool FMergeMeshesWorker::IsTickable() const
{
	return true;
}

TStatId FMergeMeshesWorker::GetStatId() const
{
	RETURN_QUICK_DECLARE_CYCLE_STAT(FMergeMeshesWorker, STATGROUP_Tickables);
}

void FMergeMeshesWorker::AddReferencedObjects(FReferenceCollector& Collector)
{

}

bool FMergeMeshesWorker::CheckIsExistPackage(const UObject* InObject) const
{
	auto MySettings = GetDefault<UShooterEditorSettings>();

	//if (InObject && MySettings)
	//{
	//	FString LeftS, RightS;
	//	InObject->GetFullName().Split(".", &LeftS, &RightS);

	//	return FindPackage(nullptr, *FString(MySettings->Path_CapturedStaticTexture.Path + RightS + "_ET")) || FindPackage(nullptr, *FString(MySettings->Path_CapturedStaticTexture.Path + RightS + "_EM"));
	//}

	return false;
}
