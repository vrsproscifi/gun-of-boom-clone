
#include "ShooterGameEditor.h"
#include "ShooterCaptureImages.h"
#include "ShooterEditorSettings.h"
#include "AssetToolsModule.h"
#include "AssetRegistryModule.h"
#include "SlateMaterialBrush.h"
#include "KismetEditorUtilities.h"
#include "BasicItemEntity.h"
#include "Animation/SkeletalMeshActor.h"
#include "KismetEditorUtilities.h"
#include "FileHelpers.h"

FCaptureImagesWorker::FCaptureImagesWorker()
	: CurrentStage(ECaptureItemStage::None)
{
	CurrentPeriod = .0f;

	CaptureWorld = nullptr;
	CaptureWorker = nullptr;
	DynamicMaterialForCapture = nullptr;
	PointLight = nullptr;
	SkyLight = nullptr;
}

void FCaptureImagesWorker::AddItems(const TArray<UBasicItemEntity*>& InItems)
{
	for (auto item : InItems)
	{
		if (CheckIsExistPackage(item) == false && item->GetPreviewMeshData().Mesh)
		{
			CaptureQueue.Enqueue(item);
		}
	}
}

bool FCaptureImagesWorker::StartCapture()
{
	if (CaptureQueue.IsEmpty())
	{
		return false;
	}

	if (CaptureWorld == nullptr)
	{
		CaptureWorld = NewObject<UWorld>(GetTransientPackage(), TEXT("CaptureEntityImagesWorld"), RF_Standalone | RF_Transient);
		CaptureWorld->WorldType = EWorldType::EditorPreview;

		FWorldContext& WorldContext = GEngine->CreateNewWorldContext(CaptureWorld->WorldType);
		WorldContext.SetCurrentWorld(CaptureWorld);

		CaptureWorld->InitializeNewWorld(UWorld::InitializationValues()
			.AllowAudioPlayback(false)
			.CreatePhysicsScene(false)
			.RequiresHitProxies(true)
			.CreateNavigation(false)
			.CreateAISystem(false)
			.ShouldSimulatePhysics(false)
			.SetTransactional(true));
		CaptureWorld->InitializeActorsForPlay(FURL());
		CaptureWorld->SetShouldTick(true);
	}

	if (CaptureWorld && CaptureWorld->IsValidLowLevel())
	{
		FSlateNotificationManager::Get().AddNotification(FNotificationInfo(FText::FromString("Capture entity images was started.")));

		auto MySettings = GetDefault<UShooterEditorSettings>();

		auto MyTexture = Cast<UTextureRenderTarget2D>(MySettings->Asset_SourceTexture.TryLoad());
		DynamicMaterialForCapture = UMaterialInstanceDynamic::Create(Cast<UMaterialInterface>(MySettings->Asset_SourceMaterial.TryLoad()), GetTransientPackage());
		DynamicMaterialForCapture->SetTextureParameterValue(TEXT("Source"), MyTexture);

		FActorSpawnParameters SpawnParameters;
		SpawnParameters.SpawnCollisionHandlingOverride = ESpawnActorCollisionHandlingMethod::AlwaysSpawn;
		CaptureTarget = CaptureWorld->SpawnActor<ASkeletalMeshActor>(ASkeletalMeshActor::StaticClass(), SpawnParameters);
		CaptureWorker = CaptureWorld->SpawnActor<ASceneCapture2D>(ASceneCapture2D::StaticClass(), FVector(-100, 0, 0), FRotator::ZeroRotator, SpawnParameters);
		PointLight = CaptureWorld->SpawnActor<ADirectionalLight>(ADirectionalLight::StaticClass(), FVector(-100, 0, 0), MySettings->Conf_LightRotation, SpawnParameters);
		PointLight->GetLightComponent()->Mobility = EComponentMobility::Movable;
		// Sky light for best quality
		SkyLight = NewObject<USkyLightComponent>(GetTransientPackage(), NAME_None, RF_Transient);
		SkyLight->Mobility = EComponentMobility::Movable;
		SkyLight->SourceType = ESkyLightSourceType::SLS_SpecifiedCubemap;
		SkyLight->SetCubemap(Cast<UTextureCube>(MySettings->Asset_SkyCubemap.TryLoad()));
		SkyLight->SourceCubemapAngle = MySettings->Conf_SkyCubemapAngle;
		SkyLight->SetIntensity(MySettings->Conf_SkyIntensity);
		SkyLight->SanitizeCubemapSize();
		SkyLight->SetCaptureIsDirty();
		SkyLight->SetRelativeTransform(FTransform::Identity);
		SkyLight->RegisterComponentWithWorld(CaptureWorld);

		if (CaptureTarget && CaptureWorker)
		{
			CaptureWorker->GetCaptureComponent2D()->bCaptureEveryFrame = false;
			CaptureWorker->GetCaptureComponent2D()->TextureTarget = MyTexture;
			CaptureWorker->GetCaptureComponent2D()->CaptureSource = ESceneCaptureSource::SCS_SceneColorHDR;
			CaptureWorker->GetCaptureComponent2D()->CompositeMode = ESceneCaptureCompositeMode::SCCM_Overwrite;
			CaptureWorker->GetCaptureComponent2D()->ShowOnlyActorComponents(CaptureTarget);

			CurrentPeriod = .0f;
			CurrentStage = ECaptureItemStage::Ready;

			return true;
		}
	}

	return false;
}

bool FCaptureImagesWorker::StopCapture()
{
	CaptureQueue.Empty();
	CurrentPeriod = .0f;
	CurrentStage = ECaptureItemStage::None;

	CaptureWorker->Destroy();
	CaptureWorker = nullptr;
	PointLight->Destroy();
	PointLight = nullptr;
	SkyLight->UnregisterComponent();
	SkyLight->DestroyComponent();
	SkyLight = nullptr;

	CaptureWorld->DestroyWorld(true);
	CaptureWorld = nullptr;

	return true;
}

bool FCaptureImagesWorker::ClearCaptures()
{
	return false;
}

void FCaptureImagesWorker::Tick(float DeltaTime)
{
	if (CaptureQueue.IsEmpty() == false && CaptureWorld && CurrentStage != ECaptureItemStage::None)
	{
		CurrentPeriod += DeltaTime;

		auto MySettings = GetDefault<UShooterEditorSettings>();

		if (CurrentPeriod > 0.2f && CurrentStage == ECaptureItemStage::Ready)
		{
			UBasicItemEntity* Target = nullptr;
			CaptureQueue.Peek(Target);

			if (Target && Target->IsValidLowLevel())
			{
				CaptureTarget->GetSkeletalMeshComponent()->SetSkeletalMesh(Target->GetPreviewMeshData().Mesh);
				CaptureTarget->GetSkeletalMeshComponent()->SetAnimInstanceClass(Target->GetPreviewMeshData().Anim);
				CaptureTarget->GetSkeletalMeshComponent()->SetRelativeLocation(FVector::ZeroVector);
				CaptureTarget->SetActorRotation(MySettings->Conf_ModelRotation);
				CaptureTarget->GetSkeletalMeshComponent()->UpdateBounds();
			}

			CurrentStage = ECaptureItemStage::Initialize;
			CurrentPeriod = .0f;
		}
		else if (CurrentPeriod > 0.2f && CurrentStage == ECaptureItemStage::Initialize)
		{
			CaptureTarget->GetSkeletalMeshComponent()->PrestreamTextures(2.1f, false, 1);
			
			const auto Testdfsdfsdf = -CaptureTarget->GetSkeletalMeshComponent()->Bounds.Origin * 1.0f;
			CaptureTarget->GetSkeletalMeshComponent()->SetRelativeLocation(Testdfsdfsdf);

			CurrentStage = ECaptureItemStage::Centering;
			CurrentPeriod = .0f;
		}
		else if (CurrentPeriod > 0.4f && CurrentStage == ECaptureItemStage::Centering)
		{
			UBasicItemEntity* Target = nullptr;
			CaptureQueue.Peek(Target);

			if (Target && Target->IsValidLowLevel())
			{
				FVector BoundOrigin;
				FVector BoundExtent;

				//CaptureTarget->SetActorRelativeTransform(/*Target->GetCaptureImageSettings().IsValid() ? Target->GetCaptureImageSettings().TransformOffsets : */FTransform::Identity);				

				CaptureTarget->GetActorBounds(false, BoundOrigin, BoundExtent);
				CaptureWorker->SetActorLocation(FVector(-BoundExtent.GetMax() * /*Target->GetCaptureImageSettings().IsValid() ? Target->GetCaptureImageSettings().DistanceMultipler : */MySettings->Conf_DistanceMultipler, 0, 0));			
				CaptureWorker->GetCaptureComponent2D()->ShowOnlyActorComponents(CaptureTarget);

				CurrentStage = ECaptureItemStage::Offsetting;
				CurrentPeriod = .0f;
			}
		}
		else if (CurrentPeriod > 0.1f && CurrentStage == ECaptureItemStage::Offsetting)
		{
			CaptureWorld->UpdateWorldComponents(false, true);

			CurrentStage = ECaptureItemStage::Update;
			CurrentPeriod = .0f;
		}
		else if (CurrentPeriod > 0.1f && CurrentStage == ECaptureItemStage::Update)
		{
			CaptureWorker->GetCaptureComponent2D()->CaptureScene();

			CurrentStage = ECaptureItemStage::Capture;
			CurrentPeriod = .0f;
		}
		else if (CurrentPeriod > 0.4f && CurrentStage == ECaptureItemStage::Capture)
		{
			UBasicItemEntity* Target = nullptr;
			CaptureQueue.Dequeue(Target);

			if (Target && Target->IsValidLowLevel())
			{
				FString LeftS, RightS;
				Target->GetFullName().Split(".", &LeftS, &RightS);
				LeftS = RightS;

				if (auto CreatedTexture = CreateStaticTexture(CaptureWorker->GetCaptureComponent2D()->TextureTarget, MySettings->Path_CapturedStaticTexture.Path, RightS))
				{
					CreatedTexture->CompressionSettings = TextureCompressionSettings::TC_Default;
					CreatedTexture->SRGB = true;
					CreatedTexture->Modify();
					CreatedTexture->PostEditChange();

					FString PackageName = CreatedTexture->GetOutermost()->GetName();
					FString PackageName2 = PackageName;

					FAssetToolsModule& AssetToolsModule = FModuleManager::Get().LoadModuleChecked<FAssetToolsModule>("AssetTools");
					AssetToolsModule.Get().CreateUniqueAssetName(MySettings->Path_CapturedStaticTexture.Path + LeftS, "_EM", PackageName, LeftS);

					UMaterialInstanceConstant* TempMat = NewObject<UMaterialInstanceConstant>(CreatePackage(nullptr, *PackageName), *LeftS, DynamicMaterialForCapture->Parent->GetMaskedFlags());
					TempMat->SetParentEditorOnly(DynamicMaterialForCapture->Parent);
					TempMat->SetTextureParameterValueEditorOnly(TEXT("Source"), CreatedTexture);

					UMaterialInstanceConstant* TempMat2 = nullptr;

					if (auto IconMat = Cast<UMaterialInterface>(MySettings->Asset_IconMaterial.TryLoad()))
					{
						AssetToolsModule.Get().CreateUniqueAssetName(MySettings->Path_CapturedStaticTexture.Path + LeftS, "_EMI", PackageName2, LeftS);

						TempMat2 = NewObject<UMaterialInstanceConstant>(CreatePackage(nullptr, *PackageName2), *LeftS, IconMat->GetMaskedFlags());
						TempMat2->SetParentEditorOnly(IconMat);
						TempMat2->SetTextureParameterValueEditorOnly(TEXT("Source"), CreatedTexture);
					}

					if (TempMat)
					{
						TempMat->Modify();
						TempMat->PostEditChange();

						// Notify the asset registry
						FAssetRegistryModule::AssetCreated(TempMat);
					}

					if (TempMat2)
					{
						TempMat2->Modify();
						TempMat2->PostEditChange();

						// Notify the asset registry
						FAssetRegistryModule::AssetCreated(TempMat2);
					}

					if (MySettings->Conf_IfSaveCaptures)
					{
						Target->GetItemProperty().Image = FSlateMaterialBrush(*TempMat, FVector2D(CreatedTexture->GetSizeX(), CreatedTexture->GetSizeY()));
						if (TempMat2) Target->GetItemProperty().Icon = FSlateMaterialBrush(*TempMat2, FVector2D(CreatedTexture->GetSizeX(), CreatedTexture->GetSizeY()));

						TArray<UPackage*> PackagesToSave;
						PackagesToSave.Add(CreatedTexture->GetOutermost());
						PackagesToSave.Add(Target->GetOutermost());
						PackagesToSave.Add(TempMat->GetOutermost());
						if (TempMat2) PackagesToSave.Add(TempMat2->GetOutermost());

						FEditorFileUtils::PromptForCheckoutAndSave(PackagesToSave, /*bCheckDirty*/false, /*bPromptToSave*/false);
					}

					FNotificationInfo NotificationInfo(FText::FromString("Captured image: " + CreatedTexture->GetFullName()));
					NotificationInfo.bUseLargeFont = false;

					auto localMyNotifyEc = FSlateNotificationManager::Get().AddNotification(NotificationInfo);
					localMyNotifyEc->SetExpireDuration(10);
					localMyNotifyEc->ExpireAndFadeout();
				}
			}

			CurrentPeriod = .0f;
			CurrentStage = ECaptureItemStage::Save;

			if (CaptureQueue.IsEmpty())
			{
				FSlateNotificationManager::Get().AddNotification(FNotificationInfo(FText::FromString("Capture entity images is completed! Destination folder: " + MySettings->Path_CapturedStaticTexture.Path)));
			}
		}
		else if (CurrentPeriod > 0.2f && CurrentStage == ECaptureItemStage::Save)
		{
			CurrentStage = CaptureQueue.IsEmpty() ? ECaptureItemStage::None : ECaptureItemStage::Ready;
			if (CurrentStage == ECaptureItemStage::None)
			{
				StopCapture();
			}
		}
	}
}

ETickableTickType FCaptureImagesWorker::GetTickableTickType() const
{
	return ETickableTickType::Always;
}

bool FCaptureImagesWorker::IsTickable() const
{
	return true;
}

TStatId FCaptureImagesWorker::GetStatId() const
{
	RETURN_QUICK_DECLARE_CYCLE_STAT(FCaptureImagesWorker, STATGROUP_Tickables);
}

void FCaptureImagesWorker::AddReferencedObjects(FReferenceCollector& Collector)
{
	Collector.AddReferencedObject(CaptureWorld);
	Collector.AddReferencedObject(CaptureWorker);
	Collector.AddReferencedObject(DynamicMaterialForCapture);
	Collector.AddReferencedObject(PointLight);
	Collector.AddReferencedObject(SkyLight);
}

UTexture2D* FCaptureImagesWorker::CreateStaticTexture(UTextureRenderTarget2D* InRenderTarget,
	const FString& InRelativePath,
	const FString& InName)
{
	UTexture2D* NewObj = nullptr;

	if (InRenderTarget)
	{
		FString Name;
		FString PackageName;

		if (InRelativePath.IsEmpty() == false && InName.IsEmpty() == false)
		{
			PackageName = InRelativePath + InName;
			Name = InName;
		}
		else if (InName.IsEmpty() == false)
		{
			PackageName = InRenderTarget->GetOutermost()->GetName() + InName;
			Name = InName;
		}

		FAssetToolsModule& AssetToolsModule = FModuleManager::Get().LoadModuleChecked<FAssetToolsModule>("AssetTools");
		AssetToolsModule.Get().CreateUniqueAssetName(PackageName, "_ET", PackageName, Name);

		NewObj = InRenderTarget->ConstructTexture2D(CreatePackage(nullptr, *PackageName), Name, InRenderTarget->GetMaskedFlags(), CTF_Default, nullptr);

		if (NewObj)
		{
			NewObj->Modify();
			NewObj->MarkPackageDirty();
			NewObj->PostEditChange();
			NewObj->UpdateResource();

			// Notify the asset registry
			FAssetRegistryModule::AssetCreated(NewObj);
		}
	}

	return NewObj;
}

bool FCaptureImagesWorker::CheckIsExistPackage(const UObject* InObject) const
{
	auto MySettings = GetDefault<UShooterEditorSettings>();

	if (InObject && MySettings)
	{
		FString LeftS, RightS;
		InObject->GetFullName().Split(".", &LeftS, &RightS);

		return FindPackage(nullptr, *FString(MySettings->Path_CapturedStaticTexture.Path + RightS + "_ET")) || FindPackage(nullptr, *FString(MySettings->Path_CapturedStaticTexture.Path + RightS + "_EM"));
	}

	return false;
}
