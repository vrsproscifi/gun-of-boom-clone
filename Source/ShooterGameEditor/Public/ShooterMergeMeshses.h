// Copyright 1998-2014 Epic Games, Inc. All Rights Reserved.
#pragma once

#include "GCObject.h"
#include "TickableEditorObject.h"

class UBasicItemEntity;

namespace EMergeMeshStage
{
	enum Type
	{
		None,
		Initialize,
		Merge,
		Save,
		Ready,
		End
	};
}

class SHOOTERGAMEEDITOR_API FMergeMeshesWorker
	: public FGCObject
	, public FTickableEditorObject
{
public:

	FMergeMeshesWorker();

	void AddItems(const TArray<UBasicItemEntity*>& InItem);
	bool StartMerge();
	bool StopMerge();
	bool ClearMerge();

	virtual void Tick(float DeltaTime) override;
	virtual ETickableTickType GetTickableTickType() const override;
	virtual bool IsTickable() const override;

	virtual TStatId GetStatId() const override;

	virtual void AddReferencedObjects(FReferenceCollector& Collector) override;

protected:

	EMergeMeshStage::Type CurrentStage;
	float CurrentPeriod;

	TQueue<UBasicItemEntity*> MergeQueue;
	TArray<UPackage*> PackagesToSave;

	bool CheckIsExistPackage(const UObject* InObject) const;
};
