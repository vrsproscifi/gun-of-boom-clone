// Copyright 1998-2014 Epic Games, Inc. All Rights Reserved.
#pragma once

#include "GCObject.h"
#include "TickableEditorObject.h"

class ASkeletalMeshActor;
class UBasicItemEntity;

namespace ECaptureItemStage
{
	enum Type
	{
		None,
		Initialize,
		Centering,
		Offsetting,
		Update,
		Capture,
		Save,
		Ready,
		End
	};
}

class SHOOTERGAMEEDITOR_API FCaptureImagesWorker
	: public FGCObject
	, public FTickableEditorObject
{
public:

	FCaptureImagesWorker();

	void AddItems(const TArray<UBasicItemEntity*>& InItem);
	bool StartCapture();
	bool StopCapture();
	bool ClearCaptures();

	virtual void Tick(float DeltaTime) override;
	virtual ETickableTickType GetTickableTickType() const override;
	virtual bool IsTickable() const override;

	virtual TStatId GetStatId() const override;

	virtual void AddReferencedObjects(FReferenceCollector& Collector) override;

protected:

	UWorld* CaptureWorld;
	ASceneCapture2D* CaptureWorker;
	ASkeletalMeshActor* CaptureTarget;
	ADirectionalLight* PointLight;
	USkyLightComponent* SkyLight;
	UMaterialInstanceDynamic* DynamicMaterialForCapture;

	ECaptureItemStage::Type CurrentStage;
	float CurrentPeriod;

	TQueue<UBasicItemEntity*> CaptureQueue;

	UTexture2D* CreateStaticTexture(UTextureRenderTarget2D* InRenderTarget, const FString& InRelativePath, const FString& InName);
	bool CheckIsExistPackage(const UObject* InObject) const;
};
