// Copyright 1998-2014 Epic Games, Inc. All Rights Reserved.
#pragma once

#ifndef __SHOOTERGAMEEDITOR_H__
#define __SHOOTERGAMEEDITOR_H__


////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
//	Engine Includes
#include "Engine.h"
#include "Net/UnrealNetwork.h"

#include "Core.h"
#include "Platform.h"
#include "CoreUObject.h"
#include "ModuleManager.h"

////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
//	Slate Includes
#include "SlateCore.h"
#include "SlateBasics.h"
#include "SlateExtras.h"

class FMergeMeshesWorker;
class FCaptureImagesWorker;
DECLARE_LOG_CATEGORY_EXTERN(LogShooterGameEditor, All, All)

class SHOOTERGAMEEDITOR_API FShooterGameEditorModule : public FDefaultModuleImpl
{
public:

	virtual void StartupModule() override;
	virtual void ShutdownModule() override;

	static TSharedPtr<FCaptureImagesWorker> CaptureImagesWorker;
	static TSharedPtr<FMergeMeshesWorker> MergeMeshesWorker;
};

#endif
