// Copyright 1998-2014 Epic Games, Inc. All Rights Reserved.

using UnrealBuildTool;
using System.IO;

public class ShooterGameEditor : ModuleRules
{
	public ShooterGameEditor(ReadOnlyTargetRules Target) : base(Target)
	{
        PrivatePCHHeaderFile = "Public/ShooterGameEditor.h";

        //==================================================
        //  PublicDependencyModuleNames
        PublicDependencyModuleNames.AddRange(
			new string[] {
				"Core", 
				"CoreUObject", 
				"Engine", 
				"InputCore",
				"AssetRegistry",
				"OnlineSubsystem",
				"OnlineSubsystemUtils",
				"HTTP",
				"Json",
				"JsonUtilities",
				"UnrealEd", 
				"PropertyEditor"
			});
		
        //==================================================
        //  PrivateDependencyModuleNames
        PrivateDependencyModuleNames.AddRange(
			new string[] {
				"InputCore",
				"Slate",
				"SlateCore",
				"XmlParser",
				"ShooterGame",
				"ShooterGameFrameWork",
			});

		PrivateIncludePathModuleNames.AddRange(
			new string[] {
				"ShooterGameFrameWork",
				"ShooterGame",
				"PropertyEditor",
			});

		//==================================================
		PublicIncludePaths.AddRange(
			new string[] {
				"ShooterGameEditor/Public",
			});

		PrivateIncludePaths.AddRange(
			new string[] {
				"ShooterGameEditor/Private",
			});

		PrivateIncludePaths.AddRange(
			new string[] {
				"ShooterGame",
				"ShooterGameFrameWork/Public",
				"ShooterGameFrameWork/Public/Player",
				"ShooterGameFrameWork/Public/Item",
				"ShooterGameFrameWork/Public/Game",
			}
		);

		//if (Target.Type != TargetRules.TargetType.Server)
		//{
		//	PublicDependencyModuleNames.AddRange(new string[] { "AppFramework", "RHI", "SlateRHIRenderer", "RenderCore", "MoviePlayer", "UMG" });
		//}
	}
}
