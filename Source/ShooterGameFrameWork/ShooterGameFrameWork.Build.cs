// Copyright 1998-2017 Epic Games, Inc. All Rights Reserved.

using UnrealBuildTool;

public class ShooterGameFrameWork : ModuleRules
{
	public ShooterGameFrameWork(ReadOnlyTargetRules Target) : base(Target)
    {
        OptimizeCode = CodeOptimization.InShippingBuildsOnly;
        PCHUsage = PCHUsageMode.UseExplicitOrSharedPCHs;
        //PrivatePCHHeaderFile = "Public/ShooterGameFrameWorkModule.h";

        PrivateIncludePaths.AddRange(
			new string[] { 
				"ShooterGameFrameWork/Private",
            }
		);
		
		PublicIncludePaths.AddRange(
			new string[] { 
				"ShooterGameFrameWork/Public",

			    //=======================================
				"ShooterGameFrameWork/Public/Engine",			
				"ShooterGameFrameWork/Public/Engine/Http",
				"ShooterGameFrameWork/Public/Engine/Types",

				//=======================================
				"ShooterGameFrameWork/Public/Extensions",		
			
				//=======================================
                "ShooterGameFrameWork/Public/Entity",

				//=======================================
                "ShooterGameFrameWork/Public/Game",
				
                //=======================================
                "ShooterGameFrameWork/Public/Item",
				"ShooterGameFrameWork/Public/Item/Armour",
				"ShooterGameFrameWork/Public/Item/Weapon",

                "ShooterGameFrameWork/Public/Award/EverydayAward",
                "ShooterGameFrameWork/Public/Award/LevelAward",


			    //=======================================
                "ShooterGameFrameWork/Public/Player",
            }
        );
		
        PublicDependencyModuleNames.AddRange(
			new string[] {
				"Core",
				"CoreUObject",
				"Engine",
                "PakFile",

                "OnlineSubsystem",
				"OnlineSubsystemUtils",
				"AssetRegistry",
                "AIModule",
				"GameplayTasks",

                "Json",
                "JsonUtilities",
                "HTTP",
                "Slate",
                "SlateCore",
            }
		);

        PrivateDependencyModuleNames.AddRange(
			new string[] {
                "Slate",
                "SlateCore",
                "InputCore",
				"Json",
			}
		);

		if (Target.Type == TargetRules.TargetType.Editor)
		{
			MinFilesUsingPrecompiledHeaderOverride = 3;
			bFasterWithoutUnity = true;
		}
	}
}
