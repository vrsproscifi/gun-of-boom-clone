#pragma once

//==============================
#include "LevelAwardContainer.generated.h"

//==============================
USTRUCT()
struct SHOOTERGAMEFRAMEWORK_API FLevelAwardItemContainer
{
	GENERATED_USTRUCT_BODY()

	UPROPERTY(VisibleAnywhere, Transient)		int32			ModelId;
	UPROPERTY(VisibleAnywhere, Transient)		int32			Amount;

	FLevelAwardItemContainer()
		: ModelId(0)
		, Amount(0)
	{

	}
};

USTRUCT()
struct SHOOTERGAMEFRAMEWORK_API FLevelRewardContainer
{
	GENERATED_USTRUCT_BODY()

	UPROPERTY()		TArray<FLevelAwardItemContainer>	Items;
	UPROPERTY()		int32								Donate;
	UPROPERTY()		int32								Money;
};

USTRUCT()
struct SHOOTERGAMEFRAMEWORK_API FLevelAwardContainer
{
	GENERATED_USTRUCT_BODY()

	UPROPERTY()		FLevelRewardContainer				Reward;
	UPROPERTY()		int32								EntityId;

	FLevelAwardContainer()
		: EntityId(0)
	{

	}
};