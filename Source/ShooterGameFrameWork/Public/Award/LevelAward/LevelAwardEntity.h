#pragma once
//==============================
#include "BasicAwardEntity.h"
#include "LevelAwardContainer.h"

//==============================
#include "LevelAwardEntity.generated.h"

//==============================
class UBasicItemEntity;

USTRUCT(BlueprintType, Blueprintable)
struct SHOOTERGAMEFRAMEWORK_API FLevelRewardItem : public FLevelAwardItemContainer
{
	GENERATED_USTRUCT_BODY()

	FLevelRewardItem()
		: Instance(nullptr)
	{

	}

	FLevelRewardItem(const int32& InModelId, const int32& InAmount);

protected:
	UPROPERTY(VisibleAnywhere, Transient) mutable UBasicItemEntity* Instance;

public:
	UBasicItemEntity* GetInstance();
	UBasicItemEntity* GetInstance() const;
};

//==============================
USTRUCT(BlueprintType, Blueprintable)
struct SHOOTERGAMEFRAMEWORK_API FLevelAwardProperty : public FBasicGameProperty
{
	GENERATED_USTRUCT_BODY()

	FLevelAwardProperty()
		: RewardDonate(0)
		, RewardMoney(0)
	{

	}

	UPROPERTY(VisibleAnywhere, Transient)	int32								RewardDonate;
	UPROPERTY(VisibleAnywhere, Transient)	int32								RewardMoney;
	UPROPERTY(VisibleAnywhere, Transient)	TArray<FLevelRewardItem>			RewardItems;
};



//==============================
/*
* Базовый класс для внутри игровых наград за получение уровня.
* Хранит информацию о списке предметов получаемых за достижение уровня
*/
UCLASS()
class SHOOTERGAMEFRAMEWORK_API ULevelAwardEntity : public UBasicAwardEntity
{
	GENERATED_UCLASS_BODY()

protected:
	//======================================================
	UPROPERTY(EditDefaultsOnly, meta = (ShowOnlyInnerProperties))
	FLevelAwardProperty LevelAwardProperty;

public:
	//======================================================
	void Initialize(const FLevelAwardContainer& initializer);
	
	//======================================================
	const FBasicGameProperty& GetBasicProperty() const override final
	{
		return LevelAwardProperty;
	}

	FBasicGameProperty& GetBasicProperty() override final
	{
		return LevelAwardProperty;
	}

	UFUNCTION(BlueprintCallable)
		const FLevelAwardProperty& GetLevelAwardProperty() const
	{
		return LevelAwardProperty;
	}

	FORCEINLINE FLevelAwardProperty& GetLevelAwardProperty()
	{
		return LevelAwardProperty;
	}

	//======================================================
	UFUNCTION(BlueprintCallable, BlueprintPure)
	TArray<UBasicItemEntity*> GetNewAvalibleItems() const;

	//======================================================
	/* 
		Список предметов который игрок получает в качестве вознагражения за достижение уровня
	*/
	UFUNCTION(BlueprintCallable, BlueprintPure)
	const TArray<FLevelRewardItem>& GetRewardItems() const;
};