#pragma once
//==============================
#include "BasicGameProperty.h"

//==============================
#include "SlateCore/Public/Styling/SlateBrush.h"

//==============================
#include "EverydayAwardProperty.generated.h"

//==============================
class UCaseEntity;

//==============================
USTRUCT(BlueprintType, Blueprintable)
struct SHOOTERGAMEFRAMEWORK_API FEverydayAwardProperty : public FBasicGameProperty
{
	GENERATED_USTRUCT_BODY()

	FEverydayAwardProperty()
		: RewardCaseId(0)
		, Instance(nullptr)
	{

	}

public:

	UPROPERTY(EditDefaultsOnly)				FSlateBrush Image;
	UPROPERTY(VisibleAnywhere, Transient)	int32	RewardCaseId;

protected:
	UPROPERTY(VisibleAnywhere, Transient) mutable UCaseEntity* Instance;

public:
	UCaseEntity* GetCaseInstance();
	UCaseEntity* GetCaseInstance() const;
};