#pragma once
//==============================
#include "BasicAwardEntity.h"
#include "EverydayAwardContainer.h"
#include "EverydayAwardProperty.h"

//==============================
#include "EverydayAwardEntity.generated.h"


//==============================
/*
* Базовый класс для внутри игровых наград за ежедневых вход в игру.
* Хранит информацию об иконке, заголовке, номере дня
* Для ежедневных наград - награды отображаются не из базы данных, а нарисованные
*/
UCLASS()
class SHOOTERGAMEFRAMEWORK_API UEverydayAwardEntity : public UBasicAwardEntity
{
	GENERATED_UCLASS_BODY()

private:
	UPROPERTY(Transient, AssetRegistrySearchable)				int32	RewardCaseId;

protected:
	virtual void UpdateEditorHelperProperties() override;


	//======================================================
	UPROPERTY(EditDefaultsOnly, meta = (ShowOnlyInnerProperties))
		FEverydayAwardProperty EverydayAwardProperty;

public:
	//======================================================
	void Initialize(const FEverydayAwardContainer& initializer);

public:
	const FBasicGameProperty& GetBasicProperty() const override final
	{
		return EverydayAwardProperty;
	}

	FBasicGameProperty& GetBasicProperty() override final
	{
		return EverydayAwardProperty;
	}

	UFUNCTION(BlueprintCallable)
		const FEverydayAwardProperty& GetEverydayAwardProperty() const
	{
		return EverydayAwardProperty;
	}

	FORCEINLINE FEverydayAwardProperty& GetEverydayAwardProperty()
	{
		return EverydayAwardProperty;
	}

	FORCEINLINE const int32& GetRewardCaseId() const
	{
		return GetEverydayAwardProperty().RewardCaseId;
	}

};