#pragma once

//==============================
#include "EverydayAwardContainer.generated.h"

USTRUCT()
struct SHOOTERGAMEFRAMEWORK_API FEverydayAwardContainer
{
	GENERATED_USTRUCT_BODY()

	UPROPERTY()		int32								EntityId;
	UPROPERTY()		int32								RewardCaseId;

	FEverydayAwardContainer()
		: EntityId(0)
		, RewardCaseId(0)
	{

	}
};