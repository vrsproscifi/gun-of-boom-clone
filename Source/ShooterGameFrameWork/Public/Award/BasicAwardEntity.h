#pragma once
//==============================
#include "BasicGameEntity.h"
#include "Extensions/Entity/GetEntityArrayAs.h"

//==============================
#include "BasicAwardEntity.generated.h"


//==============================
/*
* Базовый класс для внутри игровых наград.
* Используется в качестве навигации / разделения функционала (предметы, кейсы, достижения, награды) 
*/
UCLASS(abstract, Blueprintable, BlueprintType)
class SHOOTERGAMEFRAMEWORK_API UBasicAwardEntity : public UBasicGameEntity
{
	GENERATED_UCLASS_BODY()

};