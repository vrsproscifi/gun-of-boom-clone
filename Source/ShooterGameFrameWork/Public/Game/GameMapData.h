#pragma once
//==============================
#include "BasicGameData.h"
#include "SlateBrush.h"
#include "GameMapData.generated.h"
//==============================


UCLASS(Blueprintable, BlueprintType)
class SHOOTERGAMEFRAMEWORK_API UGameMapData : public UBasicGameData
{
	GENERATED_BODY()

protected:

	UPROPERTY(EditDefaultsOnly, BlueprintReadOnly) TArray<FSlateBrush> Images;

public:
	static UGameMapData* GetByFlagId(const int64& InFlagId);
	static UGameMapData* GetById(const int64& id);

	const TArray<FSlateBrush>& GetImages() const
	{
		return Images;
	}
};