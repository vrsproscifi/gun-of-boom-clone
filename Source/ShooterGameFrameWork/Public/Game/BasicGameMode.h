#pragma once
//=============================================
#include "Extensions/UObjectExtensions.h"

//=============================================
#include <GameFramework/GameMode.h>
#include <GameFramework/GameState.h>

//=============================================
#include "BasicGameMode.generated.h"

//=============================================
class ABasicGameState;

//=============================================
struct FPlayerGameAchievement;

UCLASS(BlueprintType, Blueprintable)
class SHOOTERGAMEFRAMEWORK_API ABasicGameMode : public AGameMode
{
	GENERATED_UCLASS_BODY()

protected:
	virtual void PreInitializeComponents() override;
	virtual void EndPlay(const EEndPlayReason::Type EndPlayReason) override;
	virtual void InitGame(const FString& MapName, const FString& Options, FString& ErrorMessage) override;
	virtual bool AllowPausing(APlayerController* PC) override
	{
		return false;
	}

		//=========================================
#pragma region DefaultTimer
protected:
	FTimerHandle TimerHandle_DefaultTimer;
	virtual void DefaultTimer();
#pragma endregion

public:
	template< class T = ABasicGameState>
	T* GetBaseGameState() const
	{
		return GetValidObjectAs<T>(GameState);
	}

	template< class T = ABasicGameState>
	T* GetBaseGameState()
	{
		return GetValidObjectAs<T>(GameState);
	}


	UWorld* GetValidWorld() const;

	UWorld* GetValidWorld();

	//=========================================
public:
#pragma region GetPlayerState

	/** @return Returns the first player controller, or NULL if there is not one. */
	APlayerController* GetFirstPlayerController() const;

#pragma region AController
	APlayerState * GetPlayerState(AController* InController) const;
	APlayerState * GetPlayerState(AController* InController);

	template <typename TPlayerState = APlayerState>
	TPlayerState* GetPlayerState(AController* InController) const
	{
		return GetValidObjectAs<TPlayerState>(GetPlayerState(InController));
	}

	template <typename TPlayerState = APlayerState>
	TPlayerState* GetPlayerState(AController* InController)
	{
		return GetValidObjectAs<TPlayerState>(GetPlayerState(InController));
	}
#pragma endregion

	//------------------------------------------------------------

#pragma region APawn
	APlayerState * GetPlayerState(APawn* InPawn) const;
	APlayerState * GetPlayerState(APawn* InPawn);

	template <typename TPlayerState = APlayerState>
	TPlayerState* GetPlayerState(APawn* InPawn) const
	{
		return GetValidObjectAs<TPlayerState>(GetPlayerState(InPawn));
	}

	template <typename TPlayerState = APlayerState>
	TPlayerState* GetPlayerState(APawn* InPawn)
	{
		return GetValidObjectAs<TPlayerState>(GetPlayerState(InPawn));
	}

#pragma endregion
#pragma endregion


	virtual bool GivePlayerAchievement(AController* InController, const int32& InAchievementId, const int32& InAmount = 1, const int32& InScore = 0);
	virtual bool GetAchievements(AController* InController, TArray<FPlayerGameAchievement>& OutAchievements) const;


public:
#pragma region GetPlayerStates
	TArray<APlayerState*> GetPlayerStates(const bool& InIsAllowBots = false, const bool& InIsAllowInactivePlayers = true) const;

	template<typename TPlayerState = APlayerState>
	TArray<TPlayerState*> GetPlayerStates(const bool& InIsAllowBots = false, const bool& InIsAllowInactivePlayers = true) const
	{
		//=======================================
		TArray<TPlayerState*> PlayerStatesResult;
		TArray<APlayerState*> PlayerStatesArray = GetPlayerStates(InIsAllowBots, InIsAllowInactivePlayers);

		//=======================================
		for (auto It : PlayerStatesArray)
		{
			if (auto ps = GetValidObjectAs<TPlayerState>(It))
			{
				PlayerStatesResult.Add(ps);
			}
		}

		//=======================================
		return PlayerStatesResult;
	}
#pragma endregion

};