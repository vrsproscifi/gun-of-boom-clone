#pragma once
//=========================================
#include "SlateBrush.h"

//=========================================
#include "BasicGameEntity.h"
#include "BasicGameProperty.h"
#include "Sound/SoundCue.h"

//=========================================
#include "BasicGameAchievement.generated.h"

//=========================================
class USoundCue;

USTRUCT(BlueprintType, Blueprintable)
struct SHOOTERGAMEFRAMEWORK_API FGameAchievementProperty : public FBasicGameProperty
{
	GENERATED_USTRUCT_BODY()
	UPROPERTY(EditDefaultsOnly)		FString							AchievementName;
	UPROPERTY(EditDefaultsOnly)		FSlateBrush						Image;
	UPROPERTY(EditDefaultsOnly)		TSoftObjectPtr<USoundCue>		NotifySound;
	UPROPERTY(EditDefaultsOnly)		int32							ScorePoints;
	UPROPERTY(EditDefaultsOnly)		bool							bShowNotify;
	UPROPERTY(EditDefaultsOnly)		bool							bPlayNotify;
	UPROPERTY(EditDefaultsOnly)		bool							bAllowOverride;
	UPROPERTY(EditDefaultsOnly)		bool							bDisplayInPersonalResult;

	DEFINE_SOFT_OBJECT_GETTER(USoundCue, NotifySound);

};

USTRUCT(BlueprintType, Blueprintable)
struct SHOOTERGAMEFRAMEWORK_API FGameAchievementStepProperty : public FBasicGameProperty
{
	GENERATED_USTRUCT_BODY()

	/*
	The number of the stage of achievement, regardless of the position of the stage in the array
	*/
	UPROPERTY(EditDefaultsOnly)		int32		Step;

	/*
	Item model as a reward
	*/
	UPROPERTY(VisibleAnywhere)		int32		Award;

	UPROPERTY(VisibleAnywhere)		int32		ScorePoints;

	/*
	Number of items as a reward
	*/
	UPROPERTY(VisibleAnywhere)		int32		Amount;

	UPROPERTY(EditDefaultsOnly)		FSlateBrush Image;
	UPROPERTY(EditDefaultsOnly)		int32		AchievementId;
};


UCLASS(BlueprintType, Blueprintable)
class SHOOTERGAMEFRAMEWORK_API UBasicGameAchievement : public UBasicGameEntity
{
	GENERATED_UCLASS_BODY()

	//================================================================================================================================
	//	For EDITOR HELPER
private:
//#if WITH_EDITOR && WITH_METADATA
	UPROPERTY(Transient, AssetRegistrySearchable)		int32			ScorePoints;
	UPROPERTY(Transient, AssetRegistrySearchable)		bool			bShowNotify;
	UPROPERTY(Transient, AssetRegistrySearchable)		bool			bPlayNotify;
	UPROPERTY(Transient, AssetRegistrySearchable)		bool			bAllowOverride;
	UPROPERTY(Transient, AssetRegistrySearchable)		bool			bDisplayInPersonalResult;
//#endif

protected:
	virtual void UpdateEditorHelperProperties() override;


	//================================================================================================================================
#pragma region Property
private:
	UPROPERTY(EditDefaultsOnly, meta = (ShowOnlyInnerProperties)) FGameAchievementProperty AchievementProperty;
#pragma region GetBasicProperty
public:
	const FBasicGameProperty& GetBasicProperty() const override final
	{
		return AchievementProperty;
	}

	FBasicGameProperty& GetBasicProperty() override final
	{
		return AchievementProperty;
	}

	UFUNCTION(BlueprintCallable)
	const FGameAchievementProperty& GetAchievementProperty() const
	{
		return AchievementProperty;
	}

	FGameAchievementProperty& GetAchievementProperty()
	{
		return AchievementProperty;
	}

#pragma endregion

	UPROPERTY(BlueprintReadOnly, EditDefaultsOnly, meta = (ShowOnlyInnerProperties)) TArray<FGameAchievementStepProperty> Steps;

	const FGameAchievementStepProperty* GetStepById(const int32& id) const;

	const FGameAchievementStepProperty* GetStepByStep(const int32& step) const;

	const FGameAchievementStepProperty* GetStepByAmount(const int32& amount) const;


	UFUNCTION(BlueprintCallable)
	bool GetStepById(const int32& InId, FGameAchievementStepProperty& OutStep);

	UFUNCTION(BlueprintCallable)
	bool GetStepByStep(const int32& InStep, FGameAchievementStepProperty& OutStep);


};