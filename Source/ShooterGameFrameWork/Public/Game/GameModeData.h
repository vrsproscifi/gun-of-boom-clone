#pragma once
//==============================
#include "BasicGameData.h"

//==============================
#include "GameModeData.generated.h"


UCLASS(Blueprintable, BlueprintType)
class SHOOTERGAMEFRAMEWORK_API UGameModeData : public UBasicGameData
{
	GENERATED_BODY()

public:
	static UGameModeData* GetById(const int64& id);
	static UGameModeData* GetByFlagId(const int64& InFlagId);
};