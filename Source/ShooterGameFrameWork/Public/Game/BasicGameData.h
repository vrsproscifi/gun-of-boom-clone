#pragma once
//==============================
#include "Engine/DataAsset.h"

//==============================
#include "BasicGameData.generated.h"


UCLASS(abstract, Blueprintable, BlueprintType)
class SHOOTERGAMEFRAMEWORK_API UBasicGameData : public UDataAsset
{
	GENERATED_BODY()

protected:
	UPROPERTY(EditDefaultsOnly, BlueprintReadOnly) FString Name;

	UPROPERTY(EditDefaultsOnly, BlueprintReadOnly) FText DisplayName;
	UPROPERTY(EditDefaultsOnly, BlueprintReadOnly) FText Description;
	UPROPERTY(VisibleDefaultsOnly) int64 Flags;
	// Value = Index
	UPROPERTY(EditDefaultsOnly) int64 Value;

public:
	virtual void PostLoad() override;

#if WITH_EDITOR
	virtual void PostEditChangeProperty(struct FPropertyChangedEvent& PropertyChangedEvent) override;
#endif

	const FText& GetDisplayName() const
	{
		return DisplayName;
	}

	const FText& GetDescription() const
	{
		return Description;
	}

	const FString& GetName() const
	{
		return Name;
	}

	const int64& GetValue() const
	{
		return Value;
	}

	int64 ToFlag() const
	{
		return int64(1) << Value;
	}
};