#pragma once
//=============================================
#include "Extensions/UObjectExtensions.h"

//=============================================
#include "Engine/Types/LokaGuid.h"
#include "Engine/Http/FailedOperationResponse.h"
#include "Engine/Http/OperationRequestTimerMethod.h"

//=============================================
#include "GameFramework/GameMode.h"
#include "GameFramework/GameState.h"
#include "Components/SceneComponent.h"

//=============================================
#include <TimerManager.h>

//=============================================
#include "BasicGameComponent.generated.h"

//=============================================
struct FOperationRequestServerHost;
class UOperationRequest;

//=============================================
class ABasicGameMode;
class ABasicGameState;

class ABasicPlayerController;
class ABasicPlayerState;

//=============================================
typedef const TAttribute<FGuid>& TokenAttribRef;
typedef TAttribute<FGuid> TokenAttrib;

//=============================================
UCLASS(Transient)
class SHOOTERGAMEFRAMEWORK_API UBasicGameComponent : public USceneComponent
{
	GENERATED_BODY()
private:
	void RemoveOperationRequests();

public:
	UBasicGameComponent();
	virtual TAttribute<FGuid> GetIdentityToken() const;
	virtual void EndPlay(const EEndPlayReason::Type EndPlayReason) override;
	virtual void BeginDestroy() override;


	UFUNCTION(BlueprintCallable, BlueprintPure, Category = Runtime)
		bool HasAuthority() const;


protected:
	UWorld* GetValidWorld() const;
	UWorld* GetValidWorld();

	//========================================================
#pragma region TimerManager
	FTimerManager* GetTimerManager() const;
	FTimerManager* GetTimerManager();

	void StartTimer(UOperationRequest* operation, const float& delay = 0) const;
	void StartTimer(UOperationRequest* operation, const float& delay = 0);

	void StopTimer(UOperationRequest* operation) const;
	void StopTimer(UOperationRequest* operation);

#pragma endregion
	
	//========================================================
#pragma region GetBaseGameMode
public:
	AGameMode * GetBaseGameMode();
	AGameMode * GetBaseGameMode() const;


	template<typename TGameMode = ABasicGameMode>
	TGameMode* GetGameMode() const
	{
		return GetValidObjectAs<TGameMode>(GetBaseGameMode());
	}

	template<typename TGameMode = ABasicGameMode>
	TGameMode* GetGameMode()
	{
		return GetValidObjectAs<TGameMode>(GetBaseGameMode());
	}

#pragma endregion


#pragma region GetBaseGameState
	AGameState* GetBaseGameState() const;
	AGameState* GetBaseGameState();

public:
	template< class T = ABasicGameState>
	T* GetBaseGameState() const
	{
		return GetValidObjectAs<T>(GetBaseGameState());
	}

	template< class T = ABasicGameState>
	T* GetBaseGameState()
	{
		return GetValidObjectAs<T>(GetBaseGameState());
	}
#pragma endregion

protected:

	UPROPERTY(Transient, VisibleInstanceOnly) 
	TArray<UOperationRequest*> OperationRequests;

	UOperationRequest* CreateRequest(const FOperationRequestServerHost& server_host, const FString& operation_name, TokenAttrib token, const float& rate = 0, const EOperationRequestTimerMethod& loop = EOperationRequestTimerMethod::None, FTimerDelegate timer = FTimerDelegate());
	UOperationRequest* CreateRequest(const FOperationRequestServerHost& server_host, const FString& operation_name, const float& rate, const EOperationRequestTimerMethod& loop, FTimerDelegate timer, TokenAttrib token, const int32& timerLimit, const bool& timerValidate);

	UOperationRequest* CreateRequest(const FOperationRequestServerHost& server_host, const FString& operation_name, const float& rate = 0, const EOperationRequestTimerMethod& loop = EOperationRequestTimerMethod::None, FTimerDelegate timer = FTimerDelegate(), const int32& timerLimit = 5, const bool& timerValidate = false);

private:
	UOperationRequest* AddRequest(UOperationRequest* request);
};