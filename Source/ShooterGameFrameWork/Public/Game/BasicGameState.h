#pragma once
//=============================================
#include <GameFramework/GameState.h>
#include <GameFramework/GameMode.h>

//=============================================
#include "Extensions/UObjectExtensions.h"

//=============================================
#include "BasicGameState.generated.h"

//=============================================
class AGameMode;
class ABasicGameMode;

//=============================================
UCLASS(BlueprintType, Blueprintable)
class SHOOTERGAMEFRAMEWORK_API ABasicGameState 
	: public AGameState
	//: public LokaNetUserInterface
{
	GENERATED_UCLASS_BODY()

	//============================================
	AGameMode* GetBaseGameMode();
	AGameMode* GetBaseGameMode() const;

	template<typename TGameMode = ABasicGameMode>
	TGameMode* GetBasicGameMode() const
	{
		return Cast<TGameMode>(GetBaseGameMode());
	}

	template<typename TGameMode = ABasicGameMode>
	TGameMode* GetBasicGameMode()
	{
		return Cast<TGameMode>(GetBaseGameMode());
	}

	//============================================
	UWorld* GetValidWorld() const;
	UWorld* GetValidWorld();

};