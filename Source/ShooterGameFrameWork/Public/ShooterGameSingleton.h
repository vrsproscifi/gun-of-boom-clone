#pragma once
//=============================================
#include "Extensions/UObjectExtensions.h"
#include "Engine/Http/FailedOperationResponse.h"
#include "Item/BasicItemContainer.h"
#include "Templates/SubclassOf.h"
#include "Containers/Map.h"

//====================================================
#include "ShooterGameSingleton.generated.h"

//====================================================
class UDataAsset;
class UBasicGameEntity;

class UBasicItemEntity;
class UBasicGameAchievement;

//====================================================
class UOperationRequest;
struct FGameStoreContainer;

//====================================================
class USubClassMap;
class UBoneDamageRate;

//====================================================
class UCaseEntity;

//====================================================
class ULevelAwardEntity;
class UEverydayAwardEntity;

//====================================================
UCLASS(Blueprintable, BlueprintType)
class SHOOTERGAMEFRAMEWORK_API UShooterGameSingleton : public UObject
{
	GENERATED_UCLASS_BODY()

protected:
	//=======================================================
	UPROPERTY(Transient, VisibleInstanceOnly) FDateTime					ListOfProductsRequesDate;
	UPROPERTY(Transient, VisibleInstanceOnly) class UOperationRequest*	ListOfProducts;
	void OnListOfProductsSuccessfully(const FGameStoreContainer& data);
	void OnListOfProductsFailed(const FRequestExecuteError& error);

public:

	static UShooterGameSingleton* Get();

	virtual void ProcessLoadGameData();

	virtual void LoadGameData();
	virtual void ReloadGameData();
	virtual bool IsLoadedData() const;
	virtual void PostInitProperties() override;

	//======================================================
public:
	UPROPERTY(Transient, VisibleAnywhere) bool bGameSingletonLoaded;
	UPROPERTY(Transient, VisibleAnywhere) FGuid LastLocalToken;
	UPROPERTY(Transient, VisibleAnywhere) FGuid LastLocalPlayerId;

	//======================================================
#pragma region Items
private:
	UPROPERTY(Transient, VisibleAnywhere)	TArray<ULevelAwardEntity*> LevelAwards;
	UPROPERTY(Transient, VisibleAnywhere)	TArray<UEverydayAwardEntity*> EverydayAwards;
	UPROPERTY(Transient, VisibleAnywhere)	TArray<UCaseEntity*> Cases;
	UPROPERTY(Transient, VisibleAnywhere)	TArray<UBasicItemEntity*> Items;
	UPROPERTY(Transient, VisibleAnywhere)	TArray<UBasicGameAchievement*> Achievements;
	UPROPERTY(Transient, VisibleAnywhere)	TArray<UDataAsset*> DataAssets;
	UPROPERTY(Transient, VisibleAnywhere)	UBoneDamageRate* DamageRates;
	UPROPERTY(Transient, VisibleAnywhere)	USubClassMap* SubClassMap;

	//-------------------------------------------------
#pragma region DamageRates
public:
	const UBoneDamageRate* GetDamageRates() const;
	const float* FindBonesDamageRate(const FName& InName) const;

#pragma endregion

	//-------------------------------------------------
#pragma region FindSubClassByName
public:
	UFUNCTION(BlueprintCallable, BlueprintPure, Category = Component)
	TSubclassOf<UObject> FindSubClassByName(const FName& InClassName) const;

#pragma endregion

	//-------------------------------------------------
#pragma region FindItemById
public:
	UFUNCTION(BlueprintCallable, BlueprintPure, Category = Component)
	UBasicItemEntity * FindItemById(const int32& InModelId) const;
	UBasicItemEntity * FindItemById(const int32& InModelId);

	UFUNCTION(BlueprintCallable, BlueprintPure, Category = Component)
	TArray<UBasicItemEntity*> FindItemsByType(const EGameItemType& InModelType) const;
	TArray<UBasicItemEntity*> FindItemsByType(const EGameItemType& InModelType);


	template<typename T = UBasicItemEntity>
	T* FindItemById(const int32& InModelId) const
	{
		return GetValidObjectAs<T>(FindItemById(InModelId));
	}

	template<typename T = UBasicItemEntity>
	T* FindItemById(const int32& InModelId)
	{
		return GetValidObjectAs<T>(FindItemById(InModelId));
	}
#pragma endregion

	//-------------------------------------------------
#pragma region FindItemByName
public:
	UFUNCTION(BlueprintCallable, BlueprintPure, Category = Component)
	UBasicItemEntity * FindItemByName(const FString& InModelName) const;
	UBasicItemEntity * FindItemByName(const FString& InModelName);

	template<typename T = UBasicItemEntity>
	T* FindItemByName(const FString& InModelName) const
	{
		return GetValidObjectAs<T>(FindItemByName(InModelName));
	}

	template<typename T = UBasicItemEntity>
	T* FindItemByName(const FString& InModelName)
	{
		return GetValidObjectAs<T>(FindItemByName(InModelName));
	}
#pragma endregion

	//-------------------------------------------------
#pragma region FindAwardById
public:
	UFUNCTION(BlueprintCallable, BlueprintPure, Category = Component)
	ULevelAwardEntity* FindLevelAwardById(const int32& InLevel) const;
	ULevelAwardEntity* FindLevelAwardById(const int32& InLevel);

	UFUNCTION(BlueprintCallable, BlueprintPure, Category = Component)
	UEverydayAwardEntity* FindEverydayAwardById(const int32& InDayStep) const;
	UEverydayAwardEntity* FindEverydayAwardById(const int32& InDayStep);
#pragma endregion

	//-------------------------------------------------
#pragma region FindCaseById
public:
	UFUNCTION(BlueprintCallable, BlueprintPure, Category = Component)
	UCaseEntity* FindCaseById(const int32& InModelId) const;
	UCaseEntity* FindCaseById(const int32& InModelId);
#pragma endregion

	//-------------------------------------------------
#pragma region FindAchievementById
public:
	UFUNCTION(BlueprintCallable, BlueprintPure, Category = Component)
	UBasicGameAchievement * FindAchievementById(const int32& InModelId) const;
	UBasicGameAchievement * FindAchievementById(const int32& InModelId);
#pragma endregion

	//-------------------------------------------------
	const TArray<UEverydayAwardEntity*>& GetEverydayAwards() const
	{
		return EverydayAwards;
	}

	const TArray<UBasicItemEntity*>& GetItems() const
	{
		return Items;
	}

	const TArray<UDataAsset*>& GetDataAssets() const
	{
		return DataAssets;
	}

	template<class T = UDataAsset>
	TArray<T*> GetDataAssets() const
	{
		TArray<T*> ReturnData;
		for (auto TargetAsset : DataAssets)
		{
			if (auto CastedAsset = Cast<T>(TargetAsset))
			{
				ReturnData.Add(CastedAsset);
			}
		}

		return ReturnData;
	}



#pragma endregion
};