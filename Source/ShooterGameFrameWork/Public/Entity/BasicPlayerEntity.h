#pragma once
//=======================================================
#include "Components/SceneComponent.h"

//=======================================================
#include "UObjectExtensions.h"

//=======================================================
#include "BasicPlayerEntity.generated.h"

//=======================================================
class UBasicGameEntity;

//=======================================================
UCLASS(abstract, Blueprintable, BlueprintType)
class SHOOTERGAMEFRAMEWORK_API UBasicPlayerEntity : public USceneComponent
{
	GENERATED_UCLASS_BODY()
	//==========================================
#pragma region EntityId
public:
	bool SetEntityId(const FString& id)
	{
		return FGuid::Parse(id, EntityId);
	}

	void SetEntityId(const FGuid& id)
	{
		EntityId = id;
	}

	UFUNCTION(BlueprintCallable, BlueprintPure, Category = Entity)
	const FGuid& GetEntityId() const
	{
		return EntityId;
	}

	static bool IsEqual(const UBasicPlayerEntity* InEntity, const FGuid& InEntityId);
	static bool IsEqualModelId(const UBasicPlayerEntity* InEntity, const int32& InModelId);

	UFUNCTION(BlueprintCallable, BlueprintPure, Category = Entity)
		bool IsEqual(const FGuid& id) const
	{
		return EntityId == id;
	}

	UFUNCTION(BlueprintCallable, BlueprintPure, Category = Entity)
	bool IsValidEntityId() const
	{
		return GetEntityId().IsValid();
	}

	UFUNCTION(BlueprintCallable, BlueprintPure, Category = Entity)
		bool IsInValidEntityId() const
	{
		return !IsValidEntityId();
	}

	bool IsEqual(const FString& InStringIndex) const
	{
		FGuid TargetGuid;
		return FGuid::Parse(InStringIndex, TargetGuid) && IsEqual(TargetGuid);
	}

protected:

	UPROPERTY(Replicated, VisibleInstanceOnly) FGuid EntityId;
#pragma endregion


protected:
	UFUNCTION() 	
	virtual void OnRep_Instance();
	
	UPROPERTY(ReplicatedUsing = OnRep_Instance, VisibleInstanceOnly)
	int32				EntityModel;
	
	UPROPERTY(VisibleInstanceOnly)
	UBasicGameEntity*	EntityInstance;

	UFUNCTION(BlueprintCallable)
	virtual void SetEntityModel(const int32& InModelId);

	virtual void SetEntity_Intrenal(UBasicGameEntity* entity);

public:

	UFUNCTION(BlueprintCallable)
	virtual void SetEntity(UBasicGameEntity* entity);

	UFUNCTION(BlueprintCallable)
	virtual void ReplaceEntity(UBasicGameEntity* entity);

	UFUNCTION(BlueprintCallable, BlueprintPure)
	virtual UBasicGameEntity* GetEntity() const;

	UBasicGameEntity* GetEntity();


	template<class T = UBasicGameEntity>
	T* GetEntity() const
	{
		return GetValidObjectAs<T>(EntityInstance);
	}

	template<class T = UBasicGameEntity>
	T* GetEntity()
	{
		return GetValidObjectAs<T>(EntityInstance);
	}

	bool IsEqual(const int32& id) const
	{
		return GetModelId() == id;
	}

	UFUNCTION(BlueprintCallable, BlueprintPure)
	const int32& GetModelId() const
	{
		return EntityModel;
	}

};