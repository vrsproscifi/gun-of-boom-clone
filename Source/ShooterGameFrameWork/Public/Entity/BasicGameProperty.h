#pragma once

#include "Extensions/SoftObjectExtensions.h"
#include "BasicGameProperty.generated.h"

//===================================
#ifdef INVALID_GAME_ENTITY_ID
#undef INVALID_GAME_ENTITY_ID
#endif

//===================================
#define INVALID_GAME_ENTITY_ID (-1)

//===================================

USTRUCT(BlueprintType, Blueprintable)
struct SHOOTERGAMEFRAMEWORK_API FBasicGameProperty
{
	GENERATED_USTRUCT_BODY()

	static FBasicGameProperty Default;

	FBasicGameProperty()
		: ModelId(INVALID_GAME_ENTITY_ID)
	{

	}

	UPROPERTY(EditDefaultsOnly)	int32 ModelId;
	UPROPERTY(EditDefaultsOnly, meta=(MultiLine=true)) FText Title;
	UPROPERTY(EditDefaultsOnly, meta=(MultiLine=true)) FText Description;

};