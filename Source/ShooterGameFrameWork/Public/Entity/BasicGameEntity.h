#pragma once
//==============================
#include "Engine/DataAsset.h"

//==============================
#include "BasicGameProperty.h"
#include "BasicGameEntity.generated.h"


UCLASS(abstract, Blueprintable, BlueprintType)
class SHOOTERGAMEFRAMEWORK_API UBasicGameEntity : public UDataAsset
{
	GENERATED_UCLASS_BODY()

	static const FBasicGameProperty Default;

private:
//#if WITH_EDITOR && WITH_METADATA
	UPROPERTY(Transient, AssetRegistrySearchable)	int32 ModelId;
	UPROPERTY(Transient, AssetRegistrySearchable)	FText Title;
	UPROPERTY(Transient, AssetRegistrySearchable)	FText Description;
//#endif

protected:
	virtual void UpdateEditorHelperProperties();

#if WITH_EDITOR && WITH_METADATA
	virtual void PostInitProperties() override;
	virtual bool Modify(bool bAlwaysMarkDirty) override;
	virtual void PostLoad() override;
	virtual void PostEditChangeProperty(struct FPropertyChangedEvent& PropertyChangedEvent)override;
	virtual void PostEditUndo() override;
#endif

public:
	UFUNCTION(BlueprintCallable, BlueprintPure)
	static FString GetEntityName(const UBasicGameEntity* InItem);

	//================================================================================================================================
#pragma region GetBasicProperty
public:
	UFUNCTION(BlueprintCallable, BlueprintPure)
	virtual const FBasicGameProperty& GetBasicProperty() const PURE_VIRTUAL(UBasicGameEntity::GetBasicProperty, return UBasicGameEntity::Default;);
	virtual FBasicGameProperty& GetBasicProperty() PURE_VIRTUAL(UBasicGameEntity::GetBasicProperty, return const_cast<FBasicGameProperty&>(UBasicGameEntity::Default););

	template<typename TProperty>
	const TProperty& GetBasicProperty() const
	{
		return static_cast<const TProperty&>(GetBasicProperty());
	}

	template<typename TProperty>
	TProperty& GetBasicProperty()
	{
		return static_cast<TProperty&>(GetBasicProperty());
	}

#pragma endregion

public:
	UFUNCTION(BlueprintCallable, BlueprintPure)
	const int32& GetModelId() const
	{
		return GetBasicProperty().ModelId;
	}

	UFUNCTION(BlueprintCallable, BlueprintPure)
	virtual FString GetModelName() const
	{
		return FString::FormatAsNumber(GetBasicProperty().ModelId);
	}

	UFUNCTION(BlueprintCallable, BlueprintPure)
	bool IsEqualModelId(const int32& InModelId) const 
	{
		return GetModelId() == InModelId;
	}

	UFUNCTION(BlueprintCallable, BlueprintPure)
	bool IsEqualModelName(const FString& InModelName) const
	{
		return GetModelName() == InModelName;
	}

	//================================================================================================================================

};