#pragma once

#include <Array.h>
//=========================================
#include "Award/LevelAward/LevelAwardContainer.h"
#include "Award/EverydayAward/EverydayAwardContainer.h"

#include "ProductItemContainer.h"
#include "BasicItemContainer.h"
#include "CaseContainer.h"

//=========================================
#include "GameStoreContainer.generated.h"
//

USTRUCT()
struct SHOOTERGAMEFRAMEWORK_API FGameStoreContainer
{
	GENERATED_USTRUCT_BODY()

	UPROPERTY()		TArray<FEverydayAwardContainer> EveryDay;
	UPROPERTY()		TArray<FLevelAwardContainer> Level;
	UPROPERTY()		TArray<FProductItemContainer> Products;
	UPROPERTY()		TArray<FBasicItemContainer> Items;
	UPROPERTY()		TArray<FCaseContainer> Cases;
};