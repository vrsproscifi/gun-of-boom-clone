// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "Player/BasicPlayerItem.h"
#include "ArmourPlayerItem.generated.h"


class UArmourItemEntity;


UCLASS()
class SHOOTERGAMEFRAMEWORK_API UArmourPlayerItem : public UBasicPlayerItem
{
	GENERATED_BODY()
	
public:

	UFUNCTION(BlueprintCallable, BlueprintPure, Category = Inventory)
	FORCENOINLINE UArmourItemEntity* GetArmourEntity() const;
	
};
