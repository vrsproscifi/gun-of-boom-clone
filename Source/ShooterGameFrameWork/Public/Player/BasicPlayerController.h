#pragma once
//=============================================
#include "Extensions/UObjectExtensions.h"

//=============================================
#include <GameFramework/PlayerController.h>

//=============================================
#include "BasicPlayerController.generated.h"

//=============================================
class ABasicPlayerCharacter;
class ABasicPlayerState;
class ABasicGameState;
class ABasicGameMode;

//=============================================
class AGameState;
class AGameMode;

//=============================================
UCLASS(BlueprintType, Blueprintable)
class SHOOTERGAMEFRAMEWORK_API ABasicPlayerController : public APlayerController
{
	GENERATED_UCLASS_BODY()

public:

	virtual void SetupInputComponent() override;
	virtual void OnEscape();

	virtual void FailedToSpawnPawn() override;

	UFUNCTION(Reliable, Client)
	virtual void PlayerTravel(const FString& URL, const FString& Token);

	virtual void PlayerTravelToLobby();

	UFUNCTION(BlueprintCallable)
	virtual int32 GivePlayerAchievement(const int32& InAchievementId, const int32& InAmount = 1, const int32& InScore = 0);

	virtual void EndPlay(const EEndPlayReason::Type EndPlayReason) override;

	template<typename T = ABasicPlayerCharacter>
	T* GetBaseCharacter() const
	{
		return GetValidObjectAs<T>(GetCharacter());
	}

	template<typename T = ABasicPlayerCharacter>
	T* GetBasePlayerCharacter() const
	{
		return GetValidObjectAs<T>(GetCharacter());
	}

	template<typename T = ABasicPlayerState>
	T* GetBasicPlayerState()
	{
		return GetValidObjectAs<T>(PlayerState);
	}

	template<typename T = ABasicPlayerState>
	T* GetBasicPlayerState() const
	{
		return GetValidObjectAs<T>(PlayerState);
	}

	//================================================
#pragma region GetValidWorld

	UWorld* GetValidWorld();

	UFUNCTION(BlueprintCallable)
		UWorld* GetValidWorld() const;

	FTimerManager* GetTimerManager() const;
	FTimerManager* GetTimerManager();
#pragma endregion

	//================================================
	UFUNCTION(BlueprintCallable)
	ABasicGameState* GetBasicGameState() const;
	ABasicGameState* GetBasicGameState();

	//================================================
	template<typename T = ABasicGameState>
	T* GetGameStateAs() const
	{
		return GetValidObjectAs<T>(GetBaseGameState());
	}

	template<typename T = ABasicGameState>
	T* GetGameStateAs()
	{
		return GetValidObjectAs<T>(GetBaseGameState());
	}

	//================================================
	UFUNCTION(BlueprintCallable)
	AGameState* GetBaseGameState() const;
	AGameState* GetBaseGameState();

	//================================================
	UFUNCTION(BlueprintCallable)
	ABasicGameMode* GetBasicGameMode() const;
	ABasicGameMode* GetBasicGameMode();

	//================================================
	template<typename T = ABasicGameMode>
	T* GetGameModeAs() const
	{
		return GetValidObjectAs<T>(GetBaseGameMode());
	}

	template<typename T = ABasicGameMode>
	T* GetGameModeAs()
	{
		return GetValidObjectAs<T>(GetBaseGameMode());
	}

	//================================================
	UFUNCTION(BlueprintCallable)
	AGameMode* GetBaseGameMode() const;
	AGameMode* GetBaseGameMode();

	virtual void SetPawn(APawn* InPawn) override;
};