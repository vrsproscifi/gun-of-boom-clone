#pragma once

#include "PlayerGameAchievement.generated.h"


//=============================================
USTRUCT(BlueprintType, Blueprintable)
struct SHOOTERGAMEFRAMEWORK_API FPlayerGameAchievement
{
	GENERATED_USTRUCT_BODY()
	UPROPERTY(EditDefaultsOnly)				int32 Key;
	UPROPERTY(EditDefaultsOnly)				int32 Value;
	UPROPERTY(EditDefaultsOnly)				int32 LastValue;

	FPlayerGameAchievement()
		: Key(-1)
		, Value(0)
		, LastValue(0)
	{

	}

	FPlayerGameAchievement(const int32& InKey, const int32& InValue)
		: Key(InKey)
		, Value(InValue)
	{
		
	}

};

class FPlayerGameAchievements : public TArray<FPlayerGameAchievement>
{
public:

	FPlayerGameAchievements() : TArray<FPlayerGameAchievement>() {}
	FPlayerGameAchievements(const TArray<FPlayerGameAchievement>& InArray) : TArray<FPlayerGameAchievement>(InArray) {}

	bool ContainsAchievement(const int32& InAchievementId) const
	{
		return GetAchievementById(InAchievementId) != nullptr;
	}

	FPlayerGameAchievement* GetAchievementById(const int32& InAchievementId)
	{
		//====================================================
		auto achievement = FindByPredicate([&, InAchievementId](const FPlayerGameAchievement& a)
		{
			return a.Key == InAchievementId;
		});

		return achievement;
	}

	const FPlayerGameAchievement* GetAchievementById(const int32& InAchievementId) const
	{
		//====================================================
		auto achievement = FindByPredicate([&, InAchievementId](const FPlayerGameAchievement& a)
		{
			return a.Key == InAchievementId;
		});

		return achievement;
	}

	int32 GetAchievementValueById(const int32& InAchievementId, const int32& InDefault) const
	{
		auto achievement = GetAchievementById(InAchievementId);
		if (achievement)
		{
			return achievement->Value;
		}
		return InDefault;
	}

	bool GetAchievementById(const int32& InAchievementId, FPlayerGameAchievement& OutAchievement)
	{
		auto achievement = GetAchievementById(InAchievementId);
		if (achievement)
		{
			OutAchievement = *achievement;
			return true;
		}
		return false;
	}


	void GivePlayerAchievement(const int32& InAchievementId, const int32& InAmount)
	{
		//====================================================
		auto achievement = GetAchievementById(InAchievementId);

		//====================================================
		if (achievement == nullptr)
		{
			Add(FPlayerGameAchievement(InAchievementId, InAmount));
		}
		else
		{
			achievement->LastValue = achievement->Value;
			achievement->Value += InAmount;
		}

		//====================================================
	}
};