// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "Player/BasicPlayerItem.h"
#include "AidPlayerItem.generated.h"


class UAidItemEntity;


UCLASS()
class SHOOTERGAMEFRAMEWORK_API UAidPlayerItem : public UBasicPlayerItem
{
	GENERATED_BODY()
	
public:

	UFUNCTION(BlueprintCallable, BlueprintPure, Category = Inventory)
	FORCENOINLINE UAidItemEntity* GetAidEntity() const;
	
};
