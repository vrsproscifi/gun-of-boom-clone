#pragma once
//=======================================================
#include "BasicPlayerEntity.h"
#include "BasicItemProperty.h"

//=======================================================
#include "BasicPlayerItem.generated.h"

//=======================================================
class UBasicItemEntity;

//=======================================================
//	UPlayerInventoryItem

UENUM()
namespace EPlayerItemState
{
	enum Type
	{
		None,
		Purchased,
		Equipped,
		Active,
		Temporary,
		End
	};
}

/*
 * Inventory item
 */
UCLASS(ClassGroup = (Utility, Common), BlueprintType, hideCategories = (Trigger, PhysicsVolume), meta = (BlueprintSpawnableComponent))
class SHOOTERGAMEFRAMEWORK_API UBasicPlayerItem : public UBasicPlayerEntity
{
	GENERATED_UCLASS_BODY()

public:
	static FString GetEntityName(const UBasicPlayerItem* InItem);

	UFUNCTION(BlueprintCallable, Category = Inventory)
	bool HasAllowSomeBought() const;

	UFUNCTION(BlueprintCallable, Category = Inventory)
	bool HasIgnoreRequirements() const;


	UFUNCTION(BlueprintCallable, Category = Inventory)
	virtual void InitializeInstanceDynamic(UBasicItemEntity* InInstance, const bool InIsReplicated = true);
	virtual void SetItemEntity(UBasicItemEntity* entity);

	UFUNCTION(BlueprintCallable, Category = Inventory)
	virtual void SetState(const TEnumAsByte<EPlayerItemState::Type>& InState);

	UFUNCTION(BlueprintCallable, BlueprintPure, Category = Inventory)
	TEnumAsByte<EPlayerItemState::Type> GetState() const;

	UFUNCTION(BlueprintCallable, BlueprintPure, Category = Inventory)
	USkeletalMesh* GetPreviewMesh() const;

	UFUNCTION(BlueprintCallable, BlueprintPure, Category = Inventory)
	UBasicItemEntity* GetEntityBase() const;

	UFUNCTION(BlueprintCallable, BlueprintPure, Category = Inventory)
	virtual float GetMass() const;

	UFUNCTION(BlueprintCallable, Category = Inventory)
	void SetLevel(const int32& InLevel);

	UFUNCTION(BlueprintCallable, Category = Inventory)
		void IncLevel();

	UFUNCTION(BlueprintCallable, BlueprintPure, Category = Inventory)
	const uint8& GetLevel() const;

	UFUNCTION(BlueprintCallable, BlueprintPure, Category = Inventory)
		uint8 GetRequiredLevel() const;

	UFUNCTION(BlueprintCallable, BlueprintPure, Category = Inventory)
		uint8 GetNextLevel() const;

	UFUNCTION(BlueprintCallable, BlueprintPure, Category = Inventory)
		uint8 GetMaxLevel() const;

	UFUNCTION(BlueprintCallable, BlueprintPure, Category = Inventory)
	bool IsMaximumLevelReached()  const;


	UFUNCTION(BlueprintCallable, Category = Inventory)
		void SetAmount(const int32& InAmount);

	UFUNCTION(BlueprintCallable, BlueprintPure, Category = Inventory)
	const int32& GetAmount() const;

	UFUNCTION(BlueprintCallable, Category = Inventory)
		bool AnyAmount() const;

	UFUNCTION(BlueprintCallable, Category = Inventory)
		virtual void UseItem();


	UFUNCTION(BlueprintCallable, BlueprintPure, Category = Inventory)
	EGameItemType GetItemType() const;

#pragma region GetItemProperty

	const FBasicItemProperty* GetBaseItemProperty() const;
	FBasicItemProperty* GetBaseItemProperty();

	const FGameItemCost GetItemCost() const;

	template<typename T = FBasicItemProperty>
	T* GetItemProperty()
	{
		if (auto property = GetBaseItemProperty())
		{
			return static_cast<T*>(property);
		}
		return nullptr;
	}

	template<typename T = FBasicItemProperty>
	const T* GetItemProperty() const
	{
		if (auto property = GetBaseItemProperty())
		{
			return static_cast<const T*>(property);
		}
		return nullptr;
	}

#pragma endregion 

protected:

	virtual void OnRep_Instance() override;

	UPROPERTY(VisibleInstanceOnly, Replicated)	TEnumAsByte<EPlayerItemState::Type> State;
	UPROPERTY(VisibleInstanceOnly, Replicated)	uint8 Level;
	UPROPERTY(VisibleInstanceOnly, Replicated)	int32 Amount;
};