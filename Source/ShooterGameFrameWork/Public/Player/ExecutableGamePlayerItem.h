#pragma once

//=======================================================
#include <TimerManager.h>

//=======================================================
#include "BasicGamePlayerItem.h"

//=======================================================
#include "ExecutableGamePlayerItem.generated.h"

//=======================================================
class UBasicPlayerItem;

//=======================================================

DECLARE_DELEGATE_OneParam(FOnPlayerItemUsed, const UBasicPlayerItem*);

//=======================================================

UCLASS(Abstract)
class SHOOTERGAMEFRAMEWORK_API UExecutableGamePlayerItem : public UBasicGamePlayerItem
{
	GENERATED_UCLASS_BODY()

protected:
	//----------------------------------------
	UPROPERTY(Transient, ReplicatedUsing = OnRep_Activated, VisibleInstanceOnly)
	bool bActivated;

	//----------------------------------------
	UPROPERTY(Transient)
	FTimerHandle	ProcessTimerHandle;
	FTimerDelegate	ProcessTimerDelegate;

	UPROPERTY(Transient)
	float ActivatedTime;

protected:
	virtual void BeginDestroy() override;
	virtual void PostInitProperties() override;
	virtual void TickComponent(float DeltaTime, ELevelTick TickType, FActorComponentTickFunction* ThisTickFunction) override;

	//----------------------------------------
	UFUNCTION()
	virtual void OnRep_Activated();

	//----------------------------------------
	UFUNCTION()		
	virtual void OnProcess() PURE_VIRTUAL(UExecutableGamePlayerItem::OnProcess, ;);

	//----------------------------------------
	UFUNCTION()
	virtual void StopTimer();

	//----------------------------------------
	UFUNCTION(BlueprintCallable, BlueprintPure)
	virtual bool IsAllowExecuteProcess() const PURE_VIRTUAL(UExecutableGamePlayerItem::IsAllowExecuteProcess, return false;);

	UFUNCTION(BlueprintCallable)
	virtual void SetProcessState(bool InIsStarted);

protected:
	UFUNCTION(BlueprintCallable, BlueprintPure)
	bool IsValidTimerHandle() const;

	UFUNCTION(BlueprintCallable)
	bool StartGameTimer(const float& InRate, const bool& InbLoop, const float& InFirstDelay);

	UFUNCTION(BlueprintCallable)
	bool StopGameTimer();
	

public:
	FOnPlayerItemUsed OnPlayerItemUsed;

	UFUNCTION(BlueprintCallable, BlueprintPure)
	virtual bool IsActivated() const
	{
		return bActivated;
	}

	UFUNCTION(BlueprintCallable, BlueprintPure)
	virtual bool IsProcessStarted() const
	{
		return IsActivated();
	}

	//----------------------------------------
	UFUNCTION(BlueprintCallable, BlueprintPure)
	virtual bool IsAllowStartProcess() const;

	//----------------------------------------
	UFUNCTION(BlueprintCallable)
	virtual bool StartProcess() PURE_VIRTUAL(UExecutableGamePlayerItem::StartProcess, return false;);

	//----------------------------------------
	UFUNCTION(BlueprintCallable)
	virtual bool StopProcess() { return true; }

	virtual void UseItem() override;

	UFUNCTION(BlueprintCallable, BlueprintPure, Category = "Executable|Game")
	virtual float GetExecuteInterval() const;

	UFUNCTION(BlueprintCallable, BlueprintPure, Category = "Executable|Game")
	virtual float GetExecuteTime() const;

	UFUNCTION(BlueprintCallable, BlueprintPure, Category = "Executable|Game")
	virtual float GetReadyPercent() const;

	UFUNCTION(BlueprintCallable, BlueprintPure, Category = "Executable|Game")
	virtual float GetExecutePercent() const;
};
