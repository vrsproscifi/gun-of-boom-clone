#pragma once
//=============================================
#include "AdsRewardedVideo.h"
#include "Engine/Network/LokaNetUserInterface.h"

//=============================================
#include <GameFramework/PlayerState.h>

//=============================================
#include "Extensions/UObjectExtensions.h"

//=============================================
#include "PlayerGameAchievement.h"

//=============================================
#include "BasicPlayerState.generated.h"

//=============================================
class ABasicPlayerCharacter;
class ABasicPlayerController;

//=============================================
DECLARE_MULTICAST_DELEGATE(FOnAuthorizationComplete);




//=============================================
UCLASS(BlueprintType, Blueprintable)
class SHOOTERGAMEFRAMEWORK_API ABasicPlayerState 
	: public APlayerState
	, public ILokaNetUserInterface
{
	GENERATED_UCLASS_BODY()

	//================================================
#pragma region IdentityToken

private:
	UPROPERTY(VisibleInstanceOnly)	FGuid PlayerAccountId;
	UPROPERTY(VisibleInstanceOnly)	FGuid IdentityToken;
	UPROPERTY(VisibleInstanceOnly)	bool bIsAllowUserInventory;
public:
	bool IsAllowUserInventory() const override { return bIsAllowUserInventory; }
	bool SetIdentityTokenString(const FString& InTokenString) override final;
	void SetIdentityToken(const FGuid& InToken) override final;

	UFUNCTION(BlueprintCallable, BlueprintPure, Category = Ads)
	virtual bool PlayRewardVideo(const FPlayRewardedVideoRequest& InVideoRequest) const;
	
	UFUNCTION(BlueprintCallable, BlueprintPure, Category = Ads)
		virtual void OnPlayRewardVideoSuccess(const FPlayRewardedVideoResponse& InResponse);

	UFUNCTION(BlueprintCallable, BlueprintPure, Category = Ads)
		virtual void OnPlayRewardVideoClosed(const FPlayRewardedVideoResponse& InResponse);


	void SetPlayerAccountId(const FGuid& InPlayerId)
	{
		PlayerAccountId = InPlayerId;
	}

	UFUNCTION(BlueprintCallable, BlueprintPure, Category = Ads)
	FGuid GetPlayerId() const 
	{
		return PlayerAccountId;
	}

	UFUNCTION(BlueprintCallable, BlueprintPure, Category = Ads)
	virtual int32 GetPlayerLevel() const;

	


	FGuid GetIdentityToken() const override final
	{
		return IdentityToken; 
	}
#pragma endregion

	//================================================
#pragma region GetBaseCharacter
	UFUNCTION(BlueprintCallable, BlueprintPure, Category = Character)
	ABasicPlayerCharacter* GetBaseCharacter() const;

	template<typename T = ABasicPlayerCharacter>
	T* GetBaseCharacter() const
	{
		return GetValidObjectAs<T>(GetBaseCharacter());
	}

	template<typename T = ABasicPlayerCharacter>
	T* GetBasePlayerCharacter() const
	{
		return GetValidObjectAs<T>(GetBaseCharacter());
	}
#pragma endregion

	//================================================
#pragma region GetBaseController
	UFUNCTION(BlueprintCallable, BlueprintPure, Category = Character)
		ABasicPlayerController* GetBaseController();

	template<typename T = ABasicPlayerController>
	T* GetBaseController()
	{
		return GetValidObjectAs<T>(GetOwner());
	}

	template<typename T = ABasicPlayerController>
	T* GetBaseController() const
	{
		return GetValidObjectAs<T>(GetOwner());
	}

#pragma endregion

	//================================================
#pragma region GetValidWorld
	UWorld* GetValidWorld()
	{
		return GetValidObject(GetWorld());
	}

	UWorld* GetValidWorld() const
	{
		return GetValidObject(GetWorld());
	}

	FTimerManager* GetTimerManager() const;
	FTimerManager* GetTimerManager();

#pragma endregion

public:
	UPROPERTY(VisibleInstanceOnly)	FGuid MemberId;

public:
	virtual void PostInitializeComponents() override;
	virtual void CopyProperties(APlayerState* PlayerState) override;
	virtual void ClientInitialize(class AController* C) override;
	virtual void OnReactivated() override;


	virtual void BeginPlay() override;
	virtual void EndPlay(const EEndPlayReason::Type EndPlayReason) override;

	//================================================
	UFUNCTION(BlueprintCallable, BlueprintPure, Category = Runtime)
	bool HasLocalClientState() const;

	UFUNCTION()	virtual void OnFullyLoadedData();

	UFUNCTION()	virtual void OnCreateUserInterface();
	UFUNCTION()	virtual void OnInitializeCharacter();

public:
	FOnAuthorizationComplete OnAuthorizationCompleteDelegate;

	//==============================================================
#pragma region Achievements
private:
	UPROPERTY(Replicated)
	TArray<FPlayerGameAchievement> Achievements;

public:

	UFUNCTION(BlueprintCallable)
	bool ContainsAchievement(const int32& InAchievementId) const;

	FPlayerGameAchievement* GetAchievementById(const int32& InAchievementId);
	const FPlayerGameAchievement* GetAchievementById(const int32& InAchievementId) const;

	UFUNCTION(BlueprintCallable)
	bool GetAchievementById(const int32& InAchievementId, FPlayerGameAchievement& OutAchievement);
	
	UFUNCTION(BlueprintCallable)
	int32 GetAchievementValueById(const int32& InAchievementId, const int32& InDefault = 0) const;

	UFUNCTION(BlueprintCallable)
	virtual int32 GivePlayerAchievement(const int32& InAchievementId, const int32& InAmount = 1, const int32& InScore = 0);
	
	UFUNCTION(Reliable, Client)
	virtual void NotifyPlayerAchievement(const int32& InAchievementId, const int32& InTotalAmount, const int32& InAmount = 1, const int32& InScore = 0);

	UFUNCTION(BlueprintCallable)
	virtual const TArray<FPlayerGameAchievement>& GetAchievements() const
	{
		return Achievements;
	}

	UFUNCTION(BlueprintCallable)
	/** get number of kills */
	virtual int32 GetKills() const
	{
		return -1;
	}

	UFUNCTION(BlueprintCallable)
	/** get number of deaths */
	virtual int32 GetDeaths() const
	{
		return -1;
	}

	UFUNCTION(BlueprintCallable)
	/** get number of points */
	virtual int32 GetScore() const
	{
		return -1;
	}

#pragma endregion

	virtual void Reset() override;

};