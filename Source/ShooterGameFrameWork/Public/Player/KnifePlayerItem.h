// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "Player/BasicPlayerItem.h"
#include "KnifePlayerItem.generated.h"

class UKnifeItemEntity;
/**
 * 
 */
UCLASS()
class SHOOTERGAMEFRAMEWORK_API UKnifePlayerItem : public UBasicPlayerItem
{
	GENERATED_BODY()

public:

	UFUNCTION(BlueprintCallable, BlueprintPure, Category = Inventory)
	UKnifeItemEntity* GetKnifeEntity() const;
	
};
