// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "Player/BasicPlayerItem.h"
#include "GrenadePlayerItem.generated.h"


class UGrenadeItemEntity;


UCLASS()
class SHOOTERGAMEFRAMEWORK_API UGrenadePlayerItem : public UBasicPlayerItem
{
	GENERATED_BODY()
	
public:

	UFUNCTION(BlueprintCallable, BlueprintPure, Category = Inventory)
	FORCENOINLINE UGrenadeItemEntity* GetGrenadeEntity() const;
	
};
