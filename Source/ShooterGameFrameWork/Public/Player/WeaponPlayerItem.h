// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "Player/BasicPlayerItem.h"
#include "WeaponPlayerItem.generated.h"


class UWeaponItemEntity;
/**
 * 
 */
UCLASS()
class SHOOTERGAMEFRAMEWORK_API UWeaponPlayerItem : public UBasicPlayerItem
{
	GENERATED_BODY()
	
public:

	UFUNCTION(BlueprintCallable, BlueprintPure, Category = Inventory)
	FORCENOINLINE UWeaponItemEntity* GetWeaponEntity() const;

	virtual void ReplaceEntity(UBasicGameEntity* entity) override;
};
