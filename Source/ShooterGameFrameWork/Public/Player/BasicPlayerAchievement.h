#pragma once
//=======================================================
#include "BasicPlayerEntity.h"

//=======================================================
#include "UObjectExtensions.h"

//=======================================================
#include "BasicPlayerAchievement.generated.h"

//=======================================================
class UBasicGameAchievement;

UCLASS(ClassGroup = (Utility, Common), BlueprintType, hideCategories = (Trigger, PhysicsVolume), meta = (BlueprintSpawnableComponent))
class SHOOTERGAMEFRAMEWORK_API UBasicPlayerAchievement : public UBasicPlayerEntity
{
	GENERATED_UCLASS_BODY()
protected:
	virtual void OnRep_Instance() override;

private:
	UPROPERTY(Replicated) int32 Amount;

public:
	UFUNCTION(BlueprintCallable)
	const int32& GetAmount() const
	{
		return Amount;
	}
};