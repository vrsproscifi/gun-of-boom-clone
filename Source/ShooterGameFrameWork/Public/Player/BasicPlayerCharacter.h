#pragma once
//=============================================
#include "Extensions/UObjectExtensions.h"

//=============================================
#include <GameFramework/Character.h>
#include <Engine/World.h>


//=============================================
#include "Engine/Animation/TwoAnimSequence.h"

//=============================================
#include "BasicPlayerCharacter.generated.h"

class UBasicPlayerItem;

//=============================================
class ABasicPlayerController;
class ABasicPlayerState;
class ABasicGameState;
class AGameState;

//=============================================
struct FTwoAnimMontage;

//=============================================
UCLASS(BlueprintType, Blueprintable)
class SHOOTERGAMEFRAMEWORK_API ABasicPlayerCharacter : public ACharacter
{
	GENERATED_UCLASS_BODY()

public:
	UFUNCTION(BlueprintCallable, Category = "Game|Animation")
	virtual float PlayTwoAnimMontage(FTwoAnimMontage& Animation);

	/** get camera view type */
	UFUNCTION(BlueprintCallable, Category = "Game|Character")
	virtual bool IsFirstPerson() const;

	/** check if pawn is still alive */
	UFUNCTION(BlueprintCallable, Category = "Game|Character")
	virtual bool IsAlive() const;

	template<typename T = ABasicPlayerController>
	T* GetBaseController()
	{
		return GetValidObjectAs<T>(GetController());
	}

	template<typename T = ABasicPlayerController>
	T* GetBaseController() const
	{
		return GetValidObjectAs<T>(GetController());
	}


	template<typename T = ABasicPlayerState>
	T* GetBasePlayerState()
	{
		return GetValidObjectAs<T>(GetPlayerState());
	}

	template<typename T = ABasicPlayerState>
	T* GetBasePlayerState() const
	{
		return GetValidObjectAs<T>(GetPlayerState());
	}

	//================================================
#pragma region GetValidWorld
	UWorld* GetValidWorld()
	{
		return GetValidObject(GetWorld());
	}

	UFUNCTION(BlueprintCallable, Category = "Game|World")
	UWorld* GetValidWorld() const
	{
		return GetValidObject(GetWorld());
	}

	FTimerManager* GetTimerManager() const;
	FTimerManager* GetTimerManager();

#pragma endregion

	virtual void PostInitializeComponents() override;
	AGameState* GetBaseGameState();

	UFUNCTION(BlueprintCallable, Category = "Game|World")
	AGameState* GetBaseGameState() const;


	template<typename T = ABasicGameState>
	T* GetBasicGameState()
	{
		return GetValidObjectAs<T>(GetBaseGameState());
	}

	template<typename T = ABasicGameState>
	T* GetBasicGameState() const
	{
		return GetValidObjectAs<T>(GetBaseGameState());
	}

	virtual void SetupPlayerInputComponent(UInputComponent* PlayerInputComponent) override;
	virtual void EndPlay(const EEndPlayReason::Type EndPlayReason) override;

	virtual void MoveForward(float InValue) {}
	virtual void MoveRight(float InValue) {}
	virtual void MoveUp(float InValue) {}

	virtual void TurnAtRate(float InValue) {}
	virtual void TurnAtValue(float InValue) {}

	virtual void LookUpAtRate(float InValue) {}
	virtual void LookUpAtValue(float InValue) {}

	virtual void InitializeInstance(UBasicPlayerItem* InInstance);
	virtual void InitializeItem(UBasicPlayerItem* InInstance, bool IsDefault = false);

	UFUNCTION(BlueprintCallable)
	virtual int32 GivePlayerAchievement(const int32& InAchievementId, const int32& InAmount = 1, const int32& InScore = 0);

protected:

	UFUNCTION()
	virtual void OnRep_Instance();

	UPROPERTY(ReplicatedUsing = OnRep_Instance)
	UBasicPlayerItem* Instance;
};