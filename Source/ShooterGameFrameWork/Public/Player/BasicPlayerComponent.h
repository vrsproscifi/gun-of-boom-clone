#pragma once
//=============================================
#include "AdsRewardedVideo.h"
#include "Game/BasicGameComponent.h"

//=============================================
#include "Extensions/GameViewportExtensions.h"

//=============================================
#include "BasicPlayerComponent.generated.h"

//=============================================
class UOperationRequest;

//=============================================
class ILokaNetUserInterface;

//=============================================
UCLASS(Transient)
class SHOOTERGAMEFRAMEWORK_API UBasicPlayerComponent : public UBasicGameComponent
{
	GENERATED_BODY()

public:
	UBasicPlayerComponent();

	UFUNCTION(BlueprintCallable, BlueprintPure, Category = Ads)
	virtual bool PlayRewardVideo(const FPlayRewardedVideoRequest& InVideoRequest) const;

	UFUNCTION(BlueprintCallable, BlueprintPure, Category = Ads)
	virtual void OnPlayRewardVideoSuccess(const FPlayRewardedVideoResponse& InResponse);

	UFUNCTION(BlueprintCallable, BlueprintPure, Category = Ads)
	virtual void OnPlayRewardVideoCanceled(const FPlayRewardedVideoResponse& InResponse);

	virtual bool IsAllowServerQueries() const;

	virtual TAttribute<FGuid> GetIdentityToken() const override;

	UFUNCTION(BlueprintCallable, BlueprintPure, Category = Ads)
	virtual FGuid GetPlayerId() const;

	UFUNCTION(BlueprintCallable, BlueprintPure, Category = Ads)
	virtual int32 GetPlayerLevel() const;

	UFUNCTION(BlueprintCallable, BlueprintPure, Category = Runtime)
	bool HasLocalClientComponent() const;

	UFUNCTION(BlueprintCallable, BlueprintPure, Category = Identity)
	TScriptInterface<ILokaNetUserInterface> GetUserOwner() const;

	virtual void BeginPlay() override;
	virtual void EndPlay(const EEndPlayReason::Type EndPlayReason) override;

private:
	FORCENOINLINE void OnAuthorizationComplete_Proxy()
	{
		if (HasAuthority())
		{
			OnAuthorizationComplete();
		}
	}

protected:
	virtual void OnAuthorizationComplete();
	virtual void CreateGameWidgets() {}

	//========================================================
#pragma region GetBasicPlayerState
public:

	template<typename T = ABasicPlayerState>
	T* GetBasicPlayerState()
	{
		return GetValidObjectAs<T>(GetOwner());
	}

	template<typename T = ABasicPlayerState>
	T* GetBasicPlayerState() const
	{
		return GetValidObjectAs<T>(GetOwner());
	}
#pragma endregion

	//========================================================
#pragma region GetBaseController
public:
	ABasicPlayerController * GetBaseController() const;

	UFUNCTION(BlueprintCallable)
	ABasicPlayerController * GetBaseController();

	template<typename T = ABasicPlayerController>
	T* GetBaseController()
	{
		return GetValidObjectAs<T>(GetBaseController());
	}

	template<typename T = ABasicPlayerController>
	T* GetBaseController() const
	{
		return GetValidObjectAs<T>(GetBaseController());
	}
#pragma endregion
};