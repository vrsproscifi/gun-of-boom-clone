#pragma once
//=======================================================
#include "BasicPlayerItem.h"

//=======================================================
#include "BasicGamePlayerItem.generated.h"

//=======================================================
class ABasicPlayerCharacter;
class ABasicPlayerState;

class UBasicPlayerItem;

UCLASS(Abstract)
class SHOOTERGAMEFRAMEWORK_API UBasicGamePlayerItem : public UBasicPlayerItem
{
	GENERATED_UCLASS_BODY()

private:
	UPROPERTY(ReplicatedUsing = OnRep_Instance, VisibleInstanceOnly)
	UBasicPlayerItem* Instance;

public:
	//--------------------------------------------------------------------
	UFUNCTION(BlueprintCallable, Category = Inventory)
	virtual void InitializeInstance(UBasicPlayerItem* InInstance);

#pragma region GetBaseInstance
public:
	UFUNCTION(BlueprintCallable, BlueprintPure, Category = GamePlay)
	UBasicPlayerItem * GetBaseInstance() const;
	UBasicPlayerItem* GetBaseInstance();

	template<typename T = UBasicPlayerItem>
	T* GetInstance()
	{
		return GetValidObjectAs<T>(GetBaseInstance());
	}

	template<typename T = UBasicPlayerItem>
	T* GetInstance() const
	{
		return GetValidObjectAs<T>(GetBaseInstance());
	}

#pragma endregion 

	//--------------------------------------------------------------------
#pragma region GetBasicCharacter
public:
	UFUNCTION(BlueprintCallable, BlueprintPure, Category = GamePlay)
		FString GetPlayerName() const;

	UFUNCTION(BlueprintCallable, BlueprintPure, Category = GamePlay)
	ABasicPlayerCharacter* GetBasicCharacter() const;
	ABasicPlayerCharacter* GetBasicCharacter();

	template<typename T = ABasicPlayerCharacter>
	T* GetCharacter() const
	{
		return GetValidObjectAs<T>(GetBasicCharacter());
	}

	template<typename T = ABasicPlayerCharacter>
	T* GetCharacter()
	{
		return GetValidObjectAs<T>(GetBasicCharacter());
	}
#pragma endregion 

	//--------------------------------------------------------------------
#pragma region GetBasicPlayerState
public:
	UFUNCTION(BlueprintCallable, BlueprintPure, Category = GamePlay)
	ABasicPlayerState* GetBasicPlayerState() const;
	ABasicPlayerState* GetBasicPlayerState();

	template<typename T = ABasicPlayerState>
	T* GetPlayerState() const
	{
		return GetValidObjectAs<T>(GetBasicPlayerState());
	}

	template<typename T = ABasicPlayerState>
	T* GetPlayerState()
	{
		return GetValidObjectAs<T>(GetBasicPlayerState());
	}
#pragma endregion 

	//--------------------------------------------------------------------
#pragma region GetValidWorld
	UWorld* GetValidWorld();

	UFUNCTION(BlueprintCallable, BlueprintPure, Category = GamePlay)
		UWorld* GetValidWorld() const;

	FTimerManager* GetTimerManager() const;
	FTimerManager* GetTimerManager();
#pragma endregion
};