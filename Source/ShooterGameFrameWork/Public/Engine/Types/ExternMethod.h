#pragma once

#ifndef __ExternMethod_H__
#define __ExternMethod_H__

//====================================
#ifdef EXTERN
#undef EXTERN
#endif

//====================================
#ifdef _MSC_VER  
#define EXTERN extern
#else
#define EXTERN 
#endif

#endif