#pragma once

//==========================================
#include "Extensions/SoftObjectExtensions.h"
#include "Animation/AnimSequence.h"
#include "Animation/AnimMontage.h"

//==========================================
#include "TwoAnimSequence.generated.h"


//==========================================
USTRUCT(BlueprintType, Blueprintable)
struct SHOOTERGAMEFRAMEWORK_API FTwoAnimSequence
{
	GENERATED_USTRUCT_BODY()

	/** animation played on pawn (1st person view) */
	UPROPERTY(BlueprintReadOnly, EditDefaultsOnly, Category = Animation)
		TSoftObjectPtr<UAnimSequence> Pawn1P;

	/** animation played on pawn (3rd person view) */
	UPROPERTY(BlueprintReadOnly, EditDefaultsOnly, Category = Animation)
		TSoftObjectPtr<UAnimSequence> Pawn3P;


	DEFINE_SOFT_OBJECT_GETTER(UAnimSequence, Pawn1P);
	DEFINE_SOFT_OBJECT_GETTER(UAnimSequence, Pawn3P);
};

//==========================================
USTRUCT(BlueprintType, Blueprintable)
struct SHOOTERGAMEFRAMEWORK_API FTwoAnimMontage //FWeaponAnim
{
	GENERATED_USTRUCT_BODY()

	/** animation played on pawn (1st person view) */
	UPROPERTY(BlueprintReadOnly, EditDefaultsOnly, Category = Animation)
		TSoftObjectPtr<UAnimMontage> Pawn1P;

	/** animation played on pawn (3rd person view) */
	UPROPERTY(BlueprintReadOnly, EditDefaultsOnly, Category = Animation)
		TSoftObjectPtr<UAnimMontage> Pawn3P;

	DEFINE_SOFT_OBJECT_GETTER(UAnimMontage, Pawn1P);
	DEFINE_SOFT_OBJECT_GETTER(UAnimMontage, Pawn3P);
};