#pragma once

#include "OperationRequestServerHost.h"
#include "OperationRequestTimerMethod.h"
#include "FailedOperationResponse.h"

#include "Handler/ArrayObjectRequestHandler.h"
#include "Handler/CallBackRequestHandler.h"
#include "Handler/ObjectRequestHandler.h"
#include "Handler/SimpleRequestHandler.h"
#include "Handler/EnumRequestHandler.h"

#include "ExternMethod.h"
#include <TimerManager.h>
#include "Misc/Attribute.h"

//====================================================
#include "RequestManager.generated.h"


typedef const TAttribute<FGuid>& TokenAttribRef;
typedef TAttribute<FGuid> TokenAttrib;


class IHttpRequest;

UCLASS(Blueprintable, BlueprintType)
class SHOOTERGAMEFRAMEWORK_API UOperationRequest : public UObject
{
	GENERATED_UCLASS_BODY()

private:
	//======================================
	//				Property
	TMap<int32, TSharedPtr<FBaseRequestHandler>> Handlers;
	mutable TWeakPtr<IHttpRequest>		Request;
	TokenAttrib					Token;

	//======================================
	UPROPERTY(Transient, VisibleInstanceOnly)	FOperationRequestServerHost			ServerHost;
	UPROPERTY(Transient, VisibleInstanceOnly)	FString								OperationName;

	//======================================
	UPROPERTY(Transient, VisibleInstanceOnly)	mutable FDateTime					LastExecuteDate;
	UPROPERTY(Transient, VisibleInstanceOnly)	mutable FRequestExecuteError		LastExecuteError;
	UPROPERTY(Transient, VisibleInstanceOnly)	mutable FRequestExecuteError		PrewExecuteError;
	//======================================
	//				Timer

	mutable FTimerDelegate				TimerDelegate;
	mutable FTimerDelegate				TimerProxy;

	UPROPERTY(Transient, VisibleInstanceOnly)	mutable FDateTime				TimerStartDate;
	UPROPERTY(Transient, VisibleInstanceOnly)	mutable FDateTime				TimerLastExecuteDate;
	UPROPERTY(Transient, VisibleInstanceOnly)	mutable FTimerHandle			TimerHandle;
	UPROPERTY(Transient, VisibleInstanceOnly)	mutable int32					TimerItter;
	UPROPERTY(Transient, VisibleInstanceOnly)	float							TimerRate;
	UPROPERTY(Transient, VisibleInstanceOnly)	bool							TimerValidate;
	UPROPERTY(Transient, VisibleInstanceOnly)	int32							TimerLimit;
	UPROPERTY(Transient, VisibleInstanceOnly)	EOperationRequestTimerMethod	TimerLoop;

public:
	UFUNCTION()
		void SetServerHost(const FOperationRequestServerHost& InHost);

	UFUNCTION(BlueprintCallable)
		void SetOperationName(const FString& InName);

	UFUNCTION(BlueprintCallable)
		void SetTimerRequestMethod(const EOperationRequestTimerMethod& InMethod);

	UFUNCTION(BlueprintCallable)
		void SetTimerAttempLimit(const int32& InAttempLimits);

	UFUNCTION(BlueprintCallable)
		void SetTimerRequestRate(const float& InRate);

	void SetOperationToken(const TokenAttrib& InTokenAttrib);

	void SetTimerRequestDelegate(const FTimerDelegate& InDelegate);

	UFUNCTION(BlueprintCallable)
	void SetTimerValidationState(const bool& InValidate);

	UFUNCTION(BlueprintCallable)
		void EnableTimerValidation();

	UFUNCTION(BlueprintCallable)
		void DisableTimerValidation();

	virtual void BeginDestroy() override;

public:
	UFUNCTION(BlueprintCallable, BlueprintPure)
	FString GetItterationString() const;

	UFUNCTION(BlueprintCallable, BlueprintPure)
	int32 GetItterationLimit() const;

	UFUNCTION(BlueprintCallable, BlueprintPure)
	int32 GetItterationNum() const;

	UFUNCTION(BlueprintCallable, BlueprintPure)
	bool ItterationInc() const;

	UFUNCTION(BlueprintCallable)
	void ResetItterations() const;

	UFUNCTION(BlueprintCallable, BlueprintPure)
	bool IsValidTimer() const;

	UFUNCTION(BlueprintCallable, BlueprintPure)
	bool IsTimerActive() const;

	FTimerDelegate& GetTimerDelegate()
	{
		return TimerDelegate;
	}

	FTimerDelegate& GetTimerDelegate() const
	{
		return TimerDelegate;
	}

	FTimerManager* GetTimerManager() const;

	UFUNCTION(BlueprintCallable)
	void StartTimer(const float& InStartDelay = 0) const;

	UFUNCTION(BlueprintCallable)
	void StopTimer() const;

	void OnTimerItterationProxy() const;

	//======================================
	//				Contructor
public:
	//	При возникнновении ошибки во время запроса, например ошибка сервера или десериализации
	FOnFailedOperationResponse OnFailedResponse;

	//	При окончании попыток выполнить запрос
	FOnFailedOperationResponse OnLoseAttemps;
public:

	UFUNCTION(BlueprintCallable)
	void OnPreDestruct();

	//======================================
	//				Methods
private:	
	//	Возвращает не пустой токен, если это возможно
	FGuid ChoseToken(TokenAttribRef token) const;

	//------------------------------------------
	//	Отправляет сериализованный запрос
	void SendRequest(const FString& content, const FString& verb, TokenAttribRef token) const;
	static void OnProxyRequestComplete(const UOperationRequest* Context, TSharedPtr<class IHttpRequest> HttpRequest, TSharedPtr<class IHttpResponse, ESPMode::ThreadSafe> HttpResponse, bool bSucceeded);

public:
	//------------------------------------------
	//	Отправляет запрос серверу без параметров
	void GetRequest(TokenAttribRef token = TokenAttrib()) const;

	//------------------------------------------
	//	Передает серверу объект
	template<typename TRequestObject>
	FORCEINLINE void SendRequestObject(const TRequestObject& object, TokenAttribRef token = TokenAttrib()) const;

	//template<typename TRequestObject>
	//void GetRequestParam(const TRequestObject& object, TokenAttribRef token = "") const
	//{
	//	FString objectStr;
	//	if (FJsonObjectConverter::UStructToJsonObjectString(objectStr, &object, 0, 0))
	//	{
	//		SendRequest(objectStr, token);
	//	}
	//}


	//======================================
	//				Binding
	
	/*! Пример использования: Request->Bind(200).AddUObject(this, &Class::Method);*/
	FCallBackRequestHandler::FOnDeSerialize& Bind(const int32& code)
	{
		if (Handlers.Contains(code) == false)
		{
			auto handler = new FCallBackRequestHandler(this);
			Handlers.Add(code, TSharedPtr<FCallBackRequestHandler>(handler));
		}
		return StaticCastSharedPtr<FCallBackRequestHandler>(Handlers[code])->Handler;
	}

	/*! Пример использования: Request->BindObject<FSquadInviteInformation>(200).AddUObject(this, &Class::Method);*/
	template<typename TResponseObject>
	typename FObjectRequestHandler<TResponseObject>::FOnDeSerialize& BindObject(const int32& code)
	{
		if(Handlers.Contains(code) == false)
		{
			auto handler = new FObjectRequestHandler<TResponseObject>(this);
			Handlers.Add(code, TSharedPtr<FObjectRequestHandler<TResponseObject>>(handler));
		}
		return StaticCastSharedPtr<FObjectRequestHandler<TResponseObject>>(Handlers[code])->Handler;
	}

	/*! Пример использования: Request->BindObject<EIsoCountry>(200).AddUObject(this, &Class::Method);*/
	template<typename TResponseEnumType>
	typename FEnumRequestHandler<TResponseEnumType>::FOnDeSerialize& BindEnum(const int32& code)
	{
		if (Handlers.Contains(code) == false)
		{
			auto handler = new FEnumRequestHandler<TResponseEnumType>(this);
			Handlers.Add(code, TSharedPtr<FEnumRequestHandler<TResponseEnumType>>(handler));
		}
		return StaticCastSharedPtr<FEnumRequestHandler<TResponseEnumType>>(Handlers[code])->Handler;
	}


	/*! Пример использования: Request->BindArray<TArray<FSquadInviteInformation>>(200).AddUObject(this, &Class::Method);*/
	template<typename TResponseObject>
	typename FArrayObjectRequestHandler<TResponseObject>::FOnDeSerialize& BindArray(const int32& code)
	{
		if (Handlers.Contains(code) == false)
		{
			auto handler = new FArrayObjectRequestHandler<TResponseObject>(this);
			Handlers.Add(code, TSharedPtr<FArrayObjectRequestHandler<TResponseObject>>(handler));
		}
		return StaticCastSharedPtr<FArrayObjectRequestHandler<TResponseObject>>(Handlers[code])->Handler;
	}
};

template<typename TRequestObject>
FORCEINLINE void UOperationRequest::SendRequestObject(const TRequestObject& object, TokenAttribRef token) const
{
	FString jsonStr;
	jsonStr.Reserve(512);
#ifdef _MSC_VER  
	static_assert(std::is_class<TRequestObject>::value, "Is not child of UStruct");
#endif
	FJsonObjectConverter::UStructToJsonObjectString(TRequestObject::StaticStruct(), &object, jsonStr, 0, 0);

	SendRequest(jsonStr, FVerb::Post, token);
}

template<>
FORCEINLINE void UOperationRequest::SendRequestObject<FGuid>(const FGuid& object, TokenAttribRef token) const
{
	SendRequestObject<FTextDataContiner>(object, token);
}

template<>
FORCEINLINE void UOperationRequest::SendRequestObject<FLokaGuid>(const FLokaGuid& object, TokenAttribRef token) const
{
	SendRequestObject<FTextDataContiner>(object, token);
}

template<>
FORCEINLINE void UOperationRequest::SendRequestObject<FString>(const FString& object, TokenAttribRef token) const
{
	SendRequestObject<FTextDataContiner>(object, token);
}

template<>
FORCEINLINE void UOperationRequest::SendRequestObject<FText>(const FText& object, TokenAttribRef token) const
{
	SendRequestObject<FTextDataContiner>(object, token);
}

template<>
FORCEINLINE void UOperationRequest::SendRequestObject<FName>(const FName& object, TokenAttribRef token) const
{
	SendRequestObject<FTextDataContiner>(object, token);
}

template<>
FORCEINLINE void UOperationRequest::SendRequestObject<int32>(const int32& object, TokenAttribRef token) const
{
	SendRequestObject<FINT32DataContiner>(object, token);
}

template<>
FORCEINLINE void UOperationRequest::SendRequestObject<int64>(const int64& object, TokenAttribRef token) const
{
	SendRequestObject<FINT64DataContiner>(object, token);
}

template<>
FORCEINLINE void UOperationRequest::SendRequestObject<uint32>(const uint32& object, TokenAttribRef token) const
{
	SendRequestObject<FINT32DataContiner>(object, token);
}

template<>
FORCEINLINE void UOperationRequest::SendRequestObject<uint64>(const uint64& object, TokenAttribRef token) const
{
	SendRequestObject<FUINT64DataContiner>(object, token);
}

template<>
FORCEINLINE void UOperationRequest::SendRequestObject<float>(const float& object, TokenAttribRef token) const
{
	SendRequestObject<FFloatDataContiner>(object, token);
}
