#pragma once
#include "FailedOperationResponse.generated.h"


UENUM()
enum class ERequestErrorCode : uint8
{
	None,

	BadResponse,
	HandlerNotFound,
	SerializationFailed,
	ConnectionFailed,
	Unauthorized,
	ServerError,
	Timeout,
	BadGateway,
};

//	Информация об ошибке при выполнение запроса
USTRUCT()
struct SHOOTERGAMEFRAMEWORK_API FRequestExecuteError
{
	GENERATED_USTRUCT_BODY()

	//	Дата обработки ошибки
	UPROPERTY() FDateTime HandledDate;

	//	Внутренний код ошибки
	UPROPERTY() ERequestErrorCode Error;

	//	Название операции, в которой произошла ошибка
	UPROPERTY() FString Operation;

	//	Контент, полученный от сервер
	UPROPERTY() FString Content;

	//	Ответ, полученный от сервер
	UPROPERTY() uint32 Status;

	FString GetStatusCode() const
	{
		return FString::FormatAsNumber(Status);
	}

	bool IsTimeout() const
	{
		if (Status == 0) return true;
		if (Status == 408) return true;
		if (Status == 417) return true;
		if (Status == 504) return true;
		if (Status == 522) return true;
		if (Status == 524) return true;
		return false;
	}

	bool IsBadGateway() const
	{
		if (Status == 502) return true;
		if (Status == 503) return true;
		if (Status == 521) return true;
		if (Status == 523) return true;
		return false;
	}

	FRequestExecuteError()
		: Error(ERequestErrorCode::None)
		, Status(0)
	{
		HandledDate = FDateTime::Now();
	}

	FRequestExecuteError(const FString& operation)
		: Error(ERequestErrorCode::None)
		, Operation(operation)
		, Status(0)
	{
		HandledDate = FDateTime::Now();
	}

	FText ToShortText() const
	{
		return FText::FromString(ToShortString());
	}

	FString ToShortString() const
	{
		return FString::Printf(TEXT("Status: %d / %d"), static_cast<int32>(Status), static_cast<int32>(Error));
	}

	FText ToText() const
	{
		return FText::FromString(ToString());
	}


	FString ToString() const
	{
		TMap<FString, FStringFormatArg> args;
		args.Add("Operation", FStringFormatArg(Operation));
		args.Add("Status", FStringFormatArg(Status));
		args.Add("Error", FStringFormatArg(static_cast<int32>(Error)));

		args.Add("Content", FStringFormatArg(Content));
		args.Add("HandledDate", FStringFormatArg(HandledDate.ToString()));

		args.Add("Header", FStringFormatArg("================= RequestExecuteError ========== \r\n"));

		args.Add("ContentBegin", FStringFormatArg("\n================= ContentBegin ========== \r\n\r\n"));

		args.Add("ContentEnd", FStringFormatArg("\n\n================= ContentEnd ========== \r\n"));

		return FString::Format(TEXT("{Header} Operation: {Operation} -- Status: {Status} / {Error}\r\n HandledDate: {HandledDate}\r\n {ContentBegin} {Content} {ContentEnd} \r\n"), args);
	}

};

DECLARE_DELEGATE_OneParam(FOnFailedOperationResponse, const FRequestExecuteError&);
