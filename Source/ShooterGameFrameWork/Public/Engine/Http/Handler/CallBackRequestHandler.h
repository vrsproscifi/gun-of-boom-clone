#pragma once

#ifndef __CallBackRequestHandler_H__
#define __CallBackRequestHandler_H__

#include "BaseRequestHandler.h"


EXTERN class SHOOTERGAMEFRAMEWORK_API FCallBackRequestHandler : public FBaseRequestHandler
{
public:
	DECLARE_MULTICAST_DELEGATE(FOnDeSerialize);

	FOnDeSerialize Handler;

	FCallBackRequestHandler(const TWeakObjectPtr<UOperationRequest>& operation)
		: FBaseRequestHandler(operation)
	{

	}

	virtual bool OnDeSerialize(const FString& /*objectStr*/) const override
	{
		if (Handler.IsBound())
		{
			Handler.Broadcast();
		}

		return true;
	}
};

#endif