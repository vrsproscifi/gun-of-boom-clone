#pragma once

#ifndef __ObjectRequestHandler_H__
#define __ObjectRequestHandler_H__

#include "BaseRequestHandler.h"
#include "RequestDataContainer.h"
#include "JsonObjectConverter.h"

template<class TResponseObject>
class FObjectRequestHandler : public FBaseRequestHandler
{
public:
	DECLARE_MULTICAST_DELEGATE_OneParam(FOnDeSerialize, const TResponseObject&)

	FObjectRequestHandler(const TWeakObjectPtr<UOperationRequest>& operation)
		: FBaseRequestHandler(operation)
	{

	}

	FOnDeSerialize Handler;
	virtual bool OnDeSerialize(const FString& objectStr) const override
	{
		TResponseObject object;

		//===============================
#ifdef _MSC_VER  
		static_assert(std::is_class<TResponseObject>::value, "Is not child of UStruct");
#endif
		if (FJsonObjectConverter::JsonObjectStringToUStruct(objectStr, &object, 0, 0))
		{
			//===============================
			if (Handler.IsBound())
			{
				Handler.Broadcast(object);
			}
			return true;
		}
		return false;
	}
};

#endif