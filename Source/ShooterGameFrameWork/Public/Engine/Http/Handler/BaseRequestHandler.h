#pragma once

#ifndef __BaseRequestHandler_H__
#define __BaseRequestHandler_H__


#include "ExternMethod.h"

class UOperationRequest;


EXTERN class SHOOTERGAMEFRAMEWORK_API FBaseRequestHandler
{
public:
	mutable FDateTime	LastHandleDate;
	mutable FString		LastHandleContent;
	const				TWeakObjectPtr<UOperationRequest> Operation;

	FBaseRequestHandler(const TWeakObjectPtr<UOperationRequest>& operation);
	virtual ~FBaseRequestHandler() {}

	virtual bool OnDeSerializeProxy(const FString& objectStr) const;
	virtual bool OnDeSerialize(const FString& objectStr) const = 0;
};


#endif