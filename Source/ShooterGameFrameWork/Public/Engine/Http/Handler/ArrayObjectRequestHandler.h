#pragma once

#ifndef __ArrayObjectRequestHandler_H__
#define __ArrayObjectRequestHandler_H__

#include "BaseRequestHandler.h"
#include "JsonObjectConverter.h"

template<class TResponseObject>
class FArrayObjectRequestHandler : public FBaseRequestHandler
{
public:
	DECLARE_MULTICAST_DELEGATE_OneParam(FOnDeSerialize, const TResponseObject&)

	FOnDeSerialize Handler;

	FArrayObjectRequestHandler(const TWeakObjectPtr<UOperationRequest>& operation)
		: FBaseRequestHandler(operation)
	{

	}

	virtual bool OnDeSerialize(const FString& objectStr) const override
	{
		TResponseObject object;

		//===============================
#ifdef _MSC_VER  
		static_assert(std::is_class<TResponseObject>::value, "Is not child of UStruct");
#endif
		if (FJsonObjectConverter::JsonArrayStringToUStruct(objectStr, &object, 0, 0))
		{
			//===============================
			if (Handler.IsBound())
			{
				Handler.Broadcast(object);
			}
			return true;
		}
		return false;
	}
};

#endif