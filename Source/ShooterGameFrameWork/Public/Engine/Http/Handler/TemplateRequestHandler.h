#pragma once

#ifndef _TemplateRequestHandler_H__
#define _TemplateRequestHandler_H__

#include "BaseRequestHandler.h"
#include "JsonObjectConverter.h"
#include "RequestDataContainer.h"

#define DECLARE_REQUEST_HANDLER(CONTAINER, TYPE) \
template<> \
class FObjectRequestHandler<TYPE> : public FBaseRequestHandler \
{\
public:\
	DECLARE_MULTICAST_DELEGATE_OneParam(FOnDeSerialize, const TYPE&)\
	FOnDeSerialize Handler;\
	FObjectRequestHandler(const TWeakObjectPtr<UOperationRequest>& operation):FBaseRequestHandler(operation){}\
	virtual bool OnDeSerialize(const FString& objectStr) const override\
	{\
		CONTAINER object;\
		if (FJsonObjectConverter::JsonObjectStringToUStruct(objectStr, &object, 0, 0))\
		{\
			if (Handler.IsBound())\
			{\
				TYPE value;\
				if (CONTAINER::Parse(object.Value, value))\
				{\
					Handler.Broadcast(value);\
				}\
				else\
				{\
					return false;\
				}\
			}\
			return true;\
		}\
		return false;\
	}\
};

#endif