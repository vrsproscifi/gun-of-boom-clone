#include "BaseRequestHandler.h"
#include "UObjectExtensions.h"
#include "RequestManager.h"

FBaseRequestHandler::FBaseRequestHandler(const TWeakObjectPtr<UOperationRequest>& operation)
	: Operation(operation)
{

}

bool FBaseRequestHandler::OnDeSerializeProxy(const FString& objectStr) const
{
	if (IsValidObject(Operation))
	{
		LastHandleContent = objectStr;
		LastHandleDate = FDateTime::Now();
		return OnDeSerialize(objectStr);
	}
	return false;
}

bool FBaseRequestHandler::OnDeSerialize(const FString& objectStr) const
{
	return false;
}