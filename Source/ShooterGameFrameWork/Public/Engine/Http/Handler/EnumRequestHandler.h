#pragma once

#ifndef __EnumRequestHandler_H__
#define __EnumRequestHandler_H__

#include "BaseRequestHandler.h"
#include "RequestDataContainer.h"
#include "JsonObjectConverter.h"

template<class TResponseObject>
class FEnumRequestHandler : public FBaseRequestHandler
{
public:
	DECLARE_MULTICAST_DELEGATE_OneParam(FOnDeSerialize, const TResponseObject&)

	FEnumRequestHandler(const TWeakObjectPtr<UOperationRequest>& operation)
		: FBaseRequestHandler(operation)
	{

	}

	FOnDeSerialize Handler;
	virtual bool OnDeSerialize(const FString& objectStr) const override
	{
		FEnumDataContiner object;

		//===============================
		if (FJsonObjectConverter::JsonObjectStringToUStruct(objectStr, &object, 0, 0))
		{
			//===============================
			if (Handler.IsBound())
			{
				Handler.Broadcast(static_cast<TResponseObject>(object.Value));
			}
			return true;
		}
		return false;
	}
};

#endif