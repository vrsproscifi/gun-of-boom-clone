#pragma once

#ifndef __SimpleRequestHandler_H__
#define __SimpleRequestHandler_H__

#include "TemplateRequestHandler.h"


DECLARE_REQUEST_HANDLER(FTextDataContiner, FGuid);
DECLARE_REQUEST_HANDLER(FTextDataContiner, FLokaGuid);
DECLARE_REQUEST_HANDLER(FTextDataContiner, FString);
DECLARE_REQUEST_HANDLER(FTextDataContiner, FName);

DECLARE_REQUEST_HANDLER(FINT32DataContiner, bool);
DECLARE_REQUEST_HANDLER(FINT32DataContiner, int32);
DECLARE_REQUEST_HANDLER(FUINT32DataContiner, uint32);
DECLARE_REQUEST_HANDLER(FINT64DataContiner, int64);
DECLARE_REQUEST_HANDLER(FUINT64DataContiner, uint64);
DECLARE_REQUEST_HANDLER(FFloatDataContiner, float);

#endif