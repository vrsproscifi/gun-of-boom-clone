#pragma once

//====================================================
#include "OperationRequestServerHost.generated.h"


struct SHOOTERGAMEFRAMEWORK_API FVerb
{
	static const FString Post;
	static const FString Get;
};

USTRUCT()
struct SHOOTERGAMEFRAMEWORK_API FOperationRequestServerHost
{
	GENERATED_USTRUCT_BODY()

	static const FOperationRequestServerHost MasterClient;
	static const FOperationRequestServerHost MasterServer;

	FOperationRequestServerHost()
	{

	}

	FOperationRequestServerHost(const FString &host, const FString &scheme)
		: Host(host)
		, Scheme(scheme)
	{

	}

	UPROPERTY(Transient, VisibleInstanceOnly)
		FString Host;

	UPROPERTY(Transient, VisibleInstanceOnly)
		FString Scheme;
};