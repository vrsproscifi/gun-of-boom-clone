#pragma once
//==========================================
#include "DecalData.h"

//==========================================
#include "Engine/EngineTypes.h"
#include "GameFramework/Actor.h"

//==========================================
#include "ShooterImpactEffect.generated.h"

//==========================================
class UMaterial;
class UParticleSystem;
class USoundCue;




//==========================================
UCLASS(Abstract, Blueprintable)
class SHOOTERGAMEFRAMEWORK_API AShooterImpactEffect : public AActor
{
	GENERATED_UCLASS_BODY()

	/** default impact FX used when material specific override doesn't exist */
	UPROPERTY(EditDefaultsOnly, Category=Defaults)
	UParticleSystem* FX[EShooterPhysMaterialType::End];

	/** default impact sound used when material specific override doesn't exist */
	UPROPERTY(EditDefaultsOnly, Category=Defaults)
	USoundCue* Sound[EShooterPhysMaterialType::End];

	/** default decal when material specific override doesn't exist */
	UPROPERTY(EditDefaultsOnly, Category=Defaults)
	FDecalData DefaultDecal;

	/** surface data for spawning */
	UPROPERTY(BlueprintReadOnly, Category=Surface)
	FHitResult SurfaceHit;

	/** spawn effect */
	virtual void PostInitializeComponents() override;

protected:

	/** get FX for material type */
	UParticleSystem* GetImpactFX(TEnumAsByte<EPhysicalSurface> SurfaceType) const;

	/** get sound for material type */
	USoundCue* GetImpactSound(TEnumAsByte<EPhysicalSurface> SurfaceType) const;

	UMaterial* GetImpactDecal(TEnumAsByte<EPhysicalSurface> SurfaceType) const;
};
