#pragma once

//==========================================
#include "Engine/Types/ShooterPhysMaterialType.h"

//==========================================
#include "DecalData.generated.h"

//==========================================
class UMaterial;

//==========================================
USTRUCT()
struct SHOOTERGAMEFRAMEWORK_API FDecalData
{
	GENERATED_USTRUCT_BODY()

		/** material */
	UPROPERTY(EditDefaultsOnly, Category = Decal)
	UMaterial* DecalMaterial[EShooterPhysMaterialType::End];

	/** quad size (width & height) */
	UPROPERTY(EditDefaultsOnly, Category = Decal)
		float DecalSize;

	/** lifespan */
	UPROPERTY(EditDefaultsOnly, Category = Decal)
		float LifeSpan;

	/** defaults */
	FDecalData()
		: DecalSize(256.f)
		, LifeSpan(10.f)
	{
	}
};