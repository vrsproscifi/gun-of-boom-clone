// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "Engine/DataAsset.h"
#include "IsoCountry.h"
#include "WorldFlagsData.generated.h"


class UMaterialInstanceDynamic;
class UTexture2D;
class UMaterialInterface;

UCLASS()
class SHOOTERGAMEFRAMEWORK_API UWorldFlagsData : public UDataAsset
{
	GENERATED_BODY()

public:

	UWorldFlagsData();

	virtual void PostInitProperties() override;

	UFUNCTION(BlueprintCallable, BlueprintPure, Category = "Icon|Flag")
	UMaterialInstanceDynamic* GetFlagMaterialByCode(const EIsoCountry& InCode = EIsoCountry::RU) const;

	UFUNCTION(BlueprintCallable, BlueprintPure, Category = "Icon|Flag")
	int32 GetFlagIndexByCode(const EIsoCountry& InCode = EIsoCountry::RU) const;

	UFUNCTION(BlueprintCallable, BlueprintPure, Category = "Icon|Flag")
	UTexture2D* GetFlagsTexture() const;

	UFUNCTION(BlueprintCallable, BlueprintPure, Category = "Icon|Flag")
	FVector2D GetFlagsTextureSubImages() const;

#if WITH_EDITOR
	virtual void PostEditChangeProperty(struct FPropertyChangedEvent& PropertyChangedEvent) override;
#endif

protected:

	void RefreshMIDS();

	UPROPERTY(EditDefaultsOnly)
	UTexture2D* FlagsSheet;

	UPROPERTY(EditDefaultsOnly)
	FVector2D SheetSubImages;

	UPROPERTY(EditDefaultsOnly)
	FName Parameter_SheetSubImages;

	UPROPERTY(EditDefaultsOnly)
	UMaterialInterface* FlagMaterial;

	UPROPERTY(EditDefaultsOnly)
	FName Parameter_SheetTexture;

	UPROPERTY(EditDefaultsOnly)
	FName Parameter_SheetIndex;

	UPROPERTY(EditDefaultsOnly)
	TMap<EIsoCountry, int32> FlagsMap;

	UPROPERTY(EditDefaultsOnly)
	TMap<EIsoCountry, UMaterialInstanceDynamic*> FlagsMIDS;
};
