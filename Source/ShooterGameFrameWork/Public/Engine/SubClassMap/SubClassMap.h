#pragma once
//=========================================
#include "Engine/DataAsset.h"
#include "Templates/Casts.h"

//=========================================
#include "SubClassMap.generated.h"

//=========================================
USTRUCT(Blueprintable, BlueprintType)
struct SHOOTERGAMEFRAMEWORK_API FSubClassNamePair
{
	GENERATED_USTRUCT_BODY();

	UPROPERTY(EditDefaultsOnly)		FName Name;
	UPROPERTY(EditDefaultsOnly)		TSubclassOf<UObject> Object;
};


UCLASS(Blueprintable, BlueprintType, EditInlineNew)
class SHOOTERGAMEFRAMEWORK_API USubClassMap : public UDataAsset
{
	GENERATED_UCLASS_BODY()

private:
	UPROPERTY(EditDefaultsOnly)					TArray<FSubClassNamePair> Objects;
	UPROPERTY(Transient)						TMap<FName, TSubclassOf<UObject>> ObjectsMap;

public:
	UFUNCTION(BlueprintCallable)
	void UpdateObjectsMap();

	UFUNCTION(BlueprintCallable)
	const TArray<FSubClassNamePair>& GetObjects() const
	{
		return Objects;
	}

	UFUNCTION(BlueprintCallable, Category = "Game|Weapon")
	const TMap<FName, TSubclassOf<UObject>>& GetObjectsMap() const
	{
		return ObjectsMap;
	}
};
