#pragma once

//=========================================
#include "AdsRewardedVideo.generated.h"

//=========================================
USTRUCT(Blueprintable, BlueprintType)
struct SHOOTERGAMEFRAMEWORK_API FPlayRewardedVideoRequest
{
	GENERATED_USTRUCT_BODY();

	UPROPERTY(EditDefaultsOnly)		FDateTime Started;
	UPROPERTY(EditDefaultsOnly)		FString Category;
	UPROPERTY(EditDefaultsOnly)		FString Entity;

	FPlayRewardedVideoRequest()
		: Started(FDateTime::UtcNow())
	{
		
	}

	FPlayRewardedVideoRequest(const FString& InCategory, const FString& InEntity)
		: Category(InCategory)
		, Entity(InEntity)
	{
		
	}
};

//=========================================
USTRUCT(Blueprintable, BlueprintType)
struct SHOOTERGAMEFRAMEWORK_API FPlayRewardedVideoResponse : public FPlayRewardedVideoRequest
{
	GENERATED_USTRUCT_BODY();

	UPROPERTY(EditDefaultsOnly)		FDateTime Completed;


	FPlayRewardedVideoResponse()
		: Completed(FDateTime::UtcNow())
	{
		
	}

	FPlayRewardedVideoResponse(const FPlayRewardedVideoRequest& InReuest)
		: FPlayRewardedVideoRequest(InReuest.Category, InReuest.Entity)
	{

	}

	FPlayRewardedVideoResponse(const FString& InCategory, const FString& InEntity)
		: FPlayRewardedVideoRequest(InCategory, InEntity)
	{

	}
};
