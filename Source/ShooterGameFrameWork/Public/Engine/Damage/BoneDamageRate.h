#pragma once

//=========================================
#include "Engine/DataAsset.h"

//=========================================
#include "BoneDamageRate.generated.h"

//=========================================
USTRUCT(Blueprintable, BlueprintType)
struct SHOOTERGAMEFRAMEWORK_API FBoneDamageRatePair
{
	GENERATED_USTRUCT_BODY();

	UPROPERTY(EditDefaultsOnly)		FName Name;

	UPROPERTY(EditDefaultsOnly)		float Rate;
};

UCLASS(Blueprintable, BlueprintType, EditInlineNew)
class SHOOTERGAMEFRAMEWORK_API UBoneDamageRate : public UDataAsset
{
	GENERATED_UCLASS_BODY()

private:
	UPROPERTY(EditDefaultsOnly)					TArray<FBoneDamageRatePair> Rates;
	UPROPERTY(Transient)						TMap<FName, float> RatesMap;

public:
	UFUNCTION(BlueprintCallable, Category = "Game|Weapon")
	void UpdateRatesMap();

	UFUNCTION(BlueprintCallable, Category = "Game|Weapon")
	const TArray<FBoneDamageRatePair>& GetRates() const
	{
		return Rates;
	}

	UFUNCTION(BlueprintCallable, Category = "Game|Weapon")
	const TMap<FName, float>& GetRatesMap() const
	{
		return RatesMap;
	}
};
