#pragma once
#include "BasicItemEntity.h"
#include "GameFramework/Actor.h"
#include "BasicItemInstance.generated.h"

//========================================


UCLASS(BlueprintType, Blueprintable)
class SHOOTERGAMEFRAMEWORK_API ABasicItemInstance : public AActor
{
	GENERATED_UCLASS_BODY()

private:
	//========================================
	/* EntityId in data base*/
	UPROPERTY(VisibleInstanceOnly) FGuid EntityId;

	/* ModelId in game singleton*/
	UPROPERTY(VisibleInstanceOnly) int32 ModelId;

	/* Model entity cache*/
	UPROPERTY() UBasicItemEntity* ModelEntity;

public:
	const FGuid& GetEntityId() const 
	{
		return EntityId;
	}

	//========================================
	UFUNCTION() UBasicItemEntity* GetModelEntity() const
	{
		return ModelEntity;
	}
};