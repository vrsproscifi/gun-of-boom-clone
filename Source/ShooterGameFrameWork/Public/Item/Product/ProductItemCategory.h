#pragma once

//==========================================
#include "ProductItemCategory.generated.h"

//==========================================
UENUM(BlueprintType, Blueprintable)
enum class EProductItemCategory : uint8
{
	None,

	//------------------------
	Money,
	Donate,
	Booster,

	//------------------------
	Cases,
	TrophyCases,
	BattleCoins,

	//------------------------
	End
};