#pragma once
#include "BaseProductItemEntity.h"
#include "BaseProductItemProperty.h"
#include "ExchangeItemEntity.generated.h"


UCLASS()
class SHOOTERGAMEFRAMEWORK_API UExchangeItemEntity : public UBaseProductItemEntity
{
	GENERATED_UCLASS_BODY()


	//================================================================================================================================
#pragma region Property
private:
	UPROPERTY(EditDefaultsOnly, meta = (ShowOnlyInnerProperties)) FBaseProductItemProperty ProductProperty;
#pragma region GetBasicProperty
public:

	const FBasicGameProperty& GetBasicProperty() const override final
	{
		return ProductProperty;
	}

	FBasicGameProperty& GetBasicProperty() override final
	{
		return ProductProperty;
	}

	UFUNCTION(BlueprintCallable, BlueprintPure)
	const FBaseProductItemProperty& GetProductProperty() const
	{
		return GetItemProperty<FBaseProductItemProperty>();
	}

	FORCEINLINE FBaseProductItemProperty& GetProductProperty()
	{
		return GetItemProperty<FBaseProductItemProperty>();
	}
#pragma endregion
#pragma endregion

};