﻿#pragma once
//==========================================
#include "Item/GameItemCost.h"

//==========================================
#include "ProductItemCostCollection.h"

//==========================================
#include "ProductItemContainer.generated.h"


//==========================================
USTRUCT(BlueprintType, Blueprintable)
struct SHOOTERGAMEFRAMEWORK_API FProductItemContainer
{
	GENERATED_USTRUCT_BODY()

	//===============================================
	UPROPERTY()				FString ProductId;
	UPROPERTY()				TArray<FProductItemCostCollection> CostsV2;
};