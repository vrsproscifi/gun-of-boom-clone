﻿#pragma once
//==========================================
#include "Item/GameItemCost.h"

//==========================================
#include "ProductDiscountCoupone.h"

//==========================================
#include "ProductItemCostCollection.generated.h"


//==========================================
USTRUCT(BlueprintType, Blueprintable)
struct SHOOTERGAMEFRAMEWORK_API FProductItemCostCollection
{
	GENERATED_USTRUCT_BODY()

	UPROPERTY()				EProductDiscountCoupone Discount;
	UPROPERTY()				TArray<FGameItemCost> Costs;
};