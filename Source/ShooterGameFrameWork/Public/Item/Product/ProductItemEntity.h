#pragma once
#include "BaseProductItemEntity.h"
#include "ProductItemProperty.h"
#include "ProductItemEntity.generated.h"


struct FProductItemContainer;
UCLASS()
class SHOOTERGAMEFRAMEWORK_API UProductItemEntity : public UBaseProductItemEntity
{
	GENERATED_UCLASS_BODY()


	//================================================================================================================================
#pragma region Property
private:
	UPROPERTY(EditDefaultsOnly, meta = (ShowOnlyInnerProperties)) FProductItemProperty ProductProperty;
#pragma region GetBasicProperty
public:
	static EIsoCountry CurrentPlayerCountry;

	const FBasicGameProperty& GetBasicProperty() const override final
	{
		return ProductProperty;
	}

	FBasicGameProperty& GetBasicProperty() override final
	{
		return ProductProperty;
	}


	UFUNCTION(BlueprintCallable, BlueprintPure)
		FString GetTargetProductId() const;

	UFUNCTION(BlueprintCallable, BlueprintPure)
	const FProductItemProperty& GetProductProperty() const
	{
		return GetItemProperty<FProductItemProperty>();
	}

	FORCEINLINE FProductItemProperty& GetProductProperty()
	{
		return GetItemProperty<FProductItemProperty>();
	}
#pragma endregion
#pragma endregion

	void InitializeProduct(const FProductItemContainer& ProductContainer);

	virtual const FGameItemCost& GetItemCost() const override;

	//===============================================
	const TArray<FGameItemCost>* GetCostsByDiscountCoupone(const EProductDiscountCoupone& InDiscountCoupone) const;

	//===============================================
	virtual const FGameItemCost* GetNonDiscountedCost() const override;
	virtual const FGameItemCost* GetDiscountedCost() const override;
};