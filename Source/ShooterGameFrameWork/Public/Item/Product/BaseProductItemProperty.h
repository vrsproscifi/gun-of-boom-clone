﻿#pragma once
//==========================================
#include "BasicItemProperty.h"

//==========================================
#include "ProductItemCategory.h"

//==========================================
#include "BaseProductItemProperty.generated.h"


//==========================================
USTRUCT(BlueprintType, Blueprintable)
struct SHOOTERGAMEFRAMEWORK_API FBaseProductItemProperty : public FBasicItemProperty
{
	GENERATED_USTRUCT_BODY()

	//===============================================
	UPROPERTY(EditDefaultsOnly)				EProductItemCategory	ProductCategory;
};