#pragma once
#include "ProductDiscountCoupone.generated.h"

//=======================================================
UENUM()
enum class EProductDiscountCoupone : uint8
{
	None,
	Percent_5,
	Percent_15,
	Percent_30,
	Percent_50,
	Percent_70,
};

static FString GetProductDiscountCouponeSuffix(const EProductDiscountCoupone& InDiscountCoupone)
{
	if (InDiscountCoupone == EProductDiscountCoupone::Percent_5)
	{
		return "_d_5";
	}

	if (InDiscountCoupone == EProductDiscountCoupone::Percent_15)
	{
		return "_d_15";
	}

	if (InDiscountCoupone == EProductDiscountCoupone::Percent_30)
	{
		return "_d_30";
	}

	if (InDiscountCoupone == EProductDiscountCoupone::Percent_50)
	{
		return "_d_50";
	}

	if (InDiscountCoupone == EProductDiscountCoupone::Percent_70)
	{
		return "_d_70";
	}

	return FString();
}
