#pragma once
#include "BasicItemEntity.h"
#include "BaseProductItemProperty.h"
#include "BaseProductItemEntity.generated.h"


UCLASS()
class SHOOTERGAMEFRAMEWORK_API UBaseProductItemEntity : public UBasicItemEntity
{
	GENERATED_UCLASS_BODY()


private:
//#if WITH_EDITOR && WITH_METADATA
	UPROPERTY(Transient, AssetRegistrySearchable)				EProductItemCategory	ProductCategory;
//#endif

protected:
	virtual void UpdateEditorHelperProperties() override;

	//================================================================================================================================
#pragma region GetBasicProperty
public:

	UFUNCTION(BlueprintCallable, BlueprintPure)
		bool IsInAppPurchase() const;

	UFUNCTION(BlueprintCallable, BlueprintPure)
		const EProductItemCategory& GetProductCategory() const;

	UFUNCTION(BlueprintCallable, BlueprintPure)
		FString GetProductCategoryName() const;

	UFUNCTION(BlueprintCallable, BlueprintPure)
		uint8 GetProductCategoryId() const;


	UFUNCTION(BlueprintCallable, BlueprintPure)
		const FBaseProductItemProperty& GetBaseProductProperty() const
	{
		return GetItemProperty<FBaseProductItemProperty>();
	}

	FORCEINLINE FBaseProductItemProperty& GetBaseProductProperty()
	{
		return GetItemProperty<FBaseProductItemProperty>();
	}
#pragma endregion
};