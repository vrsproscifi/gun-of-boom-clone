﻿#pragma once
//==========================================
#include "BaseProductItemProperty.h"

//==========================================
#include "ProductItemCostCollection.h"

//==========================================
#include "ProductItemProperty.generated.h"


//==========================================
USTRUCT(BlueprintType, Blueprintable)
struct SHOOTERGAMEFRAMEWORK_API FProductItemProperty : public FBaseProductItemProperty
{
	GENERATED_USTRUCT_BODY()

	//===============================================
	UPROPERTY(VisibleAnywhere, Transient)				TArray<FProductItemCostCollection>	Costs;
};