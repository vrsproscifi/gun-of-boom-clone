#pragma once
//=========================================
#include "SlateCore/Public/Styling/SlateBrush.h"

//=========================================
#include "GameItemType.h"
#include "ItemInstanceFlags.h"

//=========================================
#include "GameItemCost.h"
#include "GameItemModification.h"

//==========================================
#include "Product/ProductDiscountCoupone.h"

//=========================================
#include "BasicGameProperty.h"
#include "BasicItemProperty.generated.h"

USTRUCT()
struct SHOOTERGAMEFRAMEWORK_API FBasicItemProperty : public FBasicGameProperty
{
	GENERATED_USTRUCT_BODY()

	UPROPERTY(EditDefaultsOnly)				bool								bIsHiddenInShop;

	UPROPERTY(VisibleAnywhere, Transient)	EProductDiscountCoupone				ActiveDiscount;
	UPROPERTY(VisibleAnywhere, Transient)	FDateTime							EndActiveDiscountDate;

	UPROPERTY(EditDefaultsOnly)				FString					ProductId;

	UPROPERTY(VisibleAnywhere, Transient)	EItemInstanceFlags Flags;

	UPROPERTY(EditDefaultsOnly)				int32 DisplayPosition;

	UPROPERTY(EditDefaultsOnly)				FSlateBrush Image;
	UPROPERTY(EditDefaultsOnly)				FSlateBrush Icon;

	/*	Required player level */
	UPROPERTY(VisibleAnywhere, Transient)	uint32	Level;

	UPROPERTY(VisibleAnywhere, Transient)	uint32	AdditionalId;
	UPROPERTY(VisibleAnywhere, Transient)	uint32	AmountInProduct;


	UPROPERTY(VisibleAnywhere, Transient)	FGameItemCost	DiscountedCost;
	UPROPERTY(VisibleAnywhere, Transient)	FGameItemCost	Cost;
	UPROPERTY(EditAnywhere)					EGameItemType	Type;

	// Temp here, for calculate mass into item while editing.
	UPROPERTY(BlueprintReadOnly, EditDefaultsOnly, meta = (DisplayName = "Mass In Kg"))
	float Mass;

	UPROPERTY(Transient, VisibleAnywhere)				TArray<FGameItemModification> Modifications;

	static FGameItemModification* GetSafeModification(TArray<FGameItemModification> &InModifications, const int32& InLevel)
	{
		if (InModifications.Num())
		{
			auto result = InModifications.FindByPredicate([&](const FGameItemModification& m)
			{
				return m.Position == InLevel;
			});

			if (result == nullptr)
			{
				InModifications.Sort([](const FGameItemModification& A, const FGameItemModification& B) { return A.Position < B.Position; });
				result = &InModifications.Last();
			}
			return result;
		}
		return nullptr;
	}

	FBasicItemProperty()
	{
		bIsHiddenInShop = false;
	}
	
	FBasicItemProperty(const EGameItemType&& type)
		: Type(type)
	{
		bIsHiddenInShop = false;
	}
};