#pragma once
#include "BasicItemEntity.h"
#include "GrenadeItemProperty.h"
#include "GrenadeItemEntity.generated.h"


UCLASS()
class SHOOTERGAMEFRAMEWORK_API UGrenadeItemEntity : public UBasicItemEntity
{
	GENERATED_UCLASS_BODY()

		//================================================================================================================================
#pragma region Property
private:
	UPROPERTY(EditDefaultsOnly) FGrenadeItemProperty GrenadeItemProperty;

#pragma region GetBasicProperty
public:
	float GetDefaultDynamicValue() const override final
	{
		return GetGrenadeProperty().Damage;
	}

	const FBasicGameProperty& GetBasicProperty() const override final
	{
		return GrenadeItemProperty;
	}

	FBasicGameProperty& GetBasicProperty() override final
	{
		return GrenadeItemProperty;
	}

	UFUNCTION(BlueprintCallable)
	const FGrenadeItemProperty& GetGrenadeProperty() const
	{
		return GetItemProperty<FGrenadeItemProperty>();
	}
#pragma endregion
#pragma endregion
	//================================================================================================================================

};