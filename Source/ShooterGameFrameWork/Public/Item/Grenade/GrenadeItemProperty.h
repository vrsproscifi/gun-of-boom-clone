#pragma once
//==========================================
#include "BasicItemProperty.h"

//==========================================
#include "Engine/Animation/TwoAnimSequence.h"
#include "Sound/SoundCue.h"

//==========================================
#include "GrenadeItemProperty.generated.h"

class USoundCue;
USTRUCT()
struct SHOOTERGAMEFRAMEWORK_API FGrenadeItemProperty : public FBasicItemProperty
{
	GENERATED_USTRUCT_BODY()

	uint32 GetDefaultAmount() const
	{
		return FMath::Max<uint32>(1, DefaultAmount);
	}

	UPROPERTY(EditDefaultsOnly, Category = Damage)
		uint32 DefaultAmount;

	UPROPERTY(EditDefaultsOnly, Category = Animation)
		FTwoAnimMontage FireAnim;

	UPROPERTY(EditDefaultsOnly, Category = Damage)
		float AcivationDelay;

	UPROPERTY(EditDefaultsOnly, Category = Damage)
		float Damage;

	UPROPERTY(EditDefaultsOnly, Category = Damage)
		float DamageRadius;

	// * Use 0.0 to ditonate on hit any surface
	UPROPERTY(EditAnywhere, Category = Gameplay)
		float DetonationDelay;

	UPROPERTY(EditAnywhere, Category = Gameplay)
		TSubclassOf<UDamageType> DamageType;

	UPROPERTY(EditAnywhere, Category = Gameplay)
		UParticleSystem* AirFX;

	UPROPERTY(EditAnywhere, Category = Gameplay)
		UParticleSystem* ExplosionFX;

	UPROPERTY(EditAnywhere, Category = Gameplay)
		TSoftObjectPtr<USoundCue> ExplosionSound;

	UPROPERTY(EditAnywhere, Category = Gameplay)
		FVector SpawnGrenadeOffset;

	DEFINE_SOFT_OBJECT_GETTER(USoundCue, ExplosionSound);
};
