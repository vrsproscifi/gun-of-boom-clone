#pragma once
//=========================================
#include "GameItemType.generated.h"

//=========================================
UENUM()
enum class EGameItemType : uint8
{
	None,

	//================================
	Rifle,
	Shotgun,
	SniperRifle,
	MachineGun,
	Pistol,
	Knife,

	//================================
	Head,
	Torso,
	Legs,

	//================================
	Grenade,
	AidKit,
	Stimulant,
	RepairKit,

	//================================
	Skin,
	Mask,
	Costume,

	//================================
	End
};

struct FGameItemType
{
	static TArray<FString> GetNames()
	{
		static const TArray<FString> Names =
		{
			"Rifle",
			"Shotgun",
			"SniperRifle",
			"MachineGun",
			"Pistol",
			"Knife",
			"Head",
			"Torso",
			"Legs",
			"Grenade",
			"AidKit",
			"Stimulant",
			"RepairKit",
			"Skin",
			"Mask",
			"Costume"
		};

		return Names;
	}

	static FString GetName(const EGameItemType& InItemType)
	{
		auto Index = static_cast<uint32>(InItemType);
		return GetNames()[Index];
	}

	static TArray<FString> GetGroupNames()
	{
		static const TArray<FString> Names =
		{
			"Weapon",
			"Weapon",
			"Weapon",
			"Weapon",
			"Weapon",
			"Weapon",
			"Armour",
			"Armour",
			"Armour",
			"Grenade",
			"AidKit",
			"Stimulant",
			"RepairKit",
			"Visual",
			"Visual",
			"Visual"
		};

		return Names;
	}

	static FString GetItemTypeGroupName(const EGameItemType& InItemType)
	{
		auto Index = static_cast<uint32>(InItemType);
		return GetGroupNames()[Index];
	}

	

};