#pragma once
#include "BasicItemEntity.h"
#include "AidItemProperty.h"
#include "AidItemEntity.generated.h"


UCLASS()
class SHOOTERGAMEFRAMEWORK_API UAidItemEntity : public UBasicItemEntity
{
	GENERATED_UCLASS_BODY()

private:
//#if WITH_EDITOR && WITH_METADATA
	UPROPERTY(Transient, AssetRegistrySearchable)	float Amount;
	UPROPERTY(Transient, AssetRegistrySearchable)	float ActivationTime;
	UPROPERTY(Transient, AssetRegistrySearchable)	float ActivationDelay;
//#endif

protected:
	virtual void UpdateEditorHelperProperties() override;


		//================================================================================================================================
#pragma region Property
private:
	UPROPERTY(EditDefaultsOnly) FAidItemProperty AidProperty;

#pragma region GetBasicProperty
public:
	const FBasicGameProperty& GetBasicProperty() const override final
	{
		return AidProperty;
	}

	float GetDefaultDynamicValue() const override final
	{
		return GetAidProperty().Amount;
	}

	FBasicGameProperty& GetBasicProperty() override final
	{
		return AidProperty;
	}

	UFUNCTION(BlueprintCallable)
	const FAidItemProperty& GetAidProperty() const
	{
		return GetItemProperty<FAidItemProperty>();
	}
#pragma endregion
#pragma endregion
	//================================================================================================================================

};
