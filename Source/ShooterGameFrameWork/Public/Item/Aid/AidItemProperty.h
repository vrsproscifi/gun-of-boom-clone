#pragma once
#include "BasicItemProperty.h"
#include "Particles/ParticleSystem.h"
#include "AidItemProperty.generated.h"

USTRUCT()
struct SHOOTERGAMEFRAMEWORK_API FAidItemProperty : public FBasicItemProperty
{
	GENERATED_USTRUCT_BODY()

	UPROPERTY(EditDefaultsOnly, Category = Effects)		UParticleSystem* ActiveFX;

	UPROPERTY(EditDefaultsOnly, Category = Property)	float Amount = 400.0f;
	UPROPERTY(EditDefaultsOnly, Category = Property)	float ActivationTime = 1.0f;
	UPROPERTY(EditDefaultsOnly, Category = Property)	float ActivationDelay = .0f;
};
