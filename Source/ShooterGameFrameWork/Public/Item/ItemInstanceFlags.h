#pragma once

//=========================================
#include "ItemInstanceFlags.generated.h"

//=========================================
UENUM(BlueprintType, Blueprintable, meta = (Bitflags))
enum class EItemInstanceFlags : uint8
{
	None,
	Hidded = 1,
	Subscribe = 2,
	AllowSome = 4,
	IgnoreRequirements = 8,

	Product = 16,
	Service = 32,
	Currency = 64,
	Case = 128,
};

ENUM_CLASS_FLAGS(EItemInstanceFlags);