#pragma once
//=========================================
#include "GameItemCost.h"

//=========================================
#include "ItemInstanceFlags.h"

//=========================================
#include "GameItemModification.h"

//=========================================
#include "BasicItemContainer.generated.h"



USTRUCT()
struct SHOOTERGAMEFRAMEWORK_API FBasicItemContainer
{
	GENERATED_USTRUCT_BODY()

	UPROPERTY()		int32 ModelId;

	UPROPERTY()		uint32 AdditionalId;

	UPROPERTY()		uint32 AmountInProduct;

	UPROPERTY()		uint8 Level;

	UPROPERTY()		FGameItemCost Cost;

	UPROPERTY()		EItemInstanceFlags Flags;

	UPROPERTY()		TArray<FGameItemModification> Modifications;
};
