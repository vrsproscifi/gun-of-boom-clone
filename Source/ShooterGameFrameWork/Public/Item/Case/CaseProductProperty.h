#pragma once

//===================================
#include "BaseProductItemProperty.h"

//===================================
#include "CaseProductProperty.generated.h"

//===================================

class UCaseEntity;

//===================================

USTRUCT(BlueprintType, Blueprintable)
struct SHOOTERGAMEFRAMEWORK_API FCaseProductProperty : public FBaseProductItemProperty
{
	GENERATED_USTRUCT_BODY()

	UPROPERTY(VisibleAnywhere, Transient)
	UCaseEntity* CaseEntity;
};