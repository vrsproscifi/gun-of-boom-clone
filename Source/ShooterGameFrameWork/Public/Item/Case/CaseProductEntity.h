#pragma once
//==============================
#include "ProductItemEntity.h"

//==============================
#include "CaseProductProperty.h"

//==============================
#include "CaseProductEntity.generated.h"

//==============================
class UCaseEntity;
struct FCaseProperty;
struct FCaseItemProperty;

/*
 * Кейс, который в магазине / арсенале
 */
UCLASS()
class SHOOTERGAMEFRAMEWORK_API UCaseProductEntity : public UBaseProductItemEntity
{
	GENERATED_UCLASS_BODY()

public:
	virtual void Initialize(const FBasicItemContainer& initializer) override;

	UFUNCTION(BlueprintCallable)
	UCaseEntity* GetCaseEntity() const;
	UCaseEntity* GetCaseEntity();

	const FCaseProperty* GetCaseEntityProperty() const;
	const FCaseProperty* GetCaseEntityProperty();

	const TArray<FCaseItemProperty>* GetDropedItems() const;
	const TArray<FCaseItemProperty>* GetDropedItems();


	//TArray<UBasicItemEntity> Get

	//================================================================================================================================
#pragma region Property
private:
	UPROPERTY(EditDefaultsOnly, meta = (ShowOnlyInnerProperties)) FCaseProductProperty CaseProductProperty;
#pragma region GetBasicProperty
public:
	const FBasicGameProperty& GetBasicProperty() const override final
	{
		return CaseProductProperty;
	}

	float GetDefaultDynamicValue() const override final
	{
		return 0.0f;
	}

	FBasicGameProperty& GetBasicProperty() override final
	{
		return CaseProductProperty;
	}

	UFUNCTION(BlueprintCallable)
	const FCaseProductProperty& GetCaseProductProperty() const
	{
		return GetItemProperty<FCaseProductProperty>();
	}

	FORCEINLINE FCaseProductProperty& GetCaseProductProperty()
	{
		return GetItemProperty<FCaseProductProperty>();
	}
#pragma endregion
#pragma endregion

};