#pragma once

#include "BasicGameEntity.h"
#include "BasicItemProperty.h"
#include "MeshMergeFunctionLibrary.h"
#include "BasicItemEntity.generated.h"


class UBasicPlayerItem;
class UAnimInstance;
struct FBasicItemContainer;
class USkeletalMesh;


USTRUCT(Blueprintable)
struct FMeshAnimationPair
{
	GENERATED_USTRUCT_BODY();

	UPROPERTY(EditAnywhere)		int32						MinLOD;
	UPROPERTY(EditAnywhere)		USkeletalMesh*				Mesh;	
	UPROPERTY(EditAnywhere)		TSubclassOf<UAnimInstance>	Anim;

	//UPROPERTY(EditAnywhere)		FSkeletalMeshMergeParams ModularMesh;
};

USTRUCT(Blueprintable)
struct FPreviewMeshSettings
{
	GENERATED_USTRUCT_BODY();

	UPROPERTY(EditAnywhere)		FTransform		Mesh;
	UPROPERTY(EditAnywhere)		FTransform		Pivot;
	UPROPERTY(EditAnywhere)		FTransform		Camera;
};

UCLASS()
class SHOOTERGAMEFRAMEWORK_API UBasicItemEntity : public UBasicGameEntity
{
	GENERATED_UCLASS_BODY()

public:
	//===============================================
	virtual const FGameItemCost* GetNonDiscountedCost() const;
	virtual const FGameItemCost* GetDiscountedCost() const;

	UFUNCTION(BlueprintCallable)
	void SetActiveDiscount(const EProductDiscountCoupone& InDiscountCoupone, const FDateTime& InDiscountCouponeEndDate);

	UFUNCTION(BlueprintCallable, BlueprintPure)
		const EProductDiscountCoupone& GetActiveDiscount() const;

	UFUNCTION(BlueprintCallable, BlueprintPure)
		int32 GetActiveDiscountPercent() const;

	UFUNCTION(BlueprintCallable, BlueprintPure)
		bool IsDiscountedProduct() const;

	UFUNCTION(BlueprintCallable, BlueprintPure)
		FTimespan GetAvalibleDiscountTime() const;

protected:
	virtual void UpdateEditorHelperProperties() override;

public:
	UPROPERTY(Transient, AssetRegistrySearchable)				FString					ProductId;

	static FString GetEntityName(const UBasicItemEntity* InItem);
	virtual void Initialize(const FBasicItemContainer& initializer);

	//================================================================================================================================
#pragma region GetBasicProperty
public:
	virtual FString GetModelName() const override;

	UFUNCTION(BlueprintCallable, BlueprintPure)
		const FBasicItemProperty& GetItemProperty() const
	{
		return GetItemProperty<FBasicItemProperty>();
	}

	UFUNCTION(BlueprintCallable, BlueprintPure)
		virtual float GetDefaultDynamicValue() const
	{
		return 0.0f;
	}

	UFUNCTION(BlueprintCallable, BlueprintPure)
	float GetDynamicValueForLevel(const int32& InLevel) const;

	FBasicItemProperty& GetItemProperty()
	{
		return GetItemProperty<FBasicItemProperty>();
	}

	template<typename TProperty = FBasicItemProperty>
	const TProperty& GetItemProperty() const
	{
		return GetBasicProperty<TProperty>();
	}

	template<typename TProperty = FBasicItemProperty>
	TProperty& GetItemProperty()
	{
		return GetBasicProperty<TProperty>();
	}

	UFUNCTION(BlueprintCallable, BlueprintPure)
	const EGameItemType& GetItemType() const
	{
		return GetItemProperty().Type;
	}

	template<typename TModelType = EGameItemType>
	TModelType GetItemType() const
	{
		return static_cast<TModelType>(GetItemProperty().Type);
	}

	UFUNCTION(BlueprintCallable, BlueprintPure)
	FString GetItemTypeName() const
	{
		return FGameItemType::GetName(GetItemType());
	}

	UFUNCTION(BlueprintCallable, BlueprintPure)
	FString GetItemTypeGroupName() const
	{
		return FGameItemType::GetItemTypeGroupName(GetItemType());
	}

	template<typename TModelType = int32>
	const TModelType GetItemModelId() const
	{
		return static_cast<TModelType>(GetItemProperty().ModelId);
	}

	UFUNCTION(BlueprintCallable, BlueprintPure)
	int32 GetItemRequiredLevel() const
	{
		return GetItemProperty().Level;
	}

#pragma endregion
	UFUNCTION(BlueprintCallable, BlueprintPure)
	virtual const FGameItemCost& GetItemCost() const
	{
		return GetItemProperty().Cost;
	}

	UFUNCTION(BlueprintCallable, BlueprintPure)
		bool IsHiddenInShop() const;

	UFUNCTION(BlueprintCallable, BlueprintPure, Category = Description)
		bool HasAllowSomeBought() const;

	UFUNCTION(BlueprintCallable, BlueprintPure, Category = Description)
		bool HasIgnoreRequirements() const;

	UFUNCTION(BlueprintCallable, BlueprintPure, Category = Description)
		bool IsService() const;

	UFUNCTION(BlueprintCallable, BlueprintPure, Category = Description)
		bool IsCurrency() const;

	UFUNCTION(BlueprintCallable, BlueprintPure, Category = Description)
		bool IsProduct() const;

	UFUNCTION(BlueprintCallable, BlueprintPure, Category = Description)
		bool IsCase() const;


	UFUNCTION(BlueprintCallable, BlueprintPure, Category = Description)
	FORCEINLINE float GetMass() const 
	{
		return GetItemProperty().Mass;
	}

	UFUNCTION(BlueprintCallable, BlueprintPure, Category = Mesh)
	USkeletalMesh* GetSkeletalMesh() const;

protected:
	UPROPERTY(BlueprintReadOnly, EditDefaultsOnly, Category = Mesh)			FMeshAnimationPair PreviewMeshData;
	UPROPERTY(BlueprintReadOnly, EditDefaultsOnly, Category = Mesh)			FPreviewMeshSettings PreviewSettings;
	UPROPERTY(BlueprintReadOnly, EditDefaultsOnly, Category = Inventory)	TSubclassOf<UBasicPlayerItem> InventoryClass;

public:

	UFUNCTION(BlueprintCallable, BlueprintPure, Category = Mesh)
	FMeshAnimationPair GetPreviewMeshData() const;

	UFUNCTION(BlueprintCallable, BlueprintPure, Category = Mesh)
	FORCEINLINE FPreviewMeshSettings GetPreviewSettings() const { return PreviewSettings; }

	UFUNCTION(BlueprintCallable, BlueprintPure, Category = Inventory)
	FORCEINLINE TSubclassOf<UBasicPlayerItem> GetInventoryClass() const { return InventoryClass; }
};