#pragma once
//=========================================
#include "GameItemCost.h"

//=========================================
#include "GameItemModification.generated.h"


USTRUCT()
struct SHOOTERGAMEFRAMEWORK_API FGameItemModification
{
	GENERATED_USTRUCT_BODY()

	UPROPERTY(VisibleInstanceOnly)		FGameItemCost	Cost;
	UPROPERTY(VisibleInstanceOnly)		uint32			Level;
	UPROPERTY(VisibleInstanceOnly)		uint32			Position;

	UPROPERTY(VisibleInstanceOnly)		float			Damage;
	UPROPERTY(VisibleInstanceOnly)		float			Coefficient;

	FGameItemModification()
		: Level(0)		
		, Position(0)
		, Damage(0)
		, Coefficient(0)
	{
		
	}

	FGameItemModification(const float& InDamage)
		: Level(0)
		, Position(0)
		, Damage(InDamage)		
		, Coefficient(1)
	{
		
	}

};