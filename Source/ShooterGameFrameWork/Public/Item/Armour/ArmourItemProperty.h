#pragma once
#include "BasicItemProperty.h"
#include "ArmourItemProperty.generated.h"

USTRUCT()
struct SHOOTERGAMEFRAMEWORK_API FArmourItemProperty : public FBasicItemProperty
{
	GENERATED_USTRUCT_BODY()

	UPROPERTY(EditDefaultsOnly)	uint32 Armour;
};
