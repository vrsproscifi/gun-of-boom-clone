#pragma once
#include "BasicItemEntity.h"
#include "ArmourItemProperty.h"
#include "ArmourItemEntity.generated.h"


UCLASS()
class SHOOTERGAMEFRAMEWORK_API UArmourItemEntity : public UBasicItemEntity
{
	GENERATED_UCLASS_BODY()

		//================================================================================================================================
#pragma region Property
private:
	UPROPERTY(EditDefaultsOnly) FArmourItemProperty ArmourProperty;

#pragma region GetBasicProperty
public:
	const FBasicGameProperty& GetBasicProperty() const override final
	{
		return ArmourProperty;
	}

	float GetDefaultDynamicValue() const override final
	{
		return GetArmourProperty().Armour;
	}

	FBasicGameProperty& GetBasicProperty() override final
	{
		return ArmourProperty;
	}

	UFUNCTION(BlueprintCallable)
		const FArmourItemProperty& GetArmourProperty() const
	{
		return GetItemProperty<FArmourItemProperty>();
	}
#pragma endregion
#pragma endregion
	//================================================================================================================================

};