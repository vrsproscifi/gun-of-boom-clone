#pragma once

//=========================================
#include "IsoCountry.h"

//=========================================
#include "GameItemCost.generated.h"

//=========================================
UENUM(Blueprintable, BlueprintType)
enum class EGameCurrency : uint8
{
	//----------------------
	None,
	
	//----------------------
	Money,
	Donate,

	//----------------------
	BattleCoins,
	Reserved_2,
	Reserved_3,

	//----------------------
	/// <summary>
	/// Рубль
	/// </summary>
	RUB = 6,

	/// <summary>
	/// Доллар
	/// </summary>
	USD,

	/// <summary>
	/// Евро
	/// </summary>
	EUR,

	/// <summary>
	/// Гривна
	/// </summary>
	UAH = 9,

	/// <summary>
	/// Белорусский рубль
	/// </summary>
	BRL,

	/// <summary>
	/// Казахстанский тенге
	/// </summary>
	KZT,

	Fiat_4,
	Fiat_5,
	Fiat_6,
	Fiat_7,
	Fiat_8,
	Fiat_9,

	//----------------------
	End,
};


//=========================================
USTRUCT(Blueprintable, BlueprintType)
struct SHOOTERGAMEFRAMEWORK_API FGameItemCost
{
	GENERATED_USTRUCT_BODY()

	UPROPERTY(EditAnywhere)					int64			Amount;
	UPROPERTY(EditAnywhere)					EGameCurrency	Currency;
	UPROPERTY(VisibleAnywhere, Transient)	EIsoCountry		Country;
	FGameItemCost()
		: Amount(0)
		, Currency(EGameCurrency::None)
		, Country(EIsoCountry::None)
	{
		
	}

	FString CurrencyAsISO() const
	{
		switch (Currency) 
		{
		case EGameCurrency::Money:  return "Money";
		case EGameCurrency::Donate:  return "Donate";
		case EGameCurrency::BattleCoins:  return "BattleCoins";
		case EGameCurrency::RUB: return "RUB";
		case EGameCurrency::USD: return "USD";
		case EGameCurrency::EUR: return "EUR";
		case EGameCurrency::UAH: return "UAH";
		case EGameCurrency::KZT: return "KZT";
		default: return "Unk";
		}
	}	


	int32 AmountAsCents() const
	{
		return static_cast<int32>(FMath::Clamp<int64>(Amount * 100, 0, INT32_MAX));
	}

	float AmountAsCurrency() const
	{
		return Amount / 100.0f; 
	}
};
