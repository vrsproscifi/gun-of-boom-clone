// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "Item/BasicItemEntity.h"
#include "Sound/SoundCue.h"
#include "CharacterItemEntity.generated.h"

class USoundCue;

USTRUCT(Blueprintable)
struct SHOOTERGAMEFRAMEWORK_API FCharacterItemProperty : public FBasicItemProperty
{
	GENERATED_USTRUCT_BODY()

	UPROPERTY(EditAnywhere, BlueprintReadWrite) float			Health;
	UPROPERTY(EditAnywhere, BlueprintReadWrite) TSoftObjectPtr<USoundCue>		HitSound;
	DEFINE_SOFT_OBJECT_GETTER(USoundCue, HitSound);
};

UCLASS()
class SHOOTERGAMEFRAMEWORK_API UCharacterItemEntity : public UBasicItemEntity
{
	friend class FMergeMeshesWorker;

	GENERATED_UCLASS_BODY()
	
#pragma region Property
private:
	UPROPERTY(EditDefaultsOnly) FCharacterItemProperty CharacterProperty;
#pragma region GetBasicProperty
public:
	const FBasicGameProperty& GetBasicProperty() const override final
	{
		return CharacterProperty;
	}

	FBasicGameProperty& GetBasicProperty() override final
	{
		return CharacterProperty;
	}

	UFUNCTION(BlueprintCallable)
	const FCharacterItemProperty& GetCharacterProperty() const
	{
		return GetItemProperty<FCharacterItemProperty>();
	}

	FCharacterItemProperty& GetCharacterProperty()
	{
		return GetItemProperty<FCharacterItemProperty>();
	}

#pragma endregion
#pragma endregion
	
protected:

	UPROPERTY(BlueprintReadOnly, EditDefaultsOnly, Category = Mesh)		FMeshAnimationPair TPPMeshData;
	UPROPERTY(BlueprintReadOnly, EditDefaultsOnly, Category = Mesh)		FMeshAnimationPair FPPMeshData;
	UPROPERTY(BlueprintReadOnly, EditDefaultsOnly, Category = Mesh)		FMeshAnimationPair LobbyMeshData;

public:

	UFUNCTION(BlueprintCallable, BlueprintPure, Category = Mesh)
	FORCEINLINE FMeshAnimationPair GetTPPMeshData() const {	return TPPMeshData;	}

	UFUNCTION(BlueprintCallable, BlueprintPure, Category = Mesh)
	FORCEINLINE FMeshAnimationPair GetFPPMeshData() const { return FPPMeshData; }

	UFUNCTION(BlueprintCallable, BlueprintPure, Category = Mesh)
	FORCEINLINE FMeshAnimationPair GetLobbyMeshData() const { return LobbyMeshData; }
};
