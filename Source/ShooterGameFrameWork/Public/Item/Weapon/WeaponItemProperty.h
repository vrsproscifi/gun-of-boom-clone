#pragma once
//==========================================
#include "BasicItemProperty.h"

//==========================================
#include "Engine/Animation/TwoAnimSequence.h"
#include "Sound/SoundCue.h"
#include "Particles/ParticleSystem.h"

#include "Engine/Canvas.h"

//==========================================
#include "WeaponItemProperty.generated.h"

//==========================================
class UParticleSystem;
class UForceFeedbackEffect;
class UAnimMontage;
class UCameraShake;
class USoundCue;
class AShooterImpactEffect;

//==========================================
USTRUCT(BlueprintType, Blueprintable)
struct SHOOTERGAMEFRAMEWORK_API FWeaponConfiguration
{
	GENERATED_USTRUCT_BODY()

	//* Amount of ammo on clip
	UPROPERTY(EditDefaultsOnly, Category = WeaponStat)	int32	AmmoPerClip;
	
	//* 60 / FireRate = Time Between shots
	UPROPERTY(EditDefaultsOnly, Category = WeaponStat)	float	FireRate;	
	
	//* Clip reloading duration in seconds
	UPROPERTY(EditDefaultsOnly, Category = WeaponStat)	float	ReloadDuration;	
	
	//* Weapon mass in kg, maybe not used
	UPROPERTY(EditDefaultsOnly, Category = WeaponStat)	float	Mass;	
	
	//* Weapon feedback on fireing in degress
	UPROPERTY(EditDefaultsOnly, Category = WeaponStat)	float	Feedback;	
	
	//* Weapon spread on fireing in degress of cone
	UPROPERTY(EditDefaultsOnly, Category = WeaponStat)	float	Spread;	
	
	//* Shoot distance limit in meters
	UPROPERTY(EditDefaultsOnly, Category = WeaponStat)	float	Range;		
	
	//* Amount of damage per shoot
	UPROPERTY(EditDefaultsOnly, Category = WeaponStat)	float	Damage;			

	//* Amount of damage per shoot
	UPROPERTY(EditDefaultsOnly, Category = WeaponStat)	float	TargetingZoomDistance;

	//* true to reload by once bullet in time, false reload all clip
	UPROPERTY(EditDefaultsOnly, Category = WeaponStat)	bool	IsPartlyReload;	
	
	//* true to fireing while started, false to stop fire after shoot
	UPROPERTY(EditDefaultsOnly, Category = WeaponStat)	bool	IsAutomaticFire;	
	
	//* true to enable sniper scope overlay
	UPROPERTY(EditDefaultsOnly, Category = WeaponStat)	bool	HasScope;
	UPROPERTY(EditDefaultsOnly, Category = WeaponStat)	FSlateBrush		ScopeBrush;
	UPROPERTY(EditDefaultsOnly, Category = WeaponStat)	FSlateColor		ScopeColor;


	UPROPERTY(EditDefaultsOnly, Category = WeaponStat)	TSubclassOf<UDamageType>	DamageType;


	//* Minimum bullets for once shot
	UPROPERTY(EditDefaultsOnly, Category = WeaponStat, meta = (UIMin = 1, UIMax = 80))			int32 BulletsMin;
	
	//* Maximum bullets for once shot
	UPROPERTY(EditDefaultsOnly, Category = WeaponStat, meta = (UIMin = 1, UIMax = 100))			int32 BulletsMax;		
	
	//* If shoting over one bullet apply grouping
	UPROPERTY(EditDefaultsOnly, Category = WeaponStat, meta = (UIMin = 1.0f, UIMax = 50.0f))	float BulletsGrouping;		

	// Calculated from rate of fire
	float TimeBetweenShots() const
	{
		return 60.0f / FireRate;
	}

	FWeaponConfiguration()
		: AmmoPerClip(4)
		, FireRate(200)
		, ReloadDuration(1.5f)
		, Mass(0)
		, Feedback(0)
		, Spread(0)
		, Range(1000)
		, Damage(10)
		, TargetingZoomDistance(50)
		, IsPartlyReload(false)
		, IsAutomaticFire(true)
		, HasScope(false)
		, BulletsMin(1)
		, BulletsMax(1)
		, BulletsGrouping(1) { }
};

USTRUCT(BlueprintType, Blueprintable)
struct SHOOTERGAMEFRAMEWORK_API FWeaponItemProperty : public FBasicItemProperty
{
	GENERATED_USTRUCT_BODY()

	FWeaponItemProperty()
		//: FBasicItemProperty(EGameItemType::Rifle)
		: Name_InstantMuzzle(TEXT("InstMuzzle"))
		, Name_ProjMuzzle(TEXT("ProjMuzzle"))
	{

	}

	/** crosshair parts icons (left, top, right, bottom and center) */
	UPROPERTY(EditDefaultsOnly, Category = HUD)
	FCanvasIcon Crosshair[5];

	/** crosshair parts icons when targeting (left, top, right, bottom and center) */
	UPROPERTY(EditDefaultsOnly, Category = HUD)
		FCanvasIcon AimingCrosshair[5];

	/** only use red colored center part of aiming crosshair */
	UPROPERTY(EditDefaultsOnly, Category = HUD)
		bool UseLaserDot;

	/** false = default crosshair */
	UPROPERTY(EditDefaultsOnly, Category = HUD)
		bool UseCustomCrosshair;

	/** false = use custom one if set, otherwise default crosshair */
	UPROPERTY(EditDefaultsOnly, Category = HUD)
		bool UseCustomAimingCrosshair;

	/** true - crosshair will not be shown unless aiming with the weapon */
	UPROPERTY(EditDefaultsOnly, Category = HUD)
		bool bHideCrosshairWhileNotAiming;

	/** FX for muzzle flash */
	UPROPERTY(EditDefaultsOnly, Category = Effects)
		TSoftObjectPtr<UParticleSystem> MuzzleFX;

	DEFINE_SOFT_OBJECT_GETTER(UParticleSystem, MuzzleFX);


	/** FX for impact of any surface */
	UPROPERTY(EditDefaultsOnly, Category = Effects)		
		TSubclassOf<AShooterImpactEffect> ImpactEffect;

	/** FX for visual trace from weapon to hit point */
	UPROPERTY(EditDefaultsOnly, Category = Effects)		
		TSoftObjectPtr<UParticleSystem> TraceFX;

	DEFINE_SOFT_OBJECT_GETTER(UParticleSystem, TraceFX);

	/** camera shake on firing */
	UPROPERTY(EditDefaultsOnly, Category = Effects)
		TSubclassOf<UCameraShake> FireCameraShake;

	/** force feedback effect to play when the weapon is fired */
	UPROPERTY(EditDefaultsOnly, Category = Effects)
		UForceFeedbackEffect *FireForceFeedback;

	/** single fire sound (bLoopedFireSound not set) */
	UPROPERTY(EditDefaultsOnly, Category = Sound)
		USoundCue* FireSound;

	//DEFINE_SOFT_OBJECT_GETTER(USoundCue, FireSound);

	/** looped fire sound (bLoopedFireSound set) */
	UPROPERTY(EditDefaultsOnly, Category = Sound)
		USoundCue* FireLoopSound;

	//DEFINE_SOFT_OBJECT_GETTER(USoundCue, FireLoopSound);


	/** finished burst sound (bLoopedFireSound set) */
	UPROPERTY(EditDefaultsOnly, Category = Sound)
		USoundCue* FireFinishSound;

	//DEFINE_SOFT_OBJECT_GETTER(USoundCue, FireFinishSound);

	/** out of ammo sound */
	UPROPERTY(EditDefaultsOnly, Category = Sound)
		TSoftObjectPtr<USoundCue> OutOfAmmoSound;

	DEFINE_SOFT_OBJECT_GETTER(USoundCue, OutOfAmmoSound);

	/** reload sound */
	UPROPERTY(EditDefaultsOnly, Category = Sound)
		TSoftObjectPtr<USoundCue> ReloadSound;

	DEFINE_SOFT_OBJECT_GETTER(USoundCue, ReloadSound);

	/** reload animations */
	UPROPERTY(EditDefaultsOnly, Category = Animation)
		FTwoAnimMontage ReloadAnim;

	/** equip sound */
	UPROPERTY(EditDefaultsOnly, Category = Sound)
		TSoftObjectPtr<USoundCue> EquipSound;

	DEFINE_SOFT_OBJECT_GETTER(USoundCue, EquipSound);

	/** equip animations */
	UPROPERTY(EditDefaultsOnly, Category = Animation)
		FTwoAnimMontage EquipAnim;

	/** fire animations */
	UPROPERTY(EditDefaultsOnly, Category = Animation)
		FTwoAnimMontage FireAnim;

	/** is muzzle FX looped? */
	UPROPERTY(EditDefaultsOnly, Category = Effects)
		uint32 bLoopedMuzzleFX : 1;

	/** is fire sound looped? */
	UPROPERTY(EditDefaultsOnly, Category = Sound)
		uint32 bLoopedFireSound : 1;

	/** is fire animation looped? */
	UPROPERTY(EditDefaultsOnly, Category = Animation)
		uint32 bLoopedFireAnim : 1;

	// * Offset if targeting
	UPROPERTY(BlueprintReadOnly, EditDefaultsOnly, Category = Animation)
		FTransform AnimOffsetTransform;

	// * Offset if not targeting
	UPROPERTY(BlueprintReadOnly, EditDefaultsOnly, Category = Animation)
		FTransform IdleOffsetTransform;

	// * Offset if targeting without module
	UPROPERTY(BlueprintReadOnly, EditDefaultsOnly, Category = Animation)
		FTransform DefaultOffsetTransform;

	UPROPERTY(BlueprintReadOnly, EditDefaultsOnly, Category = Animation)
		TSoftObjectPtr<UAnimMontage> WeaponMontageFire;
	DEFINE_SOFT_OBJECT_GETTER(UAnimMontage, WeaponMontageFire);



	UPROPERTY(BlueprintReadOnly, EditDefaultsOnly, Category = Animation)
		TSoftObjectPtr<UAnimMontage> WeaponMontageReload;
	DEFINE_SOFT_OBJECT_GETTER(UAnimMontage, WeaponMontageReload);

	UPROPERTY(BlueprintReadOnly, EditDefaultsOnly, Category = Animation)
		TSoftObjectPtr<UAnimMontage> PawnFPPMontageFire;
	DEFINE_SOFT_OBJECT_GETTER(UAnimMontage, PawnFPPMontageFire);

	UPROPERTY(BlueprintReadOnly, EditDefaultsOnly, Category = Animation)
		TSoftObjectPtr<UAnimMontage> PawnFPPMontageReload;
	DEFINE_SOFT_OBJECT_GETTER(UAnimMontage, PawnFPPMontageReload);

	UPROPERTY(BlueprintReadOnly, EditDefaultsOnly, Category = Animation)
		TSoftObjectPtr<UAnimMontage> PawnTPPMontageFire;
	DEFINE_SOFT_OBJECT_GETTER(UAnimMontage, PawnTPPMontageFire);

	UPROPERTY(BlueprintReadOnly, EditDefaultsOnly, Category = Animation)
		TSoftObjectPtr<UAnimMontage> PawnTPPMontageReload;
	DEFINE_SOFT_OBJECT_GETTER(UAnimMontage, PawnTPPMontageReload);

	UPROPERTY(BlueprintReadOnly, EditDefaultsOnly, Category = Animation)
		FTwoAnimSequence Default;

	UPROPERTY(BlueprintReadOnly, EditDefaultsOnly, Category = Animation)
		FTwoAnimSequence Walk;

	UPROPERTY(BlueprintReadOnly, EditDefaultsOnly, Category = Animation)
		FTwoAnimSequence Run;

	UPROPERTY(BlueprintReadOnly, EditDefaultsOnly, Category = Animation)
		FTwoAnimSequence Targeting;

	UPROPERTY(BlueprintReadOnly, EditDefaultsOnly, Category = Animation)
		FTwoAnimSequence WalkTargeting;

	UPROPERTY(BlueprintReadOnly, EditDefaultsOnly, Category = Animation)
		FTwoAnimSequence Jump;

	UPROPERTY(BlueprintReadOnly, EditDefaultsOnly, Category = Animation)
		FTwoAnimMontage FireInTargeting;

	UPROPERTY(BlueprintReadOnly, EditDefaultsOnly, Category = Fire)
		FName Name_InstantMuzzle;

	UPROPERTY(BlueprintReadOnly, EditDefaultsOnly, Category = Fire)
		FName Name_ProjMuzzle;

	UPROPERTY(BlueprintReadOnly, EditDefaultsOnly, Category = Achievement)
		int32 KillAchievementId;

	UPROPERTY(BlueprintReadOnly, EditDefaultsOnly, Category = Achievement)
		int32 SeriesKillAchievementId;
};