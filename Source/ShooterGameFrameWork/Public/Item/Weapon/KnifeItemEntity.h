// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "Item/BasicItemEntity.h"
#include "Engine/Animation/TwoAnimSequence.h"
#include "Sound/SoundCue.h"
#include "KnifeItemEntity.generated.h"


USTRUCT(BlueprintType, Blueprintable)
struct SHOOTERGAMEFRAMEWORK_API FKnifeItemProperty : public FBasicItemProperty
{
	GENERATED_USTRUCT_BODY()

	UPROPERTY(BlueprintReadOnly, EditDefaultsOnly, Category = Animation)		FTwoAnimMontage					FireAnimation;
	UPROPERTY(BlueprintReadOnly, EditDefaultsOnly, Category = Sound)			TSoftObjectPtr<USoundCue>		FireSound;

	DEFINE_SOFT_OBJECT_GETTER(USoundCue, FireSound);

	UPROPERTY(BlueprintReadOnly, EditDefaultsOnly, Category = Property)			float							Damage;
	UPROPERTY(BlueprintReadOnly, EditDefaultsOnly, Category = Property)			float							ActivationInterval;
	UPROPERTY(BlueprintReadOnly, EditDefaultsOnly, Category = Property)			TSubclassOf<UDamageType>		DamageType;
};

UCLASS()
class SHOOTERGAMEFRAMEWORK_API UKnifeItemEntity : public UBasicItemEntity
{
	GENERATED_BODY()

public:

	UKnifeItemEntity();

#pragma region Property
private:
	UPROPERTY(EditDefaultsOnly, meta = (ShowOnlyInnerProperties)) FKnifeItemProperty KnifeProperty;

public:
	const FBasicGameProperty& GetBasicProperty() const override final
	{
		return KnifeProperty;
	}

	FBasicGameProperty& GetBasicProperty() override final
	{
		return KnifeProperty;
	}

	UFUNCTION(BlueprintCallable)
	const FKnifeItemProperty& GetKnifeProperty() const
	{
		return GetItemProperty<FKnifeItemProperty>();
	}

	FORCEINLINE FKnifeItemProperty& GetKnifeProperty()
	{
		return GetItemProperty<FKnifeItemProperty>();
	}

	virtual float GetDefaultDynamicValue() const override;
#pragma endregion

	//================================================================================================================================

protected:

	UPROPERTY(BlueprintReadOnly, EditDefaultsOnly, Category = Mesh)		FMeshAnimationPair TPPMeshData;
	UPROPERTY(BlueprintReadOnly, EditDefaultsOnly, Category = Mesh)		FMeshAnimationPair FPPMeshData;


public:

	UFUNCTION(BlueprintCallable, BlueprintPure, Category = Mesh)
	FORCEINLINE FMeshAnimationPair GetTPPMeshData() const { return TPPMeshData; }

	UFUNCTION(BlueprintCallable, BlueprintPure, Category = Mesh)
	FORCEINLINE FMeshAnimationPair GetFPPMeshData() const { return FPPMeshData; }
};
