#pragma once
#include "BasicItemEntity.h"
#include "WeaponItemProperty.h"
#include "WeaponItemEntity.generated.h"


UCLASS()
class SHOOTERGAMEFRAMEWORK_API UWeaponItemEntity : public UBasicItemEntity
{
	GENERATED_UCLASS_BODY()

public:
	//================================================================================================================================
#pragma region Property
private:
	UPROPERTY(EditDefaultsOnly, meta = (ShowOnlyInnerProperties)) FWeaponItemProperty WeaponProperty;
#pragma region GetBasicProperty
public:
	const FBasicGameProperty& GetBasicProperty() const override final
	{
		return WeaponProperty;
	}

	float GetDefaultDynamicValue() const override final
	{
		return GetWeaponConfiguration().Damage;
	}

	FBasicGameProperty& GetBasicProperty() override final
	{
		return WeaponProperty;
	}

	UFUNCTION(BlueprintCallable)
	const FWeaponItemProperty& GetWeaponProperty() const
	{
		return GetItemProperty<FWeaponItemProperty>();
	}

	FORCEINLINE FWeaponItemProperty& GetWeaponProperty()
	{
		return GetItemProperty<FWeaponItemProperty>();
	}
#pragma endregion

private:
	UPROPERTY(EditDefaultsOnly, Category = Gameplay, meta = (ShowOnlyInnerProperties))
	FWeaponConfiguration WeaponConfiguration;

public:
	UFUNCTION(BlueprintCallable)
	const FWeaponConfiguration& GetWeaponConfiguration() const
	{
		return WeaponConfiguration;
	}

#pragma endregion
	//================================================================================================================================

protected:

	UPROPERTY(BlueprintReadOnly, EditDefaultsOnly, Category = Mesh)		FMeshAnimationPair TPPMeshData;
	UPROPERTY(BlueprintReadOnly, EditDefaultsOnly, Category = Mesh)		FMeshAnimationPair FPPMeshData;


public:

	UFUNCTION(BlueprintCallable, BlueprintPure, Category = Mesh)
	FORCEINLINE FMeshAnimationPair GetTPPMeshData() const { return TPPMeshData; }

	UFUNCTION(BlueprintCallable, BlueprintPure, Category = Mesh)
	FORCEINLINE FMeshAnimationPair GetFPPMeshData() const { return FPPMeshData; }
};