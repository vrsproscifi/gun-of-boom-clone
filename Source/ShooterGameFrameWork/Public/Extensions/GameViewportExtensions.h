#pragma once

#ifndef __GameViewportExtensions_H__
#define __GameViewportExtensions_H__

//=============================================
#include "Extensions/UObjectExtensions.h"

//=============================================
#include "Engine/GameViewportClient.h"

//=============================================
#include "Engine/Types/ExternMethod.h"

//=========================================
SHOOTERGAMEFRAMEWORK_API EXTERN UGameViewportClient* GetGameViewportClient();

//=========================================
template<typename TGameViewportClient>
EXTERN TGameViewportClient* GetGameViewportClient()
{
	return GetValidObjectAs<TGameViewportClient>(GetGameViewportClient());
}

#endif