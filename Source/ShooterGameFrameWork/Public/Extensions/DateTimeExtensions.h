#pragma once

#ifndef __FDateTimeExtensions_H__
#define __FDateTimeExtensions_H__

#include <Text.h>

struct SHOOTERGAMEFRAMEWORK_API FDateTimeExtensions
{
	static FText AsFormatedTimespan(const FTimespan& InTimespan);
};

#endif