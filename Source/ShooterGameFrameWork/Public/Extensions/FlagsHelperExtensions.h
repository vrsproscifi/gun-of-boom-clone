#pragma once


#ifndef __FFlagsHelper_H__
#define __FFlagsHelper_H__

template<typename TEnum>
static FORCEINLINE FString GetEnumValueToString(const FString& Name, TEnum Value)
{
	if (const UEnum* enumPtr = FindObject<UEnum>(ANY_PACKAGE, *Name, true))
	{
		return enumPtr->GetNameStringByIndex(static_cast<int32>(Value));
	}
	return FString("Invalid");
}

struct SHOOTERGAMEFRAMEWORK_API FFlagsHelper
{
	template<typename VariableType, typename FlagType>
	static FORCEINLINE bool HasAnyFlags(const VariableType Variable, const FlagType& InFlags)
	{
		return !!(Variable & VariableType(InFlags));
	}

	template<typename VariableType, typename FlagType>
	static FORCEINLINE bool HasAllFlags(const VariableType Variable, const FlagType& InFlags)
	{
		return ((Variable & InFlags) == InFlags);
	}

	template<typename VariableType, typename FlagType>
	static FORCEINLINE void SetFlags(VariableType& Variable, const FlagType& FlagsToSet)
	{
		Variable |= VariableType(FlagsToSet);
	}

	template<typename VariableType, typename FlagType>
	static FORCEINLINE void ToggleFlags(VariableType& Variable, const FlagType& FlagsToSet)
	{
		if (HasAnyFlags<VariableType, FlagType>(Variable, FlagsToSet))
		{
			ClearFlags<VariableType, FlagType>(Variable, FlagsToSet);
		}
		else
		{
			SetFlags<VariableType, FlagType>(Variable, Variable | FlagsToSet);
		}
	}

	template<typename VariableType, typename FlagType>
	static FORCEINLINE void ClearFlags(VariableType& Variable, const FlagType& FlagsToClear)
	{
		Variable &= ~VariableType(FlagsToClear);
	}

	template<typename VariableType>
	static FORCEINLINE VariableType ToFlag(const VariableType& Variable)
	{
		return 1 << Variable;
	}
};

#endif