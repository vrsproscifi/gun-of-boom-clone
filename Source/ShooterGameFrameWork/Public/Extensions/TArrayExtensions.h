#pragma once

#ifndef __TArrayExtensions_H__
#define __TArrayExtensions_H__

struct SHOOTERGAMEFRAMEWORK_API FArrayExtensions
{
	template <typename ElementType>
	static ElementType* TakeRandom(TArray<ElementType*>& InArray, const bool& AllowOverflow = false)
	{
		//===============================================
		if (InArray.Num())
		{
			auto Index = FMath::RandRange(0, InArray.Num() - static_cast<int32>(AllowOverflow));
			if (InArray.IsValidIndex(Index))
			{
				return InArray[Index];
			}
		}

		//===============================================
		return nullptr;
	}

	template <typename ElementType, typename Predicate>
	static ElementType* TakeRandomByPredicate(TArray<ElementType*>& InArray, Predicate Pred, const bool& AllowOverflow = false)
	{
		//===============================================
		TArray<ElementType*> Response = InArray.FilterByPredicate(Pred);

		//===============================================
		return TakeRandom(Response, AllowOverflow);
	}

	//-----------

	template <typename ElementType, typename Predicate>
	static const ElementType* TakeRandomByPredicate(const TArray<ElementType>& InArray, Predicate Pred, const bool& AllowOverflow = false)
	{
		//===============================================
		const TArray<ElementType> Response = InArray.FilterByPredicate(Pred);

		//===============================================
		return TakeRandom(Response, AllowOverflow);
	}

	template <typename ElementType, typename Predicate>
	static ElementType* TakeRandomByPredicate(TArray<ElementType>& InArray, Predicate Pred, const bool& AllowOverflow = false)
	{
		//===============================================
		TArray<ElementType> Response = InArray.FilterByPredicate(Pred);

		//===============================================
		return TakeRandom(Response, AllowOverflow);
	}

	template <typename ElementType>
	static ElementType* TakeRandom(TArray<ElementType>& InArray, const bool& AllowOverflow = false)
	{
		//===============================================
		ElementType* RESTRICT Data = InArray.GetData();

		//===============================================
		auto Index = FMath::RandRange(0, InArray.Num() - static_cast<int32>(AllowOverflow));
		if (Index >= InArray.Num())
		{
			return nullptr;
		}

		//===============================================
		return Data + Index;
	}

	template <typename ElementType>
	static const ElementType* TakeRandom(const TArray<ElementType>& InArray, const bool& AllowOverflow = false)
	{
		//===============================================
		const ElementType* RESTRICT Data = InArray.GetData();

		//===============================================
		auto Index = FMath::RandRange(0, InArray.Num() - static_cast<int32>(AllowOverflow));
		if (Index >= InArray.Num())
		{
			return nullptr;
		}

		//===============================================
		return Data + Index;
	}

};

#endif