#pragma once

#ifndef __FGameSingletonExtensions_H__
#define __FGameSingletonExtensions_H__
//=============================================
#include "Extensions/UObjectExtensions.h"
#include "Templates/Casts.h"

//=========================================
enum class EGameItemType : uint8;

//=========================================
class UShooterGameSingleton;
class UBasicGameAchievement;
class UBasicItemEntity;
class UDataAsset;
class UCaseEntity;

//=========================================
class ULevelAwardEntity;
class UEverydayAwardEntity;

//=========================================
struct SHOOTERGAMEFRAMEWORK_API FGameSingletonExtensions
{
	static int32 GameSingletonLoadAttemps;

	static UShooterGameSingleton* Get();
	static UCaseEntity * FindCaseById(const int32& InModelId);

	static bool IsGameSingletonAllowReload();
	static bool IsGameSingletonLoaded();
	static void ReloadGameSingleton();


	//===================================================
	static UBasicItemEntity * FindItemById(const int32& InModelId);

	template<typename T = UBasicItemEntity>
	static T* FindItemById(const int32& InModelId)
	{
		return GetValidObjectAs<T>(FindItemById(InModelId));
	}

	//===================================================
	static TArray<UBasicItemEntity*> FindItemsByType(const EGameItemType& InModelType);

	template<typename T = UBasicItemEntity>
	static TArray<T*> FindItemsByType(const EGameItemType& InModelType)
	{
		TArray<T*> ReturnData;
		const auto DataAssets = FindItemsByType(InModelType);
		for (auto TargetAsset : DataAssets)
		{
			if (auto CastedAsset = Cast<T>(TargetAsset))
			{
				ReturnData.Add(CastedAsset);
			}
		}
		return ReturnData;
	}

	template<typename Predicate>
	static TArray<UBasicItemEntity*> FindItemsByType(const EGameItemType& InModelType, Predicate Pred)
	{
		return FindItemsByType(InModelType).FilterByPredicate(Pred);
	}

	template<class T = UBasicItemEntity, typename Predicate>
	static TArray<T*> FindItemsByType(const EGameItemType& InModelType, Predicate Pred)
	{
		return FindItemsByType<T>(InModelType).FilterByPredicate(Pred);
	}


	//===================================================
	static UBasicGameAchievement * FindAchievementById(const int32& InModelId);

	//===================================================
	static const float* FindBonesDamageRate(const FName& InName);

	//===================================================

	static TSubclassOf<UObject> FindSubClassByName(const FName& InClassName);
	static TSubclassOf<UObject> FindSubClassByName(const FName& InClassName, UClass* InDefaultClass);

	//===================================================
	static ULevelAwardEntity* FindLevelAwardById(const int32& InLevel);
	static UEverydayAwardEntity* FindEverydayAwardById(const int32& InDayStep);

	//===================================================

	template<class T = UBasicItemEntity>
	static TArray<T*> GetItems()
	{
		TArray<T*> ReturnData;
		const auto DataAssets = GetItems();
		for (auto TargetAsset : DataAssets)
		{
			if (auto CastedAsset = Cast<T>(TargetAsset))
			{
				ReturnData.Add(CastedAsset);
			}
		}
		return ReturnData;
	}

	template<class T = UBasicItemEntity, typename Predicate>
	static TArray<T*> GetItemsAs(Predicate Pred)
	{
		TArray<T*> ReturnData;
		const auto DataAssets = GetItems(Pred);
		for (auto TargetAsset : DataAssets)
		{
			if (auto CastedAsset = GetValidObjectAs<T>(TargetAsset))
			{
				ReturnData.Add(CastedAsset);
			}
		}
		return ReturnData;
	}

	//===================================================

	template<typename Predicate>
	static TArray<UBasicItemEntity*> GetItems(Predicate Pred)
	{
		return GetItems().FilterByPredicate(Pred);
	}

	static const TArray<UBasicItemEntity*>& GetItems();

	//-------------------------------------------------
	static const TArray<UEverydayAwardEntity*>& GetEverydayAwards();

	//===================================================

	static const TArray<UDataAsset*>& GetDataAssets();

	template<class T = UDataAsset>
	static TArray<T*> GetDataAssets()
	{
		TArray<T*> ReturnData;
		const auto DataAssets = GetDataAssets();
		for (auto TargetAsset : DataAssets)
		{
			if (auto CastedAsset = GetValidObjectAs<T>(TargetAsset))
			{
				ReturnData.Add(CastedAsset);
			}
		}

		return ReturnData;
	}


	static void SetLastLocalToken(const FGuid& id);
	static FGuid GetLastLocalToken();

	static void SetLastLocalPlayerId(const FGuid& id);
	static FGuid GetLastLocalPlayerId();

};

typedef FGameSingletonExtensions FGameSingletonExtension;

#endif