#pragma once

#ifndef __SoftObjectExtensions_H__
#define __SoftObjectExtensions_H__

//===================================
#ifdef DEFINE_SOFT_OBJECT_GETTER
#undef DEFINE_SOFT_OBJECT_GETTER
#endif

//===================================
#define DEFINE_SOFT_OBJECT_GETTER(TObjectType, TObjectName) \
TObjectType* Get##TObjectName() \
{ \
	return GetOrLoadObject(TObjectName);\
}\


//===================================
template<typename TObject>
static TObject* GetOrLoadObject(const TSoftObjectPtr<TObject>& InSoftObject)
{
	return InSoftObject.LoadSynchronous();
}

template<typename TObject>
static TObject* GetOrLoadObject(TSoftObjectPtr<TObject>& InSoftObject)
{
	return InSoftObject.LoadSynchronous();
}


#endif