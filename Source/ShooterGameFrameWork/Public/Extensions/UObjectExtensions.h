#pragma once

#ifndef __UObjectExtensions_H__
#define __UObjectExtensions_H__

//-------------------------------------------------------
#include "UObject/WeakObjectPtrTemplates.h"
#include "UObject/WeakObjectPtr.h"
#include "Templates/SharedPointer.h"
#include "Templates/Casts.h"


//-------------------------------------------------------
#pragma region GetValidObject

/*
	A method for safe obtaining UObjects. 
	The object is checked for nullptr and for validity (IsValidLowLevel). 
	Sometimes a pointer can exist, but the object is already deleted / going to be deleted by the garbage collector. 
	Helps get rid of a large number of crashes when using a client-server.
*/
template<typename TObject>
TObject* GetValidObject(TObject* object)
{
	if (object && object->IsValidLowLevelFast() && object->IsValidLowLevel() && object->IsPendingKill() == false)
	{
		return object;
	}
	return nullptr;
}

template<typename TObject>
const TObject* GetValidObject(const TObject* object)
{
	if (object && object->IsValidLowLevelFast() && object->IsValidLowLevel() && object->IsPendingKill() == false)
	{
		return object;
	}
	return nullptr;
}

//=========================================

template<typename TObject = UObject>
bool IsValidObject(TObject* object)
{
	return GetValidObject(object) != nullptr;
}

template<typename TObject = UObject>
bool IsValidObject(const TObject* object)
{
	return GetValidObject(object) != nullptr;
}

//=========================================

template<typename TTo, typename TObject = UObject>
bool IsValidObjectAs(TObject* object)
{
	if (auto Object = GetValidObject(object))
	{
		return Object->IsA(TTo::StaticClass());
	}
	return false;
}

template<typename TTo, typename TObject = UObject>
bool IsValidObjectAs(const TObject* object)
{
	if (auto Object = GetValidObject(object))
	{
		return Object->IsA(TTo::StaticClass());
	}
	return false;
}

//=========================================

template<typename TObject>
TObject* GetValidObject(const TWeakObjectPtr<TObject>& object)
{
	if (IsValidObject(object))
	{
		return object.Get();
	}
	return nullptr;
}

template<typename TObject>
bool IsValidObject(const TWeakObjectPtr<TObject>& object)
{
	return object.IsValid() && object.Get() && object->IsValidLowLevelFast() && object->IsValidLowLevel() && object->IsPendingKill() == false;
}

template<typename TObject>
bool IsValidObject(const TSharedPtr<TObject>& object)
{
	return object.IsValid() && object->IsValidLowLevelFast() && object->IsValidLowLevel() && object->IsPendingKill() == false;
}



//bool IsValidObject(const TWeakObjectPtr<UObject>& object)
//{
//	return object.IsValid() && object.Get() && object->IsValidLowLevel();
//}

//template<typename TObject>
//const TWeakObjectPtr<TObject>& GetValidObject(const TWeakObjectPtr<TObject>& object)
//{
//	if (object.Get() && object->IsValidLowLevel() && object.IsValid())
//	{
//		return object;
//	}
//	return nullptr;
//}

/*
	A method for safe obtaining UObjects and cast to another child type.
	The object is checked for nullptr and for validity (IsValidLowLevel).
	Sometimes a pointer can exist, but the object is already deleted / going to be deleted by the garbage collector.
	Helps get rid of a large number of crashes when using a client-server.
*/
template <typename TTo, typename TObject = UObject>
TTo* GetValidObjectAs(TObject* object)
{
	if (IsValidObject(object))
	{
		return Cast<TTo>(object);
	}
	return nullptr;
}

template <typename TTo, typename TObject = UObject>
const TTo* GetValidObjectAs(const TObject* object)
{
	if (IsValidObject(object))
	{
		return Cast<TTo>(object);
	}
	return nullptr;
}

/*
	A method for safe obtaining UObjects and cast to another child type.
	The object is checked for nullptr and for validity (IsValidLowLevel).
	Sometimes a pointer can exist, but the object is already deleted / going to be deleted by the garbage collector.
	Helps get rid of a large number of crashes when using a client-server.
*/
template <typename TTo>
TTo* GetValidObjectAs(UObject* object)
{
	if (IsValidObject(object))
	{
		return Cast<TTo>(object);
	}
	return nullptr;
}

template <typename TTo, typename TObject = UObject>
TTo* GetValidObjectAs(const TWeakObjectPtr<TObject>& object)
{
	if (IsValidObject(object))
	{
		return Cast<TTo>(object.Get());
	}
	return nullptr;
}

template <typename TTo>
TTo* GetValidObjectAs(const TWeakObjectPtr<UObject>& object)
{
	if (IsValidObject(object))
	{
		return Cast<TTo>(object.Get());
	}
	return nullptr;
}

template <typename TTo>
const TTo* GetValidObjectAs(const UObject* object)
{
	if (IsValidObject(object))
	{
		return Cast<TTo>(object);
	}
	return nullptr;
}

#pragma endregion

//-------------------------------------------------------
//template <typename TObject>
//TObject* CreateDefultOrSubclass(const FName& name, const TSubclassOf<TObject>& ClassToCreateByDefault, bool bIsRequired = true, bool bAbstract = false, bool bIsTransient = false)
//{
//	if (ClassToCreateByDefault)
//	{
//		return Cast<TObject>(CreateDefaultSubobject(name, TObject::StaticClass(), ClassToCreateByDefault, bIsRequired, bAbstract, bIsTransient));
//	}
//	else
//	{
//		return CreateDefaultSubobject<TObject>(name, bIsTransient);
//	}
//}

#endif