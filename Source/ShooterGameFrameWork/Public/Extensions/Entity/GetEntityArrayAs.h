#pragma once

//=============================================
#ifndef __GetEntityArrayAs_H__
#define __GetEntityArrayAs_H__

//=============================================
#include "Entity/BasicGameEntity.h"

//=============================================
#include "Extensions/UObjectExtensions.h"
#include "Templates/Casts.h"

template<typename From, typename To>
static TArray<To> GetEntityArrayAs(const TArray<From>& InBasicGameEntities)
{
	//--------------------------------
	TArray<To> arr;

#ifdef _MSVC_LANG
	static_assert
		(
			std::is_base_of<From, To>::value,
			"T must be a descendant of UBasicGameEntity"
			);
#endif

	//--------------------------------
	for (auto e : InBasicGameEntities)
	{
		if (auto entity = GetValidObjectAs<To>(e))
		{
			arr.Add(entity);
		}
	}

	//--------------------------------
	return arr;
}

template<typename To>
static TArray<To> GetEntityArrayAs(const TArray<UBasicGameEntity>& InBasicGameEntities)
{
	return GetEntityArrayAs<UBasicGameEntity, To>(InBasicGameEntities);
}

#endif