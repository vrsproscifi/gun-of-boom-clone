#pragma once

//=============================================
#ifndef __FindEntityBy_H__
#define __FindEntityBy_H__

//=============================================
#include "Entity/BasicGameEntity.h"

//=============================================
#include "Extensions/UObjectExtensions.h"
#include "Templates/Casts.h"

template<class T>
static T* FindEntityById(const TArray<T*>& InArray, const int32& InModelId)
{
#ifdef _MSVC_LANG
	static_assert
	(
		std::is_base_of<UBasicGameEntity, T>::value,
		"T must be a descendant of UBasicGameEntity"
	);
#endif
	const auto result = InArray.FindByPredicate([&, InModelId](T* item)
	{
		if (auto i = GetValidObject(item))
		{
			return i->IsEqualModelId(InModelId);
		}
		return false;
	});

	if (result)
	{
		return GetValidObject(*result);
	}

	return nullptr;
}


template<class T>
static T* FindEntityByName(const TArray<T*>& InArray, const FString& InModelName)
{
#ifdef _MSVC_LANG
	static_assert
	(
		std::is_base_of<UBasicGameEntity, T>::value,
		"T must be a descendant of UBasicGameEntity"
	);
#endif

	const auto result = InArray.FindByPredicate([&, InModelName](T* item)
	{
		if (auto i = GetValidObject(item))
		{
			return i->IsEqualModelName(InModelName);
		}
		return false;
	});

	if (result)
	{
		return GetValidObject(*result);
	}

	return nullptr;
}


#endif