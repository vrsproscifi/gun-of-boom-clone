#include "DateTimeExtensions.h"
#include <Internationalization.h>

FText FDateTimeExtensions::AsFormatedTimespan(const FTimespan& InTimespan)
{
	const double TotalDays = InTimespan.GetTotalDays();
	const int64 Days = static_cast<int64>(TotalDays);

	//FNumberFormattingOptions NumberFormattingOptions;
	//NumberFormattingOptions.MinimumIntegralDigits = 2;
	//NumberFormattingOptions.MaximumIntegralDigits = 2;

	if (Days > 0)
	{
		const int64 Hours = InTimespan.GetHours();

		static const FText TimespanFormatPattern = NSLOCTEXT("Timespan", "Format_DaysMinutesEx", "{Days}d {Time}");
		FFormatNamedArguments TimeArguments;

		TimeArguments.Add(TEXT("Days"), Days);
		TimeArguments.Add(TEXT("Time"), FText::FromString(InTimespan.ToString(TEXT("%h:%m:%s")).RightChop(1)));

		return FText::Format(TimespanFormatPattern, TimeArguments);
	}

	return FText::FromString(InTimespan.ToString(TEXT("%h:%m:%s")).RightChop(1));

	//else
	//{
	//	const double TotalHours = InTimespan.GetTotalHours();
	//	const int64 Hours = static_cast<int64>(TotalHours);
	//	const int64 Minutes = InTimespan.GetMinutes();

	//	if (Hours > 0)
	//	{
	//		static const FText TimespanFormatPattern = NSLOCTEXT("Timespan", "Format_HoursMinutesEx",
	//		                                                     "{Hours}h {Minutes}m");
	//		FFormatNamedArguments TimeArguments;
	//		TimeArguments.Add(TEXT("Hours"), Hours);
	//		TimeArguments.Add(TEXT("Minutes"), FText::AsNumber(Minutes, &NumberFormattingOptions, nullptr));

	//		return FText::Format(TimespanFormatPattern, TimeArguments);
	//	}
	//	else
	//	{
	//		const int64 Seconds = InTimespan.GetSeconds();

	//		static const FText TimespanFormatPattern = NSLOCTEXT("Timespan", "Format_MinutesSecondsEx",
	//		                                                     "{Minutes}m {Seconds}s");

	//		FFormatNamedArguments TimeArguments;
	//		TimeArguments.Add(TEXT("Minutes"), Minutes);
	//		TimeArguments.Add(TEXT("Seconds"), FText::AsNumber(Seconds, &NumberFormattingOptions, nullptr));
	//		return FText::Format(TimespanFormatPattern, TimeArguments);
	//	}
	//}
}
