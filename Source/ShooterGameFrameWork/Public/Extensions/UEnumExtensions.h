#pragma once

#ifndef __UEnumExtensions_H__
#define __UEnumExtensions_H__


template<typename TEnum>
static FString GetEnumValueAsString(const FString& Name, TEnum Value)
{
	const UEnum* enumPtr = FindObject<UEnum>(ANY_PACKAGE, *Name, true);
	if (!enumPtr)
	{
		return FString("Invalid");
	}

	auto index = enumPtr->GetIndexByValue((uint8)Value);
	return enumPtr->GetNameStringByIndex(index);
}


template <typename EnumType>
static EnumType GetEnumValueFromString(const FString& EnumName, const FString& String)
{
	UEnum* Enum = FindObject<UEnum>(ANY_PACKAGE, *EnumName, true);
	if (!Enum)
	{
		return EnumType(0);
	}
#if ENGINE_MAJOR_VERSION >= 4 && ENGINE_MINOR_VERSION >= 16
#if ENGINE_MAJOR_VERSION >= 4 && ENGINE_MINOR_VERSION >= 18
	return (EnumType)Enum->GetIndexByName(FName(*String), EGetByNameFlags::CaseSensitive);
#else
	return (EnumType)Enum->GetIndexByName(FName(*String), true);
#endif
#else
	return (EnumType)Enum->FindEnumIndex(FName(*String));
#endif
}

#endif