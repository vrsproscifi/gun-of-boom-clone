#pragma once

//===================================
#include "SlateCore/Public/Styling/SlateBrush.h"

//===================================
#include "BasicGameProperty.h"

//===================================
#include "CaseItemProperty.h"

//===================================
#include "CaseProperty.generated.h"

//===================================
UENUM(BlueprintType, Blueprintable)
enum class EGameCaseType : uint8
{
	None,
	ForSale,
	EveryDay,
	EveryDayAds,
	EveryDayAward,
};
//===================================

USTRUCT(BlueprintType, Blueprintable)
struct SHOOTERGAMEFRAMEWORK_API FCaseProperty : public FBasicGameProperty
{
	GENERATED_USTRUCT_BODY()

	UPROPERTY(VisibleAnywhere, Transient)	FTimespan Delay;
	UPROPERTY(VisibleAnywhere, Transient)	TArray<FCaseItemProperty> Items;

	UPROPERTY(EditDefaultsOnly)				FSlateBrush Image;
	UPROPERTY(EditDefaultsOnly)				bool bAllowDisplayOnHomeScreen;
	UPROPERTY(EditDefaultsOnly)				bool bAllowDisplayInAvalibleCases;

	UPROPERTY(EditDefaultsOnly)				int32 DisplayPositionOnHomeScreen;
	UPROPERTY(EditDefaultsOnly)				int32 DisplayPositionInAvalibleCases;

	UPROPERTY(EditDefaultsOnly)				EGameCaseType CaseType;
	UPROPERTY(VisibleAnywhere, Transient)	bool bAllowGetBonusCase;


	FCaseProperty()
		: bAllowDisplayOnHomeScreen(false)
		, bAllowDisplayInAvalibleCases(false)
		, DisplayPositionOnHomeScreen(-1)
		, DisplayPositionInAvalibleCases(-1)
		, CaseType(EGameCaseType::None)
	{
		
	}
	
	bool IsEveryDayCase() const
	{
		return !Delay.IsZero();
	}

};