#pragma once

//=========================================
#include "CaseItemDropMethod.h"

//=========================================
#include "CaseContainer.generated.h"

USTRUCT()
struct SHOOTERGAMEFRAMEWORK_API FCaseItemContainer
{
	GENERATED_USTRUCT_BODY()

	FCaseItemContainer()
		: EntityId(0)
		, ItemModelId(0)
		, RandomMin(0)
		, RandomMax(0)
		, Chance(0)
		, DropMethod(ECaseItemDropMethod::None)
	{

	}

	UPROPERTY()		uint32 EntityId;
	UPROPERTY()		uint32 ItemModelId;
	UPROPERTY()		uint32 RandomMin;
	UPROPERTY()		uint32 RandomMax;
	UPROPERTY()		uint32 Chance;
	UPROPERTY()		ECaseItemDropMethod DropMethod;
};

USTRUCT()
struct SHOOTERGAMEFRAMEWORK_API FCaseContainer
{
	GENERATED_USTRUCT_BODY()

	FCaseContainer()
		: EntityId(0)
		, Dalay(0)
		, IsAllowGetBonusCase(false)
	{
		
	}

	UPROPERTY()		uint32 EntityId;
	UPROPERTY()		uint32 Dalay;
	UPROPERTY()		bool IsAllowGetBonusCase;
	UPROPERTY()		TArray<FCaseItemContainer> Items;

};