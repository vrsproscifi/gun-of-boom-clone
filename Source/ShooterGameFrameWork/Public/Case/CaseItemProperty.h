#pragma once

//=========================================
#include "CaseItemDropMethod.h"

//===================================
#include "CaseItemProperty.generated.h"


//===================================
class UBasicItemEntity;



//===================================
USTRUCT(BlueprintType, Blueprintable)
struct SHOOTERGAMEFRAMEWORK_API FCaseItemProperty 
{
	GENERATED_USTRUCT_BODY()

	UPROPERTY(VisibleAnywhere, Transient)				UBasicItemEntity* ModelEntity;

	UPROPERTY(VisibleAnywhere, Transient)				int32 ModelId;
	UPROPERTY(VisibleAnywhere, Transient)				int32 Amount;

	UPROPERTY(VisibleAnywhere, Transient)				int32 RandomMin;
	UPROPERTY(VisibleAnywhere, Transient)				int32 RandomMax;

	UPROPERTY(VisibleAnywhere, Transient)				ECaseItemDropMethod DropMethod;
};