#pragma once
//==============================
#include "BasicGameEntity.h"

//==============================
#include "CaseProperty.h"

//==============================
#include "CaseEntity.generated.h"

//==============================
struct FCaseContainer;

//==============================
/*
* Иформация о кейсе, т.к один кейс может использоваться в нескольких товарах
*/
UCLASS()
class SHOOTERGAMEFRAMEWORK_API UCaseEntity : public UBasicGameEntity
{
	GENERATED_UCLASS_BODY()

public:
	void Initialize(const FCaseContainer& initializer);

	//================================================================================================================================
#pragma region Property
private:
	UPROPERTY(EditDefaultsOnly, meta = (ShowOnlyInnerProperties)) FCaseProperty CaseProperty;
#pragma region GetBasicProperty
public:
	const FBasicGameProperty& GetBasicProperty() const override final
	{
		return CaseProperty;
	}

	FBasicGameProperty& GetBasicProperty() override final
	{
		return CaseProperty;
	}

	UFUNCTION(BlueprintCallable)
		const FCaseProperty& GetCaseProperty() const
	{
		return CaseProperty;
	}

	FORCEINLINE FCaseProperty& GetCaseProperty()
	{
		return CaseProperty;
	}

	UFUNCTION(BlueprintCallable)
	const TArray<FCaseItemProperty>& GetDropedItems() const
	{
		return GetCaseProperty().Items;
	}

	const TArray<FCaseItemProperty>& GetDropedItems() 
	{
		return GetCaseProperty().Items;
	}
#pragma endregion
#pragma endregion
};