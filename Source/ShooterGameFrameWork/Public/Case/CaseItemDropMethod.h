#pragma once

//===================================
#include "CaseItemDropMethod.generated.h"


//===================================
UENUM(BlueprintType, Blueprintable)
enum class ECaseItemDropMethod : uint8
{
	None,
	Random,
	Guaranteed
};
