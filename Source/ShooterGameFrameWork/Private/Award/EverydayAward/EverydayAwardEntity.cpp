
#include "EverydayAwardEntity.h"
#include "GameSingletonExtensions.h"


UEverydayAwardEntity::UEverydayAwardEntity(const FObjectInitializer& ObjectInitializer)
{

}

UCaseEntity* FEverydayAwardProperty::GetCaseInstance()
{
	if (Instance == nullptr)
	{
		Instance = FGameSingletonExtension::FindCaseById(RewardCaseId);
	}
	return Instance;
}

UCaseEntity* FEverydayAwardProperty::GetCaseInstance() const
{
	if (Instance == nullptr)
	{
		Instance = FGameSingletonExtension::FindCaseById(RewardCaseId);
	}
	return Instance;
}

void UEverydayAwardEntity::UpdateEditorHelperProperties()
{
	Super::UpdateEditorHelperProperties();

#if WITH_EDITOR && WITH_METADATA
	RewardCaseId = GetEverydayAwardProperty().RewardCaseId;
#endif
}

void UEverydayAwardEntity::Initialize(const FEverydayAwardContainer & initializer)
{
	//==================================
	GetEverydayAwardProperty().RewardCaseId = initializer.RewardCaseId;

	//==================================
	RewardCaseId = initializer.RewardCaseId;

	//==================================
	//	Call for first init
	GetEverydayAwardProperty().GetCaseInstance();

}
