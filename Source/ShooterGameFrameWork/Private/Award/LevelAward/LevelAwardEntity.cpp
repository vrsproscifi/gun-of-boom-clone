
#include "LevelAwardEntity.h"
#include "Item/BasicItemEntity.h"
#include "GameSingletonExtensions.h"


ULevelAwardEntity::ULevelAwardEntity(const FObjectInitializer& ObjectInitializer)
{

}

void ULevelAwardEntity::Initialize(const FLevelAwardContainer & initializer)
{
	//================================================================
	LevelAwardProperty.RewardItems.Empty(initializer.Reward.Items.Num() + 1);

	//================================================================
	LevelAwardProperty.RewardDonate = initializer.Reward.Donate;
	LevelAwardProperty.RewardMoney = initializer.Reward.Money;

	//================================================================
	for (const auto& reward : initializer.Reward.Items)
	{
		LevelAwardProperty.RewardItems.Add(FLevelRewardItem(reward.ModelId, reward.Amount));
	}
}

TArray<UBasicItemEntity*> ULevelAwardEntity::GetNewAvalibleItems() const
{
	return FGameSingletonExtension::GetItems([Level = LevelAwardProperty.ModelId](const UBasicItemEntity* InItem)
	{
		return InItem && InItem->GetItemRequiredLevel() == Level;
	});
}

const TArray<FLevelRewardItem>& ULevelAwardEntity::GetRewardItems() const
{
	return GetLevelAwardProperty().RewardItems;
}

FLevelRewardItem::FLevelRewardItem(const int32& InModelId, const int32& InAmount)
{
	//==============================
	ModelId = InModelId;
	Amount = InAmount;

	//==============================
	Instance = FGameSingletonExtension::FindItemById(InModelId);
}


UBasicItemEntity* FLevelRewardItem::GetInstance()
{
	if (Instance == nullptr)
	{
		Instance = FGameSingletonExtension::FindItemById(ModelId);
	}
	return Instance;
}

UBasicItemEntity* FLevelRewardItem::GetInstance() const
{
	if (Instance == nullptr)
	{
		Instance = FGameSingletonExtension::FindItemById(ModelId);
	}
	return Instance;
}