//==============================
#include "BasicGamePlayerItem.h"


//==============================
#include "GameSingletonExtensions.h"
#include "UnrealNetwork.h"
#include "BasicPlayerCharacter.h"
#include "BasicItemEntity.h"
#include "BasicPlayerState.h"
#include "Engine/World.h"

UBasicGamePlayerItem::UBasicGamePlayerItem(const FObjectInitializer& ObjectInitializer)
	: Super(ObjectInitializer)
{
}

//--------------------------------------------------------------------

UWorld* UBasicGamePlayerItem::GetValidWorld()
{
	return GetValidObject(GetWorld());
}

UWorld* UBasicGamePlayerItem::GetValidWorld() const
{
	return GetValidObject(GetWorld());
}

void UBasicGamePlayerItem::InitializeInstance(UBasicPlayerItem* InInstance)
{
	Instance = GetValidObject(InInstance);
	if (Instance)
	{
		InitializeInstanceDynamic(InInstance->GetEntityBase(), true);
	}
}

UBasicPlayerItem* UBasicGamePlayerItem::GetBaseInstance() const
{
	return GetValidObject(Instance);
}

UBasicPlayerItem* UBasicGamePlayerItem::GetBaseInstance()
{
	return GetValidObject(Instance);
}


//--------------------------------------------------------------------

FString UBasicGamePlayerItem::GetPlayerName() const
{
	if (auto ps = GetBasicPlayerState())
	{
		return ps->GetPlayerName();
	}
	return FString();
}

ABasicPlayerCharacter* UBasicGamePlayerItem::GetBasicCharacter() const
{
	return GetValidObjectAs<ABasicPlayerCharacter>(GetOwner());
}

ABasicPlayerCharacter* UBasicGamePlayerItem::GetBasicCharacter()
{
	return GetValidObjectAs<ABasicPlayerCharacter>(GetOwner());
}

//--------------------------------------------------------------------

ABasicPlayerState* UBasicGamePlayerItem::GetBasicPlayerState() const
{
	if(const auto character = GetBasicCharacter())
	{
		return GetValidObjectAs<ABasicPlayerState>(character->GetPlayerState());
	}
	return nullptr;
}

ABasicPlayerState* UBasicGamePlayerItem::GetBasicPlayerState()
{
	if (const auto character = GetBasicCharacter())
	{
		return GetValidObjectAs<ABasicPlayerState>(character->GetPlayerState());
	}
	return nullptr;
}

FTimerManager* UBasicGamePlayerItem::GetTimerManager() const
{
	if (const auto character = GetBasicCharacter())
	{
		return character->GetTimerManager();
	}
	else if (auto w = GetValidWorld())
	{
		return &w->GetTimerManager();
	}
	return nullptr;
}

FTimerManager* UBasicGamePlayerItem::GetTimerManager() 
{
	if (const auto character = GetBasicCharacter())
	{
		return character->GetTimerManager();
	}
	else if (auto w = GetValidWorld())
	{
		return &w->GetTimerManager();
	}
	return nullptr;
}



void UBasicGamePlayerItem::GetLifetimeReplicatedProps(TArray<FLifetimeProperty>& OutLifetimeProps) const
{
	Super::GetLifetimeReplicatedProps(OutLifetimeProps);
	
	DOREPLIFETIME_CONDITION(UBasicGamePlayerItem, Instance, COND_OwnerOnly);
}