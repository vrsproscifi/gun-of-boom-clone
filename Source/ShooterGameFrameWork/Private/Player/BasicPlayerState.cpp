//=============================================
#include "BasicPlayerState.h"
#include "BasicPlayerCharacter.h"
#include "BasicPlayerController.h"

//=============================================
#include "Extensions/GameSingletonExtensions.h"
#include "Extensions/GameViewportExtensions.h"

//=============================================
#include "UnrealNetwork.h"

//=============================================
#include "TimerManager.h"

//=============================================
#include "Game/BasicGameAchievement.h"

//=============================================
ABasicPlayerState::ABasicPlayerState(const FObjectInitializer& ObjectInitializer)
	: bIsAllowUserInventory(true)
{
}

void ABasicPlayerState::Reset()
{
	Achievements.Reset(32);
	Super::Reset();
}


void ABasicPlayerState::OnFullyLoadedData()
{

}

int32 ABasicPlayerState::GetPlayerLevel() const
{
	return -1;
}

ABasicPlayerCharacter* ABasicPlayerState::GetBaseCharacter() const
{
	if (const auto MyController = GetValidObjectAs<AController>(GetOwner()))
	{
		return GetValidObjectAs<ABasicPlayerCharacter>(MyController->GetCharacter());
	}

	return nullptr;
}

ABasicPlayerController* ABasicPlayerState::GetBaseController()
{
	return GetValidObjectAs<ABasicPlayerController>(GetOwner());
}


bool ABasicPlayerState::SetIdentityTokenString(const FString& InTokenString)
{
	FGuid localTargetToken;
	if (FGuid::Parse(InTokenString, localTargetToken))
	{
		SetIdentityToken(localTargetToken);
		return true;
	}

	return false;
}

void ABasicPlayerState::SetIdentityToken(const FGuid& InToken)
{
	IdentityToken = InToken;

	if (HasLocalClientState())
	{
		FGameSingletonExtensions::SetLastLocalToken(IdentityToken);
		UE_LOG(LogInit, Log, TEXT("ABasePlayerState::SetIdentityToken >> LastLocalToken = %s"), *InToken.ToString());
	}
}

bool ABasicPlayerState::PlayRewardVideo(const FPlayRewardedVideoRequest& InVideoRequest)const
{
	return false;
}

void ABasicPlayerState::OnPlayRewardVideoSuccess(const FPlayRewardedVideoResponse& InResponse)
{
}

void ABasicPlayerState::OnPlayRewardVideoClosed(const FPlayRewardedVideoResponse& InResponse)
{
}

typedef APlayerController OnlyCastAPlayerController;

bool ABasicPlayerState::HasLocalClientState() const
{
	//	Cast вместо GetValidObject, т.к сздесь это можно и нужно!
	auto MyCtrl = Cast<OnlyCastAPlayerController>(GetOwner());
	if (MyCtrl && MyCtrl->IsValidLowLevel() && MyCtrl->IsLocalController())
	{
		if (GetNetMode() == NM_Client || GetNetMode() == NM_ListenServer || GetNetMode() == NM_Standalone)
		{
			return true;
		}
	}

	return false;
}


void ABasicPlayerState::PostInitializeComponents()
{
	Super::PostInitializeComponents();

	if (HasLocalClientState())
	{
		OnCreateUserInterface();
	}
}

void ABasicPlayerState::ClientInitialize(AController* C)
{
	Super::ClientInitialize(C);

	if (HasLocalClientState())
	{
		OnCreateUserInterface();
	}
}


void ABasicPlayerState::BeginPlay()
{
	Super::BeginPlay();
}

void ABasicPlayerState::OnReactivated()
{
	Super::OnReactivated();

	//	���� ������� �� ����
	//#if UE_SERVER
	//	GetIdentityComponent()->SendRequestExist();
	//#endif
}

FTimerManager* ABasicPlayerState::GetTimerManager() const
{
	if (IsValidObject(this))
	{
		if (auto w = GetValidWorld())
		{
			return &w->GetTimerManager();
		}
	}
	return nullptr;
}

FTimerManager* ABasicPlayerState::GetTimerManager()
{
	if (IsValidObject(this))
	{
		if (auto w = GetValidWorld())
		{
			return &w->GetTimerManager();
		}
	}
	return nullptr;
}

void ABasicPlayerState::EndPlay(const EEndPlayReason::Type EndPlayReason)
{
	if (auto tm = GetTimerManager())
	{
		tm->ClearAllTimersForObject(this);
	}

	Super::EndPlay(EndPlayReason);
}

void ABasicPlayerState::CopyProperties(APlayerState* PlayerState)
{
	Super::CopyProperties(PlayerState);

	ABasicPlayerState* PS = GetValidObjectAs<ABasicPlayerState>(PlayerState);
	if (PS)
	{
		PS->MemberId = MemberId;
		PS->Achievements = Achievements;
	}
}

void ABasicPlayerState::OnCreateUserInterface()
{
#if !UE_SERVER
	//if (HasLocalClientState())
	//{
	//	SFriendsCommands::GetAlt().SetPlayerState(this);
	//}
#endif
}

void ABasicPlayerState::OnInitializeCharacter()
{
	
}

bool ABasicPlayerState::ContainsAchievement(const int32& InAchievementId) const
{
	return GetAchievementById(InAchievementId) != nullptr;
}

FPlayerGameAchievement* ABasicPlayerState::GetAchievementById(const int32& InAchievementId)
{
	//====================================================
	const auto achievement = Achievements.FindByPredicate([&, InAchievementId](const FPlayerGameAchievement& a)
	{
		return a.Key == InAchievementId;
	});

	return achievement;
}

const FPlayerGameAchievement* ABasicPlayerState::GetAchievementById(const int32& InAchievementId) const
{
	//====================================================
	const auto achievement = Achievements.FindByPredicate([&, InAchievementId](const FPlayerGameAchievement& a)
	{
		return a.Key == InAchievementId;
	});

	return achievement;
}

int32 ABasicPlayerState::GetAchievementValueById(const int32& InAchievementId, const int32& InDefault) const
{
	const auto achievement = GetAchievementById(InAchievementId);
	if (achievement)
	{
		return achievement->Value;
	}
	return InDefault;
}

bool ABasicPlayerState::GetAchievementById(const int32& InAchievementId, FPlayerGameAchievement& OutAchievement)
{
	const auto achievement = GetAchievementById(InAchievementId);
	if (achievement)
	{
		OutAchievement = *achievement;
		return true;
	}
	return false;
}


int32 ABasicPlayerState::GivePlayerAchievement(const int32& InAchievementId, const int32& InAmount, const int32& InScore )
{
	int32 ScorePoints = 0;
	//====================================================
	auto achievement = GetAchievementById(InAchievementId);

	//====================================================
	const auto entity = FGameSingletonExtensions::FindAchievementById(InAchievementId);

	//====================================================
	if (achievement == nullptr)
	{
		Achievements.Add(FPlayerGameAchievement(InAchievementId, 0));
		achievement = GetAchievementById(InAchievementId);
	}

	//-------------------------------------------------
	checkf(achievement, TEXT("GivePlayerAchievement[%d][InAmount: %d][InScore: %d] was nullptr"), InAchievementId, InAmount, InScore);
	
	//-------------------------------------------------
	achievement->LastValue = achievement->Value;

	//-------------------------------------------------
	if (entity)
	{
		const auto& Property = entity->GetAchievementProperty();
		if (Property.bAllowOverride)
		{
			ScorePoints = InScore;
		}
		else
		{
			ScorePoints = Property.ScorePoints;
		}
	}

	//-------------------------------------------------
	//UE_LOG(LogInit, Error, TEXT("[ABasicPlayerState::GivePlayerAchievement][%s][InAchievementId: %d / InAmount: %d][Value %d -> %d]"), *GetPlayerName(), InAchievementId, InAmount, achievement->Value, achievement->Value + InAmount);

	//-------------------------------------------------
	achievement->Value += InAmount;
	
	//=====================================
	if (entity == nullptr)
	{
		UE_LOG(LogInit, Error, TEXT("[ABasicPlayerState::GivePlayerAchievement][InAchievementId: %d / InAmount: %d][Entity was nullptr]"), InAchievementId, InAmount);
		return 0;
	}

	//=====================================
	NotifyPlayerAchievement(InAchievementId, achievement->Value, InAmount, InScore);

	//=====================================
	return ScorePoints;
}

void ABasicPlayerState::NotifyPlayerAchievement_Implementation(const int32& InAchievementId, const int32& InTotalAmount, const int32& InAmount, const int32& InScore )
{

}


void ABasicPlayerState::GetLifetimeReplicatedProps(TArray<FLifetimeProperty>& OutLifetimeProps) const
{
	Super::GetLifetimeReplicatedProps(OutLifetimeProps);
	DOREPLIFETIME(ABasicPlayerState, Achievements);
}