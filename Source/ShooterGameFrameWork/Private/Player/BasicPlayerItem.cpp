//==============================
#include "BasicPlayerItem.h"

//==============================
#include "BasicItemEntity.h"

//==============================
#include "GameSingletonExtensions.h"
#include "UnrealNetwork.h"

UBasicPlayerItem::UBasicPlayerItem(const FObjectInitializer& ObjectInitializer)
	: Super(ObjectInitializer)
{
}


//==============================
USkeletalMesh* UBasicPlayerItem::GetPreviewMesh() const
{
	if (const auto e = GetEntity<UBasicItemEntity>())
	{
		return GetValidObject(e->GetPreviewMeshData().Mesh);
	}
	return nullptr;
}

void UBasicPlayerItem::InitializeInstanceDynamic(UBasicItemEntity* InInstance, const bool InIsReplicated)
{
	SetItemEntity(InInstance);

	if (IsRegistered() == false)
	{
		RegisterComponent();
	}

	SetIsReplicated(InIsReplicated);
}

//--------------------------------------------------------------------

const FBasicItemProperty* UBasicPlayerItem::GetBaseItemProperty() const
{
	if (const auto entity = GetEntity<UBasicItemEntity>())
	{
		return &entity->GetItemProperty();
	}
	return nullptr;
}

FBasicItemProperty* UBasicPlayerItem::GetBaseItemProperty()
{
	if (auto entity = GetEntity<UBasicItemEntity>())
	{
		return &entity->GetItemProperty();
	}
	return nullptr;
}

const FGameItemCost UBasicPlayerItem::GetItemCost() const
{
	if(const auto prop = GetBaseItemProperty())
	{
		return prop->Cost;
	}

	return FGameItemCost();
}

void UBasicPlayerItem::SetItemEntity(UBasicItemEntity* entity)
{
	SetEntity(entity);
}


bool UBasicPlayerItem::HasAllowSomeBought() const
{
	if (const auto entity = GetEntity<UBasicItemEntity>())
	{
		return entity->HasAllowSomeBought();
	}
	return false;
}

bool UBasicPlayerItem::HasIgnoreRequirements() const
{
	if (const auto entity = GetEntity<UBasicItemEntity>())
	{
		return entity->HasIgnoreRequirements();
	}
	return false;
}

void UBasicPlayerItem::SetState(const TEnumAsByte<EPlayerItemState::Type>& InState)
{
	State = InState;
}

TEnumAsByte<EPlayerItemState::Type> UBasicPlayerItem::GetState() const
{
	return State;
}

UBasicItemEntity* UBasicPlayerItem::GetEntityBase() const
{
	return GetValidObjectAs<UBasicItemEntity>(EntityInstance);
}

EGameItemType UBasicPlayerItem::GetItemType() const
{
	if(const auto entity = GetEntityBase())
	{
		return entity->GetItemType();
	}
	return EGameItemType::None;
}

FString UBasicPlayerItem::GetEntityName(const UBasicPlayerItem* InItem)
{
	if (InItem == nullptr)
	{
		return "nullptr";
	}
	return UBasicItemEntity::GetEntityName(InItem->GetEntityBase());
}

void UBasicPlayerItem::OnRep_Instance()
{
	auto FoundEntity = FGameSingletonExtensions::FindItemById(EntityModel);
	EntityInstance = FoundEntity;
}

float UBasicPlayerItem::GetMass() const
{
	float CalculatedMass = 10.0f;

	if (const auto e = GetEntity<UBasicItemEntity>())
	{
		CalculatedMass += e->GetMass();
	}

	return CalculatedMass;
}

bool UBasicPlayerItem::IsMaximumLevelReached()  const
{
	return GetNextLevel() > GetMaxLevel();
}

uint8 UBasicPlayerItem::GetMaxLevel() const
{
	if (const auto e = GetEntity<UBasicItemEntity>())
	{
		return e->GetItemProperty().Modifications.Num();
	}

	return GetLevel();
}

void UBasicPlayerItem::IncLevel()
{
	Level++;
}

void UBasicPlayerItem::SetLevel(const int32& InLevel)
{
	Level = static_cast<uint8>(InLevel);
}

uint8 UBasicPlayerItem::GetNextLevel() const
{
	return GetLevel() + 1;
}

const uint8& UBasicPlayerItem::GetLevel() const
{
	return Level;
}

uint8 UBasicPlayerItem::GetRequiredLevel() const
{
	if (const auto e = GetEntity<UBasicItemEntity>())
	{
		return e->GetItemProperty().Level;
	}

	return 1;
}

void UBasicPlayerItem::SetAmount(const int32& InAmount)
{
	UE_LOG(LogInit, Error, TEXT("[UBasicPlayerItem][SetAmount[%d]: %d -> %d][desc: %s][Entity: %s]"), GetModelId(), Amount, InAmount, *UBasicPlayerItem::GetEntityName(this), *GetEntityId().ToString());
	Amount = InAmount;
}

bool UBasicPlayerItem::AnyAmount() const
{
	return Amount > 0;
}

void UBasicPlayerItem::UseItem()
{
	SetAmount(GetAmount() - 1);
}

const int32& UBasicPlayerItem::GetAmount() const
{
	return Amount;
}

void UBasicPlayerItem::GetLifetimeReplicatedProps(TArray<FLifetimeProperty>& OutLifetimeProps) const
{
	Super::GetLifetimeReplicatedProps(OutLifetimeProps);

	DOREPLIFETIME_CONDITION(UBasicPlayerItem, Amount, COND_OwnerOnly);
	DOREPLIFETIME_CONDITION(UBasicPlayerItem, State, COND_OwnerOnly);
	DOREPLIFETIME_CONDITION(UBasicPlayerItem, Level, COND_OwnerOnly);
}