
//=============================================
#include "BasicPlayerController.h"
#include "BasicPlayerCharacter.h"
#include "BasicPlayerState.h"
#include "BasicGameState.h"
#include "BasicGameMode.h"

//=============================================
#include <OnlineSubsystemTypes.h>

//=============================================
#include <Engine/LocalPlayer.h>
#include <Engine/World.h>

//=============================================
#include "TimerManager.h"

ABasicPlayerController::ABasicPlayerController(const FObjectInitializer& ObjectInitializer)
{
}

UWorld* ABasicPlayerController::GetValidWorld() const
{
	return GetValidObject(GetWorld());
}

UWorld* ABasicPlayerController::GetValidWorld()
{
	return GetValidObject(GetWorld());
}

void ABasicPlayerController::SetupInputComponent()
{
	Super::SetupInputComponent();

	InputComponent->BindAction("InGameMenu", IE_Pressed, this, &ABasicPlayerController::OnEscape);
}

void ABasicPlayerController::OnEscape()
{
	
}

void ABasicPlayerController::FailedToSpawnPawn()
{
	Super::FailedToSpawnPawn();

	if (auto gm = GetBaseGameMode())
	{
		gm->RestartPlayer(this);
	}
}


void ABasicPlayerController::PlayerTravelToLobby()
{
	ClientTravel(TEXT("/Game/Maps/Lobby"), TRAVEL_Absolute);
}

int32 ABasicPlayerController::GivePlayerAchievement(const int32& InAchievementId, const int32& InAmount, const int32& InScore)
{
	if (auto ps = GetBasicPlayerState())
	{
		ps->GivePlayerAchievement(InAchievementId, InAmount, InScore);
	}
	return 0;
}


void ABasicPlayerController::EndPlay(const EEndPlayReason::Type EndPlayReason)
{
	if (auto tm = GetTimerManager())
	{
		tm->ClearAllTimersForObject(this);
	}

	Super::EndPlay(EndPlayReason);
}

void ABasicPlayerController::SetPawn(APawn* InPawn)
{
	Super::SetPawn(InPawn);

	auto CastedCharacter = GetValidObjectAs<ABasicPlayerCharacter>(InPawn);
	auto CastedPlayerState = GetValidObjectAs<ABasicPlayerState>(PlayerState);

	if (CastedCharacter && CastedPlayerState)
	{
		CastedPlayerState->OnInitializeCharacter();
	}
}

void ABasicPlayerController::PlayerTravel_Implementation(const FString& URL, const FString& Token)
{
	auto SharedToken = MakeShareable(new FUniqueNetIdString(Token, NULL_SUBSYSTEM));
	if (auto lp = GetValidObject(GetLocalPlayer()))
	{
		lp->SetCachedUniqueNetId(SharedToken);
		ClientTravel(URL + "?UserToken=" + Token, TRAVEL_Absolute);
	}
}

FTimerManager* ABasicPlayerController::GetTimerManager() const
{
	if (auto w = GetValidWorld())
	{
		return &w->GetTimerManager();
	}
	return nullptr;
}

FTimerManager* ABasicPlayerController::GetTimerManager()
{
	if (auto w = GetValidWorld())
	{
		return &w->GetTimerManager();
	}
	return nullptr;
}

//================================================
AGameMode* ABasicPlayerController::GetBaseGameMode() const
{
	if (auto world = GetValidWorld())
	{
		return GetValidObjectAs<AGameMode>(world->GetAuthGameMode());
	}
	return nullptr;
}

AGameMode* ABasicPlayerController::GetBaseGameMode()
{
	if (auto world = GetValidWorld())
	{
		return GetValidObjectAs<AGameMode>(world->GetAuthGameMode());
	}
	return nullptr;
}

ABasicGameMode* ABasicPlayerController::GetBasicGameMode() const
{
	return GetGameModeAs<ABasicGameMode>();
}

ABasicGameMode* ABasicPlayerController::GetBasicGameMode()
{
	return GetGameModeAs<ABasicGameMode>();
}

//================================================
AGameState* ABasicPlayerController::GetBaseGameState() const
{
	if (auto w = GetValidWorld())
	{
		return GetValidObject(w->GetGameState<AGameState>());
	}
	return nullptr;
}

AGameState* ABasicPlayerController::GetBaseGameState()
{
	if (auto w = GetValidWorld())
	{
		return GetValidObject(w->GetGameState<AGameState>());
	}
	return nullptr;
}

//================================================

ABasicGameState* ABasicPlayerController::GetBasicGameState() const
{
	return GetGameStateAs<ABasicGameState>();
}

ABasicGameState* ABasicPlayerController::GetBasicGameState()
{
	return GetGameStateAs<ABasicGameState>();
}