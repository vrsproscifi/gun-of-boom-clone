
//=============================================
#include "BasicPlayerCharacter.h"
#include "BasicPlayerState.h"

//=============================================
#include "Components/SkeletalMeshComponent.h"
#include "Components/InputComponent.h"
#include "GameFramework/Controller.h"
#include "GameFramework/GameState.h"
#include "Engine/World.h"

//=============================================
#include "TimerManager.h"

//=============================================
#include "UnrealNetwork.h"


//=============================================
ABasicPlayerCharacter::ABasicPlayerCharacter(const FObjectInitializer& ObjectInitializer)
{
	//PrimaryActorTick.bRunOnAnyThread = true;
}

void ABasicPlayerCharacter::PostInitializeComponents()
{
	Super::PostInitializeComponents();
	if (auto mesh = GetMesh())
	{
		mesh->SetComponentTickEnabledAsync(true);
	}
}

AGameState* ABasicPlayerCharacter::GetBaseGameState()
{
	if (auto world = GetValidWorld())
	{
		return GetValidObject(world->GetGameState<AGameState>());
	}
	return nullptr;
}

AGameState* ABasicPlayerCharacter::GetBaseGameState() const
{
	if (auto world = GetValidWorld())
	{
		return GetValidObject(world->GetGameState<AGameState>());
	}
	return nullptr;
}

bool ABasicPlayerCharacter::IsAlive() const
{
	return true;
}

bool ABasicPlayerCharacter::IsFirstPerson() const
{
	return IsAlive() && GetValidObject(Controller) && Controller->IsLocalPlayerController();
}

float ABasicPlayerCharacter::PlayTwoAnimMontage(FTwoAnimMontage& Animation)
{
	auto UseAnim = IsFirstPerson() ? Animation.GetPawn1P() : Animation.GetPawn3P();
	if (IsValidObject(UseAnim))
	{
		return PlayAnimMontage(UseAnim);
	}
	return 0.0f;
}

int32 ABasicPlayerCharacter::GivePlayerAchievement(const int32& InAchievementId, const int32& InAmount, const int32& InScore )
{
	if (auto ps = GetBasePlayerState())
	{
		ps->GivePlayerAchievement(InAchievementId, InAmount, InScore);
	}
	return 0;
}

void ABasicPlayerCharacter::EndPlay(const EEndPlayReason::Type EndPlayReason)
{
	if (auto tm = GetTimerManager())
	{
		tm->ClearAllTimersForObject(this);
	}

	Super::EndPlay(EndPlayReason);
}

void ABasicPlayerCharacter::SetupPlayerInputComponent(UInputComponent* PlayerInputComponent)
{
	check(PlayerInputComponent);

	PlayerInputComponent->BindAxis("MoveForward", this, &ABasicPlayerCharacter::MoveForward);
	PlayerInputComponent->BindAxis("MoveRight", this, &ABasicPlayerCharacter::MoveRight);
	PlayerInputComponent->BindAxis("MoveUp", this, &ABasicPlayerCharacter::MoveUp);
	PlayerInputComponent->BindAxis("Turn", this, &ABasicPlayerCharacter::TurnAtValue);
	PlayerInputComponent->BindAxis("TurnRate", this, &ABasicPlayerCharacter::TurnAtRate);
	PlayerInputComponent->BindAxis("LookUp", this, &ABasicPlayerCharacter::LookUpAtValue);
	PlayerInputComponent->BindAxis("LookUpRate", this, &ABasicPlayerCharacter::LookUpAtRate);
}

void ABasicPlayerCharacter::InitializeInstance(UBasicPlayerItem* InInstance)
{
	Instance = InInstance;
	OnRep_Instance();
}

void ABasicPlayerCharacter::InitializeItem(UBasicPlayerItem* InInstance, bool IsDefault)
{
	
}

void ABasicPlayerCharacter::OnRep_Instance()
{
	
}

void ABasicPlayerCharacter::GetLifetimeReplicatedProps(TArray< FLifetimeProperty > & OutLifetimeProps) const
{
	Super::GetLifetimeReplicatedProps(OutLifetimeProps);

	DOREPLIFETIME(ABasicPlayerCharacter, Instance);
}


FTimerManager* ABasicPlayerCharacter::GetTimerManager() const
{
	if (auto w = GetValidWorld())
	{
		return &w->GetTimerManager();
	}
	return nullptr;
}

FTimerManager* ABasicPlayerCharacter::GetTimerManager()
{
	if (auto w = GetValidWorld())
	{
		return &w->GetTimerManager();
	}
	return nullptr;
}

