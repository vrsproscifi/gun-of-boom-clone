// Fill out your copyright notice in the Description page of Project Settings.

#include "WeaponPlayerItem.h"
#include "Weapon/WeaponItemEntity.h"

UWeaponItemEntity* UWeaponPlayerItem::GetWeaponEntity() const
{
	return GetEntity<UWeaponItemEntity>();
}

void UWeaponPlayerItem::ReplaceEntity(UBasicGameEntity* entity)
{
	Super::ReplaceEntity(entity);
	SetLevel(0);
}
