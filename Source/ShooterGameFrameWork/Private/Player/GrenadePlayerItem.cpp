// Fill out your copyright notice in the Description page of Project Settings.

#include "GrenadePlayerItem.h"
#include "Grenade/GrenadeItemEntity.h"

UGrenadeItemEntity* UGrenadePlayerItem::GetGrenadeEntity() const
{
	return GetEntity<UGrenadeItemEntity>();
}
