#include "BasicPlayerComponent.h"

//=============================================
#include "Engine/Network/LokaNetUserInterface.h"

//=============================================
#include "Engine/Http/RequestManager.h"
#include "Classes/Engine/World.h"

//=============================================
#include "Player/BasicPlayerState.h"
#include "Player/BasicPlayerController.h"

//=============================================
#include "Extensions/GameSingletonExtensions.h"



//=============================================
UBasicPlayerComponent::UBasicPlayerComponent()
{
	SetIsReplicated(true);

	auto ps = GetBasicPlayerState();
	if (ps)
	{
		ps->OnAuthorizationCompleteDelegate.AddUObject(this, &UBasicPlayerComponent::OnAuthorizationComplete_Proxy);
	}
}

bool UBasicPlayerComponent::PlayRewardVideo(const FPlayRewardedVideoRequest& InVideoRequest)const
{
	auto ps = GetBasicPlayerState();
	if (ps)
	{
		return ps->PlayRewardVideo(InVideoRequest);
	}
	return false;
}

void UBasicPlayerComponent::OnPlayRewardVideoSuccess(const FPlayRewardedVideoResponse& InResponse)
{
}


void UBasicPlayerComponent::OnPlayRewardVideoCanceled(const FPlayRewardedVideoResponse& InResponse)
{
}

bool UBasicPlayerComponent::IsAllowServerQueries() const
{
	return true;
}

TScriptInterface<ILokaNetUserInterface> UBasicPlayerComponent::GetUserOwner() const
{
	return TScriptInterface<ILokaNetUserInterface>(GetOwner());
}

TAttribute<FGuid> UBasicPlayerComponent::GetIdentityToken() const
{
	return TAttribute<FGuid>::Create([&]() -> FGuid
	{
		if (HasLocalClientComponent())
		{
			return FGameSingletonExtensions::GetLastLocalToken();
		}
		
		if (auto MyUser = GetValidObjectAs<ILokaNetUserInterface>(GetOwner()))
		{
			return MyUser->GetIdentityToken();
		}

		return FGuid();
	});
}

FGuid UBasicPlayerComponent::GetPlayerId() const
{
	if(const auto ps = GetBasicPlayerState())
	{
		return ps->GetPlayerId();
	}
	return FGuid();
}

int32 UBasicPlayerComponent::GetPlayerLevel() const
{
	if (const auto ps = GetBasicPlayerState())
	{
		return ps->GetPlayerLevel();
	}
	return -1;
}


ABasicPlayerController * UBasicPlayerComponent::GetBaseController() const
{
	if (const auto o = GetBasicPlayerState<APlayerState>())
	{
		return GetValidObjectAs<ABasicPlayerController>(o->GetOwner());
	}
	return nullptr;
}

ABasicPlayerController * UBasicPlayerComponent::GetBaseController()
{
	if (const auto o = GetBasicPlayerState<APlayerState>())
	{
		return GetValidObjectAs<ABasicPlayerController>(o->GetOwner());
	}
	return nullptr;
}

bool UBasicPlayerComponent::HasLocalClientComponent() const
{
	if (GetBaseController() && GetBaseController<APlayerController>()->IsLocalController())
	{
		if (GetNetMode() == NM_Client || GetNetMode() == NM_ListenServer || GetNetMode() == NM_Standalone)
		{
			return true;
		}
	}

	return false;
}


void UBasicPlayerComponent::BeginPlay()
{
	Super::BeginPlay();

	if (!HasAnyFlags(RF_ClassDefaultObject | RF_ArchetypeObject))
	{
		if (GetValidWorld())
		{
			if (HasLocalClientComponent())
			{
				if (FApp::IsGame())
				{
					CreateGameWidgets();
				}
			}
		}
		else
		{
			UE_LOG(LogInit, Error, TEXT("[UBasicPlayerComponent::BeginPlay][World was nullptr]"));
		}
	}
}

void UBasicPlayerComponent::EndPlay(const EEndPlayReason::Type EndPlayReason)
{
	//===============================
	Super::EndPlay(EndPlayReason);
}

void UBasicPlayerComponent::OnAuthorizationComplete()
{

}