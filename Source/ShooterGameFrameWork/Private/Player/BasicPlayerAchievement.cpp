//==============================
#include "BasicPlayerAchievement.h"

//==============================
#include "GameSingletonExtensions.h"

//==============================
#include "Game/BasicGameAchievement.h"

//==============================
#include "UnrealNetwork.h"

UBasicPlayerAchievement::UBasicPlayerAchievement(const FObjectInitializer& ObjectInitializer)
	: Amount(0)
{
}

void UBasicPlayerAchievement::GetLifetimeReplicatedProps(TArray<FLifetimeProperty>& OutLifetimeProps) const
{
	Super::GetLifetimeReplicatedProps(OutLifetimeProps);

	DOREPLIFETIME(UBasicPlayerAchievement, Amount);
}

void UBasicPlayerAchievement::OnRep_Instance()
{
	SetEntity(FGameSingletonExtensions::FindAchievementById(EntityModel));
}