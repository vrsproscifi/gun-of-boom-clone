// Fill out your copyright notice in the Description page of Project Settings.

#include "ArmourPlayerItem.h"
#include "Armour/ArmourItemEntity.h"

UArmourItemEntity* UArmourPlayerItem::GetArmourEntity() const
{
	return GetEntity<UArmourItemEntity>();
}
