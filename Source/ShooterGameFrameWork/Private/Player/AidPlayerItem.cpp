// Fill out your copyright notice in the Description page of Project Settings.

#include "AidPlayerItem.h"
#include "Aid/AidItemEntity.h"

UAidItemEntity* UAidPlayerItem::GetAidEntity() const
{
	return GetEntity<UAidItemEntity>();
}
