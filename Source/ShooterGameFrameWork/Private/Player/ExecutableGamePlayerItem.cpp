//==============================
#include "ExecutableGamePlayerItem.h"

//==============================
#include <UnrealNetwork.h>
#include "Engine/Engine.h"

//==============================
UExecutableGamePlayerItem::UExecutableGamePlayerItem(const FObjectInitializer& ObjectInitializer)
	: Super(ObjectInitializer)
	, bActivated(false)
{
	PrimaryComponentTick.bCanEverTick = true;
}

void UExecutableGamePlayerItem::PostInitProperties()
{
	//----------------------------------------
	Super::PostInitProperties();

	//----------------------------------------
	ProcessTimerDelegate = FTimerDelegate::CreateUObject(this, &UExecutableGamePlayerItem::OnProcess);
}

void UExecutableGamePlayerItem::TickComponent(float DeltaTime, ELevelTick TickType,	FActorComponentTickFunction* ThisTickFunction)
{
	Super::TickComponent(DeltaTime, TickType, ThisTickFunction);

	//if (auto MyWorld = GetValidObject(GetWorld()))
	//{
	//	const float CurrentTime = (GetExecuteTime() + GetExecuteInterval()) - MyWorld->GetTimeSeconds();
	//	const float CalculatedValue = FMath::Clamp<float>(CurrentTime / GetExecuteInterval(), 0.0f, 1.0f);

	//	GEngine->AddOnScreenDebugMessage(64, 20.0f, FColor::White, FString::Printf(TEXT("GetExecutePercent >> %.1f"), CalculatedValue));
	//}
}

void UExecutableGamePlayerItem::OnRep_Activated()
{
	if (auto MyWorld = GetValidObject(GetWorld()))
	{
		if (bActivated)
		{
			ActivatedTime = MyWorld->GetTimeSeconds();
		}
	}
}

void UExecutableGamePlayerItem::BeginDestroy()
{	
	//----------------------------------------
	StopProcess();

	//----------------------------------------
	if (auto tm = GetTimerManager())
	{
		tm->ClearAllTimersForObject(this);
	}

	//----------------------------------------
	Super::BeginDestroy();
}

void UExecutableGamePlayerItem::StopTimer()
{
	StopGameTimer();
	SetProcessState(false);
}

bool UExecutableGamePlayerItem::StartGameTimer(const float& InRate, const bool& InbLoop, const float& InFirstDelay)
{
	//========================================
	if(auto tm = GetTimerManager())
	{
		tm->SetTimer(ProcessTimerHandle, ProcessTimerDelegate, InRate, InbLoop, InFirstDelay);
		return true;
	}

	//========================================
	return false;
}

bool UExecutableGamePlayerItem::StopGameTimer()
{
	//========================================
	if (auto tm = GetTimerManager())
	{
		tm->ClearTimer(ProcessTimerHandle);
		return true;
	}

	//========================================
	else
	{
		ProcessTimerHandle.Invalidate();
		return false;
	}
}

void UExecutableGamePlayerItem::SetProcessState(bool InIsStarted)
{
	//----------------------------------------
	bActivated = InIsStarted;

	//----------------------------------------
	OnRep_Activated();
}

bool UExecutableGamePlayerItem::IsAllowStartProcess() const
{
	return IsValidTimerHandle() == false && IsActivated() == false;
}

void UExecutableGamePlayerItem::UseItem()
{
	Super::UseItem();
	OnPlayerItemUsed.ExecuteIfBound(GetInstance());
}

float UExecutableGamePlayerItem::GetExecuteInterval() const
{
	return 0.250f;
}

float UExecutableGamePlayerItem::GetExecuteTime() const
{
	return ActivatedTime;
}

float UExecutableGamePlayerItem::GetReadyPercent() const
{
	if (auto MyWorld = GetValidObject(GetWorld()))
	{
		return FMath::Clamp((MyWorld->GetTimeSeconds() - GetExecuteTime()) / GetExecuteInterval(), .0f, 1.0f);
	}

	return .0f;
}

float UExecutableGamePlayerItem::GetExecutePercent() const
{
	if (auto MyWorld = GetValidObject(GetWorld()))
	{
		const float CurrentTime = (GetExecuteTime() + GetExecuteInterval()) - MyWorld->GetTimeSeconds();
		const float CalculatedValue = FMath::Clamp<float>(CurrentTime / GetExecuteInterval(), 0.0f, 1.0f);

		return CalculatedValue;
	}

	return .0f;
}

bool UExecutableGamePlayerItem::IsValidTimerHandle() const
{
	return ProcessTimerHandle.IsValid();
}


void UExecutableGamePlayerItem::GetLifetimeReplicatedProps(TArray< FLifetimeProperty > & OutLifetimeProps) const
{
	//----------------------------------------
	Super::GetLifetimeReplicatedProps(OutLifetimeProps);

	//----------------------------------------
	DOREPLIFETIME(UExecutableGamePlayerItem, bActivated);
}
