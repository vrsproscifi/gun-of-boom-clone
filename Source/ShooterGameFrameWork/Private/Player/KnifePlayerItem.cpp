// Fill out your copyright notice in the Description page of Project Settings.

#include "KnifePlayerItem.h"
#include "Weapon/KnifeItemEntity.h"

UKnifeItemEntity* UKnifePlayerItem::GetKnifeEntity() const
{
	return GetEntity<UKnifeItemEntity>();
}