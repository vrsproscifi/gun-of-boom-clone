//=========================================
#include "GameViewportExtensions.h"

//=========================================
#include "Engine/Engine.h"

//=========================================
UGameViewportClient* GetGameViewportClient()
{
	if (GEngine && GEngine->IsValidLowLevel())
	{
		return GetValidObject(GEngine->GameViewport);
	}
	return nullptr;
}
