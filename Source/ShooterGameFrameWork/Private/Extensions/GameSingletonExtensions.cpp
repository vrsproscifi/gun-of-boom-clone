//=============================================
#include "GameSingletonExtensions.h"
#include "ShooterGameSingleton.h"
//=============================================
#include "Engine.h"

//=============================================
int32 FGameSingletonExtensions::GameSingletonLoadAttemps = 1;


//=============================================
UShooterGameSingleton* FGameSingletonExtensions::Get()
{
	return CastChecked<UShooterGameSingleton>(GEngine->GameSingleton);
}

bool FGameSingletonExtensions::IsGameSingletonLoaded()
{
	return Get()->bGameSingletonLoaded;
}

bool FGameSingletonExtensions::IsGameSingletonAllowReload()
{
	return GameSingletonLoadAttemps <= 10;
}

void FGameSingletonExtensions::ReloadGameSingleton()
{
	GameSingletonLoadAttemps++;
	return Get()->ReloadGameData();
}

const float* FGameSingletonExtensions::FindBonesDamageRate(const FName& InName)
{
	return Get()->FindBonesDamageRate(InName);
}

TSubclassOf<UObject> FGameSingletonExtensions::FindSubClassByName(const FName& InClassName, UClass* InDefaultClass)
{
	if (const auto object = FindSubClassByName(InClassName))
	{
		return object;
	}
	return InDefaultClass;
}

TSubclassOf<UObject> FGameSingletonExtensions::FindSubClassByName(const FName& InClassName)
{
	return Get()->FindSubClassByName(InClassName);
}

ULevelAwardEntity* FGameSingletonExtensions::FindLevelAwardById(const int32& InLevel)
{
	return Get()->FindLevelAwardById(InLevel);
}

UEverydayAwardEntity* FGameSingletonExtensions::FindEverydayAwardById(const int32& InDayStep)
{
	return Get()->FindEverydayAwardById(InDayStep);
}

const TArray<UEverydayAwardEntity*>& FGameSingletonExtensions::GetEverydayAwards()
{
	return Get()->GetEverydayAwards();
}


UBasicGameAchievement* FGameSingletonExtensions::FindAchievementById(const int32& InModelId)
{
	return Get()->FindAchievementById(InModelId);
}


UBasicItemEntity* FGameSingletonExtensions::FindItemById(const int32& InModelId)
{
	return Get()->FindItemById(InModelId);
}

TArray<UBasicItemEntity*> FGameSingletonExtensions::FindItemsByType(const EGameItemType& InModelType)
{
	return Get()->FindItemsByType(InModelType);
}


UCaseEntity* FGameSingletonExtensions::FindCaseById(const int32& InModelId)
{
	return Get()->FindCaseById(InModelId);
}

const TArray<UBasicItemEntity*>& FGameSingletonExtensions::GetItems()
{
	return Get()->GetItems();
}

const TArray<UDataAsset*>& FGameSingletonExtensions::GetDataAssets()
{
	return Get()->GetDataAssets();
}


void FGameSingletonExtensions::SetLastLocalToken(const FGuid& id)
{
	Get()->LastLocalToken = id;
}

FGuid FGameSingletonExtensions::GetLastLocalToken()
{
	return Get()->LastLocalToken;
}

void FGameSingletonExtensions::SetLastLocalPlayerId(const FGuid& id)
{
	Get()->LastLocalPlayerId = id;
}

FGuid FGameSingletonExtensions::GetLastLocalPlayerId()
{
	return Get()->LastLocalPlayerId;
}
