//==========================================
#include "SubClassMap.h"

//==========================================
USubClassMap::USubClassMap(const FObjectInitializer& ObjectInitializer)
	: Super(ObjectInitializer)
{

}

void USubClassMap::UpdateObjectsMap()
{
	//======================================
	ObjectsMap.Empty(32);

	//======================================
	for (auto &obj : Objects)
	{
		if (ObjectsMap.Contains(obj.Name))
		{
			UE_LOG(LogInit, Error, TEXT("> [USubClassMap::UpdateObjectsMap][Bone %s duplicate]"), *obj.Name.ToString());
		}

		ObjectsMap.Add(obj.Name, obj.Object);
	}
}
