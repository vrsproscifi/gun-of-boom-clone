//==========================================
#include "ShooterImpactEffect.h"

//==========================================
#include "PhysicalMaterials/PhysicalMaterial.h"

#include "Components/PrimitiveComponent.h"
#include "Components/DecalComponent.h"
#include "Particles/ParticleSystem.h"
#include "Kismet/GameplayStatics.h"
#include "Sound/SoundCue.h"
#include "Math/Rotator.h"
#include "Materials/Material.h"

AShooterImpactEffect::AShooterImpactEffect(const FObjectInitializer& ObjectInitializer) : Super(ObjectInitializer)
{
	SetAutoDestroyWhenFinished(true);
}

void AShooterImpactEffect::PostInitializeComponents()
{
	Super::PostInitializeComponents();

	UPhysicalMaterial* HitPhysMat = SurfaceHit.PhysMaterial.Get();
	EPhysicalSurface HitSurfaceType = UPhysicalMaterial::DetermineSurfaceType(HitPhysMat);

	// show particles
	UParticleSystem* ImpactFX = GetImpactFX(HitSurfaceType);
	if (ImpactFX)
	{
		UGameplayStatics::SpawnEmitterAtLocation(this, ImpactFX, GetActorLocation(), GetActorRotation());
	}

	// play sound
	USoundCue* ImpactSound = GetImpactSound(HitSurfaceType);
	if (ImpactSound)
	{
		UGameplayStatics::PlaySoundAtLocation(this, ImpactSound, GetActorLocation());
	}

	if (auto MyDecal = GetImpactDecal(HitSurfaceType))
	{
		FRotator RandomDecalRotation = SurfaceHit.ImpactNormal.Rotation();
		RandomDecalRotation.Roll = FMath::FRandRange(-180.0f, 180.0f);

		UDecalComponent* SpawnedDecalComp = UGameplayStatics::SpawnDecalAttached(MyDecal, FVector(-5.0f, DefaultDecal.DecalSize, DefaultDecal.DecalSize),
			SurfaceHit.Component.Get(), SurfaceHit.BoneName,
			SurfaceHit.ImpactPoint, RandomDecalRotation, EAttachLocation::KeepWorldPosition,
			DefaultDecal.LifeSpan);

		if (SpawnedDecalComp)
		{
			SpawnedDecalComp->FadeScreenSize = .0001f;
		}
	}
}

UParticleSystem* AShooterImpactEffect::GetImpactFX(TEnumAsByte<EPhysicalSurface> SurfaceType) const
{
	const auto type = static_cast<EShooterPhysMaterialType::Type>(SurfaceType.GetValue());
	if (type >= EShooterPhysMaterialType::Default && type < EShooterPhysMaterialType::End && FX[SurfaceType])
	{
		return FX[SurfaceType];
	}

	return FX[0];
}

USoundCue* AShooterImpactEffect::GetImpactSound(TEnumAsByte<EPhysicalSurface> SurfaceType) const
{
	const auto type = static_cast<EShooterPhysMaterialType::Type>(SurfaceType.GetValue());
	if (type >= EShooterPhysMaterialType::Default && type < EShooterPhysMaterialType::End && Sound[SurfaceType])
	{
		return Sound[SurfaceType];
	}

	return Sound[0];
}

UMaterial* AShooterImpactEffect::GetImpactDecal(TEnumAsByte<EPhysicalSurface> SurfaceType) const
{
	const auto type = static_cast<EShooterPhysMaterialType::Type>(SurfaceType.GetValue());

	if (type >= EShooterPhysMaterialType::Default && type < EShooterPhysMaterialType::End && DefaultDecal.DecalMaterial[SurfaceType])
	{
		return DefaultDecal.DecalMaterial[SurfaceType];
	}

	return DefaultDecal.DecalMaterial[0];
}