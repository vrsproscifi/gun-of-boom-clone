// Fill out your copyright notice in the Description page of Project Settings.

#include "WorldFlagsData.h"
#include "Materials/MaterialInstanceDynamic.h"

UWorldFlagsData::UWorldFlagsData()
{
	
}

void UWorldFlagsData::PostInitProperties()
{
	Super::PostInitProperties();

	RefreshMIDS();
}

UMaterialInstanceDynamic* UWorldFlagsData::GetFlagMaterialByCode(const EIsoCountry& InCode) const
{
	if (FlagsMIDS.Contains(InCode))
	{
		return FlagsMIDS.FindChecked(InCode);
	}

	return nullptr;
}

int32 UWorldFlagsData::GetFlagIndexByCode(const EIsoCountry& InCode) const
{
	if (FlagsMap.Contains(InCode))
	{
		return FlagsMap.FindChecked(InCode);
	}

	return INDEX_NONE;
}

UTexture2D* UWorldFlagsData::GetFlagsTexture() const
{
	return FlagsSheet;
}

FVector2D UWorldFlagsData::GetFlagsTextureSubImages() const
{
	return SheetSubImages;
}
#if WITH_EDITOR
void UWorldFlagsData::PostEditChangeProperty(FPropertyChangedEvent& PropertyChangedEvent)
{
	Super::PostEditChangeProperty(PropertyChangedEvent);

	if (PropertyChangedEvent.ChangeType == EPropertyChangeType::ArrayAdd)
	{
		RefreshMIDS();
	}
}
#endif
void UWorldFlagsData::RefreshMIDS()
{
	FlagsMIDS.Empty();

	for (auto RawPair : FlagsMap)
	{
		UMaterialInstanceDynamic* TmpMat = UMaterialInstanceDynamic::Create(FlagMaterial, this);
		TmpMat->SetScalarParameterValue(Parameter_SheetIndex, RawPair.Value);
		TmpMat->SetTextureParameterValue(Parameter_SheetTexture, GetFlagsTexture());
		TmpMat->SetVectorParameterValue(Parameter_SheetSubImages, FVector(GetFlagsTextureSubImages(), 0));

		FlagsMIDS.Add(RawPair.Key, TmpMat);
	}
}
