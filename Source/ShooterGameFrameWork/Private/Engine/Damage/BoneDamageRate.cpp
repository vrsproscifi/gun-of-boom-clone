//==========================================
#include "BoneDamageRate.h"

//==========================================
UBoneDamageRate::UBoneDamageRate(const FObjectInitializer& ObjectInitializer) 
	: Super(ObjectInitializer)
{

}

void UBoneDamageRate::UpdateRatesMap()
{
	//======================================
	RatesMap.Empty(32);

	//======================================
	for (auto &bone : Rates)
	{
		if (RatesMap.Contains(bone.Name))
		{
			UE_LOG(LogInit, Error, TEXT("> [UBoneDamageRate::UpdateRatesMap][Bone %s duplicate]"), *bone.Name.ToString());
		}
		RatesMap.Add(bone.Name, bone.Rate);
	}
}
