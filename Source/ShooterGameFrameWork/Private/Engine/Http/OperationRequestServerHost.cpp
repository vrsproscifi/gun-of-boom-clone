//============================================
#include "OperationRequestServerHost.h"

//============================================
#define MASTER_SERVER_ADDRESS 2

//============================================
//
#if MASTER_SERVER_ADDRESS == 0			// FOR LOCAL SENTIKE BUILD
FOperationRequestServerHost const FOperationRequestServerHost::MasterClient = FOperationRequestServerHost("http://localhost:4444", "client");
FOperationRequestServerHost const FOperationRequestServerHost::MasterServer = FOperationRequestServerHost("http://localhost:4444", "server");
#elif MASTER_SERVER_ADDRESS == 1		// FOR Development BUILD
FOperationRequestServerHost const FOperationRequestServerHost::MasterClient = FOperationRequestServerHost("http://78.46.46.148:4443", "client");
FOperationRequestServerHost const FOperationRequestServerHost::MasterServer = FOperationRequestServerHost("http://78.46.46.148:4443", "server");
#elif MASTER_SERVER_ADDRESS == 2		// FOR Stable BUILD
FOperationRequestServerHost const FOperationRequestServerHost::MasterClient = FOperationRequestServerHost("http://136.243.2.83:4444", "client");
FOperationRequestServerHost const FOperationRequestServerHost::MasterServer = FOperationRequestServerHost("http://136.243.2.83:4444", "server");
#endif

//============================================
FString const FVerb::Post = "POST";
FString const FVerb::Get = "GET";
