//============================================
#include "RequestManager.h"
#include "Extensions/UObjectExtensions.h"

//============================================
#include "Runtime/Core/Public/Misc/Base64.h"
#include "Runtime/Online/HTTP/Public/Http.h"
#include "Runtime/Online/HTTP/Public/Interfaces/IHttpRequest.h"
#include <TimerManager.h>
#include "Engine/World.h"

UOperationRequest::UOperationRequest(const FObjectInitializer& ObjectInitializer)
	: Super(ObjectInitializer)
	, TimerItter(0)		
	, TimerRate(0.0f)
	, TimerValidate(false)
	, TimerLimit(0)
	, TimerLoop(EOperationRequestTimerMethod::None)

{
	TimerProxy.BindUObject(this, &UOperationRequest::OnTimerItterationProxy);
}


void UOperationRequest::OnPreDestruct()
{	
	//===============================
	if (auto tm = GetTimerManager())
	{
		tm->ClearAllTimersForObject(this);
	}

	//================================
	Handlers.Empty();

	//================================
	OnLoseAttemps.Unbind();
	OnFailedResponse.Unbind();

	//================================
	TimerDelegate.Unbind();
	TimerProxy.Unbind();
	TimerHandle.Invalidate();

	//================================
	if (Request.IsValid())
	{
		Request.Pin()->OnProcessRequestComplete().Unbind();
		Request.Pin()->CancelRequest();
	}
}

void UOperationRequest::SetServerHost(const FOperationRequestServerHost& InHost)
{
	ServerHost = InHost;
}

void UOperationRequest::SetOperationName(const FString& InName)
{
	OperationName = InName;
}

void UOperationRequest::SetTimerRequestMethod(const EOperationRequestTimerMethod& InMethod)
{
	TimerLoop = InMethod;
}

void UOperationRequest::SetTimerAttempLimit(const int32& InAttempLimits)
{
	TimerLimit = InAttempLimits;
}

void UOperationRequest::SetTimerRequestDelegate(const FTimerDelegate& InDelegate)
{
	TimerDelegate = InDelegate;
}

void UOperationRequest::SetTimerValidationState(const bool & InValidate)
{
	TimerValidate = InValidate;
}

void UOperationRequest::SetOperationToken(const TokenAttrib& InTokenAttrib)
{
	Token = InTokenAttrib;
}


void UOperationRequest::SetTimerRequestRate(const float& InRate)
{
	TimerRate = InRate;
}


void UOperationRequest::EnableTimerValidation()
{
	TimerValidate = true;
}

void UOperationRequest::DisableTimerValidation()
{
	TimerValidate = false;
}

void UOperationRequest::BeginDestroy()
{
	OnPreDestruct();
	Super::BeginDestroy();
}

FGuid UOperationRequest::ChoseToken(TokenAttribRef token) const
{
	if (Token.IsSet())
	{
		if (Token.Get().IsValid())
		{
			return Token.Get();
		}
	}
	return token.Get();
}

static bool HttpSendRequest(const FOperationRequestServerHost& host, const FString& Url, const FString& Data, const FString& Token, const FHttpRequestCompleteDelegate& Delegate, TWeakPtr<IHttpRequest>& request, const FString& Verb)
{
	FHttpModule::Get().SetHttpTimeout(5.0f);
	FHttpModule::Get().SetMaxReadBufferSize(8 * 1024 * 1024);
	TSharedRef<IHttpRequest> HttpRequest = FHttpModule::Get().CreateRequest();

	HttpRequest->SetContentAsString(Data);
	HttpRequest->OnProcessRequestComplete() = Delegate;
	HttpRequest->SetHeader(TEXT("Content-Type"), TEXT("application/json"));

	if (Token.IsEmpty() == false)
	{
		auto TokenBase = FBase64::Encode(Token);
		HttpRequest->SetHeader(TEXT("Authorization"), FString::Printf(TEXT("%s %s"), *host.Scheme, *TokenBase));
	}

	auto fullUrl = FString::Printf(TEXT("%s/%s"), *host.Host, *Url);

	HttpRequest->SetVerb(Verb);
	HttpRequest->SetURL(fullUrl);
	request = HttpRequest;

	return HttpRequest->ProcessRequest();
}

void UOperationRequest::OnProxyRequestComplete(const UOperationRequest* Context, TSharedPtr<class IHttpRequest> HttpRequest, TSharedPtr<class IHttpResponse, ESPMode::ThreadSafe> HttpResponse, bool bSucceeded)
{
	//==================================================
	if(IsValidObject(Context) == false)
	{
		return;
	}

	//==================================================
	const auto Token = Context->Token.Get().ToString();
	const auto& OperationName = Context->OperationName;
	const auto& Handlers = Context->Handlers;
	const auto&TimerLoop = Context->TimerLoop;

	//==================================================
	FRequestExecuteError error(OperationName);
	if (HttpResponse.IsValid())
	{		
		//==================================================

		//==================================================
		const auto code = HttpResponse->GetResponseCode();
		const auto content = HttpResponse->GetContentAsString().TrimQuotes().ReplaceEscapedCharWithChar();

		//==================================================
		error.Status = code;
		error.Content = content;

		//==================================================
		UE_LOG(LogHttp, Warning, TEXT("Request: %s -> [%s][%d] %s"), *OperationName, *Token, code, *content);


		//==================================================
		if (Handlers.Contains(code) && Handlers[code].IsValid())
		{
			if (Handlers[code]->OnDeSerializeProxy(content))
			{
				if (Context->TimerDelegate.IsBound())
				{
					Context->ResetItterations();
				}
				return;
			}
			error.Error = ERequestErrorCode::SerializationFailed;
		}
		else if (code == 401 || code >= 900)
		{
			error.Error = ERequestErrorCode::Unauthorized;
		}
		else if (code == 500)
		{
			error.Error = ERequestErrorCode::ServerError;
		}
		else if (error.IsTimeout())
		{
			error.Error = ERequestErrorCode::Timeout;
		}
		else if (error.IsBadGateway())
		{
			error.Error = ERequestErrorCode::BadGateway;
		}
		else if (code == 200)
		{
			UE_LOG(LogHttp, Warning, TEXT("OnHttpRequest[Operation: %s][Code: %d] not bind!"), *OperationName, error.Status);
			return;
		}
		else
		{
			error.Error = ERequestErrorCode::HandlerNotFound;
		}
	}
	else
	{
		error.Content = TEXT("HttpResponse is invalid");
		error.Error = ERequestErrorCode::BadResponse;
	}

	Context->PrewExecuteError = Context->LastExecuteError;
	Context->LastExecuteError = error;

	if ((TimerLoop == EOperationRequestTimerMethod::CallViaIntervalOrResponse || TimerLoop == EOperationRequestTimerMethod::CallViaResponse) && Context->TimerDelegate.IsBound())
	{
		Context->ItterationInc();
		UE_LOG(LogHttp, Error, TEXT("OnHttpRequestComplete[Operation: %s][Code: %d][Token: %s][GetItterationNum: %d]"), *OperationName, error.Status, *Token, Context->GetItterationNum());
		Context->OnTimerItterationProxy();
	}
	else
	{
		UE_LOG(LogHttp, Error, TEXT("OnHttpRequestComplete[Operation: %s][Code: %d][Token: %s]"), *OperationName, error.Status, *Token);
		Context->OnFailedResponse.ExecuteIfBound(error);
	}
}

void UOperationRequest::GetRequest(TokenAttribRef token) const
{
	if (IsValidObject(this) == false)
	{
		return;
	}

	SendRequest("", FVerb::Get, token);
}

void UOperationRequest::SendRequest(const FString& InContent, const FString& verb, TokenAttribRef token) const
{
	if(IsValidObject(this) == false)
	{
		return;
	}

	FHttpRequestCompleteDelegate OnRequestComplete;
	OnRequestComplete.BindLambda([this, c = this](FHttpRequestPtr HttpRequest, FHttpResponsePtr HttpResponse, bool bSucceeded)
	{
		if (GetValidObject(c))
		{
			OnProxyRequestComplete(c, HttpRequest, HttpResponse, bSucceeded);
		}
	});

	LastExecuteDate = FDateTime::Now();
	auto RequestToken = ChoseToken(token);
	auto StringToken = RequestToken.IsValid() ? RequestToken.ToString() : FString();
	HttpSendRequest(ServerHost, OperationName, InContent, StringToken, OnRequestComplete, Request, verb);
}


FString UOperationRequest::GetItterationString() const
{
	return FString::Printf(TEXT("%d of %d)"), GetItterationNum(), GetItterationLimit());
}

int32 UOperationRequest::GetItterationLimit() const
{
	return TimerLimit;
}

int32 UOperationRequest::GetItterationNum() const
{
	return TimerItter;
}

bool UOperationRequest::ItterationInc() const
{
	if (IsValidTimer())
	{
		TimerItter++;
		return true;
	}
	return false;
}

void UOperationRequest::ResetItterations() const
{
	TimerItter = 0;
}

bool UOperationRequest::IsValidTimer() const
{
	return TimerValidate == false || TimerValidate && TimerItter < TimerLimit;
}

bool UOperationRequest::IsTimerActive() const
{
	return TimerHandle.IsValid();
}

void UOperationRequest::StartTimer(const float& InStartDelay) const
{
	if (IsValidObject(this))
	{
		TimerStartDate = FDateTime::Now();

		const bool bIsLoopTimer =	TimerLoop == EOperationRequestTimerMethod::CallViaInterval || 
									TimerLoop == EOperationRequestTimerMethod::CallViaIntervalOrResponse;

		if (auto TM = GetTimerManager())
		{
			TM->SetTimer(TimerHandle, TimerProxy, TimerRate, bIsLoopTimer, InStartDelay);
		}
	}
}

FTimerManager* UOperationRequest::GetTimerManager() const
{
	auto World = GetValidObject(GetWorld());
	if(World)
	{
		return &World->GetTimerManager();
	}
	return nullptr;
}

void UOperationRequest::StopTimer() const
{
	if (auto TM = GetTimerManager())
	{
		if (TimerHandle.IsValid())
		{
			TM->ClearTimer(TimerHandle);
		}
	}
}

void UOperationRequest::OnTimerItterationProxy() const
{
	if (IsValidObject(this))
	{
		if (IsValidTimer())
		{
			TimerDelegate.ExecuteIfBound();
		}
		else
		{
			OnLoseAttemps.ExecuteIfBound(LastExecuteError);
		}
		TimerLastExecuteDate = FDateTime::Now();
	}
}
