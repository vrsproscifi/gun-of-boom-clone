// Fill out your copyright notice in the Description page of Project Settings.

#include "KnifeItemEntity.h"
#include "KnifePlayerItem.h"

UKnifeItemEntity::UKnifeItemEntity()
	: Super()
{
	KnifeProperty.Type = EGameItemType::Knife;
	InventoryClass = UKnifePlayerItem::StaticClass();
}

float UKnifeItemEntity::GetDefaultDynamicValue() const
{
	return KnifeProperty.Damage;
}
