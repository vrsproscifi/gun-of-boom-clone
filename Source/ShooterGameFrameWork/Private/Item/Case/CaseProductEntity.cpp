
#include "CaseProductEntity.h"
#include "GameSingletonExtensions.h"
#include "CaseEntity.h"


UCaseProductEntity::UCaseProductEntity(const FObjectInitializer& ObjectInitializer)
{
	InventoryClass = nullptr;
}

void UCaseProductEntity::Initialize(const FBasicItemContainer& initializer)
{
	Super::Initialize(initializer);

	GetCaseProductProperty().CaseEntity = FGameSingletonExtensions::FindCaseById(GetCaseProductProperty().AdditionalId);
}

UCaseEntity* UCaseProductEntity::GetCaseEntity() const
{
	if(const auto entity = GetValidObject(GetCaseProductProperty().CaseEntity))
	{
		return entity;
	}
	return FGameSingletonExtensions::FindCaseById(GetCaseProductProperty().AdditionalId);
}

UCaseEntity* UCaseProductEntity::GetCaseEntity()
{
	if (const auto entity = GetValidObject(GetCaseProductProperty().CaseEntity))
	{
		return entity;
	}
	return FGameSingletonExtensions::FindCaseById(GetCaseProductProperty().AdditionalId);
}

const FCaseProperty* UCaseProductEntity::GetCaseEntityProperty() const
{
	if(auto entity = GetCaseEntity())
	{
		return &entity->GetCaseProperty();
	}
	return nullptr;
}

const FCaseProperty* UCaseProductEntity::GetCaseEntityProperty()
{
	if (auto entity = GetCaseEntity())
	{
		return &entity->GetCaseProperty();
	}
	return nullptr;
}

const TArray<FCaseItemProperty>* UCaseProductEntity::GetDropedItems() const
{
	if (auto entity = GetCaseEntity())
	{
		return &entity->GetDropedItems();
	}
	return nullptr;
}

const TArray<FCaseItemProperty>* UCaseProductEntity::GetDropedItems()
{
	if (auto entity = GetCaseEntity())
	{
		return &entity->GetDropedItems();
	}
	return nullptr;
}