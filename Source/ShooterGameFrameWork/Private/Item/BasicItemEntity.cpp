//======================================
#include "BasicItemEntity.h"

//======================================
#include "Item/BasicItemContainer.h"

//======================================
#include "UObjectExtensions.h"
#include "BasicPlayerItem.h"

//======================================

UBasicItemEntity::UBasicItemEntity(const FObjectInitializer& ObjectInitializer)
{
	InventoryClass = UBasicPlayerItem::StaticClass();
}

//===============================================
const FGameItemCost* UBasicItemEntity::GetNonDiscountedCost() const
{
	auto &ItemCost = GetItemProperty().Cost;
	return &ItemCost;
}

const FGameItemCost* UBasicItemEntity::GetDiscountedCost() const
{
	auto &ItemCost = GetItemProperty().DiscountedCost;
	return &ItemCost;
}

const EProductDiscountCoupone& UBasicItemEntity::GetActiveDiscount() const
{
	auto& _ProductProperty = GetItemProperty();
	return _ProductProperty.ActiveDiscount;
}

int32 UBasicItemEntity::GetActiveDiscountPercent() const
{
	auto ActiveDiscount = GetActiveDiscount();
	switch (ActiveDiscount)
	{
	case EProductDiscountCoupone::Percent_5: return 5;
	case EProductDiscountCoupone::Percent_15: return 15;
	case EProductDiscountCoupone::Percent_30: return 30;
	case EProductDiscountCoupone::Percent_50: return 50;
	case EProductDiscountCoupone::Percent_70: return 70;
	}
	return 0;
}


bool UBasicItemEntity::IsDiscountedProduct() const
{
	const auto ActiveDiscount = GetActiveDiscount();
	return ActiveDiscount != EProductDiscountCoupone::None && GetAvalibleDiscountTime() > FTimespan::Zero();
}

FTimespan UBasicItemEntity::GetAvalibleDiscountTime() const
{
	auto& _ProductProperty = GetItemProperty();
	return _ProductProperty.EndActiveDiscountDate - FDateTime::UtcNow();
}

void UBasicItemEntity::SetActiveDiscount(const EProductDiscountCoupone& InDiscountCoupone,
	const FDateTime& InDiscountCouponeEndDate)
{
	GetItemProperty().ActiveDiscount = InDiscountCoupone;
	GetItemProperty().EndActiveDiscountDate = InDiscountCouponeEndDate;
	GetItemProperty().DiscountedCost = GetItemProperty().Cost;
	GetItemProperty().DiscountedCost.Amount *= ((100 - GetActiveDiscountPercent()) / 100.0f);
}

FString UBasicItemEntity::GetModelName() const
{
	const auto& productId = GetItemProperty().ProductId;
	if (productId.IsEmpty())
	{
		return Super::GetModelName();
	}
	return productId;
}

void UBasicItemEntity::UpdateEditorHelperProperties()
{
	Super::UpdateEditorHelperProperties();
#if WITH_EDITOR && WITH_METADATA
	ProductId = GetItemProperty().ProductId;
#endif
}


FString UBasicItemEntity::GetEntityName(const UBasicItemEntity* InItem)
{
	if (InItem == nullptr)
	{
		return "nullptr";
	}

	return FString::Printf(TEXT("%s / %s [id: %d] [type: %d]"), *InItem->GetName(), *InItem->GetItemProperty().Title.ToString(), InItem->GetItemModelId(), static_cast<int32>(InItem->GetItemProperty().Type));
}

float UBasicItemEntity::GetDynamicValueForLevel(const int32& InLevel) const
{
	auto modifications = GetItemProperty().Modifications;
	const auto modification = FBasicItemProperty::GetSafeModification(modifications, InLevel);
	if(modification == nullptr)
	{
		return GetDefaultDynamicValue();
	}
	return modification->Damage;
}

void UBasicItemEntity::Initialize(const FBasicItemContainer& initializer)
{
	//================================================
	TArray<FGameItemModification> modifications;

	//================================================
	GetItemProperty().Cost = initializer.Cost;
	GetItemProperty().Level = initializer.Level;
	GetItemProperty().Flags = initializer.Flags;
	GetItemProperty().AdditionalId = initializer.AdditionalId;
	GetItemProperty().AmountInProduct = initializer.AmountInProduct;

	//================================================
	if (initializer.Modifications.Num())
	{
		modifications.Add(FGameItemModification(GetDefaultDynamicValue()));
	}

	//================================================
	for (const auto &m : initializer.Modifications)
	{
		modifications.Add(m);
	}

	//================================================
	GetItemProperty().Modifications = modifications;

	//================================================
	UE_LOG(LogInit, Display, TEXT("> UBasicItemEntity::Initialize[%s  /%d][Modifications: %d]"), *GetName(), GetModelId(), initializer.Modifications.Num());

	//================================================
	for (auto& im : GetItemProperty().Modifications)
	{
		//-------------------------------------------
		im.Damage = im.Coefficient * GetDefaultDynamicValue();

		//-------------------------------------------
		if (im.Position > 0 && (im.Cost.Amount == 0 || im.Coefficient <= 0))
		{
			UE_LOG(LogInit, Error, TEXT("> UBasicItemEntity::Initialize[%s  /%d][Invalid modification at %d Position]"), *GetName(), GetModelId(), im.Position);
		}

		//-------------------------------------------
	}

	//-------------------------------------------
	GetItemProperty().Modifications.Sort([&](const FGameItemModification& InA, const FGameItemModification& InB)
	{
		return InA.Position < InB.Position;
	});

}

FMeshAnimationPair UBasicItemEntity::GetPreviewMeshData() const
{
	return PreviewMeshData;
}

bool UBasicItemEntity::IsHiddenInShop() const
{
	return GetItemProperty().bIsHiddenInShop;
}

bool UBasicItemEntity::HasAllowSomeBought() const
{
	return EnumHasAnyFlags(GetItemProperty().Flags, EItemInstanceFlags::AllowSome);
}

bool UBasicItemEntity::HasIgnoreRequirements() const
{
	return EnumHasAnyFlags(GetItemProperty().Flags, EItemInstanceFlags::IgnoreRequirements);
}

USkeletalMesh* UBasicItemEntity::GetSkeletalMesh() const
{
	return GetValidObject(GetPreviewMeshData().Mesh);
}

bool UBasicItemEntity::IsService() const
{
	return EnumHasAnyFlags(GetItemProperty().Flags, EItemInstanceFlags::Service);
}

bool UBasicItemEntity::IsCurrency() const
{
	return EnumHasAnyFlags(GetItemProperty().Flags, EItemInstanceFlags::Currency);
}

bool UBasicItemEntity::IsProduct() const
{
	return EnumHasAnyFlags(GetItemProperty().Flags, EItemInstanceFlags::Product) || (!IsService() && !IsCurrency() && !IsCase());
}

bool UBasicItemEntity::IsCase() const
{
	return EnumHasAnyFlags(GetItemProperty().Flags, EItemInstanceFlags::Case);
}