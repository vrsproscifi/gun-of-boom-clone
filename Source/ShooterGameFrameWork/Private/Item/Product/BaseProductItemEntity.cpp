
#include "BaseProductItemEntity.h"

UBaseProductItemEntity::UBaseProductItemEntity(const FObjectInitializer& ObjectInitializer)
{

}

void UBaseProductItemEntity::UpdateEditorHelperProperties()
{
	Super::UpdateEditorHelperProperties();
#if WITH_EDITOR && WITH_METADATA
	ProductCategory = GetBaseProductProperty().ProductCategory;
#endif
}

bool UBaseProductItemEntity::IsInAppPurchase() const
{
	const auto& category = GetProductCategory();

	if (category == EProductItemCategory::Donate) return true;
	if (category == EProductItemCategory::Booster) return true;
	if (category == EProductItemCategory::BattleCoins) return true;

	return false;
}

const EProductItemCategory& UBaseProductItemEntity::GetProductCategory() const
{
	return GetBaseProductProperty().ProductCategory;
}

FString UBaseProductItemEntity::GetProductCategoryName() const
{
	const auto& category = GetProductCategory();
	if (category == EProductItemCategory::Donate) return "Donate";
	if (category == EProductItemCategory::Booster) return "Booster";
	if (category == EProductItemCategory::BattleCoins) return "BattleCoins";
	return "Unk";
}


uint8 UBaseProductItemEntity::GetProductCategoryId() const
{
	return static_cast<uint8>(GetProductCategory());
}
