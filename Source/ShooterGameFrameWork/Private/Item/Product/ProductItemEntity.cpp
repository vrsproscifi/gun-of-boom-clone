
#include "ProductItemEntity.h"
#include "Culture.h"
#include "ProductItemContainer.h"

EIsoCountry UProductItemEntity::CurrentPlayerCountry = EIsoCountry::None;


UProductItemEntity::UProductItemEntity(const FObjectInitializer& ObjectInitializer)
{
}


static const FGameItemCost* FindTargetCostByGameCurrency(const TArray<FGameItemCost>& InCosts, const EIsoCountry& InIsoCountry)
{
	return InCosts.FindByPredicate([InIsoCountry](const FGameItemCost& InItemCost) { return InItemCost.Country == InIsoCountry; });
}

static const FGameItemCost* FindTargetCostByGameCurrency(const TArray<FGameItemCost>& InCosts, const TArray<EIsoCountry>& InIsoCountries)
{
	for (const auto& Country : InIsoCountries)
	{
		const auto Result = FindTargetCostByGameCurrency(InCosts, Country);
		if(Result != nullptr)
		{
			return Result;
		}
	}

	if(InCosts.Num())
	{
		return &InCosts[0];
	}

	return nullptr;
}

FString UProductItemEntity::GetTargetProductId() const
{
	auto& _ProductProperty = GetProductProperty();
	if (_ProductProperty.Costs.Num() && IsDiscountedProduct())
	{
		const auto ActiveDiscount = GetActiveDiscount();
		return _ProductProperty.ProductId + GetProductDiscountCouponeSuffix(ActiveDiscount);
	}

	return _ProductProperty.ProductId;
}

void UProductItemEntity::InitializeProduct(const FProductItemContainer& ProductContainer)
{
	ProductProperty.Costs = ProductContainer.CostsV2;
}

//===============================================
const TArray<FGameItemCost>* UProductItemEntity::GetCostsByDiscountCoupone(const EProductDiscountCoupone& InDiscountCoupone) const
{
	auto CostCollection = ProductProperty.Costs.FindByPredicate([InDiscountCoupone](const FProductItemCostCollection& InCostCollection) { return InCostCollection.Discount == InDiscountCoupone; });
	if (CostCollection)
	{
		return &CostCollection->Costs;
	}
	return nullptr;
}

//===============================================
const FGameItemCost* UProductItemEntity::GetNonDiscountedCost() const
{
	auto NonDiscountedCosts = GetCostsByDiscountCoupone(EProductDiscountCoupone::None);
	if (NonDiscountedCosts)
	{
		auto NonDiscountedCost = FindTargetCostByGameCurrency(*NonDiscountedCosts, { CurrentPlayerCountry, EIsoCountry::US, EIsoCountry::DE, EIsoCountry::RU });
		if (NonDiscountedCost)
		{
			return NonDiscountedCost;
		}
	}
	return nullptr;
}

const FGameItemCost* UProductItemEntity::GetDiscountedCost() const
{
	const auto ActiveDiscount = GetActiveDiscount();
	auto DiscountedCosts = GetCostsByDiscountCoupone(ActiveDiscount);
	if (DiscountedCosts)
	{
		auto DiscountedCost = FindTargetCostByGameCurrency(*DiscountedCosts, { CurrentPlayerCountry, EIsoCountry::US, EIsoCountry::DE, EIsoCountry::RU });
		if (DiscountedCost)
		{
			return DiscountedCost;
		}
	}

	return GetNonDiscountedCost();
}

const FGameItemCost& UProductItemEntity::GetItemCost() const
{
	auto NonDiscountedCost = GetNonDiscountedCost();
	if (NonDiscountedCost)
	{
		return *NonDiscountedCost;
	}
	return Super::GetItemCost();
}
