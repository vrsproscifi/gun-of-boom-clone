#include "AidItemEntity.h"

UAidItemEntity::UAidItemEntity(const FObjectInitializer& ObjectInitializer)
{

}


void UAidItemEntity::UpdateEditorHelperProperties()
{
	Super::UpdateEditorHelperProperties();
#if WITH_EDITOR && WITH_METADATA
	Amount = AidProperty.Amount;
	ActivationTime = AidProperty.ActivationTime;
	ActivationDelay = AidProperty.ActivationDelay;
#endif
}
