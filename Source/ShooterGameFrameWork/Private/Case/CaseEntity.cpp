
#include "CaseEntity.h"
#include "CaseContainer.h"
#include "GameSingletonExtensions.h"


UCaseEntity::UCaseEntity(const FObjectInitializer& ObjectInitializer)
{

}

void UCaseEntity::Initialize(const FCaseContainer& initializer)
{
	//====================================
	GetCaseProperty().Items.Empty(8);

	//====================================
	GetCaseProperty().ModelId = initializer.EntityId;
	GetCaseProperty().Delay = FTimespan::FromSeconds(initializer.Dalay);
	GetCaseProperty().bAllowGetBonusCase = initializer.IsAllowGetBonusCase;

	//====================================
	for (const auto &i : initializer.Items)
	{
		//--------------------------------
		FCaseItemProperty item;

		//--------------------------------
		item.ModelId = i.ItemModelId;
		item.RandomMin = i.RandomMin;
		item.RandomMax = i.RandomMax;
		item.DropMethod = i.DropMethod;
		item.ModelEntity = FGameSingletonExtensions::FindItemById(item.ModelId);

		//--------------------------------
		GetCaseProperty().Items.Add(item);
	}

}
