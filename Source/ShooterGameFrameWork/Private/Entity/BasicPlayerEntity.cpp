//==============================
#include "BasicPlayerEntity.h"

//==============================
#include "BasicGameEntity.h"

//==============================
#include "UnrealNetwork.h"
#include "GameSingletonExtensions.h"

UBasicPlayerEntity::UBasicPlayerEntity(const FObjectInitializer& ObjectInitializer)
	: EntityModel(-1)
	, EntityInstance(nullptr)
{
	SetIsReplicated(true);
}

void UBasicPlayerEntity::GetLifetimeReplicatedProps(TArray<FLifetimeProperty>& OutLifetimeProps) const
{
	Super::GetLifetimeReplicatedProps(OutLifetimeProps);

	DOREPLIFETIME_CONDITION(UBasicPlayerEntity, EntityId, COND_OwnerOnly);
	DOREPLIFETIME(UBasicPlayerEntity, EntityModel);
}

bool UBasicPlayerEntity::IsEqualModelId(const UBasicPlayerEntity* InEntity, const int32& InModelId)
{
	if (const auto entity = GetValidObject(InEntity))
	{
		return entity->IsEqual(InModelId);
	}
	return false;
}


bool UBasicPlayerEntity::IsEqual(const UBasicPlayerEntity* InEntity, const FGuid& InEntityId)
{
	if(const auto entity = GetValidObject(InEntity))
	{
		return entity->IsEqual(InEntityId);
	}
	return false;
}

void UBasicPlayerEntity::OnRep_Instance()
{
	EntityInstance = nullptr;
}

UBasicGameEntity* UBasicPlayerEntity::GetEntity() const
{
	return GetValidObject(EntityInstance);
}

UBasicGameEntity* UBasicPlayerEntity::GetEntity()
{
	return GetValidObject(EntityInstance);
}

void UBasicPlayerEntity::SetEntityModel(const int32& InModelId)
{
	EntityModel = InModelId;
	OnRep_Instance();
}

void UBasicPlayerEntity::SetEntity_Intrenal(UBasicGameEntity* entity)
{
	EntityInstance = entity;
}

void UBasicPlayerEntity::SetEntity(UBasicGameEntity* entity)
{
	SetEntity_Intrenal(entity);
	if (const auto e = GetEntity())
	{
		EntityModel = e->GetModelId();
	}
	OnRep_Instance();
}

void UBasicPlayerEntity::ReplaceEntity(UBasicGameEntity* entity)
{
	SetEntity(entity);
}
