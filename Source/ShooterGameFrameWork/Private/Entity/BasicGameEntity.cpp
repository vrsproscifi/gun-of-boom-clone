#include "BasicGameEntity.h"

const FBasicGameProperty UBasicGameEntity::Default;

UBasicGameEntity::UBasicGameEntity(const FObjectInitializer& ObjectInitializer)
{
	
}

void UBasicGameEntity::UpdateEditorHelperProperties()
{
#if WITH_EDITOR
	ModelId = GetBasicProperty().ModelId;
	Title = GetBasicProperty().Title;
	Description = GetBasicProperty().Description;
#endif
}
#if WITH_EDITOR && WITH_METADATA

void UBasicGameEntity::PostInitProperties()
{
	Super::PostInitProperties();
	//UpdateEditorHelperProperties();
}

bool UBasicGameEntity::Modify(bool bAlwaysMarkDirty) 
{
	UpdateEditorHelperProperties();
	return Super::Modify(bAlwaysMarkDirty);
}

void UBasicGameEntity::PostLoad() 
{
	Super::PostLoad();
	UpdateEditorHelperProperties();
}
//
void UBasicGameEntity::PostEditChangeProperty(struct FPropertyChangedEvent& PropertyChangedEvent) 
{
	Super::PostEditChangeProperty(PropertyChangedEvent);
	UpdateEditorHelperProperties();
}

void UBasicGameEntity::PostEditUndo()
{
	Super::PostEditUndo();
	UpdateEditorHelperProperties();
}
#endif

FString UBasicGameEntity::GetEntityName(const UBasicGameEntity* InItem)
{
	if (InItem == nullptr)
	{
		return "nullptr";
	}

	return FString::Printf(TEXT("%s / %s [id: %d] [type: %d]"), *InItem->GetName(), *InItem->GetBasicProperty().Title.ToString(), InItem->GetBasicProperty().ModelId);
}