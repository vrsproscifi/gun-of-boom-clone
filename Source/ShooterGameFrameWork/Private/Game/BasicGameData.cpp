//=====================================
#include "Game/BasicGameData.h"


void UBasicGameData::PostLoad()
{
	Super::PostLoad();
	Flags = ToFlag();
}

#if WITH_EDITOR
void UBasicGameData::PostEditChangeProperty(struct FPropertyChangedEvent& PropertyChangedEvent)
{
	Super::PostEditChangeProperty(PropertyChangedEvent);
	if (PropertyChangedEvent.GetPropertyName() == "Value")
	{
		Flags = ToFlag();
	}
}
#endif