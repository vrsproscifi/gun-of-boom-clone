//=====================================
#include "Game/BasicGameMode.h"
#include "Game/BasicGameState.h"

//=====================================
#include "Player/BasicPlayerController.h"
#include "Player/BasicPlayerState.h"


//=====================================
#include "Engine/World.h"
#include "TimerManager.h"
#include "GameFramework/WorldSettings.h"

//=====================================

ABasicGameMode::ABasicGameMode(const FObjectInitializer& ObjectInitializer)
{
	GameStateClass = ABasicGameState::StaticClass();
	PlayerStateClass = ABasicPlayerState::StaticClass();
	PlayerControllerClass = ABasicPlayerController::StaticClass();
}

UWorld* ABasicGameMode::GetValidWorld() const
{
	return GetValidObject(GetWorld());
}

UWorld* ABasicGameMode::GetValidWorld()
{
	return GetValidObject(GetWorld());
}

APlayerController * ABasicGameMode::GetFirstPlayerController() const
{
	if (auto world = GetValidWorld())
	{
		return world->GetFirstPlayerController();
	}
	return nullptr;
}

void ABasicGameMode::InitGame(const FString& MapName, const FString& Options, FString& ErrorMessage)
{
	Super::InitGame(MapName, Options, ErrorMessage);

	//=============================================================
	UE_LOG(LogInit, Display, TEXT("==============="));
	UE_LOG(LogInit, Display, TEXT("  Init Game Option: %s | Map: %s | ErrorMessage: %s"), *Options, *MapName, *ErrorMessage);

	//=============================================================
}

void ABasicGameMode::EndPlay(const EEndPlayReason::Type EndPlayReason)
{
	//=============================================================
	GetWorldTimerManager().ClearAllTimersForObject(this);
	GetWorldTimerManager().ClearAllTimersForObject(GetBaseGameState<AGameState>());

	//=============================================================
	Super::EndPlay(EndPlayReason);
}

void ABasicGameMode::PreInitializeComponents()
{
	Super::PreInitializeComponents();

	//=============================================================
	UE_LOG(LogGameMode, Log, TEXT("ABasicGameMode::PreInitializeComponents [0]"));


	//=============================================================
	if (!HasAnyFlags(RF_ClassDefaultObject | RF_ArchetypeObject))
	{
		UE_LOG(LogGameMode, Log, TEXT("ABasicGameMode::PreInitializeComponents [1]"));

		if (auto w = GetValidWorld())
		{
			UE_LOG(LogGameMode, Log, TEXT("ABasicGameMode::PreInitializeComponents [2]"));
			w->GetTimerManager().SetTimer(TimerHandle_DefaultTimer, this, &ABasicGameMode::DefaultTimer, GetWorldSettings()->GetEffectiveTimeDilation() / GetWorldSettings()->DemoPlayTimeDilation, true);
		}
	}
}

void ABasicGameMode::DefaultTimer()
{

}

#pragma region GetPlayerState


APlayerState * ABasicGameMode::GetPlayerState(AController* InController) const
{
	if (auto c = GetValidObject(InController))
	{
		return GetValidObject(c->PlayerState);
	}
	return nullptr;
}

APlayerState * ABasicGameMode::GetPlayerState(AController* InController)
{
	if (auto c = GetValidObject(InController))
	{
		return GetValidObject(c->PlayerState);
	}
	return nullptr;
}

APlayerState * ABasicGameMode::GetPlayerState(APawn* InPawn) const
{
	if (auto c = GetValidObject(InPawn))
	{
		return GetValidObject(c->GetPlayerState());
	}
	return nullptr;
}

APlayerState * ABasicGameMode::GetPlayerState(APawn* InPawn)
{
	if (auto c = GetValidObject(InPawn))
	{
		return GetValidObject(c->GetPlayerState());
	}
	return nullptr;
}

#pragma endregion


bool ABasicGameMode::GivePlayerAchievement(AController* InController, const int32& InAchievementId, const int32& InAmount, const int32& InScore )
{
	if (auto ps = GetPlayerState<ABasicPlayerState>(InController))
	{
		ps->GivePlayerAchievement(InAchievementId, InAmount, InScore);
		return true;
	}
	return false;
}

bool ABasicGameMode::GetAchievements(AController* InController, TArray<FPlayerGameAchievement>& OutAchievements) const
{
	if (auto ps = GetPlayerState<ABasicPlayerState>(InController))
	{
		OutAchievements = ps->GetAchievements();
		return true;
	}
	return false;
}


TArray<APlayerState*> ABasicGameMode::GetPlayerStates(const bool& InIsAllowBots, const bool& InIsAllowInactivePlayers) const
{
	if (auto gs = GetValidObject(GameState))
	{
		//=======================================
		TArray<APlayerState*> PlayerStatesResult;
		TArray<APlayerState*> PlayerStatesArray(GameState->PlayerArray);

		//=======================================
		if (InIsAllowInactivePlayers)
		{
			PlayerStatesArray.Append(InactivePlayerArray);
		}

		//=======================================
		if (InIsAllowBots)
		{
			return PlayerStatesArray;
		}

		//=======================================
		for (auto It : PlayerStatesArray)
		{
			if (auto ps = GetValidObject(It))
			{
				if (ps->bIsABot == false)
				{
					PlayerStatesResult.Add(ps);
				}
			}
		}

		//=======================================
		return PlayerStatesResult;
	}

	return TArray<APlayerState*>();
}
