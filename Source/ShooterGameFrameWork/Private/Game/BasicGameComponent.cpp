#include "BasicGameComponent.h"

//=============================================
#include "Engine/Http/RequestManager.h"
#include "Classes/Engine/World.h"

//=============================================
#include "TimerManager.h"

//=============================================
UBasicGameComponent::UBasicGameComponent()
{
	SetIsReplicated(true);
}

//===================================================================
//  Game  Mode Helper

AGameMode * UBasicGameComponent::GetBaseGameMode()
{
	if (auto w = GetValidWorld())
	{
		return GetValidObjectAs<AGameMode>(w->GetAuthGameMode());
	}
	return nullptr;
}

AGameMode * UBasicGameComponent::GetBaseGameMode() const
{
	if (auto w = GetValidWorld())
	{
		return GetValidObjectAs<AGameMode>(w->GetAuthGameMode());
	}
	return nullptr;
}

//===================================================================
AGameState* UBasicGameComponent::GetBaseGameState() const
{
	if (auto gm = GetBaseGameMode())
	{
		return GetValidObjectAs<AGameState>(gm->GameState);
	}
	return nullptr;

}

AGameState* UBasicGameComponent::GetBaseGameState()
{
	if (auto gm = GetBaseGameMode())
	{
		return GetValidObjectAs<AGameState>(gm->GameState);
	}
	return nullptr;
}


//===================================================================

UOperationRequest* UBasicGameComponent::CreateRequest(const FOperationRequestServerHost& server_host, const FString& operation_name, TokenAttrib token, const float& rate, const EOperationRequestTimerMethod& loop, FTimerDelegate timer)
{
	auto Request = NewObject<UOperationRequest>(this, FName(*operation_name));

	Request->SetServerHost(server_host);
	Request->SetOperationName(operation_name);

	Request->SetTimerRequestMethod(loop);
	Request->SetTimerRequestRate(rate);
	Request->SetTimerRequestDelegate(timer);
	Request->SetOperationToken(token);

	return AddRequest(Request);
}

UOperationRequest* UBasicGameComponent::CreateRequest(const FOperationRequestServerHost& server_host, const FString& operation_name, const float& rate, const EOperationRequestTimerMethod& loop, FTimerDelegate timer, TokenAttrib token, const int32& timerLimit, const bool& timerValidate)
{
	auto Request = NewObject<UOperationRequest>(this, FName(*operation_name));

	Request->SetServerHost(server_host);
	Request->SetOperationName(operation_name);

	Request->SetTimerRequestMethod(loop);
	Request->SetTimerRequestRate(rate);
	Request->SetTimerRequestDelegate(timer);
	Request->SetTimerValidationState(timerValidate);
	Request->SetTimerAttempLimit(timerLimit);
	Request->SetOperationToken(token);

	return AddRequest(Request);
}

UOperationRequest* UBasicGameComponent::CreateRequest(const FOperationRequestServerHost& server_host,
	const FString& operation_name, const float& rate, const EOperationRequestTimerMethod& loop, FTimerDelegate timer, const int32& timerLimit,
	const bool& timerValidate)
{
	return CreateRequest(server_host, operation_name, rate, loop, timer, GetIdentityToken(), timerLimit, timerValidate);
}

UOperationRequest* UBasicGameComponent::AddRequest(UOperationRequest* request)
{
	OperationRequests.Add(request);
	return request;
}

TAttribute<FGuid> UBasicGameComponent::GetIdentityToken() const
{
	return TAttribute<FGuid>::Create([&]() -> FGuid
	{
		return FGuid();
	});
}

bool UBasicGameComponent::HasAuthority() const
{
	return (GetOwner() && GetOwner()->HasAuthority());
}

void UBasicGameComponent::RemoveOperationRequests()
{
	UE_LOG(LogInit, Display, TEXT("UBasicGameComponent::RemoveOperationRequests | OperationRequests: %d"), OperationRequests.Num())

	//===============================
	if (auto tm = GetTimerManager())
	{
		tm->ClearAllTimersForObject(this);
		for (auto r : OperationRequests)
		{
			if (IsValidObject(r))
			{
				r->StopTimer();
				r->OnPreDestruct();
				tm->ClearAllTimersForObject(r);
			}
		}
	}
	else
	{
		for (auto r : OperationRequests)
		{
			if (IsValidObject(r))
			{
				r->OnPreDestruct();
			}
		}
	}

	//===============================
	OperationRequests.Reset();
}


void UBasicGameComponent::EndPlay(const EEndPlayReason::Type EndPlayReason)
{
	//===============================
	UE_LOG(LogInit, Display, TEXT("UBasicGameComponent::EndPlay | EndPlayReason : %d"), (int32)EndPlayReason)

	//===============================
	RemoveOperationRequests();

	//===============================
	Super::EndPlay(EndPlayReason);
}

void UBasicGameComponent::BeginDestroy()
{
	//===============================
	RemoveOperationRequests();

	//===============================
	Super::BeginDestroy();
}



UWorld* UBasicGameComponent::GetValidWorld() const
{
	return GetValidObject(GetWorld());
}

UWorld* UBasicGameComponent::GetValidWorld()
{
	return GetValidObject(GetWorld());
}

FTimerManager* UBasicGameComponent::GetTimerManager() const
{
	if (auto w = GetValidWorld())
	{
		return &w->GetTimerManager();
	}
	return nullptr;
}

FTimerManager* UBasicGameComponent::GetTimerManager()
{
	if (auto w = GetValidWorld())
	{
		return &w->GetTimerManager();
	}
	return nullptr;
}

void UBasicGameComponent::StartTimer(UOperationRequest* operation, const float& delay) const
{
	if (IsValidObject(operation))
	{
		operation->StartTimer(delay);
	}
}

void UBasicGameComponent::StartTimer(UOperationRequest* operation, const float& delay)
{
	if (IsValidObject(operation))
	{
		operation->StartTimer(delay);
	}
}

void UBasicGameComponent::StopTimer(UOperationRequest* operation) const
{
	if (IsValidObject(operation))
	{
		operation->StopTimer();
	}
}

void UBasicGameComponent::StopTimer(UOperationRequest* operation)
{
	if (IsValidObject(operation))
	{
		operation->StopTimer();
	}
}