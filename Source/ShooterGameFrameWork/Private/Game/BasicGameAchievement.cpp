#include "BasicGameAchievement.h"

UBasicGameAchievement::UBasicGameAchievement(const FObjectInitializer& ObjectInitializer)
{
}

void UBasicGameAchievement::UpdateEditorHelperProperties()
{
	Super::UpdateEditorHelperProperties();
#if WITH_EDITOR && WITH_METADATA
	ScorePoints= AchievementProperty.ScorePoints;
	bShowNotify= AchievementProperty.bShowNotify;
	bPlayNotify= AchievementProperty.bPlayNotify;
	bAllowOverride= AchievementProperty.bAllowOverride;
	bDisplayInPersonalResult= AchievementProperty.bDisplayInPersonalResult;
#endif
}

const FGameAchievementStepProperty* UBasicGameAchievement::GetStepById(const int32& id) const
{
	const auto r = Steps.FindByPredicate([&, id](const FGameAchievementStepProperty& property)
	{
		return id == property.ModelId;
	});

	return r;
}

const FGameAchievementStepProperty* UBasicGameAchievement::GetStepByStep(const int32& step) const
{
	const auto r = Steps.FindByPredicate([&, step](const FGameAchievementStepProperty& property)
	{
		return step == property.Step;
	});

	return r;
}

const FGameAchievementStepProperty* UBasicGameAchievement::GetStepByAmount(const int32& amount) const
{
	return nullptr;
}

bool UBasicGameAchievement::GetStepById(const int32& InId, FGameAchievementStepProperty& OutStep)
{
	auto step = GetStepById(InId);
	if (step)
	{
		OutStep = *step;
		return true;
	}
	return false;
}

bool UBasicGameAchievement::GetStepByStep(const int32& InStep, FGameAchievementStepProperty& OutStep)
{
	auto step = GetStepByStep(InStep);
	if (step)
	{
		OutStep = *step;
		return true;
	}
	return false;
}
