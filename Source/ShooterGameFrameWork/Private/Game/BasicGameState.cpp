//=====================================
#include "BasicGameState.h"

//=====================================
#include "Engine/World.h"

ABasicGameState::ABasicGameState(const FObjectInitializer& ObjectInitializer)
	: Super(ObjectInitializer)
{

}	

AGameMode* ABasicGameState::GetBaseGameMode()
{
	if (auto w = GetValidWorld())
	{
		return GetValidObjectAs<AGameMode>(w->GetAuthGameMode());
	}

	return nullptr;
}

AGameMode* ABasicGameState::GetBaseGameMode() const
{
	if (auto w = GetValidWorld())
	{
		return GetValidObjectAs<AGameMode>(w->GetAuthGameMode());
	}

	return nullptr;
}

UWorld* ABasicGameState::GetValidWorld()
{
	return GetValidObject(GetWorld());
}

UWorld* ABasicGameState::GetValidWorld() const
{
	return GetValidObject(GetWorld());
}
