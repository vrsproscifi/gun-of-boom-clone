//=====================================
#include "Game/GameMapData.h"
#include "Extensions/GameSingletonExtensions.h"

UGameMapData* UGameMapData::GetByFlagId(const int64& InFlagId)
{
	auto assets = FGameSingletonExtensions::GetDataAssets<UGameMapData>();
	if (auto r = assets.FindByPredicate([InFlagId](const UGameMapData* map)
	{
		return map->IsValidLowLevel() && map->ToFlag() == InFlagId;
	}))
	{
		return GetValidObject(*r);
	}
	return nullptr;
}


UGameMapData* UGameMapData::GetById(const int64& id)
{
	auto assets = FGameSingletonExtensions::GetDataAssets<UGameMapData>();
	if (auto r = assets.FindByPredicate([id](const UGameMapData* map)
	{
		return map->IsValidLowLevel() && map->GetValue() == id;
	}))
	{
		return GetValidObject(*r);
	}
	return nullptr;
}
