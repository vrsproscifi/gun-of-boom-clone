//=====================================
#include "Game/GameModeData.h"
#include "Extensions/GameSingletonExtensions.h"

//=====================================

UGameModeData* UGameModeData::GetByFlagId(const int64& InFlagId)
{
	auto assets = FGameSingletonExtensions::GetDataAssets<UGameModeData>();
	if (auto r = assets.FindByPredicate([InFlagId](const UGameModeData* map)
	{
		return map->IsValidLowLevel() && map->ToFlag() == InFlagId;
	}))
	{
		return GetValidObject(*r);
	}
	return nullptr;
}


UGameModeData* UGameModeData::GetById(const int64& id)
{
	auto assets = FGameSingletonExtensions::GetDataAssets<UGameModeData>();
	if (auto r = assets.FindByPredicate([id](const UGameModeData* map)
	{
		return map->IsValidLowLevel() && map->GetValue() == id;
	}))
	{
		return GetValidObject(*r);
	}
	return nullptr;
}
