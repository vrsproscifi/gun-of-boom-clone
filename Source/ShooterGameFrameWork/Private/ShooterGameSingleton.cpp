//=====================================
#include "ShooterGameSingleton.h"

//=====================================
#include "AssetRegistryModule.h"
#include "IAssetRegistry.h"
#include "Classes/Engine/Blueprint.h"
#include "Classes/Engine/DataAsset.h"
#include "UObject/ConstructorHelpers.h"
#include "Async.h"

//=====================================
#include "Item/BasicItemEntity.h"
#include "Game/BasicGameAchievement.h"
#include "Case/CaseEntity.h"

//=====================================
#include "Engine/Http/RequestManager.h"
#include "Engine/Damage/BoneDamageRate.h"
#include "Engine/SubClassMap/SubClassMap.h"
#include "Engine/Engine.h"

//=====================================
#include "Extensions/Entity/FindEntityBy.h"
#include "GameStoreContainer.h"
#include "ProductItemEntity.h"

//=====================================
#include "Award/EverydayAward/EverydayAwardEntity.h"
#include "Award/LevelAward/LevelAwardEntity.h"

//=====================================

UShooterGameSingleton::UShooterGameSingleton(const FObjectInitializer& ObjectInitializer)
	: ListOfProductsRequesDate(0)
{	
	//UOperationRequest::Factory(this, FOperationRequestServerHost::MasterClient, "Store/ListOfInstanceV2");
	ListOfProducts = NewObject<UOperationRequest>(this, "ListOfInstanceV2");
	ListOfProducts->SetServerHost(FOperationRequestServerHost::MasterClient);
	ListOfProducts->SetOperationName("Store/ListOfInstanceV2");

	ListOfProducts->BindObject<FGameStoreContainer>(200).AddUObject(this, &UShooterGameSingleton::OnListOfProductsSuccessfully);
	ListOfProducts->OnFailedResponse.BindUObject(this, &UShooterGameSingleton::OnListOfProductsFailed);
}

void UShooterGameSingleton::OnListOfProductsSuccessfully(const FGameStoreContainer& data)
{
	//==============================================
	bGameSingletonLoaded = data.Items.Num() > 0;

	//==============================================
	for (auto i : data.Cases)
	{
		//=================================
		auto instance = FindCaseById(i.EntityId);
		if (instance == nullptr)
		{
			UE_LOG(LogInit, Warning, TEXT("[OnListOfProductsSuccessfully][Case: %d][Instance was nullptr]"), i.EntityId);
			continue;
		}

		//=================================
		instance->Initialize(i);
	}

	//==============================================
	for (auto i : data.Level)
	{
		//=================================
		auto instance = FindLevelAwardById(i.EntityId);
		if (instance == nullptr)
		{
			instance = NewObject<ULevelAwardEntity>(this, ULevelAwardEntity::StaticClass(), FName(*FString::Printf(TEXT("Level_%02dSafe"), i.EntityId)));
			UE_LOG(LogInit, Warning, TEXT("[OnListOfProductsSuccessfully][dLevelAward: %d][Instance was nullptr]"), i.EntityId);
		}

		//=================================
		instance->Initialize(i);
	}

	//==============================================
	for (auto i : data.EveryDay)
	{
		//=================================
		auto instance = FindEverydayAwardById(i.EntityId);
		if (instance == nullptr)
		{
			UE_LOG(LogInit, Warning, TEXT("[OnListOfProductsSuccessfully][EveryDay: %d][Instance was nullptr]"), i.EntityId);
			continue;
		}
		instance->Initialize(i);
	}
	

	//==============================================
	for (auto i : data.Items)
	{
		//=================================
		auto instance = FindItemById(i.ModelId);
		if (instance == nullptr)
		{
			UE_LOG(LogInit, Warning, TEXT("[OnListOfProductsSuccessfully][Item: %d][Instance was nullptr]"), i.ModelId);
			continue;
		}

		//=================================
		instance->Initialize(i);
	}

	//==============================================
	for (auto i : data.Products)
	{
		//=================================
		auto instance = FindItemByName<UProductItemEntity>(i.ProductId);
		if (instance == nullptr)
		{
			UE_LOG(LogInit, Warning, TEXT("[OnListOfProductsSuccessfully][Product: %s][Instance was nullptr]"), *i.ProductId);
			continue;
		}

		//=================================
		instance->InitializeProduct(i);
	}
}

void UShooterGameSingleton::OnListOfProductsFailed(const FRequestExecuteError& error)
{
	UE_LOG(LogInit, Warning, TEXT("UShooterGameSingleton::OnListOfProductsFailed | Error: %s"), *error.ToString());
}

UShooterGameSingleton* UShooterGameSingleton::Get()
{
	return Cast<UShooterGameSingleton>(GEngine->GameSingleton);
}

void UShooterGameSingleton::ReloadGameData()
{
	UE_LOG(LogInit, Warning, TEXT("UShooterGameSingleton::ReloadGameData"));

	DamageRates = nullptr;
	SubClassMap = nullptr;
	Items.Empty(64);
	Cases.Empty(64);
	LevelAwards.Empty(64);
	EverydayAwards.Empty(16);
	Achievements.Empty(64);
	DataAssets.Empty();
	LoadGameData();
}

bool UShooterGameSingleton::IsLoadedData() const
{
	return !!Items.Num();
}

void UShooterGameSingleton::PostInitProperties()
{
	Super::PostInitProperties();
	UE_LOG(LogInit, Warning, TEXT("UShooterGameSingleton::PostInitProperties"));
	LoadGameData();
}

UBasicItemEntity* UShooterGameSingleton::FindItemByName(const FString& InModelName) const
{
	return FindEntityByName(Items, InModelName);
}

UBasicItemEntity* UShooterGameSingleton::FindItemByName(const FString& InModelName)
{
	return FindEntityByName(Items, InModelName);
}


UBasicItemEntity* UShooterGameSingleton::FindItemById(const int32& InModelId) const
{
	return FindEntityById(Items, InModelId);
}

UBasicItemEntity* UShooterGameSingleton::FindItemById(const int32& InModelId)
{
	return FindEntityById(Items, InModelId);
}

TArray<UBasicItemEntity*> UShooterGameSingleton::FindItemsByType(const EGameItemType& InModelType) const
{
	return GetItems().FilterByPredicate([InModelType](const UBasicItemEntity * InItemEntity) { return InItemEntity && InItemEntity->IsValidLowLevel() && InItemEntity->GetItemType() == InModelType; });
}

TArray<UBasicItemEntity*> UShooterGameSingleton::FindItemsByType(const EGameItemType& InModelType)
{
	return GetItems().FilterByPredicate([InModelType](const UBasicItemEntity * InItemEntity) { return InItemEntity && InItemEntity->IsValidLowLevel() && InItemEntity->GetItemType() == InModelType; });
}

UCaseEntity* UShooterGameSingleton::FindCaseById(const int32& InModelId) const
{
	return FindEntityById(Cases, InModelId);
}

UCaseEntity* UShooterGameSingleton::FindCaseById(const int32& InModelId)
{
	return FindEntityById(Cases, InModelId);
}

//-----------------------------------------------------------------------------------------------------

ULevelAwardEntity* UShooterGameSingleton::FindLevelAwardById(const int32& InLevel) const
{
	return FindEntityById(LevelAwards, InLevel);
}

ULevelAwardEntity* UShooterGameSingleton::FindLevelAwardById(const int32& InLevel)
{
	return FindEntityById(LevelAwards, InLevel);
}

UEverydayAwardEntity* UShooterGameSingleton::FindEverydayAwardById(const int32& InDayStep) const
{
	return FindEntityById(EverydayAwards, InDayStep);
}

UEverydayAwardEntity* UShooterGameSingleton::FindEverydayAwardById(const int32& InDayStep) 
{
	return FindEntityById(EverydayAwards, InDayStep);
}


//-----------------------------------------------------------------------------------------------------

UBasicGameAchievement * UShooterGameSingleton::FindAchievementById(const int32& InModelId) const
{
	return FindEntityById(Achievements, InModelId);
}

UBasicGameAchievement * UShooterGameSingleton::FindAchievementById(const int32& InModelId)
{
	return FindEntityById(Achievements, InModelId);
}

//-----------------------------------------------------------------------------------------------------

TSubclassOf<UObject> UShooterGameSingleton::FindSubClassByName(const FName& InClassName) const
{
	if (auto map = GetValidObject(SubClassMap))
	{	
		if (auto r = map->GetObjectsMap().Find(InClassName))
		{
			return *r;
		}
	}
	return nullptr;
}
//-----------------------------------------------------------------------------------------------------

struct FBlueprintAssetContainer
{
	UClass* Class;
	TArray<FAssetData> Assets;

	FBlueprintAssetContainer()
		: Class(nullptr)
	{

	}

	FBlueprintAssetContainer(UClass* c)
		: Class(c)
	{
		Assets.Reserve(2048);
	}
};

void GetAllAssetData(UClass* BaseClass, TArray<FAssetData>& AssetList, bool bRequireEntitlements = true)
{
	static FName RegistryModuleName = TEXT("AssetRegistry");

	// calling this with UBlueprint::StaticClass() is probably a bug where the user intended to call GetAllBlueprintAssetData()
	if (BaseClass == UBlueprint::StaticClass())
	{
#if DO_GUARD_SLOW
		UE_LOG(LogInit, Fatal, TEXT("GetAllAssetData() should not be used for blueprints; call GetAllBlueprintAssetData() instead"));
#else
		UE_LOG(LogInit, Error, TEXT("GetAllAssetData() should not be used for blueprints; call GetAllBlueprintAssetData() instead"));
#endif
		return;
	}
	// force disable local entitlement checks on dedicated server
	bRequireEntitlements = bRequireEntitlements && !IsRunningDedicatedServer();

	FAssetRegistryModule* AssetRegistryModule = nullptr;
	if (FModuleManager::Get().IsModuleLoaded(RegistryModuleName))
	{
		AssetRegistryModule = (FAssetRegistryModule*)FModuleManager::Get().GetModule(RegistryModuleName);
	}
	else
	{
		FModuleStatus ModuleStatus;
		if (FModuleManager::Get().QueryModule(RegistryModuleName, ModuleStatus) == false)
		{
			AssetRegistryModule = &FModuleManager::LoadModuleChecked<FAssetRegistryModule>(RegistryModuleName);
		}
	}

	if (AssetRegistryModule)
	{
		IAssetRegistry& AssetRegistry = AssetRegistryModule->Get();

		AssetRegistry.ScanPathsSynchronous({
			TEXT("/Game/Blueprints"),
			});
		FARFilter ARFilter;
		if (BaseClass != nullptr)
		{
			ARFilter.ClassNames.Add(BaseClass->GetFName());
			// Add any old names to the list in case things haven't been resaved
			TArray<FName> OldNames = FLinkerLoad::FindPreviousNamesForClass(BaseClass->GetPathName(), false);
			ARFilter.ClassNames.Append(OldNames);
		}
		ARFilter.bRecursivePaths = true;
		ARFilter.bIncludeOnlyOnDiskAssets = true;
		ARFilter.bRecursiveClasses = true;
		AssetRegistry.GetAssets(ARFilter, AssetList);
	}
}

const UBoneDamageRate* UShooterGameSingleton::GetDamageRates() const 
{
	return GetValidObject(DamageRates);
}


const float* UShooterGameSingleton::FindBonesDamageRate(const FName& InName) const
{
	if (auto rates = GetDamageRates())
	{
		return rates->GetRatesMap().Find(InName);
	}
	return nullptr;
}

void UShooterGameSingleton::ProcessLoadGameData()
{
	UE_LOG(LogInit, Warning, TEXT("UShooterGameSingleton::LoadGameData::AsyncTask"));

	TArray<FAssetData> Assets;
	GetAllAssetData(UDataAsset::StaticClass(), Assets);

	UE_LOG(LogInit, Warning, TEXT("UShooterGameSingleton::LoadGameData::AsyncTask::Assets: %d"), Assets.Num());

	for (auto TargetAsset : Assets)
	{
		if (auto CastedAsset = Cast<UBasicItemEntity>(TargetAsset.GetAsset()))
		{
			CastedAsset->AddToRoot();
			Items.Add(CastedAsset);
		}
		else if (auto CastedAchievement = Cast<UBasicGameAchievement>(TargetAsset.GetAsset()))
		{
			CastedAchievement->AddToRoot();
			Achievements.Add(CastedAchievement);
		}
		else if (auto CastedCase = Cast<UCaseEntity>(TargetAsset.GetAsset()))
		{
			CastedCase->AddToRoot();
			Cases.Add(CastedCase);
		}
		else if (auto CastedLevelAward = Cast<ULevelAwardEntity>(TargetAsset.GetAsset()))
		{
			CastedLevelAward->AddToRoot();
			LevelAwards.Add(CastedLevelAward);
		}
		else if (auto CastedEverydayAward = Cast<UEverydayAwardEntity>(TargetAsset.GetAsset()))
		{
			CastedEverydayAward->AddToRoot();
			EverydayAwards.Add(CastedEverydayAward);
		}
		else if (auto CastedBoneDamageRate = Cast<UBoneDamageRate>(TargetAsset.GetAsset()))
		{
			CastedBoneDamageRate->AddToRoot();
			DamageRates = CastedBoneDamageRate;
			DamageRates->UpdateRatesMap();
		}
		else if (auto CastedSubClassMap = Cast<USubClassMap>(TargetAsset.GetAsset()))
		{
			CastedSubClassMap->AddToRoot();
			SubClassMap = CastedSubClassMap;
			SubClassMap->UpdateObjectsMap();
		}
		else if (auto CastedDataAsset = Cast<UDataAsset>(TargetAsset.GetAsset()))
		{
			CastedDataAsset->AddToRoot();
			DataAssets.Add(CastedDataAsset);
		}
	}

	Items.Sort([&](const UBasicItemEntity& InA, const UBasicItemEntity& InB)
	{
		return InA.GetItemProperty().DisplayPosition < InB.GetItemProperty().DisplayPosition;
	});

	EverydayAwards.Sort([&](const UEverydayAwardEntity& InA, const UEverydayAwardEntity& InB)
	{
		return InA.GetModelId() < InB.GetModelId();
	});

	//======================================================================
	UE_LOG(LogInit, Warning, TEXT("UShooterGameSingleton::LoadGameData::AsyncTask::Assets: %d Complete \n\r"), Assets.Num());
	UE_LOG(LogInit, Warning, TEXT("> Items: %d"), Items.Num());
	UE_LOG(LogInit, Warning, TEXT("> Cases: %d"), Cases.Num());
	UE_LOG(LogInit, Warning, TEXT("> LevelAwards: %d"), LevelAwards.Num());
	UE_LOG(LogInit, Warning, TEXT("> EverydayAwards: %d"), EverydayAwards.Num());
	UE_LOG(LogInit, Warning, TEXT("> Achievements: %d"), Achievements.Num());
	UE_LOG(LogInit, Warning, TEXT("> DataAssets: %d"), DataAssets.Num());

	//======================================================================
	ListOfProducts->GetRequest();
}


void UShooterGameSingleton::LoadGameData()
{
	UE_LOG(LogInit, Warning, TEXT("UShooterGameSingleton::LoadGameData"));

	//======================================================================
#if UE_SERVER || 1
	ProcessLoadGameData();
#else
	AsyncTask(ENamedThreads::GameThread, [&]()
	{	
		ProcessLoadGameData();
	});
#endif
}